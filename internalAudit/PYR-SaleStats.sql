use ITREPORT;
set nocount on;
declare @soldDateFrom varchar(50)
	   ,@soldDateTo varchar(50);
set @soldDateFrom = '2557/01/08'+' 00:00:00000_0000';
set @soldDateTo = '2558/05/08'+' 00:00:00000_0000';

IF OBJECT_ID('tempdb..#TempQuerySA') IS NOT NULL  DROP TABLE #TempQuerySA; 
IF OBJECT_ID('tempdb..#TempQuerySV') IS NOT NULL  DROP TABLE #TempQuerySV; 
IF OBJECT_ID('tempdb..#TempQuerySB') IS NOT NULL  DROP TABLE #TempQuerySB; 

--การขายหน้าร้าน ...SPSale และ SPSaleDetai
select pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo as PartNo,sd.PartName,sd.BrandID,sd.CompCode,apfp.APForce,isnull(sum(sd.SaleQTY),0) as sumSoldQTY,'SA' as saleType
into #TempQuerySA
from 
	ITREPORT.dbo.wb_apForceParts apfp 
	left join DBSPS.dbo.SPSaleDetail sd on apfp.partNo=sd.PartNo and apfp.compCode=sd.CompCode
	left join dbsps.dbo.SPSale s on s.CompCode=sd.CompCode and s.BranchNo=sd.BranchNo and s.SaleNo=sd.SaleNo
	left join DBSPS.dbo.Branch br on s.BranchNo=br.BranchNo and s.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID		
where s.SaleStatus='Paid' and s.SaleDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,apfp.partNo,sd.PartName,apfp.APForce,sd.BrandID,sd.CompCode

/*--การเปิดจ๊อบซ่อมรถทั่วไป ......SVJob..*/
select 
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SV' as saleType
into #TempQuerySV
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SVPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SVPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo 
	left join DBSPS.dbo.SVJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Closed' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

/*การเปิดจ๊อบซ่อมรถมือสอง ....SBJob*/
select 
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SB' as saleType
into #TempQuerySB
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SBPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SBPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo 
	left join DBSPS.dbo.SBJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Closed' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

IF OBJECT_ID('tempdb..#TempSumAllSold') IS NOT NULL  DROP TABLE #TempSumAllSold; 
select a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce,isnull(sum(a.SumSoldQty),0) as SumSoldQty
into #TempSumAllSold from (select * from #TempQuerySA union select * from #TempQuerySV union select * from #TempQuerySB ) as a
group by a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce

select * from #TempSumAllSold;

