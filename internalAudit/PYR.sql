use serviceweb;
declare @currentDate datetime,@month int,@year int,@locat varchar(max),@day int;
set @currentDate='2016/01/01'; --&HP-14010001
set @month=MONTH(@currentDate);
set @year=YEAR(@currentDate);
set @day=DAY(@currentDate);
--print dateadd(day,+1,DATEADD(month,-1,@currentDate))

declare @pendingPeriod int;
set @pendingPeriod=2;
IF OBJECT_ID('tempdb..#tempTb') IS NOT NULL begin DROP TABLE #tempTb end

select allUnion.* 
into #tempTb
from (
select ar.CONTNO,ar.LOCAT,ar.SDATE,T_NOPAY,ar.T_LUPAY,pd.paidOfLP,pd.pendingPeriodLP,pd.periodOfSpecifiedDate,pd.paidInNextPeriod,pd.dealDate
,bahtPending,bahtPaid
from wb_armast ar 
cross apply dbo.PendingPeriodOnSpecifiedDate(ar.CONTNO,@currentDate,ar.SDATE,@pendingPeriod) pd
where ar.SDATE <= dateadd(month,-1,@currentDate) and pd.pendingPeriodLP=@pendingPeriod --and pd.paidOfLP<>pd.paidInNextPeriod
union
select ar.CONTNO,ar.LOCAT,ar.SDATE,T_NOPAY,ar.T_LUPAY,pd.paidOfLP,pd.pendingPeriodLP,pd.periodOfSpecifiedDate,pd.paidInNextPeriod,pd.dealDate
,bahtPending,bahtPaid
from HIINCOME.dbo.HARMAST ar 
cross apply dbo.PendingPeriodOnSpecifiedDateH(ar.CONTNO,@currentDate,ar.SDATE,@pendingPeriod,ar.YDATE) pd
where ar.SDATE <= dateadd(month,-1,@currentDate) and pd.pendingPeriodLP=@pendingPeriod --and pd.paidOfLP<>pd.paidInNextPeriod

) as allUnion

insert into dc_paymentResultForTwoTwo (LOCAT,mainCnt,paidCnt,paidInNextPeriodCnt,sumBahtPending,sumBahtPaidInNextPeriod,crDate)
select main.LOCAT,main.cnt as mainCnt,paid.cnt as paidCnt,paidInNextPeriod.cnt as paidInNextPeriodCnt 
	   ,bahtPending.sumBaht as sumBahtPending,bahtPaid.sumBaht as sumBahtPaidInNextPeriod,@currentDate as crDate	   
from (
	select count(*) cnt,LOCAT
	from #tempTb
	group by LOCAT
) as main
left join (
	select count(*) cnt,LOCAT
	from #tempTb
	where paidOfLP='Y' 
	group by LOCAT
) as paid on main.LOCAT=paid.LOCAT
left join (
	select count(*) cnt,LOCAT
	from #tempTb
	where paidInNextPeriod='Y' 
	group by LOCAT
) as paidInNextPeriod on main.LOCAT=paidInNextPeriod.LOCAT
left join (
	select sum(bahtPending) sumBaht,LOCAT
	from #tempTb	
	group by LOCAT
) bahtPending on main.LOCAT=bahtPending.LOCAT
left join(
select sum(bahtPaid) sumBaht,LOCAT
	from #tempTb	
	group by LOCAT
) bahtPaid on main.LOCAT=bahtPaid.LOCAT
--order by main.LOCAT

/*

DECLARE @dtDate DATETIME,@walkDate datetime;
SET @dtDate = '2014/01/15';
set @walkDate = @dtDate;
WHILE datediff(m,@dtDate,@walkDate) < 12 and DATEDIFF(mm,@walkDate,getdate())>1
BEGIN
	set @walkDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@walkDate)+1,0))
    print @walkDate;
    print DATEDIFF(mm,@walkDate,getdate());    
    EXEC calculatePaymentResultTwoTwo @walkDate
    set @walkDate = dateadd(month,1,@walkDate);
END
*/

print DATEDIFF(mm,'2014/20/01',getdate());

print DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'2014/05/11')+1,0))