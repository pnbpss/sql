
--�ٵ����͹�¡�ѹ
IF OBJECT_ID('tempdb..#TempData') IS NOT NULL  DROP TABLE #TempData; 
select 
	br.zone,p.PROVDES,ap.AUMPDES,br.tCode,rawData.*
into #TempData
from (
	select * from
	(
		select sum(p.NETPAY) as sumPayment
		,convert(varchar,datepart(dw,p.TMBILDT))
		+'_'
		+convert(varchar,(DATEPART(WEEK, p.TMBILDT)-DATEPART(WEEK, DATEADD(MM, DATEDIFF(MM,0,p.TMBILDT), 0))+ 1))
		as [dayWeek]
		,LOCATPAY as LOCATRECV
		from wb_chqtran p
		where p.FLAG<>'C' and PAYFOR in ('006','007') 
		and month(p.TMBILDT)=7
		and year(p.TMBILDT)=2015
		group by LOCATPAY
				,convert(varchar,datepart(dw,p.TMBILDT))
				+'_'
				+convert(varchar,(DATEPART(WEEK, p.TMBILDT)-DATEPART(WEEK, DATEADD(MM, DATEDIFF(MM,0,p.TMBILDT), 0))+ 1))
		
	) as s
	pivot
	(
		sum(sumPayment)
		for [dayWeek] in (
			[1_1],[2_1],[3_1],[4_1],[5_1],[6_1],[7_1],
			[1_2],[2_2],[3_2],[4_2],[5_2],[6_2],[7_2],
			[1_3],[2_3],[3_3],[4_3],[5_3],[6_3],[7_3],
			[1_4],[2_4],[3_4],[4_4],[5_4],[6_4],[7_4],
			[1_5],[2_5],[3_5],[4_5],[5_5],[6_5],[7_5]
			)
	) as p
) rawData
		left join dc_branches br on rawData.LOCATRECV=br.branchId
		left join wb_ampr ap on br.districtCode=ap.AUMPCOD
		left join wb_provinces p on ap.PROVCOD=p.PROVCOD
		order by br.tCode,p.PROVCOD,ap.AUMPCOD,rawData.LOCATRECV;
		
select * from (
	select * from #TempData
	union	
	select null as zone,PROVDES+'(���)' as PROVDES, null as AUMPDES, null as tCode,null as LOCATRECV
	,sum([1_1])[1_1],	sum([2_1])[2_1],	sum([3_1])[3_1],	sum([4_1])[4_1],	sum([5_1])[5_1],	sum([6_1])[6_1],	sum([7_1])[7_1],	
	sum([1_2])[1_2],	sum([2_2])[2_2],	sum([3_2])[3_2],	sum([4_2])[4_2],	sum([5_2])[5_2],	sum([6_2])[6_2],	sum([7_2])[7_2],	
	sum([1_3])[1_3],	sum([2_3])[2_3],	sum([3_3])[3_3],	sum([4_3])[4_3],	sum([5_3])[5_3],	sum([6_3])[6_3],	sum([7_3])[7_3],	
	sum([1_4])[1_4],	sum([2_4])[2_4],	sum([3_4])[3_4],	sum([4_4])[4_4],	sum([5_4])[5_4],	sum([6_4])[6_4],	sum([7_4])[7_4],	
	sum([1_5])[1_5],	sum([2_5])[2_5],	sum([3_5])[3_5],	sum([4_5])[4_5],	sum([5_5])[5_5],	sum([6_5])[6_5],	sum([7_5])[7_5]
	from #TempData
	group by PROVDES
) as allUnion
order by PROVDES,AUMPDES



/*
---�ٵ�����¡��͹
IF OBJECT_ID('tempdb..#TempData') IS NOT NULL  DROP TABLE #TempData; 
select 
	br.zone,p.PROVDES,ap.AUMPDES,br.tCode,rawData.*
into #TempData
from (
	select * from
	(
		select sum(p.NETPAY) as sumPayment,'M'+RIGHT('0' + RTRIM(MONTH(p.CHQDT)), 2) [month],LOCATPAY as LOCATRECV
		from wb_chqtran p
		where p.PAYFOR in ('007','006') and p.FLAG<>'C'
		and year(p.TMBILDT) = 2014		
		group by RIGHT('0' + RTRIM(MONTH(p.CHQDT)), 2),LOCATPAY,
		
	) as s
	pivot
	(
		sum(sumPayment) 
		for [month] in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])
	) as p
) as rawData 
		left join dc_branches br on rawData.LOCATRECV=br.branchId
		left join wb_ampr ap on br.districtCode=ap.AUMPCOD
		left join wb_provinces p on ap.PROVCOD=p.PROVCOD
		order by br.tCode,p.PROVCOD,ap.AUMPCOD,rawData.LOCATRECV;
select * from (
	select * from #TempData
	union	
	select null as zone,PROVDES+'(���)' as PROVDES, null as AUMPDES, null as tCode,null as LOCATRECV
	,sum([M01]),sum([M02]),sum([M03]),sum([M04]),sum([M05]),sum([M06]),sum([M07]),sum([M08]),sum([M09]),sum([M10]),sum([M11]),sum([M12])
	from #TempData
	group by PROVDES
) as allUnion
order by PROVDES,AUMPDES
*/

/*

SELECT DATEADD(month, DATEDIFF(month, 0, getdate()), 0) AS StartOfMonth
SELECT datepart(dw,DATEADD(month, DATEDIFF(month, 0, getdate()), 0)) AS dayStartOfMonth

SELECT DATEADD(month, ((YEAR(getdate()) - 1900) * 12) + MONTH(getdate()), -1) EndOfMonth
SELECT datepart(dw,DATEADD(month, ((YEAR(getdate()) - 1900) * 12) + MONTH(getdate()), -1)) as dayEndOfMonth
*/