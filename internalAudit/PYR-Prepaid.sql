use ITREPORT;
set nocount on;
declare @selectedDate varchar(50),@partGroupId varchar(2),@soldDateFrom varchar(50),@soldDateTo varchar(50);
set @selectedDate = '2558/05/08'+' 23:59:99999_9999';set @partGroupId = '09';


IF OBJECT_ID('tempdb..#temporaryBranchzone') IS NOT NULL  DROP TABLE #temporaryBranchzone; 
select * into #temporaryBranchzone from wb_branchZones;
--�ʹʵ�ͤ �����
IF OBJECT_ID('tempdb..#tempPartQueryResult') IS NOT NULL  DROP TABLE #tempPartQueryResult; 
select 
pg.GroupID, pg.GroupName
,case when isnull(zone.PROVDES,'')='' then ' ������˹�' else zone.PROVDES
 end as PROVDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.AUMPDES
 end as AUMPDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.tambon
 end as tambon
,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo,pl.PartName
,apfp.APForce,maxTimeStamp.BrandID,maxTimeStamp.CompCode,maxTimeStamp.maxTimeStamp,stc.LastQTY
into #tempPartQueryResult
from DBSPS.dbo.Branch mainBr left join (
		select max(stc.[timeStamp]) maxTimeStamp ,stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo 
		from DBSPS.dbo.SPStockCard stc 
		where stc.[TimeStamp]<=@selectedDate and stc.BrandID='001' --and stc.LastQTY<>0
		group by stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo
	) as maxTimeStamp on mainBr.BranchNo=maxTimeStamp.BranchNo and mainBr.CompCode=maxTimeStamp.CompCode
left join wb_apForceParts apfp on apfp.partNo=maxTimeStamp.PartNo and apfp.compCode=maxTimeStamp.CompCode
left join DBSPS.dbo.SPStockCard stc on maxTimeStamp.BranchNo=stc.BranchNo and maxTimeStamp.BrandID=stc.BrandID and maxTimeStamp.CompCode=stc.CompCode 
	and maxTimeStamp.maxTimeStamp=stc.[TimeStamp] and maxTimeStamp.PartNo=stc.PartNo and stc.PartNo=apfp.partNo
left join DBSPS.dbo.SPPartList pl on apfp.partNo=pl.PartNo and pl.BrandID=maxTimeStamp.BrandID and apfp.groupId=pl.GroupId
left join DBSPS.dbo.Branch br on maxTimeStamp.BranchNo=br.BranchNo
left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
where apfp.partNo is not null
--�ʹʵ�ͤ ��
--select * from #tempPartQueryResult

--�ʹ��� �����
set @soldDateFrom = '2557/05/08'+' 00:00:00000_0000';
set @soldDateTo = '2558/05/08'+' 23:59:99999_9999';

IF OBJECT_ID('tempdb..#TempQuerySA') IS NOT NULL  DROP TABLE #TempQuerySA; 
IF OBJECT_ID('tempdb..#TempQuerySV') IS NOT NULL  DROP TABLE #TempQuerySV; 
IF OBJECT_ID('tempdb..#TempQuerySB') IS NOT NULL  DROP TABLE #TempQuerySB; 

--��â��˹����ҹ ...SPSale ��� SPSaleDetai
select pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo as PartNo,sd.PartName,sd.BrandID,sd.CompCode,apfp.APForce,isnull(sum(sd.SaleQTY),0) as sumSoldQTY,'SA' as saleType
into #TempQuerySA
from 
	ITREPORT.dbo.wb_apForceParts apfp 
	left join DBSPS.dbo.SPSaleDetail sd on apfp.partNo=sd.PartNo and apfp.compCode=sd.CompCode
	left join dbsps.dbo.SPSale s on s.CompCode=sd.CompCode and s.BranchNo=sd.BranchNo and s.SaleNo=sd.SaleNo
	left join DBSPS.dbo.Branch br on s.BranchNo=br.BranchNo and s.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID		
where s.SaleStatus='Paid' and s.SaleDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,apfp.partNo,sd.PartName,apfp.APForce,sd.BrandID,sd.CompCode

/*--����Դ��ͺ����ö����� ......SVJob..*/
select 
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SV' as saleType
into #TempQuerySV
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SVPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SVPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo 
	left join DBSPS.dbo.SVJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Closed' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

/*����Դ��ͺ����ö����ͧ ....SBJob*/
select 
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SB' as saleType
into #TempQuerySB
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SBPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SBPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo 
	left join DBSPS.dbo.SBJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Closed' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

IF OBJECT_ID('tempdb..#TempSumAllSold') IS NOT NULL  DROP TABLE #TempSumAllSold; 
select a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce,isnull(sum(a.SumSoldQty),0) as SumSoldQty
into #TempSumAllSold from (select * from #TempQuerySA union select * from #TempQuerySV union select * from #TempQuerySB ) as a
group by a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce
--�ʹ��� ��


--select st.* from #tempPartQueryResult st
IF OBJECT_ID('tempdb..#tempPartQueryResult2') IS NOT NULL  DROP TABLE #tempPartQueryResult2; 
select st.*,isnull(sa.SumSoldQty,0) as SumSoldQTY
into #tempPartQueryResult2
from #tempPartQueryResult st 
   left join #TempSumAllSold sa
   on 
   st.GroupId=sa.GroupId
   and st.partNo=sa.PartNo
   and st.BranchName=sa.BranchName
   and st.APForce=sa.APForce
   and st.CompCode=sa.CompCode 
--where sa.SumSoldQty is not null
--order by st.GroupId,st.GroupName,st.PROVDES,st.BranchName
--goto TheEndOfTheScript

DECLARE @colsAp AS NVARCHAR(MAX),@colsComp AS NVARCHAR(MAX),@colsApForOrder AS NVARCHAR(MAX)
		,@colsCompForOrder AS NVARCHAR(MAX),@sumColsAp AS NVARCHAR(MAX)
	    ,@sumColsComp AS NVARCHAR(MAX),@colsApPivot as  NVARCHAR(MAX)
	    ,@colsCompPivot as  NVARCHAR(MAX),@sumPartZoneAp AS NVARCHAR(MAX)
	    ,@sumPartZoneComp AS NVARCHAR(MAX),@query AS NVARCHAR(MAX)
	    ,@colsApForNotExistBr AS NVARCHAR(MAX),@colsCompForNotExistBr AS NVARCHAR(MAX)
	    
	    ;
select @colsAp = STUFF((SELECT distinct ',isnull([' + partNo+'],0)'+' as [' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
select @sumColsAp = ' , (0+'+STUFF((SELECT distinct '+isnull([' + partNo+'],0)'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)') 
				    ,1,1,'')
					+') as sumColsAp';
select @sumColsComp = ' , (0+'+STUFF((SELECT distinct '+isnull([' + partNo+'],0)'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)') 
				    ,1,1,'')
					+') as sumColsComp';
select @sumPartZoneAp = STUFF((SELECT distinct ',sum([' + partNo+'])'+' as [' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)') 
					,1,1,'');
select @sumPartZoneComp = STUFF((SELECT distinct ',sum([' + partNo+'])'+' as [' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)') 
					,1,1,'');
select @colsApPivot = STUFF((SELECT distinct ',[' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');

set @colsComp = STUFF((SELECT distinct ',isnull([' + partNo+'],0)'+' as [' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
set @colsCompPivot = STUFF((SELECT distinct ',[' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @colsApForOrder = STUFF((SELECT distinct ',[' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @colsCompForOrder = STUFF((SELECT distinct ',[' + partNo+'] '
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');

set @colsApForNotExistBr = STUFF((SELECT distinct ',0 as [' + partNo+']'
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='Y'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @colsCompForNotExistBr = STUFF((SELECT distinct ',0 as [' + partNo+'] '
                    from #tempPartQueryResult2 where GroupID=@partGroupId and APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');


set @query = 'SELECT PROVDES,AUMPDES,tambon,branchName, '+ @colsAp + ', ''|'' as sp,'+ @colsComp+ @sumColsAp+@sumColsComp+ '			 
			  --into #pivotResultReport02
			 from 
             (
                select PROVDES,AUMPDES,tambon,branchName ,partNo ,LastQTY ,SumSoldQTY
                from #tempPartQueryResult2 where groupId='''+@partGroupId+'''
             ) x
             pivot 
             (
                sum(LastQTY) for partNo in (' + @colsApPivot+ ',sp,'+ @colsCompPivot + ')
             ) p
             pivot 
             (
                sum(SumSoldQTY) for partNo in (' + @colsApPivot+ ',sp,'+ @colsCompPivot + ')          
             ) p2
             
             goto endOfScriptSub;
             
             IF OBJECT_ID(''tempdb..##finalResultsForPrepaidSalePlaning'') IS NOT NULL  DROP TABLE ##finalResultsForPrepaidSalePlaning;
             
             select rank() OVER (order by PROVDES ,AUMPDES,tambon,branchName,'+@colsApForOrder+','+@colsCompForOrder+') as [rank],
             * 
             into ##finalResultsForPrepaidSalePlaning
             from (
             
             select 
             case when isnull(zone.PROVDES,'''')='''' then '' ������˹�'' else zone.PROVDES
			 end as PROVDES
			,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.AUMPDES
			 end as AUMPDES
			,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.tambon
			 end as tambon
             ,''(''+br.BranchNo+'')''+br.BranchName as BranchName, ' + @colsApForNotExistBr + ',0 as sumColsAp' + ',sp,'+ @colsCompForNotExistBr + ',0 as sumColsComp' + '
             ,sumColsAp+sumColsComp as sumAll
             from DBSPS.dbo.Branch br left join #pivotResultReport02 t on ''(''+br.BranchNo+'')''+br.BranchName = t.branchName
			 left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
			 where t.branchName is null and br.CompCode=''001''             
             union
             
             select PROVDES,AUMPDES,tambon,branchName, ' + @colsAp+ @sumColsAp + ',sp,'+ @colsComp+@sumColsComp+ '
             ,sumColsAp+sumColsComp as sumAll
             from #pivotResultReport02              
             union             
             select PROVDES,AUMPDES,''����''+AUMPDES collate Thai_CS_AS as tambon,null  as branchName
             ,'+@sumPartZoneAp+'
             ,sum(sumColsAp) as sumColsAp
             ,''|'' as sp
             ,'+@sumPartZoneComp+'             
             ,sum(sumColsComp) as sumColsComp
             ,sum(sumColsAp)+sum(sumColsComp) as sumAll
             from #pivotResultReport02
             group by PROVDES,AUMPDES
             union             
             select PROVDES,''���� ''+PROVDES as AUMPDES,null as tambon,null as branchName
             ,'+@sumPartZoneAp+'
             ,sum(sumColsAp) as sumColsAp
             ,''|'' as sp
             ,'+@sumPartZoneComp+'             
             ,sum(sumColsComp) as sumColsComp
             ,sum(sumColsAp)+sum(sumColsComp) as sumAll
             from #pivotResultReport02
             group by PROVDES
             union             
             select ''����������'' as PROVDES,null as AUMPDES,null as tambon,null as branchName
             ,'+@sumPartZoneAp+'
             ,sum(sumColsAp) as sumColsAp
             ,''|'' as sp
             ,'+@sumPartZoneComp+'             
             ,sum(sumColsComp) as sumColsComp
             ,sum(sumColsAp)+sum(sumColsComp) as sumAll
             from #pivotResultReport02             
             ) as aa
             endOfScriptSub:
             ';
execute(@query);
select * from ##finalResultsForPrepaidSalePlaning order by [rank];

TheEndOfTheScript: