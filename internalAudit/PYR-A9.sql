/*dbo.RPT_arpayForA9Report
PYR-A9.sql
*/

declare @year int;
set @year = 2015;
select main.*
,mpd.maxPendingMinusOne maxPDM1
,mpd.maxPending maxPD
,mpd.cntSold summAllMonth 

from 
(
	select * from 
	(
		select  
			  right('00' + cast(month(startContract) as varchar(2)), 2)+'_M' mStart
			  ,netPendingPeriod
			  ,count(*)+(1.00)-(1.00) cntSold
		FROM [tempForTest] ar left join HIC4REPORT.dbo.INVTRAN i on ar.CONTNO=i.CONTNO
		where year(startContract)=@year		  
			  and i.STAT='N' -- ö����
			  --and i.GCODE in ('05','051','052') --ö�ṹ��
			  --and i.STAT='O' and i.GCODE not in ('05','051','052') --ö����ͧ
		group by month(startContract),netPendingPeriod

	) t
	pivot
	(
		sum (cntSold) 
		for netPendingPeriod in (
			 [0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]
			--,[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24]
			--,[25],[26],[27],[28],[29],[30],[31],[32],[33],[34],[35],[36]
			--,[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48]
		)	
	)as pvt

	union 
	-- percent

	select * from 
	(
		select  
			  right('00' + cast(month(ar.startContract) as varchar(2)), 2)+'_P' mStart
			  ,ar.netPendingPeriod
			  ,
			  ((count(*)+0.00) 
			  /
				(				
					select count(*) 
					FROM [tempForTest] arh left join HIC4REPORT.dbo.INVTRAN i on arh.CONTNO=i.CONTNO	
					where year(startContract)=@year 
						and month(ar.startContract)=month(arh.startContract)						
						and i.STAT='N' -- ö����
						--and i.GCODE in ('05','051','052') --ö�ṹ��
						--and i.STAT='O' and i.GCODE not in ('05','051','052')				
				))*100
				cntSold
		FROM [tempForTest] ar left join HIC4REPORT.dbo.INVTRAN i on ar.CONTNO=i.CONTNO	
		where year(startContract)=@year 		  		  
			  and i.STAT='N' -- ö����
			  --and i.GCODE in ('05','051','052') --ö�ṹ��
			  --and i.STAT='O' and i.GCODE not in ('05','051','052')
		group by month(startContract),netPendingPeriod
	) t
	pivot
	(
		sum (cntSold) 
		for netPendingPeriod in (
			 [0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]
			--,[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24]
			--,[25],[26],[27],[28],[29],[30],[31],[32],[33],[34],[35],[36]
			--,[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48]
		)	
	) as pvt
) main left join
(
select  
		  right('00' + cast(month(startContract) as varchar(2)), 2)+'_M' mStart
		  ,max(netPendingPeriod) maxPending
		  ,max(netPendingPeriod)-1 maxPendingMinusOne
		  ,count(*)+(1.00)-(1.00) cntSold
	FROM [tempForTest] ar left join HIC4REPORT.dbo.INVTRAN i on ar.CONTNO=i.CONTNO
	where year(startContract)=@year
		  and i.STAT='N' -- ö����
		  --and i.GCODE in ('05','051','052') --ö�ṹ��
		  --and i.STAT='O' and i.GCODE not in ('05','051','052')
	group by month(startContract)
	--order by right('00' + cast(month(startContract) as varchar(2)), 2)+'_M'
) mpd on main.mStart=mpd.mStart
order by main.mStart