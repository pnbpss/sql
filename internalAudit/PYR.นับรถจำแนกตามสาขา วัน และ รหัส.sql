/*
--create table #tempForCountStrCode(
--create table serviceweb.dbo.wb_strStockHistory(
	code varchar(max)
	,dateOfStock datetime
	,branch varchar(max)
	,cntStrno int
	,needRerun char(1) 
);
*/
declare @cntStr int;
declare @branch varchar(max),@locat varchar(max);
declare @dateOfCount datetime;
declare @dataExists int; -- ������
declare @needRerun char(1);
declare @countExisted int;
set @needRerun = 'N';

select convert(varchar(max),getdate(),113);

set @dateOfCount = '2014/01/23';

if @dateOfCount>=convert(varchar,getdate(),111) goto endScript

declare branchCursor cursor for
	select LOCATCD from serviceweb.dbo.wb_invlocat 
	where LOCATCD not in ('MT��','OFF��','OFF��','SI���','OFF��','OFF¹');
	--where LOCATCD in ('YK�ù','YR��1','YR��2')
	--where LOCATCD in ('YK�ù')
	--group by LOCATCD;
open branchCursor;
fetch next from branchCursor into @locat;
While (@@FETCH_STATUS <> -1)
begin	
	
	next1:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='14' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next2;	
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '14',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('14',@dateOfCount,@locat,@cntStr,@needRerun);	
	
	next2:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='28' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next3;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '28',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('28',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next3:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='15' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next4;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '15',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('15',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next4:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='16' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next5;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '16',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('16',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next5:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='29' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next6;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '29',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('29',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next6:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='02' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next7;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '02',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('02',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next7:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='022' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next8;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '022',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('022',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next8:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='024' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next9;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '024',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('024',@dateOfCount,@locat,@cntStr,@needRerun);	
	
	next9:
	select @countExisted = count(code) from serviceweb.dbo.wb_strStockHistory where code='30' and dateOfStock=@dateOfCount and branch=@locat;
	if @countExisted > 0 goto next10;
	exec @cntStr = hiincome.dbo.countStrnoByDateCodeBranch '30',@dateOfCount,@locat;
	insert into serviceweb.dbo.wb_strStockHistory values('30',@dateOfCount,@locat,@cntStr,@needRerun);
	
	next10:

	fetch next from branchCursor into @locat;
end
deallocate branchCursor;

--select * from serviceweb.dbo.wb_strStockHistory where dateOfStock=@dateOfCount ;
--�Ѻ��ṡö������ʵ���ѹ��� query
select main.branch, code14.sumStrno as sumStrno14, code28.sumStrno as sumStrno28, code15.sumStrno as sumStrno15
	   ,code16.sumStrno as sumStrno16, code29.sumStrno as sumStrno29, code02.sumStrno as sumStrno02
	   , code022.sumStrno as sumStrno022, code024.sumStrno as sumStrno024, code30.sumStrno as sumStrno30
from 
 (select distinct branch from serviceweb.dbo.wb_strStockHistory) as main
 left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='14') as code14 on main.branch=code14.branch
 left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='28') as code28 on main.branch=code28.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='15') as code15 on main.branch=code15.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='16') as code16 on main.branch=code16.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='29') as code29 on main.branch=code29.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='02') as code02 on main.branch=code02.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='022') as code022 on main.branch=code022.branch
   left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='024') as code024 on main.branch=code024.branch
  left join 
 (select cntStrno as sumStrno, branch from serviceweb.dbo.wb_strStockHistory 
  where dateOfStock=@dateOfCount and code='30') as code30 on main.branch=code30.branch
order by main.branch

select convert(varchar(max),getdate(),113);

endScript: -- ���͹حҵ���Ѻ�ѹ�Ѩ�غѹ�����ҡ����

--select * from #tempForCountStrCode;
--drop table #tempForCountStrCode;

--endCount:
--select * from serviceweb.dbo.wb_strStockHistory where dateOfStock=@dateOfCount;
--print getdate();

--print getdate();
--14 28 15 16 29 30 02x

--delete from serviceweb.dbo.wb_strStockHistory;
--drop table serviceweb.dbo.wb_strStockHistory;