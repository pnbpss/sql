use ITREPORT;
set nocount on;
declare @selectedDate varchar(50),@partGroupId varchar(2),@soldDateFrom varchar(50),@soldDateTo varchar(50);
set @selectedDate = '2558/05/08'+' 23:59:99999_9999';set @partGroupId = '09';


IF OBJECT_ID('tempdb..#temporaryBranchzone') IS NOT NULL  DROP TABLE #temporaryBranchzone; 
select * into #temporaryBranchzone from wb_branchZones;

IF OBJECT_ID('tempdb..#tempPartQueryResult') IS NOT NULL  DROP TABLE #tempPartQueryResult; 
select 
pg.GroupID, pg.GroupName
,case when isnull(zone.PROVDES,'')='' then ' ������˹�' else zone.PROVDES
 end as PROVDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.AUMPDES
 end as AUMPDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.tambon
 end as tambon
,br.BranchNo
,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo,pl.PartName
,apfp.APForce
,maxTimeStamp.BrandID,maxTimeStamp.CompCode,maxTimeStamp.maxTimeStamp,stc.LastQTY
into #tempPartQueryResult
from DBSPS.dbo.Branch mainBr left join (
		select max(stc.[timeStamp]) maxTimeStamp ,stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo 
		from DBSPS.dbo.SPStockCard stc 
		where stc.[TimeStamp]<=@selectedDate and stc.BrandID='001' --and stc.LastQTY<>0
		group by stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo
	) as maxTimeStamp on mainBr.BranchNo=maxTimeStamp.BranchNo and mainBr.CompCode=maxTimeStamp.CompCode
left join wb_apForceParts apfp on apfp.partNo=maxTimeStamp.PartNo and apfp.compCode=maxTimeStamp.CompCode
left join DBSPS.dbo.SPStockCard stc 
	on maxTimeStamp.BranchNo=stc.BranchNo 
	and maxTimeStamp.BrandID=stc.BrandID
	and maxTimeStamp.CompCode=stc.CompCode 
	and maxTimeStamp.maxTimeStamp=stc.[TimeStamp] 
	and maxTimeStamp.PartNo=stc.PartNo	
	and stc.PartNo=apfp.partNo
left join DBSPS.dbo.SPPartList pl on apfp.partNo=pl.PartNo and pl.BrandID=maxTimeStamp.BrandID and apfp.groupId=pl.GroupId
left join DBSPS.dbo.Branch br on maxTimeStamp.BranchNo=br.BranchNo
left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
where apfp.partNo is not null



IF OBJECT_ID('tempdb..#tempPartQueryResult2') IS NOT NULL  DROP TABLE #tempPartQueryResult2;
select *
, case when APForce='Y' then ''+GroupId+'_AP'
  else ''+GroupId+'_CO'
  end as myGroup  
  into #tempPartQueryResult2
from #tempPartQueryResult
select * from #tempPartQueryResult

-- order by branchName;
DECLARE @cols AS NVARCHAR(MAX)		
		,@query AS NVARCHAR(MAX)
		,@colsApPivot AS NVARCHAR(MAX)
		,@colsCompPivot AS NVARCHAR(MAX)
		,@sumAllAp AS NVARCHAR(MAX)
		,@sumAllComp AS NVARCHAR(MAX)
		,@sumAll AS NVARCHAR(MAX)
		,@colForOrder AS NVARCHAR(MAX)
		,@sumEachCol AS NVARCHAR(MAX)
		,@colsForNotExistBr AS NVARCHAR(MAX)		
		;
--select distinct myGroup from #tempPartQueryResult2;
    
select @cols = STUFF((SELECT ',isnull([' + aa.GroupId+'_AP],0)'+' as [' + aa.GroupId+'_AP]
								,isnull([' + aa.GroupId+'_CO],0)'+' as [' + aa.GroupId+'_CO]
								,isnull([' + aa.GroupId+'_AP],0)+isnull([' + aa.GroupId+'_CO],0) as [Sum_'+aa.GroupId+']
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @sumEachCol = STUFF((SELECT ',sum(isnull([' + aa.GroupId+'_AP],0))'+' as [' + aa.GroupId+'_AP]
								,sum(isnull([' + aa.GroupId+'_CO],0))'+' as [' + aa.GroupId+'_CO]
								,sum(isnull([' + aa.GroupId+'_AP],0)+isnull([' + aa.GroupId+'_CO],0)) as [Sum_'+aa.GroupId+']
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
               
set @sumAllAp = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_AP],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as sumAllAp';
set @sumAllComp = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_CO],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as sumAllComp';
set @sumAll = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_AP],0)+isnull([' + aa.GroupId+'_CO],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as sumAll';


set @colsCompPivot = STUFF((SELECT distinct ',[' + myGroup+']'
                    from #tempPartQueryResult2 where APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @colsApPivot = STUFF((SELECT distinct ',[' + myGroup+']'
                    from #tempPartQueryResult2 where APForce='Y'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');

select @colForOrder = STUFF((SELECT ',[' + aa.GroupId+'_AP],[' + aa.GroupId+'_CO],[Sum_'+aa.GroupId+'] '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
        
set @colsForNotExistBr = STUFF((SELECT ',0'+' as [' + aa.GroupId+'_AP]
								,0 as [' + aa.GroupId+'_CO]
								,0 as [Sum_'+aa.GroupId+']
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');


set @query = 'SELECT PROVDES,AUMPDES,tambon,branchName, ' 
			 + @cols +@sumAllAp+@sumAllComp+@sumAll+ '			 
			  into #pivotResultReport02
			 from 
             (
                select PROVDES,AUMPDES,tambon,branchName,myGroup, LastQTY
                from #tempPartQueryResult2                 
             ) x
             pivot 
             (
                sum(LastQTY)
                for myGroup in (' + @colsApPivot+ ','+ @colsCompPivot + ')                
             ) p
             
             IF OBJECT_ID(''tempdb..##finalResultsForPrepaidSalePlaning'') IS NOT NULL  DROP TABLE ##finalResultsForPrepaidSalePlaning;
             
             select rank() OVER (order by PROVDES ,AUMPDES,tambon,branchName,'+@colForOrder+',sumAllAp,sumAllComp,sumAll) as [rank],
             * 
             into ##finalResultsForPrepaidSalePlaning
             from 
             (
				 select 
				 case when isnull(zone.PROVDES,'''')='''' then '' ������˹�'' else zone.PROVDES
				 end as PROVDES
				,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.AUMPDES
				 end as AUMPDES
				,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.tambon
				 end as tambon
				 ,''(''+br.BranchNo+'')''+br.BranchName as BranchName, ' + @colsForNotExistBr + ',0 as sumAllAp,0 as sumAllComp, 0 as sumAll
				 
				 from DBSPS.dbo.Branch br left join #pivotResultReport02 t on ''(''+br.BranchNo+'')''+br.BranchName = t.branchName
				 left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
				 where t.branchName is null and br.CompCode=''001''             
				 union
             
				select PROVDES,AUMPDES,tambon,branchName, ' + @cols+ ',sumAllAp,sumAllComp,sumAll             
				from #pivotResultReport02 
				union
				select PROVDES,AUMPDES,''����''+AUMPDES collate Thai_CS_AS as tambon, null as branchName, ' + @sumEachCol+ ',sum(sumAllAp) as sumAllAp,sum(sumAllComp) as sumAllComp ,sum(sumAll) as sumAll
				from #pivotResultReport02 group by PROVDES,AUMPDES
				union
				select PROVDES,''����''+PROVDES collate Thai_CS_AS AUMPDES,null as tambon, null as branchName, ' + @sumEachCol+ ',sum(sumAllAp) as sumAllAp,sum(sumAllComp) as sumAllComp ,sum(sumAll) as sumAll
				from #pivotResultReport02 group by PROVDES
				union
				select ''����������'' collate Thai_CS_AS PROVDES,null AUMPDES,null as tambon, null as branchName, ' + @sumEachCol+ ',sum(sumAllAp) as sumAllAp,sum(sumAllComp) as sumAllComp ,sum(sumAll) as sumAll
				from #pivotResultReport02 
             ) as aa             
             ';
             --
--print @query;
execute(@query);
select * from ##finalResultsForPrepaidSalePlaning order by [rank];
