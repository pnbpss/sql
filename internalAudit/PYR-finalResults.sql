declare @startMonth int,@startYear varchar(max),@startDate varchar(max),@anchorDate varchar(max),@walkDate datetime,@focusOnPeriod int;
set @focusOnPeriod=4;
select @startDate = '15',@startMonth = '11',@startYear='2013';
select @anchorDate = @startYear+'/'+convert(varchar(max),@startMonth)+'/'+@startDate;
set @walkDate = @anchorDate;--print @walkDate;--print @startYear+'/'+convert(varchar(max),@startMonth)+'/'+@startDate--print DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@startYear+'/'+convert(varchar(max),@startMonth)+'/'+@startDate)+1,0))
--WHILE datediff(m,@anchorDate,@walkDate) < 12 and DATEDIFF(mm,@walkDate,getdate())>1
WHILE DATEDIFF(mm,@walkDate,getdate())>1
BEGIN
	set @walkDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@walkDate)+1,0))    --print @walkDate;    --print DATEDIFF(mm,@walkDate,getdate());    
    if (select count(*) from dc_paymentResultForTwoTwo where month(@walkDate)=month(crDate) and year(@walkDate)=year(crDate) and focusOnPeriod=@focusOnPeriod)=0
    begin
		EXEC calculatePaymentResultTwoTwo @walkDate,@focusOnPeriod
	end
    set @walkDate = dateadd(month,1,@walkDate);
END

select i.LOCATCD
,castb.nick
,amp.AUMPDES
,m1.focusOnPeriod,m1.mainCnt m1mainCnt, m1.paidCnt m1paidCnt,m1.paidInNextPeriodCnt m1paidInNextPeriodCnt,m1.sumBahtPending m1sumBahtPending,m1.sumBahtPaidInNextPeriod m1sumBahtPaidInNextPeriod
,m2.mainCnt m2mainCnt, m2.paidCnt m2paidCnt,m2.paidInNextPeriodCnt m2paidInNextPeriodCnt,m2.sumBahtPending m2sumBahtPending,m2.sumBahtPaidInNextPeriod m2sumBahtPaidInNextPeriod
,m3.mainCnt m3mainCnt, m3.paidCnt m3paidCnt,m3.paidInNextPeriodCnt m3paidInNextPeriodCnt,m3.sumBahtPending m3sumBahtPending,m3.sumBahtPaidInNextPeriod m3sumBahtPaidInNextPeriod
,m4.mainCnt m4mainCnt, m4.paidCnt m4paidCnt,m4.paidInNextPeriodCnt m4paidInNextPeriodCnt,m4.sumBahtPending m4sumBahtPending,m4.sumBahtPaidInNextPeriod m4sumBahtPaidInNextPeriod
,m5.mainCnt m5mainCnt, m5.paidCnt m5paidCnt,m5.paidInNextPeriodCnt m5paidInNextPeriodCnt,m5.sumBahtPending m5sumBahtPending,m5.sumBahtPaidInNextPeriod m5sumBahtPaidInNextPeriod
,m6.mainCnt m6mainCnt, m6.paidCnt m6paidCnt,m6.paidInNextPeriodCnt m6paidInNextPeriodCnt,m6.sumBahtPending m6sumBahtPending,m6.sumBahtPaidInNextPeriod m6sumBahtPaidInNextPeriod
,m7.mainCnt m7mainCnt, m7.paidCnt m7paidCnt,m7.paidInNextPeriodCnt m7paidInNextPeriodCnt,m7.sumBahtPending m7sumBahtPending,m7.sumBahtPaidInNextPeriod m7sumBahtPaidInNextPeriod
,m8.mainCnt m8mainCnt, m8.paidCnt m8paidCnt,m8.paidInNextPeriodCnt m8paidInNextPeriodCnt,m8.sumBahtPending m8sumBahtPending,m8.sumBahtPaidInNextPeriod m8sumBahtPaidInNextPeriod
,m9.mainCnt m9mainCnt, m9.paidCnt m9paidCnt,m9.paidInNextPeriodCnt m9paidInNextPeriodCnt,m9.sumBahtPending m9sumBahtPending,m9.sumBahtPaidInNextPeriod m9sumBahtPaidInNextPeriod
,m10.mainCnt m10mainCnt, m10.paidCnt m10paidCnt,m10.paidInNextPeriodCnt m10paidInNextPeriodCnt,m10.sumBahtPending m10sumBahtPending,m10.sumBahtPaidInNextPeriod m10sumBahtPaidInNextPeriod
,m11.mainCnt m11mainCnt, m11.paidCnt m11paidCnt,m11.paidInNextPeriodCnt m11paidInNextPeriodCnt,m11.sumBahtPending m11sumBahtPending,m11.sumBahtPaidInNextPeriod m11sumBahtPaidInNextPeriod
,m12.mainCnt m12mainCnt, m12.paidCnt m12paidCnt,m12.paidInNextPeriodCnt m12paidInNextPeriodCnt,m12.sumBahtPending m12sumBahtPending,m12.sumBahtPaidInNextPeriod m12sumBahtPaidInNextPeriod
from wb_invlocat i
left join (
	select * from dc_paymentResultForTwoTwo where crDate= 
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+1,0)) and focusOnPeriod=@focusOnPeriod	
) as m1 on i.LOCATCD=m1.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+2,0)) and focusOnPeriod=@focusOnPeriod	
) as m2 on i.LOCATCD=m2.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+3,0)) and focusOnPeriod=@focusOnPeriod	
) as m3 on i.LOCATCD=m3.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+4,0)) and focusOnPeriod=@focusOnPeriod	
) as m4 on i.LOCATCD=m4.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+5,0)) and focusOnPeriod=@focusOnPeriod	
) as m5 on i.LOCATCD=m5.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+6,0)) and focusOnPeriod=@focusOnPeriod
	) as m6 on i.LOCATCD=m6.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+7,0)) and focusOnPeriod=@focusOnPeriod	
) as m7 on i.LOCATCD=m7.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+8,0)) and focusOnPeriod=@focusOnPeriod	
) as m8 on i.LOCATCD=m8.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+9,0)) and focusOnPeriod=@focusOnPeriod	
) as m9 on i.LOCATCD=m9.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+10,0)) and focusOnPeriod=@focusOnPeriod	
) as m10 on i.LOCATCD=m10.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+11,0)) and focusOnPeriod=@focusOnPeriod
) as m11 on i.LOCATCD=m11.LOCAT
left join (
	select * from dc_paymentResultForTwoTwo where crDate=
	DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@anchorDate)+12,0)) and focusOnPeriod=@focusOnPeriod
) as m12 on i.LOCATCD=m12.LOCAT
left join dc_currentAssignedSupervisorToBranch castb on i.LOCATCD=castb.branchId
left join dc_branches b on i.LOCATCD=b.branchId
left join wb_ampr amp on b.districtCode=amp.AUMPCOD
where b.locatTypeId='1' 
;


--select * from dc_paymentResultForTwoTwo;
--delete from dc_paymentResultForTwoTwo
--truncate table dc_paymentResultForTwoTwo
--select * from dc_branches;