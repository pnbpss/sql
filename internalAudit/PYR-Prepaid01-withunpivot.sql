use ITREPORT;
set nocount on;
declare @selectedDate varchar(50),@partGroupId varchar(2),@soldDateFrom varchar(50),@soldDateTo varchar(50);
set @selectedDate = '2558/05/11'+' 23:59:99999_9999';set @partGroupId = '08';

IF OBJECT_ID('tempdb..#temporaryBranchzone') IS NOT NULL  DROP TABLE #temporaryBranchzone; 
select * into #temporaryBranchzone from wb_branchZones;
--�ʹʵ�ͤ �����
IF OBJECT_ID('tempdb..#tempPartQueryResult') IS NOT NULL  DROP TABLE #tempPartQueryResult; 
select 
pg.GroupID, pg.GroupName
,case when isnull(zone.PROVDES,'')='' then ' ������˹�' else zone.PROVDES
 end as PROVDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.AUMPDES
 end as AUMPDES
,case when isnull(zone.PROVDES,'')=''  then ' ������˹�' else zone.tambon
 end as tambon
,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo,pl.PartName
,apfp.APForce,maxTimeStamp.BrandID,maxTimeStamp.CompCode,maxTimeStamp.maxTimeStamp,stc.LastQTY
into #tempPartQueryResult
from DBSPS.dbo.Branch mainBr left join (
		select max(stc.[timeStamp]) maxTimeStamp ,stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo 
		from DBSPS.dbo.SPStockCard stc 
		where stc.[TimeStamp]<=@selectedDate and stc.BrandID='001' --and stc.LastQTY<>0
		group by stc.CompCode,stc.BranchNo,stc.BrandID,stc.PartNo
	) as maxTimeStamp on mainBr.BranchNo=maxTimeStamp.BranchNo and mainBr.CompCode=maxTimeStamp.CompCode
left join wb_apForceParts apfp on apfp.partNo=maxTimeStamp.PartNo and apfp.compCode=maxTimeStamp.CompCode
left join DBSPS.dbo.SPStockCard stc on maxTimeStamp.BranchNo=stc.BranchNo and maxTimeStamp.BrandID=stc.BrandID and maxTimeStamp.CompCode=stc.CompCode 
	and maxTimeStamp.maxTimeStamp=stc.[TimeStamp] and maxTimeStamp.PartNo=stc.PartNo and stc.PartNo=apfp.partNo
left join DBSPS.dbo.SPPartList pl on apfp.partNo=pl.PartNo and pl.BrandID=maxTimeStamp.BrandID and apfp.groupId=pl.GroupId
left join DBSPS.dbo.Branch br on maxTimeStamp.BranchNo=br.BranchNo
left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
where apfp.partNo is not null
--�ʹʵ�ͤ ��
--select * from #tempPartQueryResult

--�ʹ��� �����
set @soldDateFrom = '2558/01/01'+' 00:00:00000_0000';
set @soldDateTo = '2558/04/30'+' 23:59:99999_9999';

IF OBJECT_ID('tempdb..#TempQuerySA') IS NOT NULL  DROP TABLE #TempQuerySA; 
IF OBJECT_ID('tempdb..#TempQuerySV') IS NOT NULL  DROP TABLE #TempQuerySV; 
IF OBJECT_ID('tempdb..#TempQuerySB') IS NOT NULL  DROP TABLE #TempQuerySB; 

--��â��˹����ҹ ...SPSale ��� SPSaleDetai
select pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as branchName,apfp.partNo as PartNo,sd.PartName,sd.BrandID,sd.CompCode,apfp.APForce,isnull(sum(sd.SaleQTY),0) as sumSoldQTY,'SA' as saleType
into #TempQuerySA
from 
	ITREPORT.dbo.wb_apForceParts apfp 
	left join DBSPS.dbo.SPSaleDetail sd on apfp.partNo=sd.PartNo and apfp.compCode=sd.CompCode
	left join dbsps.dbo.SPSale s on s.CompCode=sd.CompCode and s.BranchNo=sd.BranchNo and s.SaleNo=sd.SaleNo
	left join DBSPS.dbo.Branch br on s.BranchNo=br.BranchNo and s.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID		
where s.SaleStatus='Paid' and s.SaleDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,apfp.partNo,sd.PartName,apfp.APForce,sd.BrandID,sd.CompCode

/*--����Դ��ͺ����ö����� ......SVJob..*/
select 
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SV' as saleType
into #TempQuerySV
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SVPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SVPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo and jpd.SVIssueNo=jp.SVIssueNo
	left join DBSPS.dbo.SVJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Paid' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

/*����Դ��ͺ����ö����ͧ ....SBJob*/
select
pg.GroupID,pg.GroupName,br.BranchNo,'('+br.BranchNo+')'+br.BranchName as BranchName, jpd.PartNo,jpd.PartName,jpd.BrandID,jpd.CompCode,apfp.APForce,isnull(SUM(jpd.IssueQTY),0) as sumSoldQTY,'SB' as saleType
into #TempQuerySB
from 
	wb_apForceParts apfp 
	left join DBSPS.dbo.SBPartIssueDetail jpd on apfp.partNo=jpd.PartNo and apfp.compCode=jpd.CompCode
	left join DBSPS.dbo.SBPartIssue jp on jp.SVJobNo=jpd.SVJobNo and jp.CompCode=jpd.CompCode and jp.BranchNo=jpd.BranchNo and jpd.SVIssueNo=jp.SVIssueNo
	left join DBSPS.dbo.SBJob j on j.SVJobNo=jp.SVJobNo and j.BranchNo=jp.BranchNo and j.CompCode=jp.CompCode 
	left join DBSPS.dbo.Branch br on j.BranchNo=br.BranchNo and j.CompCode=br.CompCode	
	left join DBSPS.dbo.SPPartGroup pg on apfp.groupId=pg.GroupID
where j.[Status]='Closed' and jp.[Status]='Completed' and jp.SVIssueDate between @soldDateFrom and @soldDateTo
group by pg.GroupID,pg.GroupName,br.BranchNo,br.BranchName,jpd.partNo,jpd.PartName,apfp.APForce,jpd.BrandID,jpd.CompCode

IF OBJECT_ID('tempdb..#TempSumAllSold') IS NOT NULL  DROP TABLE #TempSumAllSold; 
select a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce,convert(int,isnull(sum(a.SumSoldQty),0)) as SumSoldQty
into #TempSumAllSold 
from (
	select * from #TempQuerySA 
	union 
	select * from #TempQuerySV --where partNo='44711-KWW-601'
	union 
	select * from #TempQuerySB --where partNo='44711-KWW-601'
) as a
group by a.GroupId,a.GroupName,a.BranchName,a.PartNo,a.PartName,a.BrandId,a.CompCode,a.APForce
--�ʹ��� ��


--select st.* from #tempPartQueryResult st
IF OBJECT_ID('tempdb..#tempPartQueryResult2') IS NOT NULL  DROP TABLE #tempPartQueryResult2; 
select 
st.GroupId,st.GroupName,st.PROVDES,st.AUMPDES,st.tambon,st.BranchNo,st.branchName
,case when st.APForce='Y' then 'AP'
	else 'CO'
end as APForce
,convert(int,isnull(SUM(sa.sumSoldQTY),0)) as soldQTY, convert(int,isnull(SUM(lastQTY),0)) as stockQTY
into #tempPartQueryResult2
from #tempPartQueryResult st 
   left join #TempSumAllSold sa
   on  st.GroupId=sa.GroupId
   and st.partNo=sa.PartNo
   and st.BranchName=sa.BranchName
   and st.APForce=sa.APForce
   and st.CompCode=sa.CompCode 
group by st.GroupId,st.GroupName,st.PROVDES,st.AUMPDES,st.tambon,st.BranchNo,st.branchName,st.APForce
order by st.PROVDES,st.AUMPDES,st.tambon,st.BranchNo,st.branchName,st.GroupId,st.GroupName,st.APForce
--where sa.SumSoldQty is not null
--order by st.GroupId,st.GroupName,st.PROVDES,st.BranchName
--goto TheEndOfTheScript
--select * from #tempPartQueryResult2;


-- order by branchName;
DECLARE @cols AS NVARCHAR(MAX)		
		,@query AS NVARCHAR(MAX)
		,@colsPivot AS NVARCHAR(MAX)
		,@colsCompPivot AS NVARCHAR(MAX)
		,@SUM_ST_AP AS NVARCHAR(MAX)
		,@SUM_ST_CO AS NVARCHAR(MAX)
		,@SUM_SO_AP AS NVARCHAR(MAX)
		,@SUM_SO_CO AS NVARCHAR(MAX)
		
		,@colForOrder AS NVARCHAR(MAX)
		,@sumEachCol AS NVARCHAR(MAX)
		,@colsForNotExistBr AS NVARCHAR(MAX)		
		;
--select distinct myGroup from #tempPartQueryResult2;
    
select @cols = STUFF((SELECT ',isnull([' + aa.GroupId+'_ST_AP],0)'+' as [' + aa.GroupId+'_ST_AP]
							  ,isnull([' + aa.GroupId+'_ST_CO],0)'+' as [' + aa.GroupId+'_ST_CO]
							  ,isnull([' + aa.GroupId+'_SO_AP],0)'+' as [' + aa.GroupId+'_SO_AP]
							  ,isnull([' + aa.GroupId+'_SO_CO],0)'+' as [' + aa.GroupId+'_SO_CO]
							  
							  ,isnull([' + aa.GroupId+'_ST_AP],0)+isnull([' + aa.GroupId+'_ST_CO],0) as [Sum_'+aa.GroupId+'_ST]
							  ,isnull([' + aa.GroupId+'_SO_AP],0)+isnull([' + aa.GroupId+'_SO_CO],0) as [Sum_'+aa.GroupId+'_SO]							  
							
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    --where aa.GroupId='08'
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @sumEachCol = STUFF((SELECT ',sum(isnull([' + aa.GroupId+'_ST_AP],0))'+' as [' + aa.GroupId+'_ST_AP]
							  ,sum(isnull([' + aa.GroupId+'_ST_CO],0)'+') as [' + aa.GroupId+'_ST_CO]
							  ,sum(isnull([' + aa.GroupId+'_SO_AP],0)'+') as [' + aa.GroupId+'_SO_AP]
							  ,sum(isnull([' + aa.GroupId+'_SO_CO],0)'+') as [' + aa.GroupId+'_SO_CO]
							  
							  ,sum(isnull([' + aa.GroupId+'_ST_AP],0)+isnull([' + aa.GroupId+'_ST_CO],0)) as [Sum_'+aa.GroupId+'_ST]
							  ,sum(isnull([' + aa.GroupId+'_SO_AP],0)+isnull([' + aa.GroupId+'_SO_CO],0)) as [Sum_'+aa.GroupId+'_SO]
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
               
set @SUM_ST_AP = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_ST_AP],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as SUM_ST_AP';
set @SUM_ST_CO = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_ST_CO],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as SUM_ST_CO';
set @SUM_SO_AP = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_SO_AP],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as SUM_SO_AP';
set @SUM_SO_CO = ',(0+'+STUFF((SELECT '+isnull([' + aa.GroupId+'_SO_CO],0)'
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')+') as SUM_SO_CO';

set @colsCompPivot = STUFF((SELECT distinct ',[' + GroupID+']'
                    from #tempPartQueryResult2 where APForce='N'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
set @colsPivot = STUFF((SELECT distinct ',[' + GroupID+'_ST_AP],[' + GroupID+'_ST_CO],[' + GroupID+'_SO_AP],[' + GroupID+'_SO_CO]'
                    from #tempPartQueryResult2 --where GroupId='08'
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');

select @colForOrder = STUFF((SELECT ',[' + aa.GroupId+'_ST_AP]
							   ,[' + aa.GroupId+'_ST_CO]
							   ,[' + aa.GroupId+'_SO_AP]
							   ,[' + aa.GroupId+'_SO_CO]
							   
							   ,[Sum_'+aa.GroupId+'_ST]
							   ,[Sum_'+aa.GroupId+'_SO]
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');
        
set @colsForNotExistBr = STUFF((SELECT ',0 as [' + aa.GroupId+'_ST_AP]
							   ,0 as [' + aa.GroupId+'_ST_CO]
							   ,0 as [' + aa.GroupId+'_SO_AP]
							   ,0 as [' + aa.GroupId+'_SO_CO]
							   
							   ,0 as [Sum_'+aa.GroupId+'_ST]
							   ,0 as [Sum_'+aa.GroupId+'_SO]								
							   '
                    from (select distinct GroupId from #tempPartQueryResult2) as aa 
                    order by aa.GroupId
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'');


IF OBJECT_ID('tempdb..#pivotedPartInfos') IS NOT NULL  DROP TABLE #pivotedPartInfos; 
select PROVDES,AUMPDES,tambon,branchNo,branchName,groupIdAndAmoutType as groupType,qty
--,apfp.APForce,apfp.groupId
into #pivotedPartInfos
from
(     
    select PROVDES,AUMPDES,tambon,branchNo,branchName, [GroupID]+'_'+upper(left([amountType],2))+'_'+APForce as groupIdAndAmoutType,qty 
     from #tempPartQueryResult2
    unpivot
    (
      qty for amountType in (stockQTY, soldQTY)
    ) unp
) src
--select * from  #pivotedPartInfos;

--goto theEndOfScript
--print @cols; print @colsPivot;

set @query = 'SELECT PROVDES,AUMPDES,tambon,branchName, ' 
			 +@cols
			 +@SUM_ST_AP
			 +@SUM_ST_CO
			 +@SUM_SO_AP
			 +@SUM_SO_CO
			 --+@sumAllComp
			 --+@sumAll
			 +' 
			  into #pivotResultReport02
			 from 
             (
                select PROVDES,AUMPDES,tambon,branchName,groupType, qty
                from #pivotedPartInfos                 
             ) x
             pivot 
             (
                sum(qty)
                for groupType in (' +@colsPivot+ ')                
             ) p
             
             --goto endsubScript
             
             IF OBJECT_ID(''tempdb..##finalResultsForPrepaidSalePlaning'') IS NOT NULL  DROP TABLE ##finalResultsForPrepaidSalePlaning;
             
             select rank() OVER (order by PROVDES ,AUMPDES,tambon,branchName,'+@colForOrder+',SUM_ST_AP,SUM_ST_CO,SUM_SO_AP,SUM_SO_CO) as [rank],
             * 
             into ##finalResultsForPrepaidSalePlaning
             from 
             (
				 select 
				 case when isnull(zone.PROVDES,'''')='''' then '' ������˹�'' else zone.PROVDES
				 end as PROVDES
				,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.AUMPDES
				 end as AUMPDES
				,case when isnull(zone.PROVDES,'''')=''''  then '' ������˹�'' else zone.tambon
				 end as tambon
				 ,''(''+br.BranchNo+'')''+br.BranchName as BranchName, ' + @colsForNotExistBr + ',0 as SUM_ST_AP,0 as SUM_ST_CO,0 as SUM_SO_AP,0 as SUM_SO_CO
				 
				 from DBSPS.dbo.Branch br left join #pivotResultReport02 t on ''(''+br.BranchNo+'')''+br.BranchName = t.branchName
				 left join #temporaryBranchzone zone on br.BranchNo= zone.DBSPS collate Thai_CS_AS and zone.DBSPS is not null
				 where t.branchName is null and br.CompCode=''001''             
				 union
             
				select PROVDES,AUMPDES,tambon,branchName, ' + @cols+@SUM_ST_AP+@SUM_ST_CO+@SUM_SO_AP+@SUM_SO_CO+ '            
				from #pivotResultReport02 				
				union
				select PROVDES,AUMPDES,''����''+AUMPDES collate Thai_CS_AS as tambon, null as branchName, ' + @sumEachCol+ ',sum(SUM_ST_AP) as SUM_ST_AP,sum(SUM_ST_CO) as SUM_ST_CO,sum(SUM_SO_AP) as SUM_SO_AP,sum(SUM_SO_CO) as SUM_SO_CO
				from #pivotResultReport02 group by PROVDES,AUMPDES
				
				union
				select PROVDES,''����''+PROVDES collate Thai_CS_AS AUMPDES,null as tambon, null as branchName, ' + @sumEachCol+ ',sum(SUM_ST_AP) as SUM_ST_AP,sum(SUM_ST_CO) as SUM_ST_CO,sum(SUM_SO_AP) as SUM_SO_AP,sum(SUM_SO_CO) as SUM_SO_CO
				from #pivotResultReport02 group by PROVDES
				union
				select ''����������'' collate Thai_CS_AS PROVDES,null AUMPDES,null as tambon, null as branchName, ' + @sumEachCol+ ',sum(SUM_ST_AP) as SUM_ST_AP,sum(SUM_ST_CO) as SUM_ST_CO,sum(SUM_SO_AP) as SUM_SO_AP,sum(SUM_SO_CO) as SUM_SO_CO
				from #pivotResultReport02 
				
             ) as aa         
             
             endsubScript:    
             ';
             --
--print @query;
execute(@query);
select * from ##finalResultsForPrepaidSalePlaning order by [rank];
theEndOfScript: