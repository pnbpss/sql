USE [serviceweb]
GO
/****** Object:  StoredProcedure [dbo].[processLoadPendingContractForTelephone]    Script Date: 05/31/2014 15:25:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Panu Boonpromsook>
-- Create date: <Create Date,,>
-- Description:	<load loog3nee3 to1ra1sab2>
-- =============================================
ALTER PROCEDURE [dbo].[processLoadPendingContractForTelephone] 
	-- Add the parameters for the stored procedure here
	@locats varchar(max),
	@conditions varchar(max),
	@fromDate varchar(max),
	@toDate varchar(max),
	@loadedByUserId varchar(max)
AS
BEGIN
	declare @sqlLocatString nvarchar(max);
	declare @sqlConditionString nvarchar(max);
	declare @loopSql nvarchar(max);
	declare @locat varchar(max),@pendingPeriodCond varchar(max),@disconnectedDayCond varchar(max);	
	declare @condId int,@countOperation int;
	
	SET NOCOUNT ON;
	set @countOperation=0;	
	set @sqlLocatString = N'declare locatCursor scroll cursor for select LOCATCD from wb_invlocat where LOCATCD in('+@locats+')';	
	EXECUTE sp_executesql @sqlLocatString; 	
	open locatCursor;
	
	set @sqlConditionString = N'declare conditionCursor scroll cursor for select id,pendingPeriod,disconnectedDay from dc_pendingConditions where id in('+@conditions+')';
	EXECUTE sp_executesql @sqlConditionString; --print @sqlConditionString;
	open conditionCursor;		
	
	begin transaction t1;	
	begin try
		fetch next from locatCursor into @locat;
		While (@@FETCH_STATUS <> -1)
		begin
			--print @locats+' '+@conditions;
			fetch first from conditionCursor into @condId,@pendingPeriodCond,@disconnectedDayCond;
			while (@@FETCH_STATUS <> -1)
			begin
				print @locat+' '+convert(varchar,@condId)+'xxx';
				--เริ่มกระบวนการ select และ insert
					set @loopSql = N'
					insert into dc_telephonings(
					loadBy,loadDate,specifiedCallStartDate,specifiedCallEndDate,contno
					,crContStatus,crDebtPlusVat,crPendingPeriod
					,crPendingFromPeriod
					,crPendingToPeriod,crDisconnectedDate,officeLoadedDate,saveCallResultBy
					,lastCallResultSavedDate,callResultId,nextPayDeal,pendingConditionIdOnLoad)
					select '''+@loadedByUserId+''',getdate(),'''+@fromDate+''','''+@toDate+''',a.CONTNO
					,a.CONTSTAT,(a.BALANC-a.SMPAY) crDebtPlusVat ,va.netPendingPeriod
					,isnull(case
						when va.netPendingPeriod>va.pendingInstallmentPeriod then va.lastPaidInstallmentPeriod-1
						else va.lastPaidInstallmentPeriod
					end,0) as crPendingFromPeriod
					,isnull(va.currentperiod,0) as currentperiod,va.disconnectedDays,getdate() as officeLoadedDate,null as saveCallResultBy
					,null,null,null,'''+convert(varchar,@condId)+'''
					from wb_armast a inner join View_Arpay va on a.CONTNO=va.CONTNO
					where va.LOCAT = '''+@locat+''' and '+@pendingPeriodCond+' and '+@disconnectedDayCond+' ';
					print @loopSql;
					EXECUTE sp_executesql @loopSql;
				--จบ	กระบวนการ select และ insert
				fetch next from conditionCursor into @condId,@pendingPeriodCond,@disconnectedDayCond;
				--set @countOperation = @countOperation +1;
			end		
			fetch next from locatCursor into @locat;
		end;
		
	insert into #temporaryRunResult (errorNumber, errorMessage) values('noError','noError');
	commit transaction t1; 	
	end try
	begin catch 
		insert into #temporaryRunResult (errorNumber, errorMessage) values(convert(varchar(max),ERROR_NUMBER()),ERROR_MESSAGE());				
		rollback transaction t1; 	
	end catch
	deallocate locatCursor;
	deallocate conditionCursor;
	--print ' there are '+convert(varchar,@countOperation)+' operations.';
END
