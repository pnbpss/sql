--รายงานสรุปการเอางานตามคืน
--select * from dc_allAssignedJobs where closed='F';
use serviceweb;
declare @issuedDateFrom datetime,@issuedDateTo datetime;
set @issuedDateFrom = '20160101';set @issuedDateTo = '20160831'
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
select 
agr.issuedMonth,
aaj.dcTFSName+'('+convert(varchar,aaj.collectorId)+')'dcTFSName,
case
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 0 and 15 then '000-015'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 16 and 30 then '016-030'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 31 and 45 then '031-045'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 46 and 60 then '046-060'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 61 and 75 then '061-075'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 76 and 90 then '076-090'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 91 and 105 then '091-105'
	when datediff(day,aaj.issuedDate,aaj.closedDate) between 106 and 120 then '106-120'
	when datediff(day,aaj.issuedDate,aaj.closedDate) > 120 then '121-MMM'
end holdJobRange
,
case
	when crDisconnectedDays between 0 and 60 then '000-060'
	when crDisconnectedDays between 61 and 90 then '061-090'
	when crDisconnectedDays between 91 and 180 then '091-180'
	when crDisconnectedDays between 181 and 365 then '181-365'
	when crDisconnectedDays > 365 then '366-MMM'
end ddRange 
,closed
into #tempTb
from dc_allAssignedJobs aaj
cross apply (select convert(varchar,datepart(yy,aaj.issuedDate)+543) +'/'+ case when datepart(mm,aaj.issuedDate)<10 then '0'+convert(varchar,datepart(mm,aaj.issuedDate)) else convert(varchar,datepart(mm,aaj.issuedDate)) end issuedMonth) agr
where aaj.issuedDate between @issuedDateFrom and @issuedDateTo;



--select issuedMonth,dcTFSName,holdJobRange+'_'+ddRange pdType,count(*)cnt from #tempTb group by issuedMonth,dcTFSName,holdJobRange,ddRange
select cntAll.dcTFSName,cntAll.cnt [all]
,isnull(cntAllSuccess.cnt,0) successed
,isnull(cntAllCancel.cnt,0) cancel
,isnull(cntAllPending.cnt,0) holding
,isnull(agr.sumAll,0) [f]
,isnull([P000-015_000-060],0)[P000-015_000-060],isnull([P000-015_061-090],0)[P000-015_061-090],isnull([P000-015_091-180],0)[P000-015_091-180],isnull([P000-015_181-365],0)[P000-015_181-365],isnull([P000-015_366-MMM],0)[P000-015_366-MMM],isnull([P016-030_000-060],0)[P016-030_000-060],isnull([P016-030_061-090],0)[P016-030_061-090],isnull([P016-030_091-180],0)[P016-030_091-180],isnull([P016-030_181-365],0)[P016-030_181-365],isnull([P016-030_366-MMM],0)[P016-030_366-MMM],isnull([P031-045_000-060],0)[P031-045_000-060],isnull([P031-045_061-090],0)[P031-045_061-090],isnull([P031-045_091-180],0)[P031-045_091-180],isnull([P031-045_181-365],0)[P031-045_181-365],isnull([P031-045_366-MMM],0)[P031-045_366-MMM],isnull([P046-060_000-060],0)[P046-060_000-060],isnull([P046-060_061-090],0)[P046-060_061-090],isnull([P046-060_091-180],0)[P046-060_091-180],isnull([P046-060_181-365],0)[P046-060_181-365],isnull([P046-060_366-MMM],0)[P046-060_366-MMM],isnull([P061-075_000-060],0)[P061-075_000-060],isnull([P061-075_061-090],0)[P061-075_061-090],isnull([P061-075_091-180],0)[P061-075_091-180],isnull([P061-075_181-365],0)[P061-075_181-365],isnull([P061-075_366-MMM],0)[P061-075_366-MMM],isnull([P076-090_000-060],0)[P076-090_000-060],isnull([P076-090_061-090],0)[P076-090_061-090],isnull([P076-090_091-180],0)[P076-090_091-180],isnull([P076-090_181-365],0)[P076-090_181-365],isnull([P076-090_366-MMM],0)[P076-090_366-MMM],isnull([P091-105_000-060],0)[P091-105_000-060],isnull([P091-105_061-090],0)[P091-105_061-090],isnull([P091-105_091-180],0)[P091-105_091-180],isnull([P091-105_181-365],0)[P091-105_181-365],isnull([P091-105_366-MMM],0)[P091-105_366-MMM],isnull([P106-120_000-060],0)[P106-120_000-060],isnull([P106-120_061-090],0)[P106-120_061-090],isnull([P106-120_091-180],0)[P106-120_091-180],isnull([P106-120_181-365],0)[P106-120_181-365],isnull([P106-120_366-MMM],0)[P106-120_366-MMM],isnull([P121-MMM_000-060],0)[P121-MMM_000-060],isnull([P121-MMM_061-090],0)[P121-MMM_061-090],isnull([P121-MMM_091-180],0)[P121-MMM_091-180],isnull([P121-MMM_181-365],0)[P121-MMM_181-365],isnull([P121-MMM_366-MMM],0)[P121-MMM_366-MMM]
from 
(
	select dcTFSName,count(*)cnt from #tempTb group by dcTFSName
) cntAll 
left join
(select dcTFSName,'P'+holdJobRange+'_'+ddRange pdType,count(*)cnt from #tempTb where closed='F' group by dcTFSName,holdJobRange,ddRange) t
pivot 
(sum(cnt) for pdType in ([P000-015_000-060],[P000-015_061-090],[P000-015_091-180],[P000-015_181-365],[P000-015_366-MMM],[P016-030_000-060],[P016-030_061-090],[P016-030_091-180],[P016-030_181-365],[P016-030_366-MMM],[P031-045_000-060],[P031-045_061-090],[P031-045_091-180],[P031-045_181-365],[P031-045_366-MMM],[P046-060_000-060],[P046-060_061-090],[P046-060_091-180],[P046-060_181-365],[P046-060_366-MMM],[P061-075_000-060],[P061-075_061-090],[P061-075_091-180],[P061-075_181-365],[P061-075_366-MMM],[P076-090_000-060],[P076-090_061-090],[P076-090_091-180],[P076-090_181-365],[P076-090_366-MMM],[P091-105_000-060],[P091-105_061-090],[P091-105_091-180],[P091-105_181-365],[P091-105_366-MMM],[P106-120_000-060],[P106-120_061-090],[P106-120_091-180],[P106-120_181-365],[P106-120_366-MMM],[P121-MMM_000-060],[P121-MMM_061-090],[P121-MMM_091-180],[P121-MMM_181-365],[P121-MMM_366-MMM])) p
cross apply (select (isnull([P000-015_000-060],0)+isnull([P000-015_061-090],0)+isnull([P000-015_091-180],0)+isnull([P000-015_181-365],0)+isnull([P000-015_366-MMM],0)+isnull([P016-030_000-060],0)+isnull([P016-030_061-090],0)+isnull([P016-030_091-180],0)+isnull([P016-030_181-365],0)+isnull([P016-030_366-MMM],0)+isnull([P031-045_000-060],0)+isnull([P031-045_061-090],0)+isnull([P031-045_091-180],0)+isnull([P031-045_181-365],0)+isnull([P031-045_366-MMM],0)+isnull([P046-060_000-060],0)+isnull([P046-060_061-090],0)+isnull([P046-060_091-180],0)+isnull([P046-060_181-365],0)+isnull([P046-060_366-MMM],0)+isnull([P061-075_000-060],0)+isnull([P061-075_061-090],0)+isnull([P061-075_091-180],0)+isnull([P061-075_181-365],0)+isnull([P061-075_366-MMM],0)+isnull([P076-090_000-060],0)+isnull([P076-090_061-090],0)+isnull([P076-090_091-180],0)+isnull([P076-090_181-365],0)+isnull([P076-090_366-MMM],0)+isnull([P091-105_000-060],0)+isnull([P091-105_061-090],0)+isnull([P091-105_091-180],0)+isnull([P091-105_181-365],0)+isnull([P091-105_366-MMM],0)+isnull([P106-120_000-060],0)+isnull([P106-120_061-090],0)+isnull([P106-120_091-180],0)+isnull([P106-120_181-365],0)+isnull([P106-120_366-MMM],0)+isnull([P121-MMM_000-060],0)+isnull([P121-MMM_061-090],0)+isnull([P121-MMM_091-180],0)+isnull([P121-MMM_181-365],0)+isnull([P121-MMM_366-MMM],0))sumAll) agr
on cntAll.dcTFSName=p.dcTFSName
left join 
(
	select dcTFSName,count(*)cnt from #tempTb where closed  in ('R','S','P','H','Q') group by dcTFSName
) cntAllSuccess on cntAll.dcTFSName=cntAllSuccess.dcTFSName
left join 
(
	select dcTFSName,count(*)cnt from #tempTb where closed ='N' group by dcTFSName
) cntAllPending on cntAll.dcTFSName=cntAllPending.dcTFSName
left join 
(
	select dcTFSName,count(*)cnt from #tempTb where closed ='C' group by dcTFSName
) cntAllCancel on cntAll.dcTFSName=cntAllCancel.dcTFSName
order by cntAll.dcTFSName
--select distinct closed from dc_allAssignedJobs
--select * from dc_allAssignedJobs where collectorId='90'