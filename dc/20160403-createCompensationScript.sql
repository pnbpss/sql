use serviceweb;
/*
create table dc_cpCompensations(
custPaidbahtFrom decimal(18,2) not null,
custPaidBahtToButLessThan decimal(18,2) not null,
cpPercent decimal(7,2) not null
);
CREATE UNIQUE CLUSTERED INDEX [dc_cpCompensationsIDX] ON [dbo].[dc_cpCompensations] 
(
	[custPaidbahtFrom] ASC,
	[custPaidBahtToButLessThan] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create table dc_cpBonus(
bathFrom decimal(18,2) not null,
BahtToLessButThan decimal(18,2) not null,
percentOfCompleteJobFrom decimal(18,2) not null,
percentOfCompleteJobToButLessThan decimal(18,2) not null,
cpBaht decimal(18,2) not null
);
CREATE UNIQUE CLUSTERED INDEX [dc_cpBonus] ON [dbo].[dc_cpBonus] 
(
	[bathFrom] ASC,
	[BahtToLessButThan] ASC,
	[percentOfCompleteJobFrom] ASC,
	[percentOfCompleteJobToButLessThan] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


create table dc_cpFuel(
custPaidFrom decimal(18,2) not null,
custPaidToButLessThan decimal(18,2) not null,
cpBaht decimal(18,2) not null
);
CREATE UNIQUE CLUSTERED INDEX [dc_cpFuelIdx] ON [dbo].[dc_cpFuel] 
(
	[custPaidFrom] ASC,
	[custPaidToButLessThan] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create table dc_cpSeize(
siezedCountFrom decimal(18,2) not null,
siezedCountToButLessThan decimal(18,2) not null,
multiplyBy decimal(18,2) not null,
bonusBaht decimal(18,2) not null
);
CREATE UNIQUE CLUSTERED INDEX [dc_cpSeize] ON [dbo].[dc_cpSeize] 
(
	[siezedCountFrom] ASC,
	[siezedCountToButLessThan] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create table dc_cpModelCompensates(
modelInSenior varchar(50) not null,
modelInRealworld varchar(50) not null,
compensations decimal(18,2) not null
);
CREATE UNIQUE CLUSTERED INDEX [dc_cpModelCompensatesIdx] ON [dbo].[dc_cpModelCompensates] 
(
	[modelInSenior] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
*/
/*
insert into dc_cpCompensations values(0,40000,0.08);
insert into dc_cpCompensations values(40000,999999999,0.09);

insert into dc_cpBonus values(0,40000,0,30,0);
insert into dc_cpBonus values(0,40000,30,35,0);
insert into dc_cpBonus values(0,40000,35,200,500);
insert into dc_cpBonus values(40000,50000,0,30,0);
insert into dc_cpBonus values(40000,50000,30,35,0);
insert into dc_cpBonus values(40000,50000,35,200,600);
insert into dc_cpBonus values(50000,60000,0,30,0);
insert into dc_cpBonus values(50000,60000,30,35,0);
insert into dc_cpBonus values(50000,60000,35,200,700);
insert into dc_cpBonus values(60000,70000,0,30,300);
insert into dc_cpBonus values(60000,70000,30,35,500);
insert into dc_cpBonus values(60000,70000,35,200,1500);
insert into dc_cpBonus values(70000,80000,0,30,600);
insert into dc_cpBonus values(70000,80000,30,35,800);
insert into dc_cpBonus values(70000,80000,35,200,1800);
insert into dc_cpBonus values(80000,90000,0,30,1200);
insert into dc_cpBonus values(80000,90000,30,35,1400);
insert into dc_cpBonus values(80000,90000,35,200,2400);
insert into dc_cpBonus values(90000,100000,0,30,2700);
insert into dc_cpBonus values(90000,100000,30,35,1700);
insert into dc_cpBonus values(90000,100000,35,200,1500);
insert into dc_cpBonus values(100000,110000,0,30,3500);
insert into dc_cpBonus values(100000,110000,30,35,2500);
insert into dc_cpBonus values(100000,110000,35,200,2300);
insert into dc_cpBonus values(110000,120000,0,30,3800);
insert into dc_cpBonus values(110000,120000,30,35,2800);
insert into dc_cpBonus values(110000,120000,35,200,2600);
insert into dc_cpBonus values(120000,130000,0,30,4200);
insert into dc_cpBonus values(120000,130000,30,35,3200);
insert into dc_cpBonus values(120000,130000,35,200,3000);
insert into dc_cpBonus values(130000,140000,0,30,5000);
insert into dc_cpBonus values(130000,140000,30,35,4000);
insert into dc_cpBonus values(130000,140000,35,200,3800);
insert into dc_cpBonus values(140000,150000,0,30,5400);
insert into dc_cpBonus values(140000,150000,30,35,4400);
insert into dc_cpBonus values(140000,150000,35,200,4200);
insert into dc_cpBonus values(150000,200000,0,30,5800);
insert into dc_cpBonus values(150000,200000,30,35,4800);
insert into dc_cpBonus values(150000,200000,35,200,4600);
insert into dc_cpBonus values(200000,250000,0,30,6600);
insert into dc_cpBonus values(200000,250000,30,35,5600);
insert into dc_cpBonus values(200000,250000,35,200,5400);
insert into dc_cpBonus values(250000,99999999,0,30,7500);
insert into dc_cpBonus values(250000,99999999,30,35,6500);
insert into dc_cpBonus values(250000,99999999,35,200,6300);




insert into dc_cpFuel values(0,1,0);
insert into dc_cpFuel values(1,6,100);
insert into dc_cpFuel values(6,11,210);
insert into dc_cpFuel values(11,21,440);
insert into dc_cpFuel values(21,31,690);
insert into dc_cpFuel values(31,41,960);
insert into dc_cpFuel values(41,51,1250);
insert into dc_cpFuel values(51,61,1800);
insert into dc_cpFuel values(61,71,2170);
insert into dc_cpFuel values(71,81,2560);
insert into dc_cpFuel values(81,91,2970);
insert into dc_cpFuel values(91,100,3400);
insert into dc_cpFuel values(100,101,3400);
insert into dc_cpFuel values(101,9999999,3535);



insert into dc_cpSeize values(0,5,200,0);
insert into dc_cpSeize values(5,9,300,1000);
insert into dc_cpSeize values(9,16,400,1500);
insert into dc_cpSeize values(16,21,500,5000);
insert into dc_cpSeize values(21,31,550,8000);
insert into dc_cpSeize values(31,41,650,10000);
insert into dc_cpSeize values(41,50,750,12000);
insert into dc_cpSeize values(50,51,750,12000);
insert into dc_cpSeize values(51,999999,800,12000);


insert into dc_cpModelCompensates values('AFS110','wave110',100);
insert into dc_cpModelCompensates values('AFS125','wave125',100);
insert into dc_cpModelCompensates values('NS110','wave110',100);
insert into dc_cpModelCompensates values('NF110','wave110',100);
insert into dc_cpModelCompensates values('NF125','wave125',100);
*/