declare @loadDate datetime;
set @loadDate='20170201';
--select * from dc_telephonings t left join dc_telephoneNumberChangings cr on t.id=cr.telephoningId where cr.telephoningId is not null and t.loadDate<@loadDate
--truncate table dc_telephoningsNotCalled

insert into dc_telephoningsNotCalled select * from dc_telephonings t left join dc_callResultOfEachConts cr on t.id=cr.telephoningId where cr.telephoningId is null and t.loadDate<@loadDate

delete from dc_telephonings where loadDate<@loadDate and id not in
(	
	select distinct telephoningId from dc_telephoneNumberChangings
	union
	select distinct telephoningId from dc_dueDatePostponements where telephoningId is not null
	union
	select distinct telephoningId from dc_callResultOfEachConts
)

--select * from dc_telephonings

--select *into #tempTb from (select distinct * from dc_telephoningsNotCalled) a

--insert into dc_telephoningsNotCalled select * from #tempTb
--select * from dc_telephonings t left join dc_callResultOfEachConts cr on t.id=cr.telephoningId where cr.telephoningId is null and t.loadDate<'20170501' order by loadDate desc
--select * from dc_telephonings t left join dc_callResultOfEachConts cr on t.id=cr.telephoningId where cr.telephoningId is not null and t.loadDate<'20170501' order by loadDate desc