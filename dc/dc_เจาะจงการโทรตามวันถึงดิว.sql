use serviceweb;
IF OBJECT_ID('tempdb.dbo.##abcBpsPanu', 'U') IS NOT NULL DROP TABLE ##abcBpsPanu;
IF OBJECT_ID('tempdb.dbo.##defBpsPanu', 'U') IS NOT NULL DROP TABLE ##defBpsPanu;
IF OBJECT_ID('tempdb.dbo.##jkfBpsPanu', 'U') IS NOT NULL DROP TABLE ##jkfBpsPanu;
declare @specifiedCallDate datetime;
declare @currentLoadDate datetime;
set @specifiedCallDate = (select max(specifiedCallStartDate) from dc_telephonings);
set @currentLoadDate = (select loadDate from dc_telephonings where loadDate=@currentLoadDate)
select top 500
t.locat,va.CONTNO contno,c.customerFullName,c.TELP cTelp 
,case when isnull(va.nextDealDate,'')='' then 'YYYY/00/00'
	else dbo.FN_ConvertDate(va.nextDealDate,'Y/M/D') 
end ndd	
,dbo.FN_ConvertDate(va.lastPaidDate,'Y/M/D') lpd
,t.crPendingPeriod cpd,va.netPendingPeriod npd
,dbo.FN_ConvertDate(lcr.dateOfCall,'Y/M/D') doc,lcr.reallyFound,cr.[description],dbo.FN_ConvertDate(opp.LACCPDT,'Y/M/D') pppdt,st.fullName agName ,st.TELP sTelp
,case when isnull(boc.contno,'N')='N' then 'N' else 'Y' end boc
into ##abcBpsPanu
from dc_telephonings t left join view_arpay2 va on t.contno=va.CONTNO 
	  left join dc_lastCallResultOfEachConts lcr on t.id=lcr.telephoningId
	  left join dc_callResults cr on lcr.callResultId=cr.id
	  left join wb_customers c on t.cuscod=c.CUSCOD 
	  left join wb_surety1 st on t.contno=st.CONTNO
	  left join dc_onPostponePayContracts opp on t.contno=opp.CONTNO
	  left join dc_beingOnCollectionConts boc on t.contno=boc.contno
where 1=1
--and va.nextDealDate between DATEADD(day,-1,getdate()) and DATEADD(month,1,getdate())
and t.crPendingPeriod between 1 and 1
and t.crDisconnectedDate between 1 and 365
and t.specifiedCallStartDate = @specifiedCallDate
--and t.locat in ('YJ���')

--select * from ##abcBpsPanu
--select count(*) cnt ,contno,'D'+replace(substring(ndd,6,5),'/','_') ndd from #abc group by contno,substring(ndd,6,5)
DECLARE @cols AS NVARCHAR(MAX),@colsForOrder AS NVARCHAR(MAX),@colsForSum AS NVARCHAR(MAX), @query  AS NVARCHAR(MAX),@query2 AS NVARCHAR(MAX)

select @cols = STUFF((SELECT distinct ',' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_')) 
                    from ##abcBpsPanu
                    order by ',' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)'),1,1,'');

select @colsForOrder = STUFF((SELECT distinct ',' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))+' desc' 
                    from ##abcBpsPanu
                    order by ',' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))+' desc' 
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)'),1,1,'');

select @colsForSum = STUFF((SELECT distinct ',sum(' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))+') '+ QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))
                    from ##abcBpsPanu
                    order by ',sum(' + QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))+') '+ QUOTENAME('D'+replace(substring(ndd,6,5),'/','_'))
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)'),1,1,'');

set @query = 'SELECT contno, ' + @cols + ' 
				into ##defBpsPanu
				from 
             (
                select contno,''D''+replace(substring(ndd,6,5),''/'',''_'') ndd, count(*) cnt
                from ##abcBpsPanu
                group by contno,''D''+replace(substring(ndd,6,5),''/'',''_'')
            ) x
            pivot 
            (
                sum(cnt)
                for ndd in (' + @cols + ')
            ) p '

execute(@query)

--print @colsForSum;

set @query2='
select * into ##jkfBpsPanu from (
select a.locat,a.customerFullName,a.cTelp,agName,sTelp,a.pppdt,a.boc,a.ndd,a.lpd,a.cpd,a.npd,a.doc,a.reallyFound,a.[description] ,'+@cols+' from ##abcBpsPanu a left join ##defBpsPanu b on a.contno=b.contno
union select ''���'' locat,null customerFullName,null cTelp,null agName,null sTelp,null pppdt,null boc,null ndd,null lpd,null cpd,null npd,null doc,null reallyFound,null [description] ,'+@colsForSum+' from ##abcBpsPanu a left join ##defBpsPanu b on a.contno=b.contno
--union select locat,null customerFullName,null cTelp,null agName,null sTelp,null pppdt,null ndd,null lpd,null cpd,null doc,null reallyFound,''�''+locat+''���'' [description] ,'+@colsForSum+' from ##abcBpsPanu a left join ##defBpsPanu b on a.contno=b.contno group by locat
) allunion
order by allunion.locat,'+@colsForOrder+'
';
execute(@query2)


select * from ##jkfBpsPanu