use serviceweb;
--ควรลบหลังเลยเดือน พ.ค. 59 ไปแล้ว เพราะอาจจะมีเร่งรัดคงเหลือหล้องอยู่ที่ไม่ได้เอาเข้าอีก
begin tran t1
begin try
	declare @maxLoadDate datetime;
	set @maxLoadDate = (select MAX(loadDate) from dc_loadedJobsForAssignToDebtCollector);
	insert into dc_loadedJobsForAssignToDebtCollectorNotAssigned
				([id],[jobBranchId],[loadDate],[loadBy],[contno],[crContStatus],[crDebtPlusVat],[crPendingPeriod],
				[crRemainDebt],[crPendingFromPeriod],[crPendingToPeriod],[crDisconnectedDays],[crLastPaidDate],
				[postponeDueTo],[lastUpdateBy],[lastUpdateDate]) 
	select ljf.[id],ljf.[jobBranchId],ljf.[loadDate],ljf.[loadBy],ljf.[contno],ljf.[crContStatus],ljf.[crDebtPlusVat]
				,ljf.[crPendingPeriod],ljf.[crRemainDebt],ljf.[crPendingFromPeriod],ljf.[crPendingToPeriod]
				,ljf.[crDisconnectedDays],ljf.[crLastPaidDate],ljf.[postponeDueTo],ljf.[lastUpdateBy],ljf.[lastUpdateDate]
	from dc_loadedJobsForAssignToDebtCollector ljf
	left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
	where
	1=1
	and loadDate<@maxLoadDate
	and irj.jobId is null

	delete from dc_loadedJobsForAssignToDebtCollector where id in
	(
		select ljf.id
		from dc_loadedJobsForAssignToDebtCollector ljf
		left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
		where
		1=1
		and loadDate<@maxLoadDate
		and irj.jobId is null
	);

commit tran t1;
end try
begin catch		
-- save error from transaction into table		
print 'error code='+convert(varchar(max),ERROR_NUMBER()) +', error Msg='+ ERROR_MESSAGE();
rollback tran t1;
end catch

--select COUNT(*)cnt from dc_loadedJobsForAssignToDebtCollectorNotAssigned where datediff(month,loadDate,GETDATE())>7