USE [serviceweb]
GO

/****** Object:  Table [dbo].[dc_loadedJobsForAssignToDebtCollector]    Script Date: 07/12/2015 22:02:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dc_loadedJobsForAssignToDebtCollector](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[jobBranchId] [bigint] NOT NULL,
	[loadDate] [datetime] NOT NULL,
	[loadBy] [varchar](50) NOT NULL,
	[contno] [varchar](50) NOT NULL,
	[crContStatus] [char](1) NOT NULL,
	[crDebtPlusVat] [decimal](18, 2) NOT NULL,
	[crPendingPeriod] [int] NOT NULL,
	[crRemainDebt] [decimal](18, 2) NOT NULL,
	[crPendingFromPeriod] [int] NOT NULL,
	[crPendingToPeriod] [int] NOT NULL,
	[crDisconnectedDays] [int] NOT NULL,
	[crLastPaidDate] [datetime] NULL,
	[postponeDueTo] [datetime] NULL,
	[lastUpdateBy] [varchar](50) NULL,
	[lastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_dc_loadedJobsForAssignToDebtCollector] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'สถานะสัญญา' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crContStatus'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ลน_คงเหลือรวม VAT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crDebtPlusVat'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'จำนวนงวดที่ค้าง' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crPendingPeriod'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'เริ่มค้างจากงวด' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crPendingFromPeriod'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ค้างจนถึงงวด' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crPendingToPeriod'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'จำนวนวันขาดการติดต่อ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crDisconnectedDays'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'วันที่เข้ามาจ่ายล่าสุด' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_loadedJobsForAssignToDebtCollector', @level2type=N'COLUMN',@level2name=N'crLastPaidDate'
GO

ALTER TABLE [dbo].[dc_loadedJobsForAssignToDebtCollector]  WITH CHECK ADD  CONSTRAINT [FK_dc_loadedJobsForAssignToDebtCollector_dc_loadedContForCollection] FOREIGN KEY([contno])
REFERENCES [dbo].[dc_loadedContForCollection] ([contno])
GO

ALTER TABLE [dbo].[dc_loadedJobsForAssignToDebtCollector] CHECK CONSTRAINT [FK_dc_loadedJobsForAssignToDebtCollector_dc_loadedContForCollection]
GO

