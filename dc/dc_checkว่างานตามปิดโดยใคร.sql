select 

assignedId,jobId
,contno
,dbo.FN_ConvertDate(loadDate,'Y/M/D')loadDate
,dbo.FN_ConvertDate(issuedDate,'Y/M/D')issuedDate
,dcTFSName
,dbo.FN_ConvertDate(closedDate,'Y/M/D')+' '+convert(varchar,closedDate,114) closedSince
,closedDate
,closed
,closedBy,u.*
from dc_allAssignedJobs asj left join dc_users u on asj.closedBy=u.id
where 1=1
--and contno in ('�HP-16040028','�HP-14120017','�HP-15070008','�HP-16070005','�HP-14080034','�HP-16050067')
--and closed='F'