use serviceweb;
IF OBJECT_ID('tempdb..#tempTb2') is not null DROP TABLE #tempTb2
declare @specifiedStartDate datetime, @fromDate datetime, @toDate datetime;
set @specifiedStartDate ='20160705';
set @fromDate = '20160705'
set @toDate  = '20160715'
select pendingStatus,locat,
isnull(D01,0)D01,isnull(D02,0)D02,isnull(D03,0)D03,isnull(D04,0)D04,isnull(D05,0)D05,isnull(D06,0)D06,
isnull(D07,0)D07,isnull(D08,0)D08,isnull(D09,0)D09,isnull(D10,0)D10,isnull(D11,0)D11,isnull(D12,0)D12,isnull(D13,0)D13,
isnull(D14,0)D14,isnull(D15,0)D15,isnull(D16,0)D16,isnull(D17,0)D17,isnull(D18,0)D18,isnull(D19,0)D19,isnull(D20,0)D20,
isnull(D21,0)D21,isnull(D22,0)D22,isnull(D23,0)D23,isnull(D24,0)D24,isnull(D25,0)D25,isnull(D26,0)D26,isnull(D27,0)D27,
isnull(D28,0)D28,isnull(D29,0)D29,isnull(D30,0)D30,isnull(D31,0)D31,
(isnull(D01,0)+isnull(D02,0)+isnull(D03,0)+isnull(D04,0)+isnull(D05,0)+isnull(D06,0)+
isnull(D07,0)+isnull(D08,0)+isnull(D09,0)+isnull(D10,0)+isnull(D11,0)+isnull(D12,0)+isnull(D13,0)+
isnull(D14,0)+isnull(D15,0)+isnull(D16,0)+isnull(D17,0)+isnull(D18,0)+isnull(D19,0)+isnull(D20,0)+
isnull(D21,0)+isnull(D22,0)+isnull(D23,0)+isnull(D24,0)+isnull(D25,0)+isnull(D26,0)+isnull(D27,0)+
isnull(D28,0)+isnull(D29,0)+isnull(D30,0)+isnull(D31,0)) sumCalled
,isnull(DNO,0)DNO

into #tempTb2
from (
 select count(*) cnt, pendingStatus,doc,locat
 from (
  SELECT 
	t.locat
     ,case
    when t.crPendingPeriod<=1 then ' 1 ����+1 �Ǵ'
    when t.crPendingPeriod=2 and t.crDisconnectedDate >=0 and t.crDisconnectedDate<=15 then ' 2 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
    when t.crPendingPeriod=2 and t.crDisconnectedDate >=16 and t.crDisconnectedDate<=90 then ' 3 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 16-90 �ѹ'
    when t.crPendingPeriod=2 and t.crDisconnectedDate >90 then ' 4 ��ҧ 2 �Ǵ �Ҵ��õԴ����ҡ���� 90 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=0 and t.crDisconnectedDate <=15 then ' 5 ��ҧ 3 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=16 and t.crDisconnectedDate <=90 then ' 6 ��ҧ 3 �Ǵ �Ҵ��õԴ��� 16-90 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=91 and t.crDisconnectedDate <=120 then ' 7 ��ҧ 3 �Ǵ �Ҵ��õԴ��͵���� 91-120 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=121 and t.crDisconnectedDate <=180 then ' 8 ��ҧ 3 �Ǵ �Ҵ��õԴ��͵���� 121-180 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=181 and t.crDisconnectedDate <=365 then ' 9 ��ҧ 3 �Ǵ �Ҵ��õԴ��͵���� 181-365 �ѹ'
    when t.crPendingPeriod=3 and t.crDisconnectedDate >=366 then '10 ��ҧ 3 �Ǵ �Ҵ��õԴ��͵���� 366 �ѹ����'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=0 and t.crDisconnectedDate <=60 then '11 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 0-60 �ѹ'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=61 and t.crDisconnectedDate <=120 then '12 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 61-120 �ѹ'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=121 and t.crDisconnectedDate <=150 then '13 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 121-150 �ѹ'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=151 and t.crDisconnectedDate <=180 then '14 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 151-180 �ѹ'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=181 and t.crDisconnectedDate <=365 then '15 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 181-365 �ѹ'
    when t.crPendingPeriod>3 and t.crDisconnectedDate >=366 then '16 ��ҧ >3 �Ǵ �Ҵ��õԴ��͵���� 366 �ѹ����'
     end pendingStatus   
   ,
	case when isnull(day(lcr.dateOfCall),0)=0 then 'DNO'
		else
		case when day(lcr.dateOfCall)<10 then 'D0'+convert(varchar(max),day(lcr.dateOfCall))
			else 'D'+convert(varchar(max),day(lcr.dateOfCall))
		end
	end doc   
    FROM dc_telephonings t 
    left join dc_lastCallResultOfEachConts lcr on t.id=lcr.telephoningId         
    where 1=1 
    and t.specifiedCallStartDate=@specifiedStartDate
    and (lcr.callOrder>=5 or lcr.callOrder is null)
    --and lcr.callOrder>=1
    --and lcr.dateOfCall between @fromDate and @toDate
 ) p
 group by pendingStatus,doc,locat
) tb
pivot
(
	sum(cnt) for doc in (DNO,D01,D02,D03,D04,D05,D06,D07,D08,D09,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31)
) pv


select pendingStatus, sum(D01)D01,sum(D02)D02,sum(D03)D03,sum(D04)D04,sum(D05)D05,sum(D06)D06,sum(D07)D07,sum(D08)D08,sum(D09)D09,sum(D10)D10,sum(D11)D11,sum(D12)D12,sum(D13)D13,sum(D14)D14,sum(D15)D15,sum(D16)D16,sum(D17)D17,sum(D18)D18,sum(D19)D19,sum(D20)D20,sum(D21)D21,sum(D22)D22,sum(D23)D23,sum(D24)D24,sum(D25)D25,sum(D26)D26,sum(D27)D27,sum(D28)D28,sum(D29)D29,sum(D30)D30,sum(D31)D31,sum(sumCalled)sumCalled,sum(DNO)DNO from #tempTb2 group by pendingStatus
union
select '����������' pendingStatus, sum(D01)D01,sum(D02)D02,sum(D03)D03,sum(D04)D04,sum(D05)D05,sum(D06)D06,sum(D07)D07,sum(D08)D08,sum(D09)D09,sum(D10)D10,sum(D11)D11,sum(D12)D12,sum(D13)D13,sum(D14)D14,sum(D15)D15,sum(D16)D16,sum(D17)D17,sum(D18)D18,sum(D19)D19,sum(D20)D20,sum(D21)D21,sum(D22)D22,sum(D23)D23,sum(D24)D24,sum(D25)D25,sum(D26)D26,sum(D27)D27,sum(D28)D28,sum(D29)D29,sum(D30)D30,sum(D31)D31,sum(sumCalled)sumCalled,sum(DNO)DNO from #tempTb2 
order by pendingStatus

/*
select pendingStatus,locat,D01,D02,D03,D04,D05,D06,D07,D08,D09,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,sumCalled,DNO from #tempTb2
union
select '����' pendingStatus,locat, sum(D01)D01,sum(D02)D02,sum(D03)D03,sum(D04)D04,sum(D05)D05,sum(D06)D06,sum(D07)D07,sum(D08)D08,sum(D09)D09,sum(D10)D10,sum(D11)D11,sum(D12)D12,sum(D13)D13,sum(D14)D14,sum(D15)D15,sum(D16)D16,sum(D17)D17,sum(D18)D18,sum(D19)D19,sum(D20)D20,sum(D21)D21,sum(D22)D22,sum(D23)D23,sum(D24)D24,sum(D25)D25,sum(D26)D26,sum(D27)D27,sum(D28)D28,sum(D29)D29,sum(D30)D30,sum(D31)D31,sum(sumCalled)sumCalled,sum(DNO)DNO from #tempTb2 group by locat
union
select '����' pendingStatus,'�������'locat, sum(D01)D01,sum(D02)D02,sum(D03)D03,sum(D04)D04,sum(D05)D05,sum(D06)D06,sum(D07)D07,sum(D08)D08,sum(D09)D09,sum(D10)D10,sum(D11)D11,sum(D12)D12,sum(D13)D13,sum(D14)D14,sum(D15)D15,sum(D16)D16,sum(D17)D17,sum(D18)D18,sum(D19)D19,sum(D20)D20,sum(D21)D21,sum(D22)D22,sum(D23)D23,sum(D24)D24,sum(D25)D25,sum(D26)D26,sum(D27)D27,sum(D28)D28,sum(D29)D29,sum(D30)D30,sum(D31)D31,sum(sumCalled)sumCalled,sum(DNO)DNO from #tempTb2 
order by locat,pendingStatus
*/