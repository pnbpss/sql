use serviceweb;
declare @locat varchar(50);
set @locat = 'YK��1'
IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results
select main.loadDate, main.cntLoadedJob
,(
    select count(*) cntLoadedJob
    from  dc_loadedContForCollection lcc 
          inner join dc_beingOnCollectionConts boc on boc.contno=lcc.contno         
    where lcc.locat=@locat and boc.loadDate<main.loadDate      
) pendingFromPreviousLoad
,(
    select count(*)
    from dc_loadedJobsForAssignToDebtCollector ljf
         left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
         left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
    where lcc.locat=@locat
         and irj.deleted='N'
         and irj.closed!='N'
         and ljf.loadDate<=main.loadDate    
) sumClosedFromPrevious
,(
    select count(*)
    from dc_loadedJobsForAssignToDebtCollector ljf
         left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
         left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
    where lcc.locat=@locat
         and irj.deleted='N'
         --and irj.closed='N'
         and ljf.loadDate<=main.loadDate    
) sumIssuedFromPrevious
, isnull(issued.cntLoadedJob,0) issued
, isnull(completed.cntLoadedJob,0) closed
, isnull(onlyContOfThisDate.cntLoadedJob,0) onlyContOfThisDate
into #Results
from (
    select count(*) cntLoadedJob, ljf.loadDate
    from dc_loadedJobsForAssignToDebtCollector ljf left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
    where 1=1
        and lcc.locat=@locat
       -- and ljf.loadDate > DATEADD(month,-6,getdate())
    group by ljf.loadDate
) as main left join (
    select count(*) cntLoadedJob, ljf.loadDate
    from dc_loadedJobsForAssignToDebtCollector ljf
        inner join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
        inner join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
    where 1=1
        and irj.jobId is not null
        and irj.deleted='N'
        and lcc.locat=@locat
    group by ljf.loadDate
) issued on main.loadDate=issued.loadDate
left join (
    select count(*) cntLoadedJob, ljf.loadDate
    from dc_loadedJobsForAssignToDebtCollector ljf left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
    left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
    where 1=1
        and irj.jobId is not null
        and irj.deleted='N' and irj.closed!='N'
        and lcc.locat=@locat
    group by ljf.loadDate
) completed on main.loadDate=completed.loadDate
left join 
(
    select 
    COUNT(*) cntLoadedJob,t.loadDate
    from dc_loadedContForCollection lcc
     left join dc_loadedJobsForAssignToDebtCollector t on lcc.contno=t.contno 
     left join dc_issuedReturnedJobFromCollectors irj on t.id=irj.jobId and irj.deleted='N'
     left join dc_beingOnCollectionConts boc on t.contno=boc.contno and t.loadDate>boc.loadDate
    where 1=1     
    and lcc.locat=@locat and boc.jobId is not null
    group by t.loadDate
    --order by t.loadDate desc
) onlyContOfThisDate on main.loadDate=onlyContOfThisDate.loadDate

--order by main.loadDate desc
select dbo.FN_ConvertDate(loadDate,'D/M/Y') loadTDate
       ,cntLoadedJob,pendingFromPreviousLoad       
       ,issued,closed
       ,onlyContOfThisDate --੾���ѭ�ҷ����Ŵ�������ͺ��� ����繧ҹ��ҧ�Ҩҡ�ͺ��͹
       ,((cntLoadedJob-(onlyContOfThisDate+issued+closed))) remainTOIssue
       ,sumIssuedFromPrevious
       ,sumClosedFromPrevious
       ,(sumIssuedFromPrevious-sumClosedFromPrevious) totalDCPending
from #Results order by loadDate desc

--select * from #Results order by loadDate desc
