select reportId,[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20]
from
	 (
		select 
		count(*) cnt,uv.reportId,datepart(hh,uv.viewTime) hh
		from dc_userViewReportsLogs uv 
			  left join dc_users u on uv.userId=u.id
			  left join dc_supervisors sup on u.loginname=sup.hiincomeUserId
		where uv.userId!=1 and uv.ipAddress!='192.168.0.170' 
			  --and sup.id is not null
		group by uv.reportId,uv.userId,datepart(hh,uv.viewTime)
	) t 
	pivot
	(
		sum(cnt) for hh in ([6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
	) p

select reportId,[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20]
from
	 (
		select 
		count(*) cnt,uv.reportId,datepart(hh,uv.viewTime) hh
		from dc_userViewReportsLogs uv 
			  left join dc_users u on uv.userId=u.id
			  left join dc_supervisors sup on u.loginname=sup.hiincomeUserId
		where uv.userId!=1 and uv.ipAddress!='192.168.0.170' and sup.id is null
		group by uv.reportId,uv.userId,datepart(hh,uv.viewTime)
	) t 
	pivot
	(
		sum(cnt) for hh in ([6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
	) p
