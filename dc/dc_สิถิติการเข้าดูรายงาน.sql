/****** Script for SelectTopNRows command from SSMS  ******/
/*
SELECT [seq]
      ,[userId]
      ,[reportId]
      ,[viewTime]
      ,[requestDump]
      ,[ipAddress]
FROM [dc_userViewReportsLogs]
*/

select '('+u.loginname+') '+u.name uname,isnull([1],0) R01,isnull(p.[8],0) R08,isnull([9],0) R09,isnull([14],0) R14,isnull([15],0) R15,isnull([16],0) R16,isnull([17],0) R17 
,(isnull([1],0)+isnull([8],0)+isnull([9],0)+isnull([14],0)+isnull([15],0)+isnull([16],0)+isnull([17],0)) sumAll
,case when isnull(sup.id,'') != '' then 'พี่'
	  when u.loginname='005' then 'บริหาร'
	  else 'น้อง'
end plevel
from 
	(
		select count(*) cnt,reportId, userId
		from dc_userViewReportsLogs
		where userId!=1 and ipAddress!='192.168.0.170'
		group by reportId,userId		
	) t
	pivot 
	(
		sum(cnt) for reportId in ([1],[8],[9],[14],[15],[16],[17])
	) p
left join dc_users u on p.userId=u.id
left join dc_supervisors sup on u.loginname=sup.hiincomeUserId
order by (isnull([1],0)+isnull([8],0)+isnull([9],0)+isnull([14],0)+isnull([15],0)+isnull([16],0)+isnull([17],0)) desc
