/****** Script for SelectTopNRows command from SSMS  ******/

insert into [dc_dueDatePostponements](
	[contno],[cuscod],[laccpdt],[apprvby],authrby,[recvdt],[delycnt],[inpdt],[userid],[memo1]
	
	,[crContStatus],[crDebtPlusVat],[crPendingPeriod],[crRemainDebt],[crPendingFromPeriod],[crDueDate]
	,[crPendingToPeriod],[crDisconnectedDays],[crLastPaidDate]
	
	,[custRequestVia],[additionalInfo],[deleted],[deletedBy],[deletedSince]
 )
 
 SELECT --top 20
	  c.[CONTNO],c.[CUSCOD],c.[LACCPDT],c.[APPRVBY],c.[AUTHRBY],c.[RECVDT],c.[DELYCNT],c.[INPDT],c.[USERID],c.[MEMO1]
      /*
      ,a.CONTSTAT [crContStatus]
      ,(a.TOTPRC-a.SMPAY) [crDebtPlusVat]
      ,va.netPendingPeriod [pendingperiod]
      ,a.EXP_AMT [crRemainDebt]
      ,case
		when isnull(va.currentperiod,0)=0 then 0
		when va.neverpaid='Y' then 1
		when va.netPendingPeriod=0 then 0
		else va.currentperiod-(va.netPendingPeriod-1)
	  end as crPendingFromPeriod
	  ,case 
		  when (va.installmentPerPeriod/va.lastPaidInstall) < 2 then 
			dateadd(month,(isnull(va.lastPaidInstallmentPeriod,1)),va.startContract)
		  else 
			dateadd(month,(isnull(va.lastPaidInstallmentPeriod,1)-1),va.startContract)
		end as crDueDate
      ,isnull(va.currentperiod,0) [pendingToPeriod]
      ,va.[disconnectedDays]
      ,va.[lastPaidDate]  
      */,null,null,null,null,null,null,null,null,null
      ,'N/A' [custRequestVia]      
      ,'##FT##' [additionalInfo]
      ,'N' [deleted]
      ,null [deletedBy]
      ,null [deletedSince]
  FROM [HIINCOME].[dbo].[ACCPCON] c 
	  left join view_arpay2 va on c.CONTNO=va.CONTNO
	  left join wb_armast a on c.CONTNO=a.CONTNO
  --where a.CONTNO='&HP-08100033'
/*
delete from dc_dueDatePostponements
DBCC CHECKIDENT (dc_dueDatePostponements, RESEED, 0)
*/
/*
create view dc_dueDatePostponeCount as
select n.contno,n.delycnt,(n.delycnt+1) nextDelycnt
from dc_dueDatePostponements n inner join (
	select max(id) id, contno from dc_dueDatePostponements
	where deleted='N'
	group by contno	
) mx on n.id=mx.id
*/