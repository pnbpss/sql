IF OBJECT_ID('tempdb..##tempdbname') IS NOT NULL  DROP TABLE ##tempdbname   -- Remoeve "tempdb.dbo"
IF OBJECT_ID('tempdb..##pivotedResult') IS NOT NULL  DROP TABLE ##pivotedResult   -- Remoeve "tempdb.dbo"

declare @specifiedDate datetime;
set @specifiedDate='20170531';

select LOCAT
,substring(convert(varchar,year(YDATE)),3,2)y
,case when month(YDATE) < 10 then '0'+CONVERT(varchar,month(YDATE)) 
	else CONVERT(varchar,month(YDATE)) 
end m
,COUNT(*)cnt 
into ##tempdbname
from wb_arhold 
where YDATE between dateadd(month,-12,@specifiedDate) and @specifiedDate
group by LOCAT,year(YDATE),month(YDATE)

DECLARE @m_y VARCHAR(8000),@sumM_Y VARCHAR(8000),@myInPV VARCHAR(8000);
SELECT @myInPV= ISNULL(@myInPV + ',','')  + ''+ QUOTENAME('Y'+y+'M'+m) from (SELECT DISTINCT y,m FROM ##tempdbname) AS ym
SELECT @m_y= ISNULL(@m_y + ',','')  + 'isnull('+ QUOTENAME('Y'+y+'M'+m)+',0)'+QUOTENAME('Y'+y+'M'+m) from (SELECT DISTINCT y,m FROM ##tempdbname) AS ym
SELECT @sumM_Y= ISNULL(@sumM_Y + '+','')  + 'isnull('+ QUOTENAME('Y'+y+'M'+m)+',0)' from (SELECT DISTINCT y,m FROM ##tempdbname) AS ym

print @myInPV; print @m_y; print @sumM_Y
declare @sql as nvarchar(max);
set @sql = N'
declare  @cntMont int;
set @cntMont = (select count(*) from (select distinct y,m from ##tempdbname) tall)
select LOCAT
,'+@m_y+'
,aggr.sumAll
,aggr.mavg
into ##pivotedResult
from (
	select LOCAT,''Y''+y+''M''+m as m,cnt  from ##tempdbname
) t pivot
(
	sum(cnt) for m in ('+@myInPV+')
) p
cross apply (
	select ('+@sumM_Y+')sumAll
			,('+@sumM_Y+')/@cntMont mavg
) aggr
';

exec sp_executesql @sql;

select * from ##pivotedResult;