use serviceweb;
IF OBJECT_ID('tempdb.dbo.#abcdef', 'U') IS NOT NULL DROP TABLE #abcdef;
IF OBJECT_ID('tempdb.dbo.#ghijkl', 'U') IS NOT NULL DROP TABLE #ghijkl;
select
asj.dcTFSName+'('+convert(varchar(max),asj.collectorId)+')' dcTFSName
,case 			
	when asj.crDisconnectedDays between 0 and 90 then 'R_000TO090'					
	when asj.crDisconnectedDays between 91 and 180 then 'R_091TO180'
	when asj.crDisconnectedDays between 181 and 365 then 'R_181TO365'
	when asj.crDisconnectedDays between 366 and 500 then 'R_366TO500'
	when asj.crDisconnectedDays between 501 and 700 then 'R_501TO700'
	when asj.crDisconnectedDays between 701 and 900 then 'R_701TO900'
	when asj.crDisconnectedDays > 900 then 'R_900UP'
end as ddRange
,asj.crRemainDebt
,asj.crDebtPlusVat
,asj.crPendingPeriod
,asj.crDisconnectedDays
,asj.closed
,seizeBill.NETPAY seizedNetPay
,dcBill.NETPAY dcNetPay
,insBill1.NETPAY insOneNetPay
,insBill2.NETPAY insTwoNetPay
--from dc_beingOnCollectionConts where 1=1 
into #abcdef
from dc_allAssignedJobs asj
	 left join wb_chqtran dcBill on asj.DCBillNo=dcBill.TMBILL and dcBill.PAYFOR='102' and dcBill.FLAG!='C'
	 left join wb_chqtran seizeBill on asj.seizedBillNo=seizeBill.TMBILL and seizeBill.PAYFOR='103' and seizeBill.FLAG!='C'
	 left join wb_chqtran insBill1 on asj.installmentBillNoOne=insBill1.TMBILL and insBill1.PAYFOR in ('006','007') and insBill1.FLAG!='C'
	 left join wb_chqtran insBill2 on asj.installmentBillNoTwo=insBill2.TMBILL and insBill2.PAYFOR in ('006','007') and insBill2.FLAG!='C'
where 1=1 and asj.issuedDate>'2016/04/30'

--select * from #abcdef where dcTFSName like '%����%'

--select dcName.dcTFSName, ddRange.ddRange from (select distinct dcTFSName from #abcdef) dcName,(select distinct ddRange from #abcdef) ddRange 

select main1.*
	,isnull(main2.sumRD,0)sumRD
	,isnull(main2.sumDPV,0)sumDPV
	,isnull(main2.avgRD,0)avgRD
	,isnull(main2.avgDPV,0)avgDPV
	,isnull(main2.avgPP,0)avgPP
	,isnull(main2.avgDD,0)avgDD
	,isnull(main2.cnt,0)cnt
	,isnull(income.sumIncome,0)sumIncome
	,isnull(closeS.cnt,0) closedS 
	,isnull(closeP.cnt,0) closedP 
	,isnull(closeR.cnt,0) closedR
	,isnull(closeC.cnt,0) closedC
	,isnull(closeF.cnt,0) closedF
	,isnull(closeN.cnt,0) closedN
	into #ghijkl
from 
(select dcName.dcTFSName, ddRange.ddRange from (select distinct dcTFSName from #abcdef) dcName,(select distinct ddRange from #abcdef) ddRange) main1
left join
(
	select dcTFSName,ddRange
	,sum(crRemainDebt)sumRD,sum(crDebtPlusVat)sumDPV
	,avg(crRemainDebt)avgRD,avg(crDebtPlusVat)avgDPV
	,avg(crPendingPeriod*1.0)avgPP
	,avg(crDisconnectedDays*1.0)[avgDD]
	,count(*) cnt
	from #abcdef main
	group by dcTFSName ,ddRange
) main2 on main1.dcTFSName=main2.dcTFSName and main1.ddRange=main2.ddRange
left join (	
	select dcTFSName,ddRange
	,sum(isnull(seizedNetPay,0)+isnull(dcNetPay,0)+isnull(insOneNetPay,0)+isnull(insTwoNetPay,0))sumIncome	
	from #abcdef main
	group by dcTFSName ,ddRange
) income on main2.dcTFSName=income.dcTFSName and main2.ddRange=income.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='S'
	group by dcTFSName,ddRange
) closeS on main2.dcTFSName=closeS.dcTFSName and main2.ddRange=closeS.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='P'
	group by dcTFSName,ddRange
) closeP on main2.dcTFSName=closeP.dcTFSName and main2.ddRange=closeP.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='R'
	group by dcTFSName,ddRange
) closeR on main2.dcTFSName=closeR.dcTFSName and main2.ddRange=closeR.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='F'
	group by dcTFSName,ddRange
) closeF on main2.dcTFSName=closeF.dcTFSName and main2.ddRange=closeF.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='C'
	group by dcTFSName,ddRange
) closeC on main2.dcTFSName=closeC.dcTFSName and main2.ddRange=closeC.ddRange
left join (
	select dcTFSName,ddRange
	,count(*) cnt
	from #abcdef main	
	where closed='N'
	group by dcTFSName,ddRange
) closeN on main2.dcTFSName=closeN.dcTFSName and main2.ddRange=closeN.ddRange



select allDc.dcTFSName,allDc.ddRange,a.sumRD,a.sumDPV,a.avgRD,a.avgDPV,a.avgPP,a.avgDD,a.cnt,a.sumIncome
,case when a.sumRD!=0 then a.sumIncome*100/a.sumRD else 0 end pcentInComeRd
,a.closedS,a.closedP,a.closedR,a.closedC,a.closedF,a.closedN
from (select dc.fname+' '+dc.sname+'('+convert(varchar(max),dc.id)+')' dcTFSName, a.ddRange
		from dc_debtCollectors dc,(select distinct ddRange from #ghijkl) a where dc.available='Y') 
	allDc left join #ghijkl a on allDc.dcTFSName=a.dcTFSName and allDc.ddRange=a.ddRange
where 1=1 and a.cnt!=0
union
select allDc.dcTFSName,'���' ddRange,sum(a.sumRD) sumRD
,sum(a.sumDPV)sumDPV,avg(a.avgRD)avgRD,avg(a.avgDPV)avgDPV,avg(a.avgPP)avgPP,avg(a.avgDD)avgDD,sum(a.cnt)cnt,sum(a.sumIncome)sumIncome
,case when sum(a.sumRD)!=0 then sum(a.sumIncome)*100/sum(a.sumRD) else 0 end pcentInComeRd
,sum(a.closedS)closedS,sum(a.closedP)closedP,sum(a.closedR)closedR,sum(a.closedC)closedC,sum(a.closedF)closedF,sum(a.closedN)closedN
from (select dc.fname+' '+dc.sname+'('+convert(varchar(max),dc.id)+')' dcTFSName, a.ddRange
		from dc_debtCollectors dc,(select distinct ddRange from #ghijkl) a where dc.available='Y') 
	allDc left join #ghijkl a on allDc.dcTFSName=a.dcTFSName and allDc.ddRange=a.ddRange
where 1=1 and a.cnt!=0
group by allDc.dcTFSName

order by dcTFSName,ddRange



