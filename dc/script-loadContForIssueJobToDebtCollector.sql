use serviceweb;
SET NOCOUNT ON;
drop table #dc_loadedJobsForAssignToDebtCollector 
select 
--top 300000
--����� ��������ѡ�ͧ�����ѭ��
a.STRNO strno,a.CUSCOD as buyerCuscod,s1.CUSCOD as suretyCuscod1,s2.CUSCOD as suretyCuscod2,s3.CUSCOD as suretyCuscod3
,a.SDATE as sdate,va.installmentPerPeriod installmentPerPeriod
--����������ѡ�ͧ�����ѭ��
,convert(varchar,getdate(),111) as [loadDate],'1398' [loadBy]
,a.CONTNO [contno],a.LOCAT [locat],a.CONTSTAT [crContStatus]
,(a.TOTPRC-a.SMPAY) crDebtPlusVat --Ź.�������
,va.netPendingPeriod [crPendingPeriod]
,a.EXP_AMT [crRemainDebt] --�ʹ��ҧ	
,case
	when isnull(va.currentperiod,0)=0 then 0
	when va.neverpaid='Y' then 1
	when va.netPendingPeriod=0 then 0
	else va.currentperiod-(va.netPendingPeriod-1)
	end as crPendingFromPeriod --,[crPendingFromPeriod]	
,isnull(va.currentperiod,0) [crPendingToPeriod]
,va.disconnectedDays [crDisconnectedDays]
,pp.LACCPDT as [postponeDueTo]
,va.lastPaidDate [crLastPaidDate]
,null [lastUpdateBy],null [lastUpdateDate]
into #dc_loadedJobsForAssignToDebtCollector 
from wb_armast a 
	left join View_Arpay va on a.CONTNO=va.CONTNO
	left join wb_invtran v on a.STRNO=v.STRNO
	left join wb_surety1 s1 on a.CONTNO=s1.CONTNO
	left join wb_surety2 s2 on a.CONTNO=s2.CONTNO
	left join wb_surety3 s3 on a.CONTNO=s3.CONTNO
	left join dc_onPostponePayContracts pp on a.CONTNO=pp.CONTNO
where (a.TOTPRC-a.SMPAY <> 0) 
	  and 
	  (
		  (va.netPendingPeriod>=3 and disconnectedDays>=15 and va.lastPaidDate is not null)
		  or
		  (va.netPendingPeriod>=3 and disconnectedDays between 1 and 15 and va.lastPaidDate is null)
	  )
	--and a.LOCAT='YK��1'
--order by a.LOCAT

--��Ң����������ҧ��ѡ dc_loadedContForCollection �����Ţ����ѭ���繵�Ǥ��
insert into dc_loadedContForCollection (
	contno,strno,locat,buyerCuscod,suretyCuscod1,suretyCuscod2,suretyCuscod3,sdate,installmentPerperiod,statusId,firstLoadBy,firstLoadSince
)
select a.contno,a.strno,a.locat,a.buyerCuscod,a.suretyCuscod1,a.suretyCuscod2,a.suretyCuscod3,a.sdate
,a.installmentPerperiod,100 statusId,'1398' as firstLoadBy
,convert(varchar,getdate(),111) as firstLoadSince
from #dc_loadedJobsForAssignToDebtCollector a 
left join dc_loadedContForCollection b on a.contno=b.contno
where b.contno is null
--select * from dc_loadedContForCollection

--��Ң����������ҧ ��è��§ҹ��餹��� ����� branchJobId �� 0 �� update field ����������������仨��§ҹ��餹�������
insert into dc_loadedJobsForAssignToDebtCollector(
		[jobBranchId],[loadDate],[loadBy],[contno]
		,[crContStatus],[crDebtPlusVat] --Ź.�������
		,[crPendingPeriod],[crRemainDebt] --�ʹ��ҧ
		,[crPendingFromPeriod],[crPendingToPeriod],[crDisconnectedDays]
		,[crLastPaidDate],[postponeDueTo],[lastUpdateBy],[lastUpdateDate]
) select 
		0 as jobBranchId
		,[loadDate],[loadBy],[contno],[crContStatus]
		,[crDebtPlusVat] --Ź.�������
		,[crPendingPeriod],[crRemainDebt] --�ʹ��ҧ
		,[crPendingFromPeriod],[crPendingToPeriod],[crDisconnectedDays]
		,[crLastPaidDate],[postponeDueTo],[lastUpdateBy],[lastUpdateDate]
from #dc_loadedJobsForAssignToDebtCollector a

/*
declare @id int,@nextJobBranchId int,@maxJobBranchId int,@locat varchar(max);
declare branchIdCursor cursor for 
select b.locat,max(jobBranchId)
from dc_loadedJobsForAssignToDebtCollector a 
inner join dc_loadedContForCollection b on a.contno=b.contno
group by b.locat
open branchIdCursor
FETCH NEXT FROM branchIdCursor 
INTO @locat,@maxJobBranchId
WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE cr1 CURSOR FOR 
			select a.id 
			from dc_loadedJobsForAssignToDebtCollector a 
				inner join dc_loadedContForCollection b on a.contno=b.contno
			where a.jobBranchId='0' and b.locat=@locat
			--order by a.id;
			set @nextJobBranchId = @maxJobBranchId; --��Ҥ���٧�ش�ͧ jobId �ͧ�����Ң��͡��
			OPEN cr1
			FETCH NEXT FROM cr1 
			INTO @id
			WHILE @@FETCH_STATUS = 0
			BEGIN
				/*
				set @nextJobBranchId = (
					select MAX(a.jobBranchId) 
					from dc_loadedJobsForAssignToDebtCollector a 
					inner join dc_loadedContForCollection b on a.contno=b.contno
					where b.locat=@locat
				);
				*/				
				set @nextJobBranchId = @nextJobBranchId + 1;
				update dc_loadedJobsForAssignToDebtCollector set jobBranchId=@nextJobBranchId where id=@id;
				FETCH NEXT FROM cr1 
				INTO @id
			END 
			CLOSE cr1;
			DEALLOCATE cr1;
		FETCH NEXT FROM branchIdCursor
		INTO @locat,@maxJobBranchId
	END
close branchIdCursor
deallocate branchIdCursor

select c.CONTDESC,b.installmentPerperiod,b.installmentPerperiod*a.crPendingPeriod,a.* 
,datediff(year,a.crLastPaidDate,getdate()) as [years]
from dc_loadedJobsForAssignToDebtCollector a left join dc_loadedContForCollection b on a.contno=b.contno
left join [HIINCOME].[dbo].[TYPCONT] c on a.crContStatus=c.CONTTYP
where a.crDisconnectedDays > 1000
order by a.crDisconnectedDays

select count(*),datediff(year,a.crLastPaidDate,getdate()) as [years]
from dc_loadedJobsForAssignToDebtCollector a left join dc_loadedContForCollection b on a.contno=b.contno
--where a.crDisconnectedDays > 1000
group by datediff(year,a.crLastPaidDate,getdate())
order by datediff(year,a.crLastPaidDate,getdate())


select * from dc_loadedContForCollection;
*/
/*
--clear �������������
delete from dc_loadedJobsForAssignToDebtCollector;
delete from dc_loadedContForCollection
DBCC CHECKIDENT (dc_loadedJobsForAssignToDebtCollector, RESEED, 0)
*/