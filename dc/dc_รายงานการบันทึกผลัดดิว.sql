--update dc_dueDatePostponements set userid=apprvby where additionalInfo='##FT##'
--update dc_dueDatePostponements set inpdt=recvdt where inpdt is null
--delete from dc_dueDatePostponements where userid='1398' and inpdt<'20160401'
--select distinct a.custRequestVia from dc_dueDatePostponements a where a.userid!='1398'
/*
select DATEDIFF(day,inpdt,laccpdt)ddINP_PP2 ,DATEDIFF(day,crLastPaidDate,laccpdt)ddPP2_LPD,*  
from dc_dueDatePostponements 
where additionalInfo!='##FT##' and userid!='admin'
--cross apply (select count(*) dM30days from dc_dueDatePostponements where apprvby=ddp.apprvby) as ppMoreThan30Days(val1)

alter view 
dc_dueDatePostponementForReport
as select * from dc_dueDatePostponements ddp
where 
ddp.userid!='admin'
and ddp.inpdt>'20160331'
--and additionalInfo!='##FT##'
and DATEDIFF(day,inpdt,laccpdt) > 0 and DATEDIFF(day,crLastPaidDate,laccpdt) > 0 
and deleted='N'

*/
select 
'('+hu.USERID+') '+hu.USERNAME+','+hu.LOCAT userInfo
,cnt
,min_ddINP_PP2
,avg_ddINP_PP2
,max_ddINP_PP2
,min_ddPP2_LPD
,avg_ddPP2_LPD
,max_PP2_LPD
,PLT_07
,P07_15
,P16_30
,P31_60
,PMoreThan60
,PLT_07A
,P07_15A
,P16_30A
,P31_60A
,PMoreThan60A
,(PLT_07A+P07_15A+P16_30A+P31_60A+PMoreThan60A) sumActive
	from (
	select apprvby,COUNT(*) cnt
	,MIN(DATEDIFF(day,inpdt,laccpdt))min_ddINP_PP2 -- ���·���ش(������ҧ�ҡ �ѹ���ѹ�֡-�Ѵ��Ѵ���)
	,AVG(DATEDIFF(day,inpdt,laccpdt)*1.0)avg_ddINP_PP2 -- �����(������ҧ�ҡ �ѹ���ѹ�֡-�Ѵ��Ѵ���)
	,MAX(DATEDIFF(day,inpdt,laccpdt))max_ddINP_PP2 -- �ҡ����ش����ش(������ҧ�ҡ �ѹ���ѹ�֡-�Ѵ��Ѵ���)
	,STDEV(DATEDIFF(day,inpdt,laccpdt)) stdev_ddINP_PP2 -- ������§ູ�ҵðҹ(������ҧ�ҡ �ѹ���ѹ�֡-�Ѵ��Ѵ���)
	,MIN(DATEDIFF(day,crLastPaidDate,laccpdt))min_ddPP2_LPD -- ���·���ش(������ҧ�ҡ �ѹ����������ش-�Ѵ��Ѵ���)
	,AVG(DATEDIFF(day,crLastPaidDate,laccpdt)*1.0)avg_ddPP2_LPD -- �����(������ҧ�ҡ �ѹ����������ش-�Ѵ��Ѵ���)
	,MAX(DATEDIFF(day,crLastPaidDate,laccpdt))max_PP2_LPD -- �ҡ����ش(������ҧ�ҡ �ѹ����������ش-�Ѵ��Ѵ���)
	,STDEV(DATEDIFF(day,crLastPaidDate,laccpdt)) stdev_PP2_LPD -- ������§ູ�ҵðҹ(������ҧ�ҡ �ѹ����������ش-�Ѵ��Ѵ���)
	from dc_dueDatePostponementForReport ddp
	where 1=1
	--and ddp.inpdt between '20160501' and getdate()
	group by apprvby
	--order by COUNT(*) desc
) as upp
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)<7
		) as pplestThan07(PLT_07)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=7 and DATEDIFF(day,inpdt,laccpdt)<=15
		) as pp07_15(P07_15)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=16 and DATEDIFF(day,inpdt,laccpdt)<=30
		) as pp16_30(P16_30)		
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=31 and DATEDIFF(day,inpdt,laccpdt)<=60
		) as pp31_60(P31_60)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>60 
		) as ppMorthan_60(PMoreThan60)

---------

cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)<7 and laccpdt>getdate()
		) as pplestThan07A(PLT_07A)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=7 and DATEDIFF(day,inpdt,laccpdt)<=15 and laccpdt>getdate()
		) as pp07_15A(P07_15A)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=16 and DATEDIFF(day,inpdt,laccpdt)<=30 and laccpdt>getdate()
		) as pp16_30A(P16_30A)		
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>=31 and DATEDIFF(day,inpdt,laccpdt)<=60 and laccpdt>getdate()
		) as pp31_60A(P31_60A)
  cross apply (
		 select count(*) dM30days from dc_dueDatePostponementForReport where apprvby=upp.apprvby and DATEDIFF(day,inpdt,laccpdt)>60 and laccpdt>getdate()
		) as ppMorthan_60A(PMoreThan60A)
  left join dc_hiincomeuserAll hu on apprvby=hu.USERID
order by cnt desc


--select * from dc_dueDatePostponementForReport where apprvby='3044'
select id,contno
,dbo.FN_ConvertDate(laccpdt,'D/M/Y') laccpdt
,authrby
,delycnt
,dbo.FN_ConvertDate(inpdt,'D/M/Y') inpdt
,crContStatus,crDebtPlusVat,crPendingPeriod
,crRemainDebt,crPendingFromPeriod
,crPendingToPeriod
,dbo.FN_ConvertDate(crDueDate,'D/M/Y') crDueDate
,crDisconnectedDays
,dbo.FN_ConvertDate(crLastPaidDate,'D/M/Y') crLastPaidDate
,custRequestVia,additionalInfo,deleted
,deletedBy
,dbo.FN_ConvertDate(deletedSince,'D/M/Y') deletedSince
,telephoningId
,cntAfterPaid 
from dc_dueDatePostponements where apprvby='3044'
--order by contno,id desc
order by custRequestVia desc