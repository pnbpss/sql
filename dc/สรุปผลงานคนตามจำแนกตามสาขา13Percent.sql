--ź��è��§ҹ���
--select count(*), contno from dc_beingOnCollectionConts group by contno having COUNT(*)>1
--select * from dc_branchOfCollectors a, dc_debtCollectors b,wb_invlocat i  where a.rate!='normal' and a.personelId=b.id and a.branchId=i.LOCATCD;
--select * from wb_payfor;
/*
alter view dc_allAssignedJobs as 
select irj.*,ljf.contno,ljf.loadDate,lcc.locat,boc.rate from 
dc_issuedReturnedJobFromCollectors irj 
left join dc_loadedJobsForAssignToDebtCollector ljf on irj.jobId=ljf.id
left join dc_loadedContForCollection lcc on lcc.contno=ljf.contno
left join dc_branchOfCollectors boc on irj.collectorId=boc.personelId and lcc.locat=boc.branchId
where irj.deleted='N'
*/
declare @collectorId int,@month int,@year int,@firstDayOfThisMonth varchar(max);
set @month=5;set @year=2016; set @firstDayOfThisMonth = convert(varchar(4),@year)+'/'+convert(varchar(2),@month)+'/01';
set @collectorId='43';
select * from (
	select b.branchId
		,( --�ҹ�����¡�͹��͹����ѧ��診
				(select count(*) from dc_allAssignedJobs irj 
				where 1=1 
				and irj.deleted='N' and irj.closed='N' 
				and irj.issuedDate<@firstDayOfThisMonth
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
				)
				+( --�ҹ�����¡�͹��͹�����Ш����͹���
				select count(*) from dc_allAssignedJobs irj 
				where 1=1 
				and irj.deleted='N' and irj.closed!='N'
				and irj.issuedDate<@firstDayOfThisMonth 
				and month(irj.closedDate)=@month and year(irj.closedDate)=@year
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13')		
			) bringForward_C
			,( --��������
				select count(*) from dc_allAssignedJobs irj 
				where 1=1 
				and irj.deleted='N' 
				and month(irj.issuedDate)=@month and year(irj.issuedDate)=@year
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
			) issuedInThisMonth_C
			,(
			 select count(*) --���л���
			from dc_allAssignedJobs irj 			
				left join wb_chqtran chq on irj.installmentBillNoOne=chq.TMBILL	
			where 1=1 
				and irj.deleted='N' and irj.closed='P'			
				and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
				and chq.TMBILL is not null
				and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
			) paid_P_C
			,(
			select count(*) --���Сó��ִ�����١����Ѻ�׹��Ф�����ͤ�ҵͺ᷹�繺ҷ
			from dc_allAssignedJobs irj 			
				left join wb_chqtran chq on irj.installmentBillNoOne=chq.TMBILL	
			where 1=1 
				and irj.deleted='N' and irj.closed='R' 
				and irj.seizedCompensationType='B' --���Ѻ��ҵͺ᷹�繺ҷ
				and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
				and chq.TMBILL is not null
				and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
			
			) seized_Rb_C
			,(
				(
				select ISNULL(sum(chq.NETPAY),0) --���л��Ժ��1
				from dc_allAssignedJobs irj 			
					left join wb_chqtran chq on irj.installmentBillNoOne=chq.TMBILL	
				where 1=1 
					and irj.deleted='N' and irj.closed='P'			
					and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
					and chq.TMBILL is not null
					and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
					and irj.collectorId=@collectorId
					and irj.locat=b.branchId
					and irj.rate='13'
				)
				+
				(
				select ISNULL(sum(chq.NETPAY),0) --���л��Ժ��2
				from dc_allAssignedJobs irj 			
					left join wb_chqtran chq on irj.installmentBillNoTwo=chq.TMBILL	
				where 1=1 
					and irj.deleted='N' and irj.closed='P'			
					and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
					and chq.TMBILL is not null
					and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
					and irj.collectorId=@collectorId
					and irj.locat=b.branchId
					and irj.rate='13'
				)
				+
				(select isnull(sum(chq.NETPAY),0) --���Сó��ִ�����١����Ѻ�׹��Ф�����ͤ�ҵͺ᷹�繺ҷ
				from dc_allAssignedJobs irj 			
					left join wb_chqtran chq on irj.installmentBillNoOne=chq.TMBILL	
				where 1=1 
					and irj.deleted='N' and irj.closed='R' 
					and irj.seizedCompensationType='B' --���Ѻ��ҵͺ᷹�繺ҷ
					and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
					and chq.TMBILL is not null
					and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
					and irj.collectorId=@collectorId
					and irj.locat=b.branchId
					and irj.rate='13'
					)
			) [billOneAndTwo_P_and_Rb_S]
			,(
				select count(*) cntJob --�ִ����
				from dc_allAssignedJobs irj 	
					left join dc_loadedJobsForAssignToDebtCollector ljf on irj.jobId=ljf.id
					left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
					left join wb_arhold ah on lcc.contno=ah.CONTNO
				where 1=1 
					and irj.deleted='N' and irj.closed='S'
					and month(ah.YDATE)=@month and year(ah.YDATE)=@year
					and irj.collectorId=@collectorId
					and irj.locat=b.branchId
					and irj.rate='13'
			) seized_S_C
			,(
			select count(*) cntJob --�ִ�����١����Ѻ�׹��Ф�����ͤ�ҵͺ᷹�繤ѹ
			from dc_allAssignedJobs irj 	
				left join dc_loadedJobsForAssignToDebtCollector ljf on irj.jobId=ljf.id
				left join dc_loadedContForCollection lcc on ljf.contno=lcc.contno
				left join wb_arhold ah on lcc.contno=ah.CONTNO
			where 1=1 
				and irj.deleted='N' and irj.closed='R' 
				and irj.seizedCompensationType='V' --���Ѻ��ҵͺ᷹�繤ѹ						
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
			) [seized_Rv_C]			
			,(
			select isnull(sum(chq.NETPAY),0) --���Сó��ִ�����١����Ѻ�׹��Ф�����ͤ�ҵͺ᷹�繺ҷ
			from dc_allAssignedJobs irj 			
				left join wb_chqtran chq on irj.installmentBillNoOne=chq.TMBILL	
			where 1=1 
				and irj.deleted='N' and irj.closed='R' 
				and irj.seizedCompensationType='B' --���Ѻ��ҵͺ᷹�繺ҷ
				and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year
				and chq.TMBILL is not null
				and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
				and irj.collectorId=@collectorId
				and irj.locat=b.branchId
				and irj.rate='13'
			)[seized_Rb_S]
	from dc_branches b
) as a
where (a.bringForward_C+issuedInThisMonth_C)<>0


