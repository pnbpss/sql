
use serviceweb;
declare @month int,@day int, @year int;
set @month=7;set @day=30;set @year=2016;
select chq.locat,chq.y,chq.m,chq.d,chq.amt,dueOnDay.amtOnDue
from 
(
	select year(chq.TMBILDT)y,month(chq.TMBILDT)m,day(chq.TMBILDT)d,sum(chq.NETPAY)amt,chq.LOCATPAY locat
	from HIC4REPORT.dbo.CHQTRAN chq
	where 1=1
	and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
	and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year and day(chq.TMBILDT)between 1 and @day
	group by year(chq.TMBILDT),month(chq.TMBILDT),day(chq.TMBILDT),chq.LOCATPAY
)
 chq
left join (
	select sum(arpay.DAMT)amtOnDue,month(arpay.DDATE)m,day(arpay.DDATE)d,year(arpay.DDATE)y,arpay.LOCAT locat
	from HIC4REPORT.dbo.ARPAY arpay
	where month(arpay.DDATE)=@month and year(arpay.DDATE)=@year and day(arpay.DDATE)between 1 and @day
	group by month(arpay.DDATE),day(arpay.DDATE),year(arpay.DDATE),arpay.LOCAT 	
) dueOnDay on chq.y=dueOnDay.y and chq.d=dueOnDay.d and chq.m=dueOnDay.m and chq.locat=dueOnDay.locat
order by chq.y desc, chq.m desc, chq.d desc;

/*
select top 5 * from HIC2REPORT.CHQTRAN
select mxDate1.d1,* from view_arpay2 va left join 
(
	select max(DATE1) d1,CONTNO from HIC2REPORT.ARPAY group by CONTNO
) mxDate1 on va.CONTNO=mxDate1.CONTNO
where mxDate1.d1>va.lastPaidDate
and va.CONTNO='�HP-14060022'
order by mxDate1.d1 desc
select top 5000 * from HIC2REPORT.ARPAY order by CONTNO,NOPAY


select sum(arpay.DAMT)amtOnDue,month(arpay.DDATE)m,day(arpay.DDATE)d,year(arpay.DDATE)y,arpay.LOCAT locat
	from HIC2REPORT.ARPAY arpay
	where month(arpay.DDATE)=7 and year(arpay.DDATE)=2016 and day(arpay.DDATE)=30
	group by month(arpay.DDATE),day(arpay.DDATE),year(arpay.DDATE),arpay.LOCAT 
	order by arpay.LOCAT,year(arpay.DDATE),month(arpay.DDATE),day(arpay.DDATE)


*/