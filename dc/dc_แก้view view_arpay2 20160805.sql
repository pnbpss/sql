alter view view_arpay2 as
SELECT     A.CONTNO, A.LOCAT, A.startContract, A.endContract, B.lastPaidDate, E.installmentPerPeriod, F.lastPaidInstallmentPeriod, B.lastPaidInstall, A.AllPeriod, 
                      D.currentperiod
                      
                      ,CASE 
						WHEN lastPaidDate IS NOT NULL THEN 
							CASE WHEN lastPaidDate < getdate() THEN DATEDIFF(DAY, lastPaidDate, getdate()) 
							ELSE 0 END 
								ELSE 
									CASE WHEN NoAllPayDate < getdate() THEN DATEDIFF(DAY, NoAllPayDate, getdate()) 
										ELSE 0 
									END 
						END AS disconnectedDays, 
                      ISNULL(C.pendingInstallmentPeriod, 0) AS pendingInstallmentPeriod
                      , CASE 
						/*������÷Ѵ���*/
						WHEN B.lastPaidInstall = 0 then pendingInstallmentPeriod
						WHEN ((E.installmentPerPeriod / B.lastPaidInstall > 2) AND (D .currentperiod >= B.lastNoPay)) THEN ISNULL(C.pendingInstallmentPeriod, 0) + 1 
						ELSE 
							CASE WHEN (F.lastPaidInstallmentPeriod = A.AllPeriod) AND (A.endContract < getdate()) AND (B.lastPaidInstall < E.installmentPerPeriod) THEN ISNULL(C.pendingInstallmentPeriod, 0) + 1 
							ELSE ISNULL(C.pendingInstallmentPeriod, 0) END 
						END AS netPendingPeriod
                      , CASE WHEN endContract < GETDATE() 
                      THEN 'Y' ELSE 'N' END AS lastpassdue, CASE WHEN lastPaidDate IS NULL THEN 'Y' ELSE 'N' END AS neverpaid, G.nextDealDate
					FROM         (SELECT     MIN(DDATE) AS startContract, MAX(DDATE) AS endContract, CONTNO, LOCAT, COUNT(NOPAY) AS AllPeriod
                       FROM          HIINCOME.dbo.ARPAY WITH (NOLOCK)
                       GROUP BY CONTNO, LOCAT) AS A LEFT OUTER JOIN
                          (
							SELECT     lnp.CONTNO, lnp.lastNoPay, arp.DATE1 AS lastPaidDate, arp.PAYMENT AS lastPaidInstall
                            FROM          (SELECT     CONTNO, MAX(NOPAY) AS lastNoPay
                                                    FROM          dbo.wb_arpay AS ARPAY_5
                                                    --WHERE      (PAYMENT > 0)����¹�ҡ��÷Ѵ����繺�÷Ѵ��ҧ��ҧ
                                                    WHERE      (DATE1 is not null)
                                                    GROUP BY CONTNO) AS lnp LEFT OUTER JOIN
                                                   dbo.wb_arpay AS arp ON lnp.CONTNO = arp.CONTNO AND lnp.lastNoPay = arp.NOPAY
                           ) AS B ON 
                      A.CONTNO = B.CONTNO LEFT OUTER JOIN
                          (SELECT     MIN(DDATE) AS LastDateMustCotact, COUNT(NOPAY) AS pendingInstallmentPeriod, CONTNO
                            FROM          HIINCOME.dbo.ARPAY AS ARPAY_4 WITH (NOLOCK)
                            WHERE      (PAYMENT = 0) AND (DDATE <= GETDATE())
                            GROUP BY CONTNO) AS C ON A.CONTNO = C.CONTNO LEFT OUTER JOIN
                          (SELECT     CONTNO, MAX(NOPAY) AS currentperiod, MAX(DDATE) AS MaxDate
                            FROM          HIINCOME.dbo.ARPAY AS ARPAY_3
                            WHERE      (DDATE <= GETDATE())
                            GROUP BY CONTNO) AS D ON A.CONTNO = D.CONTNO LEFT OUTER JOIN
                          (SELECT     CONTNO, MAX(DAMT) AS installmentPerPeriod, MAX(DDATE) AS NoAllPayDate
                            FROM          HIINCOME.dbo.ARPAY AS ARPAY_2
                            WHERE      (NOPAY = 1)
                            GROUP BY CONTNO) AS E ON A.CONTNO = E.CONTNO LEFT OUTER JOIN
                          (SELECT     MAX(NOPAY) AS lastPaidInstallmentPeriod, CONTNO
                            FROM          HIINCOME.dbo.ARPAY AS ARPAY_1 WITH (NOLOCK)
                            WHERE      (DATE1 IS NOT NULL) AND (PAYMENT <> 0)
                            GROUP BY CONTNO) AS F ON A.CONTNO = F.CONTNO LEFT OUTER JOIN
                          (SELECT     MIN(DDATE) AS nextDealDate, CONTNO
                            FROM          HIINCOME.dbo.ARPAY AS ARPAY_1 WITH (NOLOCK)
                            WHERE      (DDATE >= GETDATE())
                            GROUP BY CONTNO) AS G ON A.CONTNO = G.CONTNO
where 1=1
--and A.CONTNO='�HP-14060022'
--and lastPaidInstall = 0
--order by lastPaidDate desc