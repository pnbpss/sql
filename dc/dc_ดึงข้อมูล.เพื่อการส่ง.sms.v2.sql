use serviceweb;

--drop table #T20160225_tempSMSData
select		
left(dbo.fnRemoveNonNumericCharacters(c.TELP),10) PhoneNumber
,''
+'คุณค้างชำระ '
+ 
	replace(CONVERT(varchar, CAST((SELECT isnull(SUM(DAMT-PAYMENT),0) FROM hiincome.dbo.ARPAY WHERE CONTNO=v.CONTNO and DDATE<=getdate()) AS money), 1),'.00','')
+'บ. ให้ชำระก่อนวันที่ ' + case 
		when netPendingPeriod = 2 then 
			case 
				when disconnectedDays between 21 and 50 then dbo.FN_ConvertDate(dateadd(day,(5+1),getdate()),'D/T/M')
				when disconnectedDays between 0 and 20 then dbo.FN_ConvertDate(dateadd(day,(10+1),getdate()),'D/T/M')
				else '(เงื่อนไขไม่ครอบคลุม)'
			end		
		when netPendingPeriod >= 3 then 
			case 
				when disconnectedDays between 0 and 7 then dbo.FN_ConvertDate(dateadd(day,(7+1),getdate()),'D/T/M')
				else '(เงื่อนไขไม่ครอบคลุม)'
			end
		when netPendingPeriod = 1 then 
			case
				when disconnectedDays between 31 and 50 then dbo.FN_ConvertDate(dateadd(day,(10+1),getdate()),'D/T/M') 
				when disconnectedDays between 0 and 30 then dbo.FN_ConvertDate(dateadd(day,(15+1),getdate()),'D/T/M')				
				else '(เงื่อนไขไม่ครอบคลุม)'
			end		
	end +' ขออภัยหากชำระแล้ว' as SMS
,case 
		when netPendingPeriod = 2 then 
			case 
				when disconnectedDays between 21 and 50 then '1'
				when disconnectedDays between 0 and 20 then '2'
				else '(เงื่อนไขไม่ครอบคลุม)'
			end		
		when netPendingPeriod >= 3 then 
			case 
				when disconnectedDays between 0 and 7 then '3'
				else '(เงื่อนไขไม่ครอบคลุม)'
			end
		when netPendingPeriod = 1 then 
			case
				when disconnectedDays between 31 and 50 then '4'
				when disconnectedDays between 0 and 30 then '5'
				else '(เงื่อนไขไม่ครอบคลุม)'
			end		
	end [pendingCase]	
,c.TELP seniorTELP
,case
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),1)<>'0' then 'เบอร์ผิดพลาด'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='07' then 'เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='02' then 'เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='03' then 'เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='04' then 'เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='05' then 'เบอร์บ้าน'
	when len(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10))!=10 then 'เบอร์ผิดพลาด'
	when left(TELP,1)='T' then 'เบอร์ติดต่อไม่ได้'
	when isnull(TELP,'') = '' then 'ไม่มีเบอร์'
	else 'OK'
end as numberOk,v.LOCAT,v.CONTNO,(SELECT SUM(DAMT-PAYMENT) FROM hiincome.dbo.ARPAY WHERE CONTNO=v.CONTNO and DDATE<=getdate()) pendingInstallment
,v.pendingInstallmentPeriod,v.netPendingPeriod,((a.EXP_TO-a.EXP_FRM)+1) snPending
,dbo.FN_ConvertDate(v.lastPaidDate,'Y/M/D') lastPaidDate
,dbo.FN_ConvertDate(v.nextDealDate,'Y/M/D') nextDealDate
,v.disconnectedDays,v.installmentPerPeriod,v.lastPaidInstall
,dbo.FN_ConvertDate(a.SDATE,'Y/M/D') sdate
--into #T20160225_tempSMSData
from view_arpay2 v 
	 left join wb_armast a on v.CONTNO=a.CONTNO
	 left join wb_customers c on a.CUSCOD=c.CUSCOD
where 
	1=1 
	and (
			(v.netPendingPeriod=2 and disconnectedDays between 0 and 50) 
			or 
			(v.netPendingPeriod>=3 and disconnectedDays between 0 and 7)
			or 
			(v.netPendingPeriod=1 and disconnectedDays between 0 and 50)
		) 
	and year(a.SDATE) > (year(getdate()) - 5)
	and a.LOCAT not in ('OFFตจ','OFFตท','OFFทน','OFFยน')
--	and ((a.EXP_TO-a.EXP_FRM)+1)<v.netPendingPeriod
order by a.CONTNO;

--SELECT   o.Name, c.Name+',' FROM     sys.columns c JOIN sys.objects o ON o.object_id = c.object_id WHERE    o.type = 'U' and o.Name='T20160225_tempSMSData' ORDER BY o.Name, c.Name
/*
select LOCAT,CONTNO 'เลขที่สัญญา'
,sdate 'วันทำสัญญา'
,isnull(lastPaidDate,'ไม่เคยจ่าย') 'วันที่ชำระล่าสุด'
,installmentPerPeriod 'ชำระงวดละตามสัญญา(บาท)'
,isnull(lastPaidInstall,'0') 'ยอดชำระงวดล่าสุด'
,disconnectedDays 'ขาดการติดต่อ(วัน)'
,isnull(nextDealDate,'เลยมาแล้ว') 'วันดิวถัดไป' 
,pendingInstallment 'ยอดค้าง(บาท)'
,snPending 'งวดค้างในซีเนียร์(งวด)'
,pendingInstallmentPeriod 'จำนวนงวดที่ชำระไม่เกินครึ่ง(งวด)'
,netPendingPeriod 'งวดค้างสุทธิ(งวด)'
,seniorTELP 'เบอร์ที่บันทึกในซีเนียร์'
,numberOk 'เบอร์โอคไหม?'
,pendingCase
,SMS 'วันกำหนดชำระ' 
,PhoneNumber 'MobilePhoneNumber'
from #T20160225_tempSMSData
where numberOk='OK' 
order by pendingCase,numberOk,pendingInstallment,PhoneNumber desc

select [pendingCase], numberOk,count(*) cnt
from dc_tempForSMS
group by  [pendingCase], numberOk
order by [pendingCase],numberOk

*/


--select * from #T20160225_tempSMSData; select max(len(txt1+txt2+txt3)) from #T20160225_tempSMSData

