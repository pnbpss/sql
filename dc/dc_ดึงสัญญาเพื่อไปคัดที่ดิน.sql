select 
			top 250
			cm.IDNO,cm.MEMBCOD cMembcod,cm.CUSCOD,cont.LOCAT,year(ar.SDATE)ycont,cont.CONTNO,cm.GRADE,cm.GROUP1,datediff(year,cm.BIRTHDT,getdate()) cage,ca.ADDR1 caAddr1,ca.ADDR2 caAddr2,ca.TUMB caTumb,campr.AUMPDES caAumpdes,campr.PROVDES caProvdes,ca.ZIP as caZip,cm.SNAM,cm.NAME1,cm.NAME2,ca.TELP as custTelNo

			,s1.IDNO sIDNO,s1.fullName sFullname,s1.MEMBCOD sMembcod,s1.CUSCOD sCUSCOD,datediff(year,s1.BIRTHDT,getdate()) sage,s1a.ADDR1 s1Addr1,s1a.ADDR2 s1Addr2,s1a.TUMB s1Tumb,s1ampr.AUMPDES s1Aumpdes,s1ampr.PROVDES s1aProvdes,s1a.ZIP as s1aZip,s1a.TELP as s1TelNo
			
			,ar.CONTSTAT,i.STRNO,i.ENGNO,i.GCODE,i.TYPE,i.MODEL,i.STAT,convert(varchar,ar.SDATE,111) SDATE,ar.STDPRC,ar.TOTDWN,cont.AllPeriod,ar.TOTPRC,(ar.TOTPRC-ar.STDPRC) profit,cont.installmentPerPeriod,ar.TOTDWN TOTDWN2,cal.netPaidInstall,(cal.netPaidInstall+ar.TOTDWN) netPaidInstallPlusDown,(ar.TOTPRC-cal.netPaidInstall) remainDebt,cont.netPendingPeriod
			,case when cont.netPendingPeriod=0 then 0 
				else case 
						when cont.netPendingPeriod=cont.pendingInstallmentPeriod then cont.netPendingPeriod*cont.installmentPerPeriod
						when cont.netPendingPeriod>cont.pendingInstallmentPeriod then (cont.netPendingPeriod*cont.installmentPerPeriod)-isnull(cont.lastPaidInstall,0)
					 end
			end remainPending
			,cont.lastPaidInstallmentPeriod
			,case when cont.netPendingPeriod=0 then 0 else (cont.currentperiod-cont.netPendingPeriod)+1 end pendingFromPeriod
			,case when cont.netPendingPeriod=0 then 0 else cont.currentperiod end pendingToPeriod
			,convert(varchar,cont.lastPaidDate,111) lastPaidDate
			,convert(varchar,mxDueDatePaid.mxDueDatePaid,111) mxDueDatePaid
			,cont.disconnectedDays
			,case when cont.neverpaid='Y' then '����͹��͹���' else '��͹��ҧ����' end neverpaid

			from 
			view_arpay2 cont
			inner join wb_armast ar on cont.CONTNO=ar.CONTNO
			inner join wb_invtran i on ar.CONTNO=i.CONTNO
			inner join wb_custmast cm on ar.CUSCOD=cm.CUSCOD
			inner join wb_custaddr ca on cm.CUSCOD=ca.CUSCOD and cm.ADDRNO=ca.ADDRNO
			inner join wb_ampr campr on ca.AUMPCOD=campr.AUMPCOD
			left join wb_surety1 s1 on cont.CONTNO=s1.CONTNO 
					left join wb_custaddr s1a on s1.CUSCOD=s1a.CUSCOD and s1.ADDRNO=s1a.ADDRNO
					left join wb_ampr s1ampr on s1a.AUMPCOD=s1ampr.AUMPCOD

			cross apply (select isnull(((cont.installmentPerPeriod*(cont.lastPaidInstallmentPeriod-1))+cont.lastPaidInstall),0) netPaidInstall) cal
			cross apply (select MAX(DDATE) mxDueDatePaid from wb_arpay where CONTNO=cont.CONTNO and DATE1 is not null) mxDueDatePaid

			where 1=1