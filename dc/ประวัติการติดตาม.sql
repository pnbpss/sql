SELECT		
ljf.id as jobId
,cjp.assignedId
,dbo.FN_ConvertDate(ljf.loadDate,'D/M/Y') loadDate
,dbo.FN_ConvertDate(cjp.presentDate,'D/M/Y') presentDate			
,dbo.FN_ConvertDate(cjp.appointmentToPayDate,'D/M/Y') appointmentToPayDate
,dbo.FN_ConvertDate(cjp.contactedDate,'D/M/Y') contactedDate
,cjp.postponementId
,cjp.resultId
,cjp.comments
,cjp.NBrelatedToCustomer
,cjp.NBName
,cjp.returnedPOA
,der.[name] as enqueryResultText
,dbo.FN_ConvertDate(getPOABackDate,'D/M/Y') getPOABackDate
,dr.[description] dcResult
,dbo.FN_ConvertDate(ddp.laccpdt,'D/M/Y') laccpdt
,'|'
,irjsts.*
FROM dbo.dc_loadedJobsForAssignToDebtCollector ljf 
	 left join dc_issuedReturnedJobFromCollectors irj on ljf.id=irj.jobId
	 left join dc_collectorJobPresentations cjp on irj.assignedId=cjp.assignedId
	 left join dc_DCResults dr on cjp.resultId=dr.id
	 left join dc_dueDatePostponements ddp on cjp.postponementId=ddp.id
	 left join dc_DCEnqueryResults der on cjp.enqueryResultId=der.id
	 cross apply dbo.IRJStatus(irj.assignedId) irjsts
where ljf.contno='&HP-11050034' and irj.assignedId is not null
order by cjp.presentDate desc;
/*
select count(*), assignedId from dc_collectorJobPresentations group by assignedId
select * from dc_issuedReturnedJobFromCollectors where assignedId='1125'
select * from dc_loadedJobsForAssignToDebtCollector where id='657418'
*/