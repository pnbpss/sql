/****** Script for SelectTopNRows command from SSMS  ******/
use serviceweb;
/*
SELECT lcr.telephoningId
      ,lcr.callResultId
      ,lcr.[description]
      ,lcr.callOrder
      --,lcr.dateOfCall
      ,lcr.callTo
      ,lcr.thDateOfCall
      ,lcr.reallyFound
      ,lcr.comments
      ,lcr.telephoneNumber
      ,dbo.FN_ConvertDate(lcr.dealDate,'D/M/Y') dueDate
      ,lcr.staffName
      --,lcr.deleted
      ,lcr.contno
      ,dbo.FN_ConvertDate(lcr.loadDate,'D/M/Y') loadDate
      --,lcr.cuscod
      --,lcr.strno
      --,lcr.color
      ,lcr.stat
      ,dbo.FN_ConvertDate(lcr.sdate,'D/M/Y') sdate      
      ,lcr.crContStatus
      ,lcr.crDebtPlusVat
      ,lcr.crPendingPeriod
      ,lcr.crPendingFromPeriod
      ,lcr.crPendingToPeriod
      ,lcr.crRemainDebt
      ,lcr.crDisconnectedDate crDisconnectedDays
      ,dbo.FN_ConvertDate(lcr.crLastPaidDate,'D/M/Y') crLastPaidDate
      ,lcr.locat
      --,lcr.lastUpdateBy
      --,lcr.lastUpdateDate
      ,'|' sp1
	  ,cro.callTo croCallTo
	  ,cro.callResultId croCallResultId
	  ,cr.[description] croDescription
	  ,cro.callOrder croCallOrder
	  ,cro.reallyFound croReallyFound
	  ,dbo.FN_ConvertDate(cro.dateOfCall,'D/M/Y') croDateOfCall
	  ,cro.comments croComments
	  ,cro.telephoneNumber croTelephoneNumber
	  ,dbo.FN_ConvertDate(cro.dealDate,'D/M/Y') croDueDate
	  ,cro.staffName crStaffName 
      ,'|' sp2
      ,flcr.previousCallResultId
	 ,flcr.previousCallResultText
	 ,flcr.previousCallTelNo
	 ,dbo.FN_ConvertDate(flcr.previousDateOfCall,'D/M/Y') previousDateOfCall
	 ,flcr.previousComments	  
  FROM dc_lastCallResultOfEachConts lcr
  left join dc_callResultOfEachConts cro on lcr.telephoningId=cro.telephoningId and (lcr.callOrder-1)=cro.callOrder
  left join dc_callResults cr on cro.callResultId=cr.id
  cross apply dbo.FN_lastCallResultOfEachConts(lcr.telephoningId) flcr
  
  */
  
select pendingStatus, isnull([p0],0) P00
,isnull([p2],0) P02
,isnull([p5],0) P05
,isnull([p8],0) P08
,isnull([p9],0) P09
,isnull([p10],0) P10
,isnull([p11],0) P11
,isnull([p12],0) P12
,isnull([p13],0) P13
,isnull([p14],0) P14
,isnull([p15],0) P15
,isnull([p16],0) P16
,isnull([p17],0) P17
,isnull([p18],0) P18

,(
isnull([p2],0)+isnull([p5],0)+isnull([p8],0)
+isnull([p9],0)+isnull([p10],0)+isnull([p11],0)
+isnull([p12],0)+isnull([p13],0)+isnull([p14],0)
+isnull([p15],0)+isnull([p16],0)+isnull([p17],0)
+isnull([p18],0)) sumTelled
,(
isnull([p0],0)+isnull([p2],0)+isnull([p5],0)+isnull([p8],0)
+isnull([p9],0)+isnull([p10],0)+isnull([p11],0)
+isnull([p12],0)+isnull([p13],0)+isnull([p14],0)
+isnull([p15],0)+isnull([p16],0)+isnull([p17],0)
+isnull([p18],0)) sumAll

from (
	select count(*) cnt, pendingStatus,pCallResultId 
	from (
		SELECT 
			  case 
				when t.crPendingPeriod<=1 then 'P00_01_D00_00'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=0 and t.crDisconnectedDate<=15 then 'P02_D00_15'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=16 and t.crDisconnectedDate<=90 then 'P02_D16_90'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >90 then 'P02_DGT90'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=0 and t.crDisconnectedDate <=15 then 'PGT3_D00_15'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=16 and t.crDisconnectedDate <=90 then 'PGT3_D16_90'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=90 then 'PGT3_D16_90'
			  end pendingStatus
			  ,case
				when isnull(lcr.callResultId,0)<>0 then 'p'+convert(varchar(max),lcr.callResultId) 
				else 'p0'
				end pCallResultId
		      ,lcr.[description]
		  FROM dc_telephonings t left join dc_lastCallResultOfEachConts lcr on t.id=lcr.telephoningId
		  left join dc_callResultOfEachConts cro on lcr.telephoningId=cro.telephoningId and (lcr.callOrder-1)=cro.callOrder
		  left join dc_callResults cr on cro.callResultId=cr.id
		  cross apply dbo.FN_lastCallResultOfEachConts(lcr.telephoningId) flcr
		  where t.specifiedCallStartDate='2016/05/04' 
		  --and lcr.callResultId is not null
	) p
	group by pendingStatus,pCallResultId
) tb
pivot
(
	sum(cnt) for pCallResultId in ([p0],[p2],[p5],[p8],[p9],[p10],[p11],[p12],[p13],[p14],[p15],[p16],[p17],[p18])
) pv
