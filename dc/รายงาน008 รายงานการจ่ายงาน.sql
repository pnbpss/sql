use serviceweb;
/*
select 
irj.assignedId
,irj.issuedDate
,dc.id,dc.title+dc.fname+' '+dc.sname as dcFullName
,lcf.contno,lcf.locat jobLocat
,dc.locat dcLocat
,c.customerFullName
,irjs.cntAllPresent
,irjs.cntFoundCustomerPresent
,irjs.OthStatus
,irjs.paidStatus
,irjs.closedIrj
,case 
	when irjs.closedIrj = 'No' then '����Ѵ'
	when irjs.closedIrj = 'YF' then irjs.closedIrj+' �� ��ҧҹ�׹'
	when irjs.closedIrj = 'YP' then irjs.closedIrj+' �� Ť.����'
	when irjs.closedIrj = 'YR' then irjs.closedIrj+' �� �ִ�׹ö'
	when irjs.closedIrj = 'YS' then irjs.closedIrj+' �� �ִ'
	when irjs.closedIrj = 'YC' then irjs.closedIrj+' �� ¡��ԡ�ҹ'
end irjStatusText
,irjs.cntFoundSeized
,irjs.returnedPOA
,irjs.redempDate
,irjs.seizedCompensationType
,ljf.crRemainDebt,ljf.crDebtPlusVat,ljf.crPendingPeriod,ljf.crDisconnectedDays
from dbo.dc_issuedReturnedJobFromCollectors irj 
inner join dc_loadedJobsForAssignToDebtCollector ljf on ljf.id=irj.jobId
inner join dc_loadedContForCollection lcf on ljf.contno=lcf.contno
inner join dc_debtCollectors dc on irj.collectorId=dc.id
left join wb_customers c on lcf.buyerCuscod=c.CUSCOD
cross apply dbo.IRJStatus(irj.assignedId) irjs
where 1=1 and irj.deleted='N'
--and closedIrj like 'Y%'
*/

/*
select distinct closedIrj from dbo.dc_issuedReturnedJobFromCollectors irj cross apply dbo.IRJStatus(irj.assignedId) irjs
select * from dbo.dc_issuedReturnedJobFromCollectors where closed='R'
*/

select dcFullName,locat, 
	isnull([No],0) [No],
	isnull([YS],0) [YS],
	isnull([YR],0) [YR],
	isnull([YP],0) [YP],
	isnull([YC],0) [YC],	
	isnull([YF],0) [YF],
	(isnull([No],0)+
	isnull([YS],0)+
	isnull([YR],0)+
	isnull([YP],0)+
	isnull([YC],0)+	
	isnull([YF],0)) sumAll
	into #temporaryTable
	from (
	select 
	'('+right('000000' + cast(dc.id as varchar(6)), 6)+') '+dc.title+dc.fname+' '+dc.sname as dcFullName,dc.locat,irjs.closedIrj
	,count(*) cnt
	from dbo.dc_issuedReturnedJobFromCollectors irj 
	inner join dc_loadedJobsForAssignToDebtCollector ljf on ljf.id=irj.jobId
	inner join dc_loadedContForCollection lcf on ljf.contno=lcf.contno
	inner join dc_debtCollectors dc on irj.collectorId=dc.id
	left join wb_customers c on lcf.buyerCuscod=c.CUSCOD
	cross apply dbo.IRJStatus(irj.assignedId) irjs
	where 1=1 and irj.deleted='N'
	group by '('+right('000000' + cast(dc.id as varchar(6)), 6)+') '+dc.title+dc.fname+' '+dc.sname,dc.locat
	,irjs.closedIrj
) s
pivot
(
	sum(cnt)
	for [closedIrj] in ([YS],[YP],[YC],[No],[YF],[YR])
) pv

select * from #temporaryTable
union
select '���' dcFullName, '' locat, sum([No]) [No], sum([YS]) [YS],sum([YR]) [YR], sum([YP]) [YP],sum([YC]) [YC],	sum([YF]) [YF], sum(sumAll) sumAll
from #temporaryTable
--group by dcFullName


drop table #temporaryTable

--update dbo.dc_issuedReturnedJobFromCollectors set ARCONTNO='oAR-16040005' where ARCONTNO='oAR-16040011'