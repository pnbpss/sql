use serviceweb;
declare @startDate datetime, @endDate datetime;
set @startDate='2015/08/01';set @endDate='2015/10/31';


drop table #tempMakorn
select a.STRNO,a.SDATE,a.CONTNO,a.YDATE,i.MODEL,i.GCODE,i.STAT,isnull(y.pendingPeriodLP,0) pd
into #tempMakorn
from wb_arhold a inner join wb_invtran2 i on a.STRNO=i.STRNO and a.CONTNO=i.CONTNO
--left join wb_chqtran C on a.CONTNO=C.CONTNO and C.PAYFOR='002' and C.CANTIME is null and a.TOTPRC=(C.NETPAY+C.BALANCE)
cross apply dbo.PendingPeriodOnYdate(a.CONTNO,a.YDATE,a.SDATE) y
where a.YDATE between @startDate and @endDate and i.tbName='H'



--select * from #tempMakorn where STRNO='NF110R-0032632F5'
select * from wb_invtran2 where STRNO='NF110R-0032632F5'
select * from wb_arhold where STRNO='NF110R-0032632F5'
select * from hiincome.dbo.ARHOLD where STRNO='NF110R-0032632F5'
/*
select b.*,a.STRNO,a.CONTNO,a.YDATE,a.SDATE,a.GCODE from #tempMakorn a inner join 
(
	select count(STRNO) cnt,STRNO from #tempMakorn group by STRNO having count(STRNO)>1
) b on a.STRNO=b.STRNO
*/
--select * from #tempMakorn where STRNO='CN110C-0022516F3'

--alter table #tempMakorn alter column YDATE datetime
drop table #tempMakorn2
select 
rank() OVER (ORDER BY MODEL ASC) as idx,
MODEL, New_M1, New_M2, New_M3, Old_M1, Old_M2, Old_M3, Fin_M1, Fin_M2, Fin_M3, Sum_M1, Sum_M2, Sum_M3, Sum_all, P0_0_M1, P0_0_M2, P0_0_M3, P1_1_M1, P1_1_M2, P1_1_M3, P2_2_M1, P2_2_M2, P2_2_M3, P3_3_M1, P3_3_M2, P3_3_M3, P4_4_M1, P4_4_M2, P4_4_M3, P5_10_M1, P5_10_M2, P5_10_M3, PGT10_M1, PGT10_M2, PGT10_M3, PGT5_M1, PGT5_M2, PGT5_M3
,convert(numeric(6,2),(P0_0_M1*100.0/Sum_all)) as PCP0_0_M1	,convert(numeric(6,2),(P0_0_M2*100.0/Sum_all)) as PCP0_0_M2	,convert(numeric(6,2),(P0_0_M3*100.0/Sum_all)) as PCP0_0_M3	,convert(numeric(6,2),(P1_1_M1*100.0/Sum_all)) as PCP1_1_M1	,convert(numeric(6,2),(P1_1_M2*100.0/Sum_all)) as PCP1_1_M2	,convert(numeric(6,2),(P1_1_M3*100.0/Sum_all)) as PCP1_1_M3	,convert(numeric(6,2),(P2_2_M1*100.0/Sum_all)) as PCP2_2_M1	,convert(numeric(6,2),(P2_2_M2*100.0/Sum_all)) as PCP2_2_M2	,convert(numeric(6,2),(P2_2_M3*100.0/Sum_all)) as PCP2_2_M3	,convert(numeric(6,2),(P3_3_M1*100.0/Sum_all)) as PCP3_3_M1	,convert(numeric(6,2),(P3_3_M2*100.0/Sum_all)) as PCP3_3_M2	,convert(numeric(6,2),(P3_3_M3*100.0/Sum_all)) as PCP3_3_M3	,convert(numeric(6,2),(P4_4_M1*100.0/Sum_all)) as PCP4_4_M1	,convert(numeric(6,2),(P4_4_M2*100.0/Sum_all)) as PCP4_4_M2	,convert(numeric(6,2),(P4_4_M3*100.0/Sum_all)) as PCP4_4_M3	,convert(numeric(6,2),(P5_10_M1*100.0/Sum_all)) as PCP5_10_M1	,convert(numeric(6,2),(P5_10_M2*100.0/Sum_all)) as PCP5_10_M2	,convert(numeric(6,2),(P5_10_M3*100.0/Sum_all)) as PCP5_10_M3	,convert(numeric(6,2),(PGT10_M1*100.0/Sum_all)) as PCPGT10_M1	,convert(numeric(6,2),(PGT10_M2*100.0/Sum_all)) as PCPGT10_M2	,convert(numeric(6,2),(PGT10_M3*100.0/Sum_all)) as PCPGT10_M3	,convert(numeric(6,2),(PGT5_M1*100.0/Sum_all)) as PCPGT5_M1	,convert(numeric(6,2),(PGT5_M2*100.0/Sum_all)) as PCPGT5_M2	,convert(numeric(6,2),(PGT5_M3*100.0/Sum_all)) as PCPGT5_M3
,convert(numeric(6,2),(sumP0_2*100.0/Sum_all)) as PCsumP0_2
,convert(numeric(6,2),(sumP3_4*100.0/Sum_all)) as PCsumP3_4
,convert(numeric(6,2),(sumP5Up*100.0/Sum_all)) as PCsumP5Up
,convert(numeric(6,2),(Sum_all*100.0/Sum_all)) as PCsumAll
into #tempMakorn2

from (
	select a.MODEL
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate) and STAT='N') New_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate)) and STAT='N') New_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate)) and STAT='N') New_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate) and STAT='O' and GCODE not in ('05','051','052')) Old_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate)) and STAT='O' and GCODE not in ('05','051','052')) Old_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate)) and STAT='O' and GCODE not in ('05','051','052')) Old_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate) and GCODE in ('05','051','052')) Fin_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate)) and GCODE in ('05','051','052')) Fin_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate)) and GCODE in ('05','051','052')) Fin_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) Sum_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) Sum_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) Sum_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL) Sum_all
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=0 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P0_0_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=0 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P0_0_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=0 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P0_0_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=1 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P1_1_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=1 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P1_1_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=1 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P1_1_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=2 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P2_2_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=2 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P2_2_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=2 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P2_2_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=3 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P3_3_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=3 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P3_3_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=3 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P3_3_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=4 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P4_4_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=4 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P4_4_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd=4 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P4_4_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd between 5 and 10 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) P5_10_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd between 5 and 10 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) P5_10_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd between 5 and 10 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) P5_10_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 10 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) PGT10_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 10 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) PGT10_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 10 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) PGT10_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 5 and month(YDATE)=month(@startDate) and year(YDATE)=year(@startDate)) PGT5_M1
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 5 and month(YDATE)=month(dateadd(month,1,@startDate)) and year(YDATE)=year(dateadd(month,1,@startDate))) PGT5_M2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 5 and month(YDATE)=month(dateadd(month,2,@startDate)) and year(YDATE)=year(dateadd(month,2,@startDate))) PGT5_M3
	 
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd between 0 and 2) sumP0_2
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd between 3 and 4) sumP3_4
	 ,(select count(*) from #tempMakorn where MODEL=a.MODEL and pd > 4) sumP5Up
	 
	 from (
	  select distinct(MODEL) MODEL  from #tempMakorn
	 ) a
 ) b
 
 select * from #tempMakorn2 order by idx
 
 select
 '' as idx,	'' as model	
 ,sum(New_M1) as New_M1	,sum(New_M2) as New_M2	,sum(New_M3) as New_M3	,sum(Old_M1) as Old_M1	,sum(Old_M2) as Old_M2	,sum(Old_M3) as Old_M3	,sum(Fin_M1) as Fin_M1	,sum(Fin_M2) as Fin_M2	,sum(Fin_M3) as Fin_M3	,sum(Sum_M1) as Sum_M1	,sum(Sum_M2) as Sum_M2	,sum(Sum_M3) as Sum_M3	,sum(Sum_all) as Sum_all	,sum(P0_0_M1) as P0_0_M1	,sum(P0_0_M2) as P0_0_M2	,sum(P0_0_M3) as P0_0_M3	,sum(P1_1_M1) as P1_1_M1	,sum(P1_1_M2) as P1_1_M2	,sum(P1_1_M3) as P1_1_M3	,sum(P2_2_M1) as P2_2_M1	,sum(P2_2_M2) as P2_2_M2	,sum(P2_2_M3) as P2_2_M3	,sum(P3_3_M1) as P3_3_M1	,sum(P3_3_M2) as P3_3_M2	,sum(P3_3_M3) as P3_3_M3	,sum(P4_4_M1) as P4_4_M1	,sum(P4_4_M2) as P4_4_M2	,sum(P4_4_M3) as P4_4_M3	,sum(P5_10_M1) as P5_10_M1	,sum(P5_10_M2) as P5_10_M2	,sum(P5_10_M3) as P5_10_M3	,sum(PGT10_M1) as PGT10_M1	,sum(PGT10_M2) as PGT10_M2	,sum(PGT10_M3) as PGT10_M3	,sum(PGT5_M1) as PGT5_M1	,sum(PGT5_M2) as PGT5_M2	,sum(PGT5_M3) as PGT5_M3
 ,convert(numeric(5,2),sum(P0_0_M1)*100.0/sum(Sum_all)) as PCP0_0_M1	,convert(numeric(5,2),sum(P0_0_M2)*100.0/sum(Sum_all)) as PCP0_0_M2	,convert(numeric(5,2),sum(P0_0_M3)*100.0/sum(Sum_all)) as PCP0_0_M3	,convert(numeric(5,2),sum(P1_1_M1)*100.0/sum(Sum_all)) as PCP1_1_M1	,convert(numeric(5,2),sum(P1_1_M2)*100.0/sum(Sum_all)) as PCP1_1_M2	,convert(numeric(5,2),sum(P1_1_M3)*100.0/sum(Sum_all)) as PCP1_1_M3	,convert(numeric(5,2),sum(P2_2_M1)*100.0/sum(Sum_all)) as PCP2_2_M1	,convert(numeric(5,2),sum(P2_2_M2)*100.0/sum(Sum_all)) as PCP2_2_M2	,convert(numeric(5,2),sum(P2_2_M3)*100.0/sum(Sum_all)) as PCP2_2_M3	,convert(numeric(5,2),sum(P3_3_M1)*100.0/sum(Sum_all)) as PCP3_3_M1	,convert(numeric(5,2),sum(P3_3_M2)*100.0/sum(Sum_all)) as PCP3_3_M2	,convert(numeric(5,2),sum(P3_3_M3)*100.0/sum(Sum_all)) as PCP3_3_M3	,convert(numeric(5,2),sum(P4_4_M1)*100.0/sum(Sum_all)) as PCP4_4_M1	,convert(numeric(5,2),sum(P4_4_M2)*100.0/sum(Sum_all)) as PCP4_4_M2	,convert(numeric(5,2),sum(P4_4_M3)*100.0/sum(Sum_all)) as PCP4_4_M3	,convert(numeric(5,2),sum(P5_10_M1)*100.0/sum(Sum_all)) as PCP5_10_M1	,convert(numeric(5,2),sum(P5_10_M2)*100.0/sum(Sum_all)) as PCP5_10_M2	,convert(numeric(5,2),sum(P5_10_M3)*100.0/sum(Sum_all)) as PCP5_10_M3	,convert(numeric(5,2),sum(PGT10_M1)*100.0/sum(Sum_all)) as PCPGT10_M1	,convert(numeric(5,2),sum(PGT10_M2)*100.0/sum(Sum_all)) as PCPGT10_M2	,convert(numeric(5,2),sum(PGT10_M3)*100.0/sum(Sum_all)) as PCPGT10_M3	,convert(numeric(5,2),sum(PGT5_M1)*100.0/sum(Sum_all)) as PCPGT5_M1	,convert(numeric(5,2),sum(PGT5_M2)*100.0/sum(Sum_all)) as PCPGT5_M2	,convert(numeric(5,2),sum(PGT5_M3)*100.0/sum(Sum_all)) as PCPGT5_M3
,convert(numeric(5,2),avg(PCsumP0_2)) PCsumP0_2
,convert(numeric(5,2),avg(PCsumP3_4)) PCsumP3_4
,convert(numeric(5,2),avg(PCsumP5Up)) PCsumP5Up
,convert(numeric(5,2),avg(PCsumAll)) PCsumAll
from #tempMakorn2

