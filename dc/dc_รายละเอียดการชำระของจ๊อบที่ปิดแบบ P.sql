/*
select 
RANK() OVER (PARTITION BY boc.contno ORDER BY boc.contno,chq.TMBILL) AS r
,boc.contno			
,boc.dcTFSName dcName
,boc.assignedId,dbo.FN_ConvertDate(boc.loadDate,'D/M/Y') loadDate,dbo.FN_ConvertDate(boc.issuedDate,'D/M/Y') issuedDate
,boc.closed
,dbo.FN_ConvertDate(va.endContract,'D/M/Y') endContract,dbo.FN_ConvertDate(va.lastPaidDate,'D/M/Y') lastPaidDate
,va.netPendingPeriod npp
,va.lastPaidInstall lpi
,dbo.FN_ConvertDate(chq.TMBILDT,'D/M/Y') billDate
,chq.TMBILL billNo, chq.NETPAY netpay, chq.PAYINT [interest], chq.PAYAMT payamt
into #abc
from dc_allAssignedJobs boc 
left join view_arpay2 va on boc.contno=va.CONTNO
left join HIC4REPORT.dbo.CHQTRAN chq on boc.contno=chq.CONTNO and va.lastPaidDate=chq.TMBILDT and chq.FLAG<>'C' and chq.PAYFOR in ('006','007') and  boc.issuedDate<=chq.TMBILDT
where 1=1
--and boc.closed in ('P','R')
--and boc.contno in ('�HP-15020005','JHP-14030021','MHP-15060003','!HP-14040002')
and boc.issuedDate between '2016/04/01' and '2016/05/15'
and chq.TMBILL is not null

drop table #abc
*/

select t1.r,t1.contno,t1.dcName,t1.assignedId,t1.loadDate,t1.issuedDate,t1.closed,t1.endContract,t1.lastPaidDate,t1.npp,t1.lpi,t1.billDate
,t1.billNo billno1,t1.netpay netpay1,t1.interest interest1,t1.payamt payamt1
,t2.billNo billno2,t2.netpay netpay2,t2.interest interest2,t2.payamt payamt2
from #abc t1 left join #abc t2 
on t1.contno=t2.contno and t1.assignedId=t2.assignedId and t2.r=2
where t1.r=1
