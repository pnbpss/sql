use serviceweb
drop table ##tempCompensations;
drop table ##sumCompensation
declare @cols nvarchar(max);
		select @cols = STUFF((SELECT ',' + QUOTENAME('cid'+convert(varchar(max),dcs.compensationsId)) 
						from (
								select distinct compensationsId from dc_DCCompensationDetails where compensationsId<>9999 and month='4' and year='2016'
								union 
								select 99999 as compensationsId
							  ) dcs
						order by dcs.compensationsId
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')
		print @cols
		
declare @colsum nvarchar(max);
select @colsum = STUFF((SELECT ',sum(' + QUOTENAME('cid'+convert(varchar(max),dcs.compensationsId))+') '+QUOTENAME('cid'+convert(varchar(max),dcs.compensationsId))+' ' 
				from (
						select distinct compensationsId from dc_DCCompensationDetails where compensationsId<>9999 and month='4' and year='2016'
						union 
						select 99999 as compensationsId
					  ) dcs
				order by dcs.compensationsId
		FOR XML PATH(''), TYPE
		).value('.', 'NVARCHAR(MAX)') 
	,1,1,'')
print @colsum

		declare @query varchar(max);
		set @query=
		'
		select collectorId,
		'+@cols+'
		into ##tempCompensations
		from 
		(
		select 
		un.collectorId,un.amount,''cid''+convert(varchar(max),un.compensationsId) compensationsId
		from dc_debtCollectors dc left join
		(
			select collectorId,dc.descriptions,compensationsId,month,year,amount 
			from dc_DCCompensationDetails dcd
			left join dc_compensations dc on dcd.compensationsId=dc.id 
			where dcd.month=''4'' and dcd.year=''2016'' and compensationsId<>9999
			union
			select dns.collectorId,''ยอดรายรับรวมสุทธิ'' descriptions,''99999'' compensationsId,month,year,summaryCP amount
			from dc_dcNetIncomeBySumIncomeField dns
			where dns.month=''4'' and dns.year=''2016''
		) un on un.collectorId=dc.id
		where dc.available=''Y''
		) xp pivot
		(
			max(amount) for compensationsId in ('+@cols+')
		) p
		
		--select  ''รวม'' name, 0 collectorId,'+@colsum+' into ##sumCompensation from ##tempCompensations;
		--select '+@colsum+' into ##sumCompensation from ##tempCompensations;
		select  0 collectorId,'+@colsum+' into ##sumCompensation from ##tempCompensations;
		
		';
execute(@query);

--select * from ##tempCompensations;
--select * from ##sumCompensation;

SELECT a.title+a.fname+' '+a.sname as name, tc.*						
FROM dc_debtCollectors a left join ##tempCompensations tc on a.id=tc.collectorId 
where tc.collectorId is not null
union
SELECT 'รวม', tc.* FROM ##sumCompensation tc 



--drop table ##tempCompensationsx1