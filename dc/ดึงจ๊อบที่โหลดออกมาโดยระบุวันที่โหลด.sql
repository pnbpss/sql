/****** Script for SelectTopNRows command from SSMS  ******/
use serviceweb;
--declare @supId int;
--select @supId=id from dc_supervisors where hiincomeUserId='1138'; insert into dc_supervisorAssignedToBranch([supervisorId],[branchId],[startDate],[endDate]) values(@supId,'','2016/05/01','2500/01/01');
SELECT --[id]
      --,[jobBranchId]
      --,[loadDate]
      --,[loadBy]
      [contno] 'เลขที่สัญญา'
      ,[crContStatus] 'สถานะสัญญา'
      ,[crDebtPlusVat] 'ลน.คงเหลือ'
      ,[crPendingPeriod] 'จน.งวดค้าง'
      ,[crRemainDebt] 'ยอดค้า'
      ,[crPendingFromPeriod] 'ค้างจากงวด'
      ,[crPendingToPeriod] 'ค้างถึงงวด'
      ,[crDisconnectedDays] 'จน.วันขาดการติดต่อ'
      ,dbo.FN_ConvertDate([crLastPaidDate],'D/M/Y') 'วดป.ชำระล่าสุด'
      ,dbo.FN_ConvertDate([postponeDueTo],'D/M/Y') 'ผลัดดิวถึง'
      --,[lastUpdateBy]
      --,[lastUpdateDate]
  FROM [dc_loadedJobsForAssignToDebtCollector] 
  where loadDate ='2016/05/07'