use serviceweb;
/*
IF OBJECT_ID('tempdb.dbo.#crmkt', 'U') IS NOT NULL DROP TABLE #crmkt;
select 
chq.TMBILL,chq.PAYFOR,chq.LOCATRECV,chq.TMBILDT
,chq.NETPAY,chq.PAYINT
,chq.CONTNO CONTNO1,inv1.STRNO STRNO1,inv1.GCODE GCODE1
, ar.CONTNO CONTNO2,inv2.STRNO STRNO2,inv2.GCODE GCODE2
,datediff(day,inv1.SDATE,inv2.SDATE) sddiff
into #crmkt
from wb_chqtran chq
left join wb_invtran inv1 on chq.CONTNO=inv1.CONTNO
left join wb_armast ar on chq.TMBILDT=ar.SDATE and chq.CUSCOD=ar.CUSCOD
left join wb_invtran inv2 on ar.CONTNO=inv2.CONTNO and inv2.GCODE in ('05','051','052')

where 1=1 
and chq.PAYFOR in ('007','006') and dbo.removeFFromStrno(inv2.STRNO)=dbo.removeFFromStrno(inv1.STRNO)
--and chq.PAYFOR in ('006') and dbo.removeFFromStrno(inv2.STRNO)=dbo.removeFFromStrno(inv1.STRNO)
--and chq.PAYFOR in ('007')
and chq.FLAG!='C'
and chq.TMBILDT between '20160101' and '20160930'
--and month(chq.TMBILDT)=5 and year(chq.TMBILDT)=2014
order by chq.TMBILDT desc
*/
SELECT * FROM 
(
	select a.*,aaj.* from #crmkt a left join dc_allAssignedJobs aaj on (a.TMBILL=aaj.installmentBillNoOne)
	union
	select a.*,aaj.* from #crmkt a left join dc_allAssignedJobs aaj on (a.TMBILL=aaj.installmentBillNoTwo)
) a where jobId is not null
order by contno desc