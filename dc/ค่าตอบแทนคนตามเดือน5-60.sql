use serviceweb;
/*
select dc.id, dc.title+dc.fname+' '+dc.sname dcFullName,dc.locat
,cp.cntJobFromPreviousMonth,cp.cntJobAssignedThisMonth,cp.cntAllJob,cp.cntCustomerPaid
,cp.cntPaidAndSeized,cp.percentOfSuccessJob,cp.percentOfPaid
,cp.amountOfPaid
,cp.cntSeized,cp.cntSeized0011,cp.cntSeized1218,cp.cntSeized1924,cp.cntSeized2530,cp.cntSeized3136,cp.cntSeizedGT36
from dc_debtCollectors dc 
cross apply dbo.[fn_CompensationsCalculations.201705](dc.id,5,2017) cp 
where 1=1
and dc.locat in ('YJ���')
and dc.available='Y'

select dc.id, dc.title+dc.fname+' '+dc.sname dcFullName,dc.locat
			,cp.cntJobFromPreviousMonth,cp.cntJobAssignedThisMonth,cp.cntAllJob,cp.cntCustomerPaid
			,cp.cntPaidAndSeized,cp.percentOfSuccessJob,cp.percentOfPaid
			,cp.amountOfPaid
			,cp.cntSeized,cp.cntSeized0011,cp.cntSeized1218,cp.cntSeized1924,cp.cntSeized2530,cp.cntSeized3136,cp.cntSeizedGT36
			into #abc
			from dc_debtCollectors dc 
			cross apply dbo.[fn_CompensationsCalculations.201705](dc.id,'5','2017') cp 
			where 1=1			
			and dc.available='Y'

*/
--������������ function 
declare @month int,@year int,@collectorId int; 
set @month=5;set @year=2017;set @collectorId=61
select isnull([S0011],0) S0011,	isnull([S1218],0) S1218,	isnull([S1924],0) S1924,	isnull([S2530],0) S2530,	isnull([S3136],0) S3136,	isnull([SGT36],0) SGT36
from(
		select COUNT(*)cnt,debtC.sType from (
			select 
			case 
				when allu.mdis between 0 and 11 then 'S0011'		
				when allu.mdis between 12 and 18 then 'S1218'		
				when allu.mdis between 19 and 24 then 'S1924'		
				when allu.mdis between 25 and 30 then 'S2530'		
				when allu.mdis between 31 and 36 then 'S3136'		
				when allu.mdis > 36 then 'SGT36'
			end sType
			 from 
			(
				select --�ִẺ S
				case when ISNULL(irj.crLastPaidDate,'')='' then datediff(MONTH,ah.SDATE,ah.YDATE) 	else datediff(MONTH,irj.crLastPaidDate,ah.YDATE) end mdis
				,DATEDIFF(month,isnull(irj.crLastPaidDate,ah.SDATE),irj.loadDate) discontMonth,irj.loadDate
				,ah.SDATE,irj.crLastPaidDate,ah.YDATE,irj.crDisconnectedDays
				,irj.closed,irj.issuedDate,irj.collectorId	
				from 	dc_allAssignedJobs irj left join wb_arhold ah on irj.contno=ah.CONTNO where irj.closed='S' and month(irj.closedDate)=@month and year(irj.closedDate)=@year and irj.collectorId=@collectorId
				
				union
				
				select --�ִẺ R ���Ѻ��ҵͺ᷹�繤ѹ
				 case when ISNULL(irj.crLastPaidDate,'')='' then datediff(MONTH,ah.SDATE,chq.TMBILDT) else datediff(MONTH,irj.crLastPaidDate,chq.TMBILDT) end mdis
				 ,DATEDIFF(month,isnull(irj.crLastPaidDate,ah.SDATE),irj.loadDate) discontMonth,irj.loadDate,ah.SDATE,irj.crLastPaidDate,ah.YDATE,irj.crDisconnectedDays,irj.closed,irj.issuedDate,irj.collectorId	
				from 	dc_allAssignedJobs irj left join wb_armast ah on irj.contno=ah.CONTNO
				left join wb_chqtran chq on irj.seizedBillNo=chq.TMBILL
				where irj.closed='R' and irj.seizedCompensationType='V' and month(irj.closedDate)=@month and year(irj.closedDate)=@year and irj.collectorId=@collectorId
			)allu
		) debtC
		group by debtC.sType
)t pivot(
	sum(cnt) for sType in ([S0011],[S1218],[S1924],[S2530],[S3136],[SGT36])
)p

select * from dbo.[fn_CompensationsCalculations.201705](61,'5','2017')