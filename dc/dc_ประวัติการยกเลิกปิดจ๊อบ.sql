/****** Script for SelectTopNRows command from SSMS  ******/
SELECT ccj.assignedId
      ,dbo.FN_ConvertDate(asj.loadDate,'D/M/Y')loadDate,asj.jobId,asj.contno,asj.locat,asj.dcTFSName
      ,dbo.FN_ConvertDate(ccj.[unclosedDate],'D/M/Y')+' '
		+case when datepart(hh,ccj.unclosedDate) < 10 then '0'+convert(varchar,datepart(hh,ccj.unclosedDate)) else convert(varchar,datepart(hh,ccj.unclosedDate)) end+':'
		+case when datepart(mi,ccj.unclosedDate) < 10 then '0'+convert(varchar,datepart(mi,ccj.unclosedDate)) else convert(varchar,datepart(mi,ccj.unclosedDate)) end+':'
		+case when datepart(ss,ccj.unclosedDate) < 10 then '0'+convert(varchar,datepart(ss,ccj.unclosedDate)) else convert(varchar,datepart(ss,ccj.unclosedDate)) end+'น.' timeOfCancelClosed
      --,ccj.unclosedDate
      --,ccj.ipaddress
      ,ccj.[causeOfCancelClose] why
      ,u.name unclosedBy
      ,case 
		when asj.closed='P' then 'ปิดลค.ชำระ('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')'
		when asj.closed='S' then 'ปิดยึด('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')' 
		when asj.closed='R' then 'ปิดยึดลค.รับคืน('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')' 
		when asj.closed='C' then 'ยกเลิกงานตาม('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')' 
		when asj.closed='F' then 'เอางานตามคืน('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')' 
		when asj.closed='N' then 'เร่งรัด(ยังไม่ปิดจ๊อบ)' 
		else asj.closed+'('+dbo.FN_ConvertDate(asj.closedDate,'D/M/Y')+')'
      end currentStatus
FROM [dc_cancelCloseJobLogs] ccj left join dc_users u on ccj.unclosedBy=u.id
     left join dc_allAssignedJobs asj on ccj.assignedId=asj.assignedId
where 1=1
and asj.contno is not null
order by ccj.assignedId desc,asj.contno,ccj.unclosedDate desc



--select convert(datetime,'2016/9/8')