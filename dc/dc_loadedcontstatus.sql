USE [serviceweb]
GO

/****** Object:  Table [dbo].[dc_loadedContForCollectionStatus]    Script Date: 07/12/2015 22:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dc_loadedContForCollectionStatus](
	[id] [int] NOT NULL,
	[descriptions] [varchar](50) NOT NULL,
 CONSTRAINT [PK_dc_loadedContForCollectionStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/*

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


*/