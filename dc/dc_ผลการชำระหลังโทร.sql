/****** Script for SelectTopNRows command from SSMS  ******/
use serviceweb;
IF OBJECT_ID('tempdb..#tempTbname') is not null DROP TABLE #tempTbname  
SELECT RANK() OVER (PARTITION BY lcr.telephoningId,lcr.contno ORDER BY chq1.TMBILL) AS [rank]
,lcr.telephoningId,lcr.contno,lcr.locat,lcr.loadDate,lcr.cuscod,lcr.strno,lcr.color,lcr.stat,lcr.sdate,lcr.callOrder,lcr.callResultId
,lcr.[description] callResultDesc,lcr.dateOfCall,lcr.thDateOfCall,lcr.useNumber,lcr.comments,lcr.telephoneNumber,lcr.dealDate,lcr.staffName      
,lcr.crContStatus,lcr.crDebtPlusVat,lcr.crPendingPeriod,lcr.crPendingFromPeriod,lcr.crPendingToPeriod
,lcr.crDisconnectedDate,lcr.crLastPaidDate,lcr.lastUpdateBy,lcr.lastUpdateDate,lcr.crRemainDebt,lcr.callTo
,lcr.reallyFound,lcr.specifiedCallStartDate,chq1.TMBILL,chq1.TMBILDT,chq1.PAYAMT,chq1.PAYINT,chq1.NETPAY,chq1.PAYFOR
,datediff(day,lcr.dateOfCall,chq1.TMBILDT) ddiffCallAndPaid
,datediff(day,lcr.dateOfCall,getdate()) ddiffCallAndToday
,case when isnull(chq1.TMBILL,'N') = 'N' then 'No'
 else 'Yes'
end paid
,case 
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=0 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 5 then 'D00_05'
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=6 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 10 then 'D06_10'
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=11 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 15 then 'D11_15'
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=16 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 20 then 'D16_20'
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=21 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 25 then 'D21_25'
	when datediff(day,lcr.dateOfCall,chq1.TMBILDT) >=26 and datediff(day,lcr.dateOfCall,chq1.TMBILDT) <= 30 then 'D25_30'
	else 'notPaid'
end ddCallPaidGroup

into #tempTbname
FROM dc_lastCallResultOfEachConts lcr
   left join wb_chqtran chq1 on lcr.contno=chq1.CONTNO and chq1.PAYFOR in ('006','007') and chq1.FLAG!='C' and chq1.TMBILDT between lcr.dateOfCall and DATEADD(day,30,lcr.dateOfCall)    
where 1=1
and lcr.dateOfCall between '2016/05/01' and '2016/05/31'

select
t1.rank,t1.telephoningId,t1.contno,t1.locat,t1.loadDate,t1.cuscod,t1.strno,t1.color,t1.stat,t1.sdate,t1.callOrder,t1.callResultId,t1.callResultDesc,t1.dateOfCall,t1.thDateOfCall,t1.useNumber,t1.comments,t1.telephoneNumber,t1.dealDate,t1.staffName,t1.crContStatus,t1.crDebtPlusVat,t1.crPendingPeriod,t1.crPendingFromPeriod,t1.crPendingToPeriod,t1.crDisconnectedDate,t1.crLastPaidDate,t1.lastUpdateBy,t1.lastUpdateDate,t1.crRemainDebt,t1.callTo,t1.reallyFound,t1.specifiedCallStartDate,t1.paid
,t1.TMBILL TMBILL1, t1.TMBILDT TMBILDT1, t1.PAYAMT PAYAMT1, t1.PAYINT PAYINT1,t1.NETPAY NETPAY1,t1.PAYFOR PAYFOR1, t1.ddiffCallAndPaid ddiffCallAndPaid1
,t2.TMBILL TMBILL2, t2.TMBILDT TMBILDT2, t2.PAYAMT PAYAMT2, t2.PAYINT PAYINT2,t2.NETPAY NETPAY2,t2.PAYFOR PAYFOR2, t2.ddiffCallAndPaid ddiffCallAndPaid2
,t3.TMBILL TMBILL3, t3.TMBILDT TMBILDT3, t3.PAYAMT PAYAMT3, t3.PAYINT PAYINT3,t3.NETPAY NETPAY3,t3.PAYFOR PAYFOR3, t3.ddiffCallAndPaid ddiffCallAndPaid3
,t4.TMBILL TMBILL4, t4.TMBILDT TMBILDT4, t4.PAYAMT PAYAMT4, t4.PAYINT PAYINT4,t4.NETPAY NETPAY4,t4.PAYFOR PAYFOR4, t4.ddiffCallAndPaid ddiffCallAndPaid4
,t4.ddiffCallAndToday
,t1.ddCallPaidGroup
,isnull(t1.PAYAMT,0)+isnull(t2.PAYAMT,0)+isnull(t3.PAYAMT,0)+isnull(t4.PAYAMT,0) sumPAYAMT
,isnull(t1.PAYINT,0)+isnull(t2.PAYINT,0)+isnull(t3.PAYINT,0)+isnull(t4.PAYINT,0) sumPAYINT
,isnull(t1.NETPAY,0)+isnull(t2.NETPAY,0)+isnull(t3.NETPAY,0)+isnull(t4.NETPAY,0) sumNETPAY
from #tempTbname t1 
left join #tempTbname t2 on t1.telephoningId=t2.telephoningId and t1.contno=t2.contno and t2.[rank]=2
left join #tempTbname t3 on t1.telephoningId=t3.telephoningId and t1.contno=t3.contno and t3.[rank]=3
left join #tempTbname t4 on t1.telephoningId=t4.telephoningId and t1.contno=t4.contno and t4.[rank]=4
where t1.[rank]=1

--and t1.TMBILL is not null
--select distinct callResultId, '('+convert(varchar(max),callResultId)+')'+callResultDesc aa from #tempTbname order by callResultId