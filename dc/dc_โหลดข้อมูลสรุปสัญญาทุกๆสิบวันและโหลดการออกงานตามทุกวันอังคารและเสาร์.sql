USE [msdb]
GO

/****** Object:  Job [dc_loadContractEvery10Day]    Script Date: 06/06/2016 09:55:11 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 06/06/2016 09:55:11 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'dc_loadContractEvery10Day', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ndc_doIt]    Script Date: 06/06/2016 09:55:11 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ndc_doIt', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @date DATETIME,@endDayOfMonth int;
--set @date = ''''20160229'''';
use serviceweb;
set @date = GETDATE();
set @endDayOfMonth = day(DATEADD(dd, -DAY(DATEADD(m,1,@date)), DATEADD(m,1,@date)))
if day(@date)=1 or day(@date)=10 or DAY(@date)=15 or day(@date)=20 or DAY(@date)=@endDayOfMonth
begin
	--print ''''do something''''
	insert into dc_pendingStatsOn10201530(		
				contno,
				locat,
				gcode,
				strno,
				startContract,
				endContract,
				lastPaidDate,
				installmentPerPeriod,
				lastPaidInstallmentPeriod,
				lastPaidInstall,
				AllPeriod,
				currentperiod,
				disconnectedDays,
				pendingInstallmentPeriod,
				netPendingPeriod,
				pendingToPeriod,
				postponeDueTo,
				lastpassdue,
				neverpaid,
				nextDealDate,
				buyerCuscod,
				suretyCuscod1,
				suretyCuscod2,
				suretyCuscod3,
				sdate,
				fh,
				contStatus,
				debtPlusVat,
				remainDebt,
				pendingFromPeriod,
				loadDateTime
				) 
		select 
		a.CONTNO [contno]
		,a.LOCAT [locat]
		,v.GCODE
		,a.STRNO strno
		,va.startContract
		,va.endContract
		,va.lastPaidDate
		,va.installmentPerPeriod
		,va.lastPaidInstallmentPeriod
		,va.lastPaidInstall
		,va.AllPeriod
		,va.currentperiod
		,va.disconnectedDays
		,va.pendingInstallmentPeriod
		,va.netPendingPeriod
		,isnull(va.currentperiod,0) [crPendingToPeriod]
		,pp.LACCPDT as [postponeDueTo]
		,va.lastpassdue
		,va.neverpaid
		,va.nextDealDate
		,a.CUSCOD as buyerCuscod
		,s1.CUSCOD as suretyCuscod1
		,s2.CUSCOD as suretyCuscod2
		,s3.CUSCOD as suretyCuscod3
		,a.SDATE as sdate
		,case when v.GCODE=''''05'''' then ''''F'''' when v.GCODE=''''051'''' then ''''F'''' when v.GCODE=''''052'''' then ''''F'''' else v.STAT end FH
		,a.CONTSTAT [crContStatus]
		,(a.TOTPRC-a.SMPAY) crDebtPlusVat --ลน.คงเหลือ
		,(SELECT SUM(DAMT-PAYMENT) FROM wb_arpay WHERE CONTNO=a.CONTNO and DDATE<=getdate()) crRemainDebt --ยอดค้าง
		,case when isnull(va.currentperiod,0)=0 then 0 when va.neverpaid=''''Y'''' then 1 when va.netPendingPeriod=0 then 0 else va.currentperiod-(va.netPendingPeriod-1) end as crPendingFromPeriod 
		,getdate()
		from wb_armast a 
		left join view_arpay2 va on a.CONTNO=va.CONTNO
		left join wb_invtran v on a.STRNO=v.STRNO
		left join wb_surety1 s1 on a.CONTNO=s1.CONTNO
		left join wb_surety2 s2 on a.CONTNO=s2.CONTNO
		left join wb_surety3 s3 on a.CONTNO=s3.CONTNO
		left join dc_onPostponePayContracts pp on a.CONTNO=pp.CONTNO
		where 1=1
		and (not(va.installmentPerPeriod=va.lastPaidInstall and va.AllPeriod=va.lastPaidInstallmentPeriod and va.netPendingPeriod=0 and va.neverpaid=''''N''''))
end
else
begin
	insert into dc_pendingStatsOn10201530Stat (runtime,done) values(GETDATE(),''''yet'''');
end

delete from dc_pendingStatsOn10201530 where loadDateTime < dateadd(year,-2,getdate())', 
		@database_name=N'serviceweb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'dc_loadContEvery10DaysSchecule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160606, 
		@active_end_date=99991231, 
		@active_start_time=190000, 
		@active_end_time=235959, 
		@schedule_uid=N'85127685-0733-480e-aa60-73ae9d04d65b'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

--------------------------------
USE [msdb]
GO

/****** Object:  Job [dc_loadContForDC]    Script Date: 06/06/2016 09:55:48 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 06/06/2016 09:55:48 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'dc_loadContForDC', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'โหลดงานเพื่อออกงานตามทุกๆตอนเย็นวันอังคาร์และเย็นวันเสาร์', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ndc_doit]    Script Date: 06/06/2016 09:55:49 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ndc_doit', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'use serviceweb;
DECLARE @dc_loadedJobsForAssignToDebtCollectorTemp TABLE
(
strno varchar(20),
buyerCuscod varchar(12),
suretyCuscod1 varchar(12),
suretyCuscod2 varchar(12),
suretyCuscod3 varchar(12),
sdate datetime,
installmentPerPeriod decimal,
loadDate varchar(30),
loadBy varchar(4),
contno varchar(12),
locat varchar(5),
crContStatus varchar(1),
crDebtPlusVat decimal,
crPendingPeriod int,
crRemainDebt decimal,
crPendingFromPeriod decimal,
crPendingToPeriod decimal,
crDisconnectedDays int,
postponeDueTo datetime,
crLastPaidDate datetime,
lastUpdateBy int,
lastUpdateDate int,
color varchar(20),
STAT varchar(1),
AllPeriod int,
FH varchar(1),
MODEL varchar(20)
)
insert into @dc_loadedJobsForAssignToDebtCollectorTemp 
select 			
--เริ่ม ข้อมูลหลักของแต่ละสัญญา
a.STRNO strno,a.CUSCOD as buyerCuscod,s1.CUSCOD as suretyCuscod1,s2.CUSCOD as suretyCuscod2,s3.CUSCOD as suretyCuscod3
,a.SDATE as sdate,va.installmentPerPeriod installmentPerPeriod
--จบข้อมูลหลักของแต่ละสัญญา
,convert(varchar,getdate(),111) as [loadDate],''9999'' [loadBy]
,a.CONTNO [contno],a.LOCAT [locat],a.CONTSTAT [crContStatus]
,(a.TOTPRC-a.SMPAY) crDebtPlusVat --ลน.คงเหลือ
,va.netPendingPeriod [crPendingPeriod]
--,a.EXP_AMT [crRemainDebt] --ยอดค้าง	
,(SELECT SUM(DAMT-PAYMENT) FROM wb_arpay WHERE CONTNO=a.CONTNO and DDATE<=getdate()) crRemainDebt
,case
	when isnull(va.currentperiod,0)=0 then 0
	when va.neverpaid=''Y'' then 1
	when va.netPendingPeriod=0 then 0
	else va.currentperiod-(va.netPendingPeriod-1)
end as crPendingFromPeriod 
,isnull(va.currentperiod,0) [crPendingToPeriod]
,va.disconnectedDays [crDisconnectedDays]
,pp.LACCPDT as [postponeDueTo]
,va.lastPaidDate [crLastPaidDate]
,null [lastUpdateBy],null [lastUpdateDate],v.COLOR color,v.STAT,va.AllPeriod
,case 
	when v.GCODE=''05'' then ''F''
	when v.GCODE=''051'' then ''F''
	when v.GCODE=''052'' then ''F''
	else v.STAT
end FH
,v.MODEL				
from wb_armast a 					
	left join view_arpay2 va on a.CONTNO=va.CONTNO
	left join wb_invtran v on a.STRNO=v.STRNO
	left join wb_surety1 s1 on a.CONTNO=s1.CONTNO
	left join wb_surety2 s2 on a.CONTNO=s2.CONTNO
	left join wb_surety3 s3 on a.CONTNO=s3.CONTNO
	left join dc_onPostponePayContracts pp on a.CONTNO=pp.CONTNO
where 					
	(
		(
			 va.netPendingPeriod>=3 and
			  (
				(va.disconnectedDays>15)  or (va.disconnectedDays between 0 and 15 and va.neverpaid=''N'')
			  )
			  and a.CONTSTAT not in (select contStatus from dc_noMoreDCContStatuses)
		  )
	  or
		  (
			 va.netPendingPeriod between 1 and 2 and 
			 va.lastpassdue = ''Y''  and							 
			 a.CONTSTAT not in (select contStatus from dc_noMoreDCContStatuses)							 
		  )
	  )

--เอาข้อมูลใส่ตารางหลัก dc_loadedContForCollection โดยใช้เลขที่สัญญาเป็นตัวคุม
insert into dc_loadedContForCollection (contno,strno,locat,buyerCuscod,suretyCuscod1,suretyCuscod2,suretyCuscod3,sdate
,installmentPerperiod,statusId,firstLoadBy,firstLoadSince,[color],stat,allPeriod,FH
,model)
select a.contno,a.strno,a.locat,a.buyerCuscod,a.suretyCuscod1,a.suretyCuscod2,a.suretyCuscod3
,a.sdate
,a.installmentPerPeriod,100 statusId,''9999'' as firstLoadBy
,convert(varchar,getdate(),111) as firstLoadSince,a.[color],a.STAT,a.AllPeriod,a.FH,a.MODEL
from @dc_loadedJobsForAssignToDebtCollectorTemp a 
left join dc_loadedContForCollection b on a.contno=b.contno
where b.contno is null

--เอาข้อมูลใส่ตาราง การจ่ายงานให้คนตาม โดยให้ branchJobId เป็น 0 จะ update field นี้ก็ต่อเมื่อได้เอาไปจ่ายงานให้คนตามแล้ว
--delete from dc_loadedJobsForAssignToDebtCollector where loadDate = convert(varchar,getdate(),111);
insert into dc_loadedJobsForAssignToDebtCollector(
		[jobBranchId],[loadDate],[loadBy],[contno]
		,[crContStatus],[crDebtPlusVat] --ลน.คงเหลือ
		,[crPendingPeriod],[crRemainDebt] --ยอดค้าง
		,[crPendingFromPeriod],[crPendingToPeriod],[crDisconnectedDays]
		,[crLastPaidDate],[postponeDueTo],[lastUpdateBy],[lastUpdateDate]
) select 
		0 as jobBranchId
		,[loadDate],[loadBy],[contno],[crContStatus]
		,[crDebtPlusVat] --ลน.คงเหลือ
		,[crPendingPeriod],[crRemainDebt] --ยอดค้าง
		,[crPendingFromPeriod],[crPendingToPeriod],[crDisconnectedDays]
		,[crLastPaidDate],[postponeDueTo],[lastUpdateBy],[lastUpdateDate]
from @dc_loadedJobsForAssignToDebtCollectorTemp a', 
		@database_name=N'serviceweb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'ndc_loadContsForDCEveryTueAndSat', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=68, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20160606, 
		@active_end_date=99991231, 
		@active_start_time=193000, 
		@active_end_time=235959, 
		@schedule_uid=N'271e157a-a20f-4871-9a3a-3c591410d432'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

