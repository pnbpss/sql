use serviceweb;
--drop table #T20160225_tempSMSData
select		
left(dbo.fnRemoveNonNumericCharacters(c.TELP),10) PhoneNumber
,''
+'สินเชื่อรถจักรยานยนต์ บ.ตั้งใจยนตรการ ยอดค้างชำระ '
+ replace(CONVERT(varchar, CAST((SELECT isnull(SUM(DAMT-PAYMENT),0) FROM hiincome.dbo.ARPAY WHERE CONTNO=v.CONTNO and DDATE<=getdate()) AS money), 1),'.00','')
+' ชำระล่าสุด ' 
+ case when isnull(lastPaidDate,'')='' then '(ยังไม่เคยชำระ)' else dbo.FN_ConvertDate(lastPaidDate,'D/M/Y') end
+' ขออภัยหากชำระแล้ว ติดต่อกลับ0898745044,0897292502'
 as SMS1
,''
+'เรียนลูกค้า บริษัทขอยืนยันยอดคงเหลือ '
+ replace(CONVERT(varchar, CAST((SELECT isnull(SUM(DAMT-PAYMENT),0) FROM hiincome.dbo.ARPAY WHERE CONTNO=v.CONTNO) AS money), 1),'.00','')
+' ชำระล่าสุด ' 
+ case when isnull(lastPaidDate,'')='' then '(ยังไม่เคยชำระ)' else dbo.FN_ConvertDate(lastPaidDate,'D/M/Y') end
+' หากข้อมูลไม่ถูกต้อง กรุณาติดต่อ บ.ตั้งใจยนตรการ 089-8745044'
 as SMS2
,c.TELP seniorTELP
,case
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),1)<>'0' then '4.เบอร์ผิดพลาด'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='07' then '5.เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='02' then '5.เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='03' then '5.เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='04' then '5.เบอร์บ้าน'
	when left(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10),2)='05' then '5.เบอร์บ้าน'
	when len(left(dbo.fnRemoveNonNumericCharacters(c.TELP),10))!=10 then '4.เบอร์ผิดพลาด'
	when left(TELP,1)='T' then '3.เบอร์ติดต่อไม่ได้'
	when isnull(TELP,'') = '' then '2.ไม่มีเบอร์'
	else '1.OK'
end as numberOk
,v.LOCAT,v.CONTNO
--,(SELECT SUM(DAMT-PAYMENT) FROM hiincome.dbo.ARPAY WHERE CONTNO=v.CONTNO and DDATE<=getdate()) pendingInstallment
--,v.pendingInstallmentPeriod
,v.netPendingPeriod,((a.EXP_TO-a.EXP_FRM)+1) snPending
,dbo.FN_ConvertDate(v.lastPaidDate,'Y/M/D') lastPaidDate
,dbo.FN_ConvertDate(v.nextDealDate,'Y/M/D') nextDealDate
,v.disconnectedDays,v.installmentPerPeriod,v.lastPaidInstall
,dbo.FN_ConvertDate(a.SDATE,'Y/M/D') sdate
--into #T20160225_tempSMSData
from view_arpay2 v 
	 left join wb_armast a on v.CONTNO=a.CONTNO
	 left join wb_customers c on a.CUSCOD=c.CUSCOD
where 
	1=1 
	and v.netPendingPeriod between 1 and 2 
	and disconnectedDays between 30 and 60
	and year(a.SDATE) > (year(getdate()) - 5)
	and a.LOCAT not in ('OFFตจ','OFFตท','OFFทน','OFFยน')
--	and ((a.EXP_TO-a.EXP_FRM)+1)<v.netPendingPeriod
order by a.CONTNO;

