USE [serviceweb]
GO

/****** Object:  Table [dbo].[dc_issuedReturnedJobFromCollectors]    Script Date: 08/16/2015 22:23:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dc_issuedReturnedJobFromCollectors](
	[jobId] [bigint] NOT NULL,
	[assignedId] [bigint] IDENTITY(1,1) NOT NULL,
	[issuedDate] [datetime] NOT NULL,
	[collectorId] [int] NOT NULL,
	[jobTypeId] [int] NOT NULL,
	[returnPOA] [char](1) NOT NULL,
	[returnPOADate] [datetime] NULL,
	[success] [char](1) NOT NULL,
	[appointmentToPayDate] [datetime] NULL,
	[deleted] [char](1) NOT NULL,--add
	[ARCONTNO] [varchar](25) NULL, 
	[comments] [varchar](250) NULL,
	[userId] [varchar](50) NOT NULL, --add
	[lastUpdate] [datetime] NOT NULL, --add
	[ipAddress] [varchar](50) NOT NULL, --add
 CONSTRAINT [PK_dc_issuedReturnedJobFromCollectors] PRIMARY KEY CLUSTERED 
(
	[assignedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'คืน POWER OF ATTORNEY แล้ว' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_issuedReturnedJobFromCollectors', @level2type=N'COLUMN',@level2name=N'returnPOA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'วันที่คืน POWER OF ATTORNEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_issuedReturnedJobFromCollectors', @level2type=N'COLUMN',@level2name=N'returnPOADate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ตามได้/ไม่ได้' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_issuedReturnedJobFromCollectors', @level2type=N'COLUMN',@level2name=N'success'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'วันนัดชำระ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_issuedReturnedJobFromCollectors', @level2type=N'COLUMN',@level2name=N'appointmentToPayDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'เลขที่ลูกหนี้อื่น' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dc_issuedReturnedJobFromCollectors', @level2type=N'COLUMN',@level2name=N'ARCONTNO'
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors]  WITH CHECK ADD  CONSTRAINT [FK_dc_issuedReturnedJobFromCollectors_dc_assignedJobTypes] FOREIGN KEY([jobTypeId])
REFERENCES [dbo].[dc_assignedJobTypes] ([id])
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors] CHECK CONSTRAINT [FK_dc_issuedReturnedJobFromCollectors_dc_assignedJobTypes]
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors]  WITH CHECK ADD  CONSTRAINT [FK_dc_issuedReturnedJobFromCollectors_dc_debtCollectors] FOREIGN KEY([collectorId])
REFERENCES [dbo].[dc_debtCollectors] ([id])
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors] CHECK CONSTRAINT [FK_dc_issuedReturnedJobFromCollectors_dc_debtCollectors]
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors]  WITH CHECK ADD  CONSTRAINT [FK_dc_returnedJobFromCollectors_dc_loadedJobsForAssignToDebtCollector] FOREIGN KEY([jobId])
REFERENCES [dbo].[dc_loadedJobsForAssignToDebtCollector] ([id])
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors] CHECK CONSTRAINT [FK_dc_returnedJobFromCollectors_dc_loadedJobsForAssignToDebtCollector]
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors] ADD  CONSTRAINT [DF_dc_returnedJobFromCollectors_success]  DEFAULT ('N') FOR [success]
GO

ALTER TABLE [dbo].[dc_issuedReturnedJobFromCollectors] ADD  CONSTRAINT [DF_dc_issuedReturnedJobFromCollectors_deleted]  DEFAULT ('N') FOR [deleted]
GO

