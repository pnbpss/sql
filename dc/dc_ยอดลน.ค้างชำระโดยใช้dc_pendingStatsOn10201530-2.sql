select loadDateTime,[1],	[2],	[3],	[4],	[5],	[6],	[7],	[8],	[9],	[10],	[11],	[12],	[13],	[14],	[15],	[16],	[17],	[18],	[19],	[20],	[21],	[22],	[23],	[24],	[25],	[26],	[27],	[28],	[29],	[30],	[31],	[32],	[33],	[34],	[35],	[36],	[37],	[38],	[39],	[40],	[41],	[42],	[43],	[44],	[45],	[46],	[47],	[48],	[49],	[50],	[51],	[52],	[53],	[54],	[55],	[56],	[57],	[58],	[59],	[60]
from (
	select COUNT(*)cnt,netPendingPeriod,convert(varchar,loadDateTime,111)loadDateTime from dc_pendingStatsOn10201530 pds 
	where not (lastpassdue='Y' and netPendingPeriod=0)
	group by netPendingPeriod,convert(varchar,loadDateTime,111)
) t pivot(
	sum(cnt) for netPendingPeriod in ([1],	[2],	[3],	[4],	[5],	[6],	[7],	[8],	[9],	[10],	[11],	[12],	[13],	[14],	[15],	[16],	[17],	[18],	[19],	[20],	[21],	[22],	[23],	[24],	[25],	[26],	[27],	[28],	[29],	[30],	[31],	[32],	[33],	[34],	[35],	[36],	[37],	[38],	[39],	[40],	[41],	[42],	[43],	[44],	[45],	[46],	[47],	[48],	[49],	[50],	[51],	[52],	[53],	[54],	[55],	[56],	[57],	[58],	[59],	[60])
)p
order by loadDateTime desc


select loadDateTime,[DD_000_030],[DD_031_060],[DD_061_090],[DD_091_120],[DD_121_150],[DD_151_180],[DD_181_210],[DD_211_240],[DD_241_170],[DD_271_300],[DD_301_330],[DD_331_360],[DD_361_500],[DD_501_700],[DD_701_900],[DD_901_UP]
from (
	select COUNT(*)cnt,
	aggr.ddRange
	,convert(varchar,loadDateTime,111)loadDateTime 
	from dc_pendingStatsOn10201530 pds 
	cross apply (
		select 
			case 
				when disconnectedDays between 0 and 30 then 'DD_000_030'
				when disconnectedDays between 31 and 60 then 'DD_031_060'
				when disconnectedDays between 61 and 90 then 'DD_061_090'
				when disconnectedDays between 91 and 120 then 'DD_091_120'
				when disconnectedDays between 121 and 150 then 'DD_121_150'
				when disconnectedDays between 151 and 180 then 'DD_151_180'
				when disconnectedDays between 181 and 210 then 'DD_181_210'
				when disconnectedDays between 211 and 240 then 'DD_211_240'
				when disconnectedDays between 241 and 270 then 'DD_241_170'
				when disconnectedDays between 271 and 300 then 'DD_271_300'
				when disconnectedDays between 301 and 330 then 'DD_301_330'
				when disconnectedDays between 331 and 360 then 'DD_331_360'
				when disconnectedDays between 361 and 500 then  'DD_361_500'
				when disconnectedDays between 501 and 700 then  'DD_501_700'
				when disconnectedDays between 701 and 900 then  'DD_701_900'
				when disconnectedDays >= 901 then  'DD_901_UP'
			end ddRange
			)aggr
	where not (lastpassdue='Y' and netPendingPeriod=0)
	group by aggr.ddRange,convert(varchar,loadDateTime,111)
)t pivot (sum(cnt) for ddRange in ([DD_000_030],[DD_031_060],[DD_061_090],[DD_091_120],[DD_121_150],[DD_151_180],[DD_181_210],[DD_211_240],[DD_241_170],[DD_271_300],[DD_301_330],[DD_331_360],[DD_361_500],[DD_501_700],[DD_701_900],[DD_901_UP]))p
order by loadDateTime desc