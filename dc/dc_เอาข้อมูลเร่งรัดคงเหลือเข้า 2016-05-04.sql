/****** Script for SelectTopNRows command from SSMS  ******/
use serviceweb;
SELECT --distinct 		
	   [originalJobId]
      ,[dcName]
      ,[dcEmpId]
      ,dbo.FN_ConvertDate(main.[dateOfIssued],'D/M/Y') dateOfIssued
      ,main.[contno]
      ,convert(varchar,main.[dateOfIssued],111) cdateOfIssued      
      ,[customerName]
      ,[jobStatus]
      ,[supervisorNick]
      ,ar.CONTSTAT
      
      ,case
		when (ah.CONTNO is null and ar.CONTNO is null) then '1.ไม่มีเลขที่สัญญาในระบบซีเนียร์'
		when ah.CONTNO is not null then '2.สัญญาเป็นรถยึดไปแล้ว '
		when chq.CONTNO is not null then '3. มีการชำระแล้วหลังวันที่จ่ายงาน'
		when oth.SMPAY=oth.PAYAMT then '5.มีประวัติชำระค่าติดตามหลังการจ่ายงาน'
      end statusPending
      
      
      ,imported
      ,ar.CONTNO arcontno
      ,ah.CONTNO ahcontno
      ,dbo.FN_ConvertDate(ah.YDATE,'D/M/Y') Ydate
      ,dbo.FN_ConvertDate(oth.ARDATE,'D/M/Y') arDate
      ,oth.PAYAMT othPayamt,oth.ARCONT,oth.SMPAY,oth.BALANCE
      
      ,chq.TMBILL,dbo.FN_ConvertDate(chq.TMBILDT,'D/M/Y') tmbildt,chq.CONTNO chqCONTNO
      ,va.netPendingPeriod,va.lastpassdue,va.disconnectedDays,dbo.FN_ConvertDate(va.lastPaidDate,'D/M/Y') lastPaidDate      
      
      ,dbo.fn_getJobIdForImportDCStart(main.contno,main.dateOfIssued) as jobId
      into #tempOk
  FROM [serviceweb].[dbo].[dc_tempRemainJobBeforeUseProgram] main
  inner join 
  (select contno,max(dateOfIssued) dateOfIssued from dc_tempRemainJobBeforeUseProgram group by contno) mxdi on main.contno=mxdi.contno and main.dateOfIssued=mxdi.dateOfIssued
  left join wb_armast ar on main.contno=ar.CONTNO
  left join wb_arhold ah on main.contno=ah.CONTNO
  left join wb_chqtran chq on (main.contno=chq.CONTNO and chq.TMBILDT>=main.dateOfIssued and chq.FLAG<>'C' and chq.PAYFOR in ('006','007'))
  left join view_arpay2 va on main.contno=va.CONTNO 
  left join dc_SeniorAROTHR oth on main.contno=oth.CONTNO and oth.ARDATE>=main.dateOfIssued 

  where 1=1
  and main.imported='N'
  --and ah.CONTNO is null and ar.CONTNO is null
  --and ah.CONTNO is not null
  --and chq.CONTNO is not null  
  --and not ((ah.CONTNO is null and ar.CONTNO is null) or (ah.CONTNO is not null) or (oth.CONTNO is not null and oth.SMPAY<>0) or (dbo.fn_getJobIdForImportDCStart(main.contno,main.dateOfIssued) is null))
  --and ((ah.CONTNO is null and ar.CONTNO is null) or (ah.CONTNO is not null) or (oth.CONTNO is not null and oth.SMPAY<>0) or (dbo.fn_getJobIdForImportDCStart(main.contno,main.dateOfIssued) is null))
  --and ((ah.CONTNO is null and ar.CONTNO is null) or (ah.CONTNO is not null) or (oth.CONTNO is not null and oth.SMPAY<>0))
  --and ah.CONTNO is not null and chq.CONTNO is not null
  --and oth.CONTNO is not null and oth.SMPAY<>0
  --and va.lastpassdue='N' and va.netPendingPeriod<3
  --and va.lastpassdue='N' and va.netPendingPeriod<3
  --and va.disconnectedDays > 900
  --and dbo.fn_getJobIdForImportDCStart(main.contno,main.dateOfIssued) is not null
  --and main.contno='๕HP-15060013'
  --and chq.TMBILDT>'2016/04/30' and main.contno in ('นHP-13080002','๕HP-15080053','๕HP-15090064','๕HP-14070010','$HP-14080029','$HP-15120032','$HP-16010028','$HP-16010025')
  --and main.contno like  'ฃHP%'
  --and ah.YDATE>main.dateOfIssued and ah.YDATE>'20160501' and ARCONT<>'ผAR-16030007'
  --and main.contno like 'ดHP-15070017'-- and TMBILL!='/TB-16050014'
  and main.contno in 
  ('&HP-14090034',
'&HP-15090020',
'&HP-15110013',
'&HP-15120013',
'&HP-16020024',
'&HP-14020018',
'&HP-15020058')
--and chq.TMBILL not in ('ฤTB-16040478','ฤTB-16050085')
order by main.contno
 
--alter table dc_tempRemainJobBeforeUseProgram add imported char(1) not null default ('N')
--update dc_tempRemainJobBeforeUseProgram set imported='Y'
--delete from dc_tempRemainJobBeforeUseProgram where imported='N'

--drop table #tempOk;

/*
select 
distinct
'insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)'
+
'values ('''+convert(varchar(max),jobId)+''','''+cdateOfIssued+''','''+dcEmpId+''',''100'',''ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504'',1,getdate(),''192.168.0.170'');' sqlins
,''''+a.contno+''',' cntnoupd
--,a.contno
--,*
from #tempOk a inner join (select max(ARCONT) 
ARCONT,contno from #tempOk group by contno) b on a.contno=b.contno and isnull(a.ARCONT,'1')=isnull(b.ARCONT,'1')
*/


/*

drop table #tempOk
update dc_tempRemainJobBeforeUseProgram set contno='ทHP-15020001' where contno='ทHP-15020007'

กุ้ง
update dc_tempRemainJobBeforeUseProgram set contno='ซHP-13050081' where contno='ซHP-13080001';
update dc_tempRemainJobBeforeUseProgram set contno='กHP-13100010' where contno='กHP-131000010';
update dc_tempRemainJobBeforeUseProgram set contno='ซHP-10030077' where contno='ซHP-12120002';
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1638728','2016/02/09','47','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1639108','2016/03/17','104','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1640603','2016/02/09','23','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('ซHP-10030077','กHP-13100010','ซHP-13050081')
update dc_tempRemainJobBeforeUseProgram set contno='ซHP-15120016' where contno in ('ซHP-15120011')
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1697640','2016/04/21','76','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ซAR-16040048');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('ซHP-15120016')
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1645556','2016/03/15','23','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ดAR-16030031');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1649795','2016/02/24','20','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ผAR-16050008');

insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1645556','2016/03/15','23','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ดAR-16030031');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1649795','2016/02/24','20','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ผAR-16050008');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('ดHP-15070017',
'ผHP-15070028')

แหม่ม
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1695226','2016/04/20','66','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ฃAR-16040014');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('ฃHP-15090003')
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1636319','2016/03/30','61','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('คHP-14050030')

insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1615108','2016/03/09','65','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1619072','2016/04/07','65','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1619340','2016/04/07','65','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1628128','2016/03/15','49','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1628281','2016/03/29','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','qAR-16030038');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1635477','2016/02/13','39','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ฆAR-16040038');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1653661','2016/03/16','45','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1654342','2016/02/24','45','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1654667','2016/03/29','88','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','อAR-16040009');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1664819','2016/04/12','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','qAR-16040011');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1679624','2016/04/15','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1685643','2016/04/19','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1704411','2016/04/22','63','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1707286','2016/04/22','63','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','1');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1720923','2016/04/25','65','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ฆAR-16040039');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress,ARCONTNO)values ('1730650','2016/04/23','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170','ฤAR-16040024');
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in  ('/HP-15020001')
update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in ('สHP-14090002')
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1647968','2016/04/08','71','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1708448','2016/04/27','80','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1710164','2016/04/27','80','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1734941','2016/04/27','80','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1737041','2016/04/29','80','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1763072','2016/05/03','40','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1764432','2016/05/03','40','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1765418','2016/05/03','23','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');


delete from dbo.dc_SeniorAROTHR where ARCONT='ฬAR-16050010'

update dc_tempRemainJobBeforeUseProgram set contno='ขHP-14100040' where contno='ขHP-14100046'
update dc_tempRemainJobBeforeUseProgram set contno='ขHP-14070039' where contno='ขHP-14070008'
update dc_tempRemainJobBeforeUseProgram set contno='ขHP-14010051' where contno='ขHP-14090002'


insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1636855','2016/03/19','66','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','ศิลป์ชัย รอดคืน','45','2016/04/06','สHP-14090002','','','');

select * from dc_tempRemainJobBeforeUseProgram where contno ='สHP-14090002'

insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1681602','2016/04/06','45','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');


insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1614568','2016/04/19','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1619403','2016/03/07','82','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1619902','2016/03/07','82','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1621195','2016/03/07','82','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1623658','2016/03/07','85','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1659674','2016/04/18','85','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1660188','2016/04/18','85','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1661327','2016/04/18','85','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1679624','2016/04/15','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1686795','2016/04/19','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1729698','2016/04/23','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1730170','2016/04/23','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1730650','2016/04/23','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1731074','2016/04/23','73','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1792529','2016/05/07','84','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');

select * from wb_arpay where CONTNO='฿HP-14020018'
select * from wb_arpay where CONTNO='ฤHP-15090030'



'vHP-15040018 ',
'vHP-13010008',
'ถHP-12060024',
'NHP-1405001',
'NHP-14100016',
'ยHP-15040006',
'ยHP-15120007',
'ยHP-15090046',
'ยHP-15050005',
'ยHP-15050015',
'ยHP-13120008',
'ตHP-15040001'

*/



/*
delete from dc_tempRemainJobBeforeUseProgram where contno in ('ฤHP-13120035',
'6HP-14080001',
'6HP-15080003',
'6HP-15070004',
'6HP-15010015',
'9HP-13110019',
'9HP-13100023',
'9HP-14030044',
'ฤHP-15090028',
'ฤHP-15090030',
'ฤHP-15030019',
'ฤHP-15090005',
'ฤHP-15100017',
'฿HP-14050003',
'฿HP-15070015',
'฿HP-14020018',
'฿HP-15110012')

ฝน
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-14090034','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-15090020','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-15110013','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-15120013','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-16020024','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-14020018','','','');
insert into dc_tempRemainJobBeforeUseProgram (originalJobId,dcName,dcEmpId,dateOfIssued,contno,customerName,jobStatus,supervisorNick)values('','เทพกร  เรืองขำ','43','2016/05/10','&HP-15020058','','','');

insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1787332','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1789063','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1814581','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1814982','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1815450','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1815638','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');
insert into dc_issuedReturnedJobFromCollectors ([jobId] ,[issuedDate],[collectorId],[jobTypeId],[comments],userId,lastUpdate,ipAddress)values ('1815665','2016/05/10','43','100','ภาณุ เอาเร่งรัดคงเหลือเข้าให้20160504',1,getdate(),'192.168.0.170');

update dc_tempRemainJobBeforeUseProgram set imported='Y' where contno in 
('&HP-14090034',
'&HP-15090020',
'&HP-15110013',
'&HP-15120013',
'&HP-16020024',
'&HP-14020018',
'&HP-15020058')


*/