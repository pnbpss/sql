/*
drop table #tt;
select
--top 50
va.CONTNO,i.STRNO,dbo.removeFFromStrno(i.STRNO)strnoNoF,va.startContract,va.netPendingPeriod,va.disconnectedDays 
,datediff(year,c.BIRTHDT,ar.SDATE)cAge,c.SNAM
,case when datediff(year,c.BIRTHDT,ar.SDATE) between 0 and 20 then '00_20'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 21 and 25 then '21_25'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 26 and 30 then '26_30'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 31 and 35 then '31_35'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 36 and 40 then '36_40'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 41 and 45 then '41_45'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 46 and 50 then '46_50'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 51 and 55 then '51_55'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 56 and 60 then '56_60'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 61 and 65 then '61_65'
	when datediff(year,c.BIRTHDT,ar.SDATE) between 66 and 70 then '66_70'
	when datediff(year,c.BIRTHDT,ar.SDATE) > 70 then '71_UP'
	else 'unknow'
end ageRange
,case when netPendingPeriod=0 then 'P00_00'
				when netPendingPeriod=1 then 'P01_01'
				when netPendingPeriod=2 then 'P02_02'
				when netPendingPeriod=3 then 'P03_03'
				when netPendingPeriod=4 then 'P04_04'
				when netPendingPeriod between 5 and 5 then 'P05_05'
				when netPendingPeriod between 6 and 10 then 'P06_10'    
				when netPendingPeriod > 10 then 'P11_MM'    
end pdType
,va.lastpassdue
into #tt
from view_arpay2 va inner join wb_invtran i on va.CONTNO=i.CONTNO 
inner join wb_armast ar on i.CONTNO=ar.CONTNO
inner join wb_custmast c on ar.CUSCOD=c.CUSCOD
order by i.STRNO
*/

--select * from #tt where ageRange!='unknow' and not (netPendingPeriod=0 and lastpassdue='Y');

select COUNT(*)cnt,SNAM,ageRange,pdType from #tt where ageRange!='unknow' and not (netPendingPeriod=0 and lastpassdue='Y') group by SNAM,ageRange,pdType;

