use serviceweb;
IF OBJECT_ID('tempdb.dbo.#abc', 'U') IS NOT NULL DROP TABLE #abc;
IF OBJECT_ID('tempdb.dbo.#def', 'U') IS NOT NULL DROP TABLE #def;
declare @startDate datetime, @endDate datetime, @sumG int, @sumB int, @sumA int;
set @startDate='2016/05/01'; set @endDate = '2016/05/31'
select 
locat,contno,collectorId cid
,case when closed='N' then 'N'
	else 'Y'
end
+
case when isnull(ARCONTNO,'')='' then 'N'
	else 'Y'
end
+case when closed='P' then 'Y'
	else 'N'
end
+case when closed='S' then 'Y'
	else 'N'
end
+case when closed='R' then 'Y'
	else 'N'
end
+case when closed='F' then 'Y'
	else 'N'
end
+case when closed='C' then 'Y'
	else 'N'
end
+case when irj.cntAllPresent=0 then 'N'
	else 'Y'
end CAPSRFCP
,case when closed='N' then DATEDIFF(day,issuedDate,GETDATE()) 
else DATEDIFF(day,issuedDate,closedDate) 
end ddiff
,ajs.crDisconnectedDays dd
,ajs.crPendingPeriod pd
,irj.cntAllPresent cp
into #abc
from dc_allAssignedJobs ajs cross apply dbo.IRJStatusNoneCF(ajs.assignedId) irj
where 1=1
and ajs.issuedDate between @startDate and @endDate
--goto endScript
--select distinct CAPSRFCP from #abc order by CAPSRFCP desc



--��ṡ��������
select dc.fname+' '+dc.sname dcName
,isnull(NNNNNNNN,0) NNNNNNNN,	isnull(NNNNNNNY,0) NNNNNNNY,	isnull(NYNNNNNN,0) NYNNNNNN,	isnull(NYNNNNNY,0) NYNNNNNY,	isnull(YNNNNNYN,0) YNNNNNYN,	isnull(YNNNNYNN,0) YNNNNYNN,	isnull(YNNNYNNY,0) YNNNYNNY,	isnull(YNNYNNNY,0) YNNYNNNY,	isnull(YYNNNNYN,0) YYNNNNYN,	isnull(YYNNNYNN,0) YYNNNYNN,	isnull(YYNYNNNY,0) YYNYNNNY,	isnull(YYYNNNNY,0) YYYNNNNY
,SG.val sumG
,SB.val sumB
,SA.val sumAll
,case when SA.val=0 then 0 else (SG.val*100.00)/SA.val end pG
,case when SA.val=0 then 0 else (SB.val*100.00)/SA.val end pB
,avgh.avgddif,avgdd,avgpd,sumcp,avgcp
into #def
from (
select count(*) cntsts
,cid,CAPSRFCP from #abc group by cid,CAPSRFCP
) t
pivot
(
	sum(cntsts) for CAPSRFCP in ([NNNNNNNN],	[NNNNNNNY],	[NYNNNNNN],	[NYNNNNNY],	[YNNNNNYN],	[YNNNNYNN],	[YNNNYNNY],	[YNNYNNNY],	[YYNNNNYN],	[YYNNNYNN],	[YYNYNNNY],	[YYYNNNNY])
) p
left join(
	select avg(ddiff*1.00) avgddif,cid from #abc group by cid
) avgh on p.cid=avgh.cid
left join(
	select avg(dd*1.00) avgdd,cid from #abc group by cid
) avgdd on p.cid=avgdd.cid
left join(
	select avg(pd*1.00) avgpd,cid from #abc group by cid
) avgpd on p.cid=avgpd.cid
left join(
	select sum(cp) sumcp,cid from #abc group by cid
) sumcp on p.cid=sumcp.cid
left join(
	select avg(cp*1.00) avgcp,cid from #abc group by cid
) avgcp on p.cid=avgcp.cid
left join dc_debtCollectors dc on p.cid=dc.id
cross apply (select (isnull(NNNNNNNN,0)+	isnull(NNNNNNNY,0)+	isnull(NYNNNNNN,0)+	isnull(NYNNNNNY,0)+	isnull(YNNNNNYN,0)+	isnull(YNNNNYNN,0)+	isnull(YNNNYNNY,0)+	isnull(YNNYNNNY,0)+	isnull(YYNNNNYN,0)+	isnull(YYNNNYNN,0)+	isnull(YYNYNNNY,0)+	isnull(YYYNNNNY,0))) as SA(val)
cross apply (select (						isnull(YNNNYNNY,0)+	isnull(YNNYNNNY,0)+			isnull(YYNYNNNY,0)+	isnull(YYYNNNNY,0))) as SG(val)
cross apply (select (isnull(NNNNNNNN,0)+	isnull(NNNNNNNY,0)+	isnull(NYNNNNNN,0)+	isnull(NYNNNNNY,0)+	isnull(YNNNNNYN,0)+	isnull(YNNNNYNN,0)+			isnull(YYNNNNYN,0)+	isnull(YYNNNYNN,0))) as SB(val)

--goto endScript


select dcName,NNNNNNNN,	NNNNNNNY,	NYNNNNNN,	NYNNNNNY,	YNNNNNYN,	YNNNNYNN,	YNNNYNNY,	YNNYNNNY,	YYNNNNYN,	YYNNNYNN,	YYNYNNNY,	YYYNNNNY
,sumG,sumB,sumAll,pG,pB,avgddif,avgdd,avgpd,sumcp,avgcp from #def
union select '���� ���� �����' dcName
,sum(NNNNNNNN) NNNNNNNN,	sum(NNNNNNNY) NNNNNNNY,	sum(NYNNNNNN) NYNNNNNN,	sum(NYNNNNNY) NYNNNNNY,	sum(YNNNNNYN) YNNNNNYN,	sum(YNNNNYNN) YNNNNYNN,	sum(YNNNYNNY) YNNNYNNY,	sum(YNNYNNNY) YNNYNNNY,	sum(YYNNNNYN) YYNNNNYN,	sum(YYNNNYNN) YYNNNYNN,	sum(YYNYNNNY) YYNYNNNY,	sum(YYYNNNNY) YYYNNNNY
,sum(sumG) sumG,	sum(sumB) sumB,	sum(sumAll) sumAll ,sum(sumG*1.00)*100/sum(sumAll) pG,sum(sumB*1.00)*100/sum(sumAll) pB,avg(avgddif) avgddif,avg(avgdd) avgdd,avg(avgpd) avgpd,sum(sumcp) sumcp,avg(avgcp) avgcp from #def


--select distinct CAPSRFC from #abc
/*
--��ṡ����Ң�
select p.locat
,isnull(NNNNNNN,0) NNNNNNN	,isnull(NYNNNNN,0) NYNNNNN	,isnull(YNNNNYN,0) YNNNNYN	,isnull(YNNNNNY,0) YNNNNNY	,isnull(YYNNNNY,0) YYNNNNY	,isnull(YYYNNNN,0) YYYNNNN	,isnull(YYNYNNN,0) YYNYNNN	,isnull(YNNNYNN,0) YNNNYNN	,isnull(YYNNNYN,0) YYNNNYN	,isnull(YYNNYNN,0) YYNNYNN	,isnull(YNNYNNN,0) YNNYNNN
,SG.val sumG
,SB.val sumB
,SA.val sumAll
,case when SA.val=0 then 0 else (SG.val*100.00)/SA.val end pG
,case when SA.val=0 then 0 else (SB.val*100.00)/SA.val end pB
,avgh.avgddif,avgdd,avgpd,sumcp,avgcp
from (
select count(*) cntsts
,locat,CAPSRFC from #abc group by locat,CAPSRFC
) t
pivot
(
	sum(cntsts) for CAPSRFC in ([YNNNNNY],[YYYNNNN],[YYNYNNN],[YNNNNYN],[YNNNYNN],[NNNNNNN],[YYNNNYN],[YYNNYNN],[YNNYNNN],[YYNNNNY],[NYNNNNN])
) p
left join(
	select avg(ddiff*1.00) avgddif,locat from #abc group by locat
) avgh on p.locat=avgh.locat
left join(
	select avg(dd*1.00) avgdd,locat from #abc group by locat
) avgdd on p.locat=avgdd.locat
left join(
	select avg(pd*1.00) avgpd,locat from #abc group by locat
) avgpd on p.locat=avgpd.locat
left join(
	select sum(cp) sumcp,locat from #abc group by locat
) sumcp on p.locat=sumcp.locat
left join(
	select avg(cp*1.00) avgcp,locat from #abc group by locat
) avgcp on p.locat=avgcp.locat
cross apply (select (isnull(YYYNNNN,0)+isnull(YYNYNNN,0)+isnull(YNNNYNN,0)+isnull(YYNNYNN,0)+isnull(YNNYNNN,0)+isnull(NNNNNNN,0)+isnull(NYNNNNN,0)+isnull(YYNNNYN,0)+isnull(YNNNNYN,0)+isnull(YNNNNNY,0)+isnull(YYNNNNY,0))) as SA(val)
cross apply (select (isnull(YYYNNNN,0)+isnull(YYNYNNN,0)+isnull(YNNNYNN,0)+isnull(YYNNYNN,0)+isnull(YNNYNNN,0))) as SG(val)
cross apply (select (isnull(NNNNNNN,0)+isnull(NYNNNNN,0)+isnull(YYNNNYN,0)+isnull(YNNNNYN,0)+isnull(YNNNNNY,0)+isnull(YYNNNNY,0))) as SB(val)
order by sumG desc
*/
endScript: