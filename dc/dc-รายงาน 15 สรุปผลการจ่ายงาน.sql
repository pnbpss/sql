use serviceweb;
/*
select distinct ljf.loadDate
,(select COUNT(*) from dc_loadedJobsForAssignToDebtCollector where loadDate=ljf.loadDate) cnt

from dc_loadedJobsForAssignToDebtCollector ljf 
order by ljf.loadDate desc;
*/
/*
drop table #tempTable
declare @maxLoadDate datetime;
set @maxLoadDate = (select max(loadDate) from dc_loadedJobsForAssignToDebtCollector);
	select 	
	lcc.locat,ljf.id jobId,ljf.loadDate,ljf.contno,ljf.crPendingPeriod,ljf.crDisconnectedDays,irj.collectorId,irj.assignedId
	,isnull(cas.fname,'n/a') sup
	,  case
		when ljf.crPendingPeriod<=1 then ' 1 ปกติ+1 งวด'
		when ljf.crPendingPeriod=2 and ljf.crDisconnectedDays >=0 and ljf.crDisconnectedDays<=15 then ' 2 ค้าง 2 งวด ขาดการติดต่อ 0-15 วัน'
		when ljf.crPendingPeriod=2 and ljf.crDisconnectedDays >=16 and ljf.crDisconnectedDays<=90 then ' 3 ค้าง 2 งวด ขาดการติดต่อ 16-90 วัน'
		when ljf.crPendingPeriod=2 and ljf.crDisconnectedDays >90 then ' 4 ค้าง 2 งวด ขาดการติดต่อมากกว่า 90 วัน'
		when ljf.crPendingPeriod>=3 and ljf.crDisconnectedDays >=0 and ljf.crDisconnectedDays <=15 then ' 5 ค้างตั้งแต่ 3 งวดขึ้นไป ขาดการติดต่อ 0-15 วัน	'
		when ljf.crPendingPeriod>=3 and ljf.crDisconnectedDays >=16 and ljf.crDisconnectedDays <=90 then ' 6 ค้างตั้งแต่ 3 งวดขึ้นไป ขาดการติดต่อ 16-90 วัน	'
		when ljf.crPendingPeriod=3 and ljf.crDisconnectedDays >=90 then ' 7 ค้าง 3 งวดขาดการติดต่อมากกว่า 90 วัน	'
		when ljf.crPendingPeriod between 4 and 6 then ' 8 ค้างตั้งแต่ 4-6 งวด'
		when ljf.crPendingPeriod between 7 and 9 then ' 9 ค้างตั้งแต่ 7-9 งวด'
		when ljf.crPendingPeriod between 10 and 12 then '10 ค้างตั้งแต่ 10-12 งวด'
		when ljf.crPendingPeriod>12 then '11 ค้างมากกว่า 12 งวด'
	end pendingStatus
	,case
		when ljf.id=irj.jobId then 'c'
		else 'o'
	end [current]
	into #tempTable
	from dc_loadedContForCollection lcc
	left join dc_loadedJobsForAssignToDebtCollector ljf on lcc.contno=ljf.contno
	left join dc_beingOnCollectionConts irj on ljf.contno=irj.contno
	left join dc_currentAssignedSupervisorToBranch cas on lcc.locat=cas.branchId
	where 1=1 and ljf.loadDate=@maxLoadDate
	--and irj.assignedId is not null
*/
--select a.locat,a.jobId,a.loadDate,a.contno,a.crPendingPeriod,a.crDisconnectedDays,a.collectorId,a.assignedId,a.sup from #tempTable a


select pendingStatus,isnull([A],0) A,isnull([I],0) I,isnull([B],0) B, isnull([N],0) N 
,(
(isnull([I],0)+isnull([B],0))
*100/isnull([A],0)) P
from (
	select 'A' [status], count(*) cnt,a.pendingStatus 
	from #tempTable a 
	group by a.pendingStatus
	union
	select 'I' [status], count(*) cnt,a.pendingStatus 
	from #tempTable a where collectorId is not null and [current]='c'
	group by a.pendingStatus
	union
	select 'B' [status], count(*) cnt,a.pendingStatus 
	from #tempTable a where collectorId is not null and [current]='o'
	group by a.pendingStatus
	union
	select 'N' [status], count(*) cnt,a.pendingStatus 
	from #tempTable a where collectorId is null
	group by a.pendingStatus
	
	union
	select 'A' [status], count(*) cnt, 'รวมทั้งหมด' pendingStatus 
	from #tempTable a 	
	union
	select 'I' [status], count(*) cnt, 'รวมทั้งหมด'  pendingStatus 
	from #tempTable a where collectorId is not null	and [current]='c'
	union
	select 'B' [status], count(*) cnt, 'รวมทั้งหมด' pendingStatus 
	from #tempTable a where collectorId is not null	and [current]='o'
	union
	select 'N' [status], count(*) cnt, 'รวมทั้งหมด' pendingStatus 
	from #tempTable a where collectorId is null	
) t 
pivot
(
	sum(cnt) for [status] in ([A],[I],[B],[N])
) p
order by pendingStatus

/*
select locat,pendingStatus,isnull([A],0) A,isnull([I],0) I, isnull([B],0) B, isnull([N],0) N 
,((isnull([I],0)+isnull([B],0))*100/isnull([A],0)) P
from (
	select 'A' [status], count(*) cnt, a.locat,a.pendingStatus 
	from #tempTable a 
	group by a.locat,a.pendingStatus
	union
	select 'I' [status], count(*) cnt, a.locat,a.pendingStatus 
	from #tempTable a where collectorId is not null and [current]='c'
	group by a.locat,a.pendingStatus
	union
	select 'B' [status], count(*) cnt, a.locat,a.pendingStatus 
	from #tempTable a where collectorId is not null and [current]='o'
	group by a.locat,a.pendingStatus
	union
	select 'N' [status], count(*) cnt, a.locat,a.pendingStatus 
	from #tempTable a where collectorId is null
	group by a.locat,a.pendingStatus
	union
	select 'A' [status], count(*) cnt, a.locat, 'รวมสาขา'+a.locat pendingStatus 
	from #tempTable a 
	group by a.locat
	union
	select 'I' [status], count(*) cnt, a.locat,'รวมสาขา'+a.locat pendingStatus 
	from #tempTable a where collectorId is not null and [current]='c'
	group by a.locat
	union
	select 'B' [status], count(*) cnt, a.locat,'รวมสาขา'+a.locat pendingStatus 
	from #tempTable a where collectorId is not null and [current]='o'
	group by a.locat	
	union
	select 'N' [status], count(*) cnt, a.locat,'รวมสาขา'+a.locat pendingStatus 
	from #tempTable a where collectorId is null
	group by a.locat
	
	union
	select 'A' [status], count(*) cnt, 'รวมทั้งหมด' locat,'' pendingStatus 
	from #tempTable a 	
	union
	select 'I' [status], count(*) cnt, 'รวมทั้งหมด' locat,'' pendingStatus 
	from #tempTable a where collectorId is not null	and [current]='c'
	union
	select 'B' [status], count(*) cnt, 'รวมทั้งหมด' locat,'' pendingStatus 
	from #tempTable a where collectorId is not null	and [current]='o'
	union
	select 'N' [status], count(*) cnt, 'รวมทั้งหมด' locat,'' pendingStatus 
	from #tempTable a where collectorId is null	
	
) t 
pivot
(
	sum(cnt) for [status] in ([A],[I],[B],[N])
) p
order by locat, pendingStatus
*/