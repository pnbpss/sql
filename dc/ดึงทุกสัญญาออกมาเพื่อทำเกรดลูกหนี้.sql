use serviceweb
select 
--top 5000
--va.*
i.[TYPE],i.MODEL
--,i.BAAB
,i.GCODE
,dbo.FN_ConvertDate(i.SDATE,'Y/M/D')sdate
,case 
	when i.GCODE in ('05','051','052','053') Then 'F'
	else i.STAT	
end sType
,va.CONTNO,va.LOCAT
,amp.AUMPDES,amp.PROVDES
,dbo.FN_ConvertDate(startContract,'Y/M/D')startContract
,dbo.FN_ConvertDate(endContract,'Y/M/D')endContract
,isnull(dbo.FN_ConvertDate(lastPaidDate,'Y/M/D'),'')lastPaidDate
,installmentPerPeriod,lastPaidInstallmentPeriod,lastPaidInstall,AllPeriod,currentperiod,disconnectedDays
--,pendingInstallmentPeriod
,netPendingPeriod,lastpassdue,neverpaid
,isnull(dbo.FN_ConvertDate(nextDealDate,'Y/M/D'),'')nextDealDate
,ar.CONTSTAT
from view_arpay2 va 
inner join wb_invtran i on va.CONTNO=i.CONTNO
left join wb_branches br on va.LOCAT=br.locatcd
left join wb_armast ar on va.CONTNO=ar.CONTNO
left join wb_ampr amp on br.amphur=amp.AUMPCOD
where 
not(va.netPendingPeriod=0 and lastpassdue='Y')
and 
not(va.installmentPerPeriod=va.lastPaidInstall and lastPaidInstallmentPeriod=va.AllPeriod and va.netPendingPeriod=0) 