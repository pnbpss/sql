declare @assignedId int;
set @assignedId=42
select 
 (select count(*) from dc_collectorJobPresentations where assignedId=@assignedId) cntAllPresent
 ,(select count(*) from dc_collectorJobPresentations where assignedId=@assignedId and resultId='2') cntFoundCustomerPresent
 ,(select case when count(*)>0 then 'Y' else 'N' end from dc_issuedReturnedJobFromCollectors where assignedId=@assignedId and ARCONTNO is not null) Oth
 ,isnull((select case when oth.SMPAY=0 then 'NP' when oth.SMPAY=oth.PAYAMT then 'PF' when oth.SMPAY>oth.PAYAMT then 'PM' else 'PP'end paid from dc_issuedReturnedJobFromCollectors irj inner join dc_SeniorAROTHRTEST oth on irj.ARCONTNO=oth.ARCONT where irj.assignedId=@assignedId),'NA') paid
 ,(select case when paid='Y' then 'Y' when seized='Y' then 'Y' when forwarded='Y' then 'Y' else 'N' end from dc_issuedReturnedJobFromCollectors where assignedId=@assignedId) closed

/*
select top 5 * from dc_issuedReturnedJobFromCollectors where deleted<>'Y' ;
select distinct assignedId from dc_collectorJobPresentations where deleted<>'Y' 

--update dc_issuedReturnedJobFromCollectors set seized='N',paid='N',forwarded='N' where assignedId='1'
select 
	 assignedId,case when paid='Y' then 'Y'
	 when seized='Y' then 'Y'
	 when forwarded='Y' then 'Y'
	 else 'N'
	 end 
from dc_issuedReturnedJobFromCollectors order by assignedId
--select top 5 * from dc_SeniorAROTHRTEST where ARCONT = '&AR-15100006';
--update dc_SeniorAROTHRTEST set SMPAY=900 where ARCONT = '&AR-15090001';
--update dc_issuedReturnedJobFromCollectors set closed='N' where assignedId='1125';
--update dc_issuedReturnedJobFromCollectors set closed='N' where assignedId='1125';
select * from dc_issuedReturnedJobFromCollectors where assignedId='1125'
*/