/****** template เพื่อสร้าง USER  ******/
use HIINCOME;
SELECT [DEPCOD]
      ,[SYSTEMCOD]
      ,[MENUCODE]
      ,[MENUDESC]
      ,[M_ACCESS]
      ,[M_EDIT]
      ,[M_INSERT]
      ,[M_DELETE]
      ,[MORDER]
  FROM [HIINCOME].[dbo].[SETMNUTR]
  where 1=1
  and ((MENUDESC like 'บันทึกลูกหนี้ขอผลัดดิว') or (MENUDESC like  'บันทึกลูกหนี้ขอผลัดคิว'))
  --and (M_EDIT='T' or M_INSERT='T' or M_DELETE='T')
  and ((SYSTEMCOD='SYS15' and MENUCODE='HINCI_AR01') or ( SYSTEMCOD='SYS05' and MENUCODE='AR120'))
  

/****** ของแต่ละ user  ******/
SELECT [USERID]
      ,[SYSTEMCOD]
      ,[MENUCODE]
      ,[MENUDESC]
      ,[M_ACCESS]
      ,[M_EDIT]
      ,[M_INSERT]
      ,[M_DELETE]
      ,[MORDER]
  FROM [HIINCOME].[dbo].[MENUTRN]
  where 1=1
  --and USERID='40' 
  and ((MENUDESC like 'บันทึกลูกหนี้ขอผลัดดิว') or (MENUDESC like  'บันทึกลูกหนี้ขอผลัดคิว'))
  --and (M_EDIT='T' or M_INSERT='T' or M_DELETE='T')
  and ((SYSTEMCOD='SYS15' and MENUCODE='HINCI_AR01') or ( SYSTEMCOD='SYS05' and MENUCODE='AR120'))
  
  --select * from PASSWRD where USERID='1999' 
  --select * from PASSWRD where LEVEL_1='1' and EXPDATE is null
  --update PASSWRD set LEVEL_1='2' where USERID='40'
  --update PASSWRD set DEPCODE='MM' where USERID='40'
  --update [HIINCOME].[dbo].[MENUTRN] SET M_ACCESS='F',M_EDIT='F',M_INSERT='F',M_DELETE='F' where USERID='40' and SYSTEMCOD='SYS15' and MENUCODE='HINCI_AR01'
  --update [HIINCOME].[dbo].[MENUTRN] SET M_ACCESS='F',M_EDIT='F',M_INSERT='F',M_DELETE='F' where USERID='40' and SYSTEMCOD='SYS05' and MENUCODE='AR327'
  --select * from PASSWRD where USERID='40'
  --select * from PASSWRD where USERID='1398'
  --select * from PASSWRD where USERID='1999'