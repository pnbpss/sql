

select 
--asj1.*,
asj1.contno
,asj1.dcTFSName
,asj1.assignedId,asj1.issuedDate, asj1.closedDate
,u1.name closedBy
,asj1.crPendingPeriod,asj1.crDisconnectedDays,asj1.loadDate
,datediff(day,asj1.issuedDate,asj1.closedDate) holdDays
--,u1.*
,'|' sp
,asj2.assignedId,asj2.issuedDate,asj2.userId,asj2.lastUpdate
,asj2.crPendingPeriod,asj2.crDisconnectedDays,asj2.loadDate, asj2.closed
,u2.name lastUpdateBy
from dc_allAssignedJobs asj1 
left join dc_users u1 on asj1.closedBy=u1.id
left join dc_allAssignedJobs asj2 on 
asj1.contno=asj2.contno 
and asj1.collectorId=asj2.collectorId 
and asj1.closedDate<=asj2.issuedDate
left join dc_users u2 on asj2.userId=u2.id
where asj1.closed in ('F','C') and asj2.jobId is not null
order by asj1.contno