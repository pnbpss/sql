use serviceweb;
/*
declare @t table(d datetime,t datetime);
insert into @t values('20160501','20160501 18:59');
select * from @t;
*/
/*

select 
--sum(NETPAY)smpay
count(*)cntpay
,month(YDATE)m, year(YDATE)y
from wb_arhold
where 1=1
--and FLAG<>'C'
--and PAYFOR in ('006','007')
group by month(YDATE), year(YDATE)
order by y desc,m desc

select ah.LOCAT,ah.CONTNO,ah.STRNO,dbo.FN_ConvertDate(ah.YDATE,'Y/M/D') YDATE,ah.*
--,asj.* 
from wb_arhold ah left join dc_allAssignedJobs asj on ah.CONTNO=asj.contno
where month(YDATE) between 5 and 7 and year(YDATE)=2016 and asj.contno is null
and CONTNO='�HP-15120018'
--order by LOCAT,YDATE desc
;
--select * from wb_arhold where CONTNO='gHP-11120001'
*/

select asj.dcTFSName,asj.contno,asj.locat
,dbo.FN_ConvertDate(asj.issuedDate,'D/M/Y') issuedDate
,dbo.FN_ConvertDate(asj.closedDate,'D/M/Y') closedDate
,asj.closed
,asj.installmentBillNoOne bill1
,dbo.FN_ConvertDate(chq1.TMBILDT,'D/M/Y')  bil1Date
,chq1.NETPAY bill1NetPay
,chq2.TMBILL bill2
,dbo.FN_ConvertDate(chq2.TMBILDT,'D/M/Y') bill2Date
,chq2.NETPAY bill2NetPay
,datediff(day,chq1.TMBILDT,chq2.TMBILDT) ddiff
from dc_allAssignedJobs asj 
inner join wb_chqtran chq1 on asj.contno=chq1.CONTNO and asj.installmentBillNoOne=chq1.TMBILL and chq1.PAYFOR in ('006','007') and chq1.FLAG!='C'
inner join wb_chqtran chq2 on asj.contno=chq2.CONTNO 
		and chq2.PAYFOR in ('006','007') and chq2.FLAG!='C'
		and datediff(day,chq1.TMBILDT,chq2.TMBILDT) between 0 and 15
		and chq1.TMBILL!=chq2.TMBILL
where 1=1
and asj.closed in ('P','R') 
and installmentBillNoTwo is null
