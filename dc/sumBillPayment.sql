/**/
use serviceweb;
declare @maxDate  datetime,@year int; 
set @year =year(getdate());
set @maxDate = convert(varchar,datepart(yy,dateadd(year,1,getdate())))+'/01/01'; --����ѹ�á�ͧ�նѴ��͡��

IF OBJECT_ID('tempdb..#dc_007Clearance') IS NOT NULL  DROP TABLE #dc_007Clearance;
select TMBILL into #dc_007Clearance from dc_clearanceAndRefinance clr --where year(clr.TMBILDT)=@year

select cast(y as int)+543 as y
,CONVERT(varchar, cast(isnull(M01,0) as money),1) M01
,CONVERT(varchar, cast(isnull(M02,0) as money),1) M02
,CONVERT(varchar, cast(isnull(M03,0) as money),1) M03
,CONVERT(varchar, cast(isnull(M04,0) as money),1) M04
,CONVERT(varchar, cast(isnull(M05,0) as money),1) M05
,CONVERT(varchar, cast(isnull(M06,0) as money),1) M06
,CONVERT(varchar, cast(isnull(M07,0) as money),1) M07
,CONVERT(varchar, cast(isnull(M08,0) as money),1) M08
,CONVERT(varchar, cast(isnull(M09,0) as money),1) M09
,CONVERT(varchar, cast(isnull(M10,0) as money),1) M10
,CONVERT(varchar, cast(isnull(M11,0) as money),1) M11
,CONVERT(varchar, cast(isnull(M12,0) as money),1) M12
,CONVERT(varchar, cast(aggr.sumallM as money),1)  sumAllM
--,[M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12]
from (
	select 
	convert(varchar,year(chq.TMBILDT)) y
	,case when month(chq.TMBILDT) > 9 then 'M'+convert(varchar,month(chq.TMBILDT)) 	else 'M0'+convert(varchar,month(chq.TMBILDT)) 	end m
	,sum(chq.NETPAY) amt --����Թ
	--,count(*) amt	 --�Ѻ�ӹǹ���
	--,chq.LOCATPAY locat
	from HIINCOME.dbo.CHQTRAN chq
	where 1=1
	and chq.FLAG<>'C' 
	--and chq.FLAG='C' 
	and chq.TMBILDT<@maxDate and chq.TMBILDT>='20050101'
	--and chq.PAYFOR in ('006','007','102') and chq.TMBILL not in (select TMBILL from #dc_007Clearance)	
	and chq.PAYFOR in ('006','007','102','103','104','105','110') and chq.TMBILL not in (select TMBILL from #dc_007Clearance)	
	--and chq.PAYFOR in ('102') and chq.TMBILL not in (select TMBILL from #dc_007Clearance)	
	--and chq.PAYFOR in ('006','007') and chq.TMBILL not in (select TMBILL from #dc_007Clearance)	
	--and chq.TMBILL not in (select TMBILL from #dc_007Clearance) and chq.PAYFOR in (select FORCODE from wb_payfor)
	--and chq.TMBILL not in (select TMBILL from #dc_007Clearance) and chq.PAYFOR not in ('006','007','102')
	--and chq.PAYFOR not in ('007')
	--and chq.PAYFOR in ('007')
	--and chq.PAYFOR in ('102')
	--and chq.TMBILDT='20170527'
	--and chq.TMBILDT=convert(varchar,getdate(),111)
	--and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year and day(chq.TMBILDT)between 1 and @day
	--and chq.TMBILDT=CONVERT(varchar,dateadd(day,0,getdate()),111) 
	--and DAY(chq.TMBILDT) between 28 and 28
	--and chq.TMBILDT<@maxDate and chq.TMBILDT>='20070101' and chq.TMBILDT between '20161001' and '20161008'
	--and chq.TMBILDT<@maxDate and chq.TMBILDT>='20070101' and chq.TMBILDT between '20160901' and '20160908'
	group by year(chq.TMBILDT),month(chq.TMBILDT)
) t 
pivot (sum(amt) for m in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
cross apply (select (isnull(M01,0)+isnull(M02,0)+isnull(M03,0)+isnull(M04,0)+isnull(M05,0)+isnull(M06,0)+isnull(M07,0)+isnull(M08,0)+isnull(M09,0)+isnull(M10,0)+isnull(M11,0)+isnull(M12,0)) sumallM) aggr
order by y desc

/*
select y,CONVERT(varchar, cast(isnull(M01,0) as money),1) M01
,CONVERT(varchar, cast(isnull(M02,0) as money),1) M02
,CONVERT(varchar, cast(isnull(M03,0) as money),1) M03
,CONVERT(varchar, cast(isnull(M04,0) as money),1) M04
,CONVERT(varchar, cast(isnull(M05,0) as money),1) M05
,CONVERT(varchar, cast(isnull(M06,0) as money),1) M06
,CONVERT(varchar, cast(isnull(M07,0) as money),1) M07
,CONVERT(varchar, cast(isnull(M08,0) as money),1) M08
,CONVERT(varchar, cast(isnull(M09,0) as money),1) M09
,CONVERT(varchar, cast(isnull(M10,0) as money),1) M10
,CONVERT(varchar, cast(isnull(M11,0) as money),1) M11
,CONVERT(varchar, cast(isnull(M12,0) as money),1) M12
from (
select count(*)cnt
--,month(YDATE)m
,case when month(YDATE) > 9 then 'M'+convert(varchar,month(YDATE)) 	else 'M0'+convert(varchar,month(YDATE)) 	end m
,year(YDATE)y from wb_arhold group by month(YDATE),year(YDATE)
)t pivot (sum(cnt) for m in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
order by y desc
*/

/*
select aggr.sumNetpay,aggr.cnt,aggr.PAYFOR,pf.FORDESC from (
	select SUM(NETPAY)sumNetpay, COUNT(*)cnt,PAYFOR 
	from wb_chqtran 
	where year(TMBILDT)=2017 and month(TMBILDT)=5
	and TMBILL not in (select TMBILL from #dc_007Clearance)
	group by PAYFOR
) aggr
left join wb_payfor pf on aggr.PAYFOR=pf.FORCODE
order by aggr.sumNetpay desc
*/