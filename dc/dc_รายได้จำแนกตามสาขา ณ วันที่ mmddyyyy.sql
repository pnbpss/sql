use serviceweb;
			declare @countToDate datetime,@month int, @year int,@specifiedDay datetime;
			
			set @specifiedDay = '2017/5/15';
			
			set @countToDate=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@specifiedDay)+1,0))) --�ѹ�ش���¢ͧ��͹
			
			set @month=MONTH(@specifiedDay);
			set @year=YEAR(@specifiedDay);
			
			--select COUNT(*)cnt from dc_allAssignedJobs asj where asj.closed='N' and asj.issuedDate<=@countTodate --�ҹ�����¡�͹�ѹ���Ѻ�֧ ����ѧ���Դ��ͺ
			--select COUNT(*)cnt from dc_allAssignedJobs asj where asj.closed!='N' and asj.issuedDate<=@countTodate and asj.closedDate>@countTodate --�ҹ�����¡�͹�ѹ�Ѻ�֧ ��лԴ��ͺ���� �� � �ѹ�Ѻ�֧�ѧ�����Դ��ͺ
			--select COUNT(*)cnt from dc_allAssignedJobs asj where asj.closed!='N' and month(asj.closedDate)=@month and year(asj.closedDate)=@year  --and asj.closed not in ('F','C')
			--select COUNT(*)cnt from dc_allAssignedJobs asj where asj.closed not in ('F','C') and closedDate<=@countToDate 
			--goto endScript


			IF OBJECT_ID('tempdb.dbo.#abcdef', 'U') IS NOT NULL DROP TABLE #abcdef;
			IF OBJECT_ID('tempdb.dbo.#finalResult', 'U') IS NOT NULL DROP TABLE #finalResult;
			select
			asj.dcTFSName+'('+convert(varchar(max),asj.collectorId)+')' dcTFSName
			,case 			
				when asj.crDisconnectedDays between 0 and 90 then 'R_000TO090'					
				when asj.crDisconnectedDays between 91 and 180 then 'R_091TO180'
				when asj.crDisconnectedDays between 181 and 365 then 'R_181TO365'
				when asj.crDisconnectedDays between 366 and 500 then 'R_366TO500'
				when asj.crDisconnectedDays between 501 and 700 then 'R_501TO700'
				when asj.crDisconnectedDays between 701 and 900 then 'R_701TO900'
				when asj.crDisconnectedDays > 900 then 'R_900UP'
			end as ddRange
			,asj.crRemainDebt
			,asj.crDebtPlusVat
			,asj.crPendingPeriod
			,asj.crDisconnectedDays
			,asj.closed
			,isnull(seizeBill.NETPAY,0) seizedNetPay
			,isnull(dcBill.NETPAY,0) dcNetPay
			,isnull(insBill1.NETPAY,0) insOneNetPay
			,isnull(insBill2.NETPAY,0) insTwoNetPay
			,aggr.sumIncome
			,asj.issuedDate,asj.closedDate
			,asj.locat			
			into #abcdef
			from dc_allAssignedJobs asj				 
				 left join wb_chqtran dcBill on asj.DCBillNo=dcBill.TMBILL and dcBill.PAYFOR='102' and dcBill.FLAG!='C'
				 left join wb_chqtran seizeBill on asj.seizedBillNo=seizeBill.TMBILL and seizeBill.PAYFOR='103' and seizeBill.FLAG!='C'
				 left join wb_chqtran insBill1 on asj.installmentBillNoOne=insBill1.TMBILL and insBill1.PAYFOR in ('006','007') and insBill1.FLAG!='C'
				 left join wb_chqtran insBill2 on asj.installmentBillNoTwo=insBill2.TMBILL and insBill2.PAYFOR in ('006','007') and insBill2.FLAG!='C'
				 cross apply (select (isnull(seizeBill.NETPAY,0)+isnull(dcBill.NETPAY,0)+isnull(insBill1.NETPAY,0)+isnull(insBill2.NETPAY,0)) sumIncome ) aggr				 
			where 1=1 and asj.issuedDate>'2016/04/30' 
			--".$branchConditions." ".$conditions."
			and asj.closed in ('P','R')
			--and asj.closedDate between '20170501' and '20170531'
			and month(asj.closedDate)=@month and year(asj.closedDate)=@year
			and asj.closedDate<=@countToDate;


--select * from #abcdef where locat='YK��1';
select 	
	main.locat
	,main.completedJob	
	,main.sumIncome,main.seized,main.billOne,main.billTwo,main.avgIncome,main.maxIncome,main.stdevIncome
	--,jobInHandPending.cnt cntPending,jobInHandClosed.cnt cntClosed,jobInHandClosedB.cnt cntb
	,(jobInHandPending.cnt+jobInHandClosed.cnt+jobInHandClosedB.cnt)jobInHand 
	,cast((main.completedJob*100)/(jobInHandPending.cnt+jobInHandClosed.cnt+jobInHandClosedB.cnt) as decimal(18,1)) as pcCompleted
	,countDc.cntDc,countDcNotThisBranch.cntDc jobNotThisBranch
	,case when countDcNotThisBranch.cntDc=0 then 0.0 else cast(((jobInHandPending.cnt+jobInHandClosed.cnt+jobInHandClosedB.cnt)*100.0)/countDcNotThisBranch.cntDc as decimal(18,1)) end pcOtherBranch
	into #finalResult
from (			
		select 
		COUNT(*) completedJob
		,SUM(sumIncome)sumIncome
		,SUM(seizedNetPay)seized
		,SUM(insOneNetPay)billOne
		,SUM(insTwoNetPay)billTwo
		,cast(AVG(sumIncome) as decimal(18,2))avgIncome
		,cast(max(sumIncome) as decimal(18,2))maxIncome
		,min(sumIncome)minIncome
		,cast(stdev(sumIncome) as decimal(18,2))stdevIncome
		,m.locat locat
		from #abcdef m	
		group by m.locat	
) main
cross apply ( select COUNT(*)cnt from dc_allAssignedJobs asj where asj.locat=main.locat and asj.closed='N' and asj.issuedDate<=@countTodate)jobInHandPending
cross apply ( select COUNT(*)cnt from dc_allAssignedJobs asj where asj.locat=main.locat and asj.closed!='N' and asj.issuedDate<=@countTodate and asj.closedDate>@countTodate and asj.closed in ('P','R','S') )jobInHandClosed
cross apply ( select COUNT(*)cnt from dc_allAssignedJobs asj where asj.locat=main.locat and asj.closed!='N' and month(asj.closedDate)=@month and year(asj.closedDate)=@year and asj.closed in ('P','R','S'))jobInHandClosedB
cross apply (
	select COUNT(distinct collectorId)cntDc from dc_allAssignedJobs asj
	where 1=1 
	and asj.locat=main.locat
	and (
		(asj.closed='N' and asj.issuedDate<=@countTodate)
		or
		(asj.closed!='N' and asj.issuedDate<=@countTodate and asj.closedDate>@countTodate) and asj.closed in ('P','R','S')
		or
		(asj.closed!='N' and month(asj.closedDate)=@month and year(asj.closedDate)=@year) and asj.closed in ('P','R','S')
	)	
)countDc
cross apply (
	select COUNT(*)cntDc from dc_allAssignedJobs asj
	where 1=1 	
	and asj.locat!=main.locat
	and(
				(asj.closed='N' and asj.issuedDate<=@countTodate)
				or
				(asj.closed!='N' and asj.issuedDate<=@countTodate and asj.closedDate>@countTodate) and asj.closed in ('P','R','S')
				or
				(asj.closed!='N' and month(asj.closedDate)=@month and year(asj.closedDate)=@year) and asj.closed in ('P','R','S')
		)
		and collectorId in 
		(select collectorId from dc_allAssignedJobs asj
					where 
					asj.locat=main.locat
					and (
						(asj.closed='N' and asj.issuedDate<=@countTodate)
						or
						(asj.closed!='N' and asj.issuedDate<=@countTodate and asj.closedDate>@countTodate) and asj.closed in ('P','R','S')
						or
						(asj.closed!='N' and month(asj.closedDate)=@month and year(asj.closedDate)=@year) and asj.closed in ('P','R','S')
					)		
		)	
)countDcNotThisBranch
endScript:


select * from #abcdef
select * from #finalResult order by locat


--select COUNT(*)cnt, collectorId from dc_allAssignedJobs where closed='N' group by collectorId