/*
--��ṡ�����.�ͧ�����ѹ�ͧ��ŷ�������¡��ԡ
select PAYFOR,FORDESC,[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21]
from
 (
	select 
	count(*) cnt,uv.PAYFOR,pf.FORDESC,datepart(hh,uv.INPDT) hh
	from HIC4REPORT.dbo.CHQTRAN uv	
	left join HIC4REPORT.dbo.PAYFOR pf on uv.PAYFOR=pf.FORCODE
	where uv.FLAG<>'C'
	group by uv.PAYFOR,pf.FORDESC,datepart(hh,uv.INPDT)
) t 
pivot
(
	sum(cnt) for hh in ([6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21])
) p

--��ṡ�����.�ͧ�����ѹ�ͧ��ŷ���ա��¡��ԡ
select PAYFOR,FORDESC,[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21]
from
 (
	select 
	count(*) cnt,uv.PAYFOR,pf.FORDESC,datepart(hh,uv.INPDT) hh
	from HIC4REPORT.dbo.CHQTRAN uv	
	left join HIC4REPORT.dbo.PAYFOR pf on uv.PAYFOR=pf.FORCODE
	where uv.FLAG='C'
	group by uv.PAYFOR,pf.FORDESC,datepart(hh,uv.INPDT)
) t 
pivot
(
	sum(cnt) for hh in ([6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21])
) p
*/

--��ṡ����ѹ�������͹�ͧ�����ѹ�ͧ��ŷ������ա��¡��ԡ
select PAYFOR,FORDESC,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31]
from
 (
	select 
	count(*) cnt,uv.PAYFOR,pf.FORDESC,datepart(d,uv.INPDT) hh
	from HIC4REPORT.dbo.CHQTRAN uv	
	left join HIC4REPORT.dbo.PAYFOR pf on uv.PAYFOR=pf.FORCODE
	where uv.FLAG!='C'
	group by uv.PAYFOR,pf.FORDESC,datepart(d,uv.INPDT)
) t 
pivot
(
	sum(cnt) for hh in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])
) p

order by PAYFOR


--��ṡ����ѹ�������͹�ͧ�����ѹ�ͧ�ѻ����ͧ��ŷ������ա��¡��ԡ
select PAYFOR,FORDESC,[1] [Sunday],[2] [Monday],[3] [Tuesday],[4] [Wendnesday],[5] [Thursday],[6] [Friday],[7] [Saturday]
from
 (
	select 
	count(*) cnt,uv.PAYFOR,pf.FORDESC,datepart(dw,uv.INPDT) hh
	from HIC4REPORT.dbo.CHQTRAN uv	
	left join HIC4REPORT.dbo.PAYFOR pf on uv.PAYFOR=pf.FORCODE
	where uv.FLAG!='C' and uv.INPDT>'20101231'
	group by uv.PAYFOR,pf.FORDESC,datepart(dw,uv.INPDT)
) t 
pivot
(
	sum(cnt) for hh in ([1],[2],[3],[4],[5],[6],[7])
) p

order by PAYFOR




--�ѹ�ͧ�ѻ��������ö
select yy,[1] [Sun],[2] [Mon],[3] [Tue],[4] [Wed],[5] [Thu],[6] [Fri],[7] [Sat]
from
 (
	select 
	count(*) cnt,datepart(yy,uv.SDATE) yy,datepart(dw,uv.SDATE) hh
	from (select CONTNO,SDATE from HIC4REPORT.dbo.ARMAST union select CONTNO,SDATE from HIC4REPORT.dbo.HARMAST) uv		
	where uv.SDATE>'20101231'
	group by datepart(yy,uv.SDATE),datepart(dw,uv.SDATE) 
) t 
pivot
(
	sum(cnt) for hh in ([1],[2],[3],[4],[5],[6],[7])
) p
order by yy



--select datepart(yy,'20160508')