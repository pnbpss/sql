IF OBJECT_ID('tempdb..#temp') is not null DROP TABLE #temp
IF OBJECT_ID('tempdb..#abc') is not null DROP TABLE #abc
declare @fromppDate datetime,@toppDate datetime,@minusDay int, @plusDay int;
set @fromppDate = '20160701';
set @toppDate = '20160731';
set @minusDay = 30;
set @plusDay = 60;
select 
maxddp.id,ddp.contno,ddp.cuscod,ddp.laccpdt,ddp.apprvby,ddp.authrby,ddp.recvdt,ddp.delycnt,ddp.inpdt,ddp.userid,ddp.memo1
,ddp.crContStatus,ddp.crDebtPlusVat,ddp.crPendingPeriod,ddp.crRemainDebt,ddp.crPendingFromPeriod
,ddp.crPendingToPeriod,ddp.crDueDate,ddp.crDisconnectedDays,ddp.crLastPaidDate,ddp.custRequestVia
,ddp.additionalInfo,ddp.deleted,ddp.deletedBy,ddp.deletedSince,ddp.telephoningId,ddp.cntAfterPaid
,case
				when va.netPendingPeriod<=1 then '01. ����+1 �Ǵ'
				when va.netPendingPeriod=2 and va.disconnectedDays >=0 and va.disconnectedDays<=15 then '02. ��ҧ 2 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
				when va.netPendingPeriod=2 and va.disconnectedDays >=16 and va.disconnectedDays<=90 then '03. ��ҧ 2 �Ǵ �Ҵ��õԴ��� 15-90 �ѹ'
				when va.netPendingPeriod=2 and va.disconnectedDays >90 then '04. ��ҧ 2 �Ǵ �Ҵ��õԴ���  >90 �ѹ'	
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 0 and va.disconnectedDays<=15 then '05. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 16 and va.disconnectedDays<=30 then '06. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 16-30 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 31 and va.disconnectedDays<=45 then '07. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 31-45 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 46 and va.disconnectedDays<=60 then '08. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 46-60 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 61 and va.disconnectedDays<=90 then '09. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 61-90 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays >= 91 and va.disconnectedDays<=365 then '10. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� 91-365 �ѹ'
				when va.netPendingPeriod>=3 and va.disconnectedDays > 365  then '11. ��ҧ >=3 �Ǵ �Ҵ��õԴ��� >365 �ѹ'
			end pendingStatus
,case
				when ddp.crDisconnectedDays between 0 and 60 then 'DCD000_060'
				when ddp.crDisconnectedDays between 61 and 120 then 'DCD061_120'
				when ddp.crDisconnectedDays between 121 and 180 then 'DCD121_180'
				when ddp.crDisconnectedDays between 181 and 365 then 'DCD181_365'				
				when ddp.crDisconnectedDays > 365 then 'DCD366_UP'
			end +'_'
			--dcdWhileSave
			/*
			-N/A 1-E-Mail 1-�ҧ�Ź� 1	-���駴��µ���ͧ 1 -�١�������Ҽ�͹ 2 -�ҡ�Һ͡ 3  �ѹ�֡�ҡ��õԴ��� 4,���Ѿ��(��Ъ��) 5 ���Ѿ�� 6			
			*/
			+case 
				when custRequestVia in ('E-Mail','�ҧ�Ź�','���駴��µ���ͧ','N/A') then 'case1'
				when custRequestVia in ('�١�������Ҽ�͹') then 'case2'
				when custRequestVia in ('�ҡ�Һ͡') then 'case3'
				when custRequestVia in ('�ѹ�֡�ҡ��õԴ���') then 'case4'
				when custRequestVia in ('���Ѿ��(��Ъ��)') then 'case5'
				when custRequestVia in ('���Ѿ��') then 'case6'
				else 'caseN'
			end condPVia
,va.LOCAT,va.startContract,va.endContract,va.lastPaidDate
,va.installmentPerPeriod,va.lastPaidInstallmentPeriod
,va.lastPaidInstall,va.AllPeriod,va.currentperiod
,va.disconnectedDays,va.pendingInstallmentPeriod
,va.netPendingPeriod,va.lastpassdue,va.neverpaid,va.nextDealDate
into #temp
from (
		select max(id)id,contno,laccpdt 
		from dc_dueDatePostponements
		group by contno,laccpdt
		) maxddp
left join dc_dueDatePostponements ddp on maxddp.id=ddp.id
left join 
		(
			select 
				rank() over(partition by CONTNO order by TMBILL) as r1
				,CONTNO,TMBILL,TMBILDT
			from wb_chqtran
			where 
			TMBILDT >= dateadd(day,0-(@minusDay),@fromppDate)
			and PAYFOR in ('006','007')
			and FLAG !='C'
		) chq on maxddp.contno=chq.CONTNO and chq.TMBILDT between dateadd(day,0-(@minusDay),maxddp.laccpdt) and dateadd(day,@plusDay,maxddp.laccpdt) 
		/*wb_chqtran chq on maxddp.contno=chq.CONTNO 
		and chq.TMBILDT between dateadd(day,0-(@minusDay),maxddp.laccpdt) and dateadd(day,@plusDay,maxddp.laccpdt)
		and chq.PAYFOR in ('006','007')
		and chq.FLAG!='C'
		*/
left join view_arpay2 va on maxddp.contno=va.CONTNO
where 1=1
and maxddp.laccpdt between @fromppDate and @toppDate
--and chq.TMBILL is null --����Դ��ê���
and chq.TMBILL is not null and chq.r1=1 --�Դ��ê���
and va.CONTNO is not null --ö�ִ����/�ѭ�ҷ������������ armast ����ͧ����͡��
and ddp.inpdt>'20160331' --���੾�е��������������������Ѵ�������
and ddp.userid!='admin' --�������跴�ͧ�����
order by maxddp.contno,maxddp.id
/*
select 
main.id,main.contno,main.cuscod,main.laccpdt
,pendingStatus
,dcdWhileSave
,main.apprvby,main.authrby,main.recvdt,main.delycnt,main.inpdt,main.userid,main.memo1,main.crContStatus,main.crDebtPlusVat,main.crPendingPeriod,main.crRemainDebt,main.crPendingFromPeriod,main.crPendingToPeriod,main.crDueDate,main.crDisconnectedDays,main.crLastPaidDate,main.custRequestVia,main.additionalInfo,main.deleted,main.deletedBy,main.deletedSince,main.telephoningId,main.cntAfterPaid,main.LOCAT,main.startContract,main.endContract,main.lastPaidDate,main.installmentPerPeriod,main.lastPaidInstallmentPeriod,main.lastPaidInstall,main.AllPeriod,main.currentperiod,main.disconnectedDays,main.pendingInstallmentPeriod,main.netPendingPeriod,main.lastpassdue,main.neverpaid,main.nextDealDate
from #temp main
order by contno;
*/

select pendingStatus,locat,isnull(DCD000_060_case1,0)DCD000_060_case1,isnull(DCD000_060_case2,0)DCD000_060_case2,isnull(DCD000_060_case3,0)DCD000_060_case3,isnull(DCD000_060_case4,0)DCD000_060_case4,isnull(DCD000_060_case5,0)DCD000_060_case5,isnull(DCD000_060_case6,0)DCD000_060_case6,
						isnull(DCD061_120_case1,0)DCD061_120_case1,isnull(DCD061_120_case2,0)DCD061_120_case2,isnull(DCD061_120_case3,0)DCD061_120_case3,isnull(DCD061_120_case4,0)DCD061_120_case4,isnull(DCD061_120_case5,0)DCD061_120_case5,isnull(DCD061_120_case6,0)DCD061_120_case6,
						isnull(DCD121_180_case1,0)DCD121_180_case1,isnull(DCD121_180_case2,0)DCD121_180_case2,isnull(DCD121_180_case3,0)DCD121_180_case3,isnull(DCD121_180_case4,0)DCD121_180_case4,isnull(DCD121_180_case5,0)DCD121_180_case5,isnull(DCD121_180_case6,0)DCD121_180_case6,
						isnull(DCD181_365_case1,0)DCD181_365_case1,isnull(DCD181_365_case2,0)DCD181_365_case2,isnull(DCD181_365_case3,0)DCD181_365_case3,isnull(DCD181_365_case4,0)DCD181_365_case4,isnull(DCD181_365_case5,0)DCD181_365_case5,isnull(DCD181_365_case6,0)DCD181_365_case6,
						isnull(DCD366_UP_case1,0)DCD366_UP_case1,isnull(DCD366_UP_case2,0)DCD366_UP_case2,isnull(DCD366_UP_case3,0)DCD366_UP_case3,isnull(DCD366_UP_case4,0)DCD366_UP_case4,isnull(DCD366_UP_case5,0)DCD366_UP_case5,isnull(DCD366_UP_case6,0)DCD366_UP_case6
						,aggr.sumAll
			into #abc
			from (select count(*) cnt,locat,pendingStatus,condPVia from #temp main group by locat,pendingStatus,condPVia) t
			pivot(sum(cnt) for condPVia in (DCD000_060_case1,DCD000_060_case2,DCD000_060_case3,DCD000_060_case4,DCD000_060_case5,DCD000_060_case6,DCD061_120_case1,DCD061_120_case2,DCD061_120_case3,DCD061_120_case4,DCD061_120_case5,DCD061_120_case6,DCD121_180_case1,DCD121_180_case2,DCD121_180_case3,DCD121_180_case4,DCD121_180_case5,DCD121_180_case6,DCD181_365_case1,DCD181_365_case2,DCD181_365_case3,DCD181_365_case4,DCD181_365_case5,DCD181_365_case6,DCD366_UP_case1,DCD366_UP_case2,DCD366_UP_case3,DCD366_UP_case4,DCD366_UP_case5,DCD366_UP_case6)) p
			cross apply (select (isnull(DCD000_060_case1,0)+isnull(DCD000_060_case2,0)+isnull(DCD000_060_case3,0)+isnull(DCD000_060_case4,0)+isnull(DCD000_060_case5,0)+isnull(DCD000_060_case6,0)+isnull(DCD061_120_case1,0)+isnull(DCD061_120_case2,0)+isnull(DCD061_120_case3,0)+isnull(DCD061_120_case4,0)+isnull(DCD061_120_case5,0)+isnull(DCD061_120_case6,0)+isnull(DCD121_180_case1,0)+isnull(DCD121_180_case2,0)+isnull(DCD121_180_case3,0)+isnull(DCD121_180_case4,0)+isnull(DCD121_180_case5,0)+isnull(DCD121_180_case6,0)+isnull(DCD181_365_case1,0)+isnull(DCD181_365_case2,0)+isnull(DCD181_365_case3,0)+isnull(DCD181_365_case4,0)+isnull(DCD181_365_case5,0)+isnull(DCD181_365_case6,0)+isnull(DCD366_UP_case1,0)+isnull(DCD366_UP_case2,0)+isnull(DCD366_UP_case3,0)+isnull(DCD366_UP_case4,0)+isnull(DCD366_UP_case5,0)+isnull(DCD366_UP_case6,0)) sumAll)aggr


select pendingStatus,locat,sumAll,DCD000_060_case1,DCD061_120_case1,DCD121_180_case1,DCD181_365_case1,DCD366_UP_case1,DCD000_060_case2,
			DCD061_120_case2,DCD121_180_case2,DCD181_365_case2,DCD366_UP_case2,DCD000_060_case3,DCD061_120_case3,DCD121_180_case3,DCD181_365_case3,
			DCD366_UP_case3,DCD000_060_case4,DCD061_120_case4,DCD121_180_case4,DCD181_365_case4,DCD366_UP_case4,DCD000_060_case5,DCD061_120_case5,
			DCD121_180_case5,DCD181_365_case5,DCD366_UP_case5,DCD000_060_case6,DCD061_120_case6,DCD121_180_case6,DCD181_365_case6,DCD366_UP_case6
			from #abc
			union
			select '����' pendingStatus,locat,sum(sumAll)sumAll,sum(DCD000_060_case1)DCD000_060_case1,	sum(DCD061_120_case1)DCD061_120_case1,	sum(DCD121_180_case1)DCD121_180_case1,	sum(DCD181_365_case1)DCD181_365_case1,	sum(DCD366_UP_case1)DCD366_UP_case1,	sum(DCD000_060_case2)DCD000_060_case2,	sum(DCD061_120_case2)DCD061_120_case2,	sum(DCD121_180_case2)DCD121_180_case2,	sum(DCD181_365_case2)DCD181_365_case2,	sum(DCD366_UP_case2)DCD366_UP_case2,	sum(DCD000_060_case3)DCD000_060_case3,	sum(DCD061_120_case3)DCD061_120_case3,	sum(DCD121_180_case3)DCD121_180_case3,	sum(DCD181_365_case3)DCD181_365_case3,	sum(DCD366_UP_case3)DCD366_UP_case3,	sum(DCD000_060_case4)DCD000_060_case4,	sum(DCD061_120_case4)DCD061_120_case4,	sum(DCD121_180_case4)DCD121_180_case4,	sum(DCD181_365_case4)DCD181_365_case4,	sum(DCD366_UP_case4)DCD366_UP_case4,	sum(DCD000_060_case5)DCD000_060_case5,	sum(DCD061_120_case5)DCD061_120_case5,	sum(DCD121_180_case5)DCD121_180_case5,	sum(DCD181_365_case5)DCD181_365_case5,	sum(DCD366_UP_case5)DCD366_UP_case5,	sum(DCD000_060_case6)DCD000_060_case6,	sum(DCD061_120_case6)DCD061_120_case6,	sum(DCD121_180_case6)DCD121_180_case6,	sum(DCD181_365_case6)DCD181_365_case6,	sum(DCD366_UP_case6)DCD366_UP_case6
			from #abc group by locat
			union
			select '����' pendingStatus,'�������'locat,sum(sumAll)sumAll,sum(DCD000_060_case1)DCD000_060_case1,	sum(DCD061_120_case1)DCD061_120_case1,	sum(DCD121_180_case1)DCD121_180_case1,	sum(DCD181_365_case1)DCD181_365_case1,	sum(DCD366_UP_case1)DCD366_UP_case1,	sum(DCD000_060_case2)DCD000_060_case2,	sum(DCD061_120_case2)DCD061_120_case2,	sum(DCD121_180_case2)DCD121_180_case2,	sum(DCD181_365_case2)DCD181_365_case2,	sum(DCD366_UP_case2)DCD366_UP_case2,	sum(DCD000_060_case3)DCD000_060_case3,	sum(DCD061_120_case3)DCD061_120_case3,	sum(DCD121_180_case3)DCD121_180_case3,	sum(DCD181_365_case3)DCD181_365_case3,	sum(DCD366_UP_case3)DCD366_UP_case3,	sum(DCD000_060_case4)DCD000_060_case4,	sum(DCD061_120_case4)DCD061_120_case4,	sum(DCD121_180_case4)DCD121_180_case4,	sum(DCD181_365_case4)DCD181_365_case4,	sum(DCD366_UP_case4)DCD366_UP_case4,	sum(DCD000_060_case5)DCD000_060_case5,	sum(DCD061_120_case5)DCD061_120_case5,	sum(DCD121_180_case5)DCD121_180_case5,	sum(DCD181_365_case5)DCD181_365_case5,	sum(DCD366_UP_case5)DCD366_UP_case5,	sum(DCD000_060_case6)DCD000_060_case6,	sum(DCD061_120_case6)DCD061_120_case6,	sum(DCD121_180_case6)DCD121_180_case6,	sum(DCD181_365_case6)DCD181_365_case6,	sum(DCD366_UP_case6)DCD366_UP_case6 
			from #abc
			order by locat,pendingStatus