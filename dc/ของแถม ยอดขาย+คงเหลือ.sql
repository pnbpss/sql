select t2.PartNo,t2.PartName,t2.monthSale,t2.GroupId,t2.GroupName,t2.qty,t2.baht,onhand.OnHand
 from (
	select t1.PartNo,t1.PartName,monthSale,GroupId,GroupName,SUM(SaleQTY)qty,SUM(amount)baht
	from (
		select s.SaleNo,s.BranchNo,s.CompCode
		,'M'+SUBSTRING(s.SaleDate,1,4)+'_'+SUBSTRING(s.SaleDate,6,2) monthSale
		,sd.PartName,sd.PartNo
		,sd.Amount,sd.SaleQTY
		,pl.GroupId,pg.GroupName
		from SPSale s inner join SPSaleDetail sd on s.SaleNo=sd.SaleNo and s.CompCode=sd.CompCode and s.BranchNo=sd.BranchNo
		inner join SPPartList pl on sd.PartNo=pl.PartNo
		inner join SPPartGroup pg on pl.GroupId=pg.GroupID
		 where s.SaleStatus in ('Paid','Approved')
		 and s.SaleDate between '2560/01/01' and '2560/01/25'
		 and pg.GroupID not in ('12','05','19','21','20','17','09','11','13')
	) t1 
	group by t1.PartNo,t1.PartName,monthSale,GroupId,GroupName
) t2 left join (select PartNo,SUM(onHand)OnHand from SPPartMaster group by PartNo) onhand on t2.PartNo=onhand.PartNo
