use serviceweb;
/*
--select count(*),SC from dc_telephoneNumberChangings group by SC
select * from dc_telephoneNumberChangings where contno='ณHP-15060028' order by dtUpdate desc
select * from dc_lastCallResultOfEachConts where contno='ณHP-15060028' order by lastUpdateDate desc
select top 1 telephoneNumber from dc_telephoneNumberChangings lp 
where lp.contno='ฉHP-12090043' and lp.dtUpdate<'2016-11-11 10:56:59.983'
order by dtUpdate desc
*/

select m.telephoningId,m.contno,m.telephoneNumber changeTo,m.dtUpdate
,(
	select top 1 lp.telephoneNumber from dc_telephoneNumberChangings lp 
	where lp.contno=m.contno and lp.dtUpdate<m.dtUpdate
	order by lp.dtUpdate desc
  ) changeFrom
  ,lco.staffName st1
  ,lco2.staffName st2
from dc_telephoneNumberChangings m 
	 left join dc_lastCallResultOfEachConts lco on 
	 m.contno=lco.contno 
	 and m.telephoningId=lco.telephoningId
	 and m.dtUpdate=lco.lastUpdateDate	 
	 left join dc_lastCallResultOfEachConts lco2 on
	 m.contno=lco2.contno
	 and m.telephoningId=lco2.telephoningId
	 and abs(datediff(ms,m.dtUpdate,lco2.lastUpdateDate))<1000
where m.SC='C'
and m.dtUpdate>'20160430'
--and m.contno='XHP-16070013'