USE [serviceweb]
GO

/****** Object:  Table [dbo].[dc_loadedContForCollection]    Script Date: 07/12/2015 22:03:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dc_loadedContForCollection](
	[contno] [varchar](50) NOT NULL,
	[strno] [varchar](50) NOT NULL,
	[locat] [varchar](50) NULL,
	[buyerCuscod] [varchar](50) NOT NULL,
	[suretyCuscod1] [varchar](50) NULL,
	[suretyCuscod2] [varchar](50) NULL,
	[suretyCuscod3] [varchar](50) NULL,
	[sdate] [datetime] NOT NULL,
	[statusId] [int] NOT NULL,
	[installmentPerperiod] [decimal](18, 2) NOT NULL,
	[firstLoadBy] [varchar](50) NOT NULL,
	[firstLoadSince] [datetime] NOT NULL,
 CONSTRAINT [PK_dc_loadedContForCollection] PRIMARY KEY CLUSTERED 
(
	[contno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[dc_loadedContForCollection]  WITH CHECK ADD  CONSTRAINT [FK_dc_loadedContForCollection_dc_loadedContForCollectionStatus] FOREIGN KEY([statusId])
REFERENCES [dbo].[dc_loadedContForCollectionStatus] ([id])
GO

ALTER TABLE [dbo].[dc_loadedContForCollection] CHECK CONSTRAINT [FK_dc_loadedContForCollection_dc_loadedContForCollectionStatus]
GO

