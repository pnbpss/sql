use serviceweb;
IF OBJECT_ID('tempdb..#tempTb1') is not null DROP TABLE #tempTb1
declare @specifiedStartDate datetime, @fromDate datetime, @toDate datetime;
set @specifiedStartDate ='2016/05/04';
set @fromDate = '2016/05/12'
set @toDate  = '2016/05/15'
select pendingStatus, isnull([p0],0) P00
,isnull([p2],0) P02,isnull([p5],0) P05,isnull([p8],0) P08,isnull([p9],0) P09,isnull([p10],0) P10,isnull([p11],0) P11,isnull([p12],0) P12
,isnull([p13],0) P13,isnull([p14],0) P14,isnull([p15],0) P15,isnull([p16],0) P16,isnull([p17],0) P17,isnull([p18],0) P18
,(isnull([p2],0)+isnull([p5],0)+isnull([p8],0)+isnull([p9],0)+isnull([p10],0)+isnull([p11],0)+isnull([p12],0)+isnull([p13],0)+isnull([p14],0)
+isnull([p15],0)+isnull([p16],0)+isnull([p17],0)+isnull([p18],0)) sumTelled
,(isnull([p0],0)+isnull([p2],0)+isnull([p5],0)+isnull([p8],0)+isnull([p9],0)+isnull([p10],0)+isnull([p11],0)
+isnull([p12],0)+isnull([p13],0)+isnull([p14],0)+isnull([p15],0)+isnull([p16],0)+isnull([p17],0)+isnull([p18],0)) sumAll
into #tempTb1
from (
	select count(*) cnt, pendingStatus,pCallResultId
	from (
		SELECT		
			  case
				when t.crPendingPeriod<=1 then '1 ����+1 �Ǵ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=0 and t.crDisconnectedDate<=15 then '2 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=16 and t.crDisconnectedDate<=90 then '3 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 16-90 �ѹ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >90 then '4 ��ҧ 2 �Ǵ �Ҵ��õԴ����ҡ���� 90 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=0 and t.crDisconnectedDate <=15 then '5 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��� 0-15 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=16 and t.crDisconnectedDate <=90 then '6 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��� 16-90 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=91 then '7 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��͵���� 91 �ѹ����'
			  end pendingStatus
			  ,case
				when isnull(lcr.callResultId,0)<>0 then 'p'+convert(varchar(max),lcr.callResultId)
				else 'p0'
				end pCallResultId
			  ,lcr.[description]
			  
		  FROM dc_telephonings t 
		  left join dc_lastCallResultOfEachConts lcr on t.id=lcr.telephoningId and lcr.dateOfCall between @specifiedStartDate and @toDate		  
		  where 1=1	and t.specifiedCallStartDate=@specifiedStartDate  
	) p
	group by pendingStatus,pCallResultId
) tb
pivot(
	sum(cnt) for pCallResultId in ([p0],[p2],[p5],[p8],[p9],[p10],[p11],[p12],[p13],[p14],[p15],[p16],[p17],[p18])
) pv

select pendingStatus,P00,P02,P05,P08,P09,P10,P11,P12,P13,P14,P15,P16,P17,P18,sumTelled,sumAll from #tempTb1
union 
select '���������' pendingStatus,sum(P00) P00,sum(P02) P02,sum(P05) P05,sum(P08) P08,sum(P09) P09,sum(P10) P10,sum(P11) P11,sum(P12) P12,sum(P13) P13,sum(P14) P14,sum(P15) P15,sum(P16) P16,sum(P17) P17,sum(P18) P18,sum(sumTelled) sumTelled,sum(sumAll) sumAll from #tempTb1
order by pendingStatus


--====================================

IF OBJECT_ID('tempdb..#tempTb2') is not null DROP TABLE #tempTb2
select pendingStatus, isnull([p2],0) P02,isnull([p5],0) P05,isnull([p8],0) P08,isnull([p9],0) P09,isnull([p10],0) P10,isnull([p11],0) P11,isnull([p12],0) P12
,isnull([p13],0) P13,isnull([p14],0) P14,isnull([p15],0) P15,isnull([p16],0) P16,isnull([p17],0) P17,isnull([p18],0) P18
,(isnull([p2],0)+isnull([p5],0)+isnull([p8],0)+isnull([p9],0)+isnull([p10],0)+isnull([p11],0)+isnull([p12],0)+isnull([p13],0)+isnull([p14],0)
+isnull([p15],0)+isnull([p16],0)+isnull([p17],0)+isnull([p18],0)) sumTelled
into #tempTb2
from (
	select count(*) cnt, pendingStatus,pCallResultId
	from (
		SELECT		
			  case
				when t.crPendingPeriod<=1 then '1 ����+1 �Ǵ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=0 and t.crDisconnectedDate<=15 then '2 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 0-15 �ѹ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >=16 and t.crDisconnectedDate<=90 then '3 ��ҧ 2 �Ǵ �Ҵ��õԴ��� 16-90 �ѹ'
				when t.crPendingPeriod=2 and t.crDisconnectedDate >90 then '4 ��ҧ 2 �Ǵ �Ҵ��õԴ����ҡ���� 90 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=0 and t.crDisconnectedDate <=15 then '5 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��� 0-15 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=16 and t.crDisconnectedDate <=90 then '6 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��� 16-90 �ѹ'
				when t.crPendingPeriod>=3 and t.crDisconnectedDate >=91 then '7 ��ҧ����� 3 �Ǵ���� �Ҵ��õԴ��͵���� 91 �ѹ����'
			  end pendingStatus
			  ,case
				when isnull(lcr.callResultId,0)<>0 then 'p'+convert(varchar(max),lcr.callResultId)
				else 'p0'
				end pCallResultId
			  ,lcr.[description]
			  
		  FROM dc_telephonings t 
		  left join dc_lastCallResultOfEachConts lcr on t.id=lcr.telephoningId 		  		  
		  where 1=1	and t.specifiedCallStartDate=@specifiedStartDate  
		  and lcr.dateOfCall between @fromDate and @toDate
	) p
	group by pendingStatus,pCallResultId
) tb
pivot(
	sum(cnt) for pCallResultId in ([p2],[p5],[p8],[p9],[p10],[p11],[p12],[p13],[p14],[p15],[p16],[p17],[p18])
) pv

select pendingStatus,P02,P05,P08,P09,P10,P11,P12,P13,P14,P15,P16,P17,P18,sumTelled from #tempTb2
union 
select '���������' pendingStatus,sum(P02) P02,sum(P05) P05,sum(P08) P08,sum(P09) P09,sum(P10) P10,sum(P11) P11,sum(P12) P12,sum(P13) P13,sum(P14) P14,sum(P15) P15,sum(P16) P16,sum(P17) P17,sum(P18) P18,sum(sumTelled) sumTelled from #tempTb2
order by pendingStatus
