use HIINCOME;
declare @contno varchar(max)
	   ,@periods int
	   ,@maxDealDate datetime, @lastFullPaidPeriod int, @currentPeriod int, @realCurrentPeriod int, @lastPartialPaidPeriod int;
declare @pendingPeriods int;
set @contno='!HP-13030012';


select 
	 periods.periods --�ӹǹ�Ǵ
	,maxDealDate.maxDealDate as maxDealDate --�ѹ�Ѵ����㹧Ǵ�ش����
	,isnull(lastFullPaidPeriod.lastFullPaidPeriod,0) as lastFullPaidPeriod --�ѹ�����Фú������
	,case isnull(currentPeriod.currentPeriod,0) when 0 then periods.periods else currentPeriod.currentPeriod end as currentPeriod --�Ǵ�Ѩ�غѹ
	,currentPeriod.currentPeriod as realCurrentPeriod --�Ǵ�Ѩ�غѹ��ԧ�
	,isnull(lastPartialPaidPeriod.lastPartialPaidPeriod,0) as lastPartialPaidPeriod --�Ǵ��������§�ҧ��ǹ
	,(case isnull(currentPeriod.currentPeriod,0) when 0 then periods.periods else currentPeriod.currentPeriod end -
	 isnull(lastFullPaidPeriod.lastFullPaidPeriod,0)) as pendingPeriods --�ӹǹ�Ǵ����ҧ����
	,amountOfLastPaid.amountOfLastPaid --�Ǵ����ش�����кҧ��ǹ �Ҩ���繧Ǵ���ǡѺ�������������
	,amountOfLastPaid.DAMT --�ӹǹ����ͧ��������ЧǴ
/*
select 
	 @periods = periods.periods
	,@maxDealDate = maxDealDate.maxDealDate
	,@lastFullPaidPeriod = isnull(lastFullPaidPeriod.lastFullPaidPeriod,0)
	,@currentPeriod = case isnull(currentPeriod.currentPeriod,0) when 0 then periods.periods else currentPeriod.currentPeriod end
	,@realCurrentPeriod = currentPeriod.currentPeriod
	,@lastPartialPaidPeriod = isnull(lastPartialPaidPeriod.lastPartialPaidPeriod,0)
*/
/*
select @pendingPeriods = (case isnull(currentPeriod.currentPeriod,0) when 0 then periods.periods else currentPeriod.currentPeriod end -
	 isnull(lastFullPaidPeriod.lastFullPaidPeriod,0))
*/
from 
( --���ѭ�Ҽ�͹���С��Ǵ
  select max(NOPAY) as periods, CONTNO from ARPAY where CONTNO=@contno group by CONTNO
) as periods left join
(
  select max(DDATE) as maxDealDate, CONTNO from ARPAY where CONTNO=@contno group by CONTNO
) as maxDealDate on periods.CONTNO=maxDealDate.CONTNO left join 
( --�Ǵ�����Фú�ء�ҷ�繧Ǵ�ش����
  select max(NOPAY) as lastFullPaidPeriod, CONTNO from ARPAY 
  where CONTNO=@contno and DAMT=PAYMENT
  group by CONTNO
) as lastFullPaidPeriod on periods.CONTNO=lastFullPaidPeriod.CONTNO left join
( --�Ǵ����������Ǻ�ҧ �Ҩ����ҡѺ�Ǵ�����Фú����
  select max(NOPAY) as lastPartialPaidPeriod, CONTNO from ARPAY 
  where CONTNO=@contno and PAYMENT <> 0
  group by CONTNO
) as lastPartialPaidPeriod on periods.CONTNO=lastPartialPaidPeriod.CONTNO left join
( --�Ǵ�Ѩ�غѹ �ҡ�ѧ���֧�Ǵ�ش�����ѹ���բ������͡��
  select NOPAY as currentPeriod,CONTNO,DDATE from ARPAY
  where CONTNO=@contno
  and getdate()<=DDATE 
  and DATEADD(month, 1,getdate())>DDATE
) as currentPeriod on periods.CONTNO=currentPeriod.CONTNO left join
( -- �ӹǹ�Թ������㹧Ǵ����ش
  select DAMT as damt, PAYMENT as amountOfLastPaid,CONTNO,NOPAY from ARPAY
  where CONTNO=@contno   
) as amountOfLastPaid on maxDealDate.CONTNO=amountOfLastPaid.CONTNO and amountOfLastPaid.NOPAY=lastPartialPaidPeriod.lastPartialPaidPeriod

--print @pendingPeriods;

--print @currentPeriod - @lastFullPaidPeriod;

--select * from ARPAY where CONTNO='SHP-12110008' order by NOPAY;
