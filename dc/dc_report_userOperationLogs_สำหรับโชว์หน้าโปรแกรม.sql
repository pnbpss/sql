use serviceweb
declare @last7Day datetime;
set @last7Day = DATEADD(day,-7,getdate());
select 
u.name
,logs.descriptions
,replace(replace(logs.postReq,'array (',''),')','') postReq
,logs.ipAddress
,dbo.FN_ConvertDate(logs.dateTimeTried,'Y/M/D')d
,RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(HH, logs.dateTimeTried)), 2)
 +':'+RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(MM, logs.dateTimeTried)), 2)
 +':'+RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(SS, logs.dateTimeTried)), 2)hms

from dc_userOperationLogs logs left join dc_users u on logs.userId=u.id 
where logs.userId=1
and dateTimeTried>=@last7Day
--order by dateTimeTried desc
union
select 
u.name
,'ดูรายงานหมายเลข'+convert(varchar,logs.reportId) reportId
,replace(replace(logs.requestDump,'array (',''),')','') postReq
,logs.ipAddress
,dbo.FN_ConvertDate(logs.viewTime,'Y/M/D')d
,RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(HH, logs.viewTime)), 2)
 +':'+RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(MM, logs.viewTime)), 2)
 +':'+RIGHT('00' + CONVERT(NVARCHAR(2), DATEPART(SS, logs.viewTime)), 2)hms
 from dc_userViewReportsLogs logs left join dc_users u on  logs.userId=u.id
where logs.userId=1
and viewTime>=@last7Day
--order by logs.viewTime desc 