use serviceweb;
select a.CONTNO'เลขที่สัญญา',convert(varchar,a.LACCPDT,111) 'ผลัดถึง',convert(varchar,a.INPDT,111) 'วดป.บันทึกผลัดดิว',convert(varchar,b.issuedDate,111) 'วดป.จ่ายงาน',b.dcFullName 'พนง.เร่งรัด',b.locat 'สาขา'
,b.customerFullName 'ชื่อลค.'
,case when convert(varchar,b.issuedDate,111)>convert(varchar,a.INPDT,111) then 'จ่ายก่อนบันทึกผลัด'
	when convert(varchar,b.issuedDate,111)=convert(varchar,a.INPDT,111) then 'จ่ายวันเดียวกับวันบันทึกผลัด'
	else 'จ่ายหลังวันบันทึกผลัด'
end info
--,'',b.*
from [dc_onPostponePayContracts] a left join 
dbo.dc_beingOnCollectionConts b on a.CONTNO=b.contno
where b.contno is not null
order by issuedDate desc
--select top 5 * from wb_chqtran;