use serviceweb;
declare @maxLoadDateTime datetime;
set @maxLoadDateTime=(select MAX(loadDateTime) from dc_pendingStatsOn10201530)
--set @maxLoadDateTime = '2016/06/10'
select ysdate,msdate,isnull([m00],0) m00,	isnull([m01],0) m01,	isnull([m02],0) m02,	isnull([m03],0) m03,	isnull([m04],0) m04,	isnull([m05],0) m05,	isnull([m06],0) m06,	isnull([m07],0) m07,	isnull([m08],0) m08,	isnull([m09],0) m09,	isnull([m10],0) m10,	isnull([m11],0) m11,	isnull([m12],0) m12,	isnull([m13],0) m13,	isnull([m14],0) m14,	isnull([m15],0) m15,	isnull([m16],0) m16,	isnull([m17],0) m17,	isnull([m18],0) m18,	isnull([m19],0) m19,	isnull([m20],0) m20,	isnull([m21],0) m21,	isnull([m22],0) m22,	isnull([m23],0) m23,	isnull([m24],0) m24,	isnull([m25],0) m25,	isnull([m26],0) m26,	isnull([m27],0) m27,	isnull([m28],0) m28,	isnull([m29],0) m29,	isnull([m30],0) m30,	isnull([m31],0) m31,	isnull([m32],0) m32,	isnull([m33],0) m33,	isnull([m34],0) m34,	isnull([m35],0) m35,	isnull([m36],0) m36,	isnull([m37],0) m37,	isnull([m38],0) m38,	isnull([m39],0) m39,	isnull([m40],0) m40,	isnull([m41],0) m41,	isnull([m42],0) m42,	isnull([m43],0) m43,	isnull([m44],0) m44,	isnull([m45],0) m45,	isnull([m46],0) m46,	isnull([m47],0) m47,	isnull([m48],0) m48,	isnull([m49],0) m49,	isnull([m50],0) m50
from (
	select COUNT(*)cnt,MONTH(sdate)msdate,year(sdate)ysdate,'m'+ case when len(convert(varchar,pendingFromPeriod))=1 then '0'+convert(varchar,pendingFromPeriod) else convert(varchar,pendingFromPeriod) end pdp
	from dbo.dc_pendingStatsOn10201530 pso
	where 1=1
	and not (pso.lastpassdue='Y' and pendingFromPeriod=0)
	and convert(varchar,loadDateTime,111)=convert(varchar,@maxLoadDateTime,111)
	group by MONTH(sdate),year(sdate),pendingFromPeriod
)t pivot (
	sum(cnt) for pdp in ([m00],	[m01],	[m02],	[m03],	[m04],	[m05],	[m06],	[m07],	[m08],	[m09],	[m10],	[m11],	[m12],	[m13],	[m14],	[m15],	[m16],	[m17],	[m18],	[m19],	[m20],	[m21],	[m22],	[m23],	[m24],	[m25],	[m26],	[m27],	[m28],	[m29],	[m30],	[m31],	[m32],	[m33],	[m34],	[m35],	[m36],	[m37],	[m38],	[m39],	[m40],	[m41],	[m42],	[m43],	[m44],	[m45],	[m46],	[m47],	[m48],	[m49],	[m50])	
)p
order by ysdate desc,msdate desc


--select * from HIINCOME.dbo.ARMAST where year(SDATE)=2559

--select CONVERT(varchar,getdate(),111)