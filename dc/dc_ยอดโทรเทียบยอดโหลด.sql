select 
convert(varchar,main.loadDate,111)loadDate 
,main.cnt loadedCount
,isnull(called.cnt,0) calledCount
,isnull(aggr.notCalled,0) notCalledCount
,aggr.pcCalled
from
(
	select count(*)cnt,loadDate
	from dbo.dc_telephonings t 
	group by loadDate 
)main left join (
	select count(*)cnt,t.loadDate
	from dbo.dc_telephonings t left join dbo.dc_lastCallResultOfEachConts cro on t.id=cro.telephoningId
	where cro.telephoningId is not null
	group by t.loadDate 
)called on main.loadDate=called.loadDate 
cross apply (
	select cast((isnull(called.cnt,0)*1.00*100/main.cnt) as decimal(18,2)) pcCalled
	,main.cnt-called.cnt notCalled
)aggr
order by main.loadDate desc;
