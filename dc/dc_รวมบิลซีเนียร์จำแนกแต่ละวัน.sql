IF OBJECT_ID('tempdb..#dob') is not null DROP TABLE #dob
select '������ū������' pendingStatus,locat,isnull(D01,0)D01,isnull(D02,0)D02,isnull(D03,0)D03,isnull(D04,0)D04,isnull(D05,0)D05,isnull(D06,0)D06,
isnull(D07,0)D07,isnull(D08,0)D08,isnull(D09,0)D09,isnull(D10,0)D10,isnull(D11,0)D11,isnull(D12,0)D12,isnull(D13,0)D13,
isnull(D14,0)D14,isnull(D15,0)D15,isnull(D16,0)D16,isnull(D17,0)D17,isnull(D18,0)D18,isnull(D19,0)D19,isnull(D20,0)D20,
isnull(D21,0)D21,isnull(D22,0)D22,isnull(D23,0)D23,isnull(D24,0)D24,isnull(D25,0)D25,isnull(D26,0)D26,isnull(D27,0)D27,
isnull(D28,0)D28,isnull(D29,0)D29,isnull(D30,0)D30,isnull(D31,0)D31
into #dob
from (
	select count(*) cnt, LOCATRECV locat
	,case when day(TMBILDT)<10 then 'D0'+convert(varchar(max),day(TMBILDT))
		else 'D'+convert(varchar(max),day(TMBILDT))
	end dob
	from wb_chqtran where TMBILDT between '20160701' and '20160731' group by LOCATRECV,day(TMBILDT)
) t 
pivot
(
	sum(cnt) for dob in (D01,D02,D03,D04,D05,D06,D07,D08,D09,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31)
) pv

select * from #dob