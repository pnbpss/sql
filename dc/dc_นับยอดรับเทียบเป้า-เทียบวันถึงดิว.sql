use serviceweb;
declare @month int,@day int, @year int;
set @month=7;set @day=30;set @year=2016;

select chq.locat,chq.y,chq.m,chq.amt,dueOnDay.amtOnDue,case when dueOnDay.amtOnDue=0 then 0 else (chq.amt/dueOnDay.amtOnDue)*100 end as pcentDueDate
from 
(
	select year(chq.TMBILDT)y,month(chq.TMBILDT)m,sum(chq.NETPAY)amt,chq.LOCATPAY locat
	from HIC4REPORT.dbo.CHQTRAN chq
	where 1=1
	and chq.FLAG<>'C' and chq.PAYFOR in ('006','007')
	and month(chq.TMBILDT)=@month and year(chq.TMBILDT)=@year 
	group by year(chq.TMBILDT),month(chq.TMBILDT),chq.LOCATPAY
)
 chq
left join (
	select sum(arpay.DAMT)amtOnDue,month(arpay.DDATE)m,year(arpay.DDATE)y,arpay.LOCAT locat
	from HIC4REPORT.dbo.ARPAY arpay
	where month(arpay.DDATE)=@month and year(arpay.DDATE)=@year 
	group by month(arpay.DDATE),year(arpay.DDATE),arpay.LOCAT 	
) dueOnDay on chq.y=dueOnDay.y and chq.m=dueOnDay.m and chq.locat=dueOnDay.locat
order by chq.y desc, chq.m desc;