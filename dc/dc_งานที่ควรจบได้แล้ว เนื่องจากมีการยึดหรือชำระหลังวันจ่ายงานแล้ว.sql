use serviceweb;

declare @maxLoadDate datetime;
set @maxLoadDate = '2016/05/11';
select boc.contno
,boc.dcTFSName
,dbo.FN_ConvertDate(boc.loadDate,'D/M/Y') loadDate
,dbo.FN_ConvertDate(boc.issuedDate,'D/M/Y') issuedDate
,dbo.FN_ConvertDate(va.endContract,'D/M/Y') endContract
--,ljf.contno
,dbo.FN_ConvertDate(va.lastPaidDate,'D/M/Y') lastPaidDate,va.netPendingPeriod,ah.CONTNO ahContno,dbo.FN_ConvertDate(ah.YDATE,'D/M/Y') ydate
,case 
	when isnull(ah.YDATE,'')!='' then '�ִ����'
	when isnull(va.lastPaidDate,'')!='' then '��������'
	else 'n/a'
end whyClose
from dc_beingOnCollectionConts boc left join dc_loadedJobsForAssignToDebtCollector ljf on boc.contno=ljf.contno and ljf.loadDate=@maxLoadDate
left join view_arpay2 va on boc.contno=va.CONTNO
left join wb_arhold ah on boc.contno=ah.CONTNO
where 1=1
--and ljf.contno is null
--and va.netPendingPeriod<3
and ((va.lastPaidDate>=boc.issuedDate) or ah.CONTNO is not null)
order by boc.issuedDate desc
