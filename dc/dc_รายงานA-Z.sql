/****** Script for SelectTopNRows command from SSMS  ******/
use serviceweb;
--goto pvt
/*
IF OBJECT_ID('tempdb.dbo.#abc', 'U') IS NOT NULL DROP TABLE #abc;
IF OBJECT_ID('tempdb.dbo.#xyz', 'U') IS NOT NULL DROP TABLE #xyz;
SELECT [contno],[locat],[gcode],[strno],[startContract],[endContract],[lastPaidDate],[installmentPerPeriod],[lastPaidInstallmentPeriod]
,[lastPaidInstall],[AllPeriod],[currentperiod],[disconnectedDays],[pendingInstallmentPeriod],[netPendingPeriod],[pendingToPeriod]
,[postponeDueTo],[lastpassdue],[neverpaid],[nextDealDate],[buyerCuscod],[suretyCuscod1],[suretyCuscod2]
,[suretyCuscod3],[sdate],[fh],[contStatus],[debtPlusVat],[remainDebt],[pendingFromPeriod],[loadDateTime]
,case when netPendingPeriod=0 then 'P00_P00'
    when netPendingPeriod=1 then 'P01_P01'
    when netPendingPeriod=2 then 'P02_P02'
    when netPendingPeriod=3 then 'P03_P03'
    when netPendingPeriod=4 then 'P04_P04'
    when netPendingPeriod between 5 and 6 then 'P05_P06'
    when netPendingPeriod between 7 and 8 then 'P07_P08'
    when netPendingPeriod between 9 and 10 then 'P09_P10'
    when netPendingPeriod > 10 then 'P11_PMM'
end pdType
,datediff(month,loadDateTime,getdate()) M
--,case when convert(varchar,[loadDateTime],112)='20160630' then '3' when convert(varchar,[loadDateTime],112)='20160615' then '2' when convert(varchar,[loadDateTime],112)='20160610' then '1' end M

into #abc
FROM [serviceweb].[dbo].[dc_pendingStatsOn10201530]
where 1=1
--and datediff(month,loadDateTime,getdate()) <=3
--and datediff(month,lastPaidDate,getdate()) <=3
--and day(loadDateTime) in (30)
--and day(lastPaidDate) in (10,20,28,29,30,31)
--and day(loadDateTime) in (28,29,30,31)

--select distinct pdType+'_'+convert(varchar,[loadDateTime],112) loadDate_PDType from #abc order by pdType+'_'+convert(varchar,[loadDateTime],112)
--select distinct loadDateTime from dc_pendingStatsOn10201530

pvt:

select p.locat,rpz.[groupNo],rpz.groupName,rpz.districtZone,rpz.branchName,pv.PROVDES pv,rpz.superVisor,rpz.dcCount
,isnull(P00_P00_M3,0)P00_P00_M3,isnull(P00_P00_M2,0)P00_P00_M2,isnull(P00_P00_M1,0)P00_P00_M1
,isnull(P01_P01_M3,0)P01_P01_M3,isnull(P01_P01_M2,0)P01_P01_M2,isnull(P01_P01_M1,0)P01_P01_M1
,isnull(P02_P02_M3,0)P02_P02_M3,isnull(P02_P02_M2,0)P02_P02_M2,isnull(P02_P02_M1,0)P02_P02_M1
,isnull(P03_P03_M3,0)P03_P03_M3,isnull(P03_P03_M2,0)P03_P03_M2,isnull(P03_P03_M1,0)P03_P03_M1
,isnull(P04_P04_M3,0)P04_P04_M3,isnull(P04_P04_M2,0)P04_P04_M2,isnull(P04_P04_M1,0)P04_P04_M1
,aggr.P05_P10_M3,aggr.P05_P10_M2,aggr.P05_P10_M1
,isnull(P11_PMM_M3,0)P11_PMM_M3,isnull(P11_PMM_M2,0)P11_PMM_M2,isnull(P11_PMM_M1,0)P11_PMM_M1
,aggr.sM3,aggr.sM2,aggr.sM1
,aggr.sP00_P02_M3,aggr.sP00_P02_M2,aggr.sP00_P02_M1
,aggr.sP03_P04_M3,aggr.sP03_P04_M2,aggr.sP03_P04_M1
,aggr.sP05_PMM_M3,aggr.sP05_PMM_M2,aggr.sP05_PMM_M1
,aggr.sP03_PMM_M3,aggr.sP03_PMM_M2,aggr.sP03_PMM_M1

,difAndPercent.dP05UpM2,difAndPercent.dP05UpM1,difAndPercent.dP03UpM2,difAndPercent.dP03UpM1
,cast(difAndPercent.pP03UP_M3 as decimal(18,2))pP03UP_M3,cast(difAndPercent.pP03UP_M2 as decimal(18,2))pP03UP_M2,cast(difAndPercent.pP03UP_M1 as decimal(18,2))pP03UP_M1
,cast(difAndPercent.pP05UP_M3 as decimal(18,2))pP05UP_M3,cast(difAndPercent.pP05UP_M2 as decimal(18,2))pP05UP_M2,cast(difAndPercent.pP05UP_M1 as decimal(18,2))pP05UP_M1

into #xyz
from(
		select count(*) cnt,locat,pdType+'_M'+convert(varchar,M) PDType_loadDate
		from #abc
		group by locat,pdType+'_M'+convert(varchar,M)
	) t pivot (
	sum(cnt) for PDType_loadDate in (P00_P00_M1,P00_P00_M2,	P00_P00_M3,	P01_P01_M1,	P01_P01_M2,	P01_P01_M3,	P02_P02_M1,	P02_P02_M2,	P02_P02_M3,	P03_P03_M1,	P03_P03_M2,	P03_P03_M3,	P04_P04_M1,	P04_P04_M2,	P04_P04_M3,	P05_P06_M1,	P05_P06_M2,	P05_P06_M3,	P07_P08_M1,	P07_P08_M2,	P07_P08_M3,	P09_P10_M1,	P09_P10_M2,	P09_P10_M3,	P11_PMM_M1,	P11_PMM_M2,	P11_PMM_M3)
)p
cross apply (select 
			
			 (isnull(P05_P06_M3,0)+isnull(P07_P08_M3,0)+isnull(P09_P10_M3,0)) P05_P10_M3
			,(isnull(P05_P06_M2,0)+isnull(P07_P08_M2,0)+isnull(P09_P10_M2,0)) P05_P10_M2
			,(isnull(P05_P06_M1,0)+isnull(P07_P08_M1,0)+isnull(P09_P10_M1,0)) P05_P10_M1
			
			,(isnull(P00_P00_M3,0)+isnull(P01_P01_M3,0)+isnull(P02_P02_M3,0)+isnull(P03_P03_M3,0)+isnull(P04_P04_M3,0)+isnull(P05_P06_M3,0)+isnull(P07_P08_M3,0)+isnull(P09_P10_M3,0)+isnull(P11_PMM_M3,0))sM3
			,(isnull(P00_P00_M2,0)+isnull(P01_P01_M2,0)+isnull(P02_P02_M2,0)+isnull(P03_P03_M2,0)+isnull(P04_P04_M2,0)+isnull(P05_P06_M2,0)+isnull(P07_P08_M2,0)+isnull(P09_P10_M2,0)+isnull(P11_PMM_M2,0))sM2
			,(isnull(P00_P00_M1,0)+isnull(P01_P01_M1,0)+isnull(P02_P02_M1,0)+isnull(P03_P03_M1,0)+isnull(P04_P04_M1,0)+isnull(P05_P06_M1,0)+isnull(P07_P08_M1,0)+isnull(P09_P10_M1,0)+isnull(P11_PMM_M1,0))sM1
			
			,(isnull(P00_P00_M3,0)+isnull(P01_P01_M3,0)+isnull(P02_P02_M3,0)) sP00_P02_M3
			,(isnull(P00_P00_M2,0)+isnull(P01_P01_M2,0)+isnull(P02_P02_M2,0)) sP00_P02_M2
			,(isnull(P00_P00_M1,0)+isnull(P01_P01_M1,0)+isnull(P02_P02_M1,0)) sP00_P02_M1
			
			,(isnull(P03_P03_M3,0)+isnull(P04_P04_M3,0)) sP03_P04_M3
			,(isnull(P03_P03_M2,0)+isnull(P04_P04_M2,0)) sP03_P04_M2
			,(isnull(P03_P03_M1,0)+isnull(P04_P04_M1,0)) sP03_P04_M1
			
			,(isnull(P05_P06_M3,0)+isnull(P07_P08_M3,0)+isnull(P09_P10_M3,0)+isnull(P11_PMM_M3,0)) sP05_PMM_M3
			,(isnull(P05_P06_M2,0)+isnull(P07_P08_M2,0)+isnull(P09_P10_M2,0)+isnull(P11_PMM_M2,0)) sP05_PMM_M2
			,(isnull(P05_P06_M1,0)+isnull(P07_P08_M1,0)+isnull(P09_P10_M1,0)+isnull(P11_PMM_M1,0)) sP05_PMM_M1
			
			,(isnull(P03_P03_M3,0)+isnull(P04_P04_M3,0)+isnull(P05_P06_M3,0)+isnull(P07_P08_M3,0)+isnull(P09_P10_M3,0)+isnull(P11_PMM_M3,0)) sP03_PMM_M3
			,(isnull(P03_P03_M2,0)+isnull(P04_P04_M2,0)+isnull(P05_P06_M2,0)+isnull(P07_P08_M2,0)+isnull(P09_P10_M2,0)+isnull(P11_PMM_M2,0)) sP03_PMM_M2
			,(isnull(P03_P03_M1,0)+isnull(P04_P04_M1,0)+isnull(P05_P06_M1,0)+isnull(P07_P08_M1,0)+isnull(P09_P10_M1,0)+isnull(P11_PMM_M1,0)) sP03_PMM_M1
			) as aggr
cross apply (select 
			  aggr.sP05_PMM_M1-aggr.sP05_PMM_M2 dP05UpM1
			 ,aggr.sP05_PMM_M2-aggr.sP05_PMM_M3 dP05UpM2
			 
			 ,aggr.sP03_PMM_M1-aggr.sP03_PMM_M2 dP03UpM1
			 ,aggr.sP03_PMM_M2-aggr.sP03_PMM_M3 dP03UpM2
			 
			 ,case when sM3 = 0 then 0 else (sP03_PMM_M3*100*1.00)/sM3 end pP03UP_M3
			 ,case when sM2 = 0 then 0 else sP03_PMM_M2*100*1.00/sM2 end pP03UP_M2
			 ,case when sM1 = 0 then 0 else sP03_PMM_M1*100*1.00/sM1 end pP03UP_M1
			 
			 ,case when sM3 = 0 then 0 else sP05_PMM_M3*100*1.00/sM3 end pP05UP_M3
			 ,case when sM2 = 0 then 0 else sP05_PMM_M2*100*1.00/sM2 end pP05UP_M2
			 ,case when sM1 = 0 then 0 else sP05_PMM_M1*100*1.00/sM1 end pP05UP_M1
			 
			) as difAndPercent

inner join dc_reportA2ZInfo rpz on p.locat=rpz.branchId and rpz.display='Y'
left join wb_branchzones bz on p.locat=bz.locat
left join wb_provinces pv on bz.zone=pv.PROVCOD

select locat,	groupNo,	groupName,	districtZone,	branchName,	pv,superVisor,dcCount,	P00_P00_M3,	P00_P00_M2,	P00_P00_M1,	P01_P01_M3,	P01_P01_M2,	P01_P01_M1,	P02_P02_M3,	P02_P02_M2,	P02_P02_M1,	P03_P03_M3,	P03_P03_M2,	P03_P03_M1,	P04_P04_M3,	P04_P04_M2,	P04_P04_M1,	P05_P10_M3,	P05_P10_M2,	P05_P10_M1,	P11_PMM_M3,	P11_PMM_M2,	P11_PMM_M1,	sM3,	sM2,	sM1,	sP00_P02_M3,	sP00_P02_M2,	sP00_P02_M1,	sP03_P04_M3,	sP03_P04_M2,	sP03_P04_M1,	sP05_PMM_M3,	sP05_PMM_M2,	sP05_PMM_M1,	sP03_PMM_M3,	sP03_PMM_M2,	sP03_PMM_M1,	dP05UpM2,	dP05UpM1,	dP03UpM2,	dP03UpM1,	pP03UP_M3,	pP03UP_M2,	pP03UP_M1,	pP05UP_M3,	pP05UP_M2,	pP05UP_M1
from #xyz
union
select '๚รวม' locat,null groupNo, null groupName,districtZone,null branchName,null pv, null superVisor,null dcCount
,sum(P00_P00_M3)P00_P00_M3,	sum(P00_P00_M2)P00_P00_M2,	sum(P00_P00_M1)P00_P00_M1,	sum(P01_P01_M3)P01_P01_M3,	sum(P01_P01_M2)P01_P01_M2,	sum(P01_P01_M1)P01_P01_M1,	sum(P02_P02_M3)P02_P02_M3,	sum(P02_P02_M2)P02_P02_M2,	sum(P02_P02_M1)P02_P02_M1,	sum(P03_P03_M3)P03_P03_M3,	sum(P03_P03_M2)P03_P03_M2,	sum(P03_P03_M1)P03_P03_M1,	sum(P04_P04_M3)P04_P04_M3,	sum(P04_P04_M2)P04_P04_M2,	sum(P04_P04_M1)P04_P04_M1,	sum(P05_P10_M3)P05_P10_M3,	sum(P05_P10_M2)P05_P10_M2,	sum(P05_P10_M1)P05_P10_M1,	sum(P11_PMM_M3)P11_PMM_M3,	sum(P11_PMM_M2)P11_PMM_M2,	sum(P11_PMM_M1)P11_PMM_M1,	sum(sM3)sM3,	sum(sM2)sM2,	sum(sM1)sM1,	sum(sP00_P02_M3)sP00_P02_M3,	sum(sP00_P02_M2)sP00_P02_M2,	sum(sP00_P02_M1)sP00_P02_M1,	sum(sP03_P04_M3)sP03_P04_M3,	sum(sP03_P04_M2)sP03_P04_M2,	sum(sP03_P04_M1)sP03_P04_M1,	sum(sP05_PMM_M3)sP05_PMM_M3,	sum(sP05_PMM_M2)sP05_PMM_M2,	sum(sP05_PMM_M1)sP05_PMM_M1,	sum(sP03_PMM_M3)sP03_PMM_M3,	sum(sP03_PMM_M2)sP03_PMM_M2,	sum(sP03_PMM_M1)sP03_PMM_M1,	(sum(sP05_PMM_M2)-sum(sP05_PMM_M3))dP05UpM2,	(sum(sP05_PMM_M1)-sum(sP05_PMM_M2))dP05UpM1,	(sum(sP03_PMM_M2)-sum(sP03_PMM_M3))dP03UpM2,	(sum(sP03_PMM_M1)-sum(sP03_PMM_M2))dP03UpM1,	cast(case when sum(sM3)=0 then 0 else (sum(sP03_PMM_M3)*100/sum(sM3)) end  as decimal(5,2)) pP03UP_M3,	cast(case when sum(sM2)=0 then 0 else (sum(sP03_PMM_M2)*100/sum(sM2)) end  as decimal(5,2)) pP03UP_M2,	cast(case when sum(sM1)=0 then 0 else (sum(sP03_PMM_M1)*100/sum(sM1)) end as decimal(5,2)) pP03UP_M1,	cast(case when sum(sM3)=0 then 0 else (sum(sP05_PMM_M3)*100/sum(sM3)) end as decimal(5,2)) pP05UP_M3,	cast(case when sum(sM2)=0 then 0 else (sum(sP05_PMM_M2)*100/sum(sM2)) end as decimal(5,2)) pP05UP_M2,	cast(case when sum(sM1)=0 then 0 else (sum(sP05_PMM_M1)*100/sum(sM1)) end as decimal(5,2)) pP05UP_M1
from #xyz group by districtZone
union
select '๚รวม' locat,null groupNo, null groupName,'๚ทั้งหมด' districtZone,null branchName,null pv, null superVisor,null dcCount
,sum(P00_P00_M3)P00_P00_M3,	sum(P00_P00_M2)P00_P00_M2,	sum(P00_P00_M1)P00_P00_M1,	sum(P01_P01_M3)P01_P01_M3,	sum(P01_P01_M2)P01_P01_M2,	sum(P01_P01_M1)P01_P01_M1,	sum(P02_P02_M3)P02_P02_M3,	sum(P02_P02_M2)P02_P02_M2,	sum(P02_P02_M1)P02_P02_M1,	sum(P03_P03_M3)P03_P03_M3,	sum(P03_P03_M2)P03_P03_M2,	sum(P03_P03_M1)P03_P03_M1,	sum(P04_P04_M3)P04_P04_M3,	sum(P04_P04_M2)P04_P04_M2,	sum(P04_P04_M1)P04_P04_M1,	sum(P05_P10_M3)P05_P10_M3,	sum(P05_P10_M2)P05_P10_M2,	sum(P05_P10_M1)P05_P10_M1,	sum(P11_PMM_M3)P11_PMM_M3,	sum(P11_PMM_M2)P11_PMM_M2,	sum(P11_PMM_M1)P11_PMM_M1,	sum(sM3)sM3,	sum(sM2)sM2,	sum(sM1)sM1,	sum(sP00_P02_M3)sP00_P02_M3,	sum(sP00_P02_M2)sP00_P02_M2,	sum(sP00_P02_M1)sP00_P02_M1,	sum(sP03_P04_M3)sP03_P04_M3,	sum(sP03_P04_M2)sP03_P04_M2,	sum(sP03_P04_M1)sP03_P04_M1,	sum(sP05_PMM_M3)sP05_PMM_M3,	sum(sP05_PMM_M2)sP05_PMM_M2,	sum(sP05_PMM_M1)sP05_PMM_M1,	sum(sP03_PMM_M3)sP03_PMM_M3,	sum(sP03_PMM_M2)sP03_PMM_M2,	sum(sP03_PMM_M1)sP03_PMM_M1,	(sum(sP05_PMM_M2)-sum(sP05_PMM_M3))dP05UpM2,	(sum(sP05_PMM_M1)-sum(sP05_PMM_M2))dP05UpM1,	(sum(sP03_PMM_M2)-sum(sP03_PMM_M3))dP03UpM2,	(sum(sP03_PMM_M1)-sum(sP03_PMM_M2))dP03UpM1,	cast(case when sum(sM3)=0 then 0 else (sum(sP03_PMM_M3)*100/sum(sM3)) end  as decimal(5,2)) pP03UP_M3,	cast(case when sum(sM2)=0 then 0 else (sum(sP03_PMM_M2)*100/sum(sM2)) end  as decimal(5,2)) pP03UP_M2,	cast(case when sum(sM1)=0 then 0 else (sum(sP03_PMM_M1)*100/sum(sM1)) end as decimal(5,2)) pP03UP_M1,	cast(case when sum(sM3)=0 then 0 else (sum(sP05_PMM_M3)*100/sum(sM3)) end as decimal(5,2)) pP05UP_M3,	cast(case when sum(sM2)=0 then 0 else (sum(sP05_PMM_M2)*100/sum(sM2)) end as decimal(5,2)) pP05UP_M2,	cast(case when sum(sM1)=0 then 0 else (sum(sP05_PMM_M1)*100/sum(sM1)) end as decimal(5,2)) pP05UP_M1
from #xyz 

order by districtZone,locat
*/
IF OBJECT_ID('tempdb.dbo.#abc', 'U') IS NOT NULL DROP TABLE #abc;
IF OBJECT_ID('tempdb.dbo.#xyz', 'U') IS NOT NULL DROP TABLE #xyz;
SELECT [contno],[locat],[gcode],[strno],[startContract],[endContract],[lastPaidDate],[installmentPerPeriod],[lastPaidInstallmentPeriod]
,[lastPaidInstall],[AllPeriod],[currentperiod],[disconnectedDays],[pendingInstallmentPeriod],[netPendingPeriod],[pendingToPeriod]
,[postponeDueTo],[lastpassdue],[neverpaid],[nextDealDate],[buyerCuscod],[suretyCuscod1],[suretyCuscod2]
,[suretyCuscod3],[sdate],[fh],[contStatus],[debtPlusVat],[remainDebt],[pendingFromPeriod],[loadDateTime]
,case when disconnectedDays=0 then 'D000_000'
    when disconnectedDays between 1 and 31 then 'D001_031'
    when disconnectedDays between 32 and 60 then 'D032_060'
    when disconnectedDays between 61 and 90 then 'D061_090'
    when disconnectedDays between 91 and 120 then 'D091_120'
    when disconnectedDays between 121 and 150 then 'D121_150'
    when disconnectedDays between 151 and 180 then 'D151_180'
    when disconnectedDays between 181 and 365 then 'D181_365'
    when disconnectedDays > 365 then 'D365_UP'
end pdType
,datediff(month,loadDateTime,getdate()) M
into #abc
FROM [serviceweb].[dbo].[dc_pendingStatsOn10201530]
where 1=1
and datediff(month,loadDateTime,getdate()) <=3
and day(loadDateTime) in (28,29,30,31)

pvt:

select p.locat,rpz.[groupNo],rpz.groupName,rpz.districtZone,rpz.branchName,pv.PROVDES pv,rpz.superVisor,rpz.dcCount,
isnull(D000_000_M3,0) D000_000_M3, isnull(D000_000_M2,0) D000_000_M2, isnull(D000_000_M1,0) D000_000_M1,
isnull(D001_031_M3,0) D001_031_M3, isnull(D001_031_M2,0) D001_031_M2, isnull(D001_031_M1,0) D001_031_M1,
isnull(D032_060_M3,0) D032_060_M3, isnull(D032_060_M2,0) D032_060_M2, isnull(D032_060_M1,0) D032_060_M1,
isnull(D061_090_M3,0) D061_090_M3, isnull(D061_090_M2,0) D061_090_M2, isnull(D061_090_M1,0) D061_090_M1,
isnull(D091_120_M3,0) D091_120_M3, isnull(D091_120_M2,0) D091_120_M2, isnull(D091_120_M1,0) D091_120_M1,
isnull(D121_150_M3,0) D121_150_M3, isnull(D121_150_M2,0) D121_150_M2, isnull(D121_150_M1,0) D121_150_M1,
isnull(D151_180_M3,0) D151_180_M3, isnull(D151_180_M2,0) D151_180_M2, isnull(D151_180_M1,0) D151_180_M1,
isnull(D181_365_M3,0) D181_365_M3, isnull(D181_365_M2,0) D181_365_M2, isnull(D181_365_M1,0) D181_365_M1,
isnull(D365_UP_M3,0) D365_UP_M3 ,  isnull(D365_UP_M2,0) D365_UP_M2 ,  isnull(D365_UP_M1,0) D365_UP_M1,
aggr.D181_UP_M3,aggr.D181_UP_M2,aggr.D181_UP_M1,
aggr.sM3,aggr.sM2,aggr.sM1,aggr.D000_031_M1,aggr.D032_090_M1,aggr.D091_180_M1
,isnull(D181_365_M1,0) D181_365_M1_2,isnull(D365_UP_M1,0) D365_UP_M1_2
,aggr.D032_UP,aggr.D3M_UP_M1,aggr.D3M_UP_No365_M1,aggr.D032_180_M1,aggr.D181_UP
into #xyz
from(
		select count(*) cnt,locat,pdType+'_M'+convert(varchar,M) PDType_loadDate
		from #abc
		group by locat,pdType+'_M'+convert(varchar,M)		
	) t pivot (
		sum(cnt) for PDType_loadDate in (
						D000_000_M1, D000_000_M2, D000_000_M3,D001_031_M1, D001_031_M2, D001_031_M3,D032_060_M1, D032_060_M2, D032_060_M3,
						D061_090_M1, D061_090_M2, D061_090_M3,D091_120_M1, D091_120_M2, D091_120_M3,D121_150_M1, D121_150_M2, D121_150_M3,
						D151_180_M1, D151_180_M2, D151_180_M3,D181_365_M1, D181_365_M2, D181_365_M3,D365_UP_M1,  D365_UP_M2,  D365_UP_M3
				)
)p
cross apply (
			select 
			 (isnull(D181_365_M3,0)+isnull(D365_UP_M3,0)) D181_UP_M3
			,(isnull(D181_365_M2,0)+isnull(D365_UP_M2,0)) D181_UP_M2
			,(isnull(D181_365_M1,0)+isnull(D365_UP_M1,0)) D181_UP_M1
			
			,(isnull(D000_000_M3,0)+isnull(D001_031_M3,0)+isnull(D032_060_M3,0)+isnull(D061_090_M3,0)+isnull(D091_120_M3,0)+isnull(D121_150_M3,0)+isnull(D151_180_M3,0)+isnull(D181_365_M3,0)+isnull(D365_UP_M3,0)) sM3
			,(isnull(D000_000_M2,0)+isnull(D001_031_M2,0)+isnull(D032_060_M2,0)+isnull(D061_090_M2,0)+isnull(D091_120_M2,0)+isnull(D121_150_M2,0)+isnull(D151_180_M2,0)+isnull(D181_365_M2,0)+isnull(D365_UP_M2,0)) sM2
			,(isnull(D000_000_M1,0)+isnull(D001_031_M1,0)+isnull(D032_060_M1,0)+isnull(D061_090_M1,0)+isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)+isnull(D181_365_M1,0)+isnull(D365_UP_M1,0)) sM1
			
			,(isnull(D000_000_M1,0)+isnull(D001_031_M1,0)) D000_031_M1
			,(isnull(D032_060_M1,0)+isnull(D061_090_M1,0)) D032_090_M1
			,(isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)) D091_180_M1
			,(isnull(D032_060_M1,0)+isnull(D061_090_M1,0)+isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)+isnull(D181_365_M1,0)+isnull(D365_UP_M1,0)) D032_UP
			,(isnull(D061_090_M1,0)+isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)+isnull(D181_365_M1,0)+isnull(D365_UP_M1,0)) D3M_UP_M1
			,(isnull(D061_090_M1,0)+isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)+isnull(D181_365_M1,0)) D3M_UP_No365_M1
			,(isnull(D032_060_M1,0)+isnull(D061_090_M1,0)+isnull(D091_120_M1,0)+isnull(D121_150_M1,0)+isnull(D151_180_M1,0)) D032_180_M1
			,(isnull(D181_365_M1,0)+isnull(D365_UP_M1,0)) D181_UP			
			) aggr
inner join dc_reportA2ZInfo rpz on p.locat=rpz.branchId and rpz.display='Y'
left join wb_branchzones bz on p.locat=bz.locat
left join wb_provinces pv on bz.zone=pv.PROVCOD

select locat,	groupNo,	groupName,	districtZone,	branchName,	pv,	superVisor,	dcCount,	D000_000_M3,	D000_000_M2,	D000_000_M1,	D001_031_M3,	D001_031_M2,	D001_031_M1,	D032_060_M3,	D032_060_M2,	D032_060_M1,	D061_090_M3,	D061_090_M2,	D061_090_M1,	D091_120_M3,	D091_120_M2,	D091_120_M1,	D121_150_M3,	D121_150_M2,	D121_150_M1,	D151_180_M3,	D151_180_M2,	D151_180_M1,	D181_365_M3,	D181_365_M2,	D181_365_M1,	D365_UP_M3,	D365_UP_M2,	D365_UP_M1,	D181_UP_M3,	D181_UP_M2,	D181_UP_M1,	sM3,	sM2,	sM1,	D000_031_M1,	D032_090_M1,	D091_180_M1,	D181_365_M1_2,	D365_UP_M1_2,	D032_UP,	D3M_UP_M1,	D3M_UP_No365_M1,	D032_180_M1,	D181_UP
from #xyz

union
select '๚รวม' locat,null groupNo,null groupName,districtZone,null branchName, null pv,null superVisor, null dcCount,
sum(D000_000_M3) D000_000_M3,	sum(D000_000_M2) D000_000_M2,	sum(D000_000_M1) D000_000_M1,	sum(D001_031_M3) D001_031_M3,	sum(D001_031_M2) D001_031_M2,	sum(D001_031_M1) D001_031_M1,	sum(D032_060_M3) D032_060_M3,	sum(D032_060_M2) D032_060_M2,	sum(D032_060_M1) D032_060_M1,	sum(D061_090_M3) D061_090_M3,	sum(D061_090_M2) D061_090_M2,	sum(D061_090_M1) D061_090_M1,	sum(D091_120_M3) D091_120_M3,	sum(D091_120_M2) D091_120_M2,	sum(D091_120_M1) D091_120_M1,	sum(D121_150_M3) D121_150_M3,	sum(D121_150_M2) D121_150_M2,	sum(D121_150_M1) D121_150_M1,	sum(D151_180_M3) D151_180_M3,	sum(D151_180_M2) D151_180_M2,	sum(D151_180_M1) D151_180_M1,	sum(D181_365_M3) D181_365_M3,	sum(D181_365_M2) D181_365_M2,	sum(D181_365_M1) D181_365_M1,	sum(D365_UP_M3) D365_UP_M3,	sum(D365_UP_M2) D365_UP_M2,	sum(D365_UP_M1) D365_UP_M1,	sum(D181_UP_M3) D181_UP_M3,	sum(D181_UP_M2) D181_UP_M2,	sum(D181_UP_M1) D181_UP_M1,	sum(sM3) sM3,	sum(sM2) sM2,	sum(sM1) sM1,	sum(D000_031_M1) D000_031_M1,	sum(D032_090_M1) D032_090_M1,	sum(D091_180_M1) D091_180_M1,	sum(D181_365_M1_2) D181_365_M1_2,	sum(D365_UP_M1_2) D365_UP_M1_2,	sum(D032_UP) D032_UP,	sum(D3M_UP_M1) D3M_UP_M1,	sum(D3M_UP_No365_M1) D3M_UP_No365_M1,	sum(D032_180_M1) D032_180_M1,	sum(D181_UP) D181_UP
from #xyz group by districtZone

union
select '๚รวม' locat,null groupNo,null groupName,'๚ทั้งหมด'districtZone,null branchName, null pv,null superVisor, null dcCount,
sum(D000_000_M3) D000_000_M3,	sum(D000_000_M2) D000_000_M2,	sum(D000_000_M1) D000_000_M1,	sum(D001_031_M3) D001_031_M3,	sum(D001_031_M2) D001_031_M2,	sum(D001_031_M1) D001_031_M1,	sum(D032_060_M3) D032_060_M3,	sum(D032_060_M2) D032_060_M2,	sum(D032_060_M1) D032_060_M1,	sum(D061_090_M3) D061_090_M3,	sum(D061_090_M2) D061_090_M2,	sum(D061_090_M1) D061_090_M1,	sum(D091_120_M3) D091_120_M3,	sum(D091_120_M2) D091_120_M2,	sum(D091_120_M1) D091_120_M1,	sum(D121_150_M3) D121_150_M3,	sum(D121_150_M2) D121_150_M2,	sum(D121_150_M1) D121_150_M1,	sum(D151_180_M3) D151_180_M3,	sum(D151_180_M2) D151_180_M2,	sum(D151_180_M1) D151_180_M1,	sum(D181_365_M3) D181_365_M3,	sum(D181_365_M2) D181_365_M2,	sum(D181_365_M1) D181_365_M1,	sum(D365_UP_M3) D365_UP_M3,	sum(D365_UP_M2) D365_UP_M2,	sum(D365_UP_M1) D365_UP_M1,	sum(D181_UP_M3) D181_UP_M3,	sum(D181_UP_M2) D181_UP_M2,	sum(D181_UP_M1) D181_UP_M1,	sum(sM3) sM3,	sum(sM2) sM2,	sum(sM1) sM1,	sum(D000_031_M1) D000_031_M1,	sum(D032_090_M1) D032_090_M1,	sum(D091_180_M1) D091_180_M1,	sum(D181_365_M1_2) D181_365_M1_2,	sum(D365_UP_M1_2) D365_UP_M1_2,	sum(D032_UP) D032_UP,	sum(D3M_UP_M1) D3M_UP_M1,	sum(D3M_UP_No365_M1) D3M_UP_No365_M1,	sum(D032_180_M1) D032_180_M1,	sum(D181_UP) D181_UP
from #xyz 

order by districtZone,locat

