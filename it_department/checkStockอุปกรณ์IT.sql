use DBFREE
select LastQTY
--,[TimeStamp]
,stc.PartNo,pl.PartName,stc.BranchNo
,bz.locat
,br.BranchName 
FROM [SPStockCard] stc inner join 
(
	select MAX([TimeStamp])maxTimStamp,PartNo,BranchNo  from SPStockCard where (PartNo like 'IT-CPU%'  or PartNo like 'IT-NOTE%')
	group by PartNo,BranchNo 
) mxTs on stc.PartNo=mxTs.PartNo and stc.[TimeStamp]=mxTs.maxTimStamp and mxTs.BranchNo=stc.BranchNo
left join Branch br on stc.BranchNo=br.BranchNo
left join SPPartList pl on stc.PartNo=pl.PartNo
left join ITREPORT.dbo.wb_branchZones bz on br.BranchNo=bz.DBFREE collate Thai_CS_AS
where LastQTY > 0