use itsupport;
declare @fromDate datetime,@toDate datetime;
set @fromDate='2015/06/01'; 
set @toDate='2016/06/30';

select
	year(jobs.DateNow) '��', month(jobs.DateNow) '��͹', 
	isnull(catParent.descriptions,'') +' ' + cat.descriptions [��Ǣ��]
	,COUNT(*) '�ӹǹ��¡��'
	,sum(
		(convert(int,substring(jobs.take_care,1,2))*60)*2.5
		+
		convert(int,substring(jobs.take_care,4,2))*2.5
	) as [���ҷ����(�ҷ�)]
,sum((
		(convert(int,substring(jobs.take_care,1,2))*60)
		+
		convert(int,substring(jobs.take_care,4,2))
	))/COUNT(*) as [avg]
FROM RepairIt jobs 
	 left join it_supportCcatergoriesView cat on jobs.Category=cat.id
	 left join it_supportCatergories catParent on cat.parentId=catParent.id
where jobs.DateNow between @fromDate and @toDate
group by year(jobs.DateNow), month(jobs.DateNow),catParent.descriptions,cat.descriptions
order by year(jobs.DateNow) desc, month(jobs.DateNow) desc,catParent.descriptions,cat.descriptions;
goto endScript;

SELECT jobs.[Id] [�Ţ����ͺ]
	,staffs.Name [���;�ѡ�ҹ]
    ,' '+dbo.FN_ConvertDate(jobs.[DateNow],'Y/M/D') as  [�ѹ���Ѿ����]
    ,jobs.[DetailIt] [��������´]
    ,jobs.[DetailEdit] [�Ը����]
    ,catParent.[descriptions] [��Ǵ��ѡ]
    ,cat.[descriptions] [��Ǵ����]
    --,jobs.[PhardwareId]
    --,jobs.[PsoftwareId]
    ,jobs.[Cause] [���˵�]
    ,jobs.[YN] [��]
    ,jobs.[InformantB] [�������]
      --,jobs.[InformantItId] staffId 
      ,jobs.[BranchId] [�Ң�/�Ϳ���]
      --,jobs.take_care
      ,datediff(minute,'00:00:00',jobs.take_care) minuitUsed
      --,jobs.[ComM]
      --,jobs.[IP_COM]
      --,jobs.[TIMENOW]
      --,jobs.[IP_update]
      --,jobs.[time_update]
      --,jobs.[Category]
FROM RepairIt jobs 
	left join InformantIt staffs on jobs.InformantItId=staffs.I_Id
	left join it_supportCcatergoriesView cat on jobs.Category=cat.id
	left join it_supportCatergories catParent on cat.parentId=catParent.id
where jobs.DateNow between @fromDate and @toDate
order by jobs.DateNow desc,staffs.Name

select staffs.name [��ѡ�ҹ],'' as 'blank', COUNT(*) [�ӹǹ��¡��],sum(datediff(minute,'00:00:00',jobs.take_care)) as [���ҷ����(�ҷ�)]
,sum(datediff(minute,'00:00:00',jobs.take_care))/COUNT(*) as [avg]
FROM RepairIt jobs left join InformantIt staffs on jobs.InformantItId=staffs.I_Id
where jobs.DateNow between @fromDate and @toDate
group by staffs.Name;

drop table #tempSupportStats;
select 
	isnull(catParent.descriptions,cat.descriptions) [��Ǣ����ѡ]
	,cat.descriptions [��Ǣ���ͧ],COUNT(*) '�ӹǹ��¡��',sum(datediff(minute,'00:00:00',jobs.take_care)) as [���ҷ����(�ҷ�)]
,sum(datediff(minute,'00:00:00',jobs.take_care))/COUNT(*) as [avg]
into #tempSupportStats
FROM RepairIt jobs 
	 left join it_supportCcatergoriesView cat on jobs.Category=cat.id
	 left join it_supportCatergories catParent on cat.parentId=catParent.id
where jobs.DateNow between @fromDate and @toDate
group by catParent.descriptions,cat.descriptions
order by catParent.descriptions,cat.descriptions;

select [��Ǣ����ѡ],[��Ǣ���ͧ],[�ӹǹ��¡��],[���ҷ����(�ҷ�)],[avg] from #tempSupportStats
order by [��Ǣ����ѡ],[���ҷ����(�ҷ�)],[�ӹǹ��¡��]

select [��Ǣ����ѡ],sum([�ӹǹ��¡��])[�����¡��],sum([���ҷ����(�ҷ�)])[������ҷ����] from #tempSupportStats
group by [��Ǣ����ѡ]
order by [��Ǣ����ѡ],sum([���ҷ����(�ҷ�)]),sum([�ӹǹ��¡��])
endScript: