use DBSPS
IF OBJECT_ID('tempdb..#prepaidSaleInfo') IS NOT NULL  DROP TABLE #prepaidSaleInfo
IF OBJECT_ID('tempdb..#PartNo') IS NOT NULL  DROP TABLE #PartNo

--select * from Customer;
--select * from SPPartList where (PartNo like '%BOOK%' or PartNo like '%PREPAID%') and GroupId=17

select m.*,d.PartNo,d.Amount,d.SaleQTY 
into #prepaidSaleInfo
from SPSale m 
inner join SPSaleDetail d on m.BranchNo=d.BranchNo and m.SaleNo=d.SaleNo 
inner join SPPartMaster pm on pm.PartNo=d.PartNo and pm.BranchNo=m.BranchNo and d.BrandID=pm.BrandID
inner join SPPartList pl on pm.BrandID=pl.BrandID and pl.PartNo=pm.PartNo
where 1=1
and pl.GroupId='17'
and m.SaleDate between '2560/06/01' and '2560/06/30'
and m.SaleStatus not in ('Cancel')

select distinct PartNo into #PartNo from SPPartList where GroupId ='17'
--select * from #PartNo order by PartNo

select qty.BranchNo,br.BranchName
		,isnull(qty.[BOOK-TANGJAI-1],0) [qty_BOOK-TANGJAI-1],	isnull(amt.[BOOK-TANGJAI-1],0) [amt_BOOK-TANGJAI-1],	isnull(qty.[BOOK-TANGJAI-2],0) [qty_BOOK-TANGJAI-2],	isnull(amt.[BOOK-TANGJAI-2],0) [amt_BOOK-TANGJAI-2],	isnull(qty.[BOOK-TANGJAI-3],0) [qty_BOOK-TANGJAI-3],	isnull(amt.[BOOK-TANGJAI-3],0) [amt_BOOK-TANGJAI-3],	isnull(qty.[BOOK-TANGJAI-4],0) [qty_BOOK-TANGJAI-4],	isnull(amt.[BOOK-TANGJAI-4],0) [amt_BOOK-TANGJAI-4],	isnull(qty.[BOOK-TANGJAI-5],0) [qty_BOOK-TANGJAI-5],	isnull(amt.[BOOK-TANGJAI-5],0) [amt_BOOK-TANGJAI-5],	isnull(qty.[BOOK-TANGJAI-6],0) [qty_BOOK-TANGJAI-6],	isnull(amt.[BOOK-TANGJAI-6],0) [amt_BOOK-TANGJAI-6],	isnull(qty.[BOOK-TANGJAI-7],0) [qty_BOOK-TANGJAI-7],	isnull(amt.[BOOK-TANGJAI-7],0) [amt_BOOK-TANGJAI-7],	isnull(qty.[BOOK-TANGJAI-8],0) [qty_BOOK-TANGJAI-8],	isnull(amt.[BOOK-TANGJAI-8],0) [amt_BOOK-TANGJAI-8],	isnull(qty.[BOOK-TANGJAI-9],0) [qty_BOOK-TANGJAI-9],	isnull(amt.[BOOK-TANGJAI-9],0) [amt_BOOK-TANGJAI-9],	isnull(qty.[PREPAID-AT-L],0) [qty_PREPAID-AT-L],	isnull(amt.[PREPAID-AT-L],0) [amt_PREPAID-AT-L],	isnull(qty.[PREPAID-AT-M],0) [qty_PREPAID-AT-M],	isnull(amt.[PREPAID-AT-M],0) [amt_PREPAID-AT-M],	isnull(qty.[PREPAID-AT-S],0) [qty_PREPAID-AT-S],	isnull(amt.[PREPAID-AT-S],0) [amt_PREPAID-AT-S],	isnull(qty.[PREPAID-MT-L],0) [qty_PREPAID-MT-L],	isnull(amt.[PREPAID-MT-L],0) [amt_PREPAID-MT-L],	isnull(qty.[PREPAID-MT-M],0) [qty_PREPAID-MT-M],	isnull(amt.[PREPAID-MT-M],0) [amt_PREPAID-MT-M],	isnull(qty.[PREPAID-MT-S],0) [qty_PREPAID-MT-S],	isnull(amt.[PREPAID-MT-S],0) [amt_PREPAID-MT-S]
		,grandSumQty.qty grandSumQty
		,grandSumamt.amt grandSumAmt
 from (
	select BranchNo, p1.[BOOK-TANGJAI-1],	p1.[BOOK-TANGJAI-2],	[BOOK-TANGJAI-3],	[BOOK-TANGJAI-4],	[BOOK-TANGJAI-5],	[BOOK-TANGJAI-6],	[BOOK-TANGJAI-7],	[BOOK-TANGJAI-8],	[BOOK-TANGJAI-9],	[PREPAID-AT-L],	[PREPAID-AT-M],	[PREPAID-AT-S],	[PREPAID-MT-L],	[PREPAID-MT-M],	[PREPAID-MT-S]
	from (
		select branchNo,partNo,sum(SaleQTY) sumQTY	from #prepaidSaleInfo group by branchNo,partNo 
	) t pivot (
		sum(sumQTY) for PartNo in ([BOOK-TANGJAI-1],	[BOOK-TANGJAI-2],	[BOOK-TANGJAI-3],	[BOOK-TANGJAI-4],	[BOOK-TANGJAI-5],	[BOOK-TANGJAI-6],	[BOOK-TANGJAI-7],	[BOOK-TANGJAI-8],	[BOOK-TANGJAI-9],	[PREPAID-AT-L],	[PREPAID-AT-M],	[PREPAID-AT-S],	[PREPAID-MT-L],	[PREPAID-MT-M],	[PREPAID-MT-S])
	) p1
)qty left join (
	select BranchNo, p1.[BOOK-TANGJAI-1],	p1.[BOOK-TANGJAI-2],	[BOOK-TANGJAI-3],	[BOOK-TANGJAI-4],	[BOOK-TANGJAI-5],	[BOOK-TANGJAI-6],	[BOOK-TANGJAI-7],	[BOOK-TANGJAI-8],	[BOOK-TANGJAI-9],	[PREPAID-AT-L],	[PREPAID-AT-M],	[PREPAID-AT-S],	[PREPAID-MT-L],	[PREPAID-MT-M],	[PREPAID-MT-S]
	from (
		select branchNo,partNo,SUM(Amount) sumAmount	from #prepaidSaleInfo group by branchNo,partNo 
	) t pivot (
		sum(sumAmount) for PartNo in ([BOOK-TANGJAI-1],	[BOOK-TANGJAI-2],	[BOOK-TANGJAI-3],	[BOOK-TANGJAI-4],	[BOOK-TANGJAI-5],	[BOOK-TANGJAI-6],	[BOOK-TANGJAI-7],	[BOOK-TANGJAI-8],	[BOOK-TANGJAI-9],	[PREPAID-AT-L],	[PREPAID-AT-M],	[PREPAID-AT-S],	[PREPAID-MT-L],	[PREPAID-MT-M],	[PREPAID-MT-S])
	) p1
)amt on qty.BranchNo=amt.BranchNo
left join Branch br on qty.BranchNo=br.BranchNo
cross apply (select (isnull(qty.[BOOK-TANGJAI-1],0)+isnull(qty.[BOOK-TANGJAI-2],0)+isnull(qty.[BOOK-TANGJAI-3],0)+isnull(qty.[BOOK-TANGJAI-4],0)+isnull(qty.[BOOK-TANGJAI-5],0)+isnull(qty.[BOOK-TANGJAI-6],0)+isnull(qty.[BOOK-TANGJAI-7],0)+isnull(qty.[BOOK-TANGJAI-8],0)+isnull(qty.[BOOK-TANGJAI-9],0)+isnull(qty.[PREPAID-AT-L],0)+isnull(qty.[PREPAID-AT-M],0)+isnull(qty.[PREPAID-AT-S],0)+isnull(qty.[PREPAID-MT-L],0)+isnull(qty.[PREPAID-MT-M],0)+isnull(qty.[PREPAID-MT-S],0))qty)grandSumQty
cross apply (select (isnull(amt.[BOOK-TANGJAI-1],0)+isnull(amt.[BOOK-TANGJAI-2],0)+isnull(amt.[BOOK-TANGJAI-3],0)+isnull(amt.[BOOK-TANGJAI-4],0)+isnull(amt.[BOOK-TANGJAI-5],0)+isnull(amt.[BOOK-TANGJAI-6],0)+isnull(amt.[BOOK-TANGJAI-7],0)+isnull(amt.[BOOK-TANGJAI-8],0)+isnull(amt.[BOOK-TANGJAI-9],0)+isnull(amt.[PREPAID-AT-L],0)+isnull(amt.[PREPAID-AT-M],0)+isnull(amt.[PREPAID-AT-S],0)+isnull(amt.[PREPAID-MT-L],0)+isnull(amt.[PREPAID-MT-M],0)+isnull(amt.[PREPAID-MT-S],0))amt)grandSumamt