use ITREPORT;
go
alter view PremiumDetails
as 
select 
convert(varchar(4),(convert(int,substring(a.SaleDate,1,4))-543))+'/'+substring(a.SaleDate,6,2)+'/'+substring(a.SaleDate,9,2) SDATE
,b.PartNo,b.PartName,b.Price,b.SaleQTY
,case when p.UM='EA' then '˹���' else p.UM end unitCount
,p.GroupId,c.CustIDCardNo,c.CustFName,c.CustLName,a.SaleStatus
,a.SaleNo,a.BranchNo,brz.locat HPLocat
from dbfree.dbo.SPSale a 
inner join dbfree.dbo.SPSaleDetail b on a.SaleNo=b.SaleNo and a.BranchNo=b.BranchNo
inner join dbfree.dbo.SPPartList p on b.PartNo=p.PartNo and b.BrandID=p.BrandID
inner join dbfree.dbo.Customer c on a.CustID=c.CustID
inner join wb_branchZones brz on b.BranchNo=brz.DBFREE collate Thai_CS_AS
where 1=1
--and a.SaleNo in ('60RTSL/0000010','60RTSL/0000011','60RTSL/0000012','60RTSL/0000019','60WHSL/0000007','60WHSL/0000004','60RTSL/0000026','60WHSL/0000004','60RTSL/0000025','60RTSL/0000026','60RTSL/0000017','60WHSL/0000009','60WHSL/0000012','60RTSL/0000020')
--and a.SaleStatus not in ('Paid','Approved')
and not (c.CustIDCardNo='' or c.CustIDCardNo is null) and a.SaleStatus not in ('Cancel')
and p.GroupId not in ('05','12')
--order by a.SaleDate desc;
--select * from serviceweb.dbo.wb_armast;