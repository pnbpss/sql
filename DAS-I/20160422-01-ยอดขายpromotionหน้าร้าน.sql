/*select * from dbsps.dbo.SPPartList where PartNo like 'BOOK-TANG%'
select * from dbsps.dbo.SPPartList where PartNo like 'P01%'
select distinct PartNo from dbsps.dbo.SPSaleDetail where PartNo like 'BOOK-TANG%'
select * from dbsps.dbo.SPSaleDetail where PartNo like 'P01%'

SELECT TOP 1000 'sum(isnull(['+[partNo]+'],0)) ['+partNo+'], ','['+[partNo]+'],'
,[reportId]
FROM [ITREPORT].[dbo].[wb_partNoInReports]
where reportId='001'

*/
use ITREPORT;
--query �������͡������������ temporary table
select branchNo,
isnull([P01],0) P01 ,
isnull([P02],0) P02 ,
isnull([P03],0) P03 ,
isnull([P04],0) P04 ,
isnull([P05],0) P05 ,
isnull([P06],0) P06 ,
isnull([P07],0) P07 ,
isnull([P08],0) P08 ,
isnull([P09],0) P09 ,
isnull([P10],0) P10 ,
isnull([P11],0) P11 ,
isnull([P12],0) P12 ,
isnull([P13],0) P13 ,
isnull([P14],0) P14 ,
isnull([P15],0) P15 
into #tempTable
from (
	select 
	SaleQTY
	,sd.PartNo
	,s.BranchNo
	from dbsps.dbo.SPSale s 
	inner join dbsps.dbo.SPSaleDetail sd on s.CompCode=sd.CompCode and s.SaleNo=sd.SaleNo and s.BranchNo=sd.BranchNo	
	where SaleStatus='Paid' and sd.PartNo in (select partNo from wb_partNoInReports where reportId='001')
	and s.CompCode='001'
	and s.SaleDate between '2559/01/01' and '2559/04/30'
	
) as sdt
pivot
(
	sum(saleQTY) for PartNo in 
	(
			[P01],[P02],[P03],[P04],[P05],[P06],[P07],[P08],[P09],[P10],[P11],[P12],[P13],[P14],[P15]
	)
) as p


--��Ҽ��Ѿ��� temporary table �͡���ʴ�
select '('+br.BranchNo+') '+br.BranchName brInfo,pv.ProvinceName 
,isnull([P01],0) P01 , isnull([P02],0) P02 , isnull([P03],0) P03 ,isnull([P04],0) P04 ,
isnull([P05],0) P05 ,isnull([P06],0) P06 ,isnull([P07],0) P07 ,isnull([P08],0) P08 ,isnull([P09],0) P09 ,
isnull([P10],0) P10 ,isnull([P11],0) P11 ,isnull([P12],0) P12 ,isnull([P13],0) P13 ,isnull([P14],0) P14 ,
isnull([P15],0) P15 
,
(
isnull([P01],0)+ isnull([P02],0) + isnull([P03],0) + isnull([P04],0) + isnull([P05],0)+
isnull([P06],0)+ isnull([P07],0) + isnull([P08],0) + isnull([P09],0) + isnull([P10],0)+
isnull([P11],0)+ isnull([P12],0) + isnull([P13],0) + isnull([P14],0) + isnull([P15],0)
) [sumBranch]

from (DBSPS.dbo.Branch br inner join wb_branchZones bz on bz.DBSPS=br.BranchNo)
left join #tempTable sdt on sdt.BranchNo=br.BranchNo --and br.CompCode='100'
left join DBSPS.dbo.Province pv on br.ProvinceId=pv.ProvinceId
where br.BranchNo not in ('001','086','090','088','121','123','133','044','119','132','031','035','081','091','094','112','096','131','070','077','097','098','102','111','127','136')
	  and bz.closed='N'
union --union ������� ����ѧ��Ѵ�͡���ʴ�
select '���' brInfo,pv.ProvinceName ,
sum(isnull([P01],0)) [P01],sum(isnull([P02],0)) [P02], sum(isnull([P03],0)) [P03], sum(isnull([P04],0)) [P04], sum(isnull([P05],0)) [P05], 
sum(isnull([P06],0)) [P06], sum(isnull([P07],0)) [P07], sum(isnull([P08],0)) [P08], sum(isnull([P09],0)) [P09], sum(isnull([P10],0)) [P10], 
sum(isnull([P11],0)) [P11], sum(isnull([P12],0)) [P12], sum(isnull([P13],0)) [P13], sum(isnull([P14],0)) [P14], sum(isnull([P15],0)) [P15], 

sum(
isnull([P01],0) + isnull([P02],0)+  isnull([P03],0)+ isnull([P04],0)+ 
isnull([P05],0)+ isnull([P06],0)+ isnull([P07],0)+ isnull([P08],0)+ isnull([P09],0)+ 
isnull([P10],0)+ isnull([P11],0)+ isnull([P12],0)+ isnull([P13],0)+ isnull([P14],0)+ 
isnull([P15],0)
) [sumBranch]

from (DBSPS.dbo.Branch br inner join wb_branchZones bz on bz.DBSPS=br.BranchNo)
left join #tempTable sdt on sdt.BranchNo=br.BranchNo --and br.CompCode='100'
left join DBSPS.dbo.Province pv on br.ProvinceId=pv.ProvinceId
where br.BranchNo not in ('001','086','090','088','121','123','133','044','119','132','031','035','081','091','094','112','096','131','070','077','097','098','102','111','127','136')
	  and bz.closed='N'
group by pv.ProvinceName

order by ProvinceName,brInfo  --order ����ѧ��Ѵ����Ң�

drop table #tempTable;