/*select * from dbsps.dbo.SPPartList where PartNo like 'BOOK-TANG%'
select * from dbsps.dbo.SPPartList where PartNo like 'P01%'
select distinct PartNo from dbsps.dbo.SPSaleDetail where PartNo like 'BOOK-TANG%'
select * from dbsps.dbo.SPSaleDetail where PartNo like 'BOOK-TANGJAI-1%' 

SELECT TOP 1000 'sum(isnull(['+[partNo]+'],0)) ['+partNo+'], ','['+[partNo]+'],'
,[reportId]
FROM [ITREPORT].[dbo].[wb_partNoInReports]
where reportId='002'

*/
use ITREPORT;
--pivot �ʹ���
select branchNo,
isnull([BOOK-TANGJAI-6],0) [BOOK-TANGJAI-6] ,
isnull([BOOK-TANGJAI-1],0) [BOOK-TANGJAI-1] ,
isnull([BOOK-TANGJAI-2],0) [BOOK-TANGJAI-2] ,
isnull([BOOK-TANGJAI-8],0) [BOOK-TANGJAI-8] ,
isnull([BOOK-TANGJAI-5],0) [BOOK-TANGJAI-5] ,
isnull([BOOK-TANGJAI-9],0) [BOOK-TANGJAI-9] ,
isnull([BOOK-TANGJAI-4],0) [BOOK-TANGJAI-4] ,
isnull([BOOK-TANGJAI-3],0) [BOOK-TANGJAI-3] 
into #tempTableQTY
from (
	select 	
	SaleQTY
	,sd.PartNo
	,s.BranchNo
	from dbsps.dbo.SPSale s 
	inner join dbsps.dbo.SPSaleDetail sd on s.CompCode=sd.CompCode and s.SaleNo=sd.SaleNo and s.BranchNo=sd.BranchNo	
	where SaleStatus='Paid' and sd.PartNo in (select partNo from wb_partNoInReports where reportId='002')
	and s.CompCode='001'
	and s.SaleDate between '2559/01/01' and '2559/04/30'
	
) as sdt
pivot
(
	sum(saleQTY) for PartNo in 
	(
	[BOOK-TANGJAI-6],
	[BOOK-TANGJAI-1],
	[BOOK-TANGJAI-2],
	[BOOK-TANGJAI-8],
	[BOOK-TANGJAI-5],
	[BOOK-TANGJAI-9],
	[BOOK-TANGJAI-4],
	[BOOK-TANGJAI-3]
	)
) as p

------pivot �ҷ
select branchNo,
isnull([BOOK-TANGJAI-6],0) [BOOK-TANGJAI-6] ,
isnull([BOOK-TANGJAI-1],0) [BOOK-TANGJAI-1] ,
isnull([BOOK-TANGJAI-2],0) [BOOK-TANGJAI-2] ,
isnull([BOOK-TANGJAI-8],0) [BOOK-TANGJAI-8] ,
isnull([BOOK-TANGJAI-5],0) [BOOK-TANGJAI-5] ,
isnull([BOOK-TANGJAI-9],0) [BOOK-TANGJAI-9] ,
isnull([BOOK-TANGJAI-4],0) [BOOK-TANGJAI-4] ,
isnull([BOOK-TANGJAI-3],0) [BOOK-TANGJAI-3] 
into #tempTableBaht
from (
	select 	
	Amount
	,sd.PartNo
	,s.BranchNo
	from dbsps.dbo.SPSale s 
	inner join dbsps.dbo.SPSaleDetail sd on s.CompCode=sd.CompCode and s.SaleNo=sd.SaleNo and s.BranchNo=sd.BranchNo	
	where SaleStatus='Paid' and sd.PartNo in (select partNo from wb_partNoInReports where reportId='002')
	and s.CompCode='001'
	and s.SaleDate between '2559/01/01' and '2559/04/30'
	
) as sdt
pivot
(
	sum(Amount) for PartNo in 
	(
	[BOOK-TANGJAI-6],
	[BOOK-TANGJAI-1],
	[BOOK-TANGJAI-2],
	[BOOK-TANGJAI-8],
	[BOOK-TANGJAI-5],
	[BOOK-TANGJAI-9],
	[BOOK-TANGJAI-4],
	[BOOK-TANGJAI-3]
	)
) as p

--��Ҽ��Ѿ���͡���ʴ�
select '('+br.BranchNo+') '+br.BranchName brInfo,pv.ProvinceName ,
	isnull(sdt.[BOOK-TANGJAI-1],0) [BOOK-TANGJAI-1_qty] ,isnull(Baht.[BOOK-TANGJAI-1],0) [BOOK-TANGJAI-1_baht] ,
	isnull(sdt.[BOOK-TANGJAI-2],0) [BOOK-TANGJAI-2_qty] ,isnull(Baht.[BOOK-TANGJAI-2],0) [BOOK-TANGJAI-2_baht] ,
	isnull(sdt.[BOOK-TANGJAI-3],0) [BOOK-TANGJAI-3_qty] ,isnull(Baht.[BOOK-TANGJAI-3],0) [BOOK-TANGJAI-3_baht] ,
	isnull(sdt.[BOOK-TANGJAI-4],0) [BOOK-TANGJAI-4_qty] ,isnull(Baht.[BOOK-TANGJAI-4],0) [BOOK-TANGJAI-4_baht] ,
	isnull(sdt.[BOOK-TANGJAI-5],0) [BOOK-TANGJAI-5_qty] ,isnull(Baht.[BOOK-TANGJAI-5],0) [BOOK-TANGJAI-5_baht] ,
	isnull(sdt.[BOOK-TANGJAI-6],0) [BOOK-TANGJAI-6_qty] ,isnull(Baht.[BOOK-TANGJAI-6],0) [BOOK-TANGJAI-6_baht] ,
	isnull(sdt.[BOOK-TANGJAI-8],0) [BOOK-TANGJAI-8_qty] ,isnull(Baht.[BOOK-TANGJAI-8],0) [BOOK-TANGJAI-8_baht] ,
	isnull(sdt.[BOOK-TANGJAI-9],0) [BOOK-TANGJAI-9_qty] ,isnull(Baht.[BOOK-TANGJAI-9],0) [BOOK-TANGJAI-9_baht] ,
	(isnull(sdt.[BOOK-TANGJAI-6],0) +
	isnull(sdt.[BOOK-TANGJAI-1],0)  +
	isnull(sdt.[BOOK-TANGJAI-2],0)  +
	isnull(sdt.[BOOK-TANGJAI-8],0)  +
	isnull(sdt.[BOOK-TANGJAI-5],0)  +
	isnull(sdt.[BOOK-TANGJAI-9],0)  +
	isnull(sdt.[BOOK-TANGJAI-4],0)  +
	isnull(sdt.[BOOK-TANGJAI-3],0) ) sumQTY,
	(
	isnull(baht.[BOOK-TANGJAI-6],0) +
	isnull(baht.[BOOK-TANGJAI-1],0) +
	isnull(baht.[BOOK-TANGJAI-2],0) +
	isnull(baht.[BOOK-TANGJAI-8],0) +
	isnull(baht.[BOOK-TANGJAI-5],0) +
	isnull(baht.[BOOK-TANGJAI-9],0) +
	isnull(baht.[BOOK-TANGJAI-4],0) +
	isnull(baht.[BOOK-TANGJAI-3],0) 
	) sumBaht
	from (DBSPS.dbo.Branch br inner join wb_branchZones bz on bz.DBSPS=br.BranchNo)
	left join #tempTableQTY sdt on sdt.BranchNo=br.BranchNo
	left join #tempTableBaht Baht on Baht.BranchNo=br.BranchNo
	left join DBSPS.dbo.Province pv on br.ProvinceId=pv.ProvinceId
	where br.BranchNo not in ('001','086','090','088','121','123','133','044','119','132','031','035','081','091','094','112','096','131','070','077','097','098','102','111','127','136')
		  and bz.closed='N'
		  
union --union �Ѻ��������Шѧ��Ѵ
	
	select '���' brInfo,pv.ProvinceName ,
	sum(isnull(sdt.[BOOK-TANGJAI-6],0)) [BOOK-TANGJAI-6_qty], sum(isnull(baht.[BOOK-TANGJAI-6],0)) [BOOK-TANGJAI-6_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-1],0)) [BOOK-TANGJAI-1_qty], sum(isnull(baht.[BOOK-TANGJAI-1],0)) [BOOK-TANGJAI-1_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-2],0)) [BOOK-TANGJAI-2_qty], sum(isnull(baht.[BOOK-TANGJAI-2],0)) [BOOK-TANGJAI-2_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-8],0)) [BOOK-TANGJAI-8_qty], sum(isnull(baht.[BOOK-TANGJAI-8],0)) [BOOK-TANGJAI-8_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-5],0)) [BOOK-TANGJAI-5_qty], sum(isnull(baht.[BOOK-TANGJAI-5],0)) [BOOK-TANGJAI-5_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-9],0)) [BOOK-TANGJAI-9_qty], sum(isnull(baht.[BOOK-TANGJAI-9],0)) [BOOK-TANGJAI-9_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-4],0)) [BOOK-TANGJAI-4_qty], sum(isnull(baht.[BOOK-TANGJAI-4],0)) [BOOK-TANGJAI-4_baht], 
	sum(isnull(sdt.[BOOK-TANGJAI-3],0)) [BOOK-TANGJAI-3_qty], sum(isnull(baht.[BOOK-TANGJAI-3],0)) [BOOK-TANGJAI-3_baht],

	sum(isnull(sdt.[BOOK-TANGJAI-6],0) + isnull(sdt.[BOOK-TANGJAI-1],0) +isnull(sdt.[BOOK-TANGJAI-2],0) + isnull(sdt.[BOOK-TANGJAI-8],0)  +
	isnull(sdt.[BOOK-TANGJAI-5],0) +isnull(sdt.[BOOK-TANGJAI-9],0) + isnull(sdt.[BOOK-TANGJAI-4],0) +isnull(sdt.[BOOK-TANGJAI-3],0) ) 
	sumQTY,
	sum(isnull(baht.[BOOK-TANGJAI-6],0) +isnull(baht.[BOOK-TANGJAI-1],0) +isnull(baht.[BOOK-TANGJAI-2],0) +isnull(baht.[BOOK-TANGJAI-8],0) +
	isnull(baht.[BOOK-TANGJAI-5],0) +isnull(baht.[BOOK-TANGJAI-9],0) +isnull(baht.[BOOK-TANGJAI-4],0) +isnull(baht.[BOOK-TANGJAI-3],0))
	sumBaht
	from (DBSPS.dbo.Branch br inner join wb_branchZones bz on bz.DBSPS=br.BranchNo)
	left join #tempTableQTY sdt on sdt.BranchNo=br.BranchNo
	left join #tempTableBaht Baht on Baht.BranchNo=br.BranchNo
	left join DBSPS.dbo.Province pv on br.ProvinceId=pv.ProvinceId
	where br.BranchNo not in ('001','086','090','088','121','123','133','044','119','132','031','035','081','091','094','112','096','131','070','077','097','098','102','111','127','136')
		  and bz.closed='N'
	group by pv.ProvinceName
order by ProvinceName,brInfo --order ����ѧ��Ѵ����Ң�

--drop table ��� ���ͨ������ҧ����
drop table #tempTableQTY;
drop table #tempTableBaht