/****** Script for SelectTopNRows command from SSMS  ******/
IF OBJECT_ID('tempdb.dbo.#PremiumDetails', 'U') IS NOT NULL DROP TABLE #PremiumDetails;
IF OBJECT_ID('tempdb.dbo.#tempPremium', 'U') IS NOT NULL DROP TABLE #tempPremium;
declare @sdate varchar(max);
set @sdate='2017/01/16'
select * into #PremiumDetails from PremiumDetails where SDATE=@sdate;
create nonclustered index pmidxxx001 on #PremiumDetails (SDATE,HPLocat,CustIdCardNo)

SELECT 
	  m.[TYPE],m.IDNO,m.customerFullName
	  ,m.SDATE mSdate
	  ,p.SDATE pSdate
      ,p.[PartNo]
      ,p.[PartName]
      ,p.[Price]
      ,p.[SaleQTY]
      ,p.[unitCount]
      ,m.CONTNO
      ,p.SaleNo
      ,m.LOCAT
      ,p.BranchNo
      ,m.TELP
      --,p.[GroupId]
      --,p.[CustIDCardNo]
      --,p.[CustFName]
      --,p.[CustLName]
      --,p.[SaleStatus]
  into #tempPremium
  FROM motorCycleSalesInfos m left join #PremiumDetails p  on m.SDATE=p.SDATE and m.IDNO=p.CustIDCardNo collate Thai_CS_AS and m.LOCAT=p.HPLocat collate Thai_CS_AS
  where 1=1
  and m.SDATE = @sdate
order by m.SDATE,m.IDNO desc
--  drop table #tempPremium
--
select COUNT(*)cnt,mSDATE,IDNO,LOCAT 
from #tempPremium 
group by mSDATE,IDNO,LOCAT order by COUNT(*) desc;

select * from #tempPremium where IDNO='1920600108481'

--select * from #tempPremium where IDNO='3800800756913'
--select * from PremiumDetails where SaleNo='59RTSL/0000406' 