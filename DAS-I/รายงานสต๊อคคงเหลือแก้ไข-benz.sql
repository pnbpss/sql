IF OBJECT_ID('tempdb..#temp1') IS NOT NULL DROP TABLE #temp1
IF OBJECT_ID('tempdb..#maxTs') IS NOT NULL DROP TABLE #maxTs

create table #maxTs (maxTS varchar(250),	PartNo varchar(250),	BranchNo varchar(3),	BrandID varchar(3),	CompCode varchar(3))
insert into #maxTs select max(spstc.[TimeStamp])maxTS,spstc.PartNo,spstc.BranchNo,spstc.BrandID,spstc.CompCode 
							from DBSPS.dbo.SPStockCard spstc 
							where spstc.LastQTY>0
							group by spstc.PartNo,spstc.BranchNo,spstc.BrandID,spstc.CompCode
create index abcdefgMaxTs on #maxTs (PartNo,BranchNo,BrandID,CompCode)

select main.sakha
    ,main.LastIn
    ,main.PartNo
    ,main.PartName
    ,main.ProductName
    ,main.GroupName
    ,main.Cost
    ,main.LastQTY
    ,main.penmoney
    ,convert(nvarchar,main.datenow)+' �ѹ'  as datenow
into #temp1     
from (
  select '('+sc.BranchNo+') '+b.BranchName as sakha
      ,sc.PartNo
      ,sl.PartName
      ,p.ProductName
      ,sg.GroupName
      ,sc.Cost
      ,sc.LastQTY
      ,(sc.Cost*sc.LastQTY)as penmoney
      ,sc.BranchNo
      ,sm.LastIn
	  --,DATEDIFF(DAY,convert(nvarchar(4),left(LastIn,4)-543)+RIGHT(LastIn,6),getdate())as datenow
      ,datediff(day,convert(varchar(4),convert(smallint,left(LastIn,4))-543)+RIGHT(LastIn,6),GETDATE()) datenow
      --,ROW_NUMBER() over(partition by sc.PartNo,sc.BranchNo order by sc.PartNo,TimeStamp desc)as Nrow 
  from DBSPS.dbo.SPStockCard as sc
  inner join #maxTs mxTimeStamp on sc.PartNo=mxTimeStamp.PartNo and sc.BranchNo=mxTimeStamp.BranchNo and sc.BrandID=mxTimeStamp.BrandID and sc.CompCode=mxTimeStamp.CompCode and sc.[TimeStamp]=mxTimeStamp.[maxTS]
  inner join DBSPS.dbo.SPPartList as sl on sc.BrandID = sl.BrandID and sc.PartNo = sl.PartNo
  inner join DBSPS.dbo.SPPartGroup as sg on sl.GroupId = sg.GroupID
  inner join DBSPS.dbo.Branch as b on sc.BranchNo = b.BranchNo
  inner join DBSPS.dbo.SPPartMaster as sm on sc.BranchNo = sm.BranchNo and sc.PartNo = sm.PartNo --and sm.LastIn like '%/%'
  inner join DBSPS.dbo.Product as p on sl.ProductID = p.ProductID
  where sm.LastIn between '2500/01/03' and '2560/10/04' -- ".$branchn." ".$praphats."
  --and sc.LastQTY>0
)as main 
where 1=1
--and main.LastQTY>0

--and main.Nrow = 1
--order by main.sakha,main.LastIn asc 
--###insert ��� temp ����ͧ order by (����ջ���ª�� �������)

select sakha,LastIn,PartNo,PartName,ProductName,GroupName,Cost,LastQTY,penmoney,datenow
from #temp1
union all
select '���������','','','','','',convert(int,''),sum(LastQTY)as LastQTY,sum(penmoney)as penmoney,''
from #temp1

order by sakha