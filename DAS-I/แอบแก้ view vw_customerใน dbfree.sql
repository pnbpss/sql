--alter view VW_Customer as
/*
--�ͧ���
SELECT  CompCode AS COM, CustID AS CUS, ISNULL
                          ((SELECT     CustPrefixName
                              FROM         dbo.CustPrefix
                              WHERE     (CustPrefixId = dbo.Customer.CustPFCode)), '') AS PRF, CustFName + ' ' + CustLName AS NAM, 
                      (CASE WHEN MailTo = '2' THEN IDCardAddressNo + '||' + IDCardVillageNo + '||' + IDCardVillageName + '||' + IDCardSoi + '||' + IDCardRoad + '||' + IDCardTambon
                       + '||' + ISNULL
                          ((SELECT     AmphurName
                              FROM         Amphur
                              WHERE     ProvinceID = IDCardProvinceCode AND AmphurId = IDCardAmphurCode), '') + '||' + ISNULL
                          ((SELECT     ProvinceName
                              FROM         Province
                              WHERE     ProvinceId = IDCardProvinceCode), '') 
                      + '||' + IDCardZIPCode + '||' + IDCardTelNo + '||' + IDCardProvinceCode WHEN MailTo = '3' THEN StayAddressNo + '||' + StayVillageNo + '||' + StayVillageName
                       + '||' + StaySoi + '||' + StayRoad + '||' + StayTambon + '||' + ISNULL
                          ((SELECT     AmphurName
                              FROM         Amphur
                              WHERE     ProvinceID = StayProvinceCode AND AmphurId = StayAmphurCode), '') + '||' + ISNULL
                          ((SELECT     ProvinceName
                              FROM         Province
                              WHERE     ProvinceId = StayProvinceCode), '') 
                      + '||' + StayZIPCode + '||' + StayTelNo + '||' + StayProvinceCode WHEN MailTo = '4' THEN OfficeAddressNo + '||' + OfficeVillageNo + '||' + OfficeVillageName
                       + '||' + OfficeSoi + '||' + OfficeRoad + '||' + OfficeTambon + '||' + ISNULL
                          ((SELECT     AmphurName
                              FROM         Amphur
                              WHERE     ProvinceID = OfficeProvinceCode AND AmphurId = OfficeAmphurCode), '') + '||' + ISNULL
                          ((SELECT     ProvinceName
                              FROM         Province
                              WHERE     ProvinceId = OfficeProvinceCode), '') 
                      + '||' + OfficeZIPCode + '||' + OfficeTelNo + '||' + OfficeProvinceCode ELSE HomeAddressNo + '||' + HomeVillageNo + '||' + HomeVillageName + '||' + HomeSoi
                       + '||' + HomeRoad + '||' + HomeTambon + '||' + ISNULL
                          ((SELECT     AmphurName
                              FROM         Amphur
                              WHERE     ProvinceID = HomeProvinceCode AND AmphurId = HomeAmphurCode), '') + '||' + ISNULL
                          ((SELECT     ProvinceName
                              FROM         Province
                              WHERE     ProvinceId = HomeProvinceCode), '') + '||' + HomeZIPCode + '||' + HomeTelNo + '||' + HomeProvinceCode END) AS ADR
FROM         dbo.Customer

alter view VW_CustAddress
SELECT     CompCode, CustID, CustBirthDate AS BirthDate, 
                      CASE WHEN MailTo = '2' THEN IDCardProvinceCode WHEN MailTo = '3' THEN StayProvinceCode WHEN MailTo = '4' THEN OfficeProvinceCode ELSE HomeProvinceCode
                       END AS ProvinceCode, 
                      CASE WHEN MailTo = '2' THEN IDCardAmphurCode WHEN MailTo = '3' THEN StayAmphurCode WHEN MailTo = '4' THEN OfficeAmphurCode ELSE HomeAmphurCode
                       END AS AmphurCode, 
                      CASE WHEN MailTo = '2' THEN IDCardTambon WHEN MailTo = '3' THEN StayTambon WHEN MailTo = '4' THEN OfficeTambon ELSE HomeTambon END
                       AS Tambon, 
                      CASE WHEN MailTo = '2' THEN IDCardVillageNo WHEN MailTo = '3' THEN StayVillageNo WHEN MailTo = '4' THEN OfficeVillageNo ELSE HomeVillageNo
                       END AS VillageNo, 
                      CASE WHEN MailTo = '2' THEN IDCardVillageName WHEN MailTo = '3' THEN StayVillageName WHEN MailTo = '4' THEN OfficeVillageName ELSE HomeVillageName
                       END AS VillageName, 
                      CASE WHEN MailTo = '2' THEN IDCardAddressNo WHEN MailTo = '3' THEN StayAddressNo WHEN MailTo = '4' THEN OfficeAddressNo ELSE HomeAddressNo
                       END AS AddressNo, 
                      CASE WHEN MailTo = '2' THEN IDCardSoi WHEN MailTo = '3' THEN StaySoi WHEN MailTo = '4' THEN OfficeSoi ELSE HomeSoi END AS Soi, 
                      CASE WHEN MailTo = '2' THEN IDCardRoad WHEN MailTo = '3' THEN StayRoad WHEN MailTo = '4' THEN OfficeRoad ELSE HomeRoad END AS Road, 
                      CASE WHEN MailTo = '2' THEN IDCardTelNo WHEN MailTo = '3' THEN StayTelNo WHEN MailTo = '4' THEN OfficeTelNo ELSE HomeTelNo END AS TelNo,
                       CASE WHEN MailTo = '2' THEN IDCardFaxNo WHEN MailTo = '3' THEN StayFaxNo WHEN MailTo = '4' THEN OfficeFaxNo ELSE HomeFaxNo END AS FaxNo,
                       CASE WHEN MailTo = '2' THEN IDCardZIPCode WHEN MailTo = '3' THEN StayZIPCode WHEN MailTo = '4' THEN OfficeZIPCode ELSE HomeZIPCode END
                       AS ZIPCode, ISNULL
                          ((SELECT     ReserveAmt
                              FROM         dbo.CustomerExtend AS X
                              WHERE     (CompCode = A.CompCode) AND (CustID = A.CustID)), 0) AS Active
FROM         dbo.Customer AS A

*/

--����
alter view VW_Customer as
SELECT  CompCode AS COM, CustID AS CUS
						--, ISNULL ((SELECT     CustPrefixName  FROM         dbo.CustPrefix  WHERE     (CustPrefixId = CustPFCode)), '') AS PRF
						,isnull(b.CustPrefixName,'') PRF
                       ,a.CustFName + ' ' + a.CustLName AS NAM                     
                        ,
                        case 
							when a.MailTo='2' then IDCardAddressNo + '||' + IDCardVillageNo + '||' + IDCardVillageName + '||' + IDCardSoi + '||' + IDCardRoad + '||' + IDCardTambon + '||' +isnull(cardAmp.AmphurName,'')+'||'+isnull(cardProv.ProvinceName,'')+'||'+ IDCardZIPCode + '||' + IDCardTelNo + '||' + IDCardProvinceCode 
							when a.MailTo='3' then StayAddressNo + '||' + StayVillageNo + '||' + StayVillageName + '||' + StaySoi + '||' + StayRoad + '||' + StayTambon + '||'+isnull(resAmp.AmphurName,'')+'||'+isnull(resProv.ProvinceName,'') + '||' + StayZIPCode + '||' + StayTelNo + '||' + StayProvinceCode 
							when a.MailTo='4' then OfficeAddressNo + '||' + OfficeVillageNo + '||' + OfficeVillageName+ '||' + OfficeSoi + '||' + OfficeRoad + '||' + OfficeTambon + '||'+isnull(offAmp.AmphurName,'')+'||'+isnull(offProv.ProvinceName,'') + '||' + OfficeZIPCode + '||' + OfficeTelNo + '||' + OfficeProvinceCode 
							else HomeAddressNo + '||' + HomeVillageNo + '||' + HomeVillageName + '||' + HomeSoi+ '||' + HomeRoad + '||' + HomeTambon+ '||'+isnull(homeAmp.AmphurName,'')+'||'+isnull(homeProv.ProvinceName,'')+'||'+ HomeZIPCode + '||' + HomeTelNo + '||' + HomeProvinceCode 
                        end ADR
                        
FROM  dbo.Customer a 			
		   left join dbo.CustPrefix b on a.CustPFCode=b.CustPrefixId		
		   
		   left join dbo.Province cardProv on a.IDCardProvinceCode=cardProv.ProvinceId		   
		   left join dbo.Amphur cardAmp on a.IDCardAmphurCode=cardAmp.AmphurId and cardProv.ProvinceId=cardAmp.ProvinceId
		   
		   left join dbo.Province resProv on a.StayProvinceCode=resProv.ProvinceId
		   left join dbo.Amphur resAmp on a.StayAmphurCode=resAmp.AmphurId and resProv.ProvinceId=resAmp.ProvinceId
		   
		   left join dbo.Province offProv on a.OfficeProvinceCode=offProv.ProvinceId
		   left join dbo.Amphur offAmp on a.OfficeAmphurCode=offAmp.AmphurId and offProv.ProvinceId=offAmp.ProvinceId		   
		   
		   left join dbo.Province homeProv on a.HomeProvinceCode=homeProv.ProvinceId
		   left join dbo.Amphur homeAmp on a.HomeAmphurCode=homeAmp.AmphurId and homeProv.ProvinceId=homeAmp.ProvinceId
