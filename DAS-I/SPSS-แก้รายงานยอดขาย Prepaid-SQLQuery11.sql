select stc.*,spl.PartName,sps.SaleDate,sps.SaleNo,cust.CustGrpCode,br.BranchName,prv.ProvinceName
,stc.OutQTY*stc.Price amountSalePrice
,SUBSTRING(stc.PartNo,9,2) atmt, SUBSTRING(stc.PartNo,12,1) size
from SPStockCard  stc 
left join SPPartList spl on stc.PartNo=spl.PartNo and stc.BrandID=spl.BrandID 
left join SPSale sps on stc.RefNo=sps.SaleNo and stc.BranchNo=sps.BranchNo and stc.CompCode=sps.CompCode
left join Customer cust on sps.CompCode=cust.CompCode and sps.CustID=cust.CustID
left join Branch br on stc.BranchNo=br.BranchNo and stc.CompCode=br.CompCode
left join Province prv on br.ProvinceId=prv.ProvinceId
where 1=1
and stc.Remark ='Sale'
and stc.TrnsDate between '2560/09/01' and '2560/10/26'
--and spl.GroupId='17'
and (stc.PartNo like 'PREPAID-AT%' or stc.PartNo like 'PREPAID-MT%')