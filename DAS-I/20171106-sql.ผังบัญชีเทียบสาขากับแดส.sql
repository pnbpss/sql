/****** Script for SelectTopNRows command from SSMS  ******/
SELECT a.[locat],a.LOCATNM
,b.id_account 
,dbfreeBranch.BranchNo dbFreeBranchNo
,dbspsBranch.BranchNo dbSpsBranchNo 
, [AUMPDES]+'/'+[PROVDES] zoneInfo
,right('000'+b.DBFREE,3) dbfree
,dbfreeBranch.BranchName dbfreeBranchName
,right('000'+b.DBSPS,3) dbsps
,dbspsBranch.BranchName dbspsBranchName
FROM [ITREPORT].[dbo].wb_invlocat a left join wb_idaccount b on a.locat=b.locat collate Thai_CI_AS
left join DBFREE.dbo.Branch dbfreeBranch on  right('000'+b.DBFREE,3)=dbfreeBranch.BranchNo and dbfreeBranch.CompCode='001'
left join DBSPS.dbo.Branch dbspsBranch on  right('000'+b.DBSPS,3)=dbspsBranch.BranchNo and dbspsBranch.CompCode='001' 


