/****** Script for SelectTopNRows command from SSMS  ******/
use ITREPORT;
declare @sdate varchar(max); set @sdate=convert(varchar,(YEAR(DATEADD(day,0,getdate()))+543))+'/'+substring(CONVERT(varchar,DATEADD(day,0,getdate()),111),6,5);
IF OBJECT_ID('tempdb.dbo.#customer', 'U') IS NOT NULL DROP TABLE #customer; select c.firstName,c.lastName,TELP,IDNO into #customer from hp_customers c; create index customerIndex001 on #customer (IDNO);
delete from rpt_SMS4VerifyWithCustomers where dateOfPremiums=@sdate
insert into rpt_SMS4VerifyWithCustomers
select dstn.CustID,c.CustFName,c.CustLName
,prc.TELP TELP
,CASE 
	when ISNULL(csn.TELP,'NULL')='NULL' then 'ไม่เจอในซีเนียร์(รหัสบัตรปชช.)'
	WHEN LEFT(prc.TELP, 1) <> '0' THEN 'เบอร์ผิดพลาด' 
	WHEN LEFT(prc.TELP, 2) = '07' THEN 'เบอร์บ้าน' 
	WHEN LEFT(prc.TELP, 2) = '02' THEN 'เบอร์บ้าน' 
	WHEN LEFT(prc.TELP, 2) = '03' THEN 'เบอร์บ้าน' 
	WHEN LEFT(prc.TELP, 2) = '04' THEN 'เบอร์บ้าน' 
	WHEN LEFT(prc.TELP, 2) = '05' THEN 'เบอร์บ้าน' 	
	WHEN len(prc.TELP) != 10 THEN 'เบอร์ผิดพลาด' 		
	WHEN LEFT(csn.TELP, 1) = 'T' THEN 'เบอร์ติดต่อไม่ได้' 	
	when  SUBSTRING(csn.TELP,1,1)!='0' THEN 'เบอร์ผิดพลาด' 
	WHEN LEFT(csn.TELP, 1) = 'S' THEN 'เบอร์ปิดกั้นSMS' 
	WHEN isnull(csn.TELP, '') = '' THEN 'ไม่มีเบอร์' 
	ELSE 'OK' 
END AS numberOk
,c.CustFName var1,aggr.msg var2
,SMS1.msg msg1,case when len(SMS1.msg)>70 then ceiling(CONVERT(decimal(18,2),len(SMS1.msg))/67) else 1.00 end countSMS1 
,SMS2.msg msg2,case when len(SMS2.msg)>70 then ceiling(CONVERT(decimal(18,2),len(SMS2.msg))/67) else 1.00 end countSMS2 
,br.BranchName
,GETDATE() loadDateTime
,convert(datetime,CONVERT(varchar,getdate(),111)) loadDate,@sdate dateOfPremiums
 from (
	select distinct CustID,BranchNo from Premiums p 
	where dasSDATE=@sdate 
)dstn 
left join DBFREE.dbo.Customer c on dstn.CustID=c.CustID 
left join #customer csn on c.CustIDCardNo=csn.IDNO collate Thai_CI_AS
left join DBFREE.dbo.Branch br on dstn.BranchNo=br.BranchNo
cross apply (select LEFT(dbo.fnRemoveNonNumericCharacters(csn.TELP),10) TELP)prc
cross apply (select dbo.premiumForCustomer(@sdate,dstn.CustID)msg)aggr
cross apply (select 'เรียนคุณ'+c.CustFName+ ' คุณได้รับของแถม(' +aggr.msg+')จากบ.ตั้งใจยนตรการ ในวันที่ '+@sdate+' หากได้รับสินค้าไม่ครบตามจำนวนดังกล่าว ให้ติดต่อกลับ 0899733616' msg)SMS1
cross apply (select 'คุณได้รับของแถม(' +aggr.msg+')จากตั้งใจยนตรการในวันที่ '+@sdate+' หากได้รับสินค้าไม่ครบให้โทรกลับ 0899733616' msg)SMS2

/*
select *  from rpt_SMS4VerifyWithCustomers where dateOfPremiums='2560/03/07'
delete from rpt_SMS4VerifyWithCustomers where dateOfPremiums='2560/03/07'
--select MAX(r1) from (
SELECT 
	--TOP 1000 
[snSDATE],[dasSDATE],[PartNo],[PartName],[Price],[SaleQTY],[unitCount],[LineNum],[GroupId],[CustIDCardNo],[CustFName],[CustLName],[SaleStatus],[SaleNo],[BranchNo]
,RANK() over (partition by pd.dasSDATE,pd.CustIDCardNo ORDER By pd.PartNo) r1,hpc.TELP,pd.HomeTelNo
      --into #premiums
FROM [ITREPORT].[dbo].[PremiumDetails] pd left join #customer hpc on pd.CustIDCardNo=hpc.IDNO collate Thai_CS_AS
where pd.dasSDATE between @sdate and @sdate 
and pd.CustIDCardNo='1820500103683'
--)allu
*/

/*
select * from DBFREE.dbo.SPSale s inner join DBFREE.dbo.SPSaleDetail spd on s.SaleNo=spd.SaleNo and s.BranchNo=spd.BranchNo 
where s.SaleNo='60RTSL/0000018' and s.BranchNo='108'
select distinct SaleType from DBFREE.dbo.SPSale
select * from #customer where IDNO='1820500103683'
select distinct CustIDCardNo,SaleNo, from Premiums where dasSDATE='2560/01/11' and CustIDCardNo='1820500103683'
select convert(varchar(5),convert(int,sum([SaleQTY])))sqlQty,PartName,[unitCount] from PremiumDetails where dasSDATE='2560/01/11' and CustIDCardNo='1820500103683' group by PartName,[unitCount]
select * from PremiumDetails where dasSDATE='2560/01/11' and CustIDCardNo='1820500103683'
*/

-- distinct หา บัตรประจำตัวประชาชน, และ saleNo จาก view Premiums (ลูกค้า 1 คน คงไม่เข้าหลายร้านในวันเดียวกัน)
--select distinct CustIDCardNo,SaleNo from Premiums where dasSDATE=@sdate

--select * from DBFREE.dbo.Customer where CustIDCardNo='1810100032983'
--select dbo.premiumForCustomer('2560/01/11','3860400028465')
/*
--หายอดรวมของ ของแถมแต่ละชิ้นก่อน เนื่องจากลูกค้าบางรายออกรถหลายคันและได้ของแถมจากหลายบิล
declare @prm table(prm varchar(max));
insert into @prm
select --convert(varchar(5),convert(int,sum([SaleQTY])))sqlQty,PartName,[unitCount],
''+PartName+' '+ convert(varchar(5),convert(int,sum([SaleQTY])))+' '+[unitCount] sql
from PremiumDetails 
where dasSDATE='2560/01/11' and CustIDCardNo='1820500103683' 
group by PartName,[unitCount]

DECLARE @Names VARCHAR(max);
set @Names = '';
SELECT @Names = COALESCE(@Names + '{{', '') + prm+'}}'
FROM @prm th 
print replace(replace(replace(@Names,'}}{{',','),'{{','#'),'}}','#');

select convert(datetime,CONVERT(varchar,getdate(),111))
*/

 --update rpt_SMS4VerifyWithCustomers set msg1=replace(msg1,'0869478548','0899733616'),msg2=replace(msg2,'0869478548','0899733616')