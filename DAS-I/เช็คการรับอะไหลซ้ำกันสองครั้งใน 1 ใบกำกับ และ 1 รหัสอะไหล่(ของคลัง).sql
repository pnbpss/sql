use RMSDB;
IF OBJECT_ID('tempdb..#abc') is not null DROP TABLE #abc
IF OBJECT_ID('tempdb..#def') is not null DROP TABLE #def

select uall.* 
into #abc
from (
select a.BInvoiceNo [taxno],b.PartNo,b.ReceiveQTY,a.ReceiveNo,a.ReceiveDate,a.UserID
from dbsps.dbo.SPBReceive a inner join dbsps.dbo.SPBReceiveDetail b on a.ReceiveNo=b.ReceiveNo
left join [DBSPS].[dbo].[OthTableStatus] oth on a.ReceiveNo=oth.DocumentNo and oth.BranchNo='001' and oth.TableName='SPBReceive' 
where a.ReceiveStatus='Approved' and a.BInvoiceNo like '%PD%' and a.BInvoiceNo!=''  and a.BranchNo='001' and oth.[Status] is null
union
select a.TaxInvoiceNo [taxno],b.PartNo,b.ReceiveQTY,a.ReceiveNo,a.ReceiveDate,a.UserID
from dbsps.dbo.SPBReceive a inner join dbsps.dbo.SPBReceiveDetail b on a.ReceiveNo=b.ReceiveNo
left join [DBSPS].[dbo].[OthTableStatus] oth on a.ReceiveNo=oth.DocumentNo and oth.BranchNo='001' and oth.TableName='SPBReceive'
where a.ReceiveStatus='Approved' and a.TaxInvoiceNo like '%PD%' and a.TaxInvoiceNo!='' and a.BranchNo='001' and oth.[Status] is null
) uall


select upload.PO_INV_NO,upload.sumOrdQty,das.sumRec
,(upload.sumOrdQty-das.sumRec) diff 
,allRec.ReceiveNo,allRec.ReceiveQTY,upload.PartNo,allRec.ReceiveDate,allRec.UserId
,RANK() over (partition by upload.PartNo order by upload.PO_INV_NO,upload.PartNo,allRec.ReceiveNo) as r
into #def
from (
	select SUM(OrderQTY) sumOrdQty,PartNo,u.PO_INV_NO 
	from RMSDB.dbo.T_POReceive_Upload u 
	group by PartNo,u.PO_INV_NO
)upload left join (
	select SUM(recv.ReceiveQTY) sumRec,taxno,PartNo from (
		select a.BInvoiceNo [taxno],b.PartNo,b.ReceiveQTY,b.ReceiveNo
		from dbsps.dbo.SPBReceive a inner join dbsps.dbo.SPBReceiveDetail b on a.ReceiveNo=b.ReceiveNo
		left join [DBSPS].[dbo].[OthTableStatus] oth on a.ReceiveNo=oth.DocumentNo and oth.BranchNo='001' and oth.TableName='SPBReceive'
		where a.ReceiveStatus='Approved' and a.BInvoiceNo like '%PD%' and a.BInvoiceNo!='' and a.BranchNo='001' and b.BranchNo='001' and oth.[Status] is null
		union
		select a.TaxInvoiceNo [taxno],b.PartNo,b.ReceiveQTY,b.ReceiveNo
		from dbsps.dbo.SPBReceive a inner join dbsps.dbo.SPBReceiveDetail b on a.ReceiveNo=b.ReceiveNo
		left join [DBSPS].[dbo].[OthTableStatus] oth on a.ReceiveNo=oth.DocumentNo and oth.BranchNo='001' and oth.TableName='SPBReceive'
		where a.ReceiveStatus='Approved' and a.TaxInvoiceNo like '%PD%' and a.TaxInvoiceNo!='' and a.BranchNo='001' and b.BranchNo='001' and oth.[Status] is null
	) recv
	--where taxno='PD2015293688'
	group by taxno,PartNo
) das on upload.PO_INV_NO=das.taxno and upload.PartNo=das.PartNo
left join #abc allRec on upload.PO_INV_NO=allRec.taxno and upload.PartNo=allRec.PartNo
where (upload.sumOrdQty-das.sumRec) < 0 
--order by upload.PO_INV_NO,upload.PartNo,r
--and upload.PO_INV_NO='PD2015293688'

--goto endScript


select main.PO_INV_NO,'' dealerPONumber,main.sumOrdQty,main.sumRec,main.diff
,r1.r,r1.ReceiveNo,r1.ReceiveQTY,r1.PartNo,r1.ReceiveDate,r1.UserId
,r2.r,r2.ReceiveNo,r2.ReceiveQTY,r2.PartNo,r2.ReceiveDate,r2.UserId
,r3.r,r3.ReceiveNo,r3.ReceiveQTY,r3.PartNo,r3.ReceiveDate,r3.UserId
,r4.r,r4.ReceiveNo,r4.ReceiveQTY,r4.PartNo,r4.ReceiveDate,r4.UserId
from (select distinct main.PO_INV_NO,main.PartNo,main.sumOrdQty,main.sumRec,main.diff from #def main) main
	left join #def r1 on main.PO_INV_NO=r1.PO_INV_NO and main.PartNo=r1.PartNo and r1.r=1
	left join #def r2 on main.PO_INV_NO=r2.PO_INV_NO and main.PartNo=r2.PartNo and r2.r=2
	left join #def r3 on main.PO_INV_NO=r3.PO_INV_NO and main.PartNo=r3.PartNo and r3.r=3
	left join #def r4 on main.PO_INV_NO=r4.PO_INV_NO and main.PartNo=r4.PartNo and r4.r=4
where 1=1 and r1.r=1 

endScript: