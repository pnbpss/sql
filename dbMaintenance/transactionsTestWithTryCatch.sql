
/*
--���ҧ table ����ҡ�͹ 4 table
--����� table zzaa �� primary key ����Ŵ� a ��蹤�ͫ�������

create table zzaa (a int not null,b varchar(max),c datetime); 
alter table zzaa add primary key (a);
create table zzbb (a int not null,b varchar(max));
create table zzcc (a int not null,b varchar(max));
create table zzdd (a int not null,b varchar(max));

*/

--�������÷� transaction
declare @errorOccured int;
set @errorOccured = 0;
begin transaction t1;
begin try 
	insert into zzbb (a,b) values (2,'value 1');
		
	-- �ҡ�ѹ㹤����á �ѹ�м�ҹ, �ҡ run ���駷���ͧ�ѹ�� error �ç��÷Ѵ��ҧ��ҧ���
	--insert into zzaa (a,b,c) values (2,'value 1',getdate()); 
	insert into zzaa (a,b,c) values (2,'value 1','aaaa'); 
	-- ������� a �� primary key �ѹ�� insert ����������� 
	-- ����ѹ�дմ�͡���� ��� catch ���� rollback
	 
	insert into zzbb (a,b) values (2,'value 2'); 
	insert into zzcc (a,b) values (2,'value 1'); 
	declare @maxbb int;
	set @maxbb = (select max(a) from zzbb);
	insert into zzdd (a,b) values(@maxbb,'value 1');
	commit transaction t1; print 'ok'; --�ҡ�ѹ run ��ҹ������������������� commit
end try
begin catch 
	declare @errorMessage varchar(max); 
	declare @errorNumber int;
	set @errorMessage = (SELECT convert(varchar(max),ERROR_NUMBER())+':'+ERROR_MESSAGE());
	print 'Error('+ @errorMessage+')';
	
	-- �ҡ�ѹ catch error(�� error �Դ���) �ѹ��������� rollback ��͹��Ѻ����������ء� table
	rollback transaction t1; 
	-- �ҡ��Һ�÷Ѵ��ҧ������͡ table zzbb �ж١ insert ���¤�� (2,'value 1') ������� 
	--�֧������ error ��ǹ table zzbb �Ѻ zzcc ���ѧ�� insert �����ŵ������
	--(��觼Դ��ѡ transaction processing)	
end catch
-- begin catch �е�ͧ����Ѵ �ҡ entry 㹷ѹ��

--�� transaction

/*
--�����鷴�ͧ select ������
select 'zzaa' as tableAA,* from zzaa;
select 'zzbb' as tableBB,* from zzbb;
select 'zzcc' as tableCC,* from zzcc;
select 'zzdd' as tableDD,* from zzdd;
*/

/*
--������������������͡
delete from zzaa;
delete from zzbb;
delete from zzcc;
delete from zzdd;

*/

/*
drop table zzaa;
drop table zzbb;
drop table zzcc;
drop table zzdd;
*/