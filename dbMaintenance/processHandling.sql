select * from sys.sysprocesses where blocked = 0 and spid in 
(select blocked from sys.sysprocesses where blocked > 0) 

DECLARE @sqltext VARBINARY(1280)
SELECT @sqltext = sql_handle
FROM sys.sysprocesses
WHERE spid = 425
SELECT TEXT
FROM sys.dm_exec_sql_text(@sqltext)
GO

--kill 425;

