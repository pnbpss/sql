select
   syscolumns.name as [Column],
   syscolumns.xusertype as [Type],
   sysobjects.xtype as [Objtype]
   ,INFORMATION_SCHEMA.COLUMNS.DATA_TYPE
   ,*
from 
   sysobjects, syscolumns,INFORMATION_SCHEMA.COLUMNS 
where sysobjects.id = syscolumns.id and INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME=syscolumns.name and sysobjects.name =INFORMATION_SCHEMA.COLUMNS.TABLE_NAME
--and   sysobjects.xtype = 'u'
and sysobjects.[type]='U' --table
and INFORMATION_SCHEMA.COLUMNS.DATA_TYPE='text'
--and sysobjects.[type]='V' --view
--and sysobjects.[type]='FN' --function
--and   sysobjects.name = 'MyTableName'
order by sysobjects.name,syscolumns.name