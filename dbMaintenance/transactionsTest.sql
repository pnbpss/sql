/*
drop table zzaa;
drop table zzbb;
drop table zzcc;
drop table zzdd;

create table zzaa (a int not null,b varchar(max));
alter table zzaa add primary key (a);
create table zzbb (a int not null,b varchar(max));
create table zzcc (a int not null,b varchar(max));
create table zzdd (a int not null,b varchar(max));

*/



declare @errorOccured int;
set @errorOccured = 0;
begin tran t1;
insert into zzbb (a,b) values (2,'value 1'); if @@ERROR>0 begin set @errorOccured = 1; end
insert into zzbb (a,b) values (2,'value 2'); if @@ERROR>0 begin set @errorOccured = 1; end
insert into zzbb (a,b) values (2,'value 3'); if @@ERROR>0 begin set @errorOccured = 1; end
insert into zzaa (a,b) values (2,'value 1'); if @@ERROR>0 begin set @errorOccured = 1; end
insert into zzbb (a,b) values (2,'value 4'); if @@ERROR>0 begin set @errorOccured = 1; end
insert into zzcc (a,b) values (2,'value 1'); if @@ERROR>0 begin set @errorOccured = 1; end
declare @maxbb int;
set @maxbb = (select max(a) from zzbb);
insert into zzdd (a,b) values(@maxbb,'value 1');
if @errorOccured = 1 
	begin 
		rollback tran t1; 
		print 'error'; 
	end else 
	begin 
		commit tran t1;
		print 'ok';  
	end


select 'zzaa' as tableAA,* from zzaa;
select 'zzbb' as tableBB,* from zzbb;
select 'zzcc' as tableCC,* from zzcc;
select 'zzdd' as tableDD,* from zzdd;


/*
begin tran t1
delete from zzaa;
delete from zzbb;
delete from zzcc;
delete from zzdd;
commit tran t1;
*/