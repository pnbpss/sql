/****** Script for SelectTopNRows command from SSMS  ******/
/*
use DBSPS
declare @partNo as varchar(max);
declare @t1 table (pn varchar(max));
set @partNo='18318-KYT-900TB';
insert into @t1 select spn from [RMSDB].[dbo].[T_SubstitutionPartsFinal] where pn=@partNo;
insert into @t1 select @partNo;

SELECT pl.PartName,br.BranchName, main.[CompCode]
,main.[BranchNo],main.[BrandID],main.[PartNo],main.[RefNo],main.[TrnsDate],main.[InQTY],main.[OutQTY],main.[LastQTY],main.[Price]
,main.[Cost],main.[Remark],main.[TimeStamp],main.[UserID]
FROM [DBSPS].[dbo].[SPStockCard] main 
	inner join (select MAX([TimeStamp]) tstmp,PartNo,CompCode,BranchNo 
				from [DBSPS].[dbo].[SPStockCard] spstck
				where PartNo in (select pn from @t1)
				--where PartNo ='18318-KYT-900TB'
				group by PartNo,CompCode,BranchNo
				) mxtstmp on main.[TimeStamp]=mxtstmp.tstmp and main.PartNo=mxtstmp.PartNo and main.BranchNo=mxtstmp.BranchNo
				  and main.CompCode=mxtstmp.CompCode
	inner join SPPartList pl on main.PartNo=pl.PartNo
	inner join Branch br on main.BranchNo=br.BranchNo
where main.PartNo in (select pn from @t1)
*/
--select * from SPPartList

use DBSPS
/*
declare @partNo as varchar(max);
--declare @t1 table (pn varchar(max));
--set @partNo='18318-KYT-900TB';
--insert into @t1 select spn from [RMSDB].[dbo].[T_SubstitutionPartsFinal] where pn=@partNo;
--insert into @t1 select @partNo;

IF OBJECT_ID('tempdb.dbo.#abcdef', 'U') IS NOT NULL DROP TABLE #abcdef;

select stc.*,pm.Onhand 
into #abcdef
from (
SELECT --'(^..^)' a,
main.[PartNo]
--,pl.PartName
--,main.[CompCode]
--,'BR'+main.[BranchNo] branchNo
,main.[BranchNo]
,br.BranchName
,main.[BrandID]
--,main.[RefNo],main.[TrnsDate],main.[InQTY],main.[OutQTY]
,sum(main.[LastQTY])sumQty
--,main.[Price],main.[Cost],main.[Remark],main.[TimeStamp],main.[UserID]
FROM [DBSPS].[dbo].[SPStockCard] main 
	inner join (select MAX([TimeStamp]) tstmp,PartNo,CompCode,BranchNo,BrandID
				from [DBSPS].[dbo].[SPStockCard] spstck
				--where PartNo in (select pn from @t1)
				--where PartNo ='18318-KYT-900TB'
				where 1=1
				--LastQTY>0 
				--and PartNo not like '%(ก)%' 
				--and PartNo='22535-KWW-P10' and BranchNo='001'
				group by PartNo,CompCode,BranchNo,BrandID
				) mxtstmp on main.[TimeStamp]=mxtstmp.tstmp 
				  and main.PartNo=mxtstmp.PartNo 
				  and main.BranchNo=mxtstmp.BranchNo				  
				  and main.CompCode=mxtstmp.CompCode
				  and main.BrandID=mxtstmp.BrandID
	inner join SPPartList pl on main.PartNo=pl.PartNo and main.BrandID= pl.BrandID and pl.ProductID not in ('003') --and pl.BrandID='003'
	inner join Branch br on main.BranchNo=br.BranchNo	
where 1=1
--and main.BranchNo='001'
and pl.PartNo not like '%(ก)%' 
--and main.PartNo='22535-KWW-P10'
group by
--,main.[CompCode]
--,
main.[BranchNo]
,br.BranchName
,pl.PartName
,main.[BrandID]
,main.[PartNo]
) stc left join SPPartMaster pm on stc.branchNo=pm.BranchNo and stc.PartNo=pm.PartNo and stc.BrandID=pm.BrandID
where 1=1
--and stc.sumQty!=pm.Onhand
--select * from SPStockCard where BranchNo='001' and PartNo='ZH42701-KVB-900' order by [TimeStamp] desc
--select * from #abcdef;
*/
--IF OBJECT_ID('tempdb.dbo.#abcdef', 'U') IS NOT NULL DROP TABLE #abcdef;
IF OBJECT_ID('tempdb.dbo.#hij', 'U') IS NOT NULL DROP TABLE #hij;

declare @p1 varchar(max),@pn varchar(max),@sumPnQty int, @spn varchar(max),@sumSpnQty int;
declare @t1 table (PartNo varchar(max),BranchNo varchar(4),BranchName varchar(max),BrandId varchar(4),sumQty int, onHand int)
declare @t2 table (PartNo varchar(max),BranchNo varchar(4),BranchName varchar(max),BrandId varchar(4),sumQty int, onHand int)
declare @t3 table (pn varchar(max),sumPnQty int, spn varchar(max),sumSpnQty int)
insert into @t1 select * from #abcdef;
insert into @t2 select * from #abcdef;

declare partInfo cursor for select PartNo from #abcdef
open partInfo
FETCH NEXT FROM partInfo  INTO @p1 
WHILE @@FETCH_STATUS = 0 
begin
	print @p1
	--set @pn=@p1;
	--set @sumPnQty = (select SUM(a.sumQty) from @t1 a where a.PartNo=@pn)
	--set @sumSpnQty = (select SUM(a.sumQty) from @t1 a where a.PartNo in (select spn from RMSDB.dbo.T_SubstitutionPartsFinal where pn=@p1))
	--insert into @t3 (pn,sumPnQty,sumSpnQty) values (@pn,@sumPnQty,@sumSpnQty);
	--delete from @t1 where PartNo in (select spn from RMSDB.dbo.T_SubstitutionPartsFinal where pn=@p1)
	FETCH NEXT FROM partInfo  INTO @p1
end
CLOSE partInfo;  
DEALLOCATE partInfo;

select * into #hij from @t3;
