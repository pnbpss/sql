use RMSDB;
declare @t as table(
 pn varchar(max),st varchar(max)
)
--insert into @t select PartNo,SubstitutionWith from T_Substitution_Parts
--truncate table T_Computed_Substitution_Parts
--insert into T_Computed_Substitution_Parts
select sp.PartNo,pst.st from 
( 
	select PartNo,SubstitutionWith from T_Substitution_Parts 
	union 
	select SubstitutionWith PartNo,PartNo SubstitutionWith from T_Substitution_Parts 
) sp
cross apply T_FN_PartSubstitutionMain(sp.PartNo) pst where sp.PartNo in 
(	
	select distinct PartNo from ( 
	select distinct PartNo PartNo from T_Substitution_Parts
	union
	select distinct SubstitutionWith PartNo from T_Substitution_Parts
	) un	
--'06110-397-405'
)

--select * from T_Computed_Substitution_Parts where PartNo='06110-397-405'
--select * from T_FN_PartSubstitutionMain('11100-KZR-600')
--select * from T_FN_PartSubstitutionSub('11100-KZR-600')
--select * from T_Substitution_Parts

select * from T_Computed_Substitution_Parts where PartNo='11100-KZR-600'