use RMSDB

--��� T_RawSubstitutionParts(�ҡ�Ѵ����) ������ T_SubstitutionParts ���ͧ truncate table T_SubstitutionParts ��͹
truncate table T_SubstitutionParts
declare @p1 varchar(max),@p2 varchar(max),@result varchar(max);
declare partInfo cursor for select PartNo,SubstitutionWith from T_RawSubstitutionParts
open partInfo
FETCH NEXT FROM partInfo  INTO @p1, @p2  
WHILE @@FETCH_STATUS = 0  
begin
	set @result = '';
	exec dbo.PS_ComputeSubstitutionPart @p1, @p2, @result output
	print @p1+' '+@p2+' '+@result
	FETCH NEXT FROM partInfo  INTO @p1, @p2		
end
CLOSE partInfo;  
DEALLOCATE partInfo; 
go
/*
--���ͧ select
declare @focusOn varchar(max);
set @focusOn='95001-45100-40S'; --focusOn ���������������ʹ�
with pst as (
	select a.pn,a.spn,0 as dept,ring from T_SubstitutionParts a where a.pn=@focusOn
	union all
	select a2.pn,a2.spn,p.dept+1,a2.ring 
		from T_SubstitutionParts a2 
			inner join pst p on a2.spn=p.pn 
			where 1=1
			--and p.dept<100 
			and a2.pn!=@focusOn
)
select * from pst order by pn OPTION (MAXRECURSION 1000) 
*/

--������ final ��������ä������� ���ͧ truncate table T_SubstitutionPartsFinal ��͹�ء����
truncate table T_SubstitutionPartsFinal
insert into T_SubstitutionPartsFinal (PartNo,pn,spn,dept,ring)
SELECT [PartNo],spn.pn,spn.spn,spn.dept,spn.ring
FROM (SELECT distinct [pn] PartNo FROM [RMSDB].[dbo].[T_SubstitutionParts]) tod
cross apply dbo.FN_SubstitutionParts(tod.PartNo) spn
where 1=1
go
--and tod.PartNo='22201-KBW-900'
/*
truncate table T_SubstitutionParts
exec dbo.PS_CreateRing 'a','b';
exec dbo.PS_AddToRing 'a','d';
exec dbo.PS_AddToRing 'a','c';
exec dbo.PS_AddToRing 'a','x';

exec dbo.PS_CreateRing 's','u';
exec dbo.PS_AddToRing 's','t';
exec dbo.PS_AddToRing 'u','v';
exec dbo.PS_AddToRing 's','w';

exec dbo.PS_MergeRing 'b','s'

select * from T_SubstitutionParts order by pn;
*/

select * from T_SubstitutionPartsFinal order by ring
select * from T_SubstitutionParts order by ring