/****** Script for SelectTopNRows command from SSMS  ******/
use RMSDB;
SELECT a.[PRNo] 'เลขที่ใบสั่ง'
	  ,c.BranchName 'สาขา'
	  ,b.PRCreatedDate 'เลขาศูนย์ฯอนุมัติใบสั่ง'
	  ,b.ReasonCode 'ประเภทการสั่ง'
      ,a.[BrandID] 'รหัสยี่ห้อ'
      ,a.[PartNo] 'หมายเลขอะไหล่'
      ,d.PartName 'ชื่ออะไหล่'
      ,a.[TransferStatus] 
      ,a.[OrderQTY] 'ยอดสั่ง'
      ,a.[PendingQTY] 'ยอดค้าง'
      ,a.[UnitPrice]
      ,a.[UpdateBy] 
      ,a.[LastUpdate]
      ,a.[RecordStatus]      
  FROM [RMSDB].[dbo].[T_OrderControl_Detail] a 
	   left join [RMSDB].[dbo].[T_PRHeader] b on a.PRNo=b.PRNo
	   left join DBSPS.dbo.Branch c on b.BranchCode=c.BranchNo
	   left join DBSPS.dbo.SPPartList d on a.PartNo=d.PartNo and a.BrandID=d.BrandID
  where a.PendingQTY>0 --and month(b.PRCreatedDate)=9 and YEAR(b.PRCreatedDate)=2015
	    and b.PRCreatedDate between '2013/01/01' and dateadd(day,1,getdate())
  order by b.PRCreatedDate desc
  --select dateadd(day,1,getdate())