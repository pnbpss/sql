--select COUNT(*)cnt,ring from T_SubstitutionParts group by ring order by ring
use RMSDB;
select ring,isnull(pl.PartName,rtp.partName+' rawNut')PartName,isnull(R1,'')R1,	isnull(R2,'')R2,	isnull(R3,'')R3,	isnull(R4,'')R4,	isnull(R5,'')R5,	isnull(R6,'')R6,	isnull(R7,'')R7,	isnull(R8,'')R8,	isnull(R9,'')R9,	isnull(R10,'')R10,	isnull(R11,'')R11,	isnull(R12,'')R12,	isnull(R13,'')R13,	isnull(R14,'')R14,	isnull(R15,'')R15,	isnull(R16,'')R16,	isnull(R17,'')R17,	isnull(R18,'')R18,	isnull(R19,'')R19,	isnull(R20,'')R20
from
(
	select ring,max(spn)spn,'R'+convert(varchar,rnk) RNK
	from (
		select stts.spn,stts.ring, rank() over( partition by stts.ring order by stts.spn  ) rnk
		from(
			select distinct spn,ring from T_SubstitutionParts
		) stts
	) t1 group by ring,rnk
) t pivot
(
	 max(spn) for RNK in (R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,R16,R17,R18,R19,R20)
) p
left join DBSPS.dbo.SPPartList pl on p.R1=pl.PartNo and pl.BrandID='001'
left join (SELECT distinct [PartNo],max(partName) partName
			FROM [RMSDB].[dbo].[T_RawSubstitutionParts]
			group by PartNo
			) rtp on p.R1=rtp.PartNo
where 1=1
order by R20 desc,	R19 desc,	R18 desc,	R17 desc,	R16 desc,	R15 desc,	R14 desc,	R13 desc,	R12 desc,	R11 desc,	R10 desc,	R9 desc,	R8 desc,	R7 desc,	R6 desc,	R5 desc,	R4 desc,	R3 desc,	R2 desc,	R1 desc
--order by ring
--and pl.PartName is null

--select * from DBSPS.dbo.SPPartList where PartNo='13218-REA-Z01'