use serviceweb;
IF OBJECT_ID('tempdb.dbo.#tempPendingStats', 'U') IS NOT NULL DROP TABLE #tempPendingStats;

SELECT va.[CONTNO],ar.[LOCAT],ar.CUSCOD cuscod,[netPendingPeriod],getdate() loadDateTime into #tempPendingStats FROM view_arpay2 va inner join wb_armast ar on va.CONTNO=ar.CONTNO
CREATE NONCLUSTERED INDEX [chedNoreIdx] ON #tempPendingStats ([contno] ASC,[cuscod] ASC,[netPendingPeriod] ASC) INCLUDE ( [loadDateTime]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

declare @startDate datetime, @endDate datetime;
set @startDate='20160801';set @endDate='20160831'
truncate table ar_riskAssestment;
insert into ar_riskAssestment 
select allu.LOCAT,convert(varchar,allu.SDATE,111)SDATE,allu.CONTNO
	,case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end CUSCOD
	,allu.contCustName
	,case when allu.GCODE in ('05','051','052') then '�ṹ��' else case when allu.STAT='N' then 'ö����' else 'ö����ͧ' end end typeOfProduct
	,case when allu.buyer='Y' then '1' else '' end buyer
	,case when allu.buyer='N' then '1' else '' end surety
	,case when allu.GRADE='F' then '1' else ''end F
	,case when allu.GRADE='FF' then '1'else ''end FF
	,case when mt1.cnt>0 then '1' else ''end mt1
	,case when mt1.cnt=0 then '' else convert(varchar,mt1.cnt) end mt1Cnt
	,case when lsurety.cnt>0 then '1'else ''end lsurety
	,case when fForB.cnt>0 then '1' else ''end fForB
	,case when seizedCount.cnt=1 then '1'else ''end seized1,case when seizedCount.cnt=2 then '1'else ''end seized2,case when seizedCount.cnt>=3 then '1'else ''end seized3up
	,case when convert(varchar,pd1.cnt)!='0' then convert(varchar,pd1.cnt) else '' end pd1Cnt
	,case when convert(varchar,pd2.cnt)!='0' then convert(varchar,pd2.cnt) else '' end pd2Cnt
	,case when convert(varchar,pd3up.cnt)!='0' then convert(varchar,pd3up.cnt) else '' end pd3upCnt
	,case when wrongzone.cnt=1 then '1' else '' end wz
	
	 from (
		select cont.LOCAT,cont.SDATE,cont.CONTNO,cont.CUSCOD,buyer.customerFullName contCustName
		,'Y' buyer,buyer.GRADE,i.STAT,i.GCODE,null suretyCuscod,null suretyName
		from wb_armast cont 
		left join wb_invtran i on cont.CONTNO=i.CONTNO and cont.STRNO=i.STRNO
		left join wb_customers buyer on cont.CUSCOD=buyer.CUSCOD
		where cont.SDATE between @startDate and @endDate
		union
		select cont.LOCAT,cont.SDATE,cont.CONTNO,cont.CUSCOD,s1.fullName contCustName
		,'N' buyer,s1.GRADE,i.STAT,i.GCODE,s1.CUSCOD suretyCuscod,s1.fullName suretyName
		from wb_armast cont 
		left join wb_invtran i on cont.CONTNO=i.CONTNO and cont.STRNO=i.STRNO
		left join wb_surety1 s1 on cont.CONTNO=s1.CONTNO
		where cont.SDATE between @startDate and @endDate and s1.CONTNO is not null
) allu
--�����ҡ���� 1 �ѹ����60�ѹ
cross apply (select (
				select isnull(COUNT(*),0)cnt 
				from wb_armast h 
				where h.CUSCOD=allu.CUSCOD and datediff(day,SDATE,allu.SDATE)<60
				and h.CONTNO!=allu.CONTNO
				)+
				(select isnull(COUNT(*),0)cnt 
				from wb_arhold h 
				where h.CUSCOD=allu.CUSCOD and datediff(day,SDATE,allu.SDATE)<61
				and h.CONTNO!=allu.CONTNO)
				cnt
			)   mt1
--��Ѵ�ѹ���ͼ�Ѵ�ѹ���
cross apply(
	select 
	--set1.buyer s1buyer,set1.CONTNO s1contno,set1.surety s1sure,set2.buyer s2buyer,set2.CONTNO s2contno,set2.surety s2sure
	COUNT(*)cnt
	from (
	select s1.CUSCOD surety,ar1.CONTNO,ar1.CUSCOD buyer,ar1.SDATE from wb_armast ar1 inner join wb_surety1 s1 on ar1.CONTNO=s1.CONTNO
	) set1 inner join (
	select s1.CUSCOD surety,ar1.CONTNO,ar1.CUSCOD buyer,ar1.SDATE from wb_armast ar1 inner join wb_surety1 s1 on ar1.CONTNO=s1.CONTNO
	) set2 on set1.buyer=set2.surety and set1.surety=set2.buyer and abs(DATEDIFF(day,set1.SDATE,set2.SDATE)) < 60 and set1.CONTNO!=set2.CONTNO
	where set1.CONTNO=allu.CONTNO
)lsurety
--��� F �ҫ���ö
cross apply
(
	select --ar1.CUSCOD,ar1.CONTNO,ar1.SDATE,ar2.CONTNO,ar2.SDATE,DATEDIFF(day,ar1.SDATE,ar2.SDATE) dd
	COUNT(*)cnt
	from wb_armast ar1 inner join wb_invtran i1 on ar1.CONTNO=i1.CONTNO and ar1.STRNO=i1.STRNO
	inner join wb_armast ar2 inner join wb_invtran i2 on ar2.CONTNO=i2.CONTNO and ar2.STRNO=i2.STRNO
	on ar1.CUSCOD=ar2.CUSCOD
	and ar1.CONTNO!=ar2.CONTNO
	and ar1.SDATE<=ar2.SDATE
	where i1.GCODE in ('05','051','052') 
	and i2.GCODE not in ('05','051','052') 
	and abs(DATEDIFF(day,ar1.SDATE,ar2.SDATE))<30
	and ar1.CONTNO = allu.CONTNO
) fForB
cross apply(
	select COUNT(*)cnt from wb_arhold 
		where CUSCOD=(select case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end)
	and SDATE<=allu.SDATE
) seizedCount

cross apply(
	select COUNT(*)cnt 
	from #tempPendingStats va  
	where va.CUSCOD=(select case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end)
	and va.netPendingPeriod=1 
	and va.contno!=allu.CONTNO
)pd1
cross apply(
	select COUNT(*)cnt 
	from #tempPendingStats va  
	where va.CUSCOD=(select case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end)
	and va.netPendingPeriod=2 
	and va.contno!=allu.CONTNO
)pd2
cross apply(	
	select COUNT(*)cnt 
	from #tempPendingStats va  
	where va.CUSCOD=(select case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end)
	and va.netPendingPeriod>2 
	and va.contno!=allu.CONTNO
)pd3up
cross apply
(
	SELECT count(*)cnt
	FROM [wb_contractSaleWrongZone] wz inner join wb_armast ar on wz.contno=ar.CONTNO
	where wz.contno=allu.CONTNO and ar.CUSCOD=(select case when allu.buyer='Y' then allu.CUSCOD else allu.suretyCuscod end)
) wrongzone
order by allu.CONTNO, buyer desc


select * from ar_riskAssestment;
/*
select oth.[ARCONT]
,oth.[TSALE],oth.[CONTNO]
,oth.[CUSCOD]
,c.customerFullName
,oth.[LOCAT]
,oth.[PAYFOR]
,oth.[PAYAMT]
,oth.[VATRT]
,oth.[TAXNO]
,convert(varchar,oth.[ARDATE],111) ARDATE
,oth.[SMPAY]
,oth.[SMCHQ]
,oth.[BALANCE]
,oth.[USERID]
,oth.[INPDT]
,oth.[MEMO1]
,oth.[INCFL]
,du.USERNAME
--,chq.CONTNO
from dc_SeniorAROTHR oth 
left join dc_hiincomeuser du on oth.USERID=du.USERID
left join wb_chqtran chq on oth.ARCONT=chq.CONTNO and chq.FLAG!='C'
left join wb_customers c on oth.CUSCOD=c.CUSCOD
where datediff(day,ARDATE,getdate())>30 and chq.CONTNO is null and oth.SMPAY=0

select oth.* into dc_othForTest
from dc_SeniorAROTHR oth 
left join dc_hiincomeuser du on oth.USERID=du.USERID
left join wb_chqtran chq on oth.ARCONT=chq.CONTNO and chq.FLAG!='C'
left join wb_customers c on oth.CUSCOD=c.CUSCOD
where datediff(day,ARDATE,getdate())>30 and chq.CONTNO is null and oth.SMPAY=0

--truncate table dc_OthTrash
select * from dc_OthTrash
alter table dc_OthTrash add dateTimeDelete datetime;
*/