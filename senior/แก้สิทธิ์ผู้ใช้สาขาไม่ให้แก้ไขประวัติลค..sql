/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [USERID]
      ,[SYSTEMCOD]
      ,[MENUCODE]
      ,[MENUDESC]
      ,[M_ACCESS]
      ,[M_EDIT]
      ,[M_INSERT]
      ,[M_DELETE]
      ,[MORDER]
  FROM [HIINCOME].[dbo].[MENUTRN]
  where 1=1
  --and USERID='006' 
  --and M_EDIT='T' 
  and M_DELETE='T'
  and MENUCODE='HINCI_SE01' 
  and SYSTEMCOD='SYS15'
  and USERID in (
  select USERID from HIINCOME.dbo.PASSWRD where EXPDATE is null 
			and not (
				LOCAT not like 'TJ%'
				and LOCAT not like 'YR%'
				and LOCAT not like 'YK%'
				and LOCAT not like 'YJ%'
				and LOCAT not like 'YN%'
				and LOCAT not like 'VJ%'
				and LOCAT not like 'MT%'
				and LOCAT not like 'F%'			
			) and USERID not IN ('006','005')
  )
  /*
  update [HIINCOME].[dbo].[MENUTRN]
  set M_EDIT='F'
  where
  USERID='1398' 
  and M_EDIT='T' 
  and MENUCODE='HINCI_SE01' 
  and SYSTEMCOD='SYS15'
  */
select * from PASSWRD where EXPDATE is null and LOCAT not like 'OFF%';
select * from PASSWRD where EXPDATE is null and LOCAT like 'OFF%';
  

--Select เฉพาะลูกพนักงานสาขา
select * from PASSWRD where EXPDATE is null 
	and not (
		LOCAT not like 'TJ%'
		and LOCAT not like 'YR%'
		and LOCAT not like 'YK%'
		and LOCAT not like 'YJ%'
		and LOCAT not like 'YN%'
		and LOCAT not like 'VJ%'
		and LOCAT not like 'MT%'
		and LOCAT not like 'F%'			
	) and USERID not IN ('006','005');


--update Select เฉพาะลูกพนักงานสาขา ไม่ให้แก้ประวัติลค.
/*
update [HIINCOME].[dbo].[MENUTRN]
  set M_EDIT='F'
  where  
  M_EDIT='T' 
  and MENUCODE='HINCI_SE01' 
  and SYSTEMCOD='SYS15'
  and USERID in (
		select USERID from PASSWRD where EXPDATE is null 
			and not (
				LOCAT not like 'TJ%'
				and LOCAT not like 'YR%'
				and LOCAT not like 'YK%'
				and LOCAT not like 'YJ%'
				and LOCAT not like 'YN%'
				and LOCAT not like 'VJ%'
				and LOCAT not like 'MT%'
				and LOCAT not like 'F%'			
			) and USERID not IN ('006','005')
)
*/