use HIINCOME;
IF OBJECT_ID('tempdb..#TempTBSaleStat') is not null DROP TABLE #TempTBSaleStat
select * into #TempTBSaleStat 
from (
	select i.SDATE,[TYPE],i.MODEL,i.BAAB,serviceweb.dbo.fnRemoveNonNumericCharactersForColor(i.COLOR)COLOR,i.CRLOCAT from HIINCOME.dbo.INVTRAN i where datediff(day,i.SDATE,GETDATE())<=180 and i.STAT='N' and i.SDATE is not null and i.[TYPE]='HONDA'
	union
	select i.SDATE,[TYPE],i.MODEL,i.BAAB,serviceweb.dbo.fnRemoveNonNumericCharactersForColor(i.COLOR)COLOR,i.CRLOCAT from HIINCOME.dbo.HINVTRAN i where datediff(day,i.SDATE,GETDATE())<=180 and i.STAT='N' and i.SDATE is not null and i.[TYPE]='HONDA'
) salestat

select main.CRLOCAT,main.MODEL,main.BAAB,main.cnt totalSale
,isnull(d000To015.cnt,0) d000To015
,isnull(d016To030.cnt,0) d016To030
,isnull(d031To060.cnt,0) d031To060
,isnull(d061To090.cnt,0) d061To090
,isnull(d091To120.cnt,0) d091To120
,isnull(d121To150.cnt,0) d121To150
,isnull(d151To180.cnt,0) d151To180
from (select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s group by s.[MODEL],s.BAAB,s.CRLOCAT
) main 
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 0 and 15 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d000To015 on main.[MODEL]=d000To015.MODEL and main.[CRLOCAT]=d000To015.[CRLOCAT] and main.[BAAB]=d000To015.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 16 and 30 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d016To030 on main.[MODEL]=d016To030.MODEL and main.[CRLOCAT]=d016To030.[CRLOCAT] and main.[BAAB]=d016To030.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 31 and 60 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d031To060 on main.[MODEL]=d031To060.MODEL and main.[CRLOCAT]=d031To060.[CRLOCAT] and main.[BAAB]=d031To060.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 61 and 90 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d061To090 on main.[MODEL]=d061To090.MODEL and main.[CRLOCAT]=d061To090.[CRLOCAT] and main.[BAAB]=d061To090.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 91 and 120 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d091To120 on main.[MODEL]=d091To120.MODEL and main.[CRLOCAT]=d091To120.[CRLOCAT] and main.[BAAB]=d091To120.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 121 and 150 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d121To150 on main.[MODEL]=d121To150.MODEL and main.[CRLOCAT]=d121To150.[CRLOCAT] and main.[BAAB]=d121To150.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 151 and 180 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d151To180 on main.[MODEL]=d151To180.MODEL and main.[CRLOCAT]=d151To180.[CRLOCAT] and main.[BAAB]=d151To180.[BAAB]