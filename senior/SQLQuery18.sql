--select top 5 * from CHQTRAN;

select 
--sum(PAYAMT) sumIncome
CONVERT(varchar, CAST(sum(PAYAMT) AS money), 1) sumIncome
,month(TMBILDT) as [month],year(TMBILDT) as [year]
from chqtran
where 
--TMBILDT = '2014/03/31'
TMBILDT between '2013/01/01' and '2014/03/31'
group by month(TMBILDT),year(TMBILDT)
order by year(TMBILDT) desc, month(TMBILDT) desc