use serviceweb
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
declare @strno varchar(max);
set @strno='126862'

select CONTNO,STRNO,SDATE,LOCAT,TOTPRC,SMPAY,c.customerFullName,
case 
	when typ='hold' then '�ִ'
	when typ='acti' then case when TOTPRC>SMPAY then '�ѧ��診' else '������� (�ѧ����������͡�ҡ�к�)' end
	when typ='close' then '�������'
	when typ='cred' then '����ʴ'
	when typ='tran' then '�����/������'
	else 'n/a'
end typ
--,netPendingPeriod,currentperiod
--,replace(replace(involved,'buyer','������'),'surety','����Ӥ����') involved
into #tempTb
from (
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,h.SMPAY,'hold' typ,null netPendingPeriod, null currentperiod,'buyer' involved, null CONTSTAT 
	from wb_arhold a inner join wb_harmast h on a.CONTNO=h.CONTNO 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,a.TOTPRC SMPAY,'close' typ,null netPendingPeriod, null currentperiod,'buyer'  involved,null CONTSTAT  
	from wb_arclose a 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,a.SMPAY,'acti' typ,null netPendingPeriod,null currentperiod,'buyer' involved,a.CONTSTAT 
	from wb_armast a 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,a.SMPAY,'cred' typ,null netPendingPeriod, null currentperiod,'buyer' involved,null CONTSTAT from wb_arcred a where a.STRNO like '%'+@strno+'%'
	
	union
	select s.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,h.SMPAY,'hold' typ,null netPendingPeriod, null currentperiod,'surety '+convert(varchar(max),s.GARNO) involved,null CONTSTAT 
	from wb_armgar s inner join wb_arhold a on s.CONTNO=a.CONTNO inner join wb_harmast h on a.CONTNO=h.CONTNO 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,a.TOTPRC SMPAY,'close' typ,null netPendingPeriod, null currentperiod,'surety '+convert(varchar(max),s.GARNO) involved,null CONTSTAT  
	from wb_armgar s inner join wb_arclose a on s.CONTNO=a.CONTNO 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,a.CUSCOD,a.STRNO,a.SDATE,a.TOTPRC,a.SMPAY,'acti' typ,va.netPendingPeriod,va.currentperiod,'surety '+convert(varchar(max),s.GARNO) involved,a.CONTSTAT 
	from wb_armgar s inner join wb_armast a on s.CONTNO=a.CONTNO inner join view_arpay3 va on a.CONTNO=va.CONTNO 
	where a.STRNO like '%'+@strno+'%'
	
	union
	select a.CONTNO,a.LOCAT,b.CUSCOD,a.STRNO,b.SDATE,a.TOTPRC,a.SMPAY,'tran' typ,null netPendingPeriod,null currentPeriod,'buyer' involved, null CONTSTAT 
	from wb_artrans a inner join wb_arinvoi b on a.CONTNO=b.CONTNO 
	where a.STRNO like '%'+@strno+'%'
)uall left join wb_customers c on uall.CUSCOD=c.CUSCOD


select 
CONTNO,STRNO,convert(varchar(12),SDATE,111) soldDate,LOCAT,TOTPRC,SMPAY,customerFullName,typ 
from #tempTb
order by SDATE desc
