--select top 10 * from invmovt;
--select top 10000 maxMovSeq.maxMovSeq,maxMovSeq.strno,moveto.moveto,moveto.movedt,i.SDATE,i.CONTNO,i.INPDT,case when moveto.movedt<i.INPDT then 'Y' else 'N' end as xx
select year(i.SDATE) [year],count(*) cnt
from 
(
	select max(movseq) maxMovSeq,strno from invmovt
	group by strno
) as maxMovSeq 
inner join (
	select strno,movseq,moveto,movedt from invmovt
) as moveto on maxMovSeq.strno=moveto.strno and maxMovSeq.maxMovSeq=moveto.movseq
inner join INVTRAN i on maxMovSeq.strno=i.STRNO and i.CRLOCAT=moveto.moveto
inner join ARMAST a on i.CONTNO=a.CONTNO and i.STRNO=a.STRNO
where i.SDATE<moveto.movedt
group by year(i.SDATE)
--order by moveto.movedt desc
order by year(i.SDATE) desc;

