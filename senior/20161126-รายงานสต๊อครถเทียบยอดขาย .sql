use HIINCOME;
--goto skipper

IF OBJECT_ID('tempdb..##TemporaryTb') is not null DROP TABLE ##TemporaryTb
declare @specifiedDate datetime;
set @specifiedDate = '2016/11/25';
exec sp_executesql N'
select * 
into ##TemporaryTb
from (
	SELECT V.RECVNO,V.RECVDT,V.INVNO,V.INVDT,V.TAXNO,V.TAXDT,T.GCODE, T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB
	,T.COLOR,T.CC, T.STRNO
	
	,T.ENGNO,T.KEYNO,T.STAT,T.RVLOCAT,T.CRLOCAT,T.MILERT, T.NETCOST,T.CRVAT,T.TOTCOST,T.TADDCOST,T.BONUS FROM INVINVO V,INVTRAN T WHERE (V.RECVNO=T.RECVNO) AND (V.LOCAT=T.RVLOCAT) and ''A''=@P1 AND (T.SDATE>@P2 OR T.SDATE IS NULL)   AND not exists(select strno  from (select strno,ydate as movedt,ylocat as moveto,sdate from arhold  union select strno,ydate as movedt,ylocat as moveto,sdate from archag) as c where c.strno = t.strno  and c.movedt >= v.recvdt and @P2>=c.sdate and @P2<c.movedt)  AND (T.TYPE LIKE @P3) AND (T.MODEL LIKE @P4) AND (T.BAAB LIKE @P5) AND (T.COLOR LIKE @P6)  AND (T.RVLOCAT LIKE @P7)  AND (T.STAT LIKE @P8) AND (V.RECVDT<= @P9) GROUP BY t.strno,t.recvno,v.recvdt,t.rvlocat,t.totcost,T.TADDCOST,T.BONUS,t.crvat,t.netcost,t.milert, t.crlocat,t.stat,t.keyno,t.engno,t.cc,t.color,T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB, V.INVDT,V.TAXNO
	,V.TAXDT
	,T.GCODE,V.INVNO,V.INVDT,V.RECVNO,V.RECVDT HAVING (@P10  < (select  min(b.movedt) from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as b where t.strno = b.strno and b.movedt >= v.recvdt) OR t.strno  not  in (select strno  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = t.strno and c.movedt >= v.recvdt)) UNION SELECT V.RECVNO,V.RECVDT,V.INVNO,V.INVDT,V.TAXNO,V.TAXDT
	,T.GCODE, T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB,T.COLOR,T.CC, T.STRNO
	
	,T.ENGNO,T.KEYNO,T.STAT,T.RVLOCAT,T.CRLOCAT,T.MILERT, T.NETCOST,T.CRVAT,T.TOTCOST,T.TADDCOST,T.BONUS FROM INVINVO V,INVTRAN T,(select strno,movedt,moveto,movseq from invmovt       union select strno,ydate as movedt,ylocat as moveto,10000 as movseq from arhold       union select strno,ydate as movedt,ylocat as moveto,10000 as movseq from archag) as E WHERE (V.RECVNO=T.RECVNO) AND (V.LOCAT=T.RVLOCAT) AND (T.STRNO = E.STRNO) and ''A''=@P11 AND (T.SDATE>@P12 OR T.SDATE IS NULL)   AND not exists(select strno  from (select strno,ydate as movedt,ylocat as moveto,sdate from arhold  union select strno,ydate as movedt,ylocat as moveto,sdate from archag) as c where c.strno = t.strno  and c.movedt >= v.recvdt and @P2>=c.sdate and @P2<c.movedt)  AND (T.TYPE LIKE @P13) AND (T.MODEL LIKE @P14) AND (T.BAAB LIKE @P15) AND (T.COLOR LIKE @P16)  AND (E.MOVETO LIKE @P17)  AND (T.STAT LIKE @P18)   and (e.movseq=(select max(movseq) from invmovt where strno=e.strno and movedt<=@P2) or (e.movseq=0) or ((e.movseq=10000) and e.strno not in (select strno from invmovt where strno=e.strno and e.movedt=movedt))  ) AND (E.MOVEDT<=@P19) and (v.recvdt<=e.movedt) GROUP BY t.strno,t.recvno,v.recvdt,t.rvlocat,t.totcost,T.TADDCOST,T.BONUS,t.crvat,t.netcost,t.milert, t.crlocat,t.stat,t.keyno,t.engno,t.cc,t.color,T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB, 
	V.INVDT,V.TAXNO,V.TAXDT,T.GCODE,V.INVNO,V.INVDT,V.RECVNO,V.RECVDT,e.strno,e.movedt HAVING (@P20  < (select  min(c.movedt)  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = e.strno and c.movedt > e.movedt) OR e.strno  not  in (select strno  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = e.strno and c.movedt > e.movedt)) 
)allu
'
,N'@P1 varchar(1),@P2 datetime,@P3 varchar(1),@P4 varchar(1),@P5 varchar(1),@P6 varchar(1),@P7 varchar(1),@P8 varchar(1),@P9 datetime,@P10 datetime,@P11 varchar(1),@P12 datetime,@P13 varchar(1),@P14 varchar(1),@P15 varchar(1),@P16 varchar(1),@P17 varchar(1),@P18 varchar(1),@P19 datetime,@P20 datetime'
,'A',@specifiedDate,'%','%','%','%','%','%',@specifiedDate,@specifiedDate,'A',@specifiedDate,'%','%','%','%','%','%',@specifiedDate,@specifiedDate

update ##TemporaryTb set COLOR = serviceweb.dbo.fnRemoveNonNumericCharactersForColor(COLOR);

-------------
IF OBJECT_ID('tempdb..#tempTBForA') is not null DROP TABLE #tempTBForA
select pv.PROVDES,CRLOCAT,MODEL,BAAB,COLOR,count(*)cnt,null maxStock,STAT
into #tempTBForA
from ##TemporaryTb byLocat
left join serviceweb.dbo.wb_branches bz on byLocat.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
where CRLOCAT not in ('OFF��','OFF��','OFF��')
group by pv.PROVDES,CRLOCAT,MODEL,BAAB,COLOR,STAT
update main set maxStock = j.[max] from #tempTBForA main left join serviceweb.dbo.wb_branchMaxMinStock j on main.CRLOCAT=j.locat and main.MODEL=j.model and main.BAAB=j.[type] and main.COLOR=j.color



IF OBJECT_ID('tempdb..#TempTBSaleStat') is not null DROP TABLE #TempTBSaleStat
IF OBJECT_ID('tempdb..#SaleStat') is not null DROP TABLE #SaleStat
select salestat.* into #TempTBSaleStat 
from (
	select i.SDATE,[TYPE],i.MODEL,i.BAAB,serviceweb.dbo.fnRemoveNonNumericCharactersForColor(i.COLOR)COLOR,i.CRLOCAT from HIINCOME.dbo.INVTRAN i where datediff(day,i.SDATE,GETDATE())<=180 and i.STAT='N' and i.SDATE is not null and i.[TYPE]='HONDA'
	union
	select i.SDATE,[TYPE],i.MODEL,i.BAAB,serviceweb.dbo.fnRemoveNonNumericCharactersForColor(i.COLOR)COLOR,i.CRLOCAT from HIINCOME.dbo.HINVTRAN i where datediff(day,i.SDATE,GETDATE())<=180 and i.STAT='N' and i.SDATE is not null and i.[TYPE]='HONDA'
) salestat
where CRLOCAT not in ('OFF��','OFF��','OFF��')

select main.CRLOCAT,main.MODEL,main.BAAB,main.cnt totalSale
,isnull(d000To015.cnt,0) d000To015
,isnull(d016To030.cnt,0) d016To030
,isnull(d031To060.cnt,0) d031To060
,isnull(d061To090.cnt,0) d061To090
,isnull(d091To120.cnt,0) d091To120
,isnull(d121To150.cnt,0) d121To150
,isnull(d151To180.cnt,0) d151To180
into #SaleStat
from (select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s group by s.[MODEL],s.BAAB,s.CRLOCAT
) main 
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 0 and 15 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d000To015 on main.[MODEL]=d000To015.MODEL and main.[CRLOCAT]=d000To015.[CRLOCAT] and main.[BAAB]=d000To015.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 16 and 30 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d016To030 on main.[MODEL]=d016To030.MODEL and main.[CRLOCAT]=d016To030.[CRLOCAT] and main.[BAAB]=d016To030.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 31 and 60 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d031To060 on main.[MODEL]=d031To060.MODEL and main.[CRLOCAT]=d031To060.[CRLOCAT] and main.[BAAB]=d031To060.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 61 and 90 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d061To090 on main.[MODEL]=d061To090.MODEL and main.[CRLOCAT]=d061To090.[CRLOCAT] and main.[BAAB]=d061To090.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 91 and 120 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d091To120 on main.[MODEL]=d091To120.MODEL and main.[CRLOCAT]=d091To120.[CRLOCAT] and main.[BAAB]=d091To120.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 121 and 150 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d121To150 on main.[MODEL]=d121To150.MODEL and main.[CRLOCAT]=d121To150.[CRLOCAT] and main.[BAAB]=d121To150.[BAAB]
left join 
(select COUNT(*)cnt,s.[MODEL],s.BAAB,s.CRLOCAT from #TempTBSaleStat s where datediff(day,s.SDATE,GETDATE()) between 151 and 180 group by s.[MODEL],s.BAAB,s.CRLOCAT)
d151To180 on main.[MODEL]=d151To180.MODEL and main.[CRLOCAT]=d151To180.[CRLOCAT] and main.[BAAB]=d151To180.[BAAB]


select PROVDES,CRLOCAT,MODEL,BAAB,COLOR,cnt,isnull(convert(varchar,maxStock),'n/a') maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB and a.COLOR=main.COLOR),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB and a.COLOR=main.COLOR),0)old
,case when isnull(convert(varchar,maxStock),'n/a')='n/a' then 'n/a'
  else convert(varchar,(maxStock-cnt))
end maxMinusAmount
,null totalSale,	null d000To015,	null d016To030,	null d031To060,	null d061To090,	null d091To120,	null d121To150,	null d151To180
from #tempTBForA main
union 
select PROVDES,CRLOCAT,MODEL,BAAB,'���� '+MODEL+'('+BAAB+')'COLOR,sum(cnt)cnt
,null maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB),0)old
,null maxMinusAmount
,isnull((select totalSale from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) totalSale
,isnull((select d000To015 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d000To015
,isnull((select d016To030 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d016To030
,isnull((select d031To060 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d031To060
,isnull((select d061To090 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d061To090
,isnull((select d091To120 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d091To120
,isnull((select d121To150 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d121To150
,isnull((select d151To180 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d151To180
from #tempTBForA main
group by PROVDES,CRLOCAT,MODEL,BAAB
union 
select PROVDES,CRLOCAT,'����'+CRLOCAT MODEL,''BAAB,''COLOR,sum(cnt)cnt
,convert(varchar,(select top 1 maxSt from serviceweb.dbo.servicewebdbowbarholddistrictname where locat=CRLOCAT)) maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT),0)old
,convert(varchar,((select top 1 maxSt from serviceweb.dbo.servicewebdbowbarholddistrictname where locat=CRLOCAT)-sum(cnt))) maxMinusAmount
,null totalSale,	null d000To015,	null d016To030,	null d031To060,	null d061To090,	null d091To120,	null d121To150,	null d151To180
from #tempTBForA main
group by PROVDES,CRLOCAT
union
select PROVDES,'����'+PROVDES CRLOCAT,''MODEL,''BAAB,''COLOR,sum(cnt)cnt,null maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES),0)old
,null maxMinusAmount
,null totalSale,	null d000To015,	null d016To030,	null d031To060,	null d061To090,	null d091To120,	null d121To150,	null d151To180
from #tempTBForA main
group by PROVDES 
union
select '����������' PROVDES, '' CRLOCAT,'' MODEL,''BAAB,''COLOR,sum(cnt)cnt,null maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N'),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O'),0)old
,null maxMinusAmount
,null totalSale,	null d000To015,	null d016To030,	null d031To060,	null d061To090,	null d091To120,	null d121To150,	null d151To180
from #tempTBForA main
order by PROVDES,CRLOCAT,MODEL

/*ö������ʹ��� ��������ʵ�ͤ �¡����Ң�*/
select pv.PROVDES,apm.AUMPDES,ss.CRLOCAT,ss.MODEL,ss.BAAB,ss.totalSale,ss.d000To015,ss.d016To030,ss.d031To060,ss.d061To090,ss.d091To120,ss.d121To150,ss.d151To180
from #SaleStat ss left join #tempTBForA st on ss.CRLOCAT=st.CRLOCAT and ss.MODEL=st.MODEL and ss.BAAB=st.BAAB 
left join serviceweb.dbo.wb_branches bz on ss.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
where st.MODEL is null
order by pv.PROVDES,apm.AUMPDES,CRLOCAT desc

/*ö������ʹ��� ��������ʵ�ͤ ��ṡ��������*/
select ss.MODEL,ss.BAAB,pv.PROVDES,apm.AUMPDES,sum(ss.totalSale) totalSale,sum(ss.d000To015) d000To015,sum(ss.d016To030) d016To030,sum(ss.d031To060) d031To060,sum(ss.d061To090) d061To090,sum(ss.d091To120) d091To120,sum(ss.d121To150) d121To150,sum(ss.d151To180) d151To180
from #SaleStat ss left join #tempTBForA st on ss.CRLOCAT=st.CRLOCAT and ss.MODEL=st.MODEL and ss.BAAB=st.BAAB
left join serviceweb.dbo.wb_branches bz on ss.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
where st.MODEL is null
group by pv.PROVDES,apm.AUMPDES,ss.MODEL,ss.BAAB
union
select ss.MODEL,ss.BAAB,'����'+ss.MODEL+'('+ss.BAAB+')' PROVDES,''AUMPDES,sum(ss.totalSale) totalSale,sum(ss.d000To015) d000To015,sum(ss.d016To030) d016To030,sum(ss.d031To060) d031To060,sum(ss.d061To090) d061To090,sum(ss.d091To120) d091To120,sum(ss.d121To150) d121To150,sum(ss.d151To180) d151To180
from #SaleStat ss left join #tempTBForA st on ss.CRLOCAT=st.CRLOCAT and ss.MODEL=st.MODEL and ss.BAAB=st.BAAB
left join serviceweb.dbo.wb_branches bz on ss.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
where st.MODEL is null
group by ss.MODEL,ss.BAAB

order by ss.MODEL,ss.BAAB,pv.PROVDES,apm.AUMPDES

IF OBJECT_ID('tempdb..#stockAndSaleStat') is not null DROP TABLE #stockAndSaleStat
select allu.PROVDES,apm.AUMPDES,allu.CRLOCAT,allu.MODEL,allu.BAAB,allu.cntNew,allu.totalSale,allu.d000To015,allu.d016To030,allu.d031To060,allu.d061To090,allu.d091To120,allu.d121To150,allu.d151To180
into #stockAndSaleStat
from(
	/*ö������ʵ�ͤ*/
	select PROVDES,CRLOCAT,MODEL,BAAB
	,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB),0)cntNew
	,isnull((select totalSale from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) totalSale
	,isnull((select d000To015 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d000To015
	,isnull((select d016To030 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d016To030
	,isnull((select d031To060 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d031To060
	,isnull((select d061To090 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d061To090
	,isnull((select d091To120 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d091To120
	,isnull((select d121To150 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d121To150
	,isnull((select d151To180 from #SaleStat a where main.CRLOCAT=a.CRLOCAT and main.MODEL=a.MODEL and main.BAAB=a.BAAB),0) d151To180
	from #tempTBForA main where main.STAT='N' group by PROVDES,CRLOCAT,MODEL,BAAB
	union 
	/*ö������ʹ��� ��������ʵ�ͤ �¡����Ң�*/
	select pv.PROVDES,ss.CRLOCAT,ss.MODEL,ss.BAAB,0 cntNew
	,ss.totalSale,ss.d000To015,ss.d016To030,ss.d031To060,ss.d061To090,ss.d091To120,ss.d121To150,ss.d151To180
	from #SaleStat ss left join #tempTBForA st on ss.CRLOCAT=st.CRLOCAT and ss.MODEL=st.MODEL and ss.BAAB=st.BAAB 
	left join serviceweb.dbo.wb_branches bz on ss.CRLOCAT=bz.locatcd
	left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
	left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
	where st.MODEL is null 
)allu
left join serviceweb.dbo.wb_branches bz on allu.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD

order by allu.PROVDES,apm.AUMPDES,allu.CRLOCAT desc

select MODEL MODEL,BAAB BAAB,sum(cntNew) cntNew,sum(totalSale) totalSale,sum(d000To015) d000To015,sum(d016To030) d016To030,sum(d031To060) d031To060,sum(d061To090) d061To090,sum(d091To120) d091To120,sum(d121To150) d121To150,sum(d151To180) d151To180
,serviceweb.dbo.nextTrendOfSale(1,sum(d091To120),2,sum(d061To090),3,sum(d031To060),4,sum(d000To015)+sum(d016To030)) trend
,sum(cntNew)-serviceweb.dbo.nextTrendOfSale(1,sum(d091To120),2,sum(d061To090),3,sum(d031To060),4,sum(d000To015)+sum(d016To030)) situ
,case when serviceweb.dbo.nextTrendOfSale(1,sum(d091To120),2,sum(d061To090),3,sum(d031To060),4,sum(d000To015)+sum(d016To030))=0 then 0 
	else sum(cntNew)/serviceweb.dbo.nextTrendOfSale(1,sum(d091To120),2,sum(d061To090),3,sum(d031To060),4,sum(d000To015)+sum(d016To030))
end overstock
from #stockAndSaleStat
group by MODEL,BAAB
order by MODEL,BAAB

select MODEL,BAAB,sum(cntNew) cntNew,sum(totalSale) totalSale
		,sum(d000To015) d000To015
		,sum(d016To030) d016To030
		,sum(d031To060) d031To060
		,sum(d061To090) d061To090
		,sum(d091To120) d091To120
		,sum(d121To150) d121To150
		,sum(d151To180) d151To180
		,serviceweb.dbo.nextTrendOfSale(
		1,sum(d091To120)
		,2,sum(d061To090)
		,3,sum(d031To060)
		,4,sum(d000To015)+sum(d016To030)
		) trend
from #stockAndSaleStat group by MODEL,BAAB order by MODEL,BAAB

select PROVDES PROVDES,MODEL MODEL,BAAB BAAB,sum(cntNew) cntNew,sum(totalSale) totalSale,sum(d000To015) d000To015,sum(d016To030) d016To030,sum(d031To060) d031To060,sum(d061To090) d061To090,sum(d091To120) d091To120,sum(d121To150) d121To150,sum(d151To180) d151To180 
,serviceweb.dbo.nextTrendOfSale(
		1,sum(d091To120)
		,2,sum(d061To090)
		,3,sum(d031To060)
		,4,sum(d000To015)+sum(d016To030)
		) trend
from #stockAndSaleStat group by PROVDES,MODEL,BAAB order by PROVDES,MODEL,BAAB 

select PROVDES PROVDES,AUMPDES AUMPDES,MODEL MODEL,BAAB BAAB,sum(cntNew) cntNew,sum(totalSale) totalSale,sum(d000To015) d000To015,sum(d016To030) d016To030,sum(d031To060) d031To060,sum(d061To090) d061To090,sum(d091To120) d091To120,sum(d121To150) d121To150,sum(d151To180) d151To180 
,serviceweb.dbo.nextTrendOfSale(1,sum(d091To120),2,sum(d061To090),3,sum(d031To060),4,sum(d000To015)+sum(d016To030)) trend
from #stockAndSaleStat group by PROVDES,AUMPDES,MODEL,BAAB order by PROVDES,AUMPDES,MODEL,BAAB

select PROVDES,AUMPDES,CRLOCAT,MODEL,BAAB,cntNew,totalSale,d000To015,d016To030,d031To060,d061To090,d091To120,d121To150,d151To180 
,serviceweb.dbo.nextTrendOfSale(1,d091To120,2,d061To090,3,d031To060,4,d000To015+d016To030) trend
from #stockAndSaleStat

