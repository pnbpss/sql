use HIINCOME;
--select top 5 * from CHQTRAN;
select 
--sum(PAYAMT) sumIncome
CONVERT(varchar, CAST(sum(PAYAMT) AS money), 1) sumIncome
,month(TMBILDT) as [month],year(TMBILDT) as [year]
from CHQTRAN
where 
--TMBILDT = '2014/04/19'
--TMBILDT between '2014/01/01' and '2014/12/31'
year(TMBILDT) = 2012
and FLAG<>'C'
group by month(TMBILDT),year(TMBILDT)
order by year(TMBILDT) desc, month(TMBILDT) desc