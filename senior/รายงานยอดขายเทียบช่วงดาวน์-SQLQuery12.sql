declare @ddiff int;
IF OBJECT_ID('tempdb..#TemporaryTb') is not null DROP TABLE #TemporaryTb
IF OBJECT_ID('tempdb..#rawData') is not null DROP TABLE #rawData
set @ddiff=365;

select * into #rawData
from (
select ar.TOTDWN,i.MODEL,i.BAAB,pv.PROVDES,apm.AUMPDES,ar.LOCAT,DATEDIFF(month,ar.SDATE,getdate())*1.00 dm
	,case 
		when ar.TOTDWN between 0 and 5000 then 'd00000_05000'
		when ar.TOTDWN between 5001 and 7500 then 'd05001_07500'
		when ar.TOTDWN between 7501 and 10000 then 'd07501_10000'
		when ar.TOTDWN between 10001 and 15000 then 'd10001_15000'
		when ar.TOTDWN between 15001 and 20000 then 'd15001_20000'			
		when ar.TOTDWN between 20001 and 25000 then 'd20001_25000'
		when ar.TOTDWN between 25001 and 30000 then 'd25001_30000'
		when ar.TOTDWN between 30001 and 35000 then 'd30001_35000'
		when ar.TOTDWN between 35001 and 40000 then 'd35001_40000'
		when ar.TOTDWN > 40000 then 'd40000_UP'
	end dwnRange
	from wb_armast ar inner join wb_invtran i on ar.STRNO=i.STRNO and ar.CONTNO=i.CONTNO 
	left join serviceweb.dbo.wb_branches bz on ar.LOCAT=bz.locatcd
	left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
	left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD		
	where datediff(day,ar.SDATE,GETDATE())<@ddiff and i.STAT='N' and ar.TSALE='H'
	and ar.LOCAT not in ('OFF��','OFF��','OFF��')
	union
	select ar.TOTDWN,i.MODEL,i.BAAB,pv.PROVDES,apm.AUMPDES,ar.LOCAT,DATEDIFF(month,ar.SDATE,getdate())*1.00 dm
	,case 
		when ar.TOTDWN between 0 and 5000 then 'd00000_05000'
		when ar.TOTDWN between 5001 and 7500 then 'd05001_07500'
		when ar.TOTDWN between 7501 and 10000 then 'd07501_10000'
		when ar.TOTDWN between 10001 and 15000 then 'd10001_15000'
		when ar.TOTDWN between 15001 and 20000 then 'd15001_20000'			
		when ar.TOTDWN between 20001 and 25000 then 'd20001_25000'
		when ar.TOTDWN between 25001 and 30000 then 'd25001_30000'
		when ar.TOTDWN between 30001 and 35000 then 'd30001_35000'
		when ar.TOTDWN between 35001 and 40000 then 'd35001_40000'
		when ar.TOTDWN > 40000 then 'd40000_UP'
	end dwnRange
	from wb_harmast ar inner join wb_hinvtran i on ar.STRNO=i.STRNO and ar.CONTNO=i.CONTNO 
	left join serviceweb.dbo.wb_branches bz on ar.LOCAT=bz.locatcd
	left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
	left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
	where datediff(day,ar.SDATE,GETDATE())<@ddiff and i.STAT='N' and ar.TSALE='H'
	and ar.LOCAT not in ('OFF��','OFF��','OFF��')
)allu

select MODEL model,BAAB [type],dwnRange,PROVDES prov,AUMPDES amp,LOCAT locat,cast(avg(dm) as decimal(18,2)) avgdm,COUNT(*)cnt,cast(isnull(STDEV(dm),0.00) as decimal(18,2)) stdevm
into #TemporaryTb from #rawData group by MODEL,BAAB,dwnRange,PROVDES,AUMPDES,LOCAT


select prov,amp,model,[type],isnull(d00000_05000,0)d00000_05000,isnull(d05001_07500,0)d05001_07500,isnull(d07501_10000,0)d07501_10000
,isnull(d10001_15000,0)d10001_15000,isnull(d15001_20000,0)d15001_20000,isnull(d20001_25000,0)d20001_25000,isnull(d25001_30000,0)d25001_30000
,isnull(d30001_35000,0)d30001_35000,isnull(d35001_40000,0)d35001_40000,isnull(d40000_UP,0)d40000_UP,aggr.sumAll
from #TemporaryTb t 
pivot (
	sum(cnt) for dwnRange in ([d00000_05000],[d05001_07500],[d07501_10000],[d10001_15000],[d15001_20000],[d20001_25000],[d25001_30000],[d30001_35000],[d35001_40000],[d40000_UP])
) p
cross apply (select(isnull(d00000_05000,0)+isnull(d05001_07500,0)+isnull(d07501_10000,0)+isnull(d10001_15000,0)+isnull(d15001_20000,0)+isnull(d20001_25000,0)+isnull(d25001_30000,0)+isnull(d30001_35000,0)+isnull(d35001_40000,0)+isnull(d40000_UP,0))sumAll)aggr
order by p.model,p.[type],p.prov,p.amp


--select * from #TemporaryTb; select * from #rawData