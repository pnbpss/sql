use HIINCOME;
IF OBJECT_ID('tempdb..#cntByLocat') is not null DROP TABLE #cntByLocat
IF OBJECT_ID('tempdb..##TemporaryTb') is not null DROP TABLE ##TemporaryTb
declare @specifiedDate datetime;
set @specifiedDate = '2016/10/04';
exec sp_executesql N'
select * 
into ##TemporaryTb
from (
	SELECT V.RECVNO,V.RECVDT,V.INVNO,V.INVDT,V.TAXNO,V.TAXDT,T.GCODE, T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB
	,T.COLOR,T.CC, T.STRNO
	
	,T.ENGNO,T.KEYNO,T.STAT,T.RVLOCAT,T.CRLOCAT,T.MILERT, T.NETCOST,T.CRVAT,T.TOTCOST,T.TADDCOST,T.BONUS FROM INVINVO V,INVTRAN T WHERE (V.RECVNO=T.RECVNO) AND (V.LOCAT=T.RVLOCAT) and ''A''=@P1 AND (T.SDATE>@P2 OR T.SDATE IS NULL)   AND not exists(select strno  from (select strno,ydate as movedt,ylocat as moveto,sdate from arhold  union select strno,ydate as movedt,ylocat as moveto,sdate from archag) as c where c.strno = t.strno  and c.movedt >= v.recvdt and @P2>=c.sdate and @P2<c.movedt)  AND (T.TYPE LIKE @P3) AND (T.MODEL LIKE @P4) AND (T.BAAB LIKE @P5) AND (T.COLOR LIKE @P6)  AND (T.RVLOCAT LIKE @P7)  AND (T.STAT LIKE @P8) AND (V.RECVDT<= @P9) GROUP BY t.strno,t.recvno,v.recvdt,t.rvlocat,t.totcost,T.TADDCOST,T.BONUS,t.crvat,t.netcost,t.milert, t.crlocat,t.stat,t.keyno,t.engno,t.cc,t.color,T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB, V.INVDT,V.TAXNO
	,V.TAXDT
	,T.GCODE,V.INVNO,V.INVDT,V.RECVNO,V.RECVDT HAVING (@P10  < (select  min(b.movedt) from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as b where t.strno = b.strno and b.movedt >= v.recvdt) OR t.strno  not  in (select strno  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = t.strno and c.movedt >= v.recvdt)) UNION SELECT V.RECVNO,V.RECVDT,V.INVNO,V.INVDT,V.TAXNO,V.TAXDT
	,T.GCODE, T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB,T.COLOR,T.CC, T.STRNO
	
	,T.ENGNO,T.KEYNO,T.STAT,T.RVLOCAT,T.CRLOCAT,T.MILERT, T.NETCOST,T.CRVAT,T.TOTCOST,T.TADDCOST,T.BONUS FROM INVINVO V,INVTRAN T,(select strno,movedt,moveto,movseq from invmovt       union select strno,ydate as movedt,ylocat as moveto,10000 as movseq from arhold       union select strno,ydate as movedt,ylocat as moveto,10000 as movseq from archag) as E WHERE (V.RECVNO=T.RECVNO) AND (V.LOCAT=T.RVLOCAT) AND (T.STRNO = E.STRNO) and ''A''=@P11 AND (T.SDATE>@P12 OR T.SDATE IS NULL)   AND not exists(select strno  from (select strno,ydate as movedt,ylocat as moveto,sdate from arhold  union select strno,ydate as movedt,ylocat as moveto,sdate from archag) as c where c.strno = t.strno  and c.movedt >= v.recvdt and @P2>=c.sdate and @P2<c.movedt)  AND (T.TYPE LIKE @P13) AND (T.MODEL LIKE @P14) AND (T.BAAB LIKE @P15) AND (T.COLOR LIKE @P16)  AND (E.MOVETO LIKE @P17)  AND (T.STAT LIKE @P18)   and (e.movseq=(select max(movseq) from invmovt where strno=e.strno and movedt<=@P2) or (e.movseq=0) or ((e.movseq=10000) and e.strno not in (select strno from invmovt where strno=e.strno and e.movedt=movedt))  ) AND (E.MOVEDT<=@P19) and (v.recvdt<=e.movedt) GROUP BY t.strno,t.recvno,v.recvdt,t.rvlocat,t.totcost,T.TADDCOST,T.BONUS,t.crvat,t.netcost,t.milert, t.crlocat,t.stat,t.keyno,t.engno,t.cc,t.color,T.MOVENO,T.MOVEDT,T.TYPE,T.MODEL,T.BAAB, 
	V.INVDT,V.TAXNO,V.TAXDT,T.GCODE,V.INVNO,V.INVDT,V.RECVNO,V.RECVDT,e.strno,e.movedt HAVING (@P20  < (select  min(c.movedt)  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = e.strno and c.movedt > e.movedt) OR e.strno  not  in (select strno  from (select strno,movedt,moveto from invmovt       union select strno,ydate as movedt,ylocat as moveto from arhold       union select strno,ydate as movedt,ylocat as moveto from archag) as c where c.strno = e.strno and c.movedt > e.movedt)) 
)allu
'
,N'@P1 varchar(1),@P2 datetime,@P3 varchar(1),@P4 varchar(1),@P5 varchar(1),@P6 varchar(1),@P7 varchar(1),@P8 varchar(1),@P9 datetime,@P10 datetime,@P11 varchar(1),@P12 datetime,@P13 varchar(1),@P14 varchar(1),@P15 varchar(1),@P16 varchar(1),@P17 varchar(1),@P18 varchar(1),@P19 datetime,@P20 datetime'
,'A',@specifiedDate,'%','%','%','%','%','%',@specifiedDate,@specifiedDate,'A',@specifiedDate,'%','%','%','%','%','%',@specifiedDate,@specifiedDate
--select * from ##TemporaryTb
update ##TemporaryTb set COLOR = serviceweb.dbo.fnRemoveNonNumericCharactersForColor(COLOR)

/*
--�ʹ������ͻ�Ш��ѹ��ṡ����Ң�
IF OBJECT_ID('tempdb..##remainStock1') is not null DROP TABLE ##remainStock1
select pv.PROVDES,apm.AUMPDES,byLocat.CRLOCAT,count(*)cnt,maxstk.maxSt,cntOldNew.N,cntOldNew.O
into ##remainStock1
from ##TemporaryTb byLocat
left join serviceweb.dbo.wb_branches bz on byLocat.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
left join serviceweb.dbo.servicewebdbowbarholddistrictname maxstk on byLocat.CRLOCAT=maxstk.locat
left join
(
	select CRLOCAT,isnull([O],0)[O],isnull([N],0)[N]
		from (
			select CRLOCAT,STAT,count(*)cnt
			from ##TemporaryTb	
			group by CRLOCAT,STAT
		) t
		pivot
		(
			sum(cnt) for STAT in ([O],[N])
		) p
)cntOldNew on byLocat.CRLOCAT=cntOldNew.CRLOCAT
where 1=1
group by pv.PROVDES,apm.AUMPDES,byLocat.CRLOCAT,maxstk.maxSt,cntOldNew.N,cntOldNew.O

select allu.PROVDES,allu.AUMPDES,allu.CRLOCAT,allu.cnt,allu.maxSt,allu.N,allu.O
from (
	select PROVDES,AUMPDES,CRLOCAT,cnt,maxSt,N,O from ##remainStock1 union
	select PROVDES,AUMPDES,'����'+AUMPDES CRLOCAT,sum(cnt)cnt,sum(maxSt)maxSt,sum(N)N,sum(O)O from ##remainStock1 group by PROVDES,AUMPDES union
	select PROVDES,'����'+PROVDES AUMPDES,''CRLOCAT,sum(cnt)cnt,sum(maxSt)maxSt,sum(N)N,sum(O)O from ##remainStock1 group by PROVDES union
	select '����������'PROVDES,''AUMPDES,''CRLOCAT,sum(cnt)cnt,sum(maxSt)maxSt,sum(N)N,sum(O)O from ##remainStock1
) allu 
order by PROVDES,AUMPDES,CRLOCAT
*/


--�ʹö������ͻ�Ш��ѹ �¡���⫹�ѧ��Ѵ
IF OBJECT_ID('tempdb..#tempTBForA') is not null DROP TABLE #tempTBForA
select pv.PROVDES,CRLOCAT,MODEL,BAAB,COLOR,count(*)cnt,null maxStock,STAT
into #tempTBForA
from ##TemporaryTb byLocat
left join serviceweb.dbo.wb_branches bz on byLocat.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
--where pv.PROVDES in ('��ѧ','�ѧ��','����ɮ��ҹ�')
group by pv.PROVDES,CRLOCAT,MODEL,BAAB,COLOR,STAT

--select * from #tempTBForA
select PROVDES,CRLOCAT,MODEL,BAAB,COLOR,cnt,null maxStock 
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB and a.COLOR=main.COLOR),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT and a.MODEL=main.MODEL and a.BAAB=main.BAAB and a.COLOR=main.COLOR),0)old
from #tempTBForA main
union 
select PROVDES,CRLOCAT,'����'+CRLOCAT MODEL,''BAAB,''COLOR,sum(cnt)cnt,(select top 1 maxSt from serviceweb.dbo.servicewebdbowbarholddistrictname where locat=CRLOCAT) maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES and a.CRLOCAT=main.CRLOCAT),0)old
from #tempTBForA main
group by PROVDES,CRLOCAT
union
select PROVDES,'����'+PROVDES CRLOCAT,''MODEL,''BAAB,''COLOR,sum(cnt)cnt,null maxStock
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='N' and a.PROVDES=main.PROVDES),0)new
,isnull((select sum(cnt) from #tempTBForA a where a.STAT='O' and a.PROVDES=main.PROVDES),0)old
from #tempTBForA main
group by PROVDES 
order by PROVDES,CRLOCAT,MODEL

/*
--�ʹö������ͻ�Ш��ѹ��ṡ����Ң� ��� GCODE
IF OBJECT_ID('tempdb..#cntByLocat') is not null DROP TABLE #cntByLocat
select CRLOCAT,count(*)cnt
into #cntByLocat
from ##TemporaryTb
group by CRLOCAT

select pv.PROVDES,byLocat.CRLOCAT,cnt,byLocat_STAT.[O],byLocat_STAT.[N]
,byLocatGCODE.[C_0],	byLocatGCODE.[C_025],	byLocatGCODE.[C_05],	byLocatGCODE.[C_081],	byLocatGCODE.[C_09],	byLocatGCODE.[C_15],	byLocatGCODE.[C_19],	byLocatGCODE.[C_20],	byLocatGCODE.[C_22],	byLocatGCODE.[C_24],	byLocatGCODE.[C_01],	byLocatGCODE.[C_03],	byLocatGCODE.[C_032],	byLocatGCODE.[C_033],	byLocatGCODE.[C_17],	byLocatGCODE.[C_26],	byLocatGCODE.[C_30],	byLocatGCODE.[C_32],	byLocatGCODE.[C_37],	byLocatGCODE.[C_93],	byLocatGCODE.[C_023],	byLocatGCODE.[C_04],	byLocatGCODE.[C_27],	byLocatGCODE.[C_39],	byLocatGCODE.[C_024],	byLocatGCODE.[C_051],	byLocatGCODE.[C_072],	byLocatGCODE.[C_083],	byLocatGCODE.[C_089],	byLocatGCODE.[C_16],	byLocatGCODE.[C_31],	byLocatGCODE.[C_72],	byLocatGCODE.[C_034],	byLocatGCODE.[C_06],	byLocatGCODE.[C_071],	byLocatGCODE.[C_11],	byLocatGCODE.[C_13],	byLocatGCODE.[C_28],	byLocatGCODE.[C_33],	byLocatGCODE.[C_91],	byLocatGCODE.[C_96],	byLocatGCODE.[C_021],	byLocatGCODE.[C_091],	byLocatGCODE.[C_18],	byLocatGCODE.[C_38],	byLocatGCODE.[C_010],	byLocatGCODE.[C_011],	byLocatGCODE.[C_02],	byLocatGCODE.[C_052],	byLocatGCODE.[C_07],	byLocatGCODE.[C_10],	byLocatGCODE.[C_12],	byLocatGCODE.[C_14],	byLocatGCODE.[C_21],	byLocatGCODE.[C_90],	byLocatGCODE.[C_92],	byLocatGCODE.[C_94],	byLocatGCODE.[C_022],	byLocatGCODE.[C_08],	byLocatGCODE.[C_082],	byLocatGCODE.[C_25],	byLocatGCODE.[C_29]
from #cntByLocat byLocat
	left join
	(
		select CRLOCAT,isnull([O],0)[O],isnull([N],0)[N]
		from (
			select CRLOCAT,STAT,count(*)cnt
			from ##TemporaryTb	
			group by CRLOCAT,STAT
		) t
		pivot
		(
			sum(cnt) for STAT in ([O],[N])
		) p
	)byLocat_STAT on byLocat.CRLOCAT=byLocat_STAT.CRLOCAT
	left join
	(
		select CRLOCAT,isnull([C_0],0)[C_0],isnull([C_025],0)[C_025],isnull([C_05],0)[C_05],isnull([C_081],0)[C_081],isnull([C_09],0)[C_09],isnull([C_15],0)[C_15],isnull([C_19],0)[C_19],isnull([C_20],0)[C_20],isnull([C_22],0)[C_22],isnull([C_24],0)[C_24],isnull([C_01],0)[C_01],isnull([C_03],0)[C_03],isnull([C_032],0)[C_032],isnull([C_033],0)[C_033],isnull([C_17],0)[C_17],isnull([C_26],0)[C_26],isnull([C_30],0)[C_30],isnull([C_32],0)[C_32],isnull([C_37],0)[C_37],isnull([C_93],0)[C_93],isnull([C_023],0)[C_023],isnull([C_04],0)[C_04],isnull([C_27],0)[C_27],isnull([C_39],0)[C_39],isnull([C_024],0)[C_024],isnull([C_051],0)[C_051],isnull([C_072],0)[C_072],isnull([C_083],0)[C_083],isnull([C_089],0)[C_089],isnull([C_16],0)[C_16],isnull([C_31],0)[C_31],isnull([C_72],0)[C_72],isnull([C_034],0)[C_034],isnull([C_06],0)[C_06],isnull([C_071],0)[C_071],isnull([C_11],0)[C_11],isnull([C_13],0)[C_13],isnull([C_28],0)[C_28],isnull([C_33],0)[C_33],isnull([C_91],0)[C_91],isnull([C_96],0)[C_96],isnull([C_021],0)[C_021],isnull([C_091],0)[C_091],isnull([C_18],0)[C_18],isnull([C_38],0)[C_38],isnull([C_010],0)[C_010],isnull([C_011],0)[C_011],isnull([C_02],0)[C_02],isnull([C_052],0)[C_052],isnull([C_07],0)[C_07],isnull([C_10],0)[C_10],isnull([C_12],0)[C_12],isnull([C_14],0)[C_14],isnull([C_21],0)[C_21],isnull([C_90],0)[C_90],isnull([C_92],0)[C_92],isnull([C_94],0)[C_94],isnull([C_022],0)[C_022],isnull([C_08],0)[C_08],isnull([C_082],0)[C_082],isnull([C_25],0)[C_25],isnull([C_29],0)[C_29]
		from (
			select CRLOCAT,'C_'+GCODE GCODE,count(*)cnt
			from ##TemporaryTb
			
			group by CRLOCAT,'C_'+GCODE
		) t
		pivot
		(
			sum(cnt) for GCODE in ([C_0],[C_025],[C_05],[C_081],[C_09],[C_15],[C_19],[C_20],[C_22],[C_24],[C_01],[C_03],[C_032],[C_033],[C_17],[C_26],[C_30],[C_32],[C_37],[C_93],[C_023],[C_04],[C_27],[C_39],[C_024],[C_051],[C_072],[C_083],[C_089],[C_16],[C_31],[C_72],[C_034],[C_06],[C_071],[C_11],[C_13],[C_28],[C_33],[C_91],[C_96],[C_021],[C_091],[C_18],[C_38],[C_010],[C_011],[C_02],[C_052],[C_07],[C_10],[C_12],[C_14],[C_21],[C_90],[C_92],[C_94],[C_022],[C_08],[C_082],[C_25],[C_29])
		) p	
	)byLocatGCODE on byLocat.CRLOCAT=byLocatGCODE.CRLOCAT
	left join serviceweb.dbo.wb_branches bz on byLocat.CRLOCAT=bz.locatcd
	left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
	left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
order by pv.PROVDES,byLocat.CRLOCAT
*/

/*
--ʵ�ͤö������ͻ�Ш��ѹ����Ѻ�͡qc
IF OBJECT_ID('tempdb..#remainStock') is not null DROP TABLE #remainStock
select CRLOCAT,isnull([G02],0) [G02],isnull([G021],0) [G021],isnull([G022],0) [G022],isnull([G023],0) [G023],isnull([G024],0) [G024],isnull([G03],0) [G03],isnull([G06],0) [G06],isnull([G11],0) [G11],isnull([G14],0) [G14],isnull([G15],0) [G15],isnull([G16],0) [G16],isnull([G21],0) [G21],isnull([G22],0) [G22],isnull([G24],0) [G24],isnull([G25],0) [G25],isnull([G26],0) [G26],isnull([G27],0) [G27],isnull([G28],0) [G28],isnull([G29],0) [G29],isnull([G30],0) [G30],isnull([G31],0) [G31],isnull([G32],0) [G32],isnull([G38],0) [G38],aggr.sum02x,aggr.sumAll
into #remainStock
from (
	select CRLOCAT,'G'+GCODE GCODE,count(*)cnt
	from ##TemporaryTb	
	group by CRLOCAT,'G'+GCODE
) t
pivot
(
	sum(cnt) for GCODE in ([G02],	[G021],	[G022],	[G023],	[G024],	[G03],	[G06],	[G11],	[G14],	[G15],	[G16],	[G21],	[G22],	[G24],	[G25],	[G26],	[G27],	[G28],	[G29],	[G30],	[G31],	[G32],	[G38])
) p	
cross apply(
				select 
					(isnull([G02],0)+isnull([G021],0)+isnull([G022],0)+isnull([G023],0)+isnull([G024],0)) sum02x
					,(isnull([G02],0)+isnull([G021],0)+isnull([G022],0)+isnull([G023],0)+isnull([G024],0)+isnull([G03],0)+isnull([G06],0)+isnull([G11],0)+isnull([G14],0)+isnull([G15],0)+isnull([G16],0)+isnull([G21],0)+isnull([G22],0)+isnull([G24],0)+isnull([G25],0)+isnull([G26],0)+isnull([G27],0)+isnull([G28],0)+isnull([G29],0)+isnull([G30],0)+isnull([G31],0)+isnull([G32],0)+isnull([G38],0))sumAll
				
		   ) aggr
select pv.PROVDES,apm.AUMPDES,main.CRLOCAT,main.G02,	main.G021,	main.G022,	main.G023,	main.G024,	main.G03,	main.G06,	main.G11,	main.G14,	main.G15,	main.G16,	main.G21,	main.G22,	main.G24,	main.G25,	main.G26,	main.G27,	main.G28,	main.G29,	main.G30,	main.G31,	main.G32,	main.G38,	main.sum02x,main.sumAll
from #remainStock main
left join serviceweb.dbo.wb_branches bz on main.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD

union
select pv.PROVDES,apm.AUMPDES,'����'+apm.AUMPDES CRLOCAT,sum(G02) G02,	sum(G021) G021,	sum(G022) G022,	sum(G023) G023,	sum(G024) G024,	sum(G03) G03,	sum(G06) G06,	sum(G11) G11,	sum(G14) G14,	sum(G15) G15,	sum(G16) G16,	sum(G21) G21,	sum(G22) G22,	sum(G24) G24,	sum(G25) G25,	sum(G26) G26,	sum(G27) G27,	sum(G28) G28,	sum(G29) G29,	sum(G30) G30,	sum(G31) G31,	sum(G32) G32,	sum(G38) G38,	sum(sum02x) sum02x,sum(sumAll) sumAll
from #remainStock main
left join serviceweb.dbo.wb_branches bz on main.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
group by pv.PROVDES,apm.AUMPDES

union
select pv.PROVDES,'����'+pv.PROVDES AUMPDES,''CRLOCAT,sum(G02) G02,	sum(G021) G021,	sum(G022) G022,	sum(G023) G023,	sum(G024) G024,	sum(G03) G03,	sum(G06) G06,	sum(G11) G11,	sum(G14) G14,	sum(G15) G15,	sum(G16) G16,	sum(G21) G21,	sum(G22) G22,	sum(G24) G24,	sum(G25) G25,	sum(G26) G26,	sum(G27) G27,	sum(G28) G28,	sum(G29) G29,	sum(G30) G30,	sum(G31) G31,	sum(G32) G32,	sum(G38) G38,	sum(sum02x) sum02x,sum(sumAll) sumAll
from #remainStock main
left join serviceweb.dbo.wb_branches bz on main.CRLOCAT=bz.locatcd
left join serviceweb.dbo.wb_ampr apm on bz.amphur=apm.AUMPCOD
left join serviceweb.dbo.wb_provinces pv on apm.PROVCOD=pv.PROVCOD
group by pv.PROVDES

union
select '����������'PROVDES,''AUMPDES,''CRLOCAT,sum(G02) G02,	sum(G021) G021,	sum(G022) G022,	sum(G023) G023,	sum(G024) G024,	sum(G03) G03,	sum(G06) G06,	sum(G11) G11,	sum(G14) G14,	sum(G15) G15,	sum(G16) G16,	sum(G21) G21,	sum(G22) G22,	sum(G24) G24,	sum(G25) G25,	sum(G26) G26,	sum(G27) G27,	sum(G28) G28,	sum(G29) G29,	sum(G30) G30,	sum(G31) G31,	sum(G32) G32,	sum(G38) G38,	sum(sum02x) sum02x,sum(sumAll) sumAll
from #remainStock main
order by pv.PROVDES,apm.AUMPDES,main.CRLOCAT
*/