use serviceweb;
/*
select SHORTL,LOCATCD,LOCATNM from HIINCOME.dbo.INVLOCAT where LOCATNM like '%��Ҵ%';
select MAX(CUSCOD) maxCuscodeCustMast from HIINCOME.dbo.CUSTMAST where CUSCOD like '�%'
select MAX(CUSCOD) maxCuscodeADDR from HIINCOME.dbo.CUSTADDR where CUSCOD like '�%'
--�������� maxCuscodeADDR > maxCuscodeCustMast ��� ź record � HIINCOME.dbo.CUSTADDR �ѹ��� cuscod �ҡ���� maxCuscodeCustMast � 
--delete from HIINCOME.dbo.CUSTADDR where CUSCOD='�HI-00002717'
*/

select 
m.SHORTL 
,aggr.maxMast
,aggr.maxAddr
,compare.result
,sqlstr.runit
from HIINCOME.dbo.INVLOCAT m
cross apply (select (select MAX(CUSCOD) maxCuscodeCustMast from HIINCOME.dbo.CUSTMAST where CUSCOD like m.SHORTL+'%') maxMast,(select MAX(CUSCOD) maxCuscodeADDR from HIINCOME.dbo.CUSTADDR where CUSCOD like m.SHORTL+'%') maxAddr)aggr
cross apply (select case when aggr.maxAddr<= aggr.maxMast then 'OK' else 'Error' end result)compare
cross apply (select case when compare.result!='OK' then ' delete from HIINCOME.dbo.CUSTADDR where CUSCOD='''+aggr.maxAddr+''';--ź�������ѹ�Թ�� ' else '--�����' end runit) sqlstr
where maxMast is not null
and compare.result!='OK'