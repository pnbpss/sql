use serviceweb;
set nocount on;
IF OBJECT_ID('tempdb..#incorrectArmast') IS NOT NULL  DROP TABLE #incorrectArmast;

--script ����Ѻ update armast ᷹��� util ���ǹ�ͧ �Ǵ��ҧ, ��ҧ�ҡ�Ǵ-�Ǵ ��� �ʹ��ҧ(�ҷ)
/*
--table ����Ѻ�� logs �ͧ������
--truncate table serviceweb.dbo.wb_updateArmastUtilLogs
create table serviceweb.dbo.wb_updateArmastUtilLogs(
	CONTNO varchar(max),PIP int,netPIP int,crPDFP int, crPDTP int,snPendingFromPeriod int,snPendingToPeriod int,snPendingPeriod int,
	snPendingBaht decimal(18,2),LPD datetime,LPIP int,currentperiod int,lastPaidInstall decimal(18,2),installmentPerPeriod decimal(18,2),
	EXP_PRD decimal(18,2),EXP_AMT decimal(18,2),EXP_FRM int,EXP_TO int,LPAYD datetime,LPAYA decimal(18,2),SDATE datetime,
	crRemainDebt decimal(18,2)
	,updateDateTime datetime
	,seq int identity(1,1) not null
)
*/

/*
truncate table serviceweb.dbo.ARMAST_TESTUTIL
insert into serviceweb.dbo.ARMAST_TESTUTIL select *  from HIINCOME.dbo.ARMAST 
create clustered index ARMAST_TESTUTILIDX  on ARMAST_TESTUTIL(CONTNO)
*/


create table #incorrectArmast(
	CONTNO varchar(max),PIP int,netPIP int,crPDFP int, crPDTP int,snPendingFromPeriod int,snPendingToPeriod int,snPendingPeriod int,
	snPendingBaht decimal(18,2),LPD datetime,LPIP int,currentperiod int,lastPaidInstall decimal(18,2),installmentPerPeriod decimal(18,2),
	EXP_PRD decimal(18,2),EXP_AMT decimal(18,2),EXP_FRM int,EXP_TO int,LPAYD datetime,LPAYA decimal(18,2),SDATE datetime,
	crRemainDebt decimal(18,2)
)

insert into #incorrectArmast
select va.CONTNO,va.pendingInstallmentPeriod PIP,netPendingPeriod netPIP
,aggr1.crPendingFromPeriod crPDFP
,aggr1.crPendingToPeriod crPDTP --[crPendingToPeriod] 
,snAggr.pendingFromPeriod snPendingFromPeriod
,snAggr.pendingToPeriod snPendingToPeriod
,snPendingPeriod.amountPendingPeriod snPendingPeriod
,snPendingBaht.amountPendingBaht snPendingBaht
,lastPaidDate LPD,va.lastPaidInstallmentPeriod LPIP,va.currentperiod,va.lastPaidInstall,va.installmentPerPeriod
,a.EXP_PRD,EXP_AMT,EXP_FRM,EXP_TO,LPAYD,LPAYA
,a.SDATE
,(SELECT SUM(DAMT-PAYMENT) FROM wb_arpay WHERE CONTNO=a.CONTNO and DDATE<=getdate()) crRemainDebt --�ʹ��ҧ	
from view_arpay2 va left join wb_armast a on va.CONTNO=a.CONTNO
cross apply (
				select
				case
					when isnull(va.currentperiod,0)=0 then 0
					when va.neverpaid='Y' then 1
					--when va.netPendingPeriod=0 then 0
					when va.netPendingPeriod=0 then va.currentperiod
					else va.currentperiod-(va.netPendingPeriod-1)
				end as crPendingFromPeriod
				,isnull(va.currentperiod,0) crPendingToPeriod
		) aggr1
cross apply(
		select case
					when va.lastPaidInstall<va.installmentPerPeriod then va.lastPaidInstallmentPeriod	
					when va.lastPaidInstall>va.installmentPerPeriod then null
					else aggr1.crPendingFromPeriod
				end snPendingFromPeriod
		) aggr2
cross apply(
			select 		
				case 
					when va.netPendingPeriod=0 and ISNULL(va.lastPaidInstall,0)<>0 then 0
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)=0 then 1
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)=va.installmentPerPeriod then (va.currentperiod-va.netPendingPeriod)+1
					when va.netPendingPeriod=0 and ISNULL(va.lastPaidInstall,0)<>0 and (va.lastPaidInstall/va.installmentPerPeriod=1) then 0
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)<>0 and (va.lastPaidInstall/va.installmentPerPeriod<1) then va.lastPaidInstallmentPeriod
					when va.netPendingPeriod=0 and ISNULL(va.currentperiod,0)=0 and isnull(CONVERT(varchar,va.lastPaidDate,111),'')='' then 0					
				end pendingFromPeriod
				,case 
					when va.netPendingPeriod=0 and ISNULL(va.lastPaidInstall,0)<>0 then 0
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)=0 then va.currentperiod
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)=va.installmentPerPeriod then va.currentperiod
					when va.netPendingPeriod=0 and ISNULL(va.lastPaidInstall,0)<>0 and (va.lastPaidInstall/va.installmentPerPeriod=1) then 0
					when va.netPendingPeriod>0 and ISNULL(va.lastPaidInstall,0)<>0 and (va.lastPaidInstall/va.installmentPerPeriod<1) then va.currentperiod
					when va.netPendingPeriod=0 and ISNULL(va.currentperiod,0)=0 and isnull(CONVERT(varchar,va.lastPaidDate,111),'')='' then 0					
				end pendingToPeriod
		)snAggr
cross apply (
		select case when snAggr.pendingFromPeriod=0 and snAggr.pendingToPeriod=0 then 0
					 else (snAggr.pendingToPeriod-snAggr.pendingFromPeriod)+1
				end amountPendingPeriod				
)snPendingPeriod
cross apply (
		select case when snAggr.pendingFromPeriod=va.lastPaidInstallmentPeriod then ((va.installmentPerPeriod*snPendingPeriod.amountPendingPeriod)-va.lastPaidInstall)
			else snPendingPeriod.amountPendingPeriod*va.installmentPerPeriod
		end amountPendingBaht
	)snPendingBaht
where 1=1
goto endscript

--and va.CONTNO in ('!FN-17030036','!FN-17020015','!FN-17010010')
--and disconnectedDays between 60 and 70
--and netPendingPeriod between 2 and 3
--and netPendingPeriod<>EXP_PRD
--select * from dc_loadedJobsForAssignToDebtCollector where contno='&HP-16060016'


declare @contno varchar(max),@PIP int,@netPIP int,@crPDFP int,@crPDTP int,@snPendingFromPeriod int, @snPendingToPeriod int,@snPendingPeriod int, @snPendingBaht decimal(18,2)
			,@LPD datetime ,@LPIP int
			,@currentperiod int,@lastPaidInstall decimal(18,2),@installmentPerPeriod decimal(18,2),@EXP_PRD  decimal(18,2),@EXP_AMT decimal(18,2),@EXP_FRM int,@EXP_TO int,@LPAYD datetime,@LPAYA  decimal(18,2)
			,@SDATE datetime,@crRemainDebt  decimal(18,2)
			
declare  updateDateCursor cursor FAST_FORWARD for 
			select 
			CONTNO
			,PIP,netPIP,crPDFP,crPDTP
			,snPendingFromPeriod
			,snPendingToPeriod
			,snPendingPeriod
			,snPendingBaht
			,LPD,LPIP
			,currentperiod,lastPaidInstall,installmentPerPeriod,EXP_PRD,EXP_AMT,EXP_FRM,EXP_TO,LPAYD,LPAYA,SDATE,crRemainDebt
			from #incorrectArmast where 1=1 --and not((snPendingBaht=EXP_AMT) or (snPendingPeriod=EXP_PRD)) 
			and (snPendingBaht<>EXP_AMT or snPendingPeriod<>EXP_PRD or snPendingFromPeriod<>EXP_FRM or snPendingToPeriod<>EXP_TO)
			--order by (snPendingPeriod-EXP_PRD) desc

open updateDateCursor
fetch next from updateDateCursor 
	into @contno,@PIP,@netPIP,@crPDFP,@crPDTP
			,@snPendingFromPeriod
			,@snPendingToPeriod
			,@snPendingPeriod
			,@snPendingBaht
			,@LPD,@LPIP
			,@currentperiod,@lastPaidInstall,@installmentPerPeriod,@EXP_PRD,@EXP_AMT,@EXP_FRM,@EXP_TO,@LPAYD,@LPAYA,@SDATE,@crRemainDebt
							
while @@FETCH_STATUS = 0 begin
	update 	HIINCOME.dbo.ARMAST 
		set EXP_FRM=@snPendingFromPeriod
			,EXP_TO=@snPendingToPeriod
			,EXP_PRD=@snPendingPeriod
			,EXP_AMT=@snPendingBaht
			
	where CONTNO=@contno;
	print @contno;
	
	insert into wb_updateArmastUtilLogs (
		CONTNO
			,PIP,netPIP,crPDFP,crPDTP
			,snPendingFromPeriod
			,snPendingToPeriod
			,snPendingPeriod
			,snPendingBaht
			,LPD,LPIP
			,currentperiod,lastPaidInstall,installmentPerPeriod,EXP_PRD,EXP_AMT,EXP_FRM,EXP_TO,LPAYD,LPAYA,SDATE,crRemainDebt
			,updateDateTime
		)
	values(
			@contno
			,@PIP,@netPIP,@crPDFP,@crPDTP
			,@snPendingFromPeriod
			,@snPendingToPeriod
			,@snPendingPeriod
			,@snPendingBaht
			,@LPD,@LPIP
			,@currentperiod,@lastPaidInstall,@installmentPerPeriod,@EXP_PRD,@EXP_AMT,@EXP_FRM,@EXP_TO,@LPAYD,@LPAYA,@SDATE,@crRemainDebt
			,GETDATE())	 
	 fetch next from updateDateCursor into @contno,@PIP,@netPIP,@crPDFP,@crPDTP
			,@snPendingFromPeriod
			,@snPendingToPeriod
			,@snPendingPeriod
			,@snPendingBaht
			,@LPD,@LPIP
			,@currentperiod,@lastPaidInstall,@installmentPerPeriod,@EXP_PRD,@EXP_AMT,@EXP_FRM,@EXP_TO,@LPAYD,@LPAYA,@SDATE,@crRemainDebt
end
close updateDateCursor;  
deallocate updateDateCursor; 

endscript:
--select * from wb_updateArmastUtilLogs where CONTNO='!HP-16030007'
--select * from hiincome.dbo.armast where CONTNO='!HP-16030007'