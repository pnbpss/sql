####################################
SELECT
    s.name as ColumnName
        ,sh.name+'.'+o.name AS ObjectName
        ,o.type_desc AS ObjectType
        ,CASE
             WHEN t.name IN ('char','varchar') THEN t.name+'('+CASE WHEN s.max_length<0 then 'MAX' ELSE CONVERT(varchar(10),s.max_length) END+')'
             WHEN t.name IN ('nvarchar','nchar') THEN t.name+'('+CASE WHEN s.max_length<0 then 'MAX' ELSE CONVERT(varchar(10),s.max_length/2) END+')'
            WHEN t.name IN ('numeric') THEN t.name+'('+CONVERT(varchar(10),s.precision)+','+CONVERT(varchar(10),s.scale)+')'
             ELSE t.name
         END AS DataType

        ,CASE
             WHEN s.is_nullable=1 THEN 'NULL'
            ELSE 'NOT NULL'
        END AS Nullable
        ,CASE
             WHEN ic.column_id IS NULL THEN ''
             ELSE ' identity('+ISNULL(CONVERT(varchar(10),ic.seed_value),'')+','+ISNULL(CONVERT(varchar(10),ic.increment_value),'')+')='+ISNULL(CONVERT(varchar(10),ic.last_value),'null')
         END
        +CASE
             WHEN sc.column_id IS NULL THEN ''
             ELSE ' computed('+ISNULL(sc.definition,'')+')'
         END
        +CASE
             WHEN cc.object_id IS NULL THEN ''
             ELSE ' check('+ISNULL(cc.definition,'')+')'
         END
            AS MiscInfo
    into #TJYNTableName
    FROM sys.columns                           s
        INNER JOIN sys.types                   t ON s.system_type_id=t.user_type_id and t.is_user_defined=0
        INNER JOIN sys.objects                 o ON s.object_id=o.object_id
        INNER JOIN sys.schemas                sh on o.schema_id=sh.schema_id
        LEFT OUTER JOIN sys.identity_columns  ic ON s.object_id=ic.object_id AND s.column_id=ic.column_id
        LEFT OUTER JOIN sys.computed_columns  sc ON s.object_id=sc.object_id AND s.column_id=sc.column_id
        LEFT OUTER JOIN sys.check_constraints cc ON s.object_id=cc.parent_object_id AND s.column_id=cc.parent_column_id
    ORDER BY sh.name+'.'+o.name,s.column_id
--drop table #TJYNTableName

--เอาผลที่ได้จาก query นี้ไป สร้างชุดคำสั่งใน excel เพื่อ insert เข้า #checkColumnOfLocat
select * from #TJYNTableName 
where 
(DataType like 'varchar(%' or DataType like 'char(%' or DataType like 'nvar(%')
--DataType like 'text%'
 and ObjectType='USER_TABLE';
/*
--ตัวอย่าง script ที่สร้างได้
insert into #checkColumnOfLocat select 'CONTNO' as columnName,'dbo.ACCPCON' as tableName,count([CONTNO]) as cntExist from HIINCOME.dbo.ACCPCON where [CONTNO] like '%YKพส3%';
--อันนี้ run ช้า
*/

create table #checkColumnOfLocat
(
	columnName varchar(max)
	,tableName varchar(max),
	 cntExist int
);
--delete from #checkColumnOfLocat
insert into #checkColumnOfLocat select 'CONTNO' as columnName,'TJYN.dbo.ACCPCON' as tableName,count([CONTNO]) as cntExist from TJYN.dbo.ACCPCON where [CONTNO] like '%YKตข3%';



--เอา ผลที่ได้จาก script ข้างล่างนี้ไปสร้าง ชุดคำสั่ง ด้วย excel ไปรันเพื่อลบ สาขาจริงๆ
select * from #checkColumnOfLocat order by cntExist desc


