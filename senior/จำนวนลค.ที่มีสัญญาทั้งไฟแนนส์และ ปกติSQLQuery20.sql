

select main.CUSCOD,main.cnt,f.cnt
from (
	select a.CUSCOD,count(*)cnt
	from ARMAST a inner join serviceweb.dbo.view_arpay2 va on a.CONTNO=va.CONTNO
	where a.SDATE>'20100101'
	and a.CONTSTAT not in (select contStatus from serviceweb.dbo.dc_noMoreDCContStatuses)
	group by a.CUSCOD
	having count(*)>1
) main left join(
	select a.CUSCOD,count(*)cnt
	from ARMAST a 
	inner join INVTRAN i on a.CONTNO=i.CONTNO and i.GCODE in ('05','051','052','053')
	inner join serviceweb.dbo.view_arpay2 va on a.CONTNO=va.CONTNO
	
	where a.SDATE>'20100101'
	and a.CONTSTAT not in (select contStatus from serviceweb.dbo.dc_noMoreDCContStatuses)
	group by a.CUSCOD
	having count(*)>1
) f on main.CUSCOD=f.CUSCOD
where f.CUSCOD is not null
and main.cnt!=f.cnt
--select top 5 * from ARMAST

select * from CONDPAY