/*
IF OBJECT_ID('tempdb.dbo.#aaa', 'U') IS NOT NULL DROP TABLE #aaa;
declare @startDate datetime,@endDate datetime;
declare @lookupTable table (locat varchar(max) collate Thai_CS_AS);
insert into @lookupTable select LOCATCD from serviceweb.dbo.wb_invlocat a where a.LOCATCD like 'YR��4'
--insert into @lookupTable select LOCATCD from serviceweb.dbo.wb_invlocat a where a.LOCATCD in ('YK��1','YK��2','YJ���')
set @startDate='2017/03/01';
set @endDate='2017/03/10';

select * into #aaa from (
SELECT CONTNO,LOCAT,BILLCOLL,TOTPRC,SMPAY,'H' AS TSALE,'' AS FINCOD FROM ARMAST  WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'C' AS TSALE,'' AS FINCOD FROM ARCRED WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'F' AS TSALE,FINCOD FROM ARFINC WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'A' AS TSALE,'' AS FINCOD FROM AR_INVOI  WHERE CONTNO IN 	(SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,OPTPTOT AS TOTPRC,SMPAY,'O' AS TSALE,'' AS FINCOD FROM AROPTMST WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,BILLCOLL,TOTPRC,SMPAY,'H' AS TSALE,'' AS FINCOD FROM HARMAST  WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'C' AS TSALE,'' AS FINCOD FROM HARCRED WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'F' AS TSALE,FINCOD FROM HARFINC WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'A' AS TSALE,'' AS FINCOD FROM HAR_INVO WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,OPTPTOT AS TOTPRC,SMPAY,'O' AS TSALE,'' AS FINCOD FROM HAROPMST WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV in (select locat from @lookupTable) AND TMBILDT BETWEEN @startDate AND @endDate) 
)allu

select a.locatrecv,a.tmbill,a.tmbildt,a.contno,a.locatpay,a.payfor,a.paytyp,A.CHQDT, a.payamt,a.disct,(a.payint-a.dscint) 
as npayint,a.netpay,a.userid,a.inpdt,a.INPTIME, a.flag,b.snam+RTrim(b.name1)+'  '+RTrim(b.name2) as name,xx.totprc-xx.smpay as bal,xx.BILLCOLL,F.FINNAME  
from chqmas s  left outer join chqtran a on  A.TMBILL=s.TMBILL AND A.LOCATRECV=s.LOCATRECV left outer join custmast b on a.cuscod=b.cuscod  LEFT OUTER JOIN 
#aaa AS XX on xx.contno=a.contno and xx.locat=a.locatpay  
LEFT OUTER JOIN FINMAST F ON xx.FINCOD=F.FINCODE AND (A.PAYFOR='004' OR A.PAYFOR='011') where  a.locatrecv in (select locat from @lookupTable) 
--and a.locatpay like '%' 
and A.TMBILDT Between @startDate AND @endDate 
--and  a.Paytyp like '%' 
--and a.Payfor like '%' 
--and (a.userid like '%')  
--and (a.Tmbill in (Select Tmbill From Chqtran t Where (LocatPay like '%') and (payfor like '%'))) 
order by a.tmbill
*/
/*
use FN;
IF OBJECT_ID('tempdb.dbo.#aaa', 'U') IS NOT NULL DROP TABLE #aaa;
declare @startDate datetime,@endDate datetime,@locat varchar(max);
declare @lookupTable table (locat varchar(max) collate Thai_CS_AS);
declare @aaa table (CONTNO varchar(max),LOCAT varchar(max),BILLCOLL varchar(max),TOTPRC decimal(18,2),SMPAY decimal(18,2),TSALE varchar(max),FINCOD varchar(max))
--insert into @lookupTable select LOCATCD from serviceweb.dbo.wb_invlocat a where a.LOCATCD like 'YR��4'
--insert into @lookupTable select LOCATCD from serviceweb.dbo.wb_invlocat a where a.LOCATCD in ('YK��1','YK��2','YJ���')
set @startDate='2017/01/10';
set @endDate='2017/03/10';
set @locat='YK��1'

--select * from #aaa;
insert into @aaa select * from (
SELECT CONTNO,LOCAT,BILLCOLL,TOTPRC,SMPAY,'H' AS TSALE,'' AS FINCOD FROM ARMAST  WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'C' AS TSALE,'' AS FINCOD FROM ARCRED WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'F' AS TSALE,FINCOD FROM ARFINC WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'A' AS TSALE,'' AS FINCOD FROM AR_INVOI  WHERE CONTNO IN 	(SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,OPTPTOT AS TOTPRC,SMPAY,'O' AS TSALE,'' AS FINCOD FROM AROPTMST WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,BILLCOLL,TOTPRC,SMPAY,'H' AS TSALE,'' AS FINCOD FROM HARMAST  WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'C' AS TSALE,'' AS FINCOD FROM HARCRED WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'F' AS TSALE,FINCOD FROM HARFINC WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,TOTPRC,SMPAY,'A' AS TSALE,'' AS FINCOD FROM HAR_INVO WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate)  
	UNION 
	SELECT CONTNO,LOCAT,'' AS BILLCOLL,OPTPTOT AS TOTPRC,SMPAY,'O' AS TSALE,'' AS FINCOD FROM HAROPMST WHERE CONTNO IN (SELECT CONTNO FROM CHQTRAN WHERE LOCATRECV = @locat AND TMBILDT BETWEEN @startDate AND @endDate) 
)allu

select a.locatrecv,a.tmbill,a.tmbildt,a.contno,a.locatpay,a.payfor,a.paytyp,A.CHQDT, a.payamt,a.disct,(a.payint-a.dscint) 
as npayint,a.netpay,a.userid,a.inpdt,a.INPTIME, a.flag,b.snam+RTrim(b.name1)+'  '+RTrim(b.name2) as name,xx.totprc-xx.smpay as bal,xx.BILLCOLL,F.FINNAME  
from chqmas s  left outer join chqtran a on  A.TMBILL=s.TMBILL AND A.LOCATRECV=s.LOCATRECV left outer join custmast b on a.cuscod=b.cuscod  LEFT OUTER JOIN 
@aaa AS XX on xx.contno=a.contno collate Thai_CS_AS and xx.locat=a.locatpay  collate Thai_CS_AS
LEFT OUTER JOIN FINMAST F ON xx.FINCOD=F.FINCODE collate Thai_CS_AS AND (A.PAYFOR='004' OR A.PAYFOR='011') where  a.LOCATRECV = @locat 
--and a.locatpay like '%' 
and A.TMBILDT Between @startDate AND @endDate 
--and  a.Paytyp like '%' 
--and a.Payfor like '%' 
--and (a.userid like '%')  
--and (a.Tmbill in (Select Tmbill From Chqtran t Where (LocatPay like '%') and (payfor like '%'))) 
order by a.tmbill

*/

IF OBJECT_ID('tempdb.dbo.#tmptable', 'U') IS NOT NULL DROP TABLE #tmptable;
select * into #tmptable from dbo.FNIncomeReportByBranch('20170301','20170313','YJ���')
select * from #tmptable;
select '1. all'src,count(*)cnt,isnull(SUM(payamt),0)sumPayamt,isnull(SUM(disct),0)sumDisct,isnull(SUM(npayint),0)sumNpayint,isnull(SUM(netpay),0)sumNetpay from #tmptable
union
select '2. canceled'src,count(*)cnt,isnull(SUM(payamt),0)sumPayamt,isnull(SUM(disct),0)sumDisct,isnull(SUM(npayint),0)sumNpayint,isnull(SUM(netpay),0)sumNetpay from #tmptable where flag='C'
union
select '3. net'src,count(*)cnt,isnull(SUM(payamt),0)sumPayamt,isnull(SUM(disct),0)sumDisct,isnull(SUM(npayint),0)sumNpayint,isnull(SUM(netpay),0)sumNetpay from #tmptable where flag!='C'