use EMPBase;
IF OBJECT_ID('tempdb..#tempRpt') IS NOT NULL DROP TABLE #tempRpt
IF OBJECT_ID('tempdb..##Result1') IS NOT NULL DROP TABLE ##Result1
declare @fromDate datetime,@toDate datetime;
select @fromDate='20180101',@toDate='20180531';

select 
IDNo,msg,M,cnt
from (
	select 	
			ea.IDNo
			,'CIN' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.checkIn='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNo			
			,'COU' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.checkOut='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNo			
			,'CBS' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.brakeStart='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNo			
			,'CBE' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.brakeEnd='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
)uall