use EMPBase
select
--distinct
 p.em_code,p.name_01,p.sname_01,c.name_01 corpName,sl.id salaryId,sl.person_id
-- ,sl.salary_value
--,len(salary_value) lensl
/*
,unicode(substring(hZero.sl,1,1)) pc1
,unicode(substring(hZero.sl,2,1)) pc2
,unicode(substring(hZero.sl,3,1)) pc3
,unicode(substring(hZero.sl,4,1)) pc4
,unicode(substring(hZero.sl,5,1)) pc5
,unicode(substring(hZero.sl,6,1)) pc6
,unicode(substring(hZero.sl,7,1)) pc7
,unicode(substring(hZero.sl,8,1)) pc8
,unicode(substring(hZero.sl,9,1)) pc9
,unicode(substring(hZero.sl,10,1)) pc10
*/
,csl.salary hSalRate
,case when csl.salary='0' then 9000.00 else CAST(csl.salary as decimal(18,2)) end forCalSalRate
,mxSalary.person_id
from humano_tangjai.dbo.person_data p 
left join humano_tangjai.dbo.corp c on p.corp=c.id
left join humano_tangjai.dbo.salary sl on p.id=sl.person_id
left join (select max(sl.id)mxSalaryId,sl.person_id from humano_tangjai.dbo.salary sl where sl.[status]='1' group by person_id) mxSalary on sl.id=mxSalary.mxSalaryId and sl.person_id=mxSalary.person_id
cross apply (select convert(varchar(40),sl.salary_value) lc)ag
cross apply (select left(sl.salary_value+'000000000000',12)sl)hZero
cross apply (
select 
	case 
	
	when LEN(sl.salary_value)=6 then 			
			case when  unicode(substring(sl.salary_value,4,1))=152 then --��ѡ�����շȹ���
				''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
				+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
				+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)			
			else --��ѡ�ʹ�ӹǹ���
				''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
				+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
				+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
				+CONVERT(varchar, unicode(substring(sl.salary_value,4,1))-154)
				+CONVERT(varchar, unicode(substring(sl.salary_value,5,1))-153)			
				+CONVERT(varchar, unicode(substring(sl.salary_value,6,1))-104)
			end
	when LEN(sl.salary_value)=5 then 
				case
					when unicode(substring(sl.salary_value,1,1))=154 and unicode(substring(sl.salary_value,2,1))=150 and unicode(substring(sl.salary_value,3,1))=165 then 0 --0 �ȹ���
					else 
						'' --��ѡ���蹨ӹǹ���
						+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
						+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
						+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
						+CONVERT(varchar, unicode(substring(sl.salary_value,4,1))-154)
						+CONVERT(varchar, unicode(substring(sl.salary_value,5,1))-153)			
						--+CONVERT(varchar, unicode('-')-104)
				end
      when LEN(sl.salary_value)=4 then --��ѡ�ѹ�ӹǹ���
			case when unicode(substring(sl.salary_value,2,1))=150 then 0 else
			''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
			+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
			+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
			+CONVERT(varchar, unicode(substring(sl.salary_value,4,1))-154)				
			end
	when LEN(sl.salary_value)=3 then --��ѡ���¨ӹǹ���
			''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
			 +CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
			 +CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
	when LEN(sl.salary_value)=1 then
			''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)			
    else case --����繷ȹ���
				when LEN(sl.salary_value)=8 then --�Թ��͹��ѡ�ѹ
					''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
					+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
					+CONVERT(varchar, unicode(substring(sl.salary_value,4,1))-154)
					+CONVERT(varchar, unicode(substring(sl.salary_value,5,1))-153)
				when LEN(sl.salary_value)=9 then --�Թ��͹��ѡ����
					''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
					+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
					+CONVERT(varchar, unicode(substring(sl.salary_value,4,1))-154)
					+CONVERT(varchar, unicode(substring(sl.salary_value,5,1))-153)
					+CONVERT(varchar, unicode(substring(sl.salary_value,6,1))-104)
				when LEN(sl.salary_value)=10 then --�Թ��͹��ѡ�ʹ
					''+CONVERT(varchar, unicode(substring(sl.salary_value,1,1))-154)
					+CONVERT(varchar, unicode(substring(sl.salary_value,2,1))-152)
					+CONVERT(varchar, unicode(substring(sl.salary_value,3,1))-165)
					+CONVERT(varchar, unicode(substring(sl.salary_value,5,1))-153)
					+CONVERT(varchar, unicode(substring(sl.salary_value,6,1))-104)
					+CONVERT(varchar, unicode(substring(sl.salary_value,7,1))-103)
				else null
			end
end salary) csl
where 1=1
--and p.em_status in ('W','P','RR')
and mxSalary.person_id is not null
and p.em_code = 'TJY40000010'
--and p.em_code in (select employeeCode from EMPBase.dbo.hrm_humanoPersonAll where officeName like '%��������ʹ%' and gender='M' )


--select * from EMPBase.dbo.hrm_humanoPersonAll where employeeCode='TJY40000096'

--select * from EMPBase.dbo.FN_SlipDetails('TJY40000010',6,2017)
