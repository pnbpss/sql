use empbase;
select * from ta_shiftForEmployees 
where 1=1
--and DATEDIFF(day, activeFrom,activeTo) between 20 and 30
and IDNo='3841800071563' 
order by activeTo,IDNo
--goto grandEndscript

--select 1/0

/*
--�����������
--update ta_shiftForEmployees set activeFrom='20170101' where activeFrom<'20170101'
select * from ta_shiftForEmployees  where activeFrom<'20170101'
truncate table ta_shiftForEmployeesBak20180604
insert into ta_shiftForEmployeesBak20180604(IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress) select IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress from ta_shiftForEmployees --������
*/

--select * from ta_shiftForEmployees where insertSince>'20180601' and ipaddress<>'192.168.0.94' order by id desc



/*
--���������Ѻ �ҡ�Դ��Ҵ
truncate table ta_shiftForEmployees
insert into ta_shiftForEmployees 
select IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress
from ta_shiftForEmployeesBak20180604
*/

if object_id('tempdb..#temp_ta_shiftForEmployees') is not null drop table #temp_ta_shiftForEmployees;
			create table #temp_ta_shiftForEmployees (
				IDNo		varchar(20),
				shiftId		varchar(20),
				assignBy	varchar(50),
				assignedDate datetime,
				active		varchar(20),
				activeFrom  datetime,
				activeTo	datetime,
				insertSince datetime,
				insertBy	varchar(50),
				lastUpdate	datetime,
				updateBy	varchar(50),
				ipaddress 	varchar(20)
			);

insert into #temp_ta_shiftForEmployees
select IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress
from ta_shiftForEmployeesBak20180604


if object_id('tempdb..#addShiftToEMP') is not null drop table #addShiftToEMP; create table #addShiftToEMP (id varchar(1),msg varchar(max));

			
declare @IDNo varchar(max), @shiftId int,@assignBy varchar(max),@assignedDate datetime, @active varchar(1), @activeFrom datetime, @activeTo datetime,@insertSince datetime,@insertBy varchar(max),@lastUpdate datetime,@updateBy varchar(max),@ipaddress varchar(max),@activeFromMain datetime, @activeToMain datetime, @pe_start_date int, @pe_startmonth int, @pe_end_date int, @pe_endmonth int, @exact_current_pe_end_date datetime, @cDate datetime;			

begin tran tranADDShift
begin try
	delete from ta_shiftForEmployees
	if((select count(*) from #temp_ta_shiftForEmployees) > 0)
	begin		
		
		declare shiftCursor cursor for
		select distinct IDNo,shiftId,assignBy,getdate() as assignedDate,active,activeFrom,activeTo,getdate() as insertSince,insertBy,getdate() as lastUpdate,updateBy,ipaddress 
		from #temp_ta_shiftForEmployees	
		
		
		open shiftCursor
		fetch next from shiftCursor into @IDNo, @shiftId,@assignBy,@assignedDate, @active, @activeFromMain, @activeToMain,@insertSince,@insertBy,@lastUpdate,@updateBy,@ipaddress
		
		WHILE @@FETCH_STATUS = 0  
		begin										
			set @IDNo=@IDNo;
			
			if not (@activeFromMain=@activeToMain) 
			begin	
					select @activeFrom=null, @activeTo=null;	
					--�֧�ѹ���������鹡Ѻ����ش�ͧ�ͺ�Թ��͹�͡��
					select @pe_start_date=pe_start_date
						,@pe_startmonth = pe_startmonth
						,@pe_end_date=pe_end_date
						,@pe_endmonth =pe_endmonth 
					from hrm_paymentPackage 
					where id = (select period_no from hrm_humanoPersonAdjust where IDNo=@IDNo)
					
					--����� ����� activeFrom �֧ activeTo ���ͺ���¡���ͺ ���ͤ��������ͺ
					set @cDate=@activeFromMain
					while @cDate<=@activeToMain --ǹ�ѹ ������ѹ�á���֧�ѹ�ش���� @activeFromMain �֧ @activeToMain
					begin	
						set 					
							@exact_current_pe_end_date = 
							DATEADD(
								month,@pe_endmonth,CONVERT(varchar(4),year(@cDate))+'/'+CONVERT(varchar(2),month(@cDate))+'/'+CONVERT(varchar(2),
								--�ҡ�ѹ����ش pe_end_date 㹰ҹ������ �ҡ���� �ѹ����ش�ͧ��͹�Ѩ�غѹ �� ��͹�������͹ ��.�. ����� 30 �ѹ ���������¹�ѹ����ش �� 30
								case when @pe_end_date > day(dateadd(day,-1,dateadd(month,1,convert(varchar(4),year(@cDate))+'/'+convert(varchar(2),month(@cDate))+'/1'))) 
									then day(dateadd(day,-1,dateadd(month,1,convert(varchar(4),year(@cDate))+'/'+convert(varchar(2),month(@cDate))+'/1')))
								else @pe_end_date
								end
								))
																		
						if @activeFrom is null begin set @activeFrom = @cDate; end
						
						if @cDate=@exact_current_pe_end_date
						begin										
							set @activeTo = @exact_current_pe_end_date
							print '1:'+isnull(convert(varchar,@cDate,111),'1null')+' '+isnull(convert(varchar,@activeFrom,111),'2null')+' '+isnull(convert(varchar,@activeTo,111),'3null'); 
							exec dbo.addShiftToEmployee  @IDNo,@activeFrom,@activeTo,@assignBy,@insertBy,@updateBy,@ipaddress,@shiftId
							set @activeFrom = null
							--set @activeFrom = @cDate
						end		
						set @cDate = dateadd(day,1,@cDate) --�����ѹ��ա 1 �ѹ�������ú loop
					end
					
					if @activeFrom is not null 
					begin
						set @activeTo = @activeToMain
						print '2:'+isnull(convert(varchar,@cDate,111),'1null')+' '+isnull(convert(varchar,@activeFrom,111),'2null')+' '+isnull(convert(varchar,@activeTo,111),'3null')	
						exec dbo.addShiftToEmployee  @IDNo,@activeFrom,@activeTo,@assignBy,@insertBy,@updateBy,@ipaddress,@shiftId
					end
					
			end
			else --�ҡ�ѹ������� ����ѹ����ش��ҡѹ
			begin
					print '3:'+isnull(convert(varchar,@cDate,111),'1null')+' '+isnull(convert(varchar,@activeFromMain,111),'2null')+' '+isnull(convert(varchar,@activeToMain,111),'3null')
					exec dbo.addShiftToEmployee  @IDNo,@activeFromMain,@activeToMain,@assignBy,@insertBy,@updateBy,@ipaddress,@shiftId
					--@IDNo varchar(max),@activeFrom datetime, @activeTo datetime,@assignBy varchar(max),@insertedBy varchar(max),@updatedBy varchar(max),@IpAddress varchar(max),@shiftId int
			end	
			
			fetch next from shiftCursor into @IDNo, @shiftId,@assignBy,@assignedDate, @active, @activeFromMain, @activeToMain,@insertSince,@insertBy,@lastUpdate,@updateBy,@ipaddress
		end
		
		close shiftCursor
		DEALLOCATE shiftCursor	

		insert into #addShiftToEMP select 'S','����� :: �ѹ�֡�Чҹ���º��������';
	end
	else
	begin
		insert into #addShiftToEMP select 'F','�Դ��Ҵ::�ô�кءЧҹ���Ѻ��ѡ�ҹ���¤��';
	end
	commit tran tranADDShift;
end try
begin catch
	rollback tran tranADDShift;
	insert into #addShiftToEMP select 'F','�Դ��Ҵ :: ('+ERROR_LINE()+') '+ERROR_MESSAGE() as msg;
end catch
endScript:

select * from #addShiftToEMP
grandEndscript: