use EMPBase;
IF OBJECT_ID('tempdb..#abc') IS NOT NULL DROP TABLE  #abc;
select b.IDNo
  into #abc
  from dbo.hrm_commanders a
  left join hrm_humanoPersonAll b on a.employeeCode=b.employeeCode  
  where cEmpCode='TJY40000010' and a.[rank]=1 and b.em_status in ('W','P')
go

IF OBJECT_ID('tempdb..##shifDetails') IS NOT NULL DROP TABLE  ##shifDetails;
declare @startDate datetime, @endDate datetime;
set @startDate = convert(datetime,'2017/08/01'); 
set @endDate   = dateadd(MILLISECOND,-2,dateadd(day,1,convert(datetime,'2017/08/31')));

WITH [sample] AS (SELECT @startDate AS dt,DATEPART(dw,@startDate)dow  
UNION ALL SELECT DATEADD(dd, 1, dt),DATEPART(dw,DATEADD(dd, 1, dt))dow 
FROM sample s WHERE DATEADD(dd, 1, dt) <= @endDate)	
select allDate.dt,emp.IDNo
into ##shifDetails 
from 
(SELECT convert(varchar,dt,111)dt	
 FROM [sample]) allDate,#abc emp
option (maxrecursion 3650);

declare @columns varchar(max);
set @columns = STUFF((SELECT distinct ',' +  '[D'+replace(substring(dt,3,8),'/','')+']' dt FROM ##shifDetails ST1 order by dt  FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'' ) 

declare @sqlStr varchar(max);
set @sqlStr = '
select p.IDNo, '+@columns+'
from (select ''D''+replace(substring(dt,3,8),''/'','''')dt,sh.IDNo,substring(convert(varchar,msh.breakStart,114),1,5)+''-''+substring(convert(varchar,msh.breakEnd,114),1,5) brk from ##shifDetails sh left join ta_shiftForEmployees c on sh.IDNo=c.IDNo and sh.dt between c.activeFrom and c.activeTo left join ta_shifts msh on c.shiftId=msh.id) t
pivot (max(brk) for dt in ('+@columns+') ) p
';
--print @sqlStr
exec(@sqlStr);

