USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_translateStateOfCal]    Script Date: 12/21/2017 16:13:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_translateStateOfCal]
(
	-- Add the parameters for the function here
	@IDNo varchar(20),@stateOfCal varchar(8),@cDate datetime,@minuteLate int,@minuteEarly int,@minuteLeaveDuringBrake int,@minuteBrakeEarlyOrLate int,@paymentOfLeaveDuringBrake char(1)
	,@workStart datetime,@workEnd datetime,@isCheckin char(1)
)
RETURNS 
@results TABLE 
(
	msg varchar(1024),
	leaveInMinute int,
	leaveInDay int,
	leaveDeduction int,--0 = ไม่หัก, 1=หัก
	absenceInMinute int,
	absenceInDay int,
	lateInMinute int,
	earlyInMinute int,
	totalMinuteLatePlusEarly int,
	totalDeduction decimal(18,2),
	[absent] decimal(18,2),
	[leave] decimal(18,2),
	errandLeave decimal(18,2),
	sickLeave decimal(18,2),
	[brake] decimal(18,2),
	[lateAndEarly] decimal(18,2),
	[notCheckInOrOut] decimal(18,2)
	,dummy varchar(max)
)
AS
BEGIN
--shiftInfo
	declare 
	@shiftMusTA	char(1)
	,@shiftName	varchar(max)
	,@shiftStartTime	 time
	,@shiftHaveToCheckEnd	char(1)
	,@shiftEndTime	time
	,@shiftMinCheckInTime	time
	,@shiftMaxCheckOutTime	time
	,@shiftBrakeCheck	char(1)
	,@shiftBreakStart	time
	,@shiftBreakEnd	time
	,@shiftMinuteBrake	int
	,@leaveGroupTypeId int
	,@absent decimal(18,2)
	,@leave decimal(18,2)
	,@errandLeave decimal(18,2)
	,@sickLeave decimal(18,2)
	,@brake decimal(18,2)
	,@lateAndEarly decimal(18,2)
	,@notCheckInOrOut decimal(18,2)
	,@compromiseLate int
	,@compromiseEarly int
	
	--ดึงข้อมูล shift ออกมา ในวันนี้ออกมา
	select 
	@shiftMusTA=musTA
	,@shiftName=shiftName
	,@shiftStartTime=startTime
	,@shiftHaveToCheckEnd=haveToCheckEnd
	,@shiftEndTime=endTime
	,@shiftMinCheckInTime=minCheckInTime
	,@shiftMaxCheckOutTime=maxCheckOuttime
	,@shiftBrakeCheck=brakeCheck
	,@shiftBreakStart=breakStart
	,@shiftBreakEnd=breakEnd
	,@shiftMinuteBrake=minuteBrake
	,@compromiseLate=compromiseLate
	,@compromiseEarly=compromiseEarly
	from ta_shifts sh
	inner join ta_shiftForEmployees she on sh.id=she.shiftId
	and she.IDNo=@IDNo
	and @cDate between she.activeFrom and she.activeTo and active='Y'


	-- Fill the table variable with the rows for your result set
	declare @msg varchar(max),
	@leaveInMinute int,
	@leaveInDay int,
	@leaveDeduction int,--0 = ไม่หัก, 1=หัก
	@absenceInMinute int,
	@absenceInDay int,
	@lateInMinute int,
	@earlyInMinute int,
	@totalMinuteLatePlusEarly int,	
	@totalDeduction decimal(18,2),
	@deduction varchar(1),
	@minuteLeave int,
	@leaveName varchar(max),
	@dummy varchar(max)
	
	select @msg ='',
	@leaveInMinute=0,
	@leaveInDay=0,
	@leaveDeduction=0,--0 = ไม่หัก, 1=หัก
	@absenceInMinute=0,
	@absenceInDay=0,
	@lateInMinute=0,
	@earlyInMinute=0,
	@totalMinuteLatePlusEarly=0,	
	@totalDeduction=0.00,
	@absent=0.00,
	@leave=0.00,
	@errandLeave=0,
	@sickLeave=0,
	@brake=0.00,
	@lateAndEarly=0.00,
	@notCheckInOrOut=0.00,
	@dummy=''
	;
	
	declare @duringDayMsg varchar(max),@duringDayDeduction decimal(18,2); 
	set @duringDayMsg=''; 
	set @duringDayDeduction = 0.00;
	
	if @workStart > @cDate begin insert into @results(msg,totalDeduction)values('{{ยังไม่เริ่มทำงาน}}',0); return end --หากวันปัจจุบันน้อยกว่าวันเริ่มงาน
	if @workEnd < @cDate begin insert into @results(msg,totalDeduction)values('{{พ้นสภาพพนักงาน}}',0); return end --หากวันปัจจุบันมากกว่าวันลาออก
	if @stateOfCal='++++++++' begin insert into @results(msg,totalDeduction)values('{{ปกติ}}',0); return end --ปกติไม่ต้องคิดไรมาก	
	if @stateOfCal='F+++++++' begin insert into @results(msg,totalDeduction)values('{{หัวหน้างานยังไม่ได้กำหนดกะงานให้}}',dbo.FN_001_calculateDeduction(8*60,@IDNo)); return end --ไม่ได้กำหนดกะงานให้ที ต้องไปกำหนดกะงานให้ก่อน 
	if @stateOfCal='G+++++++' begin insert into @results(msg,totalDeduction)values('{{ปกติ(สลับวันหยุด)}}',0); return end --สลับวันหยุดทั้งวัน
	if @stateOfCal='E+++++++' begin insert into @results(msg,totalDeduction)values('{{หัวหน้างานยังไม่ได้กำหนดวันหยุดให้}}',dbo.FN_001_calculateDeduction(8*60,@IDNo)); return end --ไม่ได้กำหนดวันหยุดให้ที ต้องไปกำหนดวันหยุดให้ก่อน	
	if @stateOfCal='Z+++++++' begin insert into @results(msg,totalDeduction)values('{{Too Early To Calculate}}',0); return end --ปกติไม่ต้องคิดไรมาก
	if substring(@stateOfCal,1,1)='B'  
	begin 
		set @absent=dbo.FN_001_calculateDeduction(8*60,@IDNo);
		insert into @results(msg,totalDeduction,[absent])	values('{{ขาดงานทั้งวัน}}',round(@absent,0),round(@absent,0)); return 
	end  ---ขาดงานทั้งวันหักขาดงานทั้งวัน
	
	if @stateOfCal='H+++++++' or @stateOfCal='I+++++++'
	begin
			--ลากิจ
			declare @errandLeavecase1H decimal(18,2),@sickLeavecase1H decimal(18,2),@minuteLeaveCase1H int;
			set @errandLeavecase1H =  (
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													 from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													 where el.approveResult='Y' and el.[status]=1
													 and el.IDNo=@IDNo 
													 and 
														(--ลาช่วงเช้า
															el.startDateTime <= @cDate+@shiftStartTime 
															and el.endDateTime = dateadd(hour,4,@cDate+@shiftStartTime)	
															and lt.leaveTypeGroup=1
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime = dateadd(hour,-4,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))) 
															and el.endDateTime >= dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=1
														)
												)
			--ลาป่วย
			set @sickLeavecase1H =  (
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													where 
													el.IDNo=@IDNo 
													and el.approveResult='Y' and el.[status]=1
													and 
														(--ลาช่วงเช้า
															el.startDateTime <= @cDate+@shiftStartTime 
															and el.endDateTime = dateadd(hour,4,@cDate+@shiftStartTime)	
															and lt.leaveTypeGroup=2 
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime = dateadd(hour,-4,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))) 
															and el.endDateTime >= dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=2 
														)
											)
			set @minuteLeaveCase1H = isnull(
													(
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													 from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													 where el.approveResult='Y' and el.[status]=1
													 and el.IDNo=@IDNo 
													 and 
														(--ลาช่วงเช้า
															el.startDateTime <= @cDate+@shiftStartTime 
															and el.endDateTime = dateadd(hour,4,@cDate+@shiftStartTime)													
															and lt.leaveTypeGroup=1
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime = dateadd(hour,-4,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))) 
															and el.endDateTime >= dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=1
														)
												)
												,0)
												+
												isnull(
												(
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													where 
													el.IDNo=@IDNo 
													and el.approveResult='Y' and el.[status]=1
													and 
														(--ลาช่วงเช้า
															el.startDateTime <= @cDate+@shiftStartTime 
															and el.endDateTime = dateadd(hour,4,@cDate+@shiftStartTime)
															and lt.leaveTypeGroup=2 
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime = dateadd(hour,-4,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))) 
															and el.endDateTime >= dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=2 
														)
												)	
												,0)
			select @leave = isnull(@leave,0) + isnull(@sickLeavecase1H,0)+isnull(@errandLeavecase1H,0)
					 ,@errandLeave = isnull(@errandLeave,0) + isnull(@errandLeavecase1H,0)
					 ,@sickLeave = isnull(@sickLeave,0) + isnull(@sickLeavecase1H,0);			
			--('+convert(varchar,isnull(@minuteLeaveCase1H,-1))+')นาที		 
			insert into @results(msg,totalDeduction,sickLeave,errandLeave)	values('{{ลาหรือสลับวันหยุดทั้งวันวันแบบแบ่งครึ่งวัน}}',(@sickLeave+@errandLeave),@sickLeave,@errandLeave); return 
	end
		
	if substring(@stateOfCal,1,1)='A' or substring(@stateOfCal,1,1)='D'  or substring(@stateOfCal,1,1)='C'  --คำนวณ หัก/ไม่หัก ตามชนิดการลา
	begin 	
	set @deduction='X';
	select top 1 @msg=@msg+lt.name+'' ,@deduction = lt.paymentOnLeave,@leaveGroupTypeId=lt.[group]
	from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
	where el.IDNo=@IDNo 	
	and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
	and el.approveResult='Y' 
	and DATEDIFF(minute,el.startDateTime,el.endDateTime) >= 
	datediff(minute
	,@cDate+@shiftStartTime
	,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))	)
	
	if @deduction <> 'X' --ลาแน่นอน
	begin	
		declare @deductionRateHere decimal(18,2);
		if @deduction='Y' begin set @deductionRateHere=0 end 
									else begin 
												if @deduction='H' set @deductionRateHere=(0.5*8*60) else set @deductionRateHere=(1*8*60)
										  end
		set @leave = dbo.FN_001_calculateDeduction(@deductionRateHere,@IDNo);
		if @leaveGroupTypeId = 2 begin set @sickLeave = @leave end
		if @leaveGroupTypeId = 1 begin set @errandLeave = @leave end
		insert into @results(msg,totalDeduction,leave,sickLeave,errandLeave)
		values(@msg,round(@leave,0),round(@leave,0),round(@sickLeave,0),round(@errandLeave,0)); return 
	end
	else if exists(
			--ขอไม่สแกนทั้งวันหรือเปล่า สำหรับคนที่ต้องสแกน เช้า พัก เลิก
			select 1 from ta_empNotAttendances 
			where IDNo=@IDNo 
			and @cDate between fromDate and toDate 
			and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and @shiftBrakeCheck='Y' and @shiftHaveToCheckEnd='Y'
			and approvedDate is not null and approved='Y' and [status]<>'D' 
			union
			--ขอไม่สแกนทั้งวันหรือเปล่า สำหรับคนที่ต้องสแกน เช้าและเลิก แต่ไม่ต้องพัก
			select 1 from ta_empNotAttendances 
			where IDNo=@IDNo 
			and @cDate between fromDate and toDate 
			and checkIn='Y' and brakeStart='N' and brakeEnd='N' and checkOut='Y' and @shiftBrakeCheck='N' and @shiftHaveToCheckEnd='Y'
			and approvedDate is not null and approved='Y' and [status]<>'D' 
			union
			--ขอไม่สแกนทั้งวันหรือเปล่า สำหรับคนที่ต้องสแกน เช้า ไม่พัก ไม่เลิก
			select 1 from ta_empNotAttendances 
			where IDNo=@IDNo 
			and @cDate between fromDate and toDate 
			and checkIn='Y' and brakeStart='N' and brakeEnd='N' and checkOut='N' and @shiftBrakeCheck='N' and @shiftHaveToCheckEnd='N'
			and approvedDate is not null and approved='Y' and [status]<>'D' 
			
			)
		begin			 
			 /*เผื่อมีการขอลางานระหว่างวันด้วย*/
			 if substring(@stateOfCal,4,1)='E' --and @paymentOfLeaveDuringBrake='N'   --มีการลาระหว่างพักและต้องหัก
			begin 
				--set @leave = @leave + dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo);
				if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) end
				if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) end
				select @duringDayMsg=@duringDayMsg+'{{ลาระหว่างพัก(att)'+'('+convert(varchar,@minuteLeaveDuringBrake)+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) 
			end
			else begin insert into @results(msg,totalDeduction)values('{{ขอไม่สแกนนิ้วทั้งวัน}}',0);  return end
		end
	end	--if @stateOfCal='A+++++'  --คำนวณ หัก/ไม่หัก ตามชนิดการลา
	
	
		
	
	if substring(@stateOfCal,2,1)='D' --ลาครึ่งวันเช้า
	begin
			set @leaveName='';
			select top 1 @leaveName=lt.name+'' 
			,@deduction = lt.paymentOnLeave
			,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
			,@leaveGroupTypeId = lt.[group]
			from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
			where el.IDNo=@IDNo 	
			and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
			and el.approveResult='Y' 
			and datediff(minute,el.startDateTime,el.endDateTime) >= 4
			--datediff(minute,@cDate+@shiftStartTime	,dateadd(minute,datepart(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)))
			;
			if @shiftBrakeCheck='N' --หากไม่ต้องลงเวลาพัก
			begin
				select top 1 @leaveName=lt.name+'' 
				,@deduction = lt.paymentOnLeave
				,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
				,@leaveGroupTypeId = lt.[group]
				from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
				where el.IDNo=@IDNo 	
				and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
				and el.approveResult='Y' 
				and datediff(minute,el.startDateTime,el.endDateTime) >= 4;
			end	
			declare @minuteDeductionM decimal(18,2);
			if @deduction='Y'  set @minuteDeductionM=0	else begin if @deduction='H' begin set @minuteDeductionM=4*60*0.5  end else set @minuteDeductionM=4*60  end	
			set @leave = @leave + dbo.FN_001_calculateDeduction(@minuteDeductionM,@IDNo);
			if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+ dbo.FN_001_calculateDeduction(@minuteDeductionM,@IDNo) end
			if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteDeductionM,@IDNo) end			
			select @duringDayMsg=@duringDayMsg+'{{ลาครึ่งแรกของวัน('+isnull(@leaveName,'error:1')+')}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteDeductionM,@IDNo) 
	end
	else
	begin
			if substring(@stateOfCal,2,1)='A'  --คำนวณ หัก ขาดงานครึ่งวัน	
			 begin 
				set @notCheckInOrOut = @notCheckInOrOut+@duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo);
				select @duringDayMsg=@duringDayMsg+'{{ไม่ลงเวลาเข้า}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo) 
			 end  
			if ((@stateOfCal='+BGA++++')or(@stateOfCal='+BGF++++')
				or(@stateOfCal='+BAA++++')
				or(@stateOfCal='+BGD++++')
				) and @isCheckin = 'N' 
			begin
				set @notCheckInOrOut = @notCheckInOrOut+@duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo);
				--set @notCheckInOrOut = @duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo);
				set @duringDayMsg=@duringDayMsg+'{{ไม่ลงเวลาเข้า}}'
				--set @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo) 
			end
			if substring(@stateOfCal,2,1)='B' and substring(@stateOfCal,5,1)<>'G' and (not(((@stateOfCal='+BGA++++')or(@stateOfCal='+BGF++++')or(@stateOfCal='+BAA++++')or(@stateOfCal='+BGD++++')) and @isCheckin = 'N'))--คำนวณ หักสาย
			begin 
				declare @compromiseLateString varchar(500);
				set @compromiseLateString = ''
				if @compromiseLate>0 set @compromiseLateString =  'อนุโลม'+convert(varchar,@compromiseLate)+'นาที'
				set @lateAndEarly = @lateAndEarly + @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteLate,@IDNo);
				select @duringDayMsg=@duringDayMsg+'{{สาย'+
				@compromiseLateString
				+'('+convert(varchar,isnull(@minuteLate,-1))+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteLate,@IDNo) 
			end  
			
			if substring(@stateOfCal,2,1)='C'  --เข้าสายแต่มีใบลา หักตามชนิดการลา
			begin 
			select top 1 @leaveName=lt.name+'' 
			,@deduction = lt.paymentOnLeave
			,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
			,@leaveGroupTypeId = lt.[group]
			from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
			where el.IDNo=@IDNo 	
			and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
			and el.approveResult='Y' 
			and datediff(minute,el.startDateTime,el.endDateTime) >= @minuteLate;
			
			declare @minuteForDeductionForLate int
			if @deduction='Y' 
			begin
			set @minuteForDeductionForLate=0 
			end
			else 
			begin
			if @deduction='H'
			set @minuteForDeductionForLate=@minuteLate*0.5
			else set @minuteForDeductionForLate=@minuteLate;
			end
				set @leave = @leave +dbo.FN_001_calculateDeduction(@minuteForDeductionForLate,@IDNo) ;
				if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+ dbo.FN_001_calculateDeduction(@minuteForDeductionForLate,@IDNo) end
				if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteForDeductionForLate,@IDNo) end
				select @duringDayMsg=@duringDayMsg+'{{ลาขอเข้าสาย'+'('+convert(varchar,isnull(@minuteLate,-2))+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteForDeductionForLate,@IDNo) 
			end  
	end
	
	
	if substring(@stateOfCal,3,1)='A'    --คำนวณ หัก ไม่ลงเวลาออก
	begin 
		set @notCheckInOrOut = @notCheckInOrOut + @duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo);
		select @duringDayMsg=@duringDayMsg+'{{ไม่ลงเวลาออก}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(4*60,@IDNo) 	
	end  
	else
	begin
			if substring(@stateOfCal,3,1)='B'    --คำนวณ หัก ออกก่อน
			begin 
				declare @compromiseEarlyString varchar(500);
				set @compromiseEarlyString = '';
				--set @minuteEarly = datediff(minute,,select max() from );
				if @compromiseEarly>0 set @compromiseEarlyString='อนุโลมออกก่อน'+convert(varchar,@compromiseEarly)+'นาที'
				set @lateAndEarly = @lateAndEarly+@duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteEarly,@IDNo);
				select @duringDayMsg=@duringDayMsg+'{{ออกก่อน'+@compromiseEarlyString+'('+convert(varchar,isnull(@minuteEarly,-3))+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteEarly,@IDNo) 
			end 
			if substring(@stateOfCal,3,1)='D'    --ลาครึ่งวันหลัง 
			begin 	
			set @leaveName='';
			select top 1 @leaveName=lt.name+'' 
			,@deduction = lt.paymentOnLeave
			,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
			,@leaveGroupTypeId=lt.[group]
			from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
			where el.IDNo=@IDNo 	
			and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
			and el.approveResult='Y' 
			and datediff(minute,el.startDateTime,el.endDateTime) >= 4
			--datediff(minute,dateadd(minute,datepart(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))	,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))	);
			if @shiftBrakeCheck='N' --หากไม่ต้องลงเวลาพัก
			begin
				select top 1 @leaveName=lt.name+'' 
				,@deduction = lt.paymentOnLeave
				,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
				,@leaveGroupTypeId=lt.[group]
				from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
				where el.IDNo=@IDNo 	
				and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
				and el.approveResult='Y' 
				and datediff(minute,el.startDateTime,el.endDateTime) >= 4;
			end
			declare @minuteDeductionLeaveSecondHalf int;
			if @deduction='Y' 
			begin
				set @minuteDeductionLeaveSecondHalf = 0 
			end 
			else
			begin
				 if @deduction='H' set @minuteDeductionLeaveSecondHalf =4*60*0.5 else set @minuteDeductionLeaveSecondHalf =4*60;
			end
				set @leave = @leave +dbo.FN_001_calculateDeduction( @minuteDeductionLeaveSecondHalf ,@IDNo);
				if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+ dbo.FN_001_calculateDeduction(@minuteDeductionLeaveSecondHalf ,@IDNo) end
				if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteDeductionLeaveSecondHalf ,@IDNo) end
				select @duringDayMsg=@duringDayMsg+'{{ลาครึ่งหลังของวัน('+isnull(@leaveName,'error:3')+')}}', @duringDayDeduction = @duringDayDeduction+ dbo.FN_001_calculateDeduction( @minuteDeductionLeaveSecondHalf ,@IDNo) 
			end 
			if substring(@stateOfCal,3,1)='C' --ออกก่อน แต่ลา
			begin
				select top 1 @leaveName=lt.name+'' 
				,@deduction = lt.paymentOnLeave
				,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
				,@leaveGroupTypeId=lt.[group]
				from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
				where el.IDNo=@IDNo 	
				and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
				and el.approveResult='Y' 
				and datediff(minute,el.startDateTime,el.endDateTime) >= @minuteEarly;
				declare @deductionOnMinuteEarlyLeave int
				if @deduction = 'Y' 
					begin set @deductionOnMinuteEarlyLeave = 0 end
					else 
					begin 
					if @deduction = 'H' set @deductionOnMinuteEarlyLeave = @minuteEarly*0.5 else set @deductionOnMinuteEarlyLeave = @minuteEarly
					end
				set @leave = @leave + dbo.FN_001_calculateDeduction( @deductionOnMinuteEarlyLeave ,@IDNo);
				if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+dbo.FN_001_calculateDeduction( @deductionOnMinuteEarlyLeave ,@IDNo) end
				if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction( @deductionOnMinuteEarlyLeave ,@IDNo) end
				select @duringDayMsg=@duringDayMsg+'{{ลาขอออกก่อน('+isnull(@leaveName,'error:4')+')}}', @duringDayDeduction = @duringDayDeduction+ dbo.FN_001_calculateDeduction( @deductionOnMinuteEarlyLeave ,@IDNo) 
			end
	end 
	
	----------------------------
	if not(substring(@stateOfCal,3,1)='A' or substring(@stateOfCal,2,1)='A' ) --ไม่ได้ไม่ลงเวลาเข้าหรือออก 
	begin
		if substring(@stateOfCal,4,1)='A'	begin set @duringDayMsg=@duringDayMsg+'{{ปกติ}}' end
	
		if substring(@stateOfCal,4,1)='C'	begin set @duringDayMsg=@duringDayMsg+'{{ปกติ(ขอนุมัติไม่ลงเวลาพัก)}}' end
		
		if substring(@stateOfCal,4,1)='D'    --คำนวณ หัก ไม่ลงเวลาพักเที่ยง / ลงเวลาพักเที่ยงไม่ครบ
		begin 
			set @brake = @brake + dbo.FN_001_calculateDeduction(60,@IDNo);
			select @duringDayMsg=@duringDayMsg+'{{ไม่ลงเวลาพักหรือลงเวลาพักไม่ครบถ้วนหรือไม่ตรงตามกะงาน}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(60,@IDNo) 
		end 
		
		if substring(@stateOfCal,4,1)='F'    --คำนวณ หัก ไม่ลงเวลาพักเที่ยง / ลงเวลาพักเที่ยงไม่ครบ
		begin 
			set @brake = @brake + dbo.FN_001_calculateDeduction(@minuteBrakeEarlyOrLate,@IDNo);
			select @duringDayMsg=@duringDayMsg+'{{ไปพักก่อนหรือกลับจากพักเลท('+convert(varchar,@minuteBrakeEarlyOrLate)+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteBrakeEarlyOrLate,@IDNo) 
		end 
		
		if substring(@stateOfCal,4,1)='E' and @paymentOfLeaveDuringBrake='Y'   --มีการลาระหว่างพักและต้องหัก
		begin 
			set @leave = @leave + dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo);
			if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) end
			if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) end
			select @duringDayMsg=@duringDayMsg+'{{ลาคร่อมหรือติดกับเวลาพัก'+'('+convert(varchar,@minuteLeaveDuringBrake)+'นาที)}}', @duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteLeaveDuringBrake,@IDNo) 
		end
	end
	
		
	if substring(@stateOfCal,2,1)='E'  --สลับวันหยุดครึ่งวันเช้า
	begin set @duringDayMsg=@duringDayMsg+'{{สลับวันหยุดครึ่งวันแรก}}' end 
	
	if substring(@stateOfCal,2,1)='H'  --สลับวันหยุดครึ่งวันเช้า
	begin set @duringDayMsg=@duringDayMsg+'{{เข้าสายแต่สลับวันหยุดครึ่งวันแรก,หรือขอไม่สแกนนิ้ว}}' end 
	
	if substring(@stateOfCal,3,1)='E'  --สลับวันหยุดครึ่งวันบ่าย
	begin set @duringDayMsg=@duringDayMsg+'{{สลับวันหยุดครึ่งวันหลัง}}' end 
	
	if substring(@stateOfCal,3,1)='F'  --ขอนุมัติไม่สแกนออก
	begin set @duringDayMsg=@duringDayMsg+'{{ขอนุมัติไม่สแกนออก}}' end 		
	
	if substring(@stateOfCal,3,1)='I'  --ขอนุมัติไม่สแกนออก
	begin set @duringDayMsg=@duringDayMsg+'{{ขอนุมัติไม่สแกนออก}}' end 		
	
	if (substring(@stateOfCal,3,1)='H' and (substring(@stateOfCal,2,1)='F' or substring(@stateOfCal,2,1)='C' or substring(@stateOfCal,2,1)='B' or substring(@stateOfCal,4,1)='D')) or substring(@stateOfCal,4,1)='E' --ลาคร่อมเวลาพักระหว่าง หลังเวลาเข้างานและก่อนพัก จนถึงเลิกงาน
	begin
			--if 1=1 begin insert into @results(msg,totalDeduction)values('somethingwrong',0.05); return end
			set @duringDayMsg=''; set @duringDayDeduction=0;set @brake=0;set @lateAndEarly=0;
			select top 1 @leaveName=lt.name+'' 
			,@deduction = lt.paymentOnLeave
			,@minuteLeave = datediff(minute,el.startDateTime,el.endDateTime)
			,@leaveGroupTypeId=lt.[group]
			from ta_leaveTypes lt inner join ta_employeeLeaves el on lt.id=el.leaveId
			where el.IDNo=@IDNo 	
			and @cDate between cast(CONVERT(varchar,el.startDateTime,111) as datetime) and cast(CONVERT(varchar,el.endDateTime,111) as datetime)
			and el.approveResult='Y' 
			and datediff(minute,el.startDateTime,el.endDateTime) >= 5;		
			--if 1=1 begin insert into @results(msg,totalDeduction)values(@leaveName+convert(varchar,@minuteLeave)+' '+@deduction,0.05); return end
			declare @minuteLeaveOverlapBrake decimal(18,2);
			if @deduction = 'Y' begin set @minuteLeaveOverlapBrake = 0 end
			
			else 
			begin 	
				--if @deduction = 'H' set @minuteLeaveOverlapBrake = (@minuteLeave-60)*0.5 else set @minuteLeaveOverlapBrake = (@minuteLeave-60) 
				--if @deduction = 'H' set @minuteLeaveOverlapBrake = (60-@minuteLeave)*0.5 else set @minuteLeaveOverlapBrake = (60-@minuteLeave) 
				if @deduction = 'H' set @minuteLeaveOverlapBrake = (@minuteLeave)*0.5 else set @minuteLeaveOverlapBrake = (@minuteLeave) 
			end
			
			set @leave = @leave + dbo.FN_001_calculateDeduction(@minuteLeaveOverlapBrake,@IDNo);
			if @leaveGroupTypeId = 1 begin set @errandLeave = @errandLeave+dbo.FN_001_calculateDeduction(@minuteLeaveOverlapBrake,@IDNo) end
			if @leaveGroupTypeId = 2 begin set @sickLeave = @sickLeave+ dbo.FN_001_calculateDeduction(@minuteLeaveOverlapBrake,@IDNo) end
			
			select @duringDayMsg=@duringDayMsg+'{{ลาคร่อมเวลาพัก'+isnull(@leaveName,'error:5')+''+'('+convert(varchar,@minuteLeave)+'นาที)}}', 
			@duringDayDeduction = @duringDayDeduction+dbo.FN_001_calculateDeduction(@minuteLeaveOverlapBrake,@IDNo) 
	end
	
	if substring(@stateOfCal,5,1)='G'--ขอนุมัติไม่สแกนเข้า +BGAG+
	begin 
		set @duringDayMsg=@duringDayMsg+'{{ขอนุมัติไม่สแกนเข้า}}' 
	end
	--begin set @duringDayMsg='{{ขอนุมัติไม่สแกนเข้า}}' end 	
	/*+BGA++H+*/
	if substring(@stateOfCal,7,1)='H'--มีการลาระหว่างเข้างานถึงเริ่มพัก หรือ หลังพักถึงเลิกงาน
	begin 
			--ลากิจ
			declare @errandLeaveCase7H decimal(18,2),@sickLeaveCase7H decimal(18,2),@minuteLeaveCase7 decimal(18,2);
			set @errandLeaveCase7H =  (
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													 from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													 where el.approveResult='Y' and el.[status]=1
													 and el.IDNo=@IDNo 
													 and 
														(--ลาช่วงเช้า
															el.startDateTime > @cDate+@shiftStartTime 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))													
															and lt.leaveTypeGroup=1
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime > dateadd(minute,datepart(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=1
														)
												)
			--ลาป่วย
			set @sickLeaveCase7H =  (
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													where 
													el.IDNo=@IDNo 
													and el.approveResult='Y' and el.[status]=1
													and 
														(--ลาช่วงเช้า
															el.startDateTime > @cDate+@shiftStartTime 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))													
															and lt.leaveTypeGroup=2 
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime > dateadd(minute,datepart(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=2 
														)
											)
			set @minuteLeaveCase7 = isnull(
													(
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													 from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													 where el.approveResult='Y' and el.[status]=1
													 and el.IDNo=@IDNo 
													 and 
														(--ลาช่วงเช้า
															el.startDateTime > @cDate+@shiftStartTime 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))													
															and lt.leaveTypeGroup=1
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime > dateadd(minute,datepart(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=1
														)
												)
												,0)
												+
												isnull(
												(
													select sum(dbo.FN_001_calculateDeduction(DATEDIFF(minute,el.startDateTime,el.endDateTime)*lt.deductionRate,el.IDNo))
													from ta_leaveTypeDeductionRate lt inner join ta_employeeLeaves el on lt.id=el.leaveId 
													where 
													el.IDNo=@IDNo 
													and el.approveResult='Y' and el.[status]=1
													and 
														(--ลาช่วงเช้า
															el.startDateTime > @cDate+@shiftStartTime 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))													
															and lt.leaveTypeGroup=2 
														)
														or
														(--ลาช่วงบ่าย
															el.startDateTime > dateadd(minute,datepart(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
															and el.endDateTime < dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
															and lt.leaveTypeGroup=2 
														)
												)	
												,0)
			select @leave = @leave + isnull(@sickLeaveCase7H,0)+isnull(@errandLeaveCase7H,0)
					 ,@errandLeave = @errandLeave + isnull(@errandLeaveCase7H,0)
					 ,@sickLeave = @sickLeave + isnull(@sickLeaveCase7H,0);
			set @duringDayMsg=@duringDayMsg+'{{ลาระหว่างวัน('+convert(varchar,isnull(@minuteLeaveCase7,-1))+'บาท)}}' 
	end
	
	
	
	if @duringDayMsg <> ''
	begin 
		insert into @results(msg,totalDeduction,[absent],brake,leave,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut)
		values(@duringDayMsg,
		(round(@absent,0)+round(@brake,0)+round(@leave,0)+round(@lateAndEarly,0)+round(@notCheckInOrOut,0))
		,round(@absent,0),round(@brake,0),round(@leave,0),round(@lateAndEarly,0),round(@errandLeave,0),round(@sickLeave,0),round(@notCheckInOrOut,0));
		return 
	end
	
	set @duringDayMsg='!Error!'; set @totalDeduction=0.00;
	insert into @results(msg,
	leaveInMinute,
	leaveInDay,
	leaveDeduction,--0 = ไม่หัก, 1=หัก
	absenceInMinute,
	absenceInDay,
	lateInMinute,
	earlyInMinute,
	totalMinuteLatePlusEarly,
	totalDeduction,
	dummy)values(@duringDayMsg,
	@leaveInMinute,
	@leaveInDay,
	@leaveDeduction,--0 = ไม่หัก, 1=หัก
	@absenceInMinute,
	@absenceInDay,
	@lateInMinute,
	@earlyInMinute,
	@totalMinuteLatePlusEarly,
	@totalDeduction,
	'check')
	RETURN 
END