use EMPBase;
IF OBJECT_ID('tempdb.dbo.#Deductions', 'U') IS NOT NULL DROP TABLE #Deductions;
IF OBJECT_ID('tempdb.dbo.##finalResults', 'U') IS NOT NULL DROP TABLE ##finalResults;
declare @month varchar(max),@year varchar(max);
set @month='����Ҿѹ��'; set @year='2560'

select t2.uid,t2.uname,t2.DeptInfo,t2.sumTotal,
case when t2.DeptInfo='D999999' 
	then 
		case
			when t2.sumTotal between 6 and 11 then 200
			when t2.sumTotal > 11 then 500
			else 0
		end
 else sumTotal
end
netTotal
into #Deductions
from 
(
	select t.uid,t.uname,t.DeptInfo,SUM(t.total)sumTotal from 
	(
		select 
		dt.[uid]
		,uDed.uname+' '+ubr.bname uname
		,case when dt.typ in ('1','3') then 'D'+dept.DID 
		when dt.typ in ('2') then 'D999999'
		else '����բ�����'
		end
		DeptInfo
		--, sum(dt.total)sumTotal
		,dt.total
		from Detail dt
		left join users uDed on dt.uid=uDed.uid
		left join branch ubr on uDed.bid=ubr.bid
		left join users uKey on dt.kid=uKey.uid
		left join dep dept on uKey.did=dept.DID
		where 1=1
		and dt.monthly=@month and dt.yearly=@year
	) t
	group by t.uid,t.uname,t.DeptInfo
)t2
where 1=1
--and uid='003984'
declare @columnsForSelect nvarchar(max),@columnsForPivot nvarchar(max),@sumEachRow nvarchar(max), @sumAtBottoms nvarchar(max);
set @columnsForSelect = STUFF((SELECT distinct ',isnull(' + QUOTENAME(c.DeptInfo)+',0)'+c.DeptInfo FROM #Deductions c FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') ,1,1,'')
set @columnsForPivot = STUFF((SELECT distinct ',' + QUOTENAME(c.DeptInfo)+'' FROM #Deductions c FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') ,1,1,'')
set @sumEachRow =',('+ STUFF((SELECT distinct '+isnull(' + QUOTENAME(c.DeptInfo)+',0)' FROM #Deductions c FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') ,1,1,'')+')sumAll'

set @sumAtBottoms = STUFF((SELECT distinct ',sum(' + QUOTENAME(c.DeptInfo)+')'+c.DeptInfo FROM #Deductions c FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') ,1,1,'') + ',sum(sumAll)sumAll';

print @columnsForPivot;
print @columnsForSelect;
print @sumEachRow;
print 'sumAtbottom ==>'+ @sumAtBottoms;

declare @sqlStr nvarchar(max);
set @sqlStr = '

select * into ##finalResults
from (
	select uid,uname, '+@columnsForSelect+' '+@sumEachRow+'
	from 
	(
		select uid,uname,DeptInfo,SUM(netTotal)netTotal  from #Deductions group by uid,uname,DeptInfo
	) t pivot (
		sum(netTotal) for DeptInfo in ('+@columnsForPivot+')
	)p

	union
	select ''�'' uid, ''����'' uname, '+@sumAtBottoms+'
	from (
		select uid,uname, '+@columnsForSelect+' '+@sumEachRow+'
		from 
		(
			select uid,uname,DeptInfo,SUM(netTotal)netTotal  from #Deductions group by uid,uname,DeptInfo
		) t pivot (
			sum(netTotal) for DeptInfo in ('+@columnsForPivot+')
		)p
	) t 

)allu 
';
EXEC sp_executesql @sqlStr;

select * from ##finalResults order by uid

