use EMPBase;
--select * from hrm_humanoPersonAll where firstName ='˹��ķ��'
--select * from NDUsers where Fname='˹��ķ��'
--select * from HIINCOME.dbo.PAYFOR
/*
select eeas.empShiftId,eeas.shiftId,lt.name,el.* from ta_employeeLeavesCalTime el left join ta_leaveTypes lt on el.leaveId=lt.id
left join ta_earlyEmpActiveShift eeas on el.IDNo=eeas.IDNo and CONVERT(varchar,startDateTime,111) between eeas.activeFrom and eeas.activeTo

where startDateTime>='20171022' 
and CONVERT(varchar,startDateTime,111)=CONVERT(varchar,endDateTime,111)
and leaveDay<1 
and formated='O'
and DATEPART(hour,endDateTime)<17 and DATEPART(hour,startDateTime)>8
*/
--select CONVERT(varchar,GETDATE(),111)

IF OBJECT_ID('tempdb..#mainTa') IS NOT NULL DROP TABLE  #mainTa;
create table #mainTa (infoType varchar(max),info varchar(max));

declare @cDate datetime, @IDNo varchar(20);
select @cDate='20171121',@IDNo='3940200536679';
--�Чҹ 
insert into #mainTa (infoType,info) 
select '�Чҹ' infoType, shiftName info from ta_earlyEmpActiveShift eeas where IDNo=@IDNo and @cDate between activeFrom and activeTo and active='Y'
--����᡹����
insert into #mainTa (infoType,info) select '���ŧ����' infoType, STUFF((SELECT distinct ' ' +  substring(CONVERT(varchar,ST1.CHECKTIME,108),1,8)FROM ta_empTaDetails ST1 Where ST1.USERID=(select USERID from ta_userInfos where SSN=@IDNo) and CONVERT(varchar,ST1.CHECKTIME,111)=@cDate FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'' ) info

--�������ŧ���� +�����˵�
insert into #mainTa (infoType,info) 
select '�͹��ѵ������ŧ����' infoType, 
''+
replace(
+case when checkIn='Y' then '(���)' else'' end
+case when brakeStart='Y' then '(仾ѡ)' else'' end
+case when brakeEnd='Y' then '(��Ѻ�ҡ�ѡ)' else'' end
+case when checkOut='Y' then '(�͡)' else'' end
,')(',',')
+ '/�˵ؼ�:' 
+ reasonWhy+'/'+h.firstName+' '+h.lastName+'(͹��ѵ�)'
info
from ta_empNotAttendances ena left join hrm_humanoPersonAdjust h on ena.approvedBy=h.IDNo where ena.IDNo=@IDNo and @cDate between ena.fromDate and ena.toDate and approved='Y'
--����� 
insert into #mainTa (infoType,info) 
select 
'����ҧҹ' infoType
,'�ҡ����:'+CONVERT(varchar,startDateTime,111)+' �֧ '+CONVERT(varchar,endDateTime,111)+' /'+lt.name+'/'+el.comments info
from ta_employeeLeavesCalTime el 
left join ta_leaveTypes lt on el.leaveId=lt.id
left join ta_earlyEmpActiveShift eea on el.IDNo=eea.IDNo and @cDate between eea.activeFrom and eea.activeTo
left join ta_shiftsView sv on eea.shiftId=sv.id
where el.IDNo=@IDNo 
and el.startDateTime<=@cDate+sv.startTime
and el.endDateTime>=@cDate+sv.endTime
and el.approveResult='Y'
--�����Ѻ�ѹ��ش���� ��ش�ѹ���
insert into #mainTa (infoType,info) 
select '��Ѻ�ѹ��ش/��ش�ѹ���' inforType, convert(varchar,hws.orginalDate,111)+' '+hws.sSubstituteType+' '+hws.reasonWhy+' '+hws.approverComments info
from ta_hwSubstutionsV2 hws 
left join ta_empForHWGs ehw on hws.empHwId=ehw.id
where hws.substituteWith=@cDate and hws.approved='Y' and hws.[status]=1 and ehw.IDNo=@IDNo
--�����Ѻ�ѹ��ش���� �����ش�ѹ���
insert into #mainTa (infoType,info) 
select '��Ѻ�ѹ��ش/�����ش�ѹ���' inforType, convert(varchar,hws.orginalDate,111)+' '+hws.sSubstituteType+' '+hws.reasonWhy+' '+hws.approverComments info
from ta_hwSubstutionsV2 hws 
left join ta_empForHWGs ehw on hws.empHwId=ehw.id
where hws.orginalDate=@cDate and hws.approved='Y' and hws.[status]=1 and ehw.IDNo=@IDNo

select idx.infoType,isnull(info,'-')info from (	
	select 1 idx,'�Чҹ' infoType 
	union select 2 idx,'���ŧ����' infoType 
	union select 3,'�͹��ѵ������ŧ����' infoType 
	union select 4,'����ҧҹ' infoType 
	union select 5,'��Ѻ�ѹ��ش/��ش�ѹ���' infoType 
	union select 6,'��Ѻ�ѹ��ش/�����ش�ѹ���' infoType 	
) idx left join #mainTa mt on idx.infoType=mt.infoType
order by idx.idx