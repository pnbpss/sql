use EMPBase;
			set nocount on;
			IF OBJECT_ID('tempdb..#firstCalculation') IS NOT NULL DROP TABLE #firstCalculation
			IF OBJECT_ID('tempdb..#firstCalculation2') IS NOT NULL DROP TABLE #firstCalculation2

			IF OBJECT_ID('tempdb..#allPersonTABetweenSpecfiedDate') IS NOT NULL DROP TABLE #allPersonTABetweenSpecfiedDate


			create table #allPersonTABetweenSpecfiedDate (USERID int,CHECKTIME datetime,SENSORID nvarchar(5))

			--IF OBJECT_ID('tempdb..#ta_userInfos') IS NOT NULL DROP TABLE #ta_userInfos create table #ta_userInfos (USERID int,SSN varchar(50))

			-- table �ͧ�ѹ�������ͧ��� ����� startDate �֧ endDate
			declare @daysForCheckTa as table (dt datetime,dow varchar(20) collate Thai_CI_AS);

			--���ҧ temporary �ͧ����᡹��������������ӹǳ㹿ѧ���� �ͧ userid ���� Ẻ type (temp_ta_empTaDetails �� type)
			declare @temp_ta_empTaDetails as temp_ta_empTaDetails

			declare @month int , @year int, @startDate datetime, @endDate datetime,@IDNo varchar(20),@personInfos varchar(max),@cDate datetime, @cdow varchar(20),@msg varchar(max);
			declare @empHW char(1)
			,@assignedHwToEmp char(1)
			,@checkInOutExists char(1)
			,@noNeedAttdnShift char(1)
			,@assignedShiftToEmp char(1)
			,@empAllDayLeaveRequest char(1)
			,@empReqNoAllDayAttnd char(1)
			,@empHSubstitution char(1)
			,@completedAttndInDay char(1)
			,@noDeductionLeave char(1)

			,@isCheckin char(1) --�᡹������
			,@isLate char(1) --������
			,@minuteLate int --��¡��ҷ�
			,@requestNotAttdnCheckIn char(1) --��͹��ѵ�����᡹��������������
			,@requestLeaveAsLate char(1) --��͹��ѵ��ҵ���������������

			,@isCheckout char(1) --�᡹�͡�������
			,@isEarly  char(1) --�͡��͹�������
			,@minuteEarly int --�ӹǹ�ҷշ���͡��͹
			,@requuestNotAttdnCheckOut char(1) --�ҡ������᡹�͡��͹��ѵ������������
			,@requestLeaveAsEarly char(1)--�ҵ������᡹�͡��͹�������

			,@isCheckBrake char(1)
			,@brakeDuration int--�ӹǹ�ҷշ��仾ѡ
			,@brakeEarlyOrLate char(1)--仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
			,@minuteBrakeEarlyOrLate int--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ

			,@minuteLeaveDuringBrake int --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
			,@paymentOfLeaveDuringBrake char(1) --�ѡ����������ҧ�ѡ�������
			,@checkTimeForLeaveDuringBrakeOK char(1) --�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ

			,@stateOfCal char(6)
			,@dummy varchar(500);
			create table #firstCalculation (
				IDNo varchar(20)
				,personInfo varchar(250) collate Thai_CI_AS
				,empHW char(1)
				,assignedHwToEmp char(1)
				,checkInOutExists char(1)
				,noNeedAttdnShift char(1)
				,assignedShiftToEmp char(1)
				,empAllDayLeaveRequest char(1)
				,empReqNoAllDayAttnd char(1)
				,empHSubstitution char(1)
				,noDeductionLeave char(1)
				,completedAttndInDay char(1)
				,cDate datetime
				,cdow varchar(30) collate Thai_CI_AS
				
				,isCheckin char(1) --�᡹������
				,isLate char(1) --������
				,minuteLate int --��¡��ҷ�
				,requestNotAttdnCheckIn char(1) --��͹��ѵ�����᡹��������������
				,requestLeaveAsLate char(1)--��͹��ѵ��ҵ���������������
				
				,isCheckout char(1) --�᡹�͡�������
				,isEarly  char(1) --�͡��͹�������
				,minuteEarly int --�ӹǹ�ҷշ���͡��͹
				,requuestNotAttdnCheckOut char(1) --�ҡ������᡹�͡��͹��ѵ������������
				,requestLeaveAsEarly char(1)--�ҵ������᡹�͡�͹�������
				
				,isCheckBrake char(1) --�᡹�ѡ�������
				,brakeDuration int--�ӹǹ�ҷշ��仾ѡ
				,brakeEarlyOrLate char(1)--仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
				,minuteBrakeEarlyOrLate int--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
				
				,minuteLeaveDuringBrake int --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
				,paymentOfLeaveDuringBrake char(1) --�ѡ����������ҧ�ѡ�������
				,checkTimeForLeaveDuringBrakeOK char(1) --�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ
				
				,stateOfCal char(6)
				,dummy varchar(500)
			)
			--��͹��лշ��Фӹǳ
			set @year=2017; set @month=9;

			--��˹��ѹ�������/����ش���ʹ֧�������Ҩҡ�к�ŧ���� 
			set @startDate = dateadd(day,-10,convert(varchar,@year)+'/'+convert(varchar,@month)+'/01'); 
			set @endDate=dateadd(day,-1,dateadd(month,1,convert(varchar,@year)+'/'+convert(varchar,@month)+'/01')); 
			set @endDate= case when @endDate>GETDATE() then CONVERT(varchar,GETDATE(),111) else @endDate end
			--select @startDate,@endDate;

			--select USERID,CHECKTIME,SENSORID from ta_empTaDetails where CHECKTIME between @startDate and dateadd(day,1,@endDate)
			set @msg = '';

			insert into #allPersonTABetweenSpecfiedDate select USERID,CHECKTIME,SENSORID from ta_empTaDetails where CHECKTIME between @startDate and dateadd(day,1,@endDate)
			create index allPersonTABetweenSpecfiedDateIdx on #allPersonTABetweenSpecfiedDate (USERID,CHECKTIME);

			--loop �ͧ���Ф�
			declare personCS cursor READ_ONLY 
			for 
			select IDNo,titleName+firstName+' '+lastName
			--+'/'+positionName+'/'+corpName 
			empInfo from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
			where 1=1
			and  (em_status in ('W','P')  or ((em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)))) --��������͡����� ���ѹ���͡�ҡ���� �ѹ�á�ͧ�ͺ�Թ��͹���ӹǳ
			and IDNo not in (select IDNo from hrm_humanoPersonAll where em_status in ('W','P') group by IDNo having count(*)>1)
			and employeeCode in (select c.employeeCode from hrm_Commanders c inner join hrm_humanoPersonAll h on c.cEmpCode=h.employeeCode where h.em_status in ('W','P') and h.IDNo='3920600717310' and [rank] in (0,1)) -- ੾���١��ͧ�ͧ...
			--and employeeCode in ('TJPAT000065','RJYR000101','TJPAT000003','TJPAT000067','TJPAT000071','TJY40000010','TLL00000082','TLL00000123','TLL00000147','TLL00000220','TLL00000288','TLL00000453','TLL00000652','YKRY000043','YKRY000049','YKRY000099','YKRY000105','YKRY000204','YKRY000218','YN00000005','YN00000048')

			and  period_no not in ('PPK16C800000052')


			OPEN personCS
			fetch next from personCS into @IDNo,@personInfos
			while @@FETCH_STATUS = 0  
			begin
				
				delete from @temp_ta_empTaDetails; --�������ҷ�駡�͹ ����������Ѻ �֧�㹡�äӹǳ������
				insert into @temp_ta_empTaDetails select USERID,CHECKTIME,SENSORID from #allPersonTABetweenSpecfiedDate where USERID in (select USERID from ta_userInfos where SSN=@IDNo) and CHECKTIME between @startDate and @endDate
				
				
				--�֧ �ѹ������鹵���ͺ��äӹǳ�Թ��͹�ͧ���Ф�
				select 
				@startDate = dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)) 
				--,@endDate = dateadd(minute,59,dateadd(hour,23,dateadd(month,pe_endmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_end_date))))
				,@endDate = dateadd(minute,59,dateadd(hour,23,dateadd(month,pe_endmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,case when pe_end_date>DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1)))) then DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1))))else pe_end_date end))))
				from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id 	where h.IDNo=@IDNo
				
				set @endDate= case when @endDate>GETDATE() then CONVERT(varchar,GETDATE(),111) else @endDate end --�ҡ�ѹ�ش�����ҡ�����ѹ�Ѩ�غѹ ����ѹ�ش������ҡѺ�ѹ�Ѩ�غѹ
				
				--set @endDate = DATEADD(day,2,@endDate);
				
				-- �֧�ѹ�͡������ table @daysForCheckTa
				delete from @daysForCheckTa; insert into @daysForCheckTa select allDate.dt,allDate.dow from dbo.FN_dayList(@startDate,@endDate) allDate
				
				
				declare dayCS cursor READ_ONLY 
				for select dt,dow from @daysForCheckTa order by dt 
				OPEN dayCS
				fetch next from dayCS into @cDate,@cdow
				--print @IDNo+','+ @personInfos;
				while @@FETCH_STATUS = 0  
				begin
					--print @cdow
					set @msg = convert(varchar,@cDate,111)+' '+@cdow+' ';
					
					--�֧�����š�� ��� ���ѹ��ش, �ա���᡹����, �����, ����Ѻ�ѹ��ش ��ѹ����������
					select @empHW=empHW
							,@assignedHwToEmp=assignedHwToEmp
							,@checkInOutExists=checkInOutExists
							,@noNeedAttdnShift=noNeedAttdnShift
							,@assignedShiftToEmp=assignedShiftToEmp
							,@empAllDayLeaveRequest=empAllDayLeaveRequest
							,@empReqNoAllDayAttnd=empReqNoAllDayAttnd
							,@empHSubstitution=empHSubstitution 
							,@completedAttndInDay=completedAttndInDay
							,@noDeductionLeave=noDeductionLeave
							,@isCheckin = isCheckin--�᡹������
							,@isLate=isLate --������
							,@minuteLate=minuteLate  --��¡��ҷ�
							,@requestNotAttdnCheckIn=requestNotAttdnCheckIn --��͹��ѵ�����᡹��������������
							,@requestLeaveAsLate=requestLeaveAsLate--��͹��ѵ��ҵ���������������
							,@isCheckout=isCheckout --�᡹�͡�������
							,@isEarly=isEarly --�͡��͹�������
							,@minuteEarly=minuteEarly --�ӹǹ�ҷշ���͡��͹
							,@requuestNotAttdnCheckOut=requuestNotAttdnCheckOut --�ҡ������᡹�͡��͹��ѵ������������
							,@requestLeaveAsEarly=requestLeaveAsEarly--�ҵ������᡹�͡�͹�������				
							,@isCheckBrake=isCheckBrake
							,@brakeDuration = brakeDuration--�ӹǹ�ҷշ��仾ѡ
							,@brakeEarlyOrLate = brakeEarlyOrLate--仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
							,@minuteBrakeEarlyOrLate =minuteBrakeEarlyOrLate--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
							,@minuteLeaveDuringBrake=minuteLeaveDuringBrake --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
							,@paymentOfLeaveDuringBrake=paymentOfLeaveDuringBrake --�ѡ����������ҧ�ѡ�������
							,@checkTimeForLeaveDuringBrakeOK=checkTimeForLeaveDuringBrakeOK --�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ
							,@stateOfCal=stateOfCal
							,@dummy=dummy
					from dbo.FN_001_EmpEachDayInfo(@cDate,@IDNo,@temp_ta_empTaDetails);		
					
					insert into #firstCalculation (
						IDNo,personInfo
						,empHW
						,assignedHwToEmp			
						,assignedShiftToEmp
						,noNeedAttdnShift	
						,checkInOutExists	
						,completedAttndInDay	
						,empAllDayLeaveRequest
						,empReqNoAllDayAttnd
						,empHSubstitution
						,noDeductionLeave
						,cDate
						,cdow
						,isCheckin--�᡹������
						,isLate --������
						,minuteLate  --��¡��ҷ�
						,requestNotAttdnCheckIn --��͹��ѵ�����᡹��������������
						,requestLeaveAsLate --��͹��ѵ��ҵ���������������
						,isCheckout --�᡹�͡�������
						,isEarly  --�͡��͹�������
						,minuteEarly --�ӹǹ�ҷշ���͡��͹
						,requuestNotAttdnCheckOut --�ҡ������᡹�͡��͹��ѵ������������
						,requestLeaveAsEarly--�ҵ������᡹�͡�͹�������
						,isCheckBrake --�᡹�ѡ�������
						,brakeDuration--�ӹǹ�ҷշ��仾ѡ
						,brakeEarlyOrLate--仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
						,minuteBrakeEarlyOrLate--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
						,minuteLeaveDuringBrake --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
						,paymentOfLeaveDuringBrake --�ѡ����������ҧ�ѡ�������
						,checkTimeForLeaveDuringBrakeOK --�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ
						,stateOfCal
						,dummy
					)values(
						@IDNo,@personInfos
						,@empHW
						,@assignedHwToEmp			
						,@assignedShiftToEmp
						,@noNeedAttdnShift	
						,@checkInOutExists	
						,@completedAttndInDay	
						,@empAllDayLeaveRequest
						,@empReqNoAllDayAttnd
						,@empHSubstitution
						,@noDeductionLeave
						,@cDate
						,@cdow
						,@isCheckin--�᡹������
						,@isLate --������
						,@minuteLate  --��¡��ҷ�
						,@requestNotAttdnCheckIn --��͹��ѵ�����᡹��������������
						,@requestLeaveAsLate --��͹��ѵ��ҵ���������������
						,@isCheckout --�᡹�͡�������
						,@isEarly --�͡��͹�������
						,@minuteEarly --�ӹǹ�ҷշ���͡��͹
						,@requuestNotAttdnCheckOut --�ҡ������᡹�͡��͹��ѵ������������
						,@requestLeaveAsEarly--�ҵ������᡹�͡�͹�������
						,@isCheckBrake
						,@brakeDuration --�ӹǹ�ҷշ��仾ѡ
						,@brakeEarlyOrLate --仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
						,@minuteBrakeEarlyOrLate --�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
						,@minuteLeaveDuringBrake --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
						,@paymentOfLeaveDuringBrake --�ѡ����������ҧ�ѡ�������
						,@checkTimeForLeaveDuringBrakeOK --�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ
						,@stateOfCal
						,@dummy
					)
					fetch next from dayCS into @cDate,@cdow
				end --while
				close dayCS;  
				deallocate dayCS; 
				fetch next from personCS into @IDNo,@personInfos
			end--while ���Ф�
			close personCS;  
			deallocate personCS; 
			--�� loop �ͧ���Ф�	

select mm.IDNo,personInfo
					/*--,dbo.FN_ConvertDate(cDate,'D/M/Y')cDate*/
					,CONVERT(varchar,cDate,111) cDate
					,cdow dow
					,replace(replace(replace(tsc.msg+case when empHW='Y' then '{{�ѹ��ش}}' else '' end,'}}{{',','),'{{',''),'}}','')	msg
					,tsc.totalDeduction		
					,stateOfCal soc--ʶҹС�äӹǳ
					,brakeDuration bdr --�ӹǹ�ҷշ��仾ѡ
					,isCheckin ici--�᡹������
					,isLate il --������
					,minuteLate mnl --��¡��ҷ�
					,requestNotAttdnCheckIn rnaci--��͹��ѵ�����᡹����������
					,requestLeaveAsLate rlal--��͹��ѵ��ҵ���������������
					,isCheckout ico--�᡹�͡�������
					,isEarly  iel--�͡��͹�������
					,minuteEarly mel --�ӹǹ�ҷշ���͡��͹
					,requuestNotAttdnCheckOut rnaco --�ҡ������᡹�͡��͹��ѵ������������
					,requestLeaveAsEarly rlae--�ҵ������᡹�͡�͹�������
					,isCheckBrake icb --ŧ���Ҿѡ���§�ú����������͡
					
					,brakeEarlyOrLate beo --仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
					,minuteBrakeEarlyOrLate mbeo--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
					,assignedHwToEmp ahwte --��˹��ѹ��ش�������
					,empHW hw  --�ѹ��ش�������
					,assignedShiftToEmp ashte --��˹��Чҹ��������
					
					,noNeedAttdnShift nnash --�繡Чҹ�������ͧŧ����
					,checkInOutExists cioe --�ա��ŧ������ѹ���
					,completedAttndInDay	 catdid --ŧ������ѹ��鹤ú��� ��� �ѡ ����͡
					,empAllDayLeaveRequest eadlr --���ҧҹ����ѹ�������
					,empReqNoAllDayAttnd ernadatd --�����ŧ���ҷ���ѹ�������
					,empHSubstitution ehs --����Ѻ�ѹ��ش�������
					,noDeductionLeave ndl --�繡����Ẻ����ѡ
					,minuteLeaveDuringBrake mldb --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
					,paymentOfLeaveDuringBrake poldb --�ѡ����������ҧ�ѡ�������(�ѧ������)
					,checkTimeForLeaveDuringBrakeOK ctfldbo--�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ(�ѧ������)
					,h.workEnd
					,tsc.[absent],tsc.[leave],tsc.[brake],tsc.[lateAndEarly],tsc.errandLeave,tsc.sickLeave
					--,dummy
					into #firstCalculation2
			from #firstCalculation mm left join  hrm_humanoPersonAll h on mm.IDNO=h.IDNo collate Thai_CI_AS and h.em_status in ('W','P') 
			cross apply FN_001_translateStateOfCal(mm.IDNo,stateOfCal,cDate,minuteLate,minuteEarly,minuteLeaveDuringBrake,minuteBrakeEarlyOrLate,paymentOfLeaveDuringBrake,h.workStart,h.workEnd) tsc


select 
IDNo,personInfo,cDate,dow,msg,totalDeduction ttd
,isnull([absent],0.00)[absent]
,isnull([leave],0.00)leave
,isnull([brake],0.00)brake
,isnull([lateAndEarly],0.00)lateAndEarly
,isnull(errandLeave,0.00)errandLeave
,isnull(sickLeave,0.00)sickLeave
,soc,bdr,ici,il,mnl,rnaci,rlal,ico,iel,mel,rnaco,rlae,icb,beo,mbeo,ahwte,hw,ashte,nnash,cioe,catdid,eadlr,ernadatd,ehs,ndl,mldb,poldb,ctfldbo,workEnd
from #firstCalculation2

union 

select 
'���' IDNo,	personInfo personInfo,	null cDate,	null dow,	null msg,	cast(ceiling(isnull(sum(totalDeduction),0)) AS decimal(18,2))  ttd
,isnull(sum([absent]),0)[absent]
,isnull(sum([leave]),0)leave
,isnull(sum([brake]),0)brake
,isnull(sum([lateAndEarly]),0)lateAndEarly
,isnull(sum(errandLeave),0)errandLeave
,isnull(sum(sickLeave),0)sickLeave
,null soc,null bdr,null ici,null il,null mnl,null rnaci,null rlal,null ico,null iel,null mel,null rnaco,null rlae,null icb,null beo,null mbeo,null ahwte,null hw,null ashte,null nnash,null cioe,null catdid,null eadlr,null ernadatd,null ehs,null ndl,null mldb,null poldb,null ctfldbo,null workEnd
from #firstCalculation2
group by IDNo, personInfo
order by personInfo,IDNo, cDate 			