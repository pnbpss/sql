/****** Script for SelectTopNRows command from SSMS  ******/
use empbase;
SELECT
	  1 xx
	  -- ,[id]
      ,[employeeCode]
      ,[titleName]
      ,[firstName]
      ,[lastName]
      ,isnull([nick],'')nickName
      --,[gender]
      --,[birthdate]
      --,[workStart]
      --,[workEnd]
      --,[IDNo]
      ,[positionName]
      ,[corpName]
      ,[officeName]
     -- ,[uname]
     -- ,[hrmCorpId]
      --,[em_status]
      --,[period_no]
      --,[ssoBranchId]
      ,isnull([email],'')email
      ,isnull([mobile],'')mobileNo
  FROM [EMPBase].[dbo].[hrm_humanoPersonAll]
  where em_status in ('W','P','RR')
  and not (titleName='นาย' and positionName='พนักงานเร่งรัดหนี้สิน')
  and not (corpName='ทีแอลแอล โลจิสติกส์ บจก.' and positionName like '%พนักงานขับรถ%')
  --and mobile is not null
  --and lastName like '%ชัย%'
  
  --select * from hrm_humanoPersonAll where employeeCode='YK00000001'