USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_EmpEachDayInfo]    Script Date: 08/20/2017 08:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_EmpEachDayInfo] 
(
	-- Add the parameters for the function here
	@cDate datetime, @IDNo varchar(20),@temp_ta_empTaDetails temp_ta_empTaDetails readonly
)
RETURNS 
@EmpEachDayInfo TABLE 
(
	-- Add the column definitions for the TABLE variable here
	empHW char(1)
	,assignedHwToEmp char(1)
	,empCheckInOut char(1)
	,noNeedAttdnShift char(1)
	,assignedShiftToEmp char(1)
	,empAllDayLeaveRequest char(1)
	,empReqNoAllDayAttnd char(1)
	,empHSubstitution char(1)
	,completedAttndInDay char(1)
	,stateOfCal int
)
AS
BEGIN	
	-- Fill the table variable with the rows for your result set
	declare 
	@empHW char(1) --วันหยุดหรือไม่ 
	, @assignedHwToEmp char(1) --กำหนดข้อมูลวันหยุดให้แล้ว
	, @empCheckInOut char(1) --มีการสแกนนิ้วหรือไม่
	, @noNeedAttdnShift char(1) --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	, @assignedShiftToEmp char(1) --กำหนดกะงานในวันนี้ให้แล้ว
	, @empAllDayLeaveRequest char(1) --มีขอลาหยุดแบบทั้งวันหรือไม่
	, @empReqNoAllDayAttnd char(1) --มีขอไม่สแกนทั้งวันหรือไม่
	, @empHSubstitution char(1) --มีการขอสลับวันหยุด	
	, @completedAttndInDay char(1) --สแกนครบตามกะงาน 
	, @stateOfCal int --สถานะการประมวลผล
	;	
	declare @ta_empTaDetails table (USERID int,CHECKTIME datetime,SENSORID nvarchar(5));
	
	/*
	set @empHW='N'; --วันหยุดหรือไม่
	set @assignedHwToEmp = 'N' --กำหนดข้อมูลวันหยุดให้แล้ว
	set @empCheckInOut='N'; --มีการสแกนนิ้วหรือไม่
	set @noNeedAttdnShift  = 'N' --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	set @assignedShiftToEmp = 'N' --กำหนดกะงานในวันนี้ให้แล้ว
	set @empAllDayLeaveRequest='N'; --มีขอลาหยุดแบบทั้งวันหรือไม่
	set @empReqNoAllDayAttnd = 'N'; --มีขอไม่สแกนนิ้วทั้งวันหรือไม่
	set @empHSubstitution='N'; --มีการขอสลับวันหยุด
	set @completedAttndInDay='N'; --สแกนครบตามกะงาน 
	set @stateOfCal = 0;
	*/
	
	set @empHW=Null; --วันหยุดหรือไม่
	set @assignedHwToEmp = Null --กำหนดข้อมูลวันหยุดให้แล้ว
	set @empCheckInOut=Null; --มีการสแกนนิ้วหรือไม่
	set @noNeedAttdnShift  = Null --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	set @assignedShiftToEmp = Null --กำหนดกะงานในวันนี้ให้แล้ว
	set @empAllDayLeaveRequest=Null; --มีขอลาหยุดแบบทั้งวันหรือไม่
	set @empReqNoAllDayAttnd = Null; --มีขอไม่สแกนนิ้วทั้งวันหรือไม่
	set @empHSubstitution=Null; --มีการขอสลับวันหยุด
	set @completedAttndInDay=Null; --สแกนครบตามกะงาน 
	set @stateOfCal = 0;

	--กำหนดข้อมูลวันหยุดให้แล้ว เริ่ม
	if exists(select 1 from ta_empForHWGs ehw inner join ta_hwGroups hwg on ehw.hwGroupId=hwg.id where IDNo=@IDNo and hwg.[year]=year(@cDate))
	begin 
		set @assignedHwToEmp='Y' 	
		--เป็นวันหยุด เริ่ม
		if exists(select 1 FROM  [ta_empForHWGs] ehw inner join ta_hws m on ehw.hwGroupId=m.hwGroupId where IDNo=@IDNo and startDate=@cDate) begin set @empHW='Y' end
		--เป็นวันหยุด จบ
		else
		begin	
			set @empHW='N'
			--กำหนดกะงานให้แล้วหรือไม่ เริ่ม
			if exists(select 1 from ta_shiftForEmployees sh4emp where @cDate between sh4emp.activeFrom and sh4emp.activeTo and sh4emp.active='Y' and sh4emp.IDNo=@IDNo) 
			begin
				set @assignedShiftToEmp='Y'
				--เช็คว่าใช้กะงานแบบไม่ต้องสแกนหรือไม่ เริ่ม
				if exists(select sh.id from ta_shiftForEmployees sh4emp inner join ta_shifts sh on sh4emp.shiftId=sh.id where musTA='N' and @cDate between sh4emp.activeFrom and sh4emp.activeTo and sh4emp.active='Y' and sh4emp.IDNo=@IDNo)  
				begin 
					set @noNeedAttdnShift  = 'Y' 
					--มีการลางานหรือไม่หรือไม่ เริ่ม(กรณีที่กะงานไม่ต้องสแกน)
					if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and convert(varchar,@cDate,111) between convert(varchar,startDateTime,111) and convert(varchar,endDateTime,111))
					begin set @stateOfCal=1 end
					--มีการลางานหรือไม่หรือไม่ จบ(กรณีที่กะงานไม่ต้องสแกน)					
				end
				else 
				begin
					set @noNeedAttdnShift  = 'N'
						--มีสแกนนิ้วหรือไม่ เริ่ม		
						if exists(select 1 from @temp_ta_empTaDetails etd where etd.CHECKTIME between @cDate and dateadd(MILLISECOND,-2,dateadd(day,1,convert(datetime,@cDate)))) 
						begin 
							set @empCheckInOut='Y'; 
							--สแกนครบตามที่กำหนดในกะงานหรือไม่ เริ่ม
							if (select checkIn+checkBrake+checkOut from FN_001_01_completedDailyAttdnCheck(@cDate,@IDNo,@temp_ta_empTaDetails))='YYY'
							begin set @completedAttndInDay ='Y' end 
							else
							begin set @completedAttndInDay ='N' end 
							--สแกนครบตามที่กำหนดในกะงานหรือไม่ จบ
						end
						else 
						begin
								set @empCheckInOut='N';
								--ขออนุมัติไม่ได้สแกนทั้งวันหรือไม่ เริ่ม	
								if exists(select 1 from ta_empNotAttendances where IDNo=@IDNo and @cDate between fromDate and toDate and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and approvedDate is not null and approved='Y' and [status]<>'D')
								begin set @empReqNoAllDayAttnd='Y' end
								else 
								begin
									begin set @empReqNoAllDayAttnd='N' end
									--ขอสลับวันหยุดหรือไม่							
									if exists(select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id left join ta_hwSubstutions hs on ehg.id=hs.empHwId and hw.id=hs.hwId	where ehg.IDNo=@IDNo  and hg.year=year(@cDate) and ehg.IDNo=@IDNo and hs.approvedDate is not null and hs.approved='Y' and hs.substituteWith=@cDate)
									begin set @empHSubstitution = 'Y' end
									else
									begin set @stateOfCal=2;set @empHSubstitution = 'N' end
									--ขอสลับวันหยุดหรือไม่ จบ
								end
								--ขออนุมัติไม่ได้สแกนทั้งวันหรือไม่ จบ
						end
						--มีสแกนนิ้วหรือไม่ จบ
				end
				--เช็คว่าใช้กะงานแบบไม่ต้องสแกนหรือไม่ จบ		
			end
			else
			begin
				set @assignedShiftToEmp='N'
			end
			--กำหนดกะงานให้แล้วหรือไม่ จบ
			
			--มีใบลางานแบบทั้งวันหรือไม่ เริ่ม
			declare @startOfShift time, @endOfShift time; 
			select @startOfShift =sh.startTime,@endOfShift = isnull(sh.endTime,DATEADD(hour,9,sh.startTime)) from ta_shiftForEmployees esh inner join ta_shifts sh on esh.shiftId=sh.id where esh.IDNo=@IDNo and @cDate between esh.activeFrom and dateadd(minute,59,DATEADD(hour,23,esh.activeTo)) and esh.active='Y'
			if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and @cDate+@startOfShift between startDateTime and endDateTime and @cDate+@endOfShift between startDateTime and endDateTime and DATEDIFF(hour, @cDate+@startOfShift, @cDate+@endOfShift)>=9 )
			begin set @empAllDayLeaveRequest='Y' end
			--มีใบลางานแบบทั้งวันหรือไม่ จบ
		end
	end
	--กำหนดข้อมูลวันหยุดให้แล้ว จบ
	
	
	insert into @EmpEachDayInfo select 
					@empHW
					,@assignedHwToEmp
					,@empCheckInOut
					,@noNeedAttdnShift
					,@assignedShiftToEmp
					,@empAllDayLeaveRequest
					,@empReqNoAllDayAttnd
					,@empHSubstitution
					,@completedAttndInDay
					,@stateOfCal
	RETURN 
END