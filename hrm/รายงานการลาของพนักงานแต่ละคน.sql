use empbase;
select el.id elId, hrequester.firstName+' '+hrequester.lastName requester,lt.name leaveName,happrover.firstName+' '+happrover.lastName approver
,el.IDNo,el.startDateTime,el.endDateTime,el.comments,el.approvedBy,el.approvedDate
,el.approverComments,el.submitDate,el.imagefile,el.status,el.insertBy,el.insertSince,el.lastUpdate,el.lastUpdateBy,el.ipaddress
from ta_employeeLeaves el 
left join ta_leaveTypes lt on el.leaveId=lt.id
left join hrm_humanoPersonAll hrequester on el.IDNo=hrequester.IDNo and hrequester.em_status in ('W','P')
left join hrm_humanoPersonAll happrover on el.approvedBy=happrover.id

where 1=1
--and DATEPART(hour,startDatetime)=8
and approvedDate is null
and el.[status]<>9
and el.startDateTime between '20170822' and '20170922'

--select * from hrm_humanoPersonAll where lastName='%%'

