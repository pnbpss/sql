use EMPBase;
IF OBJECT_ID('tempdb..#tempRpt') IS NOT NULL DROP TABLE #tempRpt
IF OBJECT_ID('tempdb..##Result1') IS NOT NULL DROP TABLE ##Result1
declare 
@fromDate datetime
,@toDate datetime
,@sumMonth01 int,@sumMonth02 int,@sumMonth03 int,@sumMonth04 int,@sumMonth05 int,@sumMonth06 int,@sumMonth07 int,@sumMonth08 int,@sumMonth09 int,
@sumMonth10 int,@sumMonth11 int,@sumMonth12 int;

--declare @tempRpt table (
create table #tempRpt
(
IDNo varchar(max),empName varchar(max),positionName varchar(max),officeName varchar(max), em_status varchar(max),
[IN_01] int,	[IN_02] int,	[IN_03] int,	[IN_04] int,	[IN_05] int,	[IN_06] int,	[IN_07] int,	[IN_08] int,	[IN_09] int,	[IN_10] int,	[IN_11] int,	[IN_12] int,
[BS_01] int,	[BS_02] int,	[BS_03] int,	[BS_04] int,	[BS_05] int,	[BS_06] int,	[BS_07] int,	[BS_08] int,	[BS_09] int,	[BS_10] int,	[BS_11] int,	[BS_12] int,
[BE_01] int,	[BE_02] int,	[BE_03] int,	[BE_04] int,	[BE_05] int,	[BE_06] int,	[BE_07] int,	[BE_08] int,	[BE_09] int,	[BE_10] int,	[BE_11] int,	[BE_12] int,
[OU_01] int,	[OU_02] int,	[OU_03] int,	[OU_04] int,	[OU_05] int,	[OU_06] int,	[OU_07] int,	[OU_08] int,	[OU_09] int,	[OU_10] int,	[OU_11] int,	[OU_12] int
);

select @fromDate = '20180101', @toDate ='20180331';

insert into #tempRpt
(IDNo,empName,positionName,officeName,em_status,
[IN_01]	,[BS_01]	,[BE_01]	,[OU_01]
,[IN_02]	,[BS_02]	,[BE_02]	,[OU_02]
,[IN_03]	,[BS_03]	,[BE_03]	,[OU_03]
,[IN_04]	,[BS_04]	,[BE_04]	,[OU_04]
,[IN_05]	,[BS_05]	,[BE_05]	,[OU_05]
,[IN_06]	,[BS_06]	,[BE_06]	,[OU_06]
,[IN_07]	,[BS_07]	,[BE_07]	,[OU_07]
,[IN_08]	,[BS_08]	,[BE_08]	,[OU_08]
,[IN_09]	,[BS_09]	,[BE_09]	,[OU_09]
,[IN_10]	,[BS_10]	,[BE_10]	,[OU_10]
,[IN_11]	,[BS_11]	,[BE_11]	,[OU_11]
,[IN_12]	,[BS_12]	,[BE_12]	,[OU_12]
)
select p.IDNo,h.titleName+h.firstName+' '+h.lastName empName,h.positionName,h.officeName,h.em_status,
isnull(IN_01,0)IN_01,	isnull(BS_01,0)BS_01,	isnull(BE_01,0)BE_01,	isnull(OU_01,0)OU_01,
isnull(IN_02,0)IN_02,	isnull(BS_02,0)BS_02,	isnull(BE_02,0)BE_02,	isnull(OU_02,0)OU_02,
isnull(IN_03,0)IN_03,	isnull(BS_03,0)BS_03,	isnull(BE_03,0)BE_03,	isnull(OU_03,0)OU_03,
isnull(IN_04,0)IN_04,	isnull(BS_04,0)BS_04,	isnull(BE_04,0)BE_04,	isnull(OU_04,0)OU_04,
isnull(IN_05,0)IN_05,	isnull(BS_05,0)BS_05,	isnull(BE_05,0)BE_05,	isnull(OU_05,0)OU_05,
isnull(IN_06,0)IN_06,	isnull(BS_06,0)BS_06,	isnull(BE_06,0)BE_06,	isnull(OU_06,0)OU_06,
isnull(IN_07,0)IN_07,	isnull(BS_07,0)BS_07,	isnull(BE_07,0)BE_07,	isnull(OU_07,0)OU_07,
isnull(IN_08,0)IN_08,	isnull(BS_08,0)BS_08,	isnull(BE_08,0)BE_08,	isnull(OU_08,0)OU_08,
isnull(IN_09,0)IN_09,	isnull(BS_09,0)BS_09,	isnull(BE_09,0)BE_09,	isnull(OU_09,0)OU_09,
isnull(IN_10,0)IN_10,	isnull(BS_10,0)BS_10,	isnull(BE_10,0)BE_10,	isnull(OU_10,0)OU_10,
isnull(IN_11,0)IN_11,	isnull(BS_11,0)BS_11,	isnull(BE_11,0)BE_11,	isnull(OU_11,0)OU_11,
isnull(IN_12,0)IN_12,	isnull(BS_12,0)BS_12,	isnull(BE_12,0)BE_12,	isnull(OU_12,0)OU_12

from 
(
	select 
	allu.IDNo,allu.checkType,allu.cnt 
	from (
		select 	
			ea.IDNO
			,'IN_'+right('00'+convert(varchar(2),month(dl.dt)),2) checkType
			--,right('00'+convert(varchar(2),month(dl.dt)),2) m
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.checkIn='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNO
			,'OU_'+right('00'+convert(varchar(2),month(dl.dt)),2) checkType
			--,right('00'+convert(varchar(2),month(dl.dt)),2) m
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.checkOut='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNO
			,'BS_'+right('00'+convert(varchar(2),month(dl.dt)),2) checkType
			--,right('00'+convert(varchar(2),month(dl.dt)),2) m
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.brakeStart='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
		union all
		select 	
			ea.IDNO
			,'BE_'+right('00'+convert(varchar(2),month(dl.dt)),2) checkType
			--,right('00'+convert(varchar(2),month(dl.dt)),2) m
			,COUNT(*) cnt
		from ta_empNotAttendances ea
		cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
		where 1=1	
		and dl.dt between @fromDate and @toDate
		and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
		and ea.brakeEnd='Y'
		group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
	)allu
) t pivot (
	max(cnt) for checkType in (
	[IN_01],	[IN_02],	[IN_03],	[IN_04],	[IN_05],	[IN_06],	[IN_07],	[IN_08],	[IN_09],	[IN_10],	[IN_11],	[IN_12],
	[BS_01],	[BS_02],	[BS_03],	[BS_04],	[BS_05],	[BS_06],	[BS_07],	[BS_08],	[BS_09],	[BS_10],	[BS_11],	[BS_12],
	[BE_01],	[BE_02],	[BE_03],	[BE_04],	[BE_05],	[BE_06],	[BE_07],	[BE_08],	[BE_09],	[BE_10],	[BE_11],	[BE_12],
	[OU_01],	[OU_02],	[OU_03],	[OU_04],	[OU_05],	[OU_06],	[OU_07],	[OU_08],	[OU_09],	[OU_10],	[OU_11],	[OU_12]
	)
) p
left join hrm_humanoPersonAdjust h on p.IDNo=h.IDNo
where 1=1
and h.em_status in ('W','P','RR')
--order by p.[IN] desc

declare @sqlstr nvarchar(max);
set @sqlstr=' select IDNo,empName,officeName+'''+'/'+ '''+positionName+'''+'/'+ '''+em_status officeInfo ';

if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=1) begin set @sqlstr=@sqlstr+',[IN_01],[BS_01],[BE_01],[OU_01]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=2) begin set @sqlstr=@sqlstr+',[IN_02],[BS_02],[BE_02],[OU_02]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=3) begin set @sqlstr=@sqlstr+',[IN_03],[BS_03],[BE_03],[OU_03]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=4) begin set @sqlstr=@sqlstr+',[IN_04],[BS_04],[BE_04],[OU_04]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=5) begin set @sqlstr=@sqlstr+',[IN_05],[BS_05],[BE_05],[OU_05]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=6) begin set @sqlstr=@sqlstr+',[IN_06],[BS_06],[BE_06],[OU_06]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=7) begin set @sqlstr=@sqlstr+',[IN_07],[BS_07],[BE_07],[OU_07]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=8) begin set @sqlstr=@sqlstr+',[IN_08],[BS_08],[BE_08],[OU_08]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=9) begin set @sqlstr=@sqlstr+',[IN_09],[BS_09],[BE_09],[OU_09]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=10) begin set @sqlstr=@sqlstr+',[IN_10],[BS_10],[BE_10],[OU_10]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=11) begin set @sqlstr=@sqlstr+',[IN_11],[BS_11],[BE_11],[OU_11]'; end
if exists(select distinct(MONTH(dt))m from dbo.FN_dayList(@fromDate,@toDate) where MONTH(dt)=12) begin set @sqlstr=@sqlstr+',[IN_12],[BS_12],[BE_12],[OU_12]'; end

set @sqlstr = @sqlstr+' 
,([IN_01]+[BS_01]+[BE_01]+[OU_01]
+[IN_02]+[BS_02]+[BE_02]+[OU_02]
+[IN_03]+[BS_03]+[BE_03]+[OU_03]
+[IN_04]+[BS_04]+[BE_04]+[OU_04]
+[IN_05]+[BS_05]+[BE_05]+[OU_05]
+[IN_06]+[BS_06]+[BE_06]+[OU_06]
+[IN_07]+[BS_07]+[BE_07]+[OU_07]
+[IN_08]+[BS_08]+[BE_08]+[OU_08]
+[IN_09]+[BS_09]+[BE_09]+[OU_09]
+[IN_10]+[BS_10]+[BE_10]+[OU_10]
+[IN_11]+[BS_11]+[BE_11]+[OU_11]
+[IN_12]+[BS_12]+[BE_12]+[OU_12]
) sumAll
into ##Result1
from #tempRpt 
';

Exec sp_executesql @sqlstr;

select * from ##Result1 order by officeInfo desc,sumAll desc 