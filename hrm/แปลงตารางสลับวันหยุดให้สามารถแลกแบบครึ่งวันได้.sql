
/*
		truncate table ta_hwSubstutionsV2
		insert into ta_hwSubstutionsV2 (empHwId,hwId,status,orginalDate,substituteWith
		,sSubstituteType
		,dSubstituteType
		,reasonWhy
		,approved,approvedBy,approvedDate,approverComments,insertSince,insertBy,lastUpdate,updateBy,ipaddress)
		select 
		empHwId,hwId,status,orginalDate,substituteWith
		,sType.t sSubstituteType
		,sType.t dSubstituteType
		,reasonWhy
		,approved,approvedBy,approvedDate,approverComments,insertSince,insertBy,lastUpdate,updateBy,ipaddress
		 from ta_hwSubstutions hs, (select 'M' t union select 'E' ) sType
*/

select hg.name hName, hg.[year],hw.id hwId,hw.hwGroupId,dbo.FN_ConvertDate(hw.startDate,'D/M/Y') hDate,hw.comments		
		,hw.startDate
		,ehg.id empHwId
		,hs.reasonWhy
		,case 
			when hs.approved='Y' and isnull(hs.approvedDate,'')<>'' then 'Y' 
			when hs.approved='N' and isnull(hs.approvedDate,'')<>'' then 'N' 
			when hs.approved='N' and isnull(hs.approvedDate,'')='' then '��' 
			else '' 
		end approved
		,hs.id hsId
		,dbo.FN_ConvertDate(hs.substituteWith,'D/M/Y') substituteWith
		,hs.sSubstituteType,hs.dSubstituteType
		,hs.approverComments
		from ta_empForHWGs ehg 
		left join ta_hwGroups hg on ehg.hwGroupId=hg.id
		left join ta_hws hw on hw.hwGroupId=hg.id 
		left join ta_hwSubstutionsV2 hs on ehg.id=hs.empHwId and hw.id=hs.hwId
		left join hrm_humanoPersonAll hj on ehg.IDNo=hj.IDNo			
		where hg.hType=2 and hj.em_status in ('W','P') and ehg.IDNo='3940200536679'  and hg.year='2017'
		
		order by hw.startDate desc



select hg.name hName, hg.[year],hw.id hwId,hw.hwGroupId,dbo.FN_ConvertDate(hw.startDate,'D/M/Y') hDate,hw.comments		
,hw.startDate
,ehg.id empHwId
from ta_empForHWGs ehg 
left join ta_hwGroups hg on ehg.hwGroupId=hg.id
left join ta_hws hw on hw.hwGroupId=hg.id 
left join hrm_humanoPersonAll hj on ehg.IDNo=hj.IDNo		
where hg.hType=2 and hj.em_status in ('W','P') 
--and ehg.IDNo='3940200536679'  
and hg.[year]=2017
and ehg.id=2767 and hw.id=377
order by hw.startDate desc