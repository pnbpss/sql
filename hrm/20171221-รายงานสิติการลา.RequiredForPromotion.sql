use EMPBase;
--goto processing
declare @startDate datetime, @endDate datetime,@corpId varchar;


select @startDate='20170822', @endDate='20171221'--,@corpId=(select id from hrm_corporates where name_01 like '%����¹�á��%');
if OBJECT_ID('tempdb..#tempPreReportTable') is not null drop table #tempPreReportTable 
if OBJECT_ID('tempdb..#finalReport') is not null drop table  #finalReport
if OBJECT_ID('tempdb..#temporaryPerson') is not null drop table  #temporaryPerson
if OBJECT_ID('tempdb..#summary01') is not null drop table #summary01
if OBJECT_ID('tempdb..#beforeMax') is not null drop table #beforeMax
if OBJECT_ID('tempdb..#afterMax') is not null drop table #afterMax

select IDNo 
into #temporaryPerson
from hrm_humanoPersonAll
where 1=1
and em_status in ('W','P','RR')
--and ((officeName like '%����%') or (officeName like '%�ӹѡ�ҹ%')) and (not(officeName like '%�Ң�%' or corpName like '%��������%' ) )--office
--and (not((officeName like '%����%') or (officeName like '%�ӹѡ�ҹ%') or (corpName like '%��������%'))) --�Ң� TJ
--and corpName like '%��������%'
and period_no not in ('PPK16C800000052','PPK16C800000056') --���. ��������, ��ѡ�ҹ����Ѵ˹���Թ
--select * from #temporaryPerson

/* �����š���� */
select 
el.id leaveId,el.IDNo,el.leaveId leaveTypeId
,convert(varchar(max),el.startDateTime,111)+' '+substring(convert(varchar(max),el.startDateTime,108),1,5)+ ' to '+convert(varchar(max),el.endDateTime,111)+' '+substring(convert(varchar(max),el.endDateTime,108),1,5) fromToDateTime
,case
	when elc.formated='A' and convert(varchar(max),sdl.leaveDate,112) between CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),el.endDateTime,112)  /*and elc.leaveMinutes>=480*/ then 1.00
	when elc.formated in ('M','E') and elc.leaveMinutes=240 then 0.50
	when elc.formated = 'O' and elc.leaveMinutes <= 480 then (elc.leaveMinutes/480.00)
	when elc.formated in ('O','S') and elc.leaveMinutes > 480 and 
			(	
					/* ��� �ѹ�Ѩ�غѹ �ҡ�����ѹ������� ��й��¡��� �ѹ����ش����� */
					(CONVERT(varchar(max),sdl.leaveDate,112) > CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) < CONVERT(varchar(max),el.endDateTime,112))
					or 
					/* �ѹ��������� ���¡����ѹ�Ѩ�غѹ ��� �ѹ����ش�������ҡѺ�ѹ�Ѩ�غѹ ��� ��������ش�������ҡѺ��������ش��*/
					(
					CONVERT(varchar(max),sdl.leaveDate,112) > CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.endDateTime,112) 
					and CAST(sdl.leaveDate AS datetime)+sv.endTime=el.endDateTime
					)
					or 
					/* �ѹ��������� = �ѹ�Ѩ�غѹ ��� �ѹ����ش����ҹ��¡����ѹ�Ѩ�غѹ ��� ���������������ҡѺ������鹡�*/
					(
					CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) < CONVERT(varchar(max),el.endDateTime,112) 
					and CAST(sdl.leaveDate AS datetime)+sv.startTime=el.startDateTime
					)
					
			) then 1.00
		when elc.formated in ('O','S') and elc.leaveMinutes > 480 and 
			(	
					
					/* �ѹ��������� = �ѹ�Ѩ�غѹ ��� �ѹ����ش�����=�ѹ�Ѩ�غѹ ��� �ӹǹ�ҷշ���� ���¡��� 480*/
					(
					--CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.endDateTime,112) and
					DATEDIFF(minute
						,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime)
						,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime)
						) < 480
					)
					
			) then case when DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime)) >= 300
								then (DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime))
									-60
									) / 480.00 --������Թ�����ѹ���ź�͡���¨ӹǹҷշ�������ҧ�ѡ 60 �ҷ�( �Ҩ�е�ͧ���������� �óշ�����ҷ��������� ���������ҧ�ѡ���§ �����Ҩ�������� )
								else (DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime))-0) / 480.00--��������֧�����ѹ
					  end
			
	else 0.00
end ddrt
,el.formated
,case when ltype.[group]=1 then '�ҡԨ' when ltype.[group]=3 then '�Ҿѡ��͹' when ltype.[group]=2 then '�һ���' else 'n/a' end leaveGroup
,el.comments
--,el.approvedBy,el.approvedDate,el.approverComments,el.approveResult
--,el.submitDate
--,el.imagefile
,el.[status]
--,el.insertBy,el.insertSince
--,el.lastUpdate,el.lastUpdateBy,el.ipaddress,sdl.id
,cast(sdl.leaveDate as date)leaveDate,sdl.shiftId
,elc.leaveDay,elc.leaveHour,elc.leaveMinute,elc.leaveMinutes
into #tempPreReportTable 
from (ta_employeeLeaves el inner join ta_shiftDuringLeave sdl on el.id=sdl.leaveId  inner join ta_shiftsView sv on  sdl.shiftId=sv.id inner join hrm_humanoPersonAll h on el.IDNo=h.IDNo)
left join ta_leaveTypes ltype on el.leaveId=ltype.id
left join ta_employeeLeavesCalTime elc on el.id=elc.id
where 1=1
and el.approveResult='Y'
and el.[status]=1
and cast(sdl.leaveDate as date) between @startDate and @endDate
and h.em_status in ('W','P')
and (h.IDNo in (select IDNo from #temporaryPerson))

/*�����š�âҴ�ҹ*/
insert into #tempPreReportTable 
		(leaveId		,IDNo		,leaveTypeId,fromToDateTime,ddrt,formated							,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute)
select -1 leaveId,lab.IDNo,-1 leaveTypeId,'' fromToDateTime,1 ddrt,'N' formated,'' leaveGroup,'' comments,1 [status],cDate leaveDate,0 shiftId,1 leaveDay,0 leaveHour,0 leaveMinute 
from ta_lateAbsentStats lab inner join hrm_humanoPersonAll h on lab.IDNo=h.IDNo
where cDate between @startDate and @endDate 
and (h.IDNo in (select IDNo from #temporaryPerson))
and msg like '%�Ҵ�ҹ����ѹ%'


processing:

declare @tempPostReportTable as table (
	leaveId int,IDNo varchar(max),leaveTypeId int,fromToDateTime varchar(max),ddrt decimal(18,2),formated varchar(1),leaveGroup varchar(max),comments varchar(max),[status]int,leaveDate datetime,shiftId int,leaveDay int,leaveHour int,leaveMinute int,leaveMinutes int,mleave decimal(18,2),calStat varchar(5)
)
insert into @tempPostReportTable(leaveId,IDNo,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute,leaveMinutes,mleave,calStat)
select 
	leaveId,IDNo,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute,leaveMinutes,mleave,calStat
	
from (
select leaveId,IDNo,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute,leaveMinutes,ddrt*480 mleave,'ok' calStat from #tempPreReportTable where ddrt>0
union 
select leaveId,IDNo,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute,leaveMinutes,ddrt*480 mleave,'err' calStat from #tempPreReportTable where ddrt<=0
union 
select null leaveId,IDNo,null leaveTypeId,null fromToDateTime,sum(ddrt)ddrt,null formated,null leaveGroup,null comments,null [status],null leaveDate,null shiftId,null leaveDay,null leaveHour,null leaveMinute,null leaveMinutes,sum(ddrt*480) mleave,null calStat from #tempPreReportTable group by IDNo

) allu

select 
--case when ISNULL(t.leaveDate,'')='' then '' else h.corpName end corpName
h.corpName
,t.IDNo,h.employeeCode,h.titleName+h.firstName+' '+h.lastName empName,h.positionName+'/'+h.officeName positionAndOffice
--,leaveId
,case when isnull(t.leaveDate,'')='' then '���' else convert(varchar,t.leaveDate,111) end leaveDate
,t.ddrt
,t.formated+''
+
	case 
		when t.formated = 'A' then '(�ҷ���ѹ)' 
		when t.formated = 'E' then '(�Ҫ�ǧ����)' 
		when t.formated = 'M' then '(�Ҫ�ǧ���)' 
		when t.formated = 'N' then '(�Ҵ�ҹ)' 
		when t.formated in ('O','S') then '(���ٻẺ����)'
	 else ''
	end
+'' leavePattern
,t.leaveGroup+case when t.comments='' then '' else '{'+t.comments+'}' end leaveInfo

--,t.leaveTypeId
--,t.fromToDateTime
--,t.[status],t.shiftId,t.leaveDay,t.leaveHour,t.leaveMinute,t.leaveMinutes
,t.mleave
,t.calStat
into #finalReport
from @tempPostReportTable t 
left join hrm_humanoPersonAll h on t.IDNo=h.IDNo
where 1=1
 and h.em_status in ('W','P','RR')


/*����������������仴���*/
insert into #tempPreReportTable 
		(leaveId		,IDNo		,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute)
select -2 leaveId,lab.IDNo,-2 leaveTypeId,'' fromToDateTime,0 ddrt,'L' formated,'' leaveGroup,'' comments,1 [status],cDate leaveDate,0 shiftId,1 leaveDay,0 leaveHour,0 leaveMinute 
from ta_lateAbsentStats lab inner join hrm_humanoPersonAll h on lab.IDNo=h.IDNo
where 1=1
and cDate between @startDate and @endDate 
and (h.IDNo in (select IDNo from #temporaryPerson))
and msg like '%���%'
--and h.IDNo='3940200536679' 
and h.em_status in ('W','P','RR')

select cast(sum(t1.ddrt) as decimal(18,2))sumDdrt,COUNT(*)cnt, t1.IDNo
,t1.formated,t1.leaveTypeId 
--from @tempPostReportTable t1 
into #summary01
from #tempPreReportTable t1
group by t1.IDNo
--,MONTH(t1.leaveDate),YEAR(t1.leaveDate)
,t1.formated,t1.leaveTypeId

select cnt.IDNO--,h.firstName,h.lastName,h.nick,h.corpName,h.officeName,h.positionName
,cnt.cntLate
,cnt.cntAbsent,su.sumDayAbsent
,cnt.cntErrand,su.sumDayErrand
,cnt.cntSickWithMC,su.sumDaySickWithMC
,cnt.cntSickWithOutMC,su.sumDaySickWithOutMC
,cnt.cntVacation,su.sumDayVacation
into #beforeMax
from (
	select p1.IDNo,isnull(p1.[Late],0) cntLate,isnull(p1.[absent],0) [cntAbsent],isnull(p1.[errand],0) cntErrand,isnull(p1.[sickWithOutMC],0) cntSickWithOutMC,isnull(p1.[sickWithMC],0) cntSickWithMC,isnull(p1.[vacation],0) cntVacation
	 from (
		select t1.sumDdrt,t1.cnt,t1.IDNo,t1.formated, t1.leaveTypeId
		,lt.[group]
		,case when t1.formated='L' then 'Late' when t1.formated='N' then 'absent' 
			else
				case 
				when lt.[group]=1 then 'errand'
				when lt.[group]=2 then 
					case
						when t1.leaveTypeId=16 then 'sickWithOutMC'
						when t1.leaveTypeId=21 then 'sickWithMC'
					end
				when lt.[group]=3 then 'vacation'
				end		
			end grpdesc
		,case when t1.formated='L' then '���' when t1.formated='N' then '�Ҵ�ҹ' else  lt.name end lName
		from #summary01 t1 
		
		left join ta_leaveTypes lt on t1.leaveTypeId=lt.id
		where 1=1
		--and IDNo='3940200536679'
	) t pivot (
		max(cnt) for grpdesc in ([errand]	,[Late]	,[absent]	,[sickWithOutMC]	,[vacation]	,[sickWithMC])
	) p1	
)cnt left join
(
	select p1.IDNo,isnull(p1.[absent],0) [sumDayAbsent],isnull(p1.[errand],0) sumDayErrand,isnull(p1.[sickWithOutMC],0) sumDaySickWithOutMC,isnull(p1.[sickWithMC],0) sumDaySickWithMC,isnull(p1.[vacation],0) sumDayVacation
	 from (
		select t1.sumDdrt,t1.cnt,t1.IDNo,t1.formated, t1.leaveTypeId
		,lt.[group]
		,case when t1.formated='L' then 'Late' when t1.formated='N' then 'absent' 
			else
				case 
				when lt.[group]=1 then 'errand'
				when lt.[group]=2 then 
					case
						when t1.leaveTypeId=16 then 'sickWithOutMC'
						when t1.leaveTypeId=21 then 'sickWithMC'
					end
				when lt.[group]=3 then 'vacation'
				end		
			end grpdesc
		,case when t1.formated='L' then '���' when t1.formated='N' then '�Ҵ�ҹ' else  lt.name end lName
		from #summary01 t1 
		--left join hrm_humanoPersonAll h on t1.IDNo=h.IDNo
		left join ta_leaveTypes lt on t1.leaveTypeId=lt.id
		where 1=1
		--h.em_status in ('W','P','RR')
	) t pivot (
		max(sumDDrt) for grpdesc in ([errand],[absent],[sickWithOutMC],[vacation],[sickWithMC])
	) p1
)su on cnt.IDNo=su.IDNo
--left join hrm_humanoPersonAll h on cnt.IDNo=h.IDNo
--where h.em_status in ('W','P','RR')

select 
bm.IDNO
,max(cntLate)cntLate	,max(cntAbsent)cntAbsent	
--,max(sumDayAbsent)sumDayAbsent	
,max(cntErrand)cntErrand	,max(sumDayErrand)sumDayErrand	,max(cntSickWithMC)cntSickWithMC	,max(sumDaySickWithMC)sumDaySickWithMC	,max(cntSickWithOutMC)cntSickWithOutMC	,max(sumDaySickWithOutMC)sumDaySickWithOutMC	,max(cntVacation)cntVacation	,max(sumDayVacation)sumDayVacation
into #afterMax
from #beforeMax  bm 
group by IDNo


/*��§ҹ��ػ ��ṡ����¤�*/
select 
	h.IDNo,h.employeeCode,convert(varchar,h.workStart,111)workStart,h.firstName,h.lastName,h.nick,h.corpName,h.officeName,h.positionName
	,h.period_no
	,cntLate,cntAbsent,cntErrand,sumDayErrand,cntSickWithMC,sumDaySickWithMC,cntSickWithOutMC,sumDaySickWithOutMC,cntVacation,sumDayVacation
from hrm_humanoPersonAdjust h left  join #afterMax am on am.IDNo=h.IDNo
where h.em_status in ('W','P','RR')
--and h.IDNo='3940200536679'
and h.IDNo in (select IDNo from #temporaryPerson)

/** ��§ҹ��������´ �ͧ�����ѹ���Ф�*/
select corpName,IDNo,employeeCode,empName,positionAndOffice,leaveDate,ddrt,leavePattern,leaveInfo,mleave,calStat 
from #finalReport 
--where IDNo='3940200536679'
order by IDNo asc,leaveDate ,corpName
