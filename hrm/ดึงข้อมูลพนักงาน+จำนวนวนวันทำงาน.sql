select id,employeeCode,titleName,firstName,lastName,nick,gender
,dbo.FN_ConvertDate(birthdate,'D/M/Y') birthDate
,dbo.FN_arabicToKmer(dbo.FN_ConvertDate(birthdate,'D/M/Y')) kmerBirthDate
,dbo.FN_ConvertDate(workStart,'D/M/Y') workStart
,dbo.FN_arabicToKmer(dbo.FN_ConvertDate(workStart,'D/M/Y')) kmerWorkStart
,DATEDIFF(day,workStart,getdate()) duration
--,dbo.FN_ConvertDate(workEnd,'D/M/Y') workEnd
,IDNo
,dbo.FN_arabicToKmer(IDNo) kmerIDNo
,positionName,corpName,officeName,uname,hrmCorpId,em_status,period_no,ssoBranchId,email,mobile,stdPositionId 
from hrm_humanoPersonAll where em_status in ('W','P')


