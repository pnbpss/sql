declare @employeeCode varchar(max), @slipMonth int, @slipYear int, @corpId varchar(max),@startDateOfMonth datetime, @endDateOfMonth datetime;
set @employeeCode='TJY40000120'; --�֧�ҡ session �����ͤ�Թ
set @slipMonth=5; --��͹�ͧ�Թ��͹ ����������͡ 
set @slipYear=2017; --�բͧ�Թ��͹ ����������͡
--�ʴ���͹��ѧ����Թ 3 ��͹ �ҡ���͡��͹��ѧ�Թ�����͹������ʴ����

set @corpId = (select hrmCorpId from hrm_humanoPersonAll where employeeCode=@employeeCode);
set @startDateOfMonth = CONVERT(varchar,@slipYear)+'/'+CONVERT(varchar,@slipMonth)+'/1';
set @endDateOfMonth = dateadd(day,-1,dateadd(month,1,@startDateOfMonth));
--print @endDateOfMonth;print @startDateOfMonth

select detail_inc_dec.person_id,em_code,emp_name,em_type
,position_id
,position.name_01 as positionnm,position_level,position_level.name_01 as position_levelnm
,st_code as station_id,station.name_01 as station_nm,st_link,slip_year,slip_month
,inc_id,payinfolist.name_01 as inc_name,case when type_list=1 then isnull(inc_amount,0) else isnull(inc_amount,0) *-1 end as inc_amount
,type_list,detail_inc_dec.corp,dtlno,station_type.st_level
into #slipDetails
from
(
select person_data.id as person_id,em_code,ISNULL(prefix_01,'') +''+ ISNULL(person_data.name_01,'') +' '+ISNULL(sname_01,'') as emp_name
,initial_data.initial_name_01 as em_type,person_data.em_status,slip_year,slip_month,slip_period,inc_id, 
detail_inc_dec.lock_detail,sum(round(isnull(inc_total,0),2)) as inc_amount,person_data.corp
from humano_tangjai.dbo.person_data left join humano_tangjai.dbo.detail_inc_dec
on person_data.id=detail_inc_dec.person_id
and person_data.corp=detail_inc_dec.corp
left join humano_tangjai.dbo.initial_data on person_data.em_type = initial_data.initial_value_01
and initial_subsystem = 'Emp_Type'
where person_data.status='1' 
and person_data.corp=@corpId
and slip_year=@slipYear
and slip_month=@slipMonth
--and detail_inc_dec.lock_detail in('LCK175U00000017') 
and detail_inc_dec.status='1'
group by person_data.id,em_code,prefix_01,person_data.name_01,sname_01,person_data.corp,
detail_inc_dec.lock_detail,initial_data.initial_name_01,person_data.em_status,slip_year,slip_month,slip_period,inc_id) as detail_inc_dec
inner join
(
	select detail_inc_dec.person_id,isnull(salary.salary_value,'N/A')as salary_value from humano_tangjai.dbo.slip
	inner join humano_tangjai.dbo.detail_inc_dec on slip.person_id = detail_inc_dec.person_id and detail_inc_dec.status = '1'
	left join humano_tangjai.dbo.salary on slip.person_id = salary.person_id
	and salary.id =(select top 1 J1.id from humano_tangjai.dbo.salary J1
	Where J1.person_id=detail_inc_dec.person_id
	and J1.status = 1 and (J1.salary_status = '1' or J1.salary_status = '2')  and j1.begin_date<=@endDateOfMonth
	and (J1.end_date is null or J1.end_date>=@startDateOfMonth) order by begin_date desc)
	where detail_inc_dec.slip_year=@slipYear
	and detail_inc_dec.slip_month=@slipMonth
	--and detail_inc_dec.lock_detail in('LCK175U00000017') and slip.lock_detail in('LCK175U00000017') 
	and detail_inc_dec.corp= @corpId
	and slip.slip_month =@slipMonth 
	and slip.slip_year = @slipYear
) as per_salary on detail_inc_dec.person_id = per_salary.person_id
inner join 
(
		select id,code,name_01,name_02,id_group_inc,type_list,case when type_list =1 then 5 else 7 end as dtlno,corp
		from humano_tangjai.dbo.S_Pay_info_List
		where status='1'
		--and corp=@corpId
		union select 'Period Salary',NULL,'�Թ��͹','Period Salary',NULL,1,1 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'OT01',NULL,'�����ǧ����','OT',NULL,1,2 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'OT02',NULL,'�����ǧ����','OT',NULL,1,2 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'OT03',NULL,'�����ǧ����','OT',NULL,1,2 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'OT04',NULL,'�����ǧ����','OT',NULL,1,2 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'OT05',NULL,'�����ǧ����','OT',NULL,1,2 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'INCSOC',NULL,'��Сѹ�ѧ������ѷ�͡���','INCSOC',NULL,1,3 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'INCTAX',NULL,'���պ���ѷ�͡���','INCTAX',NULL,1,4 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'Shift Increase',NULL,'��ҡ�','Shift Increase',NULL,1,4 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'LATE',NULL,'���','LATE',NULL,-1,3 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'EARLY',NULL,'�͡��͹','EARLY',NULL,-1,3 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'SOCIAL',NULL,'��Сѹ�ѧ��','SOCIAL',NULL,-1,4 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'TAX',NULL,'����','TAX',NULL,-1,5 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'FUND',NULL,'�ͧ�ع�','FUND',NULL,-1,6 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'ABSENT',NULL,'�Ҵ�ҹ','ABSENT',NULL,-1,6 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
		union select 'LEAVE',NULL,'������Ѻ��Ҩ�ҧ','LEAVE',NULL,-1,7 as dtlno,(select id from humano_tangjai.dbo.corp where id= @corpId)
) as payinfolist
on detail_inc_dec.inc_id=payinfolist.id
and detail_inc_dec.corp=payinfolist.corp
left join humano_tangjai.dbo.per_position 
on detail_inc_dec.person_id=per_position.person_id
and detail_inc_dec.corp=per_position.corp and per_position.status='1' 
and per_position.id =(select top 1 J1.id from humano_tangjai.dbo.per_position J1 Where J1.person_id=detail_inc_dec.person_id and J1.status=1 and j1.begin_date<=@endDateOfMonth and (J1.end_date is null or J1.end_date>=@startDateOfMonth) order by begin_date desc)
left join humano_tangjai.dbo.position on per_position.position_id=position.id and position.status='1' and detail_inc_dec.corp=position.corp 
left join humano_tangjai.dbo.station on position.station_id=station.id and detail_inc_dec.corp=station.corp and station.status='1' 
left join humano_tangjai.dbo.position_level on position.position_level=position_level.id and detail_inc_dec.corp=position_level.corp 
left join humano_tangjai.dbo.station_type on station.st_type=station_type.id and detail_inc_dec.corp=station_type.corp and station_type.status='1' 
left join humano_tangjai.dbo.ps_per_resign on detail_inc_dec.person_id=ps_per_resign.person_id and detail_inc_dec.corp=ps_per_resign.corp and ps_per_resign.status = '1'
where inc_amount >0  and em_code  = @employeeCode 
group by detail_inc_dec.person_id,em_code,emp_name,em_type,salary_value,position_id,position.name_01,position_level,position_level.name_01,st_code,station.name_01,st_link,slip_year,slip_month,slip_period,inc_id,payinfolist.name_01,inc_amount,type_list,detail_inc_dec.corp,dtlno,station_type.st_level
order by em_code asc,st_link asc ,station_type.st_level  asc ,cast(type_list as int) desc ,dtlno asc,inc_id


select * from #slipDetails;

