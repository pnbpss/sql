USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_EmpEachDayInfo]    Script Date: 09/14/2017 08:26:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_EmpEachDayInfo] 
(
	-- Add the parameters for the function here
	@cDate datetime, @IDNo varchar(20),@temp_ta_empTaDetails temp_ta_empTaDetails readonly
)
RETURNS 
@EmpEachDayInfo TABLE 
(
	-- Add the column definitions for the TABLE variable here
	dummy varchar(500)
	,empHW char(1)
	,assignedHwToEmp char(1)
	,checkInOutExists char(1)
	,noNeedAttdnShift char(1)
	,assignedShiftToEmp char(1)
	,empAllDayLeaveRequest char(1)
	,empReqNoAllDayAttnd char(1)
	,empHSubstitution char(1)
	,completedAttndInDay char(1)
	,noDeductionLeave char(1)
	,isCheckin char(1) --สแกนเข้าไหม
	,isLate char(1) --สายไหม
	,minuteLate int --สายกี่นาที
	,requestNotAttdnCheckIn char(1) --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
	,requestLeaveAsLate char(1)--มีใบลาตามที่สายหรือไม่
	,isCheckout char(1) --สแกนออกหรือไม่
	,isEarly  char(1) --ออกก่อนหรือไม่
	,minuteEarly int --จำนวนนาทีที่ออกก่อน
	,requuestNotAttdnCheckOut char(1)--หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
	,requestLeaveAsEarly char(1)--ลาตามที่สแกนออก่อนหรือไม่
	,isCheckBrake char(1)--สแกนพักหรือไม่
	,brakeDuration int--จำนวนนาทีที่ไปพัก
	,brakeEarlyOrLate char(1)--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
	,minuteBrakeEarlyOrLate int--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
	,minuteLeaveDuringBrake int --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
	,paymentOfLeaveDuringBrake char(1) --หักที่ลาระหว่างพักหรือไม่
	,checkTimeForLeaveDuringBrakeOK char(1) --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
	,stateOfCal char(6)
)
AS
BEGIN	
	declare @dummy varchar(500)
	,@checkInAndCheckOutInSameDay int /*เวลาเข้าออกอยู่ในวันเดียวกัน 0=ใช่ 1=วันถัดไป */
	,@checkIncheckBrakecheckOut char(3);
	-- Fill the table variable with the rows for your result set
	
	--shiftInfo
	declare 
	@shiftMusTA	char(1)
	,@shiftName	varchar(max)
	,@shiftStartTime	 time
	,@shiftHaveToCheckEnd	char(1)
	,@shiftEndTime	time
	,@shiftMinCheckInTime	time
	,@shiftMaxCheckOutTime	time
	,@shiftBrakeCheck	char(1)
	,@shiftBreakStart	time
	,@shiftBreakEnd	time
	,@shiftMinuteBrake	int
	
	--ดึงข้อมูล shift ออกมา ในวันนี้ออกมา
		select 
			@shiftMusTA=musTA
			,@shiftName=shiftName
			,@shiftStartTime=startTime
			,@shiftHaveToCheckEnd=haveToCheckEnd
			,@shiftEndTime=endTime
			,@shiftMinCheckInTime=minCheckInTime
			,@shiftMaxCheckOutTime=maxCheckOuttime
			,@shiftBrakeCheck=brakeCheck
			,@shiftBreakStart=breakStart
			,@shiftBreakEnd=breakEnd
			,@shiftMinuteBrake=minuteBrake
			from ta_shifts sh
			inner join ta_shiftForEmployees she on sh.id=she.shiftId
			and she.IDNo=@IDNo
			and @cDate between she.activeFrom and she.activeTo and active='Y'
	
	
	declare 
	@empHW char(1) --วันหยุดหรือไม่ 
	,@assignedHwToEmp char(1) --กำหนดข้อมูลวันหยุดให้แล้ว
	,@checkInOutExists char(1) --มีการสแกนนิ้วหรือไม่
	,@noNeedAttdnShift char(1) --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	,@assignedShiftToEmp char(1) --กำหนดกะงานในวันนี้ให้แล้ว
	,@empAllDayLeaveRequest char(1) --มีขอลาหยุดแบบทั้งวันหรือไม่
	,@empReqNoAllDayAttnd char(1) --มีขอไม่สแกนทั้งวันหรือไม่
	,@empHSubstitution char(1) --มีการขอสลับวันหยุด	
	,@completedAttndInDay char(1) --สแกนครบตามกะงาน 
	,@noDeductionLeave char(1) --ลาแบบไม่หัก
	,@isCheckin char(1) --สแกนเข้าไหม
	,@isLate char(1) --สายไหม
	,@minuteLate int --สายกี่นาที
	,@requestNotAttdnCheckIn char(1) --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
	,@requestLeaveAsLate char(1) --มีใบลาตามที่สายหรือไม่
	,@isCheckout char(1) --สแกนออกหรือไม่
	,@isEarly  char(1) --ออกก่อนหรือไม่
	,@minuteEarly int --จำนวนนาทีที่ออกก่อน
	,@requuestNotAttdnCheckOut char(1)--หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
	,@requestLeaveAsEarly char(1)--ลาตามที่สแกนออก่อนหรือไม่
	,@isCheckBrake char(1)--สแกนพักหรือไม่
	,@brakeDuration int--จำนวนนาทีที่ไปพัก
	,@brakeEarlyOrLate char(1)--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
	,@minuteBrakeEarlyOrLate int--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
	,@minuteLeaveDuringBrake int ----จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
	,@paymentOfLeaveDuringBrake char(1)--หักที่ลาระหว่างพักหรือไม่
	,@checkTimeForLeaveDuringBrakeOK char(1) --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
	,@stateOfCal char(6) --สถานะการประมวลผล
	;	
	declare @ta_empTaDetails table (USERID int,CHECKTIME datetime,SENSORID nvarchar(5));
	
	/*
	set @empHW='N'; --วันหยุดหรือไม่
	set @assignedHwToEmp = 'N' --กำหนดข้อมูลวันหยุดให้แล้ว
	set @checkInOutExists='N'; --มีการสแกนนิ้วหรือไม่
	set @noNeedAttdnShift  = 'N' --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	set @assignedShiftToEmp = 'N' --กำหนดกะงานในวันนี้ให้แล้ว
	set @empAllDayLeaveRequest='N'; --มีขอลาหยุดแบบทั้งวันหรือไม่
	set @empReqNoAllDayAttnd = 'N'; --มีขอไม่สแกนนิ้วทั้งวันหรือไม่
	set @empHSubstitution='N'; --มีการขอสลับวันหยุด
	set @completedAttndInDay='N'; --สแกนครบตามกะงาน 
	set @stateOfCal = 0;
	*/
	
	set @empHW=Null; --วันหยุดหรือไม่
	set @assignedHwToEmp = Null --กำหนดข้อมูลวันหยุดให้แล้ว
	set @checkInOutExists=Null; --มีการสแกนนิ้วหรือไม่
	set @noNeedAttdnShift  = Null --ใช้กะงานที่ไม่ต้องสแกนนิ้วเข้า/ออก/พัก
	set @assignedShiftToEmp = Null --กำหนดกะงานในวันนี้ให้แล้ว
	set @empAllDayLeaveRequest=Null; --มีขอลาหยุดแบบทั้งวันหรือไม่
	set @empReqNoAllDayAttnd = Null; --มีขอไม่สแกนนิ้วทั้งวันหรือไม่
	set @empHSubstitution=Null; --มีการขอสลับวันหยุด
	set @completedAttndInDay=Null; --สแกนครบตามกะงาน 
	set @noDeductionLeave=null; --ลาแบบไม่หัก
	set @isCheckin =null --สแกนเข้าไหม
	set @isLate =null --สายไหม
	set @minuteLate =null --สายกี่นาที
	set @requestNotAttdnCheckIn=null --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
	set @requestLeaveAsLate=null  --มีใบลาตามที่สายหรือไม่
	set @isCheckout=null --สแกนออกหรือไม่
	set @isEarly=null --ออกก่อนหรือไม่
	set @requuestNotAttdnCheckOut=null--หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
	set @requestLeaveAsEarly=null--ลาตามที่สแกนออก่อนหรือไม่
	set @isCheckBrake=null --สแกนพักหรือไม่
	set @brakeDuration =null--จำนวนนาทีที่ไปพัก
	set @brakeEarlyOrLate =null--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
	set @minuteBrakeEarlyOrLate =null--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
	set @minuteLeaveDuringBrake=null --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
	set @paymentOfLeaveDuringBrake=null --หักที่ลาระหว่างพักหรือไม่
	set @checkTimeForLeaveDuringBrakeOK= null --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
	set @minuteEarly=null --จำนวนนาทีที่ออกก่อน	
	set @stateOfCal = '++++++';
	
	if @cDate >= convert(varchar,getdate(),111)
	begin
		set @stateOfCal = 'Z+++++';
		insert into @EmpEachDayInfo (stateOfCal) values(@stateOfCal); return;
	end
	
	--ถ้าลางานทั้งวัน หรือ ขออนุมัติไม่สแกนทั้งวัน ไม่ต้องคำนวณอะไร 
	if exists(
		select 1 from ta_employeeLeaves el
										where el.IDNo=@IDNo
										and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1										
										and @cDate between cast(convert(varchar,el.startDateTime,111) as datetime) and cast(convert(varchar,el.endDateTime,111) as datetime)
										and datediff(minute,el.startDateTime,el.endDateTime)
											>=
											datediff(minute
											,@cDate+@shiftStartTime
											,dateadd(minute,datepart(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))	)
		union
		select 1 from ta_empNotAttendances where IDNo=@IDNo and @cDate between fromDate and toDate and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and approvedDate is not null and approved='Y' and [status]<>'D'
	)
	begin 
		--set @stateOfCal=4
		select @stateOfCal=stuff(@stateOfCal,1,1,'D')
	end
	else
	begin
			--กำหนดข้อมูลวันหยุดให้แล้ว เริ่ม
			if exists(select 1 from ta_empForHWGs ehw inner join ta_hwGroups hwg on ehw.hwGroupId=hwg.id where IDNo=@IDNo and hwg.[year]=year(@cDate))
			begin 
				set @assignedHwToEmp='Y' 	
				--เป็นวันหยุด เริ่ม (ต้องเทียบกับการขอแลกวันหยุดด้วย หากมีการขอแลกวันหยุดไปแล้ว=ไม่ใช่วันหยุด)
				if exists(
								select 1 FROM  [ta_empForHWGs] ehw --เป็นวันหยุดหรือเปล่า
									inner join ta_hws m on ehw.hwGroupId=m.hwGroupId 
									left join ta_hwSubstutions hs on ehw.id=hs.empHwId and hs.hwId=m.id and hs.approved='Y' and hs.[status]='1'  --และไม่ได้ขอสลับไปเป็นวันอื่น
									where ehw.IDNo=@IDNo and m.startDate=@cDate and hs.id is null 								
							) begin set @empHW='Y' end
				--เป็นวันหยุด จบ
				else
				begin	
					set @empHW='N'
					--กำหนดกะงานให้แล้วหรือไม่ เริ่ม
					if exists(select 1 from ta_shiftForEmployees sh4emp where @cDate between sh4emp.activeFrom and sh4emp.activeTo and sh4emp.active='Y' and sh4emp.IDNo=@IDNo) 
					begin				
						
						set @assignedShiftToEmp='Y'
						--เช็คว่าใช้กะงานแบบไม่ต้องสแกนหรือไม่ เริ่ม					
						if  @shiftMusTA='N'
						begin 
							set @noNeedAttdnShift  = 'Y' 
							--มีการลางานหรือไม่หรือไม่ เริ่ม(กรณีที่กะงานไม่ต้องสแกน)
							if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and convert(varchar,@cDate,111) between convert(varchar,startDateTime,111) and convert(varchar,endDateTime,111))
							begin 
								--set @stateOfCal=1
								select @stateOfCal=stuff(@stateOfCal,1,1,'A')
							end
							--มีการลางานหรือไม่หรือไม่ จบ(กรณีที่กะงานไม่ต้องสแกน)					
						end
						else 
						begin
							set @noNeedAttdnShift  = 'N'
								--มีสแกนนิ้วในวันนั้นหรือไม่ เริ่ม								
								if exists(select 1 from @temp_ta_empTaDetails etd where etd.CHECKTIME between 
										dateadd(minute,datepart(minute,@shiftMinCheckInTime)*(-1),dateadd(hour,datepart(hour,@shiftMinCheckInTime)*(-1),@cDate+@shiftStartTime)) --เวลาเริ่มนับให้สแกน
										and 
										dateadd(minute,datepart(minute,@shiftMaxCheckOutTime),dateadd(hour,datepart(hour,@shiftMaxCheckOutTime),@cDate+@shiftStartTime)) --เวลาสิ้นสุดการสแกน
										)
								begin 
									set @checkInOutExists='Y'; 
									--สแกนครบตามที่กำหนดในกะงานหรือไม่ เริ่ม
									select @checkIncheckBrakecheckOut=checkIn+isCheckBrake+checkOut
											,@isCheckIn=checkIn									
											,@isLate = isLate
											,@minuteLate = minuteLate
											,@isCheckOut=checkOut
											,@isEarly = isEarly
											,@isCheckBrake=isCheckBrake
											,@minuteEarly = minuteEarly
											,@brakeDuration = brakeDuration
											,@brakeEarlyOrLate = brakeEarlyOrLate
											,@minuteBrakeEarlyOrLate = minuteBrakeEarlyOrLate									 
									from FN_001_01_completedDailyAttdnCheck(@cDate,@IDNo,@temp_ta_empTaDetails
										,@shiftMusTA,@shiftName,@shiftStartTime,@shiftHaveToCheckEnd,@shiftEndTime,@shiftMinCheckInTime
										,@shiftMaxCheckOutTime,@shiftBrakeCheck,@shiftBreakStart,@shiftBreakEnd,@shiftMinuteBrake)
																		
									set @dummy=@checkIncheckBrakecheckOut+' '+convert(varchar,@shiftEndTime)
									--set @isCheckIn = substring(@checkIncheckBrakecheckOut,1,1);							
									--set @isCheckOut = substring(@checkIncheckBrakecheckOut,3,1);
									if @checkIncheckBrakecheckOut='YYY' and @isLate='N' and @isEarly='N' --หากสแกนเข้า ออก พัก ครบ และไม่สามย ไม่ออกก่อน 
									begin set @completedAttndInDay ='Y' end 
									else
									begin
										set @completedAttndInDay ='N' 
										--สแกนเข้าหรือไม่, สายหรือไม่, และหากสายสายสายกี่นาที 
										--select @isCheckin = isCheckin, @isLate=isLate, @minuteLate = minuteLate from dbo.FN_001_02_CheckInInfo(@cDate,@IDNo,@temp_ta_empTaDetails)
										--สแกนเข้าหรือไม่, สายหรือไม่, และหากสายสายสายกี่นาที จบ
										--หากไม่ได้สแกนเข้า
										if @isCheckin = 'N'
										begin
											if exists( -- ขออนุมัติไม่ได้สแกนหรือไม่
													select * from ta_empNotAttendances where IDNo=@IDNo 	and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null and checkIn='Y' and [status]<>'D'
												) 
											begin
												set @requestNotAttdnCheckIn='Y';
											end
											else
											begin
												if exists( --ขออนุมัติลาครึ่งวันแรกหรือไม่
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
														where she.IDNo=@IDNo
														and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
														--and @cDate between she.activeFrom and she.activeTo 
														and she.active='Y'
														and el.startDateTime <= @cDate+@shiftStartTime
														and el.endDateTime >= dateadd(hour,4,@cDate+@shiftStartTime)
												)
												begin 
													select @stateOfCal=stuff(@stateOfCal,2,1,'D')
													set @isCheckBrake ='Y' --หากลาครึ่งวันก็ไม่ต้องซีเรียสเรื่องสแกนพัก
												end
												else
												begin 												
													--set @stateOfCal = 10;
													--ขอสลับวันหยุดครึ่งวันเช้าหรือไม่
													if exists(select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id 
																left join ta_hwSubstutionsV2 hs on ehg.id=hs.empHwId and hw.id=hs.hwId 
																where ehg.IDNo=@IDNo and hs.substituteWith=@cDate and hs.approved='Y' and hs.sSubstituteType='M'
																)
													begin
														select @stateOfCal=stuff(@stateOfCal,2,1,'E')
													end
													--ขอสลับวันหยุดครึ่งวันเช้าหรือไม่ จบ
													else
													begin													
															select @stateOfCal=stuff(@stateOfCal,2,1,'A')
															set @requestNotAttdnCheckIn='N'
													end
												end
												--ขออนุมัติลาครึ่งวันหรือไม่ จบ
											end	
										end
										--หากไม่ได้สแกนเข้า จบ
										
										--หากสาย มีใบลาตามที่สายหรือไม่								
										if @isLate = 'Y'
										begin
											if exists(
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
												where she.IDNo=@IDNo
												and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
												and @cDate between she.activeFrom and she.activeTo and she.active='Y'
												and @cDate+sh.startTime>=el.startDateTime 
												and DATEDIFF(minute,@cDate+sh.startTime,el.endDateTime)>=@minuteLate
											)
											begin
												set @requestLeaveAsLate='Y'  --มีใบลาตามที่สาย
												--set @stateOfCal=30;
												select @stateOfCal=stuff(@stateOfCal,2,1,'C')
											end
											else
											begin
												set @requestLeaveAsLate='N'  --ไม่มีใบลาตามที่สาย
												--set @stateOfCal=20;
												select @stateOfCal=stuff(@stateOfCal,2,1,'B')
											end
										end
										--หากสาย มีใบลาตามที่สายหรือไม่ จบ
										--เช็คการสแกนออก								
										--หากไม่ได้สแกนออก ขอไม่ได้สแกนหรือไม่ --@requestLeaveAsEarly 
										if @isCheckOut='N'
										begin
											if exists(
												select 1 from ta_empNotAttendances
												where IDNo=@IDNo
												and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null
												and checkOut='Y' and [status]<>'D'
											) 
											begin 
												set @requuestNotAttdnCheckOut='Y' 
											end 
											else 
											begin 
												--ขอลางานครึ่งวันหลังหรือไม่
												if exists(
													select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
														where she.IDNo=@IDNo
														and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
														--and @cDate between she.activeFrom and she.activeTo 
														and she.active='Y'
														and el.startDateTime <= dateadd(hour,-4,dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime)))
														and el.endDateTime >= dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
												)
												begin
													select @stateOfCal=stuff(@stateOfCal,3,1,'D') 
													set @isCheckBrake ='Y' --หากลาครึ่งวันก็ไม่ต้องซีเรียสเรื่องสแกนพัก
												end
												else
												begin
													-- ขอสลับวันหยุดช่วงบ่ายหรือไม่
													if exists(select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id 
																left join ta_hwSubstutionsV2 hs on ehg.id=hs.empHwId and hw.id=hs.hwId 
																where ehg.IDNo=@IDNo and hs.substituteWith=@cDate and hs.approved='Y' and hs.sSubstituteType='E'
																)
													begin
														select @stateOfCal=stuff(@stateOfCal,3,1,'E') 
													end
													-- ขอสลับวันหยุดช่วงบ่ายหรือไม่ จบ
													else
													begin													
														set @requuestNotAttdnCheckOut='N';/*set @stateOfCal=40*/
														select @stateOfCal=stuff(@stateOfCal,3,1,'A') 
													end
												end
												--ขอลางานครึ่งวันหลังหรือไม่ จบ
											end
										end
										--หากไม่ได้สแกนออก ขอไม่ได้สแกนหรือไม่ จบ
										--หากออกก่อนมีใบลาหรือไม่
										if @isEarly = 'Y'
										begin
											if exists(
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
												where she.IDNo=@IDNo
												and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
												and @cDate between she.activeFrom and she.activeTo and she.active='Y'
												and dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))<=el.endDateTime 
												and DATEDIFF(minute
													,el.startDateTime
													,dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))											
													)>=@minuteEarly
											)
											begin
												set @requestLeaveAsEarly='Y'  --มีใบลาตามที่ออกก่อน
												--set @stateOfCal=50;
												select @stateOfCal=stuff(@stateOfCal,3,1,'C')
											end
											else
											begin
												set @requestLeaveAsEarly='N'  --ไม่มีใบลาตามที่ออกก่อน
												--set @stateOfCal=60;
												select @stateOfCal=stuff(@stateOfCal,3,1,'B')
											end
										end
										--หากออกก่อนมีใบลาหรือไม่ จบ
										
										--จัดการกับการสแกนพักเที่ยง
										if @isCheckBrake='N'
										begin
											set @dummy = @dummy+'check brake'
											--กะงานไม่ต้องสะแกนหรือไม่
											if @shiftBrakeCheck='N' begin 	select @stateOfCal=stuff(@stateOfCal,4,1,'B') end
											else
											begin
												--ขอไม่ลงเวลาพักเที่ยงหรือไม่
												if exists(
													select 1 from ta_empNotAttendances															
															where IDNo=@IDNo
															and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null
															and (brakeStart='Y' or brakeEnd='Y')
															and [status]<>'D'
												) 
												begin select @stateOfCal=stuff(@stateOfCal,4,1,'C')	end
												else
												begin
													 --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
													 declare @caseOfLeaveDuringBrake int; set @caseOfLeaveDuringBrake=0;
													 select @caseOfLeaveDuringBrake=caseOfLeaveDuringBrake
														,@minuteLeaveDuringBrake=minuteLeave
														,@paymentOfLeaveDuringBrake=paymentOfLeaveDuringBrake
														,@checkTimeForLeaveDuringBrakeOK=checkTimeOK
														 from FN_001_caseOfLeaveDuringBrake(
														@shiftMusTA,@shiftName,@shiftStartTime,@shiftHaveToCheckEnd,@shiftEndTime,@shiftMinCheckInTime
														,@shiftMaxCheckOutTime,@shiftBrakeCheck,@shiftBreakStart,@shiftBreakEnd,@shiftMinuteBrake
														,@cDate,@IDNo
														,@temp_ta_empTaDetails
													 )
													 if @caseOfLeaveDuringBrake<>0													 
													 begin
														set @dummy = 'ลาคร่อมเวลาพัก'
														select @stateOfCal=stuff(@stateOfCal,4,1,'E')
													 end
													 --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก จบ
													 else 
														begin 
															--เช็คสแกนไปพักเลท หรือ กลับจากพักเลท
															if @brakeEarlyOrLate = 'Y'
															begin
																select @stateOfCal=stuff(@stateOfCal,4,1,'F');																
															end
															--เช็คสแกนไปพักเลท หรือ กลับจากพักเลท
															else
															begin
																select @stateOfCal=stuff(@stateOfCal,4,1,'D')	--นอกเหนือจากนั้น หัก 1 ชม.
															end
														end													 
												end
											end
										end
										else
										begin
												select @stateOfCal=stuff(@stateOfCal,4,1,'A')
										end
										--จัดการกับการสแกนพักเที่ยง จบ
									end 
									--สแกนครบตามที่กำหนดในกะงานหรือไม่ จบ
								end
								else 
								begin
										set @checkInOutExists='N';
										--ขออนุมัติไม่ได้สแกนทั้งวันหรือไม่ เริ่ม	
										if exists(select 1 from ta_empNotAttendances where IDNo=@IDNo and @cDate between fromDate and toDate and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and approvedDate is not null and approved='Y' and [status]<>'D')
										begin set @empReqNoAllDayAttnd='Y' end
										else 
										begin
											begin set @empReqNoAllDayAttnd='N' end
											--ขอสลับวันหยุดหรือไม่							
											if exists
											(
												--select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id left join ta_hwSubstutions hs on ehg.id=hs.empHwId and hw.id=hs.hwId	where ehg.IDNo=@IDNo  and hg.year=year(@cDate) and ehg.IDNo=@IDNo and hs.approvedDate is not null and hs.approved='Y' and hs.substituteWith=@cDate
												select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id left join ta_hwSubstutionsV2 hs on ehg.id=hs.empHwId and hw.id=hs.hwId where ehg.IDNo=@IDNo and hs.substituteWith=@cDate and hs.approved='Y' having COUNT(*)=2
												--สลับวันหยุดมาทั้งวันหรือเปล่า
											)
											begin set @empHSubstitution = 'Y';select @stateOfCal=stuff(@stateOfCal,1,1,'G'); end
											--ขอสลับวันหยุดหรือไม่ จบ
											else begin /*set @stateOfCal=2;*/select @stateOfCal=stuff(@stateOfCal,1,1,'B'); set @empHSubstitution = 'N' end											
										end
										--ขออนุมัติไม่ได้สแกนทั้งวันหรือไม่ จบ
								end
								--มีสแกนนิ้วในวันนั้นหรือไม่ จบ
						end
						--เช็คว่าใช้กะงานแบบไม่ต้องสแกนหรือไม่ จบ		
					end
					else
					begin
						set @assignedShiftToEmp='N'
						--set @stateOfCal=-2
						select @stateOfCal=stuff(@stateOfCal,1,1,'F');
					end
					--กำหนดกะงานให้แล้วหรือไม่ จบ
					
					--มีใบลางานแบบทั้งวันหรือไม่ เริ่ม
					declare @startOfShift time, @endOfShift time; 
					select @startOfShift =sh.startTime
					,@endOfShift = isnull(sh.endTime,DATEADD(hour,9,sh.startTime)) from ta_shiftForEmployees esh inner join ta_shifts sh on esh.shiftId=sh.id where esh.IDNo=@IDNo and @cDate between esh.activeFrom and dateadd(minute,59,DATEADD(hour,23,esh.activeTo)) and esh.active='Y'
					if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and @cDate+@startOfShift between startDateTime and endDateTime and @cDate+@endOfShift between startDateTime and endDateTime and DATEDIFF(hour, @cDate+@startOfShift, @cDate+@endOfShift)>=9 )
					begin 
						set @empAllDayLeaveRequest='Y' 
						--ลาแบบไม่หักหรือไม่ เริ่ม
						if exists(
							select 1 from ta_employeeLeaves el inner join ta_leaveTypes lt on el.leaveId=lt.id
							where 
							el.IDNo=@IDNo and el.approvedDate is not null and el.approveResult='Y' 
							and @cDate+@startOfShift between el.startDateTime and el.endDateTime 
							and @cDate+@endOfShift between el.startDateTime and el.endDateTime 
							and DATEDIFF(hour, @cDate+@startOfShift, @cDate+@endOfShift)>=9 
							and lt.paymentOnLeave in ('Y','N'))
							begin set @noDeductionLeave = 'Y' end
							else 
							begin set @noDeductionLeave = 'N' end
						--ลาแบบไม่หักหรือไม่ จบ
					end 
					--มีใบลางานแบบทั้งวันหรือไม่ จบ
				end
			end
			else --ยังไม่ได้กำหนดวันหยุดให้
			begin
				--set @stateOfCal=-1
				select @stateOfCal=stuff(@stateOfCal,1,1,'E');
			end
			--กำหนดข้อมูลวันหยุดให้แล้ว จบ			
	end--หากลางาานทั้งวันหรือขอไม่สแกนนิ้วทั้งวัน
	
	insert into @EmpEachDayInfo (
					dummy
					,empHW
					,assignedHwToEmp
					,checkInOutExists
					,noNeedAttdnShift
					,assignedShiftToEmp
					,empAllDayLeaveRequest
					,empReqNoAllDayAttnd
					,empHSubstitution
					,completedAttndInDay
					,noDeductionLeave
					,isCheckin --สแกนเข้าไหม
					,isLate --สายไหม
					,minuteLate --สายกี่นาที			
					,requestNotAttdnCheckIn --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่	
					,requestLeaveAsLate --ขออนุมัติลาตามที่สายหรือไม่ 
					,isCheckout --สแกนออกหรือไม่
					,isEarly --ออกก่อนหรือไม่
					,minuteEarly --นาทีที่ออกก่อน
					,requuestNotAttdnCheckOut --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
					,requestLeaveAsEarly --ลาตามที่สแกนออก่อนหรือไม่
					,isCheckBrake --สแกนพักหรือไม่
					,brakeDuration --จำนวนนาทีที่ไปพัก
					,brakeEarlyOrLate--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
					,minuteBrakeEarlyOrLate --จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
					,minuteLeaveDuringBrake--จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
					,paymentOfLeaveDuringBrake--หักที่ลาระหว่างพักหรือไม่
					,checkTimeForLeaveDuringBrakeOK --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
					,stateOfCal
					) select 
					@dummy
					,@empHW
					,@assignedHwToEmp
					,@checkInOutExists
					,@noNeedAttdnShift
					,@assignedShiftToEmp
					,@empAllDayLeaveRequest
					,@empReqNoAllDayAttnd
					,@empHSubstitution
					,@completedAttndInDay
					,@noDeductionLeave
					,@isCheckin --สแกนเข้าไหม
					,@isLate --สายไหม
					,@minuteLate --สายกี่นาที			
					,@requestNotAttdnCheckIn --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่	
					,@requestLeaveAsLate --ขออนุมัติลาตามที่สายหรือไม่ 
					,@isCheckout --สแกนออกหรือไม่
					,@isEarly --ออกก่อนหรือไม่
					,@minuteEarly --นาทีที่ออกก่อน
					,@requuestNotAttdnCheckOut --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
					,@requestLeaveAsEarly --ลาตามที่สแกนออก่อนหรือไม่
					,@isCheckBrake --สแกนพักหรือไม่
					,@brakeDuration--จำนวนนาทีที่ไปพัก
					,@brakeEarlyOrLate--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
					,@minuteBrakeEarlyOrLate --จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
					,@minuteLeaveDuringBrake --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
					,@paymentOfLeaveDuringBrake--หักที่ลาระหว่างพักหรือไม่
					,@checkTimeForLeaveDuringBrakeOK --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
					,@stateOfCal
	RETURN 
END


