declare @empLeaveId int
Reselect:

select @empLeaveId = null;

select 
top 1 @empLeaveId=elt.id
/*
elt.id,elt.IDNo
,elt.leaveId,elt.startDateTime,elt.endDateTime,elt.formated
,DATEDIFF(minute,elt.startDateTime,elt.endDateTime) minuteLeave,h.firstName,h.lastName,h.officeName,h.positionName,h.corpName
,elt.leaveMinutes,elt.leaveDay,elt.leaveHour,elt.leaveMinute,elt.comments,elt.approvedBy,elt.approvedDate,elt.approverComments,elt.approveResult,elt.submitDate
,' insert into ta_shiftForEmployees(IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress) values('''+elt.IDNo+''',3,''admin'',getdate(),''Y'','''+CONVERT(varchar,elt.startDateTime,112)+''','''+CONVERT(varchar,elt.startDateTime,112)+''',getdate(),''admin'',getdate(),''admin'',''192.168.0.94'') ' sql1
,' insert into ta_shiftForEmployees(IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress) values('''+elt.IDNo+''',3,''admin'',getdate(),''Y'','''+CONVERT(varchar,elt.startDateTime,112)+''','''+CONVERT(varchar,elt.endDateTime,112)+''',getdate(),''admin'',getdate(),''admin'',''192.168.0.94'') ' sql2

,elt.imagefile,elt.status,elt.insertBy,elt.insertSince,elt.lastUpdate,elt.lastUpdateBy,elt.ipaddress
*/
from ta_employeeLeavesCalTime elt 
left join hrm_humanoPersonAdjust h on elt.IDNo=h.IDNo 
left join ta_shiftDuringLeave sdl on elt.id=sdl.leaveId
where 1=1
and [status]=1
and h.em_status in ('W','P','RR')
and elt.approveResult='Y'
and sdl.id is null
--print @empLeaveId

delete from ta_shiftDuringLeave where leaveId=@empLeaveId
insert into ta_shiftDuringLeave (leaveId,leaveDate,shiftId) 
select
el.id leaveId,dl.dt leaveDate , empShift.shiftId
from ta_employeeLeaves el 
cross apply dbo.FN_dayList(
		 convert(varchar(max),el.startDateTime,112)
		,convert(varchar(max),el.endDateTime,112)
		) dl
cross apply (
	select she.shiftId shiftId from ta_shiftForEmployees she where dl.dt between she.activeFrom and she.activeTo and she.active='Y' and she.IDNo=el.IDNo
) empShift
where el.id=@empLeaveId

if ISNULL(@empLeaveId,0)=0 begin goto endScript end else goto Reselect

endScript: