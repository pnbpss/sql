use EMPBase;
--set nocount on;
IF OBJECT_ID('tempdb..#result') IS NOT NULL DROP TABLE #result
declare @leaveFromDateTime datetime, @leaveToDateTime datetime, @IDNo varchar(13), @result varchar(3);
select @leaveFromDateTime='2018/12/21 08:00', @leaveToDateTime = '2018/12/22 17:00', @IDNo='3940200536679';
if DATEDIFF(day,@leaveFromDateTime,@leaveToDateTime)>365 
begin 
	select @result='no';
end
else
begin
	declare @yearOfDayPeriod table (m int, y int, periodStart datetime, periodEnd datetime, startOfDay datetime, endOfDay datetime)
	insert into @yearOfDayPeriod
	select 
	mlist.m,ylist.y,peDate.st periodStart,peDate.ed periodEnd,dl.dt startOfDay,dateadd(minute,59,DATEADD(hour,23,dl.dt)) endOfday
	from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
	cross apply (
		select YEAR(@leaveFromDateTime)-1 y 	
		union	
		select YEAR(@leaveFromDateTime) y 	
		union	
		select YEAR(@leaveFromDateTime)+1 y 
	) ylist
	cross apply (
		select 1 m union select 2 m union select 3 m union select 4 m union select 5 m union select 6 m union select 7 m union select 8 m union select 9 m union select 10 m union select 11 m union select 12 m 
	)mlist
	cross apply (
	select
		dateadd(month,pe_startmonth,CONVERT(varchar,ylist.y)+'/'+CONVERT(varchar,mlist.m)+'/'+CONVERT(varchar,pe_start_date)) 	st
		,dateadd(
					minute,59
					,dateadd(hour,23,dateadd(
							month,pe_endmonth,CONVERT(varchar,ylist.y)+'/'
							+CONVERT(varchar,mlist.m)+'/'+CONVERT(varchar
							,case when pe_end_date>DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,ylist.y)+'/'+CONVERT(varchar,mlist.m)+'/'+CONVERT(varchar,1)))) 
								then DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,ylist.y)+'/'+CONVERT(varchar,mlist.m)+'/'+CONVERT(varchar,1))))else pe_end_date 
							end
						)
					)
				)
		) ed
	)peDate
	cross apply dbo.FN_dayList(peDate.st,peDate.ed) dl
	where h.IDNo=@IDNo and h.em_status in ('W','P','RR')
	--order by ylist.y, mlist.m
	--select @leaveFromDateTime='2018/12/19 08:00', @leaveToDateTime = '2018/12/27 17:00', @IDNo='3940200536679';

	if (select y from @yearOfDayPeriod tb where @leaveFromDateTime between tb.startOfDay and tb.endOfDay)<>(select y from @yearOfDayPeriod tb where @leaveToDateTime between tb.startOfDay and tb.endOfDay)
	begin select @result='no' end else begin select @result='yes' end

end

select @result r into #result;

select * from #result

go
