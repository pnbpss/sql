use EMPBase;
declare @year varchar(max),@month varchar(max),@zone varchar(max),@depId varchar(max);
set @year='2559';set @month='�ԧ�Ҥ�';set @zone='69';set @depId = '000007';
IF OBJECT_ID('tempdb.dbo.##tempEmpBpsPanu', 'U') IS NOT NULL DROP TABLE ##tempEmpBpsPanu;
IF OBJECT_ID('tempdb.dbo.##finalEmpBpspanu', 'U') IS NOT NULL DROP TABLE ##finalEmpBpspanu;
IF OBJECT_ID('tempdb.dbo.##finalEmpBpspanuFinal', 'U') IS NOT NULL DROP TABLE ##finalEmpBpspanuFinal;
IF OBJECT_ID('tempdb.dbo.##empHeader', 'U') IS NOT NULL DROP TABLE ##empHeader;

select 
jid,JobName,kd,DName,locat,typeDesc,SUM(total) sumTotal 
into ##tempEmpBpsPanu
from 
(
	select jid,job.JobName,kd,dep.DName
	,total
	,case when typ in ('1','3') then 'baht_'+locat
		when typ in ('2') then 'time_'+locat
	end typeDesc
	,B.locat--,B.zone
	FROM [EmpBase].[dbo].[Detail] A
	left join [EMPBase].[dbo].[users] U on A.uid=U.uid
	left join [EmpBase].[dbo].[it_EmpBase_locat] B on U.bid=B.Did
	left join [EmpBase].[dbo].dep dep on A.kd=dep.DID
	left join [EmpBase].[dbo].JobT job on A.jid=job.JobID
	where 1=1
	and yearly=@year 
	and monthly=@month 
	and B.zone=@zone
	--and dep.DID=@depId
) emp
group by jid,JobName,kd,DName,typeDesc,locat
--select COUNT(*) from ##tempEmpBpsPanu

DECLARE @colInPivots NVARCHAR(MAX),@colSelects NVARCHAR(MAX),@colSumByColumns NVARCHAR(MAX),@colSumByRowBahts NVARCHAR(MAX),@colSumByRowTimes NVARCHAR(MAX),@colForHead NVARCHAR(MAX);
DECLARE @query AS NVARCHAR(MAX);

select @colForHead = STUFF((SELECT distinct ' <th colspan=2>' + locat+'</th>' FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') , 1, 1, '');
select @colInPivots = STUFF((SELECT distinct ',' + QUOTENAME('baht_'+locat)+',' + QUOTENAME('time_'+locat) FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') , 1, 1, '');
select @colSelects = STUFF((SELECT distinct ',isnull(' + QUOTENAME('baht_'+locat)+',0)'+QUOTENAME('baht_'+locat)+',isnull(' + QUOTENAME('time_'+locat)+',0)'+QUOTENAME('time_'+locat) FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') , 1, 1, '');
select @colSumByColumns = STUFF((SELECT distinct ',sum(' + QUOTENAME('baht_'+locat)+')'+QUOTENAME('baht_'+locat)+',sum(' + QUOTENAME('time_'+locat)+')'+QUOTENAME('time_'+locat) FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '');
select @colSumByRowBahts = STUFF((SELECT distinct '+isnull(' + QUOTENAME('baht_'+locat)+',0)' FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)') , 1, 1, '');
select @colSumByRowTimes = STUFF((SELECT distinct '+isnull(' + QUOTENAME('time_'+locat)+',0)'FROM ##tempEmpBpsPanu FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '');
                       
set @colSumByRowBahts = '('+@colSumByRowBahts+') sumBahtRow'
set @colSumByRowTimes = '('+@colSumByRowTimes+') sumTimeRow'
print @colForHead
--select distinct locat from #tempEmp order by locat
--select * from #tempEmp
set @query = '
select DName,JobName
,'+@colSelects+','+@colSumByRowBahts+','+@colSumByRowTimes+'
into ##finalEmpBpspanu
from (
	select DName, JobName, typeDesc, sumTotal from ##tempEmpBpsPanu
) t
pivot (
	sum(sumTotal) for typeDesc in ('+@colInPivots+')
) p

select * into ##finalEmpBpspanuFinal
from (
	select DName,JobName,'+@colSelects+',sumBahtRow,sumTimeRow from ##finalEmpBpspanu
	union 
	select DName+''(���)'',''����''+DName JobName,'+@colSumByColumns+',sum(sumBahtRow)sumBahtRow,sum(sumTimeRow)sumTimeRow from ##finalEmpBpspanu group by DName
	
	union 
	select ''����������'' DName,'''' JobName,'+@colSumByColumns+',sum(sumBahtRow)sumBahtRow,sum(sumTimeRow)sumTimeRow from ##finalEmpBpspanu
	
) asdf

select '''+@colForHead+''' colhead into ##empHeader;

';
execute(@query);

select * from ##finalEmpBpspanuFinal order by DName
--select '<tr>'+colhead+'</tr>' colHead from ##empHeader

--select distinct locat from ##tempEmpBpsPanu order by locat
