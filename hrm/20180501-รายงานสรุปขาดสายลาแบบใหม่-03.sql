use EMPBase;
declare @tempTable1 table (
	IDNo	varchar(max),leaveTypeId int,exactMinuteLeave int,	leaveMinutes int,	startDateTime datetime,	endDateTime datetime,	focusedDate datetime,	shiftId int,	shiftStartTime time,	shiftEndTime time,	shiftBrakeStart time,	shiftBrakeEnd time,	eads datetime,	eade datetime
);
declare @fromDate datetime, @toDate datetime;
select @fromDate='20180101',@toDate='20180531';
insert into @tempTable1
select
	ea.IDNo	
	,ea.leaveId leaveTypeId
	,case	--aaa
		--��Ҫ�.������Ѻ��.����ش��ҡѹ (�ҷ���ѹ)
		when (
					datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime)  
					and 
					datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime)
				)
			then 480
			else 				
				case --eee 
					--Ǵ�.����� �������ش���ѹ���ǡѹ
					when CONVERT(varchar,ea.startDatetime,111)=CONVERT(varchar,ea.endDatetime,111) 
					then						
						case --fff 
							--�ӹǹ�ҷ�>240(���Թ�����ѹ) ��� (������ҵ����������� ���� �Ҷ֧�͹����ش��)
							when DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime) > 240 and 
								(
									(datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime))
								or
									(datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime))
								)
							then 
								DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime)-60
							else 
								case --0012
									--�ӹǹ�ҷ�<=240(�Ҥ����ѹ ���� ���¡���) ��� (������ҵ����������� ���� �Ҷ֧�͹����ش��)
									when DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime) <= 240 and 
										(
											(datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime))
										or
											(datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime))
										)
									then 
										DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime)
									else 
										leaveMinutes
								end --0012
						end --fff
					else 
						case --0014
							--�ҡ�������ѹ��е͹�������������繵͹������� ���� �͹����ش�������������繵͹����ش��
							when DATEDIFF(DAY,ea.startDateTime,ea.endDateTime) > 0 
								then
									case --0015
										--�ҡ��������ѹ��� ���� �ѹ���� �����ҡѺ 480
										when (DATEDIFF(DAY,ea.startDateTime,dailyStartAndEnd.startTime) > 0) and (DATEDIFF(DAY,dailyStartAndEnd.endTime,ea.endDateTime) > 0)  then 480
										else 
											case --0016
												--�ҡ���ѹ�ش���� ��� �Ҷ֧��鹡���ѹ�ش���� �����ҡѺ 480												
												when ea.endDateTime=dailyStartAndEnd.endTime then 480
												else 
													case --0017
														--�ҡ���ѹ�á ��� ������Ҩҡ������� �����ҡѺ 480
														when ea.startDateTime=dailyStartAndEnd.startTime then 480
														else 
															case --0018
																-- �ҡ������������Ҿѡ
																when (CONVERT(time,ea.endDateTime) <= sh.breakStart) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش)
																	then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime))
																when (CONVERT(time,ea.startDateTime) >= sh.breakEnd) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����)
																	then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime)																
																else 
																	case --0019
																		--�ҡ��������Ҿѡ Ẻ������Դ
																		when (CONVERT(time,ea.endDateTime) >= sh.breakEnd) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش)
																			then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime)) - 60
																		when (CONVERT(time,ea.startDateTime) <= sh.breakStart) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����)
																			then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime) - 60
																		else
																			case --0020
																				--�ҡ��������ҾѡẺ����Դ(��������Ѻ���Ҿѡ)
																				when (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش) �ѹ���ӹǳ����ѹ�������ش�����
																					then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime)) - (DATEDIFF(MINUTE,sh.breakStart,CONVERT(time,ea.endDateTime)))
																				when (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����) �ѹ���ӹǳ����ѹ���������鹡����
																					then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime) - (DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.breakEnd))
																				else -2	
																			end --0020
																	end --0019
															end --0018 
													end --0017
											end --0016
									end --0014
								else
								-1
						end --0014
				end --eee
	end --aaa
	exactMinuteLeave
	,ea.leaveMinutes
	,ea.startDateTime,ea.endDateTime
	,dl.dt [focusedDate]
	,she.shiftId,sh.startTime shiftStartTime,sh.endTime shiftEndTime,sh.breakStart shiftBrakeStart,sh.breakEnd shiftBrakeEnd
	,dailyStartAndEnd.startTime eads
	,dailyStartAndEnd.endTime eade
	--,'insert into ta_shiftForEmployees (IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress) values('''+ea.IDNo+''',3,''TJY4000010'',getdate(),''Y'','''+convert(varchar,dl.dt,111)+''','''+convert(varchar,dl.dt,111)+''',getdate(),''TJY4000010'',getdate(),''TJY4000010'',''192.168.0.94'') ' sqlstr
	
from ta_employeeLeavesCalTime ea
cross apply dbo.FN_dayList(convert(varchar(max),ea.startDateTime,111),convert(varchar(max),ea.endDateTime,111)) dl
left join ta_shiftForEmployees she on ea.IDNo=she.IDNo and dl.dt between she.activeFrom and she.activeTo
left join ta_shiftsView sh on she.shiftId=sh.id
cross apply (
					select dl.dt+sh.startTime startTime,dl.dt+sh.endTime endTime
				) dailyStartAndEnd
where 1=1	
and dl.dt between @fromDate and @toDate
and ea.approveResult='Y' and ea.[status]=1


select t.IDNo
	,case 
		when t.leaveTypeId in (
		 1--�ҺǪ(���ͧ)
		,2--�ҹ�Ǫ�ҵ�,����ͧ
		,3--�ҹ�觧ҹ���ͧ
		,4--�ҹ�觧ҹ�ҵԾ���ͧ
		,5--�����͹�����Ǿ�ѡ�ҹ㹺���ѷ
		,6--���͵��ͧ/���������
		,7--�ҹȾ������ ����ͧ ������ ����� ��͵�������
		,8--�ҹȾ�ҵ�
		,9--��㺢Ѻ���
		,10--���㺢Ѻ���
		,11--�Ӻѵû�ЪҪ�/��ͺѵû�ЪҪ�
		,12--�١����
		,13--�ѹ��¾����� ����ͧ ������ ����� ��͵�������
		,14--�Ӻح�ú�ͺ�ѹ��¢ͧ�ҵ�
		,15--�Ӻح�ú�ͺ�ѹ��¾�����  ����ͧ�ͧ���ͧ
		,16--�һ��� (�����)��Ѻ�ͧᾷ��
		,18--�ҡԨ
		,20--�ҡԨ�óթء�Թ
		,50--�ҵ��
		)  	then 'errand'
		
		when t.leaveTypeId in (17) then	'vacation'--�Ҿѡ��͹
		when t.leaveTypeId in (19) then 'maternity'--�Ҥ�ʹ
		when t.leaveTypeId in (
			 21--�һ��� (��)��Ѻ�ͧᾷ��
			,49--�һ��� ᾷ��Ѵ+��㺹Ѵ
		) then'sick'
		when t.leaveTypeId in (50) then 'dead'--�ҵ��
		else 'notDefined'
	end msg
	,'M_'+right('00'+convert(varchar(2),MONTH(focusedDate)),2)M
	--,SUM(t.exactMinuteLeave)exactMinuteLeave,COUNT(*)cnt
,cast(SUM(t.exactMinuteLeave)/480.00 as decimal(18,2))cnt
from @tempTable1 t
group by t.IDNo,t.leaveTypeId,MONTH(focusedDate)--,YEAR(focusedDate)