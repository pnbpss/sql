use EMPBase;

IF OBJECT_ID('tempdb..#personal') IS NOT NULL DROP TABLE #personal
IF OBJECT_ID('tempdb..#finalResult') IS NOT NULL DROP TABLE #finalResult

declare @fromdate datetime, @toDate datetime;
declare @uallTable table(IDNo varchar(max),msg varchar(max),M varchar(max),cnt decimal(18,2));

--declare #personal table (IDNo varchar(max))

select distinct IDNo into #personal from hrm_humanoPersonAdjust h where h.hrmCorpId='O6K16BG00000042'

select @fromDate='20180301',@toDate='20180420';



insert into @uallTable
select uallMain.IDNo,uallMain.msg,uallMain.M,uallMain.cnt 
from 
(
	--�Ҵ�ҹ
	select 
		uall.IDNo,typ msg,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)M 
		,COUNT(*) cnt
	from (
			select IDNo,personInfo,cDate,dow,msg,ttd,absent,leave,brake,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut,soc,'absent' typ
			from ta_lateAbsentStats 
			where 1=1
			and IDNo in (select IDNo from #personal)
			and cDate between @fromDate and @toDate
			and
			(
				(
				msg like '%�Ҵ�ҹ%' 	
				)
		)
	)uall
	group by uall.IDNo,typ,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)

	--���
	union
	select 
		uall.IDNo,typ msg,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)M 
		,COUNT(*) cnt
	from 
	(
		select IDNo,personInfo,cDate,dow,msg,ttd,[absent],leave,brake,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut,soc,'late' typ
		from ta_lateAbsentStats 
		where 1=1
		and cDate between @fromDate and @toDate
		and IDNo in (select IDNo from #personal)
		and
		(
			(
			msg like '%���%' 
			and not (msg like '%���͹����__�ҷ�(0�ҷ�)%' or msg like '%���͹����___�ҷ�(0�ҷ�)%')
			and msg not like '%����������Ѻ�ѹ��ش�����ѹ�á%'	
			)	
		)	
	)uall	
	group by uall.IDNo,typ,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)

	--���ŧ������������͡
	union
	select uall.IDNo,typ msg,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)M ,COUNT(*) cnt
	from 
	(
		select IDNo,personInfo,cDate,dow,msg,ttd,[absent],leave,brake,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut,soc,'NoAtt' typ
		from ta_lateAbsentStats 
		where 1=1
		and cDate between @fromDate and @toDate
		and IDNo in (select IDNo from #personal)
		and
		(
			(
			msg like '%���ŧ�����͡%'
			or
			msg like '%���ŧ�������%'	
			)
			
		)
	)uall
	group by uall.IDNo,typ,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)
	
	--�����ŧ����
	union
	select 
	IDNo,msg,M,cnt
	from (
		select 	
				ea.IDNo
				,'CIN' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
				,COUNT(*) cnt
			from ta_empNotAttendances ea
			cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
			where 1=1	
			and dl.dt between @fromDate and @toDate
			and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
			and ea.checkIn='Y'
			and ea.IDNo in (select IDNo from #personal)
			group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
			union all
			select 	
				ea.IDNo			
				,'COU' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
				,COUNT(*) cnt
			from ta_empNotAttendances ea
			cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
			where 1=1	
			and dl.dt between @fromDate and @toDate
			and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
			and ea.checkOut='Y'
			and ea.IDNo in (select IDNo from #personal)
			group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
			union all
			select 	
				ea.IDNo			
				,'CBS' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
				,COUNT(*) cnt
			from ta_empNotAttendances ea
			cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
			where 1=1	
			and dl.dt between @fromDate and @toDate
			and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
			and ea.brakeStart='Y'
			and ea.IDNo in (select IDNo from #personal)
			group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
			union all
			select 	
				ea.IDNo			
				,'CBE' msg ,'M_'+right('00'+convert(varchar(2),month(dl.dt)),2) M			
				,COUNT(*) cnt
			from ta_empNotAttendances ea
			cross apply dbo.FN_dayList(ea.fromDate,ea.toDate) dl
			where 1=1	
			and dl.dt between @fromDate and @toDate
			and ea.[status]='A' and ea.approved='Y' and ea.approvedDate is not null
			and ea.brakeEnd='Y'
			and ea.IDNo in (select IDNo from #personal)
			group by ea.IDNO,right('00'+convert(varchar(2),month(dl.dt)),2)
	)uall
	
	--����ѡ 
	union
	select 
	IDNo,'ttd' msg,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)M, SUM([absent]+leave+brake+lateAndEarly+errandLeave+sickLeave+notCheckInOrOut) cnt
	from ta_lateAbsentStats
	where cDate between @fromDate and @toDate and IDNo in (select IDNo from #personal)
	
	group by IDNo,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)
)uallMain

--��
declare @tempTable1 table (
	IDNo	varchar(max),leaveTypeId int,exactMinuteLeave int,	leaveMinutes int,	startDateTime datetime,	endDateTime datetime,	focusedDate datetime,	shiftId int,	shiftStartTime time,	shiftEndTime time,	shiftBrakeStart time,	shiftBrakeEnd time,	eads datetime,	eade datetime
);
insert into @tempTable1
select
	ea.IDNo	
	,ea.leaveId leaveTypeId
	,case	--aaa
		--��Ҫ�.������Ѻ��.����ش��ҡѹ (�ҷ���ѹ)
		when (
					datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime)  
					and 
					datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime)
				)
			then 480
			else 				
				case --eee 
					--Ǵ�.����� �������ش���ѹ���ǡѹ
					when CONVERT(varchar,ea.startDatetime,111)=CONVERT(varchar,ea.endDatetime,111) 
					then						
						case --fff 
							--�ӹǹ�ҷ�>240(���Թ�����ѹ) ��� (������ҵ����������� ���� �Ҷ֧�͹����ش��)
							when DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime) > 240 and 
								(
									(datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime))
								or
									(datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime))
								)
							then 
								DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime)-60
							else 
								case --0012
									--�ӹǹ�ҷ�<=240(�Ҥ����ѹ ���� ���¡���) ��� (������ҵ����������� ���� �Ҷ֧�͹����ش��)
									when DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime) <= 240 and 
										(
											(datepart(hour,ea.startDateTime) = DATEPART(hour,dailyStartAndEnd.startTime) and datepart(MINUTE,ea.startDateTime) = DATEPART(MINUTE,dailyStartAndEnd.startTime))
										or
											(datepart(hour,ea.endDateTime) = DATEPART(hour,dailyStartAndEnd.endTime) and datepart(MINUTE,ea.endDateTime) = DATEPART(MINUTE,dailyStartAndEnd.endTime))
										)
									then 
										DATEDIFF(MINUTE,ea.startDatetime,ea.endDatetime)
									else 
										leaveMinutes
								end --0012
						end --fff
					else 
						case --0014
							--�ҡ�������ѹ��е͹�������������繵͹������� ���� �͹����ش�������������繵͹����ش��
							when DATEDIFF(DAY,ea.startDateTime,ea.endDateTime) > 0 
								then
									case --0015
										--�ҡ��������ѹ��� ���� �ѹ���� �����ҡѺ 480
										when (DATEDIFF(DAY,ea.startDateTime,dailyStartAndEnd.startTime) > 0) and (DATEDIFF(DAY,dailyStartAndEnd.endTime,ea.endDateTime) > 0)  then 480
										else 
											case --0016
												--�ҡ���ѹ�ش���� ��� �Ҷ֧��鹡���ѹ�ش���� �����ҡѺ 480												
												when ea.endDateTime=dailyStartAndEnd.endTime then 480
												else 
													case --0017
														--�ҡ���ѹ�á ��� ������Ҩҡ������� �����ҡѺ 480
														when ea.startDateTime=dailyStartAndEnd.startTime then 480
														else 
															case --0018
																-- �ҡ������������Ҿѡ
																when (CONVERT(time,ea.endDateTime) <= sh.breakStart) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش)
																	then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime))
																when (CONVERT(time,ea.startDateTime) >= sh.breakEnd) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����)
																	then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime)																
																else 
																	case --0019
																		--�ҡ��������Ҿѡ Ẻ������Դ
																		when (CONVERT(time,ea.endDateTime) >= sh.breakEnd) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش)
																			then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime)) - 60
																		when (CONVERT(time,ea.startDateTime) <= sh.breakStart) and (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����)
																			then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime) - 60
																		else
																			case --0020
																				--�ҡ��������ҾѡẺ����Դ(��������Ѻ���Ҿѡ)
																				when (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.endDateTime,111)) --�����ǧ��� (�ѹ����ش) �ѹ���ӹǳ����ѹ�������ش�����
																					then DATEDIFF(MINUTE,sh.startTime,CONVERT(time,ea.endDateTime)) - (DATEDIFF(MINUTE,sh.breakStart,CONVERT(time,ea.endDateTime)))
																				when (CONVERT(varchar(max),dl.dt,111)=CONVERT(varchar(max),ea.startDateTime,111)) --�����ǧ���� (�ѹ�����) �ѹ���ӹǳ����ѹ���������鹡����
																					then DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.endTime) - (DATEDIFF(MINUTE,CONVERT(time,ea.startDateTime),sh.breakEnd))
																				else -2	
																			end --0020
																	end --0019
															end --0018 
													end --0017
											end --0016
									end --0014
								else
								-1
						end --0014
				end --eee
	end --aaa
	exactMinuteLeave
	,ea.leaveMinutes
	,ea.startDateTime,ea.endDateTime
	,dl.dt [focusedDate]
	,she.shiftId,sh.startTime shiftStartTime,sh.endTime shiftEndTime,sh.breakStart shiftBrakeStart,sh.breakEnd shiftBrakeEnd
	,dailyStartAndEnd.startTime eads
	,dailyStartAndEnd.endTime eade
	--,'insert into ta_shiftForEmployees (IDNo,shiftId,assignBy,assignedDate,active,activeFrom,activeTo,insertSince,insertBy,lastUpdate,updateBy,ipaddress) values('''+ea.IDNo+''',3,''TJY4000010'',getdate(),''Y'','''+convert(varchar,dl.dt,111)+''','''+convert(varchar,dl.dt,111)+''',getdate(),''TJY4000010'',getdate(),''TJY4000010'',''192.168.0.94'') ' sqlstr
	
from ta_employeeLeavesCalTime ea
cross apply dbo.FN_dayList(convert(varchar(max),ea.startDateTime,111),convert(varchar(max),ea.endDateTime,111)) dl
left join ta_shiftForEmployees she on ea.IDNo=she.IDNo and dl.dt between she.activeFrom and she.activeTo
left join ta_shiftsView sh on she.shiftId=sh.id
cross apply (
					select dl.dt+sh.startTime startTime,dl.dt+sh.endTime endTime
				) dailyStartAndEnd
where 1=1	
and dl.dt between @fromDate and @toDate
and ea.approveResult='Y' and ea.[status]=1
and ea.IDNo in (select IDNo from #personal)

insert into @uallTable
select t.IDNo
	,case 
		when t.leaveTypeId in (
		 1--�ҺǪ(���ͧ)
		,2--�ҹ�Ǫ�ҵ�,����ͧ
		,3--�ҹ�觧ҹ���ͧ
		,4--�ҹ�觧ҹ�ҵԾ���ͧ
		,5--�����͹�����Ǿ�ѡ�ҹ㹺���ѷ
		,6--���͵��ͧ/���������
		,7--�ҹȾ������ ����ͧ ������ ����� ��͵�������
		,8--�ҹȾ�ҵ�
		,9--��㺢Ѻ���
		,10--���㺢Ѻ���
		,11--�Ӻѵû�ЪҪ�/��ͺѵû�ЪҪ�
		,12--�١����
		,13--�ѹ��¾����� ����ͧ ������ ����� ��͵�������
		,14--�Ӻح�ú�ͺ�ѹ��¢ͧ�ҵ�
		,15--�Ӻح�ú�ͺ�ѹ��¾�����  ����ͧ�ͧ���ͧ
		,16--�һ��� (�����)��Ѻ�ͧᾷ��
		,18--�ҡԨ
		,20--�ҡԨ�óթء�Թ
		,50--�ҵ��
		)  	then 'errand'
		
		when t.leaveTypeId in (17) then	'vacation'--�Ҿѡ��͹
		when t.leaveTypeId in (19) then 'maternity'--�Ҥ�ʹ
		when t.leaveTypeId in (
			 21--�һ��� (��)��Ѻ�ͧᾷ��
			,49--�һ��� ᾷ��Ѵ+��㺹Ѵ
		) then'sick'
		when t.leaveTypeId in (50) then 'dead'--�ҵ��
		else 'notDefined'
	end msg
	,'M_'+right('00'+convert(varchar(2),MONTH(focusedDate)),2)M
	--,SUM(t.exactMinuteLeave)exactMinuteLeave,COUNT(*)cnt
,cast(SUM(t.exactMinuteLeave)/480.00 as decimal(18,2))cnt
from @tempTable1 t
group by t.IDNo,t.leaveTypeId,MONTH(focusedDate)--,YEAR(focusedDate)

select 
h.IDNo,h.titleName+h.firstName+' '+h.lastName empName
--,h.period_no
,h.corpName+'/'+h.positionName+'/'+h.officeName officeInfo
,isnull(pv.[errand_M_01],0)[errand_M_01]	,isnull(pv.[sick_M_01],0)[sick_M_01]	,isnull(pv.[vacation_M_01],0)[vacation_M_01]	,isnull(pv.[maternity_M_01],0)[maternity_M_01]	,isnull(pv.[absent_M_01],0)[absent_M_01]	,isnull(pv.[late_M_01],0)[late_M_01]	,isnull(pv.[NoAtt_M_01],0)[NoAtt_M_01]	,isnull(pv.[CIN_M_01],0)[CIN_M_01]	,isnull(pv.[CBS_M_01],0)[CBS_M_01]	,isnull(pv.[CBE_M_01],0)[CBE_M_01]	,isnull(pv.[COU_M_01],0)[COU_M_01]	,isnull(pv.[ttd_M_01],0)[ttd_M_01]
,isnull(pv.[errand_M_02],0)[errand_M_02]	,isnull(pv.[sick_M_02],0)[sick_M_02]	,isnull(pv.[vacation_M_02],0)[vacation_M_02]	,isnull(pv.[maternity_M_02],0)[maternity_M_02]	,isnull(pv.[absent_M_02],0)[absent_M_02]	,isnull(pv.[late_M_02],0)[late_M_02]	,isnull(pv.[NoAtt_M_02],0)[NoAtt_M_02]	,isnull(pv.[CIN_M_02],0)[CIN_M_02]	,isnull(pv.[CBS_M_02],0)[CBS_M_02]	,isnull(pv.[CBE_M_02],0)[CBE_M_02]	,isnull(pv.[COU_M_02],0)[COU_M_02]	,isnull(pv.[ttd_M_02],0)[ttd_M_02]
,isnull(pv.[errand_M_03],0)[errand_M_03]	,isnull(pv.[sick_M_03],0)[sick_M_03]	,isnull(pv.[vacation_M_03],0)[vacation_M_03]	,isnull(pv.[maternity_M_03],0)[maternity_M_03]	,isnull(pv.[absent_M_03],0)[absent_M_03]	,isnull(pv.[late_M_03],0)[late_M_03]	,isnull(pv.[NoAtt_M_03],0)[NoAtt_M_03]	,isnull(pv.[CIN_M_03],0)[CIN_M_03]	,isnull(pv.[CBS_M_03],0)[CBS_M_03]	,isnull(pv.[CBE_M_03],0)[CBE_M_03]	,isnull(pv.[COU_M_03],0)[COU_M_03]	,isnull(pv.[ttd_M_03],0)[ttd_M_03]
,isnull(pv.[errand_M_04],0)[errand_M_04]	,isnull(pv.[sick_M_04],0)[sick_M_04]	,isnull(pv.[vacation_M_04],0)[vacation_M_04]	,isnull(pv.[maternity_M_04],0)[maternity_M_04]	,isnull(pv.[absent_M_04],0)[absent_M_04]	,isnull(pv.[late_M_04],0)[late_M_04]	,isnull(pv.[NoAtt_M_04],0)[NoAtt_M_04]	,isnull(pv.[CIN_M_04],0)[CIN_M_04]	,isnull(pv.[CBS_M_04],0)[CBS_M_04]	,isnull(pv.[CBE_M_04],0)[CBE_M_04]	,isnull(pv.[COU_M_04],0)[COU_M_04]	,isnull(pv.[ttd_M_04],0)[ttd_M_04]
,isnull(pv.[errand_M_05],0)[errand_M_05]	,isnull(pv.[sick_M_05],0)[sick_M_05]	,isnull(pv.[vacation_M_05],0)[vacation_M_05]	,isnull(pv.[maternity_M_05],0)[maternity_M_05]	,isnull(pv.[absent_M_05],0)[absent_M_05]	,isnull(pv.[late_M_05],0)[late_M_05]	,isnull(pv.[NoAtt_M_05],0)[NoAtt_M_05]	,isnull(pv.[CIN_M_05],0)[CIN_M_05]	,isnull(pv.[CBS_M_05],0)[CBS_M_05]	,isnull(pv.[CBE_M_05],0)[CBE_M_05]	,isnull(pv.[COU_M_05],0)[COU_M_05]	,isnull(pv.[ttd_M_05],0)[ttd_M_05]
,isnull(pv.[errand_M_06],0)[errand_M_06]	,isnull(pv.[sick_M_06],0)[sick_M_06]	,isnull(pv.[vacation_M_06],0)[vacation_M_06]	,isnull(pv.[maternity_M_06],0)[maternity_M_06]	,isnull(pv.[absent_M_06],0)[absent_M_06]	,isnull(pv.[late_M_06],0)[late_M_06]	,isnull(pv.[NoAtt_M_06],0)[NoAtt_M_06]	,isnull(pv.[CIN_M_06],0)[CIN_M_06]	,isnull(pv.[CBS_M_06],0)[CBS_M_06]	,isnull(pv.[CBE_M_06],0)[CBE_M_06]	,isnull(pv.[COU_M_06],0)[COU_M_06]	,isnull(pv.[ttd_M_06],0)[ttd_M_06]
,isnull(pv.[errand_M_07],0)[errand_M_07]	,isnull(pv.[sick_M_07],0)[sick_M_07]	,isnull(pv.[vacation_M_07],0)[vacation_M_07]	,isnull(pv.[maternity_M_07],0)[maternity_M_07]	,isnull(pv.[absent_M_07],0)[absent_M_07]	,isnull(pv.[late_M_07],0)[late_M_07]	,isnull(pv.[NoAtt_M_07],0)[NoAtt_M_07]	,isnull(pv.[CIN_M_07],0)[CIN_M_07]	,isnull(pv.[CBS_M_07],0)[CBS_M_07]	,isnull(pv.[CBE_M_07],0)[CBE_M_07]	,isnull(pv.[COU_M_07],0)[COU_M_07]	,isnull(pv.[ttd_M_07],0)[ttd_M_07]
,isnull(pv.[errand_M_08],0)[errand_M_08]	,isnull(pv.[sick_M_08],0)[sick_M_08]	,isnull(pv.[vacation_M_08],0)[vacation_M_08]	,isnull(pv.[maternity_M_08],0)[maternity_M_08]	,isnull(pv.[absent_M_08],0)[absent_M_08]	,isnull(pv.[late_M_08],0)[late_M_08]	,isnull(pv.[NoAtt_M_08],0)[NoAtt_M_08]	,isnull(pv.[CIN_M_08],0)[CIN_M_08]	,isnull(pv.[CBS_M_08],0)[CBS_M_08]	,isnull(pv.[CBE_M_08],0)[CBE_M_08]	,isnull(pv.[COU_M_08],0)[COU_M_08]	,isnull(pv.[ttd_M_08],0)[ttd_M_08]
,isnull(pv.[errand_M_09],0)[errand_M_09]	,isnull(pv.[sick_M_09],0)[sick_M_09]	,isnull(pv.[vacation_M_09],0)[vacation_M_09]	,isnull(pv.[maternity_M_09],0)[maternity_M_09]	,isnull(pv.[absent_M_09],0)[absent_M_09]	,isnull(pv.[late_M_09],0)[late_M_09]	,isnull(pv.[NoAtt_M_09],0)[NoAtt_M_09]	,isnull(pv.[CIN_M_09],0)[CIN_M_09]	,isnull(pv.[CBS_M_09],0)[CBS_M_09]	,isnull(pv.[CBE_M_09],0)[CBE_M_09]	,isnull(pv.[COU_M_09],0)[COU_M_09]	,isnull(pv.[ttd_M_09],0)[ttd_M_09]
,isnull(pv.[errand_M_10],0)[errand_M_10]	,isnull(pv.[sick_M_10],0)[sick_M_10]	,isnull(pv.[vacation_M_10],0)[vacation_M_10]	,isnull(pv.[maternity_M_10],0)[maternity_M_10]	,isnull(pv.[absent_M_10],0)[absent_M_10]	,isnull(pv.[late_M_10],0)[late_M_10]	,isnull(pv.[NoAtt_M_10],0)[NoAtt_M_10]	,isnull(pv.[CIN_M_10],0)[CIN_M_10]	,isnull(pv.[CBS_M_10],0)[CBS_M_10]	,isnull(pv.[CBE_M_10],0)[CBE_M_10]	,isnull(pv.[COU_M_10],0)[COU_M_10]	,isnull(pv.[ttd_M_10],0)[ttd_M_10]
,isnull(pv.[errand_M_11],0)[errand_M_11]	,isnull(pv.[sick_M_11],0)[sick_M_11]	,isnull(pv.[vacation_M_11],0)[vacation_M_11]	,isnull(pv.[maternity_M_11],0)[maternity_M_11]	,isnull(pv.[absent_M_11],0)[absent_M_11]	,isnull(pv.[late_M_11],0)[late_M_11]	,isnull(pv.[NoAtt_M_11],0)[NoAtt_M_11]	,isnull(pv.[CIN_M_11],0)[CIN_M_11]	,isnull(pv.[CBS_M_11],0)[CBS_M_11]	,isnull(pv.[CBE_M_11],0)[CBE_M_11]	,isnull(pv.[COU_M_11],0)[COU_M_11]	,isnull(pv.[ttd_M_11],0)[ttd_M_11]
,isnull(pv.[errand_M_12],0)[errand_M_12]	,isnull(pv.[sick_M_12],0)[sick_M_12]	,isnull(pv.[vacation_M_12],0)[vacation_M_12]	,isnull(pv.[maternity_M_12],0)[maternity_M_12]	,isnull(pv.[absent_M_12],0)[absent_M_12]	,isnull(pv.[late_M_12],0)[late_M_12]	,isnull(pv.[NoAtt_M_12],0)[NoAtt_M_12]	,isnull(pv.[CIN_M_12],0)[CIN_M_12]	,isnull(pv.[CBS_M_12],0)[CBS_M_12]	,isnull(pv.[CBE_M_12],0)[CBE_M_12]	,isnull(pv.[COU_M_12],0)[COU_M_12]	,isnull(pv.[ttd_M_12],0)[ttd_M_12]
into #finalResult
from 
hrm_humanoPersonAll h left join
(
select IDNo
	,isnull([errand_M_01],0)[errand_M_01]	,isnull([sick_M_01],0)[sick_M_01]	,isnull([vacation_M_01],0)[vacation_M_01]	,isnull([maternity_M_01],0)[maternity_M_01]	,isnull([absent_M_01],0)[absent_M_01]	,isnull([late_M_01],0)[late_M_01]	,isnull([NoAtt_M_01],0)[NoAtt_M_01]	,isnull([CIN_M_01],0)[CIN_M_01]	,isnull([CBS_M_01],0)[CBS_M_01]	,isnull([CBE_M_01],0)[CBE_M_01]	,isnull([COU_M_01],0)[COU_M_01]	,isnull([ttd_M_01],0)[ttd_M_01]
	,isnull([errand_M_02],0)[errand_M_02]	,isnull([sick_M_02],0)[sick_M_02]	,isnull([vacation_M_02],0)[vacation_M_02]	,isnull([maternity_M_02],0)[maternity_M_02]	,isnull([absent_M_02],0)[absent_M_02]	,isnull([late_M_02],0)[late_M_02]	,isnull([NoAtt_M_02],0)[NoAtt_M_02]	,isnull([CIN_M_02],0)[CIN_M_02]	,isnull([CBS_M_02],0)[CBS_M_02]	,isnull([CBE_M_02],0)[CBE_M_02]	,isnull([COU_M_02],0)[COU_M_02]	,isnull([ttd_M_02],0)[ttd_M_02]
	,isnull([errand_M_03],0)[errand_M_03]	,isnull([sick_M_03],0)[sick_M_03]	,isnull([vacation_M_03],0)[vacation_M_03]	,isnull([maternity_M_03],0)[maternity_M_03]	,isnull([absent_M_03],0)[absent_M_03]	,isnull([late_M_03],0)[late_M_03]	,isnull([NoAtt_M_03],0)[NoAtt_M_03]	,isnull([CIN_M_03],0)[CIN_M_03]	,isnull([CBS_M_03],0)[CBS_M_03]	,isnull([CBE_M_03],0)[CBE_M_03]	,isnull([COU_M_03],0)[COU_M_03]	,isnull([ttd_M_03],0)[ttd_M_03]
	,isnull([errand_M_04],0)[errand_M_04]	,isnull([sick_M_04],0)[sick_M_04]	,isnull([vacation_M_04],0)[vacation_M_04]	,isnull([maternity_M_04],0)[maternity_M_04]	,isnull([absent_M_04],0)[absent_M_04]	,isnull([late_M_04],0)[late_M_04]	,isnull([NoAtt_M_04],0)[NoAtt_M_04]	,isnull([CIN_M_04],0)[CIN_M_04]	,isnull([CBS_M_04],0)[CBS_M_04]	,isnull([CBE_M_04],0)[CBE_M_04]	,isnull([COU_M_04],0)[COU_M_04]	,isnull([ttd_M_04],0)[ttd_M_04]
	,isnull([errand_M_05],0)[errand_M_05]	,isnull([sick_M_05],0)[sick_M_05]	,isnull([vacation_M_05],0)[vacation_M_05]	,isnull([maternity_M_05],0)[maternity_M_05]	,isnull([absent_M_05],0)[absent_M_05]	,isnull([late_M_05],0)[late_M_05]	,isnull([NoAtt_M_05],0)[NoAtt_M_05]	,isnull([CIN_M_05],0)[CIN_M_05]	,isnull([CBS_M_05],0)[CBS_M_05]	,isnull([CBE_M_05],0)[CBE_M_05]	,isnull([COU_M_05],0)[COU_M_05]	,isnull([ttd_M_05],0)[ttd_M_05]
	,isnull([errand_M_06],0)[errand_M_06]	,isnull([sick_M_06],0)[sick_M_06]	,isnull([vacation_M_06],0)[vacation_M_06]	,isnull([maternity_M_06],0)[maternity_M_06]	,isnull([absent_M_06],0)[absent_M_06]	,isnull([late_M_06],0)[late_M_06]	,isnull([NoAtt_M_06],0)[NoAtt_M_06]	,isnull([CIN_M_06],0)[CIN_M_06]	,isnull([CBS_M_06],0)[CBS_M_06]	,isnull([CBE_M_06],0)[CBE_M_06]	,isnull([COU_M_06],0)[COU_M_06]	,isnull([ttd_M_06],0)[ttd_M_06]
	,isnull([errand_M_07],0)[errand_M_07]	,isnull([sick_M_07],0)[sick_M_07]	,isnull([vacation_M_07],0)[vacation_M_07]	,isnull([maternity_M_07],0)[maternity_M_07]	,isnull([absent_M_07],0)[absent_M_07]	,isnull([late_M_07],0)[late_M_07]	,isnull([NoAtt_M_07],0)[NoAtt_M_07]	,isnull([CIN_M_07],0)[CIN_M_07]	,isnull([CBS_M_07],0)[CBS_M_07]	,isnull([CBE_M_07],0)[CBE_M_07]	,isnull([COU_M_07],0)[COU_M_07]	,isnull([ttd_M_07],0)[ttd_M_07]
	,isnull([errand_M_08],0)[errand_M_08]	,isnull([sick_M_08],0)[sick_M_08]	,isnull([vacation_M_08],0)[vacation_M_08]	,isnull([maternity_M_08],0)[maternity_M_08]	,isnull([absent_M_08],0)[absent_M_08]	,isnull([late_M_08],0)[late_M_08]	,isnull([NoAtt_M_08],0)[NoAtt_M_08]	,isnull([CIN_M_08],0)[CIN_M_08]	,isnull([CBS_M_08],0)[CBS_M_08]	,isnull([CBE_M_08],0)[CBE_M_08]	,isnull([COU_M_08],0)[COU_M_08]	,isnull([ttd_M_08],0)[ttd_M_08]
	,isnull([errand_M_09],0)[errand_M_09]	,isnull([sick_M_09],0)[sick_M_09]	,isnull([vacation_M_09],0)[vacation_M_09]	,isnull([maternity_M_09],0)[maternity_M_09]	,isnull([absent_M_09],0)[absent_M_09]	,isnull([late_M_09],0)[late_M_09]	,isnull([NoAtt_M_09],0)[NoAtt_M_09]	,isnull([CIN_M_09],0)[CIN_M_09]	,isnull([CBS_M_09],0)[CBS_M_09]	,isnull([CBE_M_09],0)[CBE_M_09]	,isnull([COU_M_09],0)[COU_M_09]	,isnull([ttd_M_09],0)[ttd_M_09]
	,isnull([errand_M_10],0)[errand_M_10]	,isnull([sick_M_10],0)[sick_M_10]	,isnull([vacation_M_10],0)[vacation_M_10]	,isnull([maternity_M_10],0)[maternity_M_10]	,isnull([absent_M_10],0)[absent_M_10]	,isnull([late_M_10],0)[late_M_10]	,isnull([NoAtt_M_10],0)[NoAtt_M_10]	,isnull([CIN_M_10],0)[CIN_M_10]	,isnull([CBS_M_10],0)[CBS_M_10]	,isnull([CBE_M_10],0)[CBE_M_10]	,isnull([COU_M_10],0)[COU_M_10]	,isnull([ttd_M_10],0)[ttd_M_10]
	,isnull([errand_M_11],0)[errand_M_11]	,isnull([sick_M_11],0)[sick_M_11]	,isnull([vacation_M_11],0)[vacation_M_11]	,isnull([maternity_M_11],0)[maternity_M_11]	,isnull([absent_M_11],0)[absent_M_11]	,isnull([late_M_11],0)[late_M_11]	,isnull([NoAtt_M_11],0)[NoAtt_M_11]	,isnull([CIN_M_11],0)[CIN_M_11]	,isnull([CBS_M_11],0)[CBS_M_11]	,isnull([CBE_M_11],0)[CBE_M_11]	,isnull([COU_M_11],0)[COU_M_11]	,isnull([ttd_M_11],0)[ttd_M_11]
	,isnull([errand_M_12],0)[errand_M_12]	,isnull([sick_M_12],0)[sick_M_12]	,isnull([vacation_M_12],0)[vacation_M_12]	,isnull([maternity_M_12],0)[maternity_M_12]	,isnull([absent_M_12],0)[absent_M_12]	,isnull([late_M_12],0)[late_M_12]	,isnull([NoAtt_M_12],0)[NoAtt_M_12]	,isnull([CIN_M_12],0)[CIN_M_12]	,isnull([CBS_M_12],0)[CBS_M_12]	,isnull([CBE_M_12],0)[CBE_M_12]	,isnull([COU_M_12],0)[COU_M_12]	,isnull([ttd_M_12],0)[ttd_M_12]
	
	from (
	select IDNo,msg+'_'+M info, cnt from @uallTable
	) t pivot (
		max(cnt) for info in ([xx]
		,[absent_M_01]	,[late_M_01]	,[NoAtt_M_01]	,[errand_M_01]	,[sick_M_01]	,[vacation_M_01]	,[maternity_M_01]	,[CIN_M_01]	,[COU_M_01]	,[CBS_M_01]	,[CBE_M_01]	,[ttd_M_01]
		,[absent_M_02]	,[late_M_02]	,[NoAtt_M_02]	,[errand_M_02]	,[sick_M_02]	,[vacation_M_02]	,[maternity_M_02]	,[CIN_M_02]	,[COU_M_02]	,[CBS_M_02]	,[CBE_M_02]	,[ttd_M_02]
		,[absent_M_03]	,[late_M_03]	,[NoAtt_M_03]	,[errand_M_03]	,[sick_M_03]	,[vacation_M_03]	,[maternity_M_03]	,[CIN_M_03]	,[COU_M_03]	,[CBS_M_03]	,[CBE_M_03]	,[ttd_M_03]
		,[absent_M_04]	,[late_M_04]	,[NoAtt_M_04]	,[errand_M_04]	,[sick_M_04]	,[vacation_M_04]	,[maternity_M_04]	,[CIN_M_04]	,[COU_M_04]	,[CBS_M_04]	,[CBE_M_04]	,[ttd_M_04]
		,[absent_M_05]	,[late_M_05]	,[NoAtt_M_05]	,[errand_M_05]	,[sick_M_05]	,[vacation_M_05]	,[maternity_M_05]	,[CIN_M_05]	,[COU_M_05]	,[CBS_M_05]	,[CBE_M_05]	,[ttd_M_05]
		,[absent_M_06]	,[late_M_06]	,[NoAtt_M_06]	,[errand_M_06]	,[sick_M_06]	,[vacation_M_06]	,[maternity_M_06]	,[CIN_M_06]	,[COU_M_06]	,[CBS_M_06]	,[CBE_M_06]	,[ttd_M_06]
		,[absent_M_07]	,[late_M_07]	,[NoAtt_M_07]	,[errand_M_07]	,[sick_M_07]	,[vacation_M_07]	,[maternity_M_07]	,[CIN_M_07]	,[COU_M_07]	,[CBS_M_07]	,[CBE_M_07]	,[ttd_M_07]
		,[absent_M_08]	,[late_M_08]	,[NoAtt_M_08]	,[errand_M_08]	,[sick_M_08]	,[vacation_M_08]	,[maternity_M_08]	,[CIN_M_08]	,[COU_M_08]	,[CBS_M_08]	,[CBE_M_08]	,[ttd_M_08]
		,[absent_M_09]	,[late_M_09]	,[NoAtt_M_09]	,[errand_M_09]	,[sick_M_09]	,[vacation_M_09]	,[maternity_M_09]	,[CIN_M_09]	,[COU_M_09]	,[CBS_M_09]	,[CBE_M_09]	,[ttd_M_09]
		,[absent_M_10]	,[late_M_10]	,[NoAtt_M_10]	,[errand_M_10]	,[sick_M_10]	,[vacation_M_10]	,[maternity_M_10]	,[CIN_M_10]	,[COU_M_10]	,[CBS_M_10]	,[CBE_M_10]	,[ttd_M_10]
		,[absent_M_11]	,[late_M_11]	,[NoAtt_M_11]	,[errand_M_11]	,[sick_M_11]	,[vacation_M_11]	,[maternity_M_11]	,[CIN_M_11]	,[COU_M_11]	,[CBS_M_11]	,[CBE_M_11]	,[ttd_M_11]
		,[absent_M_12]	,[late_M_12]	,[NoAtt_M_12]	,[errand_M_12]	,[sick_M_12]	,[vacation_M_12]	,[maternity_M_12]	,[CIN_M_12]	,[COU_M_12]	,[CBS_M_12]	,[CBE_M_12]	,[ttd_M_12]
		)
	) p
)pv on pv.IDNo=h.IDNo
where h.em_status in ('W','P','RR')
and h.period_no not in ('','PPK16C800000052'/*���.��������*/)
and h.IDNo not in (select IDNo from hrm_humanoPersonAll where period_no='PPK16BG00000043' and (positionName like '%��ͺ�ҹ%' or positionName like '%����ҹ%'))
and h.IDNo in (select IDNo from #personal)

if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=1) begin alter table #finalResult drop column [absent_M_01]	; alter table #finalResult drop column [late_M_01]	; alter table #finalResult drop column [NoAtt_M_01]	; alter table #finalResult drop column [errand_M_01]	; alter table #finalResult drop column [sick_M_01]	; alter table #finalResult drop column [vacation_M_01]	; alter table #finalResult drop column [maternity_M_01]	; alter table #finalResult drop column [CIN_M_01]	; alter table #finalResult drop column [COU_M_01]	; alter table #finalResult drop column [CBS_M_01]	; alter table #finalResult drop column [CBE_M_01]	; alter table #finalResult drop column [ttd_M_01] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=2) begin alter table #finalResult drop column [absent_M_02]	; alter table #finalResult drop column [late_M_02]	; alter table #finalResult drop column [NoAtt_M_02]	; alter table #finalResult drop column [errand_M_02]	; alter table #finalResult drop column [sick_M_02]	; alter table #finalResult drop column [vacation_M_02]	; alter table #finalResult drop column [maternity_M_02]	; alter table #finalResult drop column [CIN_M_02]	; alter table #finalResult drop column [COU_M_02]	; alter table #finalResult drop column [CBS_M_02]	; alter table #finalResult drop column [CBE_M_02]	; alter table #finalResult drop column [ttd_M_02] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=3) begin alter table #finalResult drop column [absent_M_03]	; alter table #finalResult drop column [late_M_03]	; alter table #finalResult drop column [NoAtt_M_03]	; alter table #finalResult drop column [errand_M_03]	; alter table #finalResult drop column [sick_M_03]	; alter table #finalResult drop column [vacation_M_03]	; alter table #finalResult drop column [maternity_M_03]	; alter table #finalResult drop column [CIN_M_03]	; alter table #finalResult drop column [COU_M_03]	; alter table #finalResult drop column [CBS_M_03]	; alter table #finalResult drop column [CBE_M_03]	; alter table #finalResult drop column [ttd_M_03] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=4) begin alter table #finalResult drop column [absent_M_04]	; alter table #finalResult drop column [late_M_04]	; alter table #finalResult drop column [NoAtt_M_04]	; alter table #finalResult drop column [errand_M_04]	; alter table #finalResult drop column [sick_M_04]	; alter table #finalResult drop column [vacation_M_04]	; alter table #finalResult drop column [maternity_M_04]	; alter table #finalResult drop column [CIN_M_04]	; alter table #finalResult drop column [COU_M_04]	; alter table #finalResult drop column [CBS_M_04]	; alter table #finalResult drop column [CBE_M_04]	; alter table #finalResult drop column [ttd_M_04] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=5) begin alter table #finalResult drop column [absent_M_05]	; alter table #finalResult drop column [late_M_05]	; alter table #finalResult drop column [NoAtt_M_05]	; alter table #finalResult drop column [errand_M_05]	; alter table #finalResult drop column [sick_M_05]	; alter table #finalResult drop column [vacation_M_05]	; alter table #finalResult drop column [maternity_M_05]	; alter table #finalResult drop column [CIN_M_05]	; alter table #finalResult drop column [COU_M_05]	; alter table #finalResult drop column [CBS_M_05]	; alter table #finalResult drop column [CBE_M_05]	; alter table #finalResult drop column [ttd_M_05] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=6) begin alter table #finalResult drop column [absent_M_06]	; alter table #finalResult drop column [late_M_06]	; alter table #finalResult drop column [NoAtt_M_06]	; alter table #finalResult drop column [errand_M_06]	; alter table #finalResult drop column [sick_M_06]	; alter table #finalResult drop column [vacation_M_06]	; alter table #finalResult drop column [maternity_M_06]	; alter table #finalResult drop column [CIN_M_06]	; alter table #finalResult drop column [COU_M_06]	; alter table #finalResult drop column [CBS_M_06]	; alter table #finalResult drop column [CBE_M_06]	; alter table #finalResult drop column [ttd_M_06] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=7) begin alter table #finalResult drop column [absent_M_07]	; alter table #finalResult drop column [late_M_07]	; alter table #finalResult drop column [NoAtt_M_07]	; alter table #finalResult drop column [errand_M_07]	; alter table #finalResult drop column [sick_M_07]	; alter table #finalResult drop column [vacation_M_07]	; alter table #finalResult drop column [maternity_M_07]	; alter table #finalResult drop column [CIN_M_07]	; alter table #finalResult drop column [COU_M_07]	; alter table #finalResult drop column [CBS_M_07]	; alter table #finalResult drop column [CBE_M_07]	; alter table #finalResult drop column [ttd_M_07] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=8) begin alter table #finalResult drop column [absent_M_08]	; alter table #finalResult drop column [late_M_08]	; alter table #finalResult drop column [NoAtt_M_08]	; alter table #finalResult drop column [errand_M_08]	; alter table #finalResult drop column [sick_M_08]	; alter table #finalResult drop column [vacation_M_08]	; alter table #finalResult drop column [maternity_M_08]	; alter table #finalResult drop column [CIN_M_08]	; alter table #finalResult drop column [COU_M_08]	; alter table #finalResult drop column [CBS_M_08]	; alter table #finalResult drop column [CBE_M_08]	; alter table #finalResult drop column [ttd_M_08] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=9) begin alter table #finalResult drop column [absent_M_09]	; alter table #finalResult drop column [late_M_09]	; alter table #finalResult drop column [NoAtt_M_09]	; alter table #finalResult drop column [errand_M_09]	; alter table #finalResult drop column [sick_M_09]	; alter table #finalResult drop column [vacation_M_09]	; alter table #finalResult drop column [maternity_M_09]	; alter table #finalResult drop column [CIN_M_09]	; alter table #finalResult drop column [COU_M_09]	; alter table #finalResult drop column [CBS_M_09]	; alter table #finalResult drop column [CBE_M_09]	; alter table #finalResult drop column [ttd_M_09] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=10) begin alter table #finalResult drop column [absent_M_10]	; alter table #finalResult drop column [late_M_10]	; alter table #finalResult drop column [NoAtt_M_10]	; alter table #finalResult drop column [errand_M_10]	; alter table #finalResult drop column [sick_M_10]	; alter table #finalResult drop column [vacation_M_10]	; alter table #finalResult drop column [maternity_M_10]	; alter table #finalResult drop column [CIN_M_10]	; alter table #finalResult drop column [COU_M_10]	; alter table #finalResult drop column [CBS_M_10]	; alter table #finalResult drop column [CBE_M_10]	; alter table #finalResult drop column [ttd_M_10] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=11) begin alter table #finalResult drop column [absent_M_11]	; alter table #finalResult drop column [late_M_11]	; alter table #finalResult drop column [NoAtt_M_11]	; alter table #finalResult drop column [errand_M_11]	; alter table #finalResult drop column [sick_M_11]	; alter table #finalResult drop column [vacation_M_11]	; alter table #finalResult drop column [maternity_M_11]	; alter table #finalResult drop column [CIN_M_11]	; alter table #finalResult drop column [COU_M_11]	; alter table #finalResult drop column [CBS_M_11]	; alter table #finalResult drop column [CBE_M_11]	; alter table #finalResult drop column [ttd_M_11] end
if not exists(select 1 from dbo.FN_dayList(@fromdate,@toDate) where MONTH(dt)=12) begin alter table #finalResult drop column [absent_M_12]	; alter table #finalResult drop column [late_M_12]	; alter table #finalResult drop column [NoAtt_M_12]	; alter table #finalResult drop column [errand_M_12]	; alter table #finalResult drop column [sick_M_12]	; alter table #finalResult drop column [vacation_M_12]	; alter table #finalResult drop column [maternity_M_12]	; alter table #finalResult drop column [CIN_M_12]	; alter table #finalResult drop column [COU_M_12]	; alter table #finalResult drop column [CBS_M_12]	; alter table #finalResult drop column [CBE_M_12]	; alter table #finalResult drop column [ttd_M_12] end

select * from #finalResult