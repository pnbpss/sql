use EMPBase;
declare @date datetime, @startDate datetime, @endDate datetime;
set @startDate = '20170101'; set @endDate = '20171231'
set @date = @startDate;
while not @date > @endDate
begin
	--print convert(varchar,@date,111);
	print datepart(dw,@date);
	set @date = DATEADD(day,1,@date);
	if datepart(dw,@date) = 7
	begin
		insert into ta_hws(hwGroupId,startDate,endDate,comments) values(7,@date,@date,'หยุดสุดสัปดาห์ วันเสาร์')
	end
end

