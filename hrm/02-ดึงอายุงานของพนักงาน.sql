select 
--[id]
	RANK() over(partition by h.IDNo order by employeeCode) r1
	,[employeeCode]
	,[titleName]
	,[firstName]
	,[lastName]
	,[nick]
	,[gender]
	,dbo.FN_ConvertDate([birthdate],'D/M/Y')birthDate
	,dbo.FN_ConvertDate([workStart],'D/M/Y')workStart
	,case when md.amount<12 then '0 �� ' else convert(nvarchar(10),(md.amount/12))+' �� ' end +case when md.amount<12 then convert(nvarchar(10),(md.amount))+' ��͹'  else convert(nvarchar(10),(md.amount%12))+' ��͹' end as mdiff
	,workStart,workEnd
	,yearDiff.amount yearDiff
	,monthDiff.amount monthDiff	
	,(datediff(day,dateadd(month,monthDiff.amount,DATEADD(YEAR,yearDiff.amount,h.workStart)),dateadd(day,+1,GETDATE()))+0) dayDiff
	,[positionName]
	,[corpName]
	,[officeName]
	,em_status
from hrm_humanoPersonAll h
	cross apply (select DATEDIFF(year,h.workStart,GETDATE())amount)yd
	cross apply (select DATEDIFF(month,h.workStart,GETDATE())amount)md
	cross apply (select DATEDIFF(day,h.workStart,GETDATE())amount)dd
	cross apply (select case when md.amount<12 then 0  else md.amount/12 end amount) yearDiff
	cross apply (select case when md.amount<12 then md.amount else md.amount%12 end amount) monthDiff
where 1=1
--positionName like '%�Ѻö%' and 
--and positionName like '%�����%'
--and positionName like '%���˹��%'
--and positionName like '%����Ѵ%'
and em_status in ('W','P','RR')
--and not (positionName like '%����Ѵ˹���Թ%' and gender='M')
--and not (positionName like '%�Ѻö%' and gender='M' and corpName like '%��������%')
order by h.workStart