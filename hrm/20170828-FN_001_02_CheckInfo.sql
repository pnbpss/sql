USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_02_CheckInInfo]    Script Date: 08/28/2017 17:38:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_02_CheckInInfo] 
(
	-- Add the parameters for the function here
	@cDate datetime,@IDNo varchar(20), @temp_ta_empTaDetails temp_ta_empTaDetails readonly
)
RETURNS @checkInInfo TABLE 
(
	-- Add the column definitions for the TABLE variable here
	isCHeckin char(1)
	,isLate char(1)
	,minuteLate int
)
AS
BEGIN
		declare @tempTaTable  table (employeeCode varchar(20), checkTime datetime);
		declare @isCheckin char(1),@isLate char(1),@minuteLate int				
				
		insert into @tempTaTable
		select SSN,et.CHECKTIME from @temp_ta_empTaDetails et inner join ta_userInfos ut on et.USERID=ut.USERID
		where ut.SSN = (select employeeCode from hrm_humanoPersonAll where IDNo=@IDNo and em_status in ('W','P')) 
		and CHECKTIME between @cDate and dateadd(minute,59,DATEADD(HOUR,23,@cDate))
		
		if exists(
			select 
				sh.musTA,sh.minCheckInTime,sh.startTime,sh.haveToCheckEnd,sh.maxCheckOutTime,sh.endTime
				,sh.brakeCheck,sh.breakStart,sh.breakEnd
				--,*
			from ta_shifts sh,ta_shiftForEmployees she,@tempTaTable ta
			where  sh.id=she.shiftId
			and she.IDNo=@IDNo and @cDate between she.activeFrom and she.activeTo and she.active='Y'
			and ta.CHECKTIME between @cDate+sh.minCheckInTime and dateadd(hour,4,@cDate+sh.startTime)
		) 
		begin
			set @isCheckin = 'Y';
			if not (exists( --เช็คว่าสายไหม
			select 
					sh.musTA,sh.minCheckInTime,sh.startTime,sh.haveToCheckEnd,sh.maxCheckOutTime,sh.endTime
					,sh.brakeCheck,sh.breakStart,sh.breakEnd
					--,*
				from ta_shifts sh,ta_shiftForEmployees she,@tempTaTable ta
				where  sh.id=she.shiftId 	and she.IDNo=@IDNo and @cDate between she.activeFrom and she.activeTo and she.active='Y'
				and ta.CHECKTIME between @cDate+sh.minCheckInTime and dateadd(minute,1,@cDate+sh.startTime)
			))
			begin 
				set @isLate='Y' 
				set @minuteLate = datediff(minute
																,(select @cDate+sh.startTime
																from ta_shifts sh,ta_shiftForEmployees she
																where  sh.id=she.shiftId	and she.IDNo=@IDNo and @cDate between she.activeFrom and she.activeTo and she.active='Y')
																,(select min(CHECKTIME) from @tempTaTable)
																)
			end
			else
			begin
				set @isLate='N' 
				set @minuteLate=null;
			end
		end
		 else
		 begin
			set @isCheckin='N'
		 end
		
		
		
		insert into @checkInInfo select @isCheckin,@isLate,@minuteLate
			
		
	RETURN 
END
