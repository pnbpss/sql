USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_calculateDeduction]    Script Date: 10/13/2017 13:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_calculateDeduction] 
(
	-- Add the parameters for the function here
	@minute int, @idNo varchar(20)
)
RETURNS decimal(18,2)
AS
BEGIN
	-- Declare the return variable here	
	declare @employeeCode varchar(20);
	set @employeeCode = (select employeeCode from hrm_humanoPersonAll h where h.IDNo=@IDNo and h.em_status in('W','P'))
	if isnull(@employeeCode,'')=''
	begin
		set @employeeCode = (select top 1 employeeCode from hrm_humanoPersonAll h where h.IDNo=@IDNo order by workEnd desc)
	end
	RETURN 
					(((dbo.FN_EmployeeSalary(
											@employeeCode,
											'Y'
											)/30 --เอาเงินเดือนหาร 30 วัน ได้ วันละ??บาท
											)/8 --เอาวันละหารด้วย 8 ชม. ได้เป็นชม.ละ??บาท
											)/60--เอา60หารได้ออกมาเป็นนาทีละ??บาท
											)*@minute

END
