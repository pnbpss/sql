USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_calculateMonthlyTA]    Script Date: 10/23/2017 11:03:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_calculateMonthlyTA] 
(
	-- Add the parameters for the function here
	@year int
	,@month int
	,@listOfPersonIDNo listOfPersonIDNo readonly
	,@ta_userInfos temp_ta_userInfosV2 readonly
)
RETURNS 
@firstCalculation TABLE 
(	
	IDNo varchar(20)
	,personInfo varchar(250) collate Thai_CI_AS
	,empHW char(1)
	,assignedHwToEmp char(1)
	,checkInOutExists char(1)
	,noNeedAttdnShift char(1)
	,assignedShiftToEmp char(1)
	,empAllDayLeaveRequest char(1)
	,empReqNoAllDayAttnd char(1)
	,empHSubstitution char(1)
	,noDeductionLeave char(1)
	,completedAttndInDay char(1)
	,cDate datetime
	,cdow varchar(30) collate Thai_CI_AS
	
	,isCheckin char(1) --สแกนเข้าไหม
	,isLate char(1) --สายไหม
	,minuteLate int --สายกี่นาที
	,requestNotAttdnCheckIn char(1) --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
	,requestLeaveAsLate char(1)--ขออนุมัติลาตามที่สายหรือไม่
	
	,isCheckout char(1) --สแกนออกหรือไม่
	,isEarly  char(1) --ออกก่อนหรือไม่
	,minuteEarly int --จำนวนนาทีที่ออกก่อน
	,requuestNotAttdnCheckOut char(1) --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
	,requestLeaveAsEarly char(1)--ลาตามที่สแกนออก่อนหรือไม่
	
	,isCheckBrake char(1) --สแกนพักหรือไม่
	,brakeDuration int--จำนวนนาทีที่ไปพัก
	,brakeEarlyOrLate char(1)--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
	,minuteBrakeEarlyOrLate int--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
	
	,minuteLeaveDuringBrake int --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
	,paymentOfLeaveDuringBrake char(1) --หักที่ลาระหว่างพักหรือไม่
	,checkTimeForLeaveDuringBrakeOK char(1) --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
	
	,stateOfCal varchar(8)
	,dummy varchar(500)
)
AS
BEGIN
	--table ของการสแกนนิ้วตามช่วงวันที่ระบุของแต่ละคน
	declare @allPersonTABetweenSpecfiedDate table (USERID int,CHECKTIME datetime,SENSORID nvarchar(5))
	
	-- table ของวันตามที่ต้องการ ตั้งแต่ startDate ถึง endDate
	declare @daysForCheckTa as table (dt datetime,dow varchar(20) collate Thai_CI_AS);

	--สร้าง temporary ของการสแกนนิ้วเพื่อเอาไปใช้คำนวณในฟังก์ชั่น ของ userid นั้นๆ แบบ type (temp_ta_empTaDetails เป็น type)
	declare @temp_ta_empTaDetails as temp_ta_empTaDetails

	declare @startDate datetime, @endDate datetime,@IDNo varchar(20),@personInfos varchar(max),@cDate datetime, @cdow varchar(20),@msg varchar(max);
	declare @empHW char(1)
	,@assignedHwToEmp char(1)
	,@checkInOutExists char(1)
	,@noNeedAttdnShift char(1)
	,@assignedShiftToEmp char(1)
	,@empAllDayLeaveRequest char(1)
	,@empReqNoAllDayAttnd char(1)
	,@empHSubstitution char(1)
	,@completedAttndInDay char(1)
	,@noDeductionLeave char(1)

	,@isCheckin char(1) --สแกนเข้าไหม
	,@isLate char(1) --สายไหม
	,@minuteLate int --สายกี่นาที
	,@requestNotAttdnCheckIn char(1) --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
	,@requestLeaveAsLate char(1) --ขออนุมัติลาตามที่สายหรือไม่

	,@isCheckout char(1) --สแกนออกหรือไม่
	,@isEarly  char(1) --ออกก่อนหรือไม่
	,@minuteEarly int --จำนวนนาทีที่ออกก่อน
	,@requuestNotAttdnCheckOut char(1) --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
	,@requestLeaveAsEarly char(1)--ลาตามที่สแกนออกก่อนหรือไม่

	,@isCheckBrake char(1)
	,@brakeDuration int--จำนวนนาทีที่ไปพัก
	,@brakeEarlyOrLate char(1)--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
	,@minuteBrakeEarlyOrLate int--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท

	,@minuteLeaveDuringBrake int --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
	,@paymentOfLeaveDuringBrake char(1) --หักที่ลาระหว่างพักหรือไม่
	,@checkTimeForLeaveDuringBrakeOK char(1) --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก

	,@stateOfCal varchar(8)
	,@dummy varchar(500)
	,@CurrentdescOutside int
	,@CurrentdescInside int
	,@loopCount int
	;
	
	
	--กำหนดวันเริ่มต้น/สิ้นสุดเพื่อดึงข้อมูลมาจากระบบลงเวลา 
	set @startDate = dateadd(day,-10,convert(varchar,@year)+'/'+convert(varchar,@month)+'/01'); 
	set @endDate=dateadd(day,-1,dateadd(month,1,convert(varchar,@year)+'/'+convert(varchar,@month)+'/01')); 
	set @endDate= case when @endDate>GETDATE() then CONVERT(varchar,GETDATE(),111) else @endDate end

	insert into @allPersonTABetweenSpecfiedDate select USERID,CHECKTIME,SENSORID from ta_empTaDetails where CHECKTIME between @startDate and dateadd(day,1,@endDate)
	--and not (USERID=3570 and CHECKTIME between '2017/10/02 08:00' and '2017/10/02 20:00')
	
	--ทดสอบ การลาครึ่งวัน รวมกับ สลับวันหยุดครึ่งวัน ทำให้ไม่มีได้ลงเวลาทั้งวัน(ใช้จงเป็นนายแบบ)
	--delete from @allPersonTABetweenSpecfiedDate where USERID=3570 and CHECKTIME between '2017/10/02 00:00' and '2017/10/02 23:00'
	
	--ทดสอบ เข้าสายหรืออกก่อนแบบอนุโลม(ใช้จงเป็นนายแบบ)
	--update @allPersonTABetweenSpecfiedDate set CHECKTIME='2017-10-02 08:01:56.000' where USERID=3570 and CHECKTIME='2017-10-02 07:51:56.000'
	--update @allPersonTABetweenSpecfiedDate set CHECKTIME='2017-10-02 16:21:56.000' where USERID=3570 and CHECKTIME='2017-10-02 17:13:16.000'
	
	--loop ของแต่ละคน
	declare personCS cursor FAST_FORWARD for 
	select distinct IDNo,titleName+firstName+' '+lastName
	--+'/'+positionName+'/'+corpName 
	empInfo from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
	where 1=1
	and  (em_status in ('W','P')  or ((em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)))) --คนที่ลาออกไปแล้ว แต่วันลาออกมากกว่า วันแรกของรอบเงินเดือนที่คำนวณ
	and IDNo not in (select IDNo from hrm_humanoPersonAll where em_status in ('W','P') group by IDNo having count(*)>1)
	and IDNo in (select IDNo from @listOfPersonIDNo)
	and  period_no not in ('PPK16C800000052')
	--order by IDNo desc
		
	OPEN personCS
	fetch next from personCS into @IDNo,@personInfos
	while @@FETCH_STATUS = 0  
	begin
		
		
		--ดึง วันเริ่มต้นตามรอบการคำนวณเงินเดือนของแต่ละคน
		select 
		@startDate = dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)) 		
		,@endDate = dateadd(minute,59,dateadd(hour,23,dateadd(month,pe_endmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,case when pe_end_date>DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1)))) then DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1))))else pe_end_date end))))
		from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id 	where h.IDNo=@IDNo
		set @endDate= case when @endDate>GETDATE() then CONVERT(varchar,GETDATE(),111) else @endDate end --หากวันสุดท้ายมากกว่าวันปัจจุบัน ให้วันสุดท้ายเท่ากับวันปัจจุบัน

		--return;		
		delete from @temp_ta_empTaDetails; --เคลียร์ค่าทิ้งก่อน เพื่อใช้สำหรับ ดึงไปในการคำนวณคนใหม่
		insert into @temp_ta_empTaDetails select USERID,CHECKTIME,SENSORID 
		from @allPersonTABetweenSpecfiedDate 
		where USERID in (select USERID from @ta_userInfos where SSN=@IDNo) and CHECKTIME between @startDate and @endDate
		
		-- ดึงวันออกมาใส่ใน table @daysForCheckTa
		delete from @daysForCheckTa; insert into @daysForCheckTa select allDate.dt,allDate.dow from dbo.FN_dayList(@startDate,@endDate) allDate
		
		declare dayCS cursor FAST_FORWARD for select dt,dow from @daysForCheckTa order by dt 
		OPEN dayCS
		fetch next from dayCS into @cDate,@cdow
		--print @IDNo+','+ @personInfos;
		while @@FETCH_STATUS = 0  
		begin
			--print @cdow
			set @msg = convert(varchar,@cDate,111)+' '+@cdow+' ';
			
			--ดึงข้อมูลการ ว่า เป็นวันหยุด, มีการสแกนนิ้ว, มีใบลา, มีสลับวันหยุด ในวันนั้นหรือไม่
			select @empHW=empHW
					,@assignedHwToEmp=assignedHwToEmp
					,@checkInOutExists=checkInOutExists
					,@noNeedAttdnShift=noNeedAttdnShift
					,@assignedShiftToEmp=assignedShiftToEmp
					,@empAllDayLeaveRequest=empAllDayLeaveRequest
					,@empReqNoAllDayAttnd=empReqNoAllDayAttnd
					,@empHSubstitution=empHSubstitution 
					,@completedAttndInDay=completedAttndInDay
					,@noDeductionLeave=noDeductionLeave
					,@isCheckin = isCheckin--สแกนเข้าไหม
					,@isLate=isLate --สายไหม
					,@minuteLate=minuteLate  --สายกี่นาที
					,@requestNotAttdnCheckIn=requestNotAttdnCheckIn --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
					,@requestLeaveAsLate=requestLeaveAsLate--ขออนุมัติลาตามที่สายหรือไม่
					,@isCheckout=isCheckout --สแกนออกหรือไม่
					,@isEarly=isEarly --ออกก่อนหรือไม่
					,@minuteEarly=minuteEarly --จำนวนนาทีที่ออกก่อน
					,@requuestNotAttdnCheckOut=requuestNotAttdnCheckOut --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
					,@requestLeaveAsEarly=requestLeaveAsEarly--ลาตามที่สแกนออก่อนหรือไม่				
					,@isCheckBrake=isCheckBrake
					,@brakeDuration = brakeDuration--จำนวนนาทีที่ไปพัก
					,@brakeEarlyOrLate = brakeEarlyOrLate--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
					,@minuteBrakeEarlyOrLate =minuteBrakeEarlyOrLate--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
					,@minuteLeaveDuringBrake=minuteLeaveDuringBrake --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
					,@paymentOfLeaveDuringBrake=paymentOfLeaveDuringBrake --หักที่ลาระหว่างพักหรือไม่
					,@checkTimeForLeaveDuringBrakeOK=checkTimeForLeaveDuringBrakeOK --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
					,@stateOfCal=stateOfCal
					,@dummy=dummy
			from dbo.FN_001_EmpEachDayInfo(@cDate,@IDNo,@temp_ta_empTaDetails);		
			
			insert into @firstCalculation (
				IDNo,personInfo
				,empHW
				,assignedHwToEmp			
				,assignedShiftToEmp
				,noNeedAttdnShift	
				,checkInOutExists	
				,completedAttndInDay	
				,empAllDayLeaveRequest
				,empReqNoAllDayAttnd
				,empHSubstitution
				,noDeductionLeave
				,cDate
				,cdow
				,isCheckin--สแกนเข้าไหม
				,isLate --สายไหม
				,minuteLate  --สายกี่นาที
				,requestNotAttdnCheckIn --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
				,requestLeaveAsLate --ขออนุมัติลาตามที่สายหรือไม่
				,isCheckout --สแกนออกหรือไม่
				,isEarly  --ออกก่อนหรือไม่
				,minuteEarly --จำนวนนาทีที่ออกก่อน
				,requuestNotAttdnCheckOut --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
				,requestLeaveAsEarly--ลาตามที่สแกนออก่อนหรือไม่
				,isCheckBrake --สแกนพักหรือไม่
				,brakeDuration--จำนวนนาทีที่ไปพัก
				,brakeEarlyOrLate--ไปพักก่อนหรือกลับจากพักเลทหรือไม่
				,minuteBrakeEarlyOrLate--จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
				,minuteLeaveDuringBrake --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
				,paymentOfLeaveDuringBrake --หักที่ลาระหว่างพักหรือไม่
				,checkTimeForLeaveDuringBrakeOK --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
				,stateOfCal
				,dummy
			)values(
				@IDNo,@personInfos
				,@empHW
				,@assignedHwToEmp			
				,@assignedShiftToEmp
				,@noNeedAttdnShift	
				,@checkInOutExists	
				,@completedAttndInDay	
				,@empAllDayLeaveRequest
				,@empReqNoAllDayAttnd
				,@empHSubstitution
				,@noDeductionLeave
				,@cDate
				,@cdow
				,@isCheckin--สแกนเข้าไหม
				,@isLate --สายไหม
				,@minuteLate  --สายกี่นาที
				,@requestNotAttdnCheckIn --ขออนุมัติไม่สแกนนิ้วเข้าหรือไม่
				,@requestLeaveAsLate --ขออนุมัติลาตามที่สายหรือไม่
				,@isCheckout --สแกนออกหรือไม่
				,@isEarly --ออกก่อนหรือไม่
				,@minuteEarly --จำนวนนาทีที่ออกก่อน
				,@requuestNotAttdnCheckOut --หากไม่ได้สแกนออกขออนุมัติแล้วหรือไม่
				,@requestLeaveAsEarly--ลาตามที่สแกนออก่อนหรือไม่
				,@isCheckBrake
				,@brakeDuration --จำนวนนาทีที่ไปพัก
				,@brakeEarlyOrLate --ไปพักก่อนหรือกลับจากพักเลทหรือไม่
				,@minuteBrakeEarlyOrLate --จำนวนาทีที่ไปพักก่อนรวมกับจำนวนนาทีที่กลับจากพักเลท
				,@minuteLeaveDuringBrake --จำนวนนาทีที่ลาคร่อมหรือติดกับเวลาพัก
				,@paymentOfLeaveDuringBrake --หักที่ลาระหว่างพักหรือไม่
				,@checkTimeForLeaveDuringBrakeOK --สแกนนิ้วครบถ้วนหรือไม่หากลาคร่อมการพัก
				,@stateOfCal
				,@dummy
			)
			fetch next from dayCS into @cDate,@cdow
			set @loopCount = @loopCount+1;	if @loopCount > 20 return
		end --while
		close dayCS;  
		deallocate dayCS; 
		fetch next from personCS into @IDNo,@personInfos
	end--while แต่ละคน
	close personCS;  
	deallocate personCS; 
	--จบ loop ของแต่ละคน		
	RETURN 
END
