declare @leaveFromDateTime datetime, @leaveToDateTime datetime, @IDNo varchar(13), @result varchar(3);
declare @startDayOfTheYear datetime, @endDayOfTheYear datetime;

declare @leaveDetailsOfTheYear table (leaveId int,IDNo varchar(13),leaveTypeId int,fromToDateTime varchar(150),ddrt decimal(18,6),formated varchar(2),leaveGroup varchar(50),comments varchar(250),[status]  int,leaveDate datetime,shiftId int,leaveDay int,leaveHour int,leaveMinute int,leaveMinutes  int)

select @leaveFromDateTime='2018/12/21 08:00', @leaveToDateTime = '2018/12/22 17:00', @IDNo='3940200536679';

/*���ѹ������� ��� �ѹ����ش�ͧ�� �ͧ�ͺ�Թ��͹�ͧ�������͹*/
select @startDayOfTheYear = peDate.st, @endDayOfTheYear =peDate.ed
from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
cross apply (
	select
		dateadd(month,pe_startmonth,CONVERT(varchar,YEAR(@leaveFromDateTime))+'/'+CONVERT(varchar,1)+'/'+CONVERT(varchar,pe_start_date)) st --Ǵ�.������鹻�
		,dateadd(
					minute,59
					,dateadd(hour,23,dateadd(
							month,pe_endmonth,CONVERT(varchar,YEAR(@leaveFromDateTime))+'/'
							+CONVERT(varchar,12)+'/'+CONVERT(varchar
							,case when pe_end_date>DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,YEAR(@leaveFromDateTime))+'/'+CONVERT(varchar,12)+'/'+CONVERT(varchar,1)))) 
								then DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,YEAR(@leaveFromDateTime))+'/'+CONVERT(varchar,12)+'/'+CONVERT(varchar,1))))else pe_end_date 
							end
						)
					)
				)
		) ed --Ǵ�.����ش��
	)peDate
where h.IDNo=@IDNo and h.em_status in ('W','P','RR') 

/* �����š���� */
insert into @leaveDetailsOfTheYear (leaveId,IDNo,leaveTypeId,fromToDateTime,ddrt,formated,leaveGroup,comments,[status],leaveDate,shiftId,leaveDay,leaveHour,leaveMinute,leaveMinutes)
select
el.id leaveId,el.IDNo,el.leaveId leaveTypeId
,convert(varchar(max),el.startDateTime,111)+' '+substring(convert(varchar(max),el.startDateTime,108),1,5)+ ' to '+convert(varchar(max),el.endDateTime,111)+' '+substring(convert(varchar(max),el.endDateTime,108),1,5) fromToDateTime
,case
	when elc.formated='A' and convert(varchar(max),sdl.leaveDate,112) between CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),el.endDateTime,112)  /*and elc.leaveMinutes>=480*/ then 1.00
	when elc.formated in ('M','E') and elc.leaveMinutes=240 then 0.50
	when elc.formated = 'O' and elc.leaveMinutes <= 480 then (elc.leaveMinutes/480.00)
	when elc.formated in ('O','S') and elc.leaveMinutes > 480 and
			(
					/* ��� �ѹ�Ѩ�غѹ �ҡ�����ѹ������� ��й��¡��� �ѹ����ش����� */
					(CONVERT(varchar(max),sdl.leaveDate,112) > CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) < CONVERT(varchar(max),el.endDateTime,112))
					or
					/* �ѹ��������� ���¡����ѹ�Ѩ�غѹ ��� �ѹ����ش�������ҡѺ�ѹ�Ѩ�غѹ ��� ��������ش�������ҡѺ��������ش��*/
					(
					CONVERT(varchar(max),sdl.leaveDate,112) > CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.endDateTime,112)
					and CAST(sdl.leaveDate AS datetime)+sv.endTime=el.endDateTime
					)
					or
					/* �ѹ��������� = �ѹ�Ѩ�غѹ ��� �ѹ����ش����ҹ��¡����ѹ�Ѩ�غѹ ��� ���������������ҡѺ������鹡�*/
					(
					CONVERT(varchar(max),sdl.leaveDate,112) = CONVERT(varchar(max),el.startDateTime,112) and CONVERT(varchar(max),sdl.leaveDate,112) < CONVERT(varchar(max),el.endDateTime,112)
					and CAST(sdl.leaveDate AS datetime)+sv.startTime=el.startDateTime
					)

			) then 1.00
		when elc.formated in ('O','S') and elc.leaveMinutes > 480 and
			(

					/* �ѹ��������� = �ѹ�Ѩ�غѹ ��� �ѹ����ش�����=�ѹ�Ѩ�غѹ ��� �ӹǹ�ҷշ���� ���¡��� 480*/
					(
					DATEDIFF(minute
						,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime)
						,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime)
						) < 480
					)

			) then case when DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime)) >= 300
								then (DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime))
									-60
									) / 480.00 --������Թ�����ѹ���ź�͡���¨ӹǹҷշ�������ҧ�ѡ 60 �ҷ�( �Ҩ�е�ͧ���������� �óշ�����ҷ��������� ���������ҧ�ѡ���§ �����Ҩ�������� )
								else (DATEDIFF(minute,cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.startDateTime,108) AS datetime),cast(convert(varchar,sdl.leaveDate,111)+' '+convert(varchar,el.endDateTime,108) AS datetime))-0) / 480.00--��������֧�����ѹ
						end

	else 0.00
end ddrt
,el.formated
,case when ltype.[group]=1 then '�ҡԨ' when ltype.[group]=3 then '�Ҿѡ��͹' when ltype.[group]=2 then '�һ���' else 'n/a' end leaveGroup
,el.comments
,el.[status]
,cast(sdl.leaveDate as date)leaveDate,sdl.shiftId
,elc.leaveDay,elc.leaveHour,elc.leaveMinute,elc.leaveMinutes
--into #tempPreReportTable
from (ta_employeeLeaves el inner join ta_shiftDuringLeave sdl on el.id=sdl.leaveId  inner join ta_shiftsView sv on  sdl.shiftId=sv.id inner join hrm_humanoPersonAll h on el.IDNo=h.IDNo)
left join ta_leaveTypes ltype on el.leaveId=ltype.id
left join ta_employeeLeavesCalTime elc on el.id=elc.id
where 1=1	
and (el.approveResult='Y' or el.approvedDate is null and el.approveResult='N')
and el.[status]=1
and cast(sdl.leaveDate as date) between @startDayOfTheYear and @endDayOfTheYear
and h.em_status in ('W','P')
and (h.IDNo =@IDNo)

select * from @leaveDetailsOfTheYear
--select SUM(ddrt)sumDayLeave,leaveTypeId,comments  from @leaveDetailsOfTheYear group by leaveTypeId,comments