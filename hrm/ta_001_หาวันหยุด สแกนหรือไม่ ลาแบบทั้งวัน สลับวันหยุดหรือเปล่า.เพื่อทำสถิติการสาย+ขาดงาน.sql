/**
�ش���ʧ�� : �����Ǻ��������š�� �Ҵ ��� ��� �� ��Ш���͹ ����ҹ�� ��ŧ㹵��ҧʶԵԡ�âҴ����� (ta_lateAbsentStats)
schedule : �ء�ѹ��� 8-11 �ͧ��͹ 㹵͹��� ���� 19.00�. ����ͧ�ѹ��ѹ��� 8 -11 �ͧ��͹ �� ���ͻ�ͧ�ѹ���������ѹ��ѹ���������Դ server �ҡ�ѹ��� 8-11 �ç�Ѻ�ѹ��ش �����������ͺ�����ء� ��Դ��è��� ��ѡ�ҹ�������Ф����
*/

use EMPBase;
set nocount on;
IF OBJECT_ID('tempdb..#firstCalculation2') IS NOT NULL DROP TABLE #firstCalculation2
--��͹��лշ��Фӹǳ
declare @year int, @month int, @msg varchar(max),@listOfPersonIDNo listOfPersonIDNo,@ta_userInfos temp_ta_userInfosV2
declare @lastMonthSameDate  datetime;
set @lastMonthSameDate = DATEADD(month,-1, getdate())
--set @lastMonthSameDate = '2017/09/11'
set @year=YEAR(@lastMonthSameDate); set @month=MONTH(@lastMonthSameDate);

if DAY(getdate()) between 7 and 11
begin
	--���͡�����ӹǳ���� @listOfPersonIDNo ��͹
	insert into @listOfPersonIDNo
	select IDNo
	from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
	where 1=1
	and  (em_status in ('W','P') 
												 or (
														(em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date))
													)
											) --��������͡����� ���ѹ���͡�ҡ���� �ѹ�á�ͧ�ͺ�Թ��͹���ӹǳ
	and IDNo not in (select IDNo from hrm_humanoPersonAll where em_status in ('W','P') group by IDNo having count(*)>1)
	--and IDNo='3940200536679'
	
	/*ź��������ҷ���Ҩ�Фӹǳ�������ͺ��͹��͹*/
	declare @IDNo varchar(13),@startDateOfPeriod datetime, @endDateOfPeriod datetime;
	
	declare personCS cursor FAST_FORWARD for 
	select distinct IDNo from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
	where 1=1
	and  (em_status in ('W','P')  or ((em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)))) --��������͡����� ���ѹ���͡�ҡ���� �ѹ�á�ͧ�ͺ�Թ��͹���ӹǳ
	and IDNo not in (select IDNo from hrm_humanoPersonAll where em_status in ('W','P') group by IDNo having count(*)>1)
	and IDNo in (select IDNo from @listOfPersonIDNo)
	and  period_no not in ('PPK16C800000052')
		
	OPEN personCS
	fetch next from personCS into @IDNo
	while @@FETCH_STATUS = 0  
	begin		
		
		--�֧ �ѹ������鹵���ͺ��äӹǳ�Թ��͹�ͧ���Ф�
		select 
		@startDateOfPeriod = dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date)) 		
		,@endDateOfPeriod = dateadd(minute,59,dateadd(hour,23,dateadd(month,pe_endmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,case when pe_end_date>DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1)))) then DATEPART(day,dateadd(day,-1,dateadd(month,1,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,1))))else pe_end_date end))))
		from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id 	where h.IDNo=@IDNo
		set @endDateOfPeriod= case when @endDateOfPeriod>GETDATE() then CONVERT(varchar,GETDATE(),111) else @endDateOfPeriod end --�ҡ�ѹ�ش�����ҡ�����ѹ�Ѩ�غѹ ����ѹ�ش������ҡѺ�ѹ�Ѩ�غѹ
		
		delete from ta_lateAbsentStats where IDNo=@IDNo and cDate between @startDateOfPeriod and @endDateOfPeriod	
		
		fetch next from personCS into @IDNo
	end
	close personCS
	deallocate personCS
	/*�� ź��������ҷ���Ҩ�Фӹǳ�������ͺ��͹��͹*/
	
	--��� userid �ҡ�к��᡹���������� @ta_userInfos ��͹
	insert into @ta_userInfos select SSN,USERID from ta_userInfos where SSN in (select IDNo from @listOfPersonIDNo)
	
	select mm.IDNo,h.titleName+h.firstName+' '+h.lastName personInfo
			,CONVERT(varchar,cDate,111) cDate
			,cdow dow
			,replace(replace(replace(tsc.msg+case when empHW='Y' then '{{�ѹ��ش}}' else '' end,'}}{{',','),'{{',''),'}}','')	msg
			,tsc.totalDeduction		
			,stateOfCal soc--ʶҹС�äӹǳ
			,brakeDuration bdr --�ӹǹ�ҷշ��仾ѡ
			,isCheckin ici--�᡹������
			,isLate il --������
			,minuteLate mnl --��¡��ҷ�
			,requestNotAttdnCheckIn rnaci--��͹��ѵ�����᡹����������
			,requestLeaveAsLate rlal--��͹��ѵ��ҵ���������������
			,isCheckout ico--�᡹�͡�������
			,isEarly  iel--�͡��͹�������
			,minuteEarly mel --�ӹǹ�ҷշ���͡��͹
			,requuestNotAttdnCheckOut rnaco --�ҡ������᡹�͡��͹��ѵ������������
			,requestLeaveAsEarly rlae--�ҵ������᡹�͡�͹�������
			,isCheckBrake icb --ŧ���Ҿѡ���§�ú����������͡
			
			,brakeEarlyOrLate beo --仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
			,minuteBrakeEarlyOrLate mbeo--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
			,assignedHwToEmp ahwte --��˹��ѹ��ش�������
			,empHW hw  --�ѹ��ش�������
			,assignedShiftToEmp ashte --��˹��Чҹ��������
			
			,noNeedAttdnShift nnash --�繡Чҹ�������ͧŧ����
			,checkInOutExists cioe --�ա��ŧ������ѹ���
			,completedAttndInDay	 catdid --ŧ������ѹ��鹤ú��� ��� �ѡ ����͡
			,empAllDayLeaveRequest eadlr --���ҧҹ����ѹ�������
			,empReqNoAllDayAttnd ernadatd --�����ŧ���ҷ���ѹ�������
			,empHSubstitution ehs --����Ѻ�ѹ��ش�������
			,noDeductionLeave ndl --�繡����Ẻ����ѡ
			,minuteLeaveDuringBrake mldb --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
			,paymentOfLeaveDuringBrake poldb --�ѡ����������ҧ�ѡ�������(�ѧ������)
			,checkTimeForLeaveDuringBrakeOK ctfldbo--�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ(�ѧ������)
			,h.workEnd
			,h.workStart
			,tsc.[absent],tsc.[leave],tsc.[brake],tsc.[lateAndEarly],tsc.errandLeave,tsc.sickLeave,tsc.notCheckInOrOut
			,mm.dummy
			--,mm.dummy mmDummy
			into #firstCalculation2
	from dbo.FN_calculateMonthlyTA(@year,@month,@listOfPersonIDNo,@ta_userInfos) mm left join  hrm_humanoPersonAll h on mm.IDNO=h.IDNo collate Thai_CI_AS 
			inner join hrm_paymentPackage pp on h.period_no=pp.id
			and(
					 h.em_status in ('W','P') 
					 or
					(h.em_status not in ('W','P')  and (em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date))   ) 
				)
	cross apply FN_001_translateStateOfCal(mm.IDNo,stateOfCal,cDate,minuteLate,minuteEarly,minuteLeaveDuringBrake,minuteBrakeEarlyOrLate,paymentOfLeaveDuringBrake,h.workStart,h.workEnd,isCheckin) tsc

	insert into ta_lateAbsentStats (IDNo,personInfo,cDate,dow,msg,ttd,[absent],leave,brake,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut,soc,bdr,ici,il,mnl,rnaci,rlal,ico,iel,mel,rnaco,rlae,icb,beo,mbeo,ahwte,hw,ashte,nnash,cioe,catdid,eadlr,ernadatd,ehs,ndl,mldb,poldb,ctfldbo,workStart,workEnd,dummy)
	select 
	fc.IDNo,fc.personInfo,fc.cDate,fc.dow,fc.msg,fc.totalDeduction ttd
	,isnull(fc.[absent],0.00)[absent]
	,isnull(fc.[leave],0.00)leave
	,isnull(fc.[brake],0.00)brake
	,isnull(fc.[lateAndEarly],0.00)lateAndEarly
	,isnull(fc.errandLeave,0.00)errandLeave
	,isnull(fc.sickLeave,0.00)sickLeave
	,isnull(fc.notCheckInOrOut,0.00) notCheckInOrOut
	,fc.soc,fc.bdr,fc.ici,fc.il,fc.mnl,fc.rnaci,fc.rlal,fc.ico,fc.iel,fc.mel,fc.rnaco,fc.rlae,fc.icb,fc.beo,fc.mbeo,fc.ahwte,fc.hw,fc.ashte,fc.nnash,fc.cioe,fc.catdid,fc.eadlr,fc.ernadatd,fc.ehs,fc.ndl,fc.mldb,fc.poldb,fc.ctfldbo,fc.workStart,fc.workEnd,fc.dummy
	from #firstCalculation2 fc left join ta_lateAbsentStats la on fc.IDNo=la.IDNo and fc.cDate=la.cDate 
	where 1=1 
	and ((fc.msg like '%���%' and fc.msg not like '%��������%') or fc.msg like '%�Ҵ�ҹ����ѹ%' or fc.msg like '%���ŧ�������%' or fc.msg like '%���ŧ�����͡%')
	and la.cDate is null

end --if DAY(getdate()) between 1 and 4 
