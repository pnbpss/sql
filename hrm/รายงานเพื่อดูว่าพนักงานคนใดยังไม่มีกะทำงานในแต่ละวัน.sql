/* ��§ҹ��ѡ�ҹ����ѧ������˹��Чҹ��� ���͡�˹�����ѧ���ú �����ҧ�ѹ��� @startDate �֧ @endDate */
USE [EMPBase]
declare @startDate datetime, @endDate datetime;
IF OBJECT_ID('tempdb..##mainShiftInfo') IS NOT NULL DROP TABLE ##mainShiftInfo
IF OBJECT_ID('tempdb..##finalResults') IS NOT NULL DROP TABLE ##finalResults
create table ##mainShiftInfo (IDNo varchar(20),dt varchar(12),assigned char(1))

set @startDate='2017/08/22'; set @endDate='2017/09/21' --�ѹ����ͧ��ô� �ҡ->�֧

insert into ##mainShiftInfo
select 
p.IDNo--,p.employeeCode,p.titleName,p.firstName,p.lastName,p.positionName,p.officeName,p.corpName
,'D'+replace(substring(convert(varchar,p.dt,111),3,8),'/','') dt
--,p.dow,sh.shiftName
,case when isnull(sh.shiftName,'')='' then 'N' else 'Y' end assigned
from 
	(
		select * from hrm_humanoPersonAll p,dbo.FN_dayList(@startDate,@endDate) 
		where p.em_status in ('W','P') 
		--and p.employeeCode in (select employeeCode from hrm_commanders where cEmpCode = (select employeeCode from hrm_humanoPersonAll where IDNo='3940200536679' and em_status in ('W','P'))) --�١��ͧ�ͧ�ѹ
		and p.employeeCode in (select employeeCode from hrm_commanders where cEmpCode = (select employeeCode from hrm_humanoPersonAll where firstName like '%����%' and lastName like '%�ӻ�%' and em_status in ('W','P'))) --�١��ͧ�ͧ����
		--and p.employeeCode in (select employeeCode from hrm_commanders where cEmpCode = (select employeeCode from hrm_humanoPersonAll where firstName like '%�����ѡɳ�%' and lastName like '%��ǹ�%' and em_status in ('W','P'))) --�١��ͧ�͹
		--and p.positionName like '%�����%' or p.positionName like '%�Ѿ%'
		--and p.IDNo='3940200536679'
		
	) 
	p left join (
		select a.*,b.IDNo,sv.shiftName from dbo.FN_dayList(@startDate,@endDate) a inner join ta_shiftForEmployees b on a.dt between b.activeFrom and b.activeTo and b.active='Y' inner join ta_shiftsView sv on b.shiftId=sv.id
	)  sh on p.IDNo=sh.IDNo and p.dt=sh.dt

declare @column nvarchar(max),@sqlStr nvarchar(max),@columnCheck nvarchar(max);

set @column = (select STUFF((SELECT distinct ',[' +replace(dt,'/','_')+']' 
				FROM ##mainShiftInfo ST1 
				order by ',[' +replace(dt,'/','_')+']' 
				FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'' ) [str])

set @columnCheck = '('+REPLACE(@column,',','+')+')'
print @column+'###'+@columnCheck

set @sqlStr = 'select IDNo,case when CHARINDEX(''N'','+@columnCheck+')<>0 then ''Not Ok'' else ''OK''  end wholeRange,'+@column+' into ##finalResults from(select IDNo,dt,assigned from ##mainShiftInfo) t pivot (max(assigned) for dt in ('+@column+')) p';
execute(@sqlStr);

select p.lastName+', '+p.firstName+(case when isnull(p.nick,'')='' or p.nick='' then '' else '('+p.nick+')' end) empName,p.positionName+'/'+p.officeName+'/'+p.corpName empInfo, fr.* from ##finalResults fr left join hrm_humanoPersonAll p on fr.IDNo=p.IDNo collate Thai_CI_AS and p.em_status in ('W','P')