USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_01_completedDailyAttdnCheck]    Script Date: 10/23/2017 14:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_01_completedDailyAttdnCheck]
(
	-- Add the parameters for the function here
	@cDate datetime, @IDNo varchar(20),@temp_ta_empTaDetails temp_ta_empTaDetails readonly
	,@shiftMusTA char(1)
	,@shiftName varchar(max)
	,@shiftStartTime time
	,@shiftHaveToCheckEnd char(1)
	,@shiftEndTime time
	,@shiftMinCheckInTime time
	,@shiftMaxCheckOutTime time
	,@shiftBrakeCheck char(1)
	,@shiftBreakStart time
	,@shiftBreakEnd time
	,@shiftMinuteBrake int
	,@compromiseLate int
	,@compromiseEarly int
	
)
RETURNS 
@dailyAttdnComplete TABLE 
(
	checkIn char(1)
	,isLate char(1)
	,minuteLate int
	,isCheckBrake char(1)
	,checkOut char(1)
	,isEarly char(1)	
	,minuteEarly int
	,brakeDuration int
	,brakeEarlyOrLate char(1)
	,minuteBrakeEarlyOrLate int
)
AS
BEGIN
		declare @tempTaTable  table (employeeCode varchar(20), checkTime datetime)
		--insert into @tempTaTable select SSN,et.CHECKTIME from @temp_ta_empTaDetails et inner join ta_userInfos ut on et.USERID=ut.USERID where ut.SSN = @IDNo
		insert into @tempTaTable select @IDNo,et.CHECKTIME from @temp_ta_empTaDetails et 
		where CHECKTIME between 		
			--@cDate 
			dateadd(minute,(-1)*DATEPART(minute,@shiftMinCheckInTime),dateadd(hour,-1*datepart(hour,@shiftMinCheckInTime),@cDate+@shiftStartTime))
			and 
			--dateadd(minute,59,DATEADD(HOUR,23,@cDate))
			dateadd(minute,DATEPART(minute,@shiftMaxCheckOutTime),dateadd(hour,datepart(hour,@shiftMaxCheckOutTime),@cDate+@shiftStartTime))
		

		--select * from @tempTaTable order by checkTime desc
		insert into @dailyAttdnComplete
		select 
		case 
			when ISNULL(convert(varchar,@shiftStartTime),'')='' then 'Y'
			else 
			case
				when exists(
							select 1 from @tempTaTable where checkTime between --นับตั้งแต่เวลาเริ่มนับสแกน						
							dateadd(minute,(-1)*DATEPART(minute,@shiftMinCheckInTime),dateadd(hour,-1*datepart(hour,@shiftMinCheckInTime),@cDate+@shiftStartTime))
							and (
									case 
										when @shiftBrakeCheck='Y' then --ถ้าต้องสแกนพัก นับถึงเวลาพักว่ามีสแกนหรือไม่
											dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))
											--dateadd(hour,4,@cDate+@shiftStartTime)
										else --ถ้าไม่ต้องสแกนพัก นับไปสี่ชม.จากเวลาเริ่มงานว่าสแกนหรือไม่
											dateadd(hour,4,@cDate+@shiftStartTime)
									end
								)
							) 
				then 'Y'
				else 'N'
			end
		end checkIn		
		,--'Y' 
		aggr.isLate
		--0 		
		,case when aggr.isLate='Y' then (datediff(minute
				,
				@cDate+@shiftStartTime
				,
				(select min(checkTime) from @tempTaTable where checkTime 
							between 
							@cDate+@shiftStartTime
							and 
							(case 
										when @shiftBrakeCheck='Y' then --ถ้าต้องสแกนพัก นับถึงเวลาพักว่ามีสแกนหรือไม่
											dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))
										else --ถ้าไม่ต้องสแกนพัก นับไปสี่ชม.จากเวลาเริ่มงานว่าสแกนหรือไม่
											dateadd(hour,4,@cDate+@shiftStartTime)
									end)
				)
						
		)) - @compromiseLate
		else 0
		end 
		minuteLate
		
		,aggr.isCheckBrake
		
		,case when @shiftHaveToCheckEnd='Y' then
			case 
				when ISNULL(convert(varchar,@shiftEndTime),'')='' then 'Y'
					else
					case 
						when exists				
						(select 1 from @tempTaTable where checkTime between
								(
									case when @shiftBrakeCheck='Y' then --ถ้าต้องสแกนพักนับตั้งแต่เวลาพักไปจนถึงเวลานับสแกนออกมากสุด
											dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))				 
										 else --ถ้าไม่ต้องสแกนพักนับตั้งแต่ชม.ที่ 5จากเริ่มงาน ไปจนถึงเวลานับสแกนออกมากสุด
											dateadd(hour,5,@cDate+@shiftStartTime)
									end						
								)
							 and 
							 dateadd(minute,DATEPART(minute,@shiftMaxCheckOutTime),dateadd(hour,datepart(hour,@shiftMaxCheckOutTime),@cDate+@shiftStartTime))				 
						 ) then 'Y'
						else 'N'
					end
			end 
		else 'Y'
		end
		checkOut		
		,aggr.isEarly
		,case when aggr.isEarly='Y' then
			(datediff(minute
					,(select max(checktime) from @tempTaTable where checktime between
						dateadd(hour,5,@cDate+@shiftStartTime)
						and
						dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
					 )					
					,dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
					))-@compromiseEarly
			else 0
			end minuteEarly
			
		,case when (
				select count(checkTime) from @tempTaTable where checkTime between 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakStart),dateadd(hour,1*datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
							and 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakEnd),dateadd(hour,1*datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
				)>1 then
				DATEDIFF(minute,
				(select min(checkTime) from @tempTaTable where checkTime 
							between 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakStart),dateadd(hour,1*datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
							and 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakEnd),dateadd(hour,1*datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
				)
				,(select max(checkTime) from @tempTaTable where checkTime 
							between 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakStart),dateadd(hour,1*datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
							and 
							dateadd(minute,(1)*DATEPART(minute,@shiftBreakEnd),dateadd(hour,1*datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
				 )
			)
		else -1 		
		end brakeDuration
		
		,brakeEarlyOrLate.result brakeEarlyOrLate
		,minuteBrakeEarlyOrLate.result minuteBrakeEarlyOrLate
		from (
			select
					case 
						when @shiftBrakeCheck='Y' then --ถ้าต้องสแกนพัก
							case when 
											( --ถ้าไม่มีการสแกนระหว่างเริ่มให้สแกนเข้าจนถึงเวลาเข้างาน
												select count(*) from @tempTaTable where checktime between
													dateadd(minute,(-1)*DATEPART(minute,@shiftMinCheckInTime),dateadd(hour,-1*datepart(hour,@shiftMinCheckInTime),@cDate+@shiftStartTime))
													and 
													@cDate+@shiftStartTime
											)=0 
											and --และมีการสแกนตั้งแต่เข้างานนับไปอีกสี่ชม.
											( 
												select count(*) from @tempTaTable where checktime between
													@cDate+@shiftStartTime													
													and 
													--dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime))													
													dateadd(hour,4,@cDate+@shiftStartTime)
											)>0
								then 'Y'
								else 'N'
							end
						else --ถ้าไม่ต้องสแกนพัก
							case when 
											( --ถ้าไม่มีการสแกนระหว่างเริ่มให้สแกนเข้าจนถึงเวลาเข้างาน
												select count(*) from @tempTaTable where checktime between
													dateadd(minute,(-1)*DATEPART(minute,@shiftMinCheckInTime),dateadd(hour,-1*datepart(hour,@shiftMinCheckInTime),@cDate+@shiftStartTime))
													and 
													@cDate+@shiftStartTime
											)=0 
											and --และมีการสแกนตั้งแต่เข้างานจนถึงชมที่4นับจากเวลาเข้างาน
											( 
												select count(*) from @tempTaTable where checktime between
													@cDate+@shiftStartTime													
													and 
													dateadd(hour,4,@cDate+@shiftStartTime)
											)>0
								then 'Y'
								else 'N'
							end
					end
			isLate
			,
			case when @shiftHaveToCheckEnd='Y' then
					case when 
							( --ถ้าไม่มีสแกนระหว่างเลิกงานจนถึงเวลานับมากสุด
								select count(*) from @tempTaTable where checktime between						
									dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
									and 
									dateadd(minute,DATEPART(minute,@shiftmaxCheckOutTime),dateadd(hour,datepart(hour,@shiftmaxCheckOutTime),@cDate+@shiftStartTime))
							)=0 
							and 
							( --และมีสแกนระหว่างครึ่งวันไปจนถึงเลิกงาน
									select count(*) from @tempTaTable where checktime between						
										(
											case when @shiftBrakeCheck='Y' then --ถ้าต้องสแกนพักนับตั้งแต่เวลาสิ้นสุดการพักจนถึงเลิกงาน
												dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
											else --ถ้าต้องไม่ต้องสแกนพักนับตั้งแต่ชม.ที่5หลังเข้างานจนถึงเลิกงาน
												dateadd(hour,5,@cDate+@shiftStartTime)
											end
										)
									and 
									dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
									
							)>0
							then 'Y'
							else 'N'
						end
				else 'N'
			end	
		isEarly
		
		,case 
			when @shiftBrakeCheck='N' then 'Y'
			else 
			case
				when (
							select COUNT(*) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก)
							between 
							dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
							and 
							dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
						 )>1 --ต้องสแกนมากกว่า 1 ครั้งระหว่าง เวลาพัก
						and
							datediff
							(minute,
								(
									select min(CHECKTIME) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก)
									between 
									dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
									and 
									dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
								),
								(
									select max(CHECKTIME) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก)
									between 
									dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
									and 
									dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
								)
							)>=15 --ระยะห่างของการสแกนระหว่างเวลาพัก(max-min๗ ต้อง >= 15 นาที
					then 'Y'
				else 'N'
			end
		end isCheckBrake
		)aggr
		cross apply (
			select
			case when isCheckBrake='N'
				then case when 
									 (
										select COUNT(*) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก) อยู่ในช่วงระหว่า เวลาพักเริ่มลบ30นาที ถึง เวลาสิ้นสุดพัก+30 นาที
										between 
										dateadd(minute,DATEPART(minute,@shiftBreakStart)-30,dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
										and 
										dateadd(minute,DATEPART(minute,@shiftBreakEnd)+30,dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
									 )>1 
									and
									datediff
									(minute,
										(
											select min(CHECKTIME) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก)
											between 
											dateadd(minute,DATEPART(minute,@shiftBreakStart)-30,dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
											and 
											dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
										),
										(
											select max(CHECKTIME) from @tempTaTable where checkTime --ถ้ามีการสแกนระหว่างเวลาพักมากกว่าสองครั้ง (เข้าและออก)
											between 
											dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
											and 
											dateadd(minute,DATEPART(minute,@shiftBreakEnd)+30,dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime))
										)
									)>=15 --ระยะห่างของการสแกนระหว่างเวลาพัก(max-min๗ ต้อง >= 15 นาที									
									
									 then 
									'Y'
								else 'N'
						end
				else 'N'
			end result		
		) brakeEarlyOrLate
		cross apply(
			select
			case
				when brakeEarlyOrLate.result='Y'
					then 						
						isnull(datediff(minute,
							(
								select max(CHECKTIME) from @tempTaTable where CHECKTIME between 
								dateadd(minute,DATEPART(minute,@shiftBreakStart)-30,dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
								and 
								dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 
							)
							,dateadd(minute,DATEPART(minute,@shiftBreakStart),dateadd(hour,datepart(hour,@shiftBreakStart),@cDate+@shiftStartTime)) 						
						),0)						
						 --นับเวลาที่สแกนไปพักก่อนภายใน 30 นาทีก่อนเวลาพัก โดยเอาเวลาที่มากสุด
						+ 						
						isnull(datediff(minute
						,dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 						
							,(
								select min(CHECKTIME) from @tempTaTable where CHECKTIME between 
								dateadd(minute,DATEPART(minute,@shiftBreakEnd),dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
								and 
								dateadd(minute,DATEPART(minute,@shiftBreakEnd)+30,dateadd(hour,datepart(hour,@shiftBreakEnd),@cDate+@shiftStartTime)) 
							)
						),0) --นับเวลาที่สแกนกลับจากพักภายใน 30 นาทีหลังเวลาพัก โดยเอาเวลาที่น้อยสุด
						
					else 0
			end result
		) minuteBrakeEarlyOrLate
	RETURN 
END
