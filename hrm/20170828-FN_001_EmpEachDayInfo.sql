USE [EMPBase]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_001_EmpEachDayInfo]    Script Date: 08/27/2017 07:54:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[FN_001_EmpEachDayInfo] 
(
	-- Add the parameters for the function here
	@cDate datetime, @IDNo varchar(20),@temp_ta_empTaDetails temp_ta_empTaDetails readonly
)
RETURNS 
@EmpEachDayInfo TABLE 
(
	-- Add the column definitions for the TABLE variable here
	dummy varchar(500)
	,empHW char(1)
	,assignedHwToEmp char(1)
	,checkInOutExists char(1)
	,noNeedAttdnShift char(1)
	,assignedShiftToEmp char(1)
	,empAllDayLeaveRequest char(1)
	,empReqNoAllDayAttnd char(1)
	,empHSubstitution char(1)
	,completedAttndInDay char(1)
	,noDeductionLeave char(1)
	,isCheckin char(1) --�᡹������
	,isLate char(1) --������
	,minuteLate int --��¡��ҷ�
	,requestNotAttdnCheckIn char(1) --��͹��ѵ�����᡹��������������
	,requestLeaveAsLate char(1)--����ҵ���������������
	,isCheckout char(1) --�᡹�͡�������
	,isEarly  char(1) --�͡��͹�������
	,minuteEarly int --�ӹǹ�ҷշ���͡��͹
	,requuestNotAttdnCheckOut char(1)--�ҡ������᡹�͡��͹��ѵ������������
	,requestLeaveAsEarly char(1)--�ҵ������᡹�͡�͹�������
	,isCheckBrake char(1)--�᡹�ѡ�������
	,stateOfCal char(6)
)
AS
BEGIN	
	declare @dummy varchar(500)
	,@checkInAndCheckOutInSameDay int /*��������͡������ѹ���ǡѹ 0=�� 1=�ѹ�Ѵ� */
	,@checkIncheckBrakecheckOut char(3);
	-- Fill the table variable with the rows for your result set
	
	--shiftInfo
	declare 
	@shiftMusTA	char(1)
	,@shiftName	varchar(max)
	,@shiftStartTime	 time
	,@shiftHaveToCheckEnd	char(1)
	,@shiftEndTime	time
	,@shiftMinCheckInTime	time
	,@shiftMaxCheckOutTime	time
	,@shiftBrakeCheck	char(1)
	,@shiftBreakStart	time
	,@shiftBreakEnd	time
	,@shiftMinuteBrake	int
	
	
	declare 
	@empHW char(1) --�ѹ��ش������� 
	,@assignedHwToEmp char(1) --��˹��������ѹ��ش�������
	,@checkInOutExists char(1) --�ա���᡹�����������
	,@noNeedAttdnShift char(1) --��Чҹ�������ͧ�᡹�������/�͡/�ѡ
	,@assignedShiftToEmp char(1) --��˹��Чҹ��ѹ����������
	,@empAllDayLeaveRequest char(1) --�բ�����شẺ����ѹ�������
	,@empReqNoAllDayAttnd char(1) --�բ�����᡹����ѹ�������
	,@empHSubstitution char(1) --�ա�â���Ѻ�ѹ��ش	
	,@completedAttndInDay char(1) --�᡹�ú����Чҹ 
	,@noDeductionLeave char(1) --��Ẻ����ѡ
	,@isCheckin char(1) --�᡹������
	,@isLate char(1) --������
	,@minuteLate int --��¡��ҷ�
	,@requestNotAttdnCheckIn char(1) --��͹��ѵ�����᡹��������������
	,@requestLeaveAsLate char(1) --����ҵ���������������
	,@isCheckout char(1) --�᡹�͡�������
	,@isEarly  char(1) --�͡��͹�������
	,@minuteEarly int --�ӹǹ�ҷշ���͡��͹
	,@requuestNotAttdnCheckOut char(1)--�ҡ������᡹�͡��͹��ѵ������������
	,@requestLeaveAsEarly char(1)--�ҵ������᡹�͡�͹�������
	,@isCheckBrake char(1)--�᡹�ѡ�������
	,@stateOfCal char(6) --ʶҹС�û����ż�
	;	
	declare @ta_empTaDetails table (USERID int,CHECKTIME datetime,SENSORID nvarchar(5));
	
	/*
	set @empHW='N'; --�ѹ��ش�������
	set @assignedHwToEmp = 'N' --��˹��������ѹ��ش�������
	set @checkInOutExists='N'; --�ա���᡹�����������
	set @noNeedAttdnShift  = 'N' --��Чҹ�������ͧ�᡹�������/�͡/�ѡ
	set @assignedShiftToEmp = 'N' --��˹��Чҹ��ѹ����������
	set @empAllDayLeaveRequest='N'; --�բ�����شẺ����ѹ�������
	set @empReqNoAllDayAttnd = 'N'; --�բ�����᡹���Ƿ���ѹ�������
	set @empHSubstitution='N'; --�ա�â���Ѻ�ѹ��ش
	set @completedAttndInDay='N'; --�᡹�ú����Чҹ 
	set @stateOfCal = 0;
	*/
	
	set @empHW=Null; --�ѹ��ش�������
	set @assignedHwToEmp = Null --��˹��������ѹ��ش�������
	set @checkInOutExists=Null; --�ա���᡹�����������
	set @noNeedAttdnShift  = Null --��Чҹ�������ͧ�᡹�������/�͡/�ѡ
	set @assignedShiftToEmp = Null --��˹��Чҹ��ѹ����������
	set @empAllDayLeaveRequest=Null; --�բ�����شẺ����ѹ�������
	set @empReqNoAllDayAttnd = Null; --�բ�����᡹���Ƿ���ѹ�������
	set @empHSubstitution=Null; --�ա�â���Ѻ�ѹ��ش
	set @completedAttndInDay=Null; --�᡹�ú����Чҹ 
	set @noDeductionLeave=null; --��Ẻ����ѡ
	set @isCheckin =null --�᡹������
	set @isLate =null --������
	set @minuteLate =null --��¡��ҷ�
	set @requestNotAttdnCheckIn=null --��͹��ѵ�����᡹��������������
	set @requestLeaveAsLate=null  --����ҵ���������������
	set @isCheckout=null --�᡹�͡�������
	set @isEarly=null --�͡��͹�������
	set @requuestNotAttdnCheckOut=null--�ҡ������᡹�͡��͹��ѵ������������
	set @requestLeaveAsEarly=null--�ҵ������᡹�͡�͹�������
	set @isCheckBrake=null --�᡹�ѡ�������
	set @minuteEarly=null --�ӹǹ�ҷշ���͡��͹	
	set @stateOfCal = 'xxxxxx';
	
	--����ҧҹ����ѹ ���� ��͹��ѵ�����᡹����ѹ ����ͧ�ӹǳ���� 
	if exists(
		select 1 from ta_employeeLeaves el
										where el.IDNo=@IDNo
										and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1										
										and @cDate between cast(convert(varchar,el.startDateTime,111) as datetime) and cast(convert(varchar,el.endDateTime,111) as datetime)
										and datediff(hour,el.startDateTime,el.endDateTime)>=9
		union
		select 1 from ta_empNotAttendances where IDNo=@IDNo and @cDate between fromDate and toDate and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and approvedDate is not null and approved='Y' and [status]<>'D'
	)
	begin 
		--set @stateOfCal=4
		select @stateOfCal=stuff(@stateOfCal,1,1,'D')
	end
	else
	begin
			--��˹��������ѹ��ش������� �����
			if exists(select 1 from ta_empForHWGs ehw inner join ta_hwGroups hwg on ehw.hwGroupId=hwg.id where IDNo=@IDNo and hwg.[year]=year(@cDate))
			begin 
				set @assignedHwToEmp='Y' 	
				--���ѹ��ش �����
				if exists(select 1 FROM  [ta_empForHWGs] ehw inner join ta_hws m on ehw.hwGroupId=m.hwGroupId where IDNo=@IDNo and startDate=@cDate) begin set @empHW='Y' end
				--���ѹ��ش ��
				else
				begin	
					set @empHW='N'
					--��˹��Чҹ�������������� �����
					if exists(select 1 from ta_shiftForEmployees sh4emp where @cDate between sh4emp.activeFrom and sh4emp.activeTo and sh4emp.active='Y' and sh4emp.IDNo=@IDNo) 
					begin
						--�֧������ shift �͡��
						select 
							@shiftMusTA=musTA
							,@shiftName=shiftName
							,@shiftStartTime=startTime
							,@shiftHaveToCheckEnd=haveToCheckEnd
							,@shiftEndTime=endTime
							,@shiftMinCheckInTime=minCheckInTime
							,@shiftMaxCheckOutTime=maxCheckOuttime
							,@shiftBrakeCheck=brakeCheck
							,@shiftBreakStart=breakStart
							,@shiftBreakEnd=breakEnd
							,@shiftMinuteBrake=minuteBrake
							from ta_shifts sh
							inner join ta_shiftForEmployees she on sh.id=she.shiftId
							and she.IDNo=@IDNo
							and @cDate between she.activeFrom and she.activeTo and active='Y'
						
						set @assignedShiftToEmp='Y'
						--�������ЧҹẺ����ͧ�᡹������� �����					
						if  @shiftMusTA='N'
						begin 
							set @noNeedAttdnShift  = 'Y' 
							--�ա���ҧҹ�������������� �����(�óշ��Чҹ����ͧ�᡹)
							if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and convert(varchar,@cDate,111) between convert(varchar,startDateTime,111) and convert(varchar,endDateTime,111))
							begin 
								--set @stateOfCal=1
								select @stateOfCal=stuff(@stateOfCal,1,1,'A')
							end
							--�ա���ҧҹ�������������� ��(�óշ��Чҹ����ͧ�᡹)					
						end
						else 
						begin
							set @noNeedAttdnShift  = 'N'
								--���᡹������ѹ���������� �����								
								if exists(select 1 from @temp_ta_empTaDetails etd where etd.CHECKTIME between 
										dateadd(minute,datepart(minute,@shiftMinCheckInTime)*(-1),dateadd(hour,datepart(hour,@shiftMinCheckInTime)*(-1),@cDate+@shiftStartTime)) --����������Ѻ����᡹
										and 
										dateadd(minute,datepart(minute,@shiftMaxCheckOutTime),dateadd(hour,datepart(hour,@shiftMaxCheckOutTime),@cDate+@shiftStartTime)) --��������ش����᡹
										)
								begin 
									set @checkInOutExists='Y'; 
									--�᡹�ú�������˹�㹡Чҹ������� �����
									select @checkIncheckBrakecheckOut=checkIn+checkBrake+checkOut
											,@isCheckIn=checkIn									
											,@isLate = isLate
											,@minuteLate = minuteLate
											,@isCheckOut=checkOut
											,@isEarly = isEarly
											,@isCheckBrake=checkBrake
											,@minuteEarly = minuteEarly											 
									from FN_001_01_completedDailyAttdnCheck(@cDate,@IDNo,@temp_ta_empTaDetails
										,@shiftMusTA,@shiftName,@shiftStartTime,@shiftHaveToCheckEnd,@shiftEndTime,@shiftMinCheckInTime
										,@shiftMaxCheckOutTime,@shiftBrakeCheck,@shiftBreakStart,@shiftBreakEnd,@shiftMinuteBrake)
																		
									set @dummy=@checkIncheckBrakecheckOut+' '+convert(varchar,@shiftEndTime)
									--set @isCheckIn = substring(@checkIncheckBrakecheckOut,1,1);							
									--set @isCheckOut = substring(@checkIncheckBrakecheckOut,3,1);
									if @checkIncheckBrakecheckOut='YYY' and @isLate='N' and @isEarly='N' --�ҡ�᡹��� �͡ �ѡ �ú ���������� ����͡��͹ 
									begin set @completedAttndInDay ='Y' end 
									else
									begin
										set @completedAttndInDay ='N' 
										--�᡹����������, ����������, ����ҡ��������¡��ҷ� 
										--select @isCheckin = isCheckin, @isLate=isLate, @minuteLate = minuteLate from dbo.FN_001_02_CheckInInfo(@cDate,@IDNo,@temp_ta_empTaDetails)
										--�᡹����������, ����������, ����ҡ��������¡��ҷ� ��
										--�ҡ������᡹���
										if @isCheckin = 'N'
										begin
											if exists( -- ��͹��ѵ�������᡹�������
													select * from ta_empNotAttendances where IDNo=@IDNo 	and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null and checkIn='Y' and [status]<>'D'
												) 
											begin
												set @requestNotAttdnCheckIn='Y';
											end
											else
											begin
												if exists( --��͹��ѵ��Ҥ����ѹ�á�������
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
														where she.IDNo=@IDNo
														and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
														--and @cDate between she.activeFrom and she.activeTo 
														and she.active='Y'
														and el.startDateTime <= @cDate+@shiftStartTime
														and el.endDateTime >= dateadd(hour,4,@cDate+@shiftStartTime)
												)
												begin 
													select @stateOfCal=stuff(@stateOfCal,2,1,'D')
													set @isCheckBrake ='Y' --�ҡ�Ҥ����ѹ������ͧ�����������ͧ�᡹�ѡ
												end
												else
												begin 												
													--set @stateOfCal = 10;
													select @stateOfCal=stuff(@stateOfCal,2,1,'A')
													set @requestNotAttdnCheckIn='N'
												end
												--��͹��ѵ��Ҥ����ѹ������� ��
											end	
										end
										--�ҡ������᡹��� ��
										
										--�ҡ��� ����ҵ���������������								
										if @isLate = 'Y'
										begin
											if exists(
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
												where she.IDNo=@IDNo
												and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
												and @cDate between she.activeFrom and she.activeTo and she.active='Y'
												and @cDate+sh.startTime>=el.startDateTime 
												and DATEDIFF(minute,@cDate+sh.startTime,el.endDateTime)>=@minuteLate
											)
											begin
												set @requestLeaveAsLate='Y'  --����ҵ��������
												--set @stateOfCal=30;
												select @stateOfCal=stuff(@stateOfCal,2,1,'C')
											end
											else
											begin
												set @requestLeaveAsLate='N'  --�������ҵ��������
												--set @stateOfCal=20;
												select @stateOfCal=stuff(@stateOfCal,2,1,'B')
											end
										end
										--�ҡ��� ����ҵ��������������� ��
										--�礡���᡹�͡								
										--�ҡ������᡹�͡ ��������᡹������� --@requestLeaveAsEarly 
										if @isCheckOut='N'
										begin
											if exists(
												select 1 from ta_empNotAttendances
												where IDNo=@IDNo
												and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null
												and checkOut='Y' and [status]<>'D'
											) 
											begin 
												set @requuestNotAttdnCheckOut='Y' 
											end 
											else 
											begin 
												--���ҧҹ�����ѹ��ѧ�������
												if exists(
													select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
														where she.IDNo=@IDNo
														and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
														--and @cDate between she.activeFrom and she.activeTo 
														and she.active='Y'
														and el.startDateTime <= dateadd(hour,-4,dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime)))
														and el.endDateTime >= dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))
												)
												begin
													select @stateOfCal=stuff(@stateOfCal,3,1,'D') 
													set @isCheckBrake ='Y' --�ҡ�Ҥ����ѹ������ͧ�����������ͧ�᡹�ѡ
												end
												else
												begin
													set @requuestNotAttdnCheckOut='N';/*set @stateOfCal=40*/
													select @stateOfCal=stuff(@stateOfCal,3,1,'A') 
												end
												--���ҧҹ�����ѹ��ѧ������� ��
											end
										end
										--�ҡ������᡹�͡ ��������᡹������� ��
										--�ҡ�͡��͹������������
										if @isEarly = 'Y'
										begin
											if exists(
												select 1 from ta_shiftForEmployees she 
														inner join ta_shifts sh on sh.id=she.shiftId
														inner join ta_employeeLeaves el on she.IDNo=el.IDNo
												where she.IDNo=@IDNo
												and el.approvedDate is not null and el.approveResult='Y' and el.[status]=1
												and @cDate between she.activeFrom and she.activeTo and she.active='Y'
												and dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))<=el.endDateTime 
												and DATEDIFF(minute
													,el.startDateTime
													,dateadd(minute,DATEPART(minute,@shiftEndTime),dateadd(hour,datepart(hour,@shiftEndTime),@cDate+@shiftStartTime))											
													)>=@minuteEarly
											)
											begin
												set @requestLeaveAsEarly='Y'  --����ҵ������͡��͹
												--set @stateOfCal=50;
												select @stateOfCal=stuff(@stateOfCal,3,1,'C')
											end
											else
											begin
												set @requestLeaveAsEarly='N'  --�������ҵ������͡��͹
												--set @stateOfCal=60;
												select @stateOfCal=stuff(@stateOfCal,3,1,'B')
											end
										end
										--�ҡ�͡��͹������������ ��
										
										--�Ѵ��áѺ����᡹�ѡ���§
										if @isCheckBrake='N'
										begin
											set @dummy = @dummy+'check brake'
											--�Чҹ����ͧ��᡹�������
											if @shiftBrakeCheck='N' begin 	select @stateOfCal=stuff(@stateOfCal,4,1,'B') end
											else
											begin
												--�����ŧ���Ҿѡ���§�������
												if exists(
													select 1 from ta_empNotAttendances															
															where IDNo=@IDNo
															and @cDate between fromDate and toDate and approved='Y' and approvedDate is not null
															and (brakeStart='Y' or brakeEnd='Y')
															and [status]<>'D'
												) 
												begin select @stateOfCal=stuff(@stateOfCal,4,1,'C')	end
												else
												begin select @stateOfCal=stuff(@stateOfCal,4,1,'D')	end
											end
										end
										else
										begin
												select @stateOfCal=stuff(@stateOfCal,4,1,'A')
										end
										--�Ѵ��áѺ����᡹�ѡ���§ ��
									end 
									--�᡹�ú�������˹�㹡Чҹ������� ��
								end
								else 
								begin
										set @checkInOutExists='N';
										--��͹��ѵ�������᡹����ѹ������� �����	
										if exists(select 1 from ta_empNotAttendances where IDNo=@IDNo and @cDate between fromDate and toDate and checkIn='Y' and brakeStart='Y' and brakeEnd='Y' and checkOut='Y' and approvedDate is not null and approved='Y' and [status]<>'D')
										begin set @empReqNoAllDayAttnd='Y' end
										else 
										begin
											begin set @empReqNoAllDayAttnd='N' end
											--����Ѻ�ѹ��ش�������							
											if exists(select 1 from ta_empForHWGs ehg left join ta_hwGroups hg on ehg.hwGroupId=hg.id left join ta_hws hw on hw.hwGroupId=hg.id left join ta_hwSubstutions hs on ehg.id=hs.empHwId and hw.id=hs.hwId	where ehg.IDNo=@IDNo  and hg.year=year(@cDate) and ehg.IDNo=@IDNo and hs.approvedDate is not null and hs.approved='Y' and hs.substituteWith=@cDate)
											begin set @empHSubstitution = 'Y' end
											else
											begin /*set @stateOfCal=2;*/select @stateOfCal=stuff(@stateOfCal,1,1,'B'); set @empHSubstitution = 'N' end
											--����Ѻ�ѹ��ش������� ��
										end
										--��͹��ѵ�������᡹����ѹ������� ��
								end
								--���᡹������ѹ���������� ��
						end
						--�������ЧҹẺ����ͧ�᡹������� ��		
					end
					else
					begin
						set @assignedShiftToEmp='N'
						--set @stateOfCal=-2
						select @stateOfCal=stuff(@stateOfCal,1,1,'F');
					end
					--��˹��Чҹ�������������� ��
					
					--����ҧҹẺ����ѹ������� �����
					declare @startOfShift time, @endOfShift time; 
					select @startOfShift =sh.startTime
					,@endOfShift = isnull(sh.endTime,DATEADD(hour,9,sh.startTime)) from ta_shiftForEmployees esh inner join ta_shifts sh on esh.shiftId=sh.id where esh.IDNo=@IDNo and @cDate between esh.activeFrom and dateadd(minute,59,DATEADD(hour,23,esh.activeTo)) and esh.active='Y'
					if exists(select 1 from ta_employeeLeaves where IDNo=@IDNo and approvedDate is not null and approveResult='Y' and @cDate+@startOfShift between startDateTime and endDateTime and @cDate+@endOfShift between startDateTime and endDateTime and DATEDIFF(hour, @cDate+@startOfShift, @cDate+@endOfShift)>=9 )
					begin 
						set @empAllDayLeaveRequest='Y' 
						--��Ẻ����ѡ������� �����
						if exists(
							select 1 from ta_employeeLeaves el inner join ta_leaveTypes lt on el.leaveId=lt.id
							where 
							el.IDNo=@IDNo and el.approvedDate is not null and el.approveResult='Y' 
							and @cDate+@startOfShift between el.startDateTime and el.endDateTime 
							and @cDate+@endOfShift between el.startDateTime and el.endDateTime 
							and DATEDIFF(hour, @cDate+@startOfShift, @cDate+@endOfShift)>=9 
							and lt.paymentOnLeave in ('Y','N'))
							begin set @noDeductionLeave = 'Y' end
							else 
							begin set @noDeductionLeave = 'N' end
						--��Ẻ����ѡ������� ��
					end 
					--����ҧҹẺ����ѹ������� ��
				end
			end
			else --�ѧ������˹��ѹ��ش���
			begin
				--set @stateOfCal=-1
				select @stateOfCal=stuff(@stateOfCal,1,1,'E');
			end
			--��˹��������ѹ��ش������� ��
			
	end--�ҡ�ҧ�ҹ����ѹ���͢�����᡹���Ƿ���ѹ
	
	insert into @EmpEachDayInfo (
					dummy
					,empHW
					,assignedHwToEmp
					,checkInOutExists
					,noNeedAttdnShift
					,assignedShiftToEmp
					,empAllDayLeaveRequest
					,empReqNoAllDayAttnd
					,empHSubstitution
					,completedAttndInDay
					,noDeductionLeave
					,isCheckin --�᡹������
					,isLate --������
					,minuteLate --��¡��ҷ�			
					,requestNotAttdnCheckIn --��͹��ѵ�����᡹��������������	
					,requestLeaveAsLate --��͹��ѵ��ҵ��������������� 
					,isCheckout --�᡹�͡�������
					,isEarly --�͡��͹�������
					,minuteEarly --�ҷշ���͡��͹
					,requuestNotAttdnCheckOut --�ҡ������᡹�͡��͹��ѵ������������
					,requestLeaveAsEarly --�ҵ������᡹�͡�͹�������
					,isCheckBrake --�᡹�ѡ�������
					,stateOfCal
					) select 
					@dummy
					,@empHW
					,@assignedHwToEmp
					,@checkInOutExists
					,@noNeedAttdnShift
					,@assignedShiftToEmp
					,@empAllDayLeaveRequest
					,@empReqNoAllDayAttnd
					,@empHSubstitution
					,@completedAttndInDay
					,@noDeductionLeave
					,@isCheckin --�᡹������
					,@isLate --������
					,@minuteLate --��¡��ҷ�			
					,@requestNotAttdnCheckIn --��͹��ѵ�����᡹��������������	
					,@requestLeaveAsLate --��͹��ѵ��ҵ��������������� 
					,@isCheckout --�᡹�͡�������
					,@isEarly --�͡��͹�������
					,@minuteEarly --�ҷշ���͡��͹
					,@requuestNotAttdnCheckOut --�ҡ������᡹�͡��͹��ѵ������������
					,@requestLeaveAsEarly --�ҵ������᡹�͡�͹�������
					,@isCheckBrake --�᡹�ѡ�������
					,@stateOfCal
	RETURN 
END

