use EMPBase;
declare @cDate datetime, @IDNo varchar(13);
select @cDate='20180210', @IDNo='3940200536679'
-- Declare the return variable here
	declare @isConflict varchar(6)
	,@checkinTime datetime
	,@checkBrakeStartTime datetime
	,@checkBrakeEndTime datetime
	,@checkoutTime datetime
	,@shiftName varchar(max)
	,@mustTa char(1);
	
	declare @noCheckin char(1), @noCheckBrakeStart char(1), @noCheckBrakeEnd char(1), @noCheckout char(1);
	
	set @isConflict = 'N';		

	-- ดึงเวลาที่ต้องสแกนนิ้วของวันนั้นออกมาก่อน
	select top 1 
	@mustTa = sv.musTA
	,@checkinTime = @cDate+sv.startTime	
	,@checkBrakeStartTime = @cDate+sv.breakStart
	,@checkBrakeEndTime = @cDate+sv.breakEnd
	,@checkoutTime = @cDate+sv.endTime
	,@shiftName = sv.shiftName
	from ta_shiftsView sv inner join ta_shiftForEmployees sfe on sv.id=sfe.shiftId 
	where sfe.IDNo=@IDNo and @cDate between sfe.activeFrom and sfe.activeTo
	
	/*test data*/
	select
	@mustTa = 'Y'
	,@checkinTime = '20180210 08:00'	
	,@checkBrakeStartTime = '20180210 12:30'
	,@checkBrakeEndTime = '20180210 13:30'
	,@checkoutTime = '20180210 17:00'
	,@shiftName = 'test shift'
	/*endtest*/
	
	if @mustTa<>'Y' begin goto endScript; end
	
	--ดึงการขอไม่สแกนนิ้วของวันนั้นออกมา
	select
	@noCheckin =ena.checkIn, @noCheckBrakeStart=ena.brakeStart, @noCheckBrakeEnd =ena.brakeEnd, @noCheckout =ena.checkOut
	from ta_empNotAttendances ena  where ena.IDNo=@IDNo and @cDate between ena.fromDate and ena.toDate and ena.[status]='A'
	
	/*test data*/
	select @noCheckin ='N', @noCheckBrakeStart='N', @noCheckBrakeEnd ='Y', @noCheckout ='Y'
	update ta_employeeLeaves set approveResult='N',[status]=1,approvedDate=null,formated='O',startDateTime='20180210 08:41' ,endDateTime= '20180210 14:55' where id='3432'	
	/*endtest*/
	
	
	--เช็คว่าการลา ขัดแย้งกับ การขออนุมัติไม่สแกนนิ้วตอนเช้าหรือไม่
	if ISNULL(@noCheckin,'X')='Y'	and
	exists
		(
			--ดึงข้อมูลการลาที่คร่อมเวลาเข้างาน ของวันนั้นออกมา
			select elc.startDateTime,elc.endDateTime,elc.formated,*
			from ta_employeeLeavesCalTime  elc
			where 1=1 and elc.[status]=1
				and IDNo=@IDNo 
				and @checkinTime between elc.startDateTime and elc.endDateTime
				and ((elc.approvedDate is not null and approveResult='Y') or (elc.approvedDate is null and approveResult='N'))
		) 
	begin
		set @isConflict='Y:X000';
		goto endScript
	end
	
	--เช็คว่าการลา ขัดแย้งกับขออนุมัติไม่สแกนกลับหรือไม่
	if ISNULL(@noCheckout,'X')='Y' and
	exists
		(
			--ดึงข้อมูลการลาที่คร่อมเวลาเข้างาน ของวันนั้นออกมา
			select elc.startDateTime,elc.endDateTime,elc.formated,*
			from ta_employeeLeavesCalTime  elc
			where 1=1 and elc.[status]=1
				and IDNo=@IDNo 
				and @checkoutTime between elc.startDateTime and elc.endDateTime
				and ((elc.approvedDate is not null and approveResult='Y') or (elc.approvedDate is null and approveResult='N'))
		) 
	begin		
			set @isConflict='Y:000X';
			goto endScript		
	end
	
	--เช็คว่าการลา ขัดแย้งกับขออนุมัติไม่สแกนไปพักหรือไม่ หากลาช่วงเช้า หรือลาทั้งวัน หรือลาวันครึ่ง
	if ISNULL(@noCheckBrakeStart,'X')='Y' and
	exists
		(
			--ดึงข้อมูลการลาที่คร่อมเวลาเข้างาน ของวันนั้นออกมา
			select elc.startDateTime,elc.endDateTime,elc.formated,*
			from ta_employeeLeavesCalTime  elc
			where 1=1 and elc.[status]=1
				and IDNo=@IDNo 
				and convert(datetime,convert(varchar,@checkBrakeStartTime,111)) between convert(datetime,convert(varchar,elc.startDateTime,111)) and convert(datetime,convert(varchar,elc.endDateTime,111))
				and elc.formated in ('M','A','S')
				and ((elc.approvedDate is not null and approveResult='Y') or (elc.approvedDate is null and approveResult='N'))
		) 
	begin		
			set @isConflict='Y:0X00';
			goto endScript		
	end
	
	--เช็คว่าการลา ขัดแย้งกับขออนุมัติไม่สแกนกลับจากพักหรือไม่ หากลาช่วงบ่าย หรือลาทั้งวัน
	if ISNULL(@noCheckBrakeEnd,'X')='Y' and
	exists
		(
			--ดึงข้อมูลการลาที่คร่อมเวลาเข้างาน ของวันนั้นออกมา
			select elc.startDateTime,elc.endDateTime,elc.formated,*
			from ta_employeeLeavesCalTime  elc
			where 1=1 and elc.[status]=1
				and IDNo=@IDNo 				
				and convert(datetime,convert(varchar,@checkBrakeEndTime,111)) between convert(datetime,convert(varchar,elc.startDateTime,111)) and convert(datetime,convert(varchar,elc.endDateTime,111))
				and elc.formated in ('E','A')
				and ((elc.approvedDate is not null and approveResult='Y') or (elc.approvedDate is null and approveResult='N'))
		) 
	begin		
			set @isConflict='Y:00X0';
			goto endScript
	end
	
	--เช็คว่าการลา ขัดแย้งกับขออนุมัติไม่สแกนกลับจากพัก/ไปพัก หรือไม่ หากลาคร่อมพักเที่ยง case 1 
	if (ISNULL(@noCheckBrakeStart,'X')='Y' or ISNULL(@noCheckBrakeEnd,'X')='Y' )and
	exists
		(
			--ดึงข้อมูลการลาที่คร่อมเวลาเข้างาน ของวันนั้นออกมา
			select elc.startDateTime,elc.endDateTime,elc.formated,*
			from ta_employeeLeavesCalTime  elc
			where 1=1 and elc.[status]=1
				and IDNo=@IDNo 
				and ((@checkBrakeStartTime between elc.startDateTime and elc.endDateTime) or (@checkBrakeEndTime between elc.startDateTime and elc.endDateTime))
				and (elc.formated='O')
				and ((elc.approvedDate is not null and approveResult='Y') or (elc.approvedDate is null and approveResult='N'))
		) 
	begin
			set @isConflict='Y:0XX0';
			goto endScript
	end

	
	select 'p'
	
	endScript:
	select @mustTa,@checkinTime,@checkBrakeStartTime,@checkBrakeEndTime,@checkoutTime,@shiftName
	select @noCheckin,@noCheckBrakeStart,@noCheckBrakeEnd,@noCheckout,@isConflict