use EMPBase;
set nocount on;
IF OBJECT_ID('tempdb..#firstCalculation2') IS NOT NULL DROP TABLE #firstCalculation2
--��͹��лշ��Фӹǳ
declare @year int, @month int, @msg varchar(max),@listOfPersonIDNo listOfPersonIDNo,@ta_userInfos temp_ta_userInfosV2
set @year=2018; set @month=6;

--���͡�����ӹǳ���� @listOfPersonIDNo ��͹
insert into @listOfPersonIDNo
select IDNo
from hrm_humanoPersonAll h inner join hrm_paymentPackage pp on h.period_no=pp.id
where 1=1
and  (em_status in ('W','P') 
											 or (
													(em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date))
												)
										) --��������͡����� ���ѹ���͡�ҡ���� �ѹ�á�ͧ�ͺ�Թ��͹���ӹǳ
and IDNo not in (select IDNo from hrm_humanoPersonAll where em_status in ('W','P') group by IDNo having count(*)>1)
--and employeeCode in (select c.employeeCode from hrm_Commanders c inner join hrm_humanoPersonAll h on c.cEmpCode=h.employeeCode where h.em_status in ('W','P') and h.IDNo='3920600717310' and [rank] in (0,1,2,3,4)) -- �١��ͧ���ԧ�����ǧ
--and  (positionName like '%�Ѿ%' or positionName like '%�����%')
--and  (positionName like '%���¡���Թ%')
--and  (positionName like '%����¹%')
--and  ((positionName like '%��ҫ���%') or (positionName like '%����%') or (positionName like '%�Ѿ%' or positionName like '%�����%'))
--and  (positionName like '%����������%')
--and  (positionName like '%����Ѵ˹��%')
--and h.firstName='���ǳ���'
--and h.lastName='��෾'
--and h.lastName='������'
and h.IDNo='1840100488928'	--��Ҿ�ó �Ӵ�
--and h.IDno in ('1920600158926','1920200059513','1929900346785','1801700077521','1920200040871','1920300091508')
--and h.IDno='1501000071638' --�ѹ���
--and h.IDNo='1801200013485' --
--and h.IDNo='1800700125231' --
--and h.IDNo in ('3920100309042','3920500135394')
--and h.corpName like '%¹�á�����%'
--and h.corpName like '%���㨾Ѳ��%'
--and h.corpName like '%¹�á�� 2004%'
--and h.corpName like '%����¹%'
--and h.corpName not like '%��������%'
--and h.hrmCorpId='O6K16BG00000041' 
--and h.period_no='PPK16BT00000049'
--and h.IDNo<>'3841800056050'/*��ǻѭ��*/	
--and h.corpName like '%¹�á�� 2007%'
--and h.corpName like '%2556%'
--and h.corpName like '%�����%' 
--and  period_no not in ('PPK16C800000052')
--and IDno='1800700115545'
--and IDNo = (select IDNO from hrm_humanopersonAll where employeeCode='TJY40000144')
--order by IDNo
--select * from @listOfPersonIDNo

--��� userid �ҡ�к��᡹���������� @ta_userInfos ��͹
insert into @ta_userInfos select SSN,USERID from ta_userInfos where SSN in (select IDNo from @listOfPersonIDNo)
--select * from @ta_userInfos 

select mm.IDNo,h.titleName+h.firstName+' '+h.lastName personInfo
		,CONVERT(varchar,cDate,111) cDate
		,cdow dow
		,replace(replace(replace(tsc.msg+case when empHW='Y' then '{{�ѹ��ش}}' else '' end,'}}{{',','),'{{',''),'}}','')	msg
		,tsc.totalDeduction		
		,stateOfCal soc--ʶҹС�äӹǳ
		,brakeDuration bdr --�ӹǹ�ҷշ��仾ѡ
		,isCheckin ici--�᡹������
		,isLate il --������
		,minuteLate mnl --��¡��ҷ�
		,requestNotAttdnCheckIn rnaci--��͹��ѵ�����᡹����������
		,requestLeaveAsLate rlal--��͹��ѵ��ҵ���������������
		,isCheckout ico--�᡹�͡�������
		,isEarly  iel--�͡��͹�������
		,minuteEarly mel --�ӹǹ�ҷշ���͡��͹
		,requuestNotAttdnCheckOut rnaco --�ҡ������᡹�͡��͹��ѵ������������
		,requestLeaveAsEarly rlae--�ҵ������᡹�͡�͹�������
		,isCheckBrake icb --ŧ���Ҿѡ���§�ú����������͡
		
		,brakeEarlyOrLate beo --仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ�������
		,minuteBrakeEarlyOrLate mbeo--�ӹǹҷշ��仾ѡ��͹����Ѻ�ӹǹ�ҷշ���Ѻ�ҡ�ѡ�ŷ
		,assignedHwToEmp ahwte --��˹��ѹ��ش�������
		,empHW hw  --�ѹ��ش�������
		,assignedShiftToEmp ashte --��˹��Чҹ��������
		
		,noNeedAttdnShift nnash --�繡Чҹ�������ͧŧ����
		,checkInOutExists cioe --�ա��ŧ������ѹ���
		,completedAttndInDay	 catdid --ŧ������ѹ��鹤ú��� ��� �ѡ ����͡
		,empAllDayLeaveRequest eadlr --���ҧҹ����ѹ�������
		,empReqNoAllDayAttnd ernadatd --�����ŧ���ҷ���ѹ�������
		,empHSubstitution ehs --����Ѻ�ѹ��ش�������
		,noDeductionLeave ndl --�繡����Ẻ����ѡ
		,minuteLeaveDuringBrake mldb --�ӹǹ�ҷշ���Ҥ�������͵Դ�Ѻ���Ҿѡ
		,paymentOfLeaveDuringBrake poldb --�ѡ����������ҧ�ѡ�������(�ѧ������)
		,checkTimeForLeaveDuringBrakeOK ctfldbo--�᡹���Ǥú��ǹ��������ҡ�Ҥ������þѡ(�ѧ������)
		,h.workEnd
		,h.workStart
		,tsc.[absent],tsc.[leave],tsc.[brake],tsc.[lateAndEarly],tsc.errandLeave,tsc.sickLeave,tsc.notCheckInOrOut
		,mm.dummy
		--,mm.dummy mmDummy
		into #firstCalculation2
from dbo.FN_calculateMonthlyTA(@year,@month,@listOfPersonIDNo,@ta_userInfos) mm left join  hrm_humanoPersonAll h on mm.IDNO=h.IDNo collate Thai_CI_AS 
		inner join hrm_paymentPackage pp on h.period_no=pp.id
		and(
				 h.em_status in ('W','P') 
				 or
				(h.em_status not in ('W','P')  and (em_status not in ('W','P')) and workEnd >= dateadd(month,pe_startmonth,CONVERT(varchar,@year)+'/'+CONVERT(varchar,@month)+'/'+CONVERT(varchar,pe_start_date))   ) 
			)
cross apply FN_001_translateStateOfCal(mm.IDNo,stateOfCal,cDate,minuteLate,minuteEarly,minuteLeaveDuringBrake,minuteBrakeEarlyOrLate,paymentOfLeaveDuringBrake,h.workStart,h.workEnd,isCheckin) tsc

select 
IDNo,personInfo,cDate,dow,msg,totalDeduction ttd
,isnull([absent],0.00)[absent]
,isnull([leave],0.00)leave
,isnull([brake],0.00)brake
,isnull([lateAndEarly],0.00)lateAndEarly
,isnull(errandLeave,0.00)errandLeave
,isnull(sickLeave,0.00)sickLeave
,isnull(notCheckInOrOut,0.00) notCheckInOrOut
,soc,bdr,ici,il,mnl,rnaci,rlal,ico,iel,mel,rnaco,rlae,icb,beo,mbeo,ahwte,hw,ashte,nnash,cioe,catdid,eadlr,ernadatd,ehs,ndl,mldb,poldb,ctfldbo,workStart,workEnd,dummy
from #firstCalculation2
where 1=1
--and msg='����' 
--and soc='++DH++++'--+FGA++
--and ( soc='+BGF++++' or soc='+BGA++++' or totalDeduction is null)
--and totalDeduction is null
--and SUBSTRING(soc,6,1)<>'+'
--and msg like '%���ŧ�������%' 
--and msg like '%�����ҧ�ѹ%' 
--and (msg like '%���%' or msg like '%���ŧ�����͡%' or msg like '%�͡��͹%')
--and IDNo='1801700023331'
--and IDNo<>'���' 
--and totalDeduction is null
--and SUBSTRING(soc,4,1)='E' --�Ҥ�������Ҿѡ

union 
select 
'���' IDNo,personInfo personInfo,	null cDate,null dow,null msg, cast(ceiling(isnull(sum(totalDeduction),0)) AS decimal(18,2))  ttd
,isnull(sum([absent]),0.00)[absent]
,isnull(sum([leave]),0.00)leave
,isnull(sum([brake]),0.00)brake
,isnull(sum([lateAndEarly]),0.00)lateAndEarly
,isnull(sum(errandLeave),0.00)errandLeave
,isnull(sum(sickLeave),0.00)sickLeave
,isnull(sum(notCheckInOrOut),0.00)notCheckInOrOut
,null soc,null bdr,null ici,null il,null mnl,null rnaci,null rlal,null ico,null iel,null mel,null rnaco,null rlae,null icb,null beo,null mbeo,null ahwte,null hw,null ashte,null nnash,null cioe,null catdid,null eadlr,null ernadatd,null ehs,null ndl,null mldb,null poldb,null ctfldbo,null workStart,null workEnd,null dummy
from #firstCalculation2
group by IDNo, personInfo

union 
select 
'����' IDNo,	'�����ء��' personInfo,	null cDate,	null dow,	null msg,	cast(ceiling(isnull(sum(totalDeduction),0)) AS decimal(18,2))  ttd
,isnull(sum([absent]),0.00)[absent]
,isnull(sum([leave]),0.00)leave
,isnull(sum([brake]),0.00)brake
,isnull(sum([lateAndEarly]),0.00)lateAndEarly
,isnull(sum(errandLeave),0.00)errandLeave
,isnull(sum(sickLeave),0.00)sickLeave
,isnull(sum(notCheckInOrOut),0.00)notCheckInOrOut
,null soc,null bdr,null ici,null il,null mnl,null rnaci,null rlal,null ico,null iel,null mel,null rnaco,null rlae,null icb,null beo,null mbeo,null ahwte,null hw,null ashte,null nnash,null cioe,null catdid,null eadlr,null ernadatd,null ehs,null ndl,null mldb,null poldb,null ctfldbo,null workStart,null workEnd,null dummy
from #firstCalculation2
order by personInfo,IDNo, cDate 

--exec tempdb..sp_help '#firstCalculation2'

---------------------summary
/*

select 
'����' IDNo,null personInfo, cDate,	null dow,	null msg,	cast(ceiling(isnull(sum(totalDeduction),0)) AS decimal(18,2))  ttd
,isnull(sum([absent]),0.00)[absent]
,isnull(sum([leave]),0.00)leave
,isnull(sum([brake]),0.00)brake
,isnull(sum([lateAndEarly]),0.00)lateAndEarly
,isnull(sum(errandLeave),0.00)errandLeave
,isnull(sum(sickLeave),0.00)sickLeave
,isnull(sum(notCheckInOrOut),0.00)notCheckInOrOut
,null soc,null bdr,null ici,null il,null mnl,null rnaci,null rlal,null ico,null iel,null mel,null rnaco,null rlae,null icb,null beo,null mbeo,null ahwte,null hw,null ashte,null nnash,null cioe,null catdid,null eadlr,null ernadatd,null ehs,null ndl,null mldb,null poldb,null ctfldbo,null workStart,null workEnd,null dummy
from #firstCalculation2
group by cDate
order by cDate desc


select 
'����' IDNo,null personInfo, null cDate, dow,	null msg,	cast(ceiling(isnull(sum(totalDeduction),0)) AS decimal(18,2))  ttd
,isnull(sum([absent]),0.00)[absent]
,isnull(sum([leave]),0.00)leave
,isnull(sum([brake]),0.00)brake
,isnull(sum([lateAndEarly]),0.00)lateAndEarly
,isnull(sum(errandLeave),0.00)errandLeave
,isnull(sum(sickLeave),0.00)sickLeave
,isnull(sum(notCheckInOrOut),0.00)notCheckInOrOut
,null soc,null bdr,null ici,null il,null mnl,null rnaci,null rlal,null ico,null iel,null mel,null rnaco,null rlae,null icb,null beo,null mbeo,null ahwte,null hw,null ashte,null nnash,null cioe,null catdid,null eadlr,null ernadatd,null ehs,null ndl,null mldb,null poldb,null ctfldbo,null workStart,null workEnd,null dummy
from #firstCalculation2
group by dow



select headerName
,isnull(notCheckOutAttndApr,0)notCheckOutAttndApr,	isnull(notCheckOutAttndApr,0)notCheckOutAttndApr,	isnull(allDayVacation,0)allDayVacation,	isnull(allDayAbsent,0)allDayAbsent,	isnull(checkinLate,0)checkinLate,	isnull(Normal,0)Normal,	isnull(brakeLateOrEarly,0)brakeLateOrEarly,	isnull(allDayErrandLeave,0)allDayErrandLeave,	isnull(allDaySickLeaveWithCer,0)allDaySickLeaveWithCer,	isnull(allDaySickLeaveNoCer,0)allDaySickLeaveNoCer,	isnull(brakeApr,0)brakeApr,	isnull(brakeErr,0)brakeErr,	isnull(AllDayNotAttndApr,0)AllDayNotAttndApr,	isnull(notAssignShiftYet,0)notAssignShiftYet,	isnull(other,0)other
from (
	select COUNT(*)cnt, [status],headerName from 
	(
		select 
		cal.IDNo,personInfo,header.firstName+' '+header.lastName headerName
		,cDate,dow,msg,totalDeduction ttd
		,isnull([absent],0.00)[absent]
		,isnull([leave],0.00)leave
		,isnull([brake],0.00)brake
		,isnull([lateAndEarly],0.00)lateAndEarly
		,isnull(errandLeave,0.00)errandLeave
		,isnull(sickLeave,0.00)sickLeave
		,isnull(notCheckInOrOut,0.00) notCheckInOrOut
		,soc
		,case 
			when msg='����' or msg='����,�ѹ��ش' then 'Normal'
			when msg like '%�͹��ѵ����ŧ���Ҿѡ%' then 'brakeApr'
			when msg like '%���ŧ���Ҿѡ����ŧ���Ҿѡ���ú%' then 'brakeErr'
			when msg like '%仾ѡ��͹���͡�Ѻ�ҡ�ѡ�ŷ%' then 'brakeLateOrEarly'
			when msg like '%������᡹���Ƿ���ѹ' then 'AllDayNotAttndApr'
			when msg like '%�͹��ѵ�����᡹���%' then 'notCheckInAttndApr'
			when msg like '%�͹��ѵ�����᡹�͡%' then 'notCheckOutAttndApr'
			when msg = '�Ҵ�ҹ����ѹ' then 'allDayAbsent'
			when msg = '�Ҿѡ��͹' then 'allDayVacation'
			when msg = '�ѧ������˹��Чҹ���' then 'notAssignShiftYet'
			when exists(select 1 from ta_leaveTypes lt where lt.name=msg and lt.[group]=1) then 'allDayErrandLeave'
			when msg = '�һ��� (�����) ��Ѻ�ͧᾷ��' then 'allDaySickLeaveNoCer'
			when msg = '�һ��� (��) ��Ѻ�ͧᾷ��' then 'allDaySickLeaveWithCer'
			when substring(msg,1,3) = '���' then 'checkinLate'
			else 'other'
		end [status]
		from #firstCalculation2 cal inner join hrm_humanoPersonAll h on cal.IDNo=h.IDNo and h.em_status in ('W','P') inner join hrm_commanders cmd on h.employeeCode=cmd.employeeCode and cmd.[rank]=1 inner join hrm_humanoPersonAll header on cmd.cEmpCode=header.employeeCode and header.em_status in ('W','P') 
		--where msg='�һ��� (��) ��Ѻ�ͧᾷ��'
	) m
	group by m.[status],headerName
)t pivot 
(
	max(cnt) for [status] in (notCheckOutAttndApr,notCheckInAttndApr,allDayVacation,	allDayAbsent,	other,	checkinLate,	Normal,	brakeLateOrEarly,	allDayErrandLeave,	allDaySickLeaveWithCer,	allDaySickLeaveNoCer,	brakeApr,	brakeErr,	AllDayNotAttndApr,	notAssignShiftYet)
) p
--select * from ta_leaveTypes
*/
--select distinct soc,msg from #firstCalculation2
