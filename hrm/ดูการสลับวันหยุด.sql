/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 hs.[id]
	,ehw.IDNo
      ,h.firstName,h.lastName
      ,[empHwId]
      ,[hwId]
      ,[status]
      ,[orginalDate]
      ,[substituteWith]
      ,[sSubstituteType]
      ,[dSubstituteType]
      ,[reasonWhy]
      ,[approved]
      ,[approvedBy]
      ,[approvedDate]
      ,[approverComments]
      ,[insertSince]
      ,[insertBy]
      ,[lastUpdate]
      ,[updateBy]
      ,[ipaddress]
     
  FROM [EMPBase].[dbo].[ta_hwSubstutions] hs left join ta_empHolidays ehw on hs.empHwId=ehw.eId
  left join hrm_humanoPersonAll h on ehw.IDNo=h.IDNo
  where orginalDate='20170920'