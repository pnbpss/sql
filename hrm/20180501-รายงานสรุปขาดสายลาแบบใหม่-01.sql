use EMPBase;
declare @fromdate datetime, @toDate datetime;
select @fromDate='20180101',@toDate='20180531';

select 
	uall.IDNo,typ msg,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)M 
	,COUNT(*) cnt
from (
		select IDNo,personInfo,cDate,dow,msg,ttd,absent,leave,brake,lateAndEarly,errandLeave,sickLeave,notCheckInOrOut,soc,'absent' typ
		from ta_lateAbsentStats 
		where 1=1
		and cDate between @fromDate and @toDate
		and
		(
			(
			msg like '%ขาดงาน%' 	
			)
	)
)uall
group by uall.IDNo,typ,'M_'+right('00'+convert(varchar(2),MONTH(cDate)),2)


