
/*
��ѭ�� ���������������� �������� ��㹤����͡������� ��㹤�����ѧ�Ѵ������� ��㹢����ž�ѡ�ҹ�Ѩ�غѹ�������
*/
use humano_Tangjai;
Select T1.em_code as em_code,T1.prefix_01 as prefix_01,T1.birthdate as birthdate,T1.name_01 as name_01,T1.sname_01 as sname_01,T1.work_start as work_start,CASE T4.initial_name_01 WHEN '' THEN T4.initial_name_02 ELSE 
ISNULL(T4.initial_name_01,T4.initial_name_02) END as em_type_name,T1.work_end as work_end,T1.id as id,T1.per_type as per_type,CASE T11.initial_name_01 WHEN '' THEN T11.initial_name_02 ELSE 
ISNULL(T11.initial_name_01,T11.initial_name_02) END as em_status_name,T1.nickname_01 as nickname_01,T1.idcard_no as idcard_no,T3.card_no as emcard_no,T1.em_status as em_status,T1.register_date as 
register_date,T1.idcard_type as idcard_type,T1.id_start_date as id_start_date,T1.id_expire_date as id_expire_date,T1.id_dest as id_dest,T5.province_name as id_prov_name,T1.gender as gender,T1.blood as 
blood,(YEAR(GETDATE()) - YEAR(birthdate)) as age,T1.height as height,T1.weight as weight,T1.nation as nation,T1.race as race,T1.religion as religion,T1.taxid as taxid,T1.marrystat as marrystat,T1.marryyear as marryyear,T1.marryprov as marryprov,T1.marrydest as marrydest,T1.tel as tel,T1.mobile as mobile,T1.email as email,T1.resign_date as resign_date,T1.period_no as period_no,T1.probation_days as probation_days,T1.probation_extend_days as probation_extend_days,T1.status as status,T1.prefix_02 as prefix_02 
FROM 
	person_data T1 left outer join initial_data T2 On T1.per_type = T2.id and T2.status=1 
	Left Outer Join em_card T3 On T1.emcard_id = T3.id 
	Left Outer Join initial_data T4 On T1.em_type = T4.initial_value_01 and T4.initial_system='personnel' and T4.initial_subsystem='Emp_Type'  
	Left Outer Join province T5 On T1.id_prov = T5.province_id and T5.status = 1 
	Left Outer Join per_position T6 on T6.person_id = T1.id and T6.status=1 and T6.position_status = 1 
	and T6.corp='O6K16BG00000043' Left outer join position T7 on T7.id = T6.position_id and T7.status=1 and T7.corp='O6K16BG00000043' Left Outer join station T8 On T8.id = T7.station_id  and T8.status=1 and T8.corp='O6K16BG00000043' Left Outer Join position_level T9 On T7.position_level=T9.id and T9.status=1 and T9.corp='O6K16BG00000043' Left Outer Join station T10 On T10.st_link=SUBSTRING(T8.st_link,1,3) and T10.status=1 and T10.corp='O6K16BG00000043' Left Outer Join initial_data T11 on T1.em_status=T11.initial_value_01 and T11.initial_system='personnel' and T11.initial_subsystem='Emp_status' and T11.status=1  
Where 1=1
and (T1.corp = 'O6K16BG00000043') 
and (T1.em_code Like 'TJ00000132%') --���ʾ�ѡ�ҹ
 and ( T1.status = 1) and (T1.per_type='1') 
and (T1.em_status IN ('W','P','RR')) 
and ((T6.position_id is null or T6.position_id='')) 
and (T1.corp='O6K16BG00000043') 
Order By T1.em_code ASC

--������� id �ͧ��ѡ�ҹ� table person_data ������ �������� sql ���仹��
--delete from per_position where person_id='H1K186900031848'