use truck;
			IF OBJECT_ID('tempdb..#ccltd') is not null DROP TABLE #ccltd
			IF OBJECT_ID('tempdb..#cntAvgStdev') is not null DROP TABLE #cntAvgStdev
			--goto processing
			IF OBJECT_ID('tempdb..#allIR') is not null DROP TABLE #allIR


			;with insRem (remReqId,remTreadDept,tinsTreadDept,serialNumber,removeDate,installDate,insReqId
			,vehicleId,rInsSince,iInsSince,tpdId,tpId,trow,tcolumn) as
			(
			select 
			tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
			,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
			,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
			,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
			from tr_tireRemovals tr inner join tr_tireInstallations ti
			on  tr.serialNumber=ti.serialNumber 
				and tr.seq=ti.seq
				and tr.installDate=ti.installDate
				and tr.vehicleId=ti.vehicleId
				and tr.tpdId=ti.tpdId
				and tr.tpId=ti.tpId
				and tr.trow=ti.trow
				and tr.tcolumn=ti.tcolumn
			where 1=1
			union all
			select 
			tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
			,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
			,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
			,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
			from tr_tireRemovals tr inner join tr_tireInstallations ti on  tr.serialNumber=ti.serialNumber and tr.seq=ti.seq and tr.installDate=ti.installDate and tr.vehicleId=ti.vehicleId and tr.tpdId=ti.tpdId and tr.tpId=ti.tpId and tr.trow=ti.trow and tr.tcolumn=ti.tcolumn 
			inner join insRem ir on tr.serialNumber=ir.serialNumber and tr.insSince=ir.iInsSince and tr.vehicleId=ir.vehicleId and tr.requestId=ir.insReqId and tr.removeDate=ir.installDate
			)
			select 			
			distinct
			ir.remReqId,ir.remTreadDept,ir.tinsTreadDept
			,ir.serialNumber,ir.removeDate,ir.installDate,ir.insReqId
			,ir.vehicleId,ir.rInsSince,ir.iInsSince
			,tpd.userCall,ir.tpdId,ir.tpId,ir.trow,ir.tcolumn
			into #allIR
			from insRem ir inner join tr_tirePositioningDetails tpd
			on ir.tpdId=tpd.id and ir.tpId=tpd.tpId and ir.trow=tpd.trow and ir.tcolumn=tpd.tcolumn
			order by insReqId;

			--select * from #allIR

			processing:
			select 
			allIR.insReqId
			,vi.plateNumber
			--,allIR.vehicleId,allIR.rInsSince
			--,allIR.iInsSince
			,allIR.tinsTreadDept
			,allIR.remReqId,allIR.remTreadDept,allIR.serialNumber
			,dbo.FN_ConvertDate(allIR.removeDate,'Y/M/D') removeDate
			,dbo.FN_ConvertDate(allIR.installDate,'Y/M/D') installDate
			,allIR.userCall wp
			--,allIR.tpdId,allIR.tpId,allIR.trow,allIR.tcolumn
			,ti.mileage insMileage
			,tr.mileage remMileage
			,(tr.mileage-ti.mileage) mileageDif
			,(allIR.tinsTreadDept-allIR.remTreadDept) tdDif
			,datediff(day,allIR.installDate,allIR.removeDate) dateDif
			,itd.treadDept itd,t.price initPrice 
			,cast(calc.bahtPerMill as decimal(18,1)) bahtPerMill
			,calc2.usedInBaht 
			,calc3.millPerKM
			,calc4.bahtPerKM
			,brnd.brand
			,br.abbreviation branch
			,cor.[description] causeOfRemove
			into #ccltd
			from #allIR allIR 
			left join tr_tireInstallations ti on allIR.insReqID=ti.requestId and allIR.serialNumber=ti.serialNumber
			left join tr_vehicles vi on  ti.vehicleId=vi.id
			left join tr_vehicleTypes vti on vi.vehicleTypeId=vti.id

			left join tr_tireRemovals tr on allIR.remReqID=tr.requestId and allIR.serialNumber=tr.serialNumber
			left join tr_vehicles vr on  tr.vehicleId=vr.id
			left join tr_vehicleTypes vtr on vr.vehicleTypeId=vtr.id 
			left join tr_causeOfRemove cor on tr.causeId=cor.id

			left join tr_tires t on allIR.serialNumber=t.serialNumber
			left join tr_initTireTreadDept itd on t.serialNumber=itd.serialNumber

			left join tr_brands brnd on t.brandId=brnd.id
			left join tr_requestForTireMaintenances rq on allIR.remReqID=rq.id
			left join tr_branches br on rq.branchId=br.id
			cross apply (select(cast(case when itd.treadDept=0 then 0 else (t.price/itd.treadDept) end as decimal(18,1)))bahtPerMill)calc
			cross apply (select(calc.bahtPerMill*(allIR.tinsTreadDept-allIR.remTreadDept))usedInBaht)calc2
			cross apply (select(cast(case when (tr.mileage-ti.mileage)=0 then 0 else (allIR.tinsTreadDept-allIR.remTreadDept)/(tr.mileage-ti.mileage) end as decimal(11,10)))millPerKM)calc3
			cross apply (select(cast(case when (tr.mileage-ti.mileage)=0 then 0 else (calc2.usedInBaht)/(tr.mileage-ti.mileage) end as decimal(15,10)))bahtPerKM)calc4
			where 1=1
			and ti.mileage is not null 
			and ti.mileage !=0 
			--and vti.typeCode in ('H','S') and vtr.typeCode in ('H','S')
			and tr.mileage is not null 
			and tr.mileage !=0 
			--and tr.mileage-ti.mileage>0
			and tr.mileage-ti.mileage between 2000 and 200000
			and bahtPerMill!=0 and bahtPerMill<1000
			and t.newOrRetread='N'
			and allIR.tinsTreadDept!=allIR.remTreadDept
			and datediff(day,allIR.installDate,allIR.removeDate) > 10
			and allIR.userCall not in ('SP1','SP2','SP2ห')
			and allIR.serialNumber not like 'TEST%'

/*

--select COUNT(*)cnt, remCauseDesc from tr_tireExplodedStats group by remCauseDesc
select COUNT(*)cnt,tes.remCauseDesc,tes.fleet,brnd.brand from tr_tireExplodedStats tes inner join tr_tires  t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id group by tes.remCauseDesc,tes.fleet,brnd.brand
*/
/*
select convert(decimal(20,2), mileageUsed*100/aggr.sumMile) cnt
,'เปอร์เซ็นต์ใช้' remCauseDesc
,case when allu.brand = '(n/a)' then 'N_A' else replace(allu.brand,' ','') end brand
from (select convert(decimal(20,4),SUM(mileageDif))mileageUsed,brand from #ccltd group by brand )allu 
cross apply (select convert(decimal(20,4),SUM(mileageDif))sumMile from #ccltd ) aggr
*/
declare @sumRem int; set @sumRem=(select COUNT(*) from tr_tireExplodedStats)
declare @sumRemBySwelling int; set @sumRemBySwelling=(select COUNT(*) from tr_tireExplodedStats where remCauseDesc='ยางบวม')
declare @sumRemByFlat int; set @sumRemByFlat=(select COUNT(*) from tr_tireExplodedStats where remCauseDesc='รั่ว')
declare @sumRemByExplode int; set @sumRemByExplode=(select COUNT(*) from tr_tireExplodedStats where remCauseDesc='ระเบิด')

select remCauseDesc,isnull([N_A],0)N_A,	isnull([ADVANCE],0)ADVANCE,	isnull([BlueStreak],0)BlueStreak,	isnull([Bridgestone],0)Bridgestone,	isnull([CHENGSHAN],0)CHENGSHAN,	isnull([CM335WESTLAKE],0)CM335WESTLAKE,	isnull([CMIC],0)CMIC,	isnull([continental],0)continental,	isnull([CST],0)CST,	isnull([Deestone],0)Deestone,	isnull([DoubleCoin],0)DoubleCoin,	isnull([DRC],0)DRC,	isnull([Dunlop],0)Dunlop,	isnull([Jinyu],0)Jinyu,	isnull([KINGSKY],0)KINGSKY,	isnull([linglong],0)linglong,	isnull([Michelin],0)Michelin,	isnull([MIRAGE],0)MIRAGE,	isnull([Otani],0)Otani,	isnull([Volvo],0)Volvo
from (
	select convert(decimal(20,2), mileageUsed*100/aggr.sumMile) cnt 	,'๚๚๚เปอร์เซ็นต์ใช้(%)' remCauseDesc 	,case when allu.brand = '(n/a)' then 'N_A' else replace(allu.brand,' ','') end brand from (select convert(decimal(20,4),SUM(mileageDif))mileageUsed,brand from #ccltd group by brand )allu cross apply (select convert(decimal(20,4),SUM(mileageDif))sumMile from #ccltd ) aggr
	union
	select COUNT(*)cnt,tes.remCauseDesc+'(เส้น)' remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id group by tes.remCauseDesc,brnd.brand
	union
	select COUNT(*)cnt,'๚๚รวมถอด(เส้น)'remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id group by brnd.brand
	union
	select cast((COUNT(*)*100.00)/@sumRem as decimal(5,2)) cnt,'๚รวมถอด(%)'remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id group by brnd.brand	
	union
	select cast((COUNT(*)*100.00)/@sumRemByExplode as decimal(5,2)) cnt,remCauseDesc+'(%)'remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id where remCauseDesc='ระเบิด' group by remCauseDesc,brnd.brand	
	union
	select cast((COUNT(*)*100.00)/@sumRemBySwelling as decimal(5,2)) cnt,remCauseDesc+'(%)'remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id where remCauseDesc='ยางบวม' group by remCauseDesc,brnd.brand	
	union
	select cast((COUNT(*)*100.00)/@sumRemByFlat as decimal(5,2)) cnt,remCauseDesc+'(%)'remCauseDesc,case when brnd.brand = '(n/a)' then 'N_A' else replace(brnd.brand,' ','') end brand from tr_tireExplodedStats tes inner join tr_tires t on tes.remSerialNumber=t.serialNumber inner join tr_brands brnd on t.brandId=brnd.id where remCauseDesc='รั่ว' group by remCauseDesc,brnd.brand	
	
)t pivot (sum(cnt) for brand in ([N_A],	[ADVANCE],	[BlueStreak],	[Bridgestone],	[CHENGSHAN],	[CM335WESTLAKE],	[CMIC],	[continental],	[CST],	[Deestone],	[DoubleCoin],	[DRC],	[Dunlop],	[Jinyu],	[KINGSKY],	[linglong],	[Michelin],	[MIRAGE],	[Otani],	[Volvo]))p
order by remCauseDesc



