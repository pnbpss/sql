--select * from tr_tireStockMovement where serialNumber in ('ivr21992u','ivr06698u') and operationOf='AD'
--update tr_tireStockMovement set treadDept='21' where serialNumber in ('ivr21992u','ivr06698u') and operationOf='AD'
use truck;
select trd.beforeTreadSN,trd.afterTreadSN
,dbo.FN_ConvertDate(trd.dateOfSendToRetread,'D/M/Y') as sentToTreadDate
,sp.name as supplierName
,t.size
,dbo.FN_ConvertDate(tsm.actionDate,'D/M/Y') as putInstockDate
,dbo.FN_ConvertDate(trd.dtbUR,'D/M/Y') as returnButCantRetreadDate
,br.name as fleetName
,t.treadPattern as treadPatternBeforeTread
,ta.treadPattern as treadPatternAfterTread
from tr_tireRetreading trd 
inner join tr_tires t on t.serialNumber=trd.beforeTreadSN
inner join tr_suppliers sp on trd.supplierId=sp.id
inner join tr_branches br on trd.branchId=br.id
left join tr_tires ta on trd.afterTreadSN=ta.serialNumber
left join tr_tireStockMovement tsm on (trd.afterTreadSN=tsm.serialNumber and ta.purchaseDate=tsm.actionDate and tsm.operationOf='AD')
where trd.beforeTreadSN='b1e2j4821'
order by trd.dateOfSendToRetread, br.name
--where t.serialNumber like 'TEST%'
--select * from tr_tireRemovals where requestId='2520' and serialNumber='XVO51858H'
--update tr_tireRemovals set expense='100' where requestId='2520' and serialNumber='XVO51858H'