--create tr_tireRemInsMovements as 
--�Դ������ҧ����
declare @requestId int;
--set @requestId = '1294';
set @requestId = '1315';
select ti.requestId,ti.installDate as movementDate,ti.vehicleId as insVehicleId, ti.serialNumber as insSerialNumber,ti.treadDept as iTreadDept,itpd.userCall as insWheel
,null as remVehicle, null as remSerialNumber,null as rTreaDept,null remWheel, '1' as movementCase,ti.insSince
--,'#' as sp, tr.*
from
((
tr_tireInstallations ti 
inner join tr_tirePositioningDetails itpd on 
ti.tpdId=itpd.id and 
ti.tpId=itpd.tpId and 
ti.trow=itpd.trow and 
ti.tcolumn=itpd.tcolumn
)
left join tr_tireRemovals tr on --����������蹶١�ʹ�͡仨ҡ���˹觷��Դ���㹤���ͧ���ǡѹ
ti.requestId=tr.requestId
and ti.insSince=tr.insSince 
and ti.installDate=tr.removeDate
and ti.serialNumber<>tr.serialNumber
and ti.vehicleId=tr.vehicleId
and ti.tpdId=tr.tpdId
and ti.tpId=tr.tpId
and ti.trow=tr.trow
and ti.tcolumn=tr.tcolumn)
left join tr_tireRemovals trb on --�����ʹ�͡�Ҩҡ�ö�ѹ�˹���������˹�㹤���ͧ���ǡѹ
ti.requestId=trb.requestId
and ti.serialNumber=trb.serialNumber
and ti.insSince=trb.insSince
and ti.installDate=trb.removeDate 
/*and ti.tpdId=trb.tpdId
and ti.tpId=trb.tpId
and ti.trow=trb.trow
and ti.tcolumn=trb.tcolumn
*/
where tr.serialNumber is null and trb.serialNumber is null
	 and ti.requestId=@requestId
--union
--�ʹ���ҧ����
select
tr.requestId,tr.removeDate as movementDate,tia.vehicleId as insVehicleId, tia.serialNumber as insSerialNumber,null as iTreadDept,null as insWheel
,tr.vehicleId as remVehicle, tr.serialNumber as remSerialNumber,tr.treadDept as rTreadDept,rtpd.userCall remWheel, '2' as movementCase,tr.insSince
--,tia.serialNumber,'#'as sp,tr.insSince,tic.*
from(
tr_tireInstallations ti 
inner join tr_tireRemovals tr on 
	ti.serialNumber=tr.serialNumber and
	ti.insSince<tr.insSince and 
	ti.vehicleId=tr.vehicleId and 
	ti.installDate=tr.installDate and
	ti.tpdId=tr.tpdId and
	ti.tpId=tr.tpId and
	ti.trow=tr.trow and
	ti.tcolumn=tr.tcolumn
inner join tr_tirePositioningDetails rtpd on tr.tpdId=rtpd.id and tr.tpId=rtpd.tpId and tr.trow=rtpd.trow and tr.tcolumn=rtpd.tcolumn
)left join tr_tireInstallations tia on
	tia.serialNumber=tr.serialNumber and
	tia.requestId=tr.requestId and
	tia.insSince=tr.insSince and 
	--tia.vehicleId=tr.vehicleId and 
	tia.installDate=tr.removeDate
left join tr_tireInstallations tic on 
	tic.serialNumber<>tr.serialNumber and
	tic.insSince=tr.insSince and 
	tic.vehicleId=tr.vehicleId and 
	tic.installDate=tr.removeDate and
	tic.tpdId=tr.tpdId and
	tic.tpId=tr.tpId and
	tic.trow=tr.trow and
	tic.tcolumn=tr.tcolumn
where tia.insSince is null and tic.serialNumber is null
	 and ti.requestId=@requestId
--union
--����¹�ҧ
select
tr.requestId,tr.removeDate as movementDate,tic.vehicleId as insVehicleId, tic.serialNumber as insSerialNumber,tic.treadDept as iTreadDept,rtpd.userCall as insWheel
,tr.vehicleId as remVehicle, tr.serialNumber as remSerialNumber,tr.treadDept as rTreadDept,rtpd.userCall remWheel, '3' as movementCase,ti.insSince
--,tia.serialNumber,'#'as sp,tr.insSince,tic.*
from(
tr_tireInstallations ti 
inner join tr_tireRemovals tr on --�ա�õԴ�����жʹ�ҧ��鹷���������ö㹵��˹� x
	ti.serialNumber<>tr.serialNumber and
	ti.insSince=tr.insSince and 
	ti.vehicleId=tr.vehicleId and 
	ti.installDate=tr.removeDate and
	ti.tpdId=tr.tpdId and
	ti.tpId=tr.tpId and
	ti.trow=tr.trow and
	ti.tcolumn=tr.tcolumn
inner join tr_tirePositioningDetails rtpd on tr.tpdId=rtpd.id and tr.tpId=rtpd.tpId and tr.trow=rtpd.trow and tr.tcolumn=rtpd.tcolumn
)left join tr_tireInstallations tia on --�ҧ��鹷�������������������㹤���ͧ����ҡ�͹��觶١�ʹ�͡���� insSince ���
	tia.serialNumber=tr.serialNumber and
	tia.requestId=tr.requestId and
	tia.insSince=tr.insSince	
	--tia.vehicleId=tr.vehicleId and 
	and tia.installDate=tr.removeDate	
inner join tr_tireInstallations tic on --�ա�õԴ����ҧ������ŧ�㹵��˹�������ǡѹ�Ѻ��鹷��ʹ
	tic.serialNumber<>tr.serialNumber and
	tic.requestId=tr.requestId and
	tic.insSince=tr.insSince and 
	tic.vehicleId=tr.vehicleId and 
	tic.installDate=tr.removeDate and
	tic.tpdId=tr.tpdId and
	tic.tpId=tr.tpId and
	tic.trow=tr.trow and
	tic.tcolumn=tr.tcolumn
where tia.serialNumber is null
	and ti.requestId=@requestId
--union
--���µ��˹��ҧ
select
tr.requestId,tr.removeDate as movementDate,tia.vehicleId as insVehicleId, tia.serialNumber as insSerialNumber,tia.treadDept as iTreadDept,tpda.userCall as insWheel
,tr.vehicleId as remVehicle, tr.serialNumber as remSerialNumber,tr.treadDept as rTreadDept,rtpd.userCall remWheel, '4' as movementCase,tia.insSince
--,tia.serialNumber,'#'as sp,tr.insSince,tic.*
from
tr_tireRemovals tr --�ա�öʹ�ҧ�͡
inner join tr_tirePositioningDetails rtpd on tr.tpdId=rtpd.id and tr.tpId=rtpd.tpId and tr.trow=rtpd.trow and tr.tcolumn=rtpd.tcolumn
inner join tr_tireInstallations tia on --�ա�õԴ����ҧ��鹹��㹹��˹������� ����ö�ѹ��� ��ѧ�ҡ�ʹ�����
	tia.serialNumber=tr.serialNumber 
	and tia.requestId=tr.requestId
	and	tia.insSince=tr.insSince
	--and tia.installDate=tr.removeDate
	inner join tr_tirePositioningDetails tpda on tia.tpdId=tpda.id and tia.tpId=tpda.tpId and tia.trow=tpda.trow and tia.tcolumn=tpda.tcolumn
where tia.insSince is not null
	and tr.requestId=@requestId
--union
--����ҧ������� ��жʹ�ҧ������㹵��˹觷�����仵��˹����
select
tr.requestId,tr.removeDate as movementDate,tin.vehicleId as insVehicleId, tin.serialNumber as insSerialNumber,tin.treadDept as iTreadDept,tpda.userCall as insWheel
,tr.vehicleId as remVehicle, tr.serialNumber as remSerialNumber,tr.treadDept as rTreadDept,rtpd.userCall remWheel, '5' as movementCase,tia.insSince
--,tia.serialNumber,'#'as sp,tr.insSince,tic.*
from(
tr_tireRemovals tr --
inner join tr_tirePositioningDetails rtpd on tr.tpdId=rtpd.id and tr.tpId=rtpd.tpId and tr.trow=rtpd.trow and tr.tcolumn=rtpd.tcolumn
inner join tr_tireInstallations tia on --�ա�öʹ�ҧ�͡ ��� �ա�õԴ����ҧ��鹹��㹹��˹������� ����ö�ѹ��� ��ѧ�ҡ�ʹ�����
	tia.serialNumber=tr.serialNumber 
	and tia.requestId=tr.requestId
	and	tia.insSince=tr.insSince
	--and tia.installDate=tr.removeDate
inner join tr_tirePositioningDetails tpda on tia.tpdId=tpda.id and tia.tpId=tpda.tpId and tia.trow=tpda.trow and tia.tcolumn=tpda.tcolumn
inner join --�ա�õԴ����ҧ�������ҡʵ�ͤŧ㹵��˹���͹��
		tr_tireInstallations tin on
		tr.requestId=tin.requestId
		and tr.vehicleId=tin.vehicleId
		and tr.serialNumber<>tin.serialNumber
		and tr.insSince=tin.insSince
		and tr.removeDate=tin.installDate
		and tr.tpdId=tin.tpdId
		and tr.tpId=tin.tpId
		and tr.trow=tin.trow
		and tr.tcolumn=tin.tcolumn
		)
			left join tr_tireRemovals trn on 
			tin.serialNumber=trn.serialNumber and
			tin.requestId=trn.requestId and
			tin.insSince=trn.insSince and 
			tin.vehicleId=trn.vehicleId and 
			tin.installDate=trn.removeDate and
			tin.tpdId=trn.tpdId and
			tin.tpId=trn.tpId and
			tin.trow=trn.trow and
			tin.tcolumn=trn.tcolumn

where tin.serialNumber is not null and trn.serialNumber is null and tr.requestId=@requestId