--drop table #tempTb
use truck;
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
IF OBJECT_ID('tempdb.dbo.#tempTb2', 'U') IS NOT NULL DROP TABLE #tempTb2;
select 
cti.plateNumber,lower(replace(brnd.brand,' ','_'))brand,vt.typeCode,vbranch.abbreviation branch,t.serialNumber,cti.userCall
,case when fetchFrom.stockType='�ҧ����' then 'NEW' when fetchFrom.stockType='�ҧ���ͧ' then 'SPR' else 'OTH' end fetchFrom
into #tempTb
from tr_currentTireInstallationInfos cti 
left join tr_tires t on cti.serialNumber=t.serialNumber
left join tr_brands brnd on t.brandId=brnd.id
left join tr_vehicles v on cti.vehicleId=v.id
left join tr_branches vbranch on v.branchId=vbranch.id
left join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
cross apply (select stockType from tr_tireStockTypes where id=dbo.fn_getTireFromStock4SubStitute(cti.serialNumber,cti.installDate))fetchFrom
where 1=1
--and vbranch.abbreviation='LP'
--and vt.typeCode='S'

select vehicleInfo,
aggr.sumNew+aggr.sumSPR+aggr.sumOTH sumAll,
aggr.sumNew,aggr.sumSPR,aggr.sumOTH,
isnull([michelin_NEW],0)michelin_NEW,	isnull([michelin_SPR],0)michelin_SPR,	isnull([michelin_OTH],0)michelin_OTH,isnull([drc_NEW],0)drc_NEW,	isnull([drc_SPR],0)drc_SPR,	isnull([drc_OTH],0)drc_OTH,isnull([goodyear_NEW],0)goodyear_NEW,	isnull([goodyear_SPR],0)goodyear_SPR,	isnull([goodyear_OTH],0)goodyear_OTH,
isnull([linglong_NEW],0)linglong_NEW,	isnull([linglong_SPR],0)linglong_SPR,	isnull([linglong_OTH],0)linglong_OTH,isnull([double_coin_NEW],0)double_coin_NEW,	isnull([double_coin_SPR],0)double_coin_SPR,	isnull([double_coin_OTH],0)double_coin_OTH,isnull([king_sky_NEW],0)king_sky_NEW,	isnull([king_sky_SPR],0)king_sky_SPR,	isnull([king_sky_OTH],0)king_sky_OTH,
isnull([chengshan_NEW],0)chengshan_NEW,	isnull([chengshan_SPR],0)chengshan_SPR,	isnull([chengshan_OTH],0)chengshan_OTH,isnull([dunlop_NEW],0)dunlop_NEW,	isnull([dunlop_SPR],0)dunlop_SPR,	isnull([dunlop_OTH],0)dunlop_OTH,isnull([bridgestone_NEW],0)bridgestone_NEW,	isnull([bridgestone_SPR],0)bridgestone_SPR,	isnull([bridgestone_OTH],0)bridgestone_OTH,
isnull([cst_NEW],0)cst_NEW,	isnull([cst_SPR],0)cst_SPR,	isnull([cst_OTH],0)cst_OTH,isnull([yokohama_NEW],0)yokohama_NEW,	isnull([yokohama_SPR],0)yokohama_SPR,	isnull([yokohama_OTH],0)yokohama_OTH,isnull([maxxis_NEW],0)maxxis_NEW,	isnull([maxxis_SPR],0)maxxis_SPR,	isnull([maxxis_OTH],0)maxxis_OTH,
isnull([advance_NEW],0)advance_NEW,	isnull([advance_SPR],0)advance_SPR,	isnull([advance_OTH],0)advance_OTH,isnull([bluestreak_NEW],0)bluestreak_NEW,	isnull([bluestreak_SPR],0)bluestreak_SPR,	isnull([bluestreak_OTH],0)bluestreak_OTH,isnull([jinyu_NEW],0)jinyu_NEW,	isnull([jinyu_SPR],0)jinyu_SPR,	isnull([jinyu_OTH],0)jinyu_OTH,
isnull([deestone_NEW],0)deestone_NEW,	isnull([deestone_SPR],0)deestone_SPR,	isnull([deestone_OTH],0)deestone_OTH,isnull([mirage_NEW],0)mirage_NEW,	isnull([mirage_SPR],0)mirage_SPR,	isnull([mirage_OTH],0)mirage_OTH,isnull([cm335_westlake_NEW],0)cm335_westlake_NEW,	isnull([cm335_westlake_SPR],0)cm335_westlake_SPR,	isnull([cm335_westlake_OTH],0)cm335_westlake_OTH,
isnull([continental_NEW],0)continental_NEW,	isnull([continental_SPR],0)continental_SPR,	isnull([continental_OTH],0)continental_OTH,isnull([otani_NEW],0)otani_NEW,	isnull([otani_SPR],0)otani_SPR,	isnull([otani_OTH],0)otani_OTH,isnull([Other_NEW],0)Other_NEW,	isnull([Other_SPR],0)Other_SPR,	isnull([Other_OTH],0)Other_OTH
into #tempTb2
from (
	select  vehicleInfo,brndFtch,COUNT(*)cnt
	from (
		select 
		branch+'_'+typeCode+'_'+plateNumber vehicleInfo,
		case 
			when brand='michelin' then brand when brand='drc' then brand when brand='goodyear' then brand when brand='linglong' then brand when brand='double_coin' then brand when brand='king_sky' then brand
			when brand='chengshan' then brand when brand='dunlop' then brand when brand='bridgestone' then brand when brand='cst' then brand when brand='yokohama' then brand when brand='maxxis' then brand
			when brand='advance' then brand when brand='bluestreak' then brand when brand='jinyu' then brand when brand='deestone' then brand when brand='mirage' then brand when brand='cm335_westlake' then brand
			when brand='continental' then brand when brand='otani' then brand else 'Other'
		end +'_'+fetchFrom brndFtch
		--,branch
		,serialNumber,userCall  
		from #tempTb
	)agr1
	group by vehicleInfo,brndFtch
)t pivot (
	sum(cnt) for brndFtch in (
		[michelin_NEW],	[michelin_SPR],	[michelin_OTH],	[drc_NEW],	[drc_SPR],	[drc_OTH],[goodyear_NEW],	[goodyear_SPR],	[goodyear_OTH],[linglong_NEW],	[linglong_SPR],	[linglong_OTH],[double_coin_NEW],	[double_coin_SPR],	[double_coin_OTH],
		[king_sky_NEW],	[king_sky_SPR],	[king_sky_OTH],	[chengshan_NEW],	[chengshan_SPR],	[chengshan_OTH],		[dunlop_NEW],	[dunlop_SPR],	[dunlop_OTH],		[bridgestone_NEW],	[bridgestone_SPR],	[bridgestone_OTH],		[cst_NEW],	[cst_SPR],	[cst_OTH],
		[yokohama_NEW],	[yokohama_SPR],	[yokohama_OTH],		[maxxis_NEW],	[maxxis_SPR],	[maxxis_OTH],		[advance_NEW],	[advance_SPR],	[advance_OTH],		[bluestreak_NEW],	[bluestreak_SPR],	[bluestreak_OTH],		[jinyu_NEW],	[jinyu_SPR],	[jinyu_OTH],
		[deestone_NEW],	[deestone_SPR],	[deestone_OTH],		[mirage_NEW],	[mirage_SPR],	[mirage_OTH],		[cm335_westlake_NEW],	[cm335_westlake_SPR],	[cm335_westlake_OTH],		[continental_NEW],	[continental_SPR],	[continental_OTH],		[otani_NEW],	[otani_SPR],	[otani_OTH],
		[Other_NEW],	[Other_SPR],	[Other_OTH]		)
) p
cross apply (
					select isnull([michelin_NEW],0)+isnull([drc_NEW],0)+isnull([goodyear_NEW],0)+isnull([linglong_NEW],0)+isnull([double_coin_NEW],0)+isnull([king_sky_NEW],0)+isnull([chengshan_NEW],0)+isnull([dunlop_NEW],0)+isnull([bridgestone_NEW],0)+isnull([cst_NEW],0)+isnull([yokohama_NEW],0)+isnull([maxxis_NEW],0)+isnull([advance_NEW],0)+isnull([bluestreak_NEW],0)+isnull([jinyu_NEW],0)+isnull([deestone_NEW],0)+isnull([mirage_NEW],0)+isnull([cm335_westlake_NEW],0)+isnull([continental_NEW],0)+isnull([otani_NEW],0)+isnull([Other_NEW],0) sumNew
					,isnull([michelin_SPR],0)+isnull([drc_SPR],0)+isnull([goodyear_SPR],0)+isnull([linglong_SPR],0)+isnull([double_coin_SPR],0)+isnull([king_sky_SPR],0)+isnull([chengshan_SPR],0)+isnull([dunlop_SPR],0)+isnull([bridgestone_SPR],0)+isnull([cst_SPR],0)+isnull([yokohama_SPR],0)+isnull([maxxis_SPR],0)+isnull([advance_SPR],0)+isnull([bluestreak_SPR],0)+isnull([jinyu_SPR],0)+isnull([deestone_SPR],0)+isnull([mirage_SPR],0)+isnull([cm335_westlake_SPR],0)+isnull([continental_SPR],0)+isnull([otani_SPR],0)+isnull([Other_SPR],0) sumSPR
					,isnull([michelin_OTH],0)+isnull([drc_OTH],0)+isnull([goodyear_OTH],0)+isnull([linglong_OTH],0)+isnull([double_coin_OTH],0)+isnull([king_sky_OTH],0)+isnull([chengshan_OTH],0)+isnull([dunlop_OTH],0)+isnull([bridgestone_OTH],0)+isnull([cst_OTH],0)+isnull([yokohama_OTH],0)+isnull([maxxis_OTH],0)+isnull([advance_OTH],0)+isnull([bluestreak_OTH],0)+isnull([jinyu_OTH],0)+isnull([deestone_OTH],0)+isnull([mirage_OTH],0)+isnull([cm335_westlake_OTH],0)+isnull([continental_OTH],0)+isnull([otani_OTH],0)+isnull([Other_OTH],0) sumOTH
				)aggr
order by vehicleInfo;


--display
select  substring(vehicleInfo,1,2)fleet,substring(vehicleInfo,4,1)HT,substring(vehicleInfo,6,200)plateNumber,	sumAll,	sumNew,	sumSPR,	sumOTH,	michelin_NEW,	michelin_SPR,	michelin_OTH,	drc_NEW,	drc_SPR,	drc_OTH,	goodyear_NEW,	goodyear_SPR,	goodyear_OTH,	linglong_NEW,	linglong_SPR,	linglong_OTH,	double_coin_NEW,	double_coin_SPR,	double_coin_OTH,	king_sky_NEW,	king_sky_SPR,	king_sky_OTH,	chengshan_NEW,	chengshan_SPR,	chengshan_OTH,	dunlop_NEW,	dunlop_SPR,	dunlop_OTH,	bridgestone_NEW,	bridgestone_SPR,	bridgestone_OTH,	cst_NEW,	cst_SPR,	cst_OTH,	yokohama_NEW,	yokohama_SPR,	yokohama_OTH,	maxxis_NEW,	maxxis_SPR,	maxxis_OTH,	advance_NEW,	advance_SPR,	advance_OTH,	bluestreak_NEW,	bluestreak_SPR,	bluestreak_OTH,	jinyu_NEW,	jinyu_SPR,	jinyu_OTH,	deestone_NEW,	deestone_SPR,	deestone_OTH,	mirage_NEW,	mirage_SPR,	mirage_OTH,	cm335_westlake_NEW,	cm335_westlake_SPR,	cm335_westlake_OTH,	continental_NEW,	continental_SPR,	continental_OTH,	otani_NEW,	otani_SPR,	otani_OTH,	Other_NEW,	Other_SPR,	Other_OTH from #tempTb2
union
select '�'fleet,''HT,'���' plateNumber, sum(sumAll)sumAll,	sum(sumNew)sumNew,	sum(sumSPR)sumSPR,	sum(sumOTH)sumOTH,	sum(michelin_NEW)michelin_NEW,	sum(michelin_SPR)michelin_SPR,	sum(michelin_OTH)michelin_OTH,	sum(drc_NEW)drc_NEW,	sum(drc_SPR)drc_SPR,	sum(drc_OTH)drc_OTH,	sum(goodyear_NEW)goodyear_NEW,	sum(goodyear_SPR)goodyear_SPR,	sum(goodyear_OTH)goodyear_OTH,	sum(linglong_NEW)linglong_NEW,	sum(linglong_SPR)linglong_SPR,	sum(linglong_OTH)linglong_OTH,	sum(double_coin_NEW)double_coin_NEW,	sum(double_coin_SPR)double_coin_SPR,	sum(double_coin_OTH)double_coin_OTH,	sum(king_sky_NEW)king_sky_NEW,	sum(king_sky_SPR)king_sky_SPR,	sum(king_sky_OTH)king_sky_OTH,	sum(chengshan_NEW)chengshan_NEW,	sum(chengshan_SPR)chengshan_SPR,	sum(chengshan_OTH)chengshan_OTH,	sum(dunlop_NEW)dunlop_NEW,	sum(dunlop_SPR)dunlop_SPR,	sum(dunlop_OTH)dunlop_OTH,	sum(bridgestone_NEW)bridgestone_NEW,	sum(bridgestone_SPR)bridgestone_SPR,	sum(bridgestone_OTH)bridgestone_OTH,	sum(cst_NEW)cst_NEW,	sum(cst_SPR)cst_SPR,	sum(cst_OTH)cst_OTH,	sum(yokohama_NEW)yokohama_NEW,	sum(yokohama_SPR)yokohama_SPR,	sum(yokohama_OTH)yokohama_OTH,	sum(maxxis_NEW)maxxis_NEW,	sum(maxxis_SPR)maxxis_SPR,	sum(maxxis_OTH)maxxis_OTH,	sum(advance_NEW)advance_NEW,	sum(advance_SPR)advance_SPR,	sum(advance_OTH)advance_OTH,	sum(bluestreak_NEW)bluestreak_NEW,	sum(bluestreak_SPR)bluestreak_SPR,	sum(bluestreak_OTH)bluestreak_OTH,	sum(jinyu_NEW)jinyu_NEW,	sum(jinyu_SPR)jinyu_SPR,	sum(jinyu_OTH)jinyu_OTH,	sum(deestone_NEW)deestone_NEW,	sum(deestone_SPR)deestone_SPR,	sum(deestone_OTH)deestone_OTH,	sum(mirage_NEW)mirage_NEW,	sum(mirage_SPR)mirage_SPR,	sum(mirage_OTH)mirage_OTH,	sum(cm335_westlake_NEW)cm335_westlake_NEW,	sum(cm335_westlake_SPR)cm335_westlake_SPR,	sum(cm335_westlake_OTH)cm335_westlake_OTH,	sum(continental_NEW)continental_NEW,	sum(continental_SPR)continental_SPR,	sum(continental_OTH)continental_OTH,	sum(otani_NEW)otani_NEW,	sum(otani_SPR)otani_SPR,	sum(otani_OTH)otani_OTH,	sum(Other_NEW)Other_NEW,	sum(Other_SPR)Other_SPR,	sum(Other_OTH)Other_OTH from #tempTb2
order by fleet


