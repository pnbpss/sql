/****** Script for SelectTopNRows command from SSMS  ******/
use truck;
SELECT m.[id]
      ,dbo.FN_convertDate(m.[createDate],'D/M/Y') [createFormDate]
      --,m.[transferFrom]
      --,m.[transferTo]
      ,m.[received]
      --,m.[receivedBy]
      ---,m.[receivedDate]
      --,m.[createBy]
      ,m.[approved]
      --,m.[approvedBy]
      ,dbo.FN_convertDate(m.[approvedDate],'D/M/Y') [approveFormDate]
      ,m.[signedShipper]
      --,m.[signedReceiver]
      ,m.[status]
      ,case 
		when m.[status]='notApproved' then 'ไม่อนุมัติ'
		when m.[status]='sending' then 'กำลังส่ง'
		when m.[status]='initial' then 'เริ่มขอ'
		when m.[status]='received' then 'รับโอนแล้ว'
		else 'ไม่โร้'
      end thStatus
      ,m.[creatorComments]
      ,m.[approverComments]
      ,d.serialNumber
      ,bf.name as fromBranchName
      ,bt.name as toBranchName
      ,cu.name as createFormBy
      ,au.name as approvedBy
  FROM tr_transferTireShipments m
  inner join tr_transferTireShipmentDetails d on m.id=d.formId
  inner join tr_branches bf on m.transferFrom=bf.id
  inner join tr_branches bt on m.transferTo=bt.id
  inner join tr_users cu on m.createBy=cu.id
  left join tr_users au on m.approvedBy=au.id
  --where m.[status] !='received'
  order by m.id