/****** Script for SelectTopNRows command from SSMS  ******/
/*
SELECT TOP 1000 [id]
      ,[createDate]
      ,[transferFrom]
      ,[transferTo]
      ,[received]
      ,[receviedBy]
      ,[receivedDate]
      ,[createBy]
      ,[approved]
      ,[aporovedBy]
      ,[approvedDate]
      ,[signedShipper]
      ,[signedReceiver]
      ,[status]
      ,[creatorComments]
      ,[approverComments]
  FROM [truck].[dbo].[tr_transferTireShipments]
  */
  --select * from tr_tires;
  use truck;
  select dup1.typeDup
  ,dup1.sn1,br1.name br1,isnull(cti1.plateNumber+'('+cti1.userCall+')','') as pn1,cts1.stockType st1,tc1.condition cond1,t1.treadPattern tp1, t1.updateInformation ui1
  ,dup1.sp
  ,dup1.sn2,br2.name br2,isnull(cti2.plateNumber+'('+cti2.userCall+')','') as pn2,cts2.stockType st2,tc2.condition cond2,t2.treadPattern tp2, t2.updateInformation ui2
  from  dbo.findDupplicateTireSuspect() dup1
  inner join tr_tires t1 on dup1.sn1=t1.serialNumber inner join tr_branches br1 on t1.branchId=br1.id 
  inner join tr_tireConditions tc1 on t1.conditionId=tc1.id
  inner join tr_tires t2 on dup1.sn2=t2.serialNumber inner join tr_branches br2 on t2.branchId=br2.id  
  inner join tr_tireConditions tc2 on t2.conditionId=tc2.id
  left join tr_currentTireInstallationInfos cti1 on dup1.sn1=cti1.serialNumber
  left join tr_currentTireInstallationInfos cti2 on dup1.sn2=cti2.serialNumber
  left join tr_currentTireStock cts1 on dup1.sn1=cts1.serialNumber
  left join tr_currentTireStock cts2 on dup1.sn2=cts2.serialNumber
  where 1=1
	--and br2.id<>br2.id
	--and dup1.pairType='NN'
	and dup1.pairType<>'NN'
  order by typeDup desc;