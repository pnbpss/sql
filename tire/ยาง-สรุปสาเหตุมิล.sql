/****** Script for SelectTopNRows command from SSMS  ******/
/*
SELECT TOP 1000 [removedSerialNumber]
      ,[removedDate]
      ,[description]
      ,[branchId]
      ,[branchName]
      ,[remCauseId]
      ,[treadDept]
      ,[treadPattern]
      ,[removedTreadDept]
  FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
*/
declare @month varchar(max);
set @month='9'
select b.name as branchName 
,atte.amountOfTireTreadExhuasted
,sotr.sumOfTreadRemain
,ate.amountOfTireExploded
,strae.sumOfTreadRemainAfterExploded
,sotr.sumOfTreadRemain / atte.amountOfTireTreadExhuasted as avgSotr
,strae.sumOfTreadRemainAfterExploded / ate.amountOfTireExploded as avgSte
from tr_branches b left join (
	select count(removedSerialNumber) amountOfTireTreadExhuasted,branchName
	FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
	where remCauseId='1' and month([removedDate])=@month
	group by branchName
) as atte on b.name=atte.branchname left join
(
	select sum(removedTreadDept) sumOfTreadRemain,branchName
	FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
	where remCauseId='1' and month([removedDate])=@month
	group by branchName
) as sotr on b.name=sotr.branchName left join
(
	select count(removedSerialNumber) amountOfTireExploded,branchName
	FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
	where remCauseId='4' and month([removedDate])=@month
	group by branchName
) as ate on b.name=ate.branchName left join
(
	select sum(removedTreadDept) sumOfTreadRemainAfterExploded,branchName
	FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
	where remCauseId='4' and month([removedDate])=@month
	group by branchName
) as strae on b.name=strae.branchName