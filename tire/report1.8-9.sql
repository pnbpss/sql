select v.plateNumber,vt.typeCode,b.name as branchName
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,1,'2014') as dateOfSubsInM01	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,1,'2014') as dateOfAuditInM01
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,2,'2014') as dateOfSubsInM02	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,2,'2014') as dateOfAuditInM02
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,3,'2014') as dateOfSubsInM03	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,3,'2014') as dateOfAuditInM03
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,4,'2014') as dateOfSubsInM04	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,4,'2014') as dateOfAuditInM04
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,5,'2014') as dateOfSubsInM05	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,5,'2014') as dateOfAuditInM05
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,6,'2014') as dateOfSubsInM06	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,6,'2014') as dateOfAuditInM06
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,7,'2014') as dateOfSubsInM07	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,7,'2014') as dateOfAuditInM07
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,8,'2014') as dateOfSubsInM08	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,8,'2014') as dateOfAuditInM08
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,9,'2014') as dateOfSubsInM09	
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,9,'2014') as dateOfAuditInM09
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,10,'2014') as dateOfSubsInM10
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,10,'2014') as dateOfAuditInM10
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,11,'2014') as dateOfSubsInM11
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,11,'2014') as dateOfAuditInM11
	,dbo.FN_substituteDateOfVehicleBetweenDates(v.id,12,'2014') as dateOfSubsInM12
	,dbo.FN_auditedDateOfVehicleBetweenDates(v.id,12,'2014') as dateOfAuditInM12	
from tr_vehicles v inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	  inner join tr_branches b on v.branchId=b.id
where b.id=1
order by b.name, vt.typeCode