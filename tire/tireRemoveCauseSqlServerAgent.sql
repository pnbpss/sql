USE [msdb]
GO

/****** Object:  Job [getTireRemoveCauseStat]    Script Date: 06/18/2018 11:38:00 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 06/18/2018 11:38:00 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'getTireRemoveCauseStat', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'�֧�������ҧ���١�ʹ�͡ �ҡ������Դ ��д֧������ SMS �ͧ��', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'dasiuser', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [doIt]    Script Date: 06/18/2018 11:38:00 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'doIt', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--��Ŵ���˵ء�öʹ�ҧ
use truck;
IF OBJECT_ID(''truck.dbo.tr_tireExplodedStats'', ''U'') IS NOT NULL DROP TABLE tr_tireExplodedStats;
declare @startDate datetime,@endDate datetime;
declare @causeOfRemove table (causeOfRemove varchar(max));
set @startDate=''2009/12/01'';set @endDate=dateadd(day,1,getdate());

select remPlateNumber,rTreadDept,remSerialNumber,allunion.branchId,remWheel,movementDate,t.price,itd.treadDept initTreadDept,remCauseDesc
,aggr1.remainValue,aggr2.usedPrice,brnd.brand,brnch.abbreviation fleet,GETDATE() lastRunDate
into tr_tireExplodedStats
from (
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate,remCauseDesc from tr_case2Scheema where movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate,remCauseDesc from tr_case3Scheema where movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate,remCauseDesc from tr_case4Scheema where movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate,remCauseDesc from tr_case8Scheema where movementDate between @startDate and @endDate
) allunion left join tr_tires t on allunion.remSerialNumber=t.serialNumber left join tr_initTireTreadDept itd on allunion.remSerialNumber=itd.serialNumber 
left join tr_brands brnd on t.brandId=brnd.id
left join tr_branches brnch on allunion.branchId=brnch.id
cross apply (select  case when itd.treadDept!=0 then  convert( decimal(18,2), (t.price/itd.treadDept)*rTreadDept) 	else 0 end remainValue)aggr1
cross apply (select t.price-aggr1.remainValue usedPrice)aggr2

', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'getTireExploedStats', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170307, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'17eb615d-ace7-4dbb-ae11-f3fa859129d6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


