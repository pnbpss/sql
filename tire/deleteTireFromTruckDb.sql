use truck;
declare @serialNumber varchar(max);
set @serialNumber ='0B1118C12';

select 'tr_tires',* from tr_tires where serialNumber=@serialNumber;
select 'tr_tireStockMovement',* from tr_tireStockMovement where serialNumber=@serialNumber;
select 'tr_tireAuditingFormDetails',*  from tr_tireAuditingFormDetails where serialNumber=@serialNumber;
select 'tr_tireRemovals',*  from tr_tireRemovals where serialNumber=@serialNumber;
select 'tr_tireInstallations',*  from tr_tireInstallations where serialNumber=@serialNumber;
select 'tr_tirePatchingDetails',*  from tr_tirePatchingDetails  where serialNumber=@serialNumber;
select 'tr_tireStatusMovements',*  from tr_tireStatusMovements where serialNumber=@serialNumber;
select 'tr_transferTires',*  from tr_transferTires where serialNumber=@serialNumber;
select 'tr_tireRetreading',* from tr_tireRetreading where afterTreadSN=@serialNumber;

goto skipper
insert into tr_tiresDeletedLogs select *,getdate() from tr_tires where serialNumber=@serialNumber;
print 'tr_tireStockMovement';delete from tr_tireStockMovement where serialNumber=@serialNumber;
print 'tr_tireAuditingFormDetails';delete from tr_tireAuditingFormDetails where serialNumber=@serialNumber;
print 'tr_tireRemovals';delete from tr_tireRemovals where serialNumber=@serialNumber;
print 'tr_tireInstallations';delete from tr_tireInstallations where serialNumber=@serialNumber;
print 'tr_tirePatchingDetails';delete from tr_tirePatchingDetails  where serialNumber=@serialNumber;
print 'tr_tireStatusMovements';delete from tr_tireStatusMovements where serialNumber=@serialNumber;
print 'tr_transferTires';delete from tr_transferTires where serialNumber=@serialNumber;
print 'auditconfirmTire';delete from tr_tireInternalAuditConfirmations where serialNumber=@serialNumber;
print 'update tr_tireRetreading';update tr_tireRetreading set afterTreadSN = null where afterTreadSN=@serialNumber;
print 'tr_tires';delete from tr_tires where serialNumber=@serialNumber;
skipper:


--update tr_tires set serialNumber='ZVJ52490U' where serialNumber like '%ZVJ52490U%'
--select * from tr_tires where serialNumber like '%ZVJ52490U%'
/*
CREATE TABLE [dbo].[tr_tiresDeletedLogs](
	[serialNumber] [varchar](50) NOT NULL,
	[newOrRetread] [char](1) NOT NULL,
	[retreaded] [char](1) NOT NULL,
	[treadPattern] [varchar](50) NOT NULL,
	[purchaseDate] [datetime] NULL,
	[size] [varchar](50) NOT NULL,
	[tubeless] [char](1) NOT NULL,
	[price] [decimal](18, 2) NOT NULL,
	[supplierId] [int] NOT NULL,
	[taxInvoiceNumber] [varchar](50) NULL,
	[PONumber] [varchar](50) NULL,
	[conditionId] [int] NOT NULL,
	[brandId] [int] NULL,
	[branchId] [int] NOT NULL,
	[comment] [varchar](max) NULL,
	[purchaseAtBranchId] [int] NULL,
	[updateInformation] [varchar](max) NULL,
	[deleteDateTime] [datetime] not null
) 
*/
