/****** Script for SelectTopNRows command from SSMS  ******/
use truck;
select updateDateTime,	changedBy,	serialNumber,	src,	treadPattern,	size,	tubeless,	price,	sp.name spname,	taxInvoiceNumber,	PONumber,	b.brand,	comment
from (
	SELECT dbo.FN_ConvertDate(updateDateTime,'Y/M/D')+' '+CONVERT(VARCHAR(9),updateDateTime,108) updateDateTime,u.name changedBy,t1.[serialNumber],'1.เดิม' src,t1.[treadPattern],t1.[size],t1.[tubeless],t1.[price],t1.[supplierId],t1.[taxInvoiceNumber],t1.[PONumber],t1.[brandId],t1.[comment] FROM [Truck].[dbo].[tr_tiresEditLogs] t1 left join [Truck].[dbo].tr_users u on t1.userId=u.id
	where not ([treadPattern]=[treadPattern2] and [size]=[size2] and [tubeless]= [tubeless2] and [price]= [price2] and [supplierId]=[supplierId2] and [taxInvoiceNumber]=[taxInvoiceNumber2] and [PONumber]=[PONumber2] and [brandId]=[brandId2] and [comment]=[comment2])
	union
	select dbo.FN_ConvertDate(updateDateTime,'Y/M/D')+' '+CONVERT(VARCHAR(9),updateDateTime,108) updateDateTime,u.name changedBy,t1.[serialNumber],'2.เปลี่ยนเป็น' src,t1.[treadPattern2],t1.[size2],t1.[tubeless2],t1.[price2],t1.[supplierId2],t1.[taxInvoiceNumber2],t1.[PONumber2],t1.[brandId2],t1.[comment2] FROM [Truck].[dbo].[tr_tiresEditLogs] t1 left join [Truck].[dbo].tr_users u on t1.userId=u.id
	where not ([treadPattern]=[treadPattern2] and [size]=[size2] and [tubeless]= [tubeless2] and [price]= [price2] and [supplierId]=[supplierId2] and [taxInvoiceNumber]=[taxInvoiceNumber2] and [PONumber]=[PONumber2] and [brandId]=[brandId2] and [comment]=[comment2])
) a left join tr_suppliers sp on a.supplierId=sp.id left join tr_brands b on a.brandId=b.id
order by [updateDateTime] desc,serialNumber,src 

--,t1.[userId],t1.[ipaddress],t1.[updateDateTime]
--BVK87471U
--delete from [tr_tiresEditLogs] where serialNumber='TEST0014'
