use truck;
--select * from tr_transferTireShipments
select 
'apr' transferType
,ttsd.serialNumber,mainTire.purchaseDate,ttsd.formId,mainTire.price initValue,ttsd.currentValue onTransferValue
,fromFleet.name [fromFleet],fromStock.stockType fromStock
,toFleet.name [toFleet],toStock.stockType toStock
,tts.approved
,reqUsr.name requestedBy,aprUsr.name approvedBy
,tts.signedShipper,tts.creatorComments
,recUsr.name receivedBy
,tts.createDate createDateTime,tts.receivedDate,'N' withVehicle
--, '|' sp,ttsd.*, '|',tts.*

from tr_transferTireShipmentDetails ttsd 
left join tr_transferTireShipments tts on ttsd.formId=tts.id
left join tr_tires mainTire on ttsd.serialNumber=mainTire.serialNumber
left join tr_branches fromFleet on tts.transferFrom=fromFleet.id
left join tr_branches toFleet on tts.transferTo=toFleet.id
left join tr_users aprUsr on tts.approvedBy=aprUsr.id
left join tr_users reqUsr on tts.createBy=reqUsr.id
left join tr_users recUsr on tts.receivedBy=recUsr.id
left join tr_tireStockTypes fromStock on ttsd.fromStockTypeId=fromStock.id
left join tr_tireStockTypes toStock on ttsd.toStockTypeId=toStock.id
--order by ttsd.serialNumber,tts.createDate desc,tts.id
union
select 'arb' transferType
,tt.serialNumber,mainTire.purchaseDate,tt.id formId
,mainTire.price initValue
,case when isnull(itt.treadDept,0) = 0 then 0
	else mainTire.price*(ttm.treadDept/itt.treadDept) 
end onTransferValue
,fromFleet.name [fromFleet],tst.stockType fromStock
,toFleet.name [toFleet]
,tst.stockType toStock
,null approved
,null requestedBy,aprUsr.name approvedBy
,null signedShipper,null creatorComments
,null receivedBy
,tt.transferDate createDateTime,tt.transferDate receivedDate
,case when tt.typeOfTransfer='V' then 'Y' else 'N' end  withVehicle 
from tr_transferTires tt 
left join tr_tires mainTire on tt.serialNumber=mainTire.serialNumber
left join tr_initTireTreadDept itt on tt.serialNumber=itt.serialNumber
left join tr_tireStockMovement ttm on 
	tt.serialNumber=ttm.serialNumber 
	--and convert(date,ttm.actionDate)=convert(date,tt.transferDate) 
	and convert(varchar(10),ttm.actionDate,111)=convert(varchar(10),tt.transferDate,111) 
	and ttm.operationOf='MO'
left join tr_tireStockTypes tst on ttm.stockTypeId=tst.id
left join tr_branches fromFleet on tt.transferFrom=fromFleet.id
left join tr_branches toFleet on tt.transferTo=toFleet.id
left join tr_users aprUsr on tt.transferBy=aprUsr.id

--select * from tr_tireStockMovement where serialNumber='3A1433E04'
--select * from tr_transferTires where serialNumber='3A1433E04';
--CONVERT(date, getdate())