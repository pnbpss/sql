/****** Script for SelectTopNRows command from SSMS  ******/
use truck;
SELECT 
ROW_NUMBER() OVER(ORDER BY
 changeDateTime DESC
 --,logs.[userId] 
  ,logs.[changedFrom]
  ,logs.[changedTo]  
  ,u.name 
 ) AS Row
	--,logs.[userId]
  ,dbo.FN_ConvertDate(logs.[changeDateTime],'Y/M/D')+' '+CONVERT(VARCHAR(9),logs.[changeDateTime],108) changeDateTime
  ,logs.[changedFrom]
  ,logs.[changedTo]
  --,logs.[ipAddress]
  ,u.name ChangedBy
FROM [Truck].[dbo].[tr_changeTireSerialNumberLogs] logs
	 left join [Truck].[dbo].tr_users u on logs.userId=u.id
where logs.changedFrom not like 'TEST%'
order by logs.changeDateTime desc