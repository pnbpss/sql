use truck;
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
IF OBJECT_ID('tempdb.dbo.#tempTb2', 'U') IS NOT NULL DROP TABLE #tempTb2;
select 
	br.name fleet,vt.typeCode headTrail,cti.plateNumber,cti.vehicleNumber,cti.userCall
	,cti.serialNumber
	,cti.treadPattern
	,cti.size
	,convert(varchar,cti.installDate,111)installDate
	,cti.treadDept
	into #tempTb
  from [tr_currentTireInstallationInfos] cti 
  left join tr_vehicles v on cti.vehicleId=v.id
  left join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
  left join tr_branches br on v.branchId=br.id
  
select * into #tempTb2 from tr_lastAuditOnVehicleTire


select fleet,plateNumber,vehicleNumber,'1.�Ţ�ҧ' info,headTrail
,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select fleet,headTrail,plateNumber,vehicleNumber,userCall,serialNumber	
  from #tempTb
) t pivot
(
	max(serialNumber) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select fleet,plateNumber,vehicleNumber,'2.��´͡�ҧ' info,headTrail
,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select fleet,headTrail,plateNumber,vehicleNumber,userCall,treadPattern from #tempTb
) t pivot
(
	max(treadPattern) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])	
) p
union
select fleet,plateNumber,vehicleNumber,'3.��Ҵ' info,headTrail,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select fleet,headTrail,plateNumber,vehicleNumber,userCall,size from #tempTb
) t pivot
(
	max(size) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select fleet,plateNumber,vehicleNumber,'4.Ǵ�.�Դ���' info,headTrail
,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select fleet,headTrail,plateNumber,vehicleNumber,userCall,installDate from #tempTb
) t pivot
(
	max(installDate) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select fleet,plateNumber,vehicleNumber,'5.�͡�֡�͹�Դ���' info,headTrail,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select fleet,headTrail,plateNumber,vehicleNumber,userCall,convert(varchar,treadDept)treadDept from #tempTb
) t pivot
(
	max(treadDept) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p

union
select fleet,plateNumber,vehicleNumber,'6.�ѹ����ʹԵ' info,headTrail,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select cti.fleet,cti.headTrail,cti.plateNumber,cti.vehicleNumber,cti.userCall,lao.auditDate 
  from #tempTb cti left join #tempTb2 lao on cti.serialNumber=lao.serialNumber and lao.plateNumber=cti.PlateNumber 
  and lao.userCall=cti.userCall  
) t pivot
(
	max(auditDate) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select fleet,plateNumber,vehicleNumber,'7.�͡�֡(�ʹԵ)' info,headTrail,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select cti.fleet,cti.headTrail,cti.plateNumber,cti.vehicleNumber,cti.userCall
  ,convert(varchar(max),lao.treadDept) treadDept 
  from #tempTb cti left join #tempTb2 lao on cti.serialNumber=lao.serialNumber and lao.plateNumber=cti.PlateNumber and lao.userCall=cti.userCall
) t pivot
(
	max(treadDept) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select fleet,plateNumber,vehicleNumber,'8.�Ţ��������ʹԵ' info,headTrail,isnull([1],'')[W1],isnull([2],'')[W2],isnull([3],'')[W3],isnull([4],'')[W4],isnull([5],'')[W5],isnull([6],'')[W6],isnull([7],'')[W7],isnull([8],'')[W8],isnull([9],'')[W9],isnull([10],'')[W10],isnull([A],'')[WA],isnull([B],'')[WB],isnull([SP1],'')[WSP1],isnull([SP2],'')[WSP2],isnull([11],'')[W11],isnull([12],'')[W12],isnull([13],'')[W13],isnull([14],'')[14],isnull([15],'')[W15],isnull([16],'')[W16],isnull([17],'')[W17],isnull([18],'')[W18],isnull([19],'')[W19],isnull([20],'')[W20],isnull([21],'')[W21],isnull([22],'')[W22],isnull([SP2�],'')[WSP2t]
from (
  select cti.fleet,cti.headTrail,cti.plateNumber,cti.vehicleNumber,cti.userCall,convert(varchar(max),lao.maxFormId) maxFormId 
  from #tempTb cti left join #tempTb2 lao on cti.serialNumber=lao.serialNumber and lao.plateNumber=cti.PlateNumber and lao.userCall=cti.userCall
) t pivot
(
	max(maxFormId) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2�])
) p
union
select distinct fleet,plateNumber, vehicleNumber,' ������' info, headTrail,'��ͷ�� 1'[W1],'��ͷ��2'[W2],'��ͷ��3'[W3],'��ͷ��4'[W4],'��ͷ��5'[W5],'��ͷ��6'[W6],'��ͷ��7'[W7],'��ͷ��8'[W8],'��ͷ��9'[W9],'��ͷ��10'[W10],'��ͷ��A'[WA],'��ͷ��B'[WB],'��ͷ��SP1'[WSP1],'��ͷ��SP2'[WSP2],'��ͷ��11'[W11],'��ͷ��12'[W12],'��ͷ��13'[W13],'��ͷ��14'[W14],'��ͷ��15'[W15],'��ͷ��16'[W16],'��ͷ��17'[W17],'��ͷ��18'[W18],'��ͷ��19'[W19],'��ͷ��20'[W20],'��ͷ��21'[W21],'��ͷ��22'[W22],'��ͷ��SP�'[WSP2t] from #tempTb

order by fleet,headTrail,plateNumber, info