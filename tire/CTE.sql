/*
drop table cteTable
create table employees (id int,name varchar(max), managerId int,groupId int);
delete from employees;
insert into employees values('1','a1',2,'1');
insert into employees values('2','a2',3,'1');
insert into employees values('3','a3',4,'1');
insert into employees values('4','a4',5,'1');
insert into employees values('5','a5',6,'1');
insert into employees values('6','a6',7,'1');
insert into employees values('7','a7',8,'1');
insert into employees values('8','a8',9,'1');
insert into employees values('9','a9',10,'1');
insert into employees values('10','a10',11,'1');
insert into employees values('11','a11',NULL,'1');
insert into employees values('12','b1',13,'2');
insert into employees values('13','b2',14,'2');
insert into employees values('14','b3',15,'2');
insert into employees values('15','b4',16,'2');
insert into employees values('16','b5',17,'2');
insert into employees values('17','b6',18,'2');
insert into employees values('18','b7',19,'2');
insert into employees values('19','b8',20,'2');
*/
/*
declare @groupId int; set @groupId = 1;
;with manangerLine (id,staffName,managerId,managerName,t) as
(
	select ep.id,ep.name as staffName,ep.managerId,mgr.name as managerName,'a' as t
	from employees ep left join employees mgr on ep.managerId=mgr.id
	where mgr.id is null and ep.groupId= @groupId
	union all
	select
	ep.id,ep.name as staffName,ep.managerId
	,mgr2.name managerName,'r' as t
	from manangerLine mgr 
		inner join employees ep on ep.managerId=mgr.id
		inner join employees mgr2 on ep.managerId=mgr2.id
	where ep.managerId is not null 
)
select id,staffName,managerId,managerName,t from manangerLine order by managerId desc;
*/
/*
;WITH Numbers AS
(
    SELECT n = 1
    UNION ALL
    SELECT n + 1
    FROM Numbers
    WHERE n+1 <= 10
)
SELECT n
FROM Numbers
*/

;with insRem (remReqId,remTreadDept,tinsTreadDept,serialNumber,removeDate,installDate,insReqId
,vehicleId,rInsSince,iInsSince,tpdId,tpId,trow,tcolumn) as
(
select 
tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
from tr_tireRemovals tr inner join tr_tireInstallations ti
on  tr.serialNumber=ti.serialNumber 
	and tr.seq=ti.seq
	and tr.installDate=ti.installDate
	and tr.vehicleId=ti.vehicleId
	and tr.tpdId=ti.tpdId
	and tr.tpId=ti.tpId
	and tr.trow=ti.trow
	and tr.tcolumn=ti.tcolumn
where tr.serialNumber='TEST0146' 
	 and tr.requestId='2084'
--order by tr.requestId desc
union all
select 
tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
from tr_tireRemovals tr inner join tr_tireInstallations ti
on  tr.serialNumber=ti.serialNumber 
	and tr.seq=ti.seq
	and tr.installDate=ti.installDate
	and tr.vehicleId=ti.vehicleId
	and tr.tpdId=ti.tpdId
	and tr.tpId=ti.tpId
	and tr.trow=ti.trow
	and tr.tcolumn=ti.tcolumn
	inner join insRem ir on
	tr.serialNumber=ir.serialNumber
	and tr.insSince=ir.iInsSince
	and tr.vehicleId=ir.vehicleId
	and tr.requestId=ir.insReqId
	and tr.removeDate=ir.installDate
)
--select * from insRem order by insReqId;
--select top 1 * from insRem order by insReqId;
select 
--top 1 
ir.remReqId,ir.remTreadDept,ir.tinsTreadDept
,ir.serialNumber,ir.removeDate,ir.installDate,ir.insReqId
,ir.vehicleId,ir.rInsSince,ir.iInsSince
,tpd.userCall,ir.tpdId,ir.tpId,ir.trow,ir.tcolumn
from insRem ir inner join tr_tirePositioningDetails tpd
on ir.tpdId=tpd.id and ir.tpId=tpd.tpId and ir.trow=tpd.trow and ir.tcolumn=tpd.tcolumn
order by insReqId;

--select * from dbo.firstInstalledInVehicleInfo(2084,'TEST0146')

select distinct aaa.userId from (
select distinct userId,branchId from tr_usersPrivileges where userId<>2 and branchId<>1000 order by userId
) as aaa