/*select b.name as branchName,vt.typeCode,count(*) as cntVehicleInFleetJob 
from tr_requestForTireMaintenances r 
inner join tr_branches b on r.branchId=b.id 
inner join tr_vehicles v on r.vehicleId=v.id 
inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id 
where r.branchId<>1000 
group by b.name,vt.typeCode 
order by b.name,vt.typeCode 
*/
declare @month int,@year int;
set @month=9; set @year=2014;
select b.name as branchName,cntJob.cntJob,cntJob.typeCode
,cntC3.cntTiresC3,cntC3.sumC3Expense
,cntC4.cntTiresC4,cntC4.sumC4Expense
,cntC5.cntTiresC5,cntC5.sumC5Expense
,cntC6.cntTiresC6,cntC6.sumC6Expense
,cte.cntChangeTube,cte.sumChangeTubeExpense
,cre.cntChangeRim,cre.sumChangeRimExpense
,acew.sumCountWheelCheckAir,ace.sumAirCheckExpense
,auditTire.countAuditedTire,audit.sumAuditExpense
,ote.sumOtherExpense
from tr_branches b left join 
( 	
	select isnull(COUNT(r.id),0) as cntJob,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where month(r.jobCompletedDate)=@month and YEAR(r.jobCompletedDate)=@year and r.branchId<>1000 
	group by r.branchId,vt.typeCode
	
) as cntJob on b.id=cntJob.branchId
left join
( --�ӹǹ�ҧ�������¹ case3
	select isnull(COUNT(c3.insSerialNumber),0) as cntTiresC3,SUM(c3.expense) as sumC3Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case3Scheema c3 on r.id=c3.requestId	
	where r.branchId<>1000 and month(c3.movementDate)=@month and year(c3.movementDate)=@year
	group by r.branchId,vt.typeCode
) as cntC3 on b.id=cntC3.branchId and cntJob.typeCode=cntC3.typeCode
left join
( --�ӹǹ�ҧ�������¹ case4
	select isnull(COUNT(c4.insSerialNumber),0) as cntTiresC4,SUM(c4.expense) as sumC4Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case4Scheema c4 on r.id=c4.requestId
	where r.branchId<>1000 and month(c4.movementDate)=@month and year(c4.movementDate)=@year
	group by r.branchId,vt.typeCode
) as cntC4 on b.id=cntC4.branchId and cntJob.typeCode=cntC4.typeCode
left join
( --��Ѻ�ҧcase5
	select isnull(COUNT(c5.insSerialNumber),0) as cntTiresC5,SUM(c5.expense) as sumC5Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case5Scheema c5 on r.id=c5.requestId
	where r.branchId<>1000 and month(c5.movementDate)=@month and year(c5.movementDate)=@year
	group by r.branchId,vt.typeCode
) as cntC5 on b.id=cntC5.branchId and cntJob.typeCode=cntC5.typeCode
left join
( --��Ѻ�ҧcase6
	select isnull(COUNT(c6.insSerialNumber),0) as cntTiresC6,SUM(c6.expense) as sumC6Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case6Scheema c6 on r.id=c6.requestId
	where r.branchId<>1000 and month(c6.movementDate)=@month and year(c6.movementDate)=@year
	group by r.branchId,vt.typeCode
) as cntC6 on b.id=cntC6.branchId and cntJob.typeCode=cntC6.typeCode
left join
( --���ҧ
	SELECT 
	vt.typeCode,r.branchId,count(ctrwd.changeTube) as cntChangeTube,sum(ctrwd.changeTubeExpense) as sumChangeTubeExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId
	inner join tr_changeTireRimWheelDetails ctrwd on ctr.requestId=ctrwd.requestId
	where ctrwd.changeTube='Y' and r.jobCompletedDate is not null and r.branchId<>1000 
		  and month(ctr.dateOfChange)=@month and YEAR(ctr.dateOfChange)=@year
	group by vt.typeCode,r.branchId
) as cte on b.id=cte.branchId and cntJob.typeCode=cte.typeCode
left join
( --�з����
	SELECT 
	vt.typeCode,r.branchId,count(ctrwd.changeRim) as cntChangeRim,sum(ctrwd.changeRimExpense) as sumChangeRimExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId
	inner join tr_changeTireRimWheelDetails ctrwd on ctr.requestId=ctrwd.requestId
	where ctrwd.changeRim='Y' and r.jobCompletedDate is not null and r.branchId<>1000 
		  and month(ctr.dateOfChange)=@month and YEAR(ctr.dateOfChange)=@year
	group by vt.typeCode,r.branchId
) as cre on b.id=cre.branchId and cntJob.typeCode=cre.typeCode
left join
( --�ӹǹ��ͻ���ҳ��÷�������ҧ
	SELECT vt.typeCode,r.branchId,sum(cw.countWheel) as sumCountWheelCheckAir
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId		
	inner join tr_countWheelThatShouldInstallTireOfVehicle cw on v.id=cw.vehicleId
	where r.jobCompletedDate is not null and r.branchId<>1000 and month(ctr.dateOfChange)=@month and YEAR(ctr.dateOfChange)=@year	
		  and r.airCheck='Y'
	group by vt.typeCode,r.branchId
) as acew on b.id=acew.branchId and cntJob.typeCode=acew.typeCode
left join
( --�������������ҧ
	SELECT 
	vt.typeCode,r.branchId,sum(ctr.checkAirExpense) as sumAirCheckExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId		
	where r.jobCompletedDate is not null and r.branchId<>1000 and month(ctr.dateOfChange)=@month and YEAR(ctr.dateOfChange)=@year
	and r.airCheck='Y'
	group by vt.typeCode,r.branchId
) as ace on b.id=ace.branchId and cntJob.typeCode=ace.typeCode
left join
( --�ӹǹ��ͷ��١�ʹԵ
	select count(tafd.serialNumber) countAuditedTire,taf.branchId,vt.typeCode 
	from tr_tireAuditingForm taf 
	inner join tr_tireAuditingVehicles tafv on taf.id=tafv.formId
	inner join tr_tireAuditingFormDetails tafd on taf.id=tafd.formId
	inner join tr_vehicles v on tafv.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where MONTH(taf.auditDate)=@month and YEAR(taf.auditDate)=@year and taf.branchId<>1000 and taf.statusId<>2
	group by taf.branchId,vt.typeCode
) as auditTire on b.id=auditTire.branchId and cntJob.typeCode=auditTire.typeCode
left join
( --���������ʹԵ�ҧ
	select SUM(taf.expense) sumAuditExpense,taf.branchId,vt.typeCode 
	from tr_tireAuditingForm taf 
	inner join tr_tireAuditingVehicles tafv on taf.id=tafv.formId
	inner join tr_vehicles v on tafv.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where MONTH(taf.auditDate)=@month and YEAR(taf.auditDate)=@year and taf.branchId<>1000 and taf.statusId<>2
	group by taf.branchId,vt.typeCode
) as audit on b.id=audit.branchId and cntJob.typeCode=audit.typeCode
left join
( --������������
	SELECT 
	vt.typeCode,r.branchId,sum(oted.expense) as sumOtherExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_otherExpenses ote on r.id=ote.requestId
	inner join tr_otherExpenseDetails oted on ote.requestId=oted.requestId
	where MONTH(ote.dateOfAction)=@month and YEAR(ote.dateOfAction)=@year and r.branchId<>1000
	group by vt.typeCode,r.branchId
) as ote on b.id=ote.branchId and cntJob.typeCode=ote.typeCode
where b.id<>1000 and cntJob.cntJob is not null
order by b.name