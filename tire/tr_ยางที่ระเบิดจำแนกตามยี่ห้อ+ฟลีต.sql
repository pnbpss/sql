/*
��§ҹ �ҧ������Դ ��ṡ���������,fleet
*/
use truck;
IF OBJECT_ID('truck.dbo.tr_tireExplodedStats', 'U') IS NOT NULL DROP TABLE tr_tireExplodedStats;
declare @startDate datetime,@endDate datetime, @causeOfRemove varchar(max);
set @startDate='2009/12/01';set @endDate=dateadd(day,1,getdate());set @causeOfRemove='���Դ'
select remPlateNumber,rTreadDept,remSerialNumber,allunion.branchId,remWheel,movementDate,t.price,itd.treadDept initTreadDept
,aggr1.remainValue,aggr2.usedPrice,brnd.brand,brnch.abbreviation fleet,GETDATE() lastRunDate
into tr_tireExplodedStats
from (
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate from tr_case2Scheema where remCauseDesc=@causeOfRemove  and movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate from tr_case3Scheema where remCauseDesc=@causeOfRemove  and movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate from tr_case4Scheema where remCauseDesc=@causeOfRemove  and movementDate between @startDate and @endDate
	union
	select remPlateNumber,rTreadDept,remSerialNumber,branchId,remWheel,movementDate from tr_case8Scheema where remCauseDesc=@causeOfRemove  and movementDate between @startDate and @endDate
) allunion left join tr_tires t on allunion.remSerialNumber=t.serialNumber left join tr_initTireTreadDept itd on allunion.remSerialNumber=itd.serialNumber 
left join tr_brands brnd on t.brandId=brnd.id
left join tr_branches brnch on allunion.branchId=brnch.id
cross apply (select  case when itd.treadDept!=0 then  convert( decimal(18,2), (t.price/itd.treadDept)*rTreadDept) 	else 0 end remainValue)aggr1
cross apply (select t.price-aggr1.remainValue usedPrice)aggr2

select COUNT(*)cnt,MIN(movementDate)minMdate,MAX(movementDate)maxMdate,remPlateNumber from tr_tireExplodedStats group by remPlateNumber order by COUNT(*) desc

select * from tr_tireExplodedStats


select cnt,brand
,convert( decimal(5,2), tes.cnt*100/aggr.allexploded) pExp
,aggr.allexploded
 from (
	select COUNT(*)*1.00 cnt,brand from tr_tireExplodedStats group by brand
)tes
cross apply (select COUNT(*)allexploded from tr_tireExplodedStats)aggr