select 
* from 
((
SELECT
tr.serialNumber AS remSerialNumber, tr.removeDate, ti.serialNumber AS insSerialNumber, ti.installDate, tr.requestId
,tr.vehicleId AS remVehicle, rtpd.userCall AS rWheelPosition, rtpd.id AS rTpdId, rtpd.tpId as rTpId, rtpd.trow as rRow, rtpd.tcolumn as rTcolumn, tr.seq as rseq
,tr.vehicleId AS insVehicle, itpd.userCall AS iWheelPosition, itpd.id AS iTpdId, itpd.tpId as iTpId, itpd.trow as iRow, itpd.tcolumn as iTcolumn, ti.seq as iseq
FROM
dbo.tr_tireRemovals AS tr 
INNER JOIN dbo.tr_tireInstallations AS ti ON ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND ti.tcolumn = tr.tcolumn AND ti.trow = tr.trow AND 
ti.installDate = tr.removeDate AND ti.vehicleId = tr.vehicleId 
INNER JOIN dbo.tr_tirePositioningDetails AS rtpd ON tr.tpdId = rtpd.id AND tr.tpId = rtpd.tpId AND tr.tcolumn = rtpd.tcolumn AND tr.trow = rtpd.trow
INNER JOIN dbo.tr_tirePositioningDetails AS itpd on ti.tpdId = itpd.id AND ti.tpId = itpd.tpId AND ti.tcolumn = itpd.tcolumn AND ti.trow = itpd.trow
where ti.seq='1' and tr.seq='1' and ti.installDate<>tr.installDate and ti.serialNumber<>tr.serialNumber
--order by ti.requestId desc
) 
union
(
SELECT
tr.serialNumber AS remSerialNumber, tr.removeDate, ti.serialNumber AS insSerialNumber, ti.installDate, tr.requestId
,tr.vehicleId AS remVehicle, rtpd.userCall AS rWheelPosition, rtpd.id AS rTpdId, rtpd.tpId as rTpId, rtpd.trow as rRow, rtpd.tcolumn as rTcolumn, tr.seq as rseq
,tr.vehicleId AS insVehicle, itpd.userCall AS iWheelPosition, itpd.id AS iTpdId, itpd.tpId as iTpId, itpd.trow as iRow, itpd.tcolumn as iTcolumn, ti.seq as iseq
FROM
dbo.tr_tireRemovals AS tr 
INNER JOIN dbo.tr_tireInstallations AS ti ON ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND ti.tcolumn = tr.tcolumn AND ti.trow = tr.trow AND 
ti.installDate = tr.removeDate AND ti.vehicleId = tr.vehicleId 
INNER JOIN dbo.tr_tirePositioningDetails AS rtpd ON tr.tpdId = rtpd.id AND tr.tpId = rtpd.tpId AND tr.tcolumn = rtpd.tcolumn AND tr.trow = rtpd.trow
INNER JOIN dbo.tr_tirePositioningDetails AS itpd on ti.tpdId = itpd.id AND ti.tpId = itpd.tpId AND ti.tcolumn = itpd.tcolumn AND ti.trow = itpd.trow
where ti.seq='1' and tr.seq='1' and ti.installDate=tr.installDate and ti.serialNumber<>tr.serialNumber
)
union
(
SELECT
tr.serialNumber AS remSerialNumber, tr.removeDate, ti.serialNumber AS insSerialNumber, ti.installDate, tr.requestId
,tr.vehicleId AS remVehicle, rtpd.userCall AS rWheelPosition, rtpd.id AS rTpdId, rtpd.tpId as rTpId, rtpd.trow as rRow, rtpd.tcolumn as rTcolumn, tr.seq as rseq
,tr.vehicleId AS insVehicle, itpd.userCall AS iWheelPosition, itpd.id AS iTpdId, itpd.tpId as iTpId, itpd.trow as iRow, itpd.tcolumn as iTcolumn, ti.seq as iseq
FROM
dbo.tr_tireRemovals AS tr 
INNER JOIN dbo.tr_tireInstallations AS ti ON ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND ti.tcolumn = tr.tcolumn AND ti.trow = tr.trow AND 
ti.installDate = tr.removeDate AND ti.vehicleId = tr.vehicleId 
INNER JOIN dbo.tr_tirePositioningDetails AS rtpd ON tr.tpdId = rtpd.id AND tr.tpId = rtpd.tpId AND tr.tcolumn = rtpd.tcolumn AND tr.trow = rtpd.trow
INNER JOIN dbo.tr_tirePositioningDetails AS itpd on ti.tpdId = itpd.id AND ti.tpId = itpd.tpId AND ti.tcolumn = itpd.tcolumn AND ti.trow = itpd.trow
where ti.seq=tr.seq+1 and ti.installDate=tr.installDate and ti.serialNumber<>tr.serialNumber
)) as allunion
where allunion.requestId='1312'
order by allunion.iseq,allunion.requestId desc


