select b.name as branchName,cntJob.typeCode

,isnull(cntC3.sumC3Expense,0) as c3
,isnull(cntC4.sumC4Expense,0) as c4
,isnull(cntC2.sumC2Expense,0) as c2
,isnull(cntC5.sumC5Expense,0) as c5
,isnull(cntC6.sumC6Expense,0) as c6
,isnull(cntC8.sumC8Expense,0) as c8

from tr_branches b left join 
( 			
	select distinct r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id				
	where r.branchId<>1000 
) as cntJob on b.id=cntJob.branchId
left join
( --จำนวนยางที่เปลี่ยน case3
	select isnull(COUNT(c3.insSerialNumber),0) as cntTiresC3,isnull(SUM(c3.expense),0) as sumC3Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case3Scheema c3 on r.id=c3.requestId	
	where r.branchId<>1000 and month(c3.movementDate)='12' and year(c3.movementDate)='2014'
	group by r.branchId,vt.typeCode
) as cntC3 on b.id=cntC3.branchId and cntJob.typeCode=cntC3.typeCode
left join
( --จำนวนยางที่เปลี่ยน case4
	select isnull(COUNT(c4.insSerialNumber),0) as cntTiresC4,isnull(SUM(c4.expense),0) as sumC4Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case4Scheema c4 on r.id=c4.requestId
	where r.branchId<>1000 and month(c4.movementDate)='12' and year(c4.movementDate)='2014'
	group by r.branchId,vt.typeCode
) as cntC4 on b.id=cntC4.branchId and cntJob.typeCode=cntC4.typeCode
left join
( --สลับยาง case5
	select isnull(COUNT(c5.insSerialNumber),0) as cntTiresC5,isnull(SUM(c5.expense),0) as sumC5Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case5Scheema c5 on r.id=c5.requestId
	where r.branchId<>1000 and month(c5.movementDate)='12' and year(c5.movementDate)='2014' and r.jobCompletedDate is not null
	group by r.branchId,vt.typeCode
) as cntC5 on b.id=cntC5.branchId and cntJob.typeCode=cntC5.typeCode
left join
( --สลับยางcase6
	select isnull(COUNT(c6.insSerialNumber),0) as cntTiresC6,isnull(SUM(c6.expense),0) as sumC6Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case6Scheema c6 on r.id=c6.requestId
	where r.branchId<>1000 and month(c6.movementDate)='12' and year(c6.movementDate)='2014' and r.jobCompletedDate is not null
	group by r.branchId,vt.typeCode
) as cntC6 on b.id=cntC6.branchId and cntJob.typeCode=cntC6.typeCode
left join
( --ถอดยาง case2
	select isnull(COUNT(c2.insSerialNumber),0) as cntTiresC2,isnull(SUM(c2.expense),0) as sumC2Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case2Scheema c2 on r.id=c2.requestId
	where r.branchId<>1000 and month(c2.movementDate)='12' and year(c2.movementDate)='2014' and r.jobCompletedDate is not null
	group by r.branchId,vt.typeCode
) as cntC2 on b.id=cntC2.branchId and cntJob.typeCode=cntC2.typeCode
left join
( --ถอดยาง case8
	select isnull(COUNT(c8.insSerialNumber),0) as cntTiresC8,isnull(SUM(c8.expense),0) as sumC8Expense,r.branchId,vt.typeCode
	from tr_requestForTireMaintenances r 
		 inner join tr_vehicles v on r.vehicleId=v.id
		 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
		 inner join tr_case8Scheema c8 on r.id=c8.requestId
	where r.branchId<>1000 and month(c8.movementDate)='12' and year(c8.movementDate)='2014' and r.jobCompletedDate is not null
	group by r.branchId,vt.typeCode
) as cntC8 on b.id=cntC8.branchId and cntJob.typeCode=cntC8.typeCode
left join
( --เปลี่ยนยางใน
	SELECT 
	vt.typeCode,r.branchId,isnull(count(ctrwd.changeTube),0) as cntChangeTube,isnull(sum(ctrwd.changeTubeExpense),0) as sumChangeTubeExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId
	inner join tr_changeTireRimWheelDetails ctrwd on ctr.requestId=ctrwd.requestId
	where r.changeTube='Y' and r.jobCompletedDate is not null and r.branchId<>1000 
		  and month(ctr.dateOfChange)='12' and YEAR(ctr.dateOfChange)='2014'
	group by vt.typeCode,r.branchId
) as cte on b.id=cte.branchId and cntJob.typeCode=cte.typeCode
left join
( --เปลี่ยนยางรอง
	SELECT 
	vt.typeCode,r.branchId,isnull(count(ctrwd.changeTubeSupporter),0) as cntChangeTubeSupporter
	, isnull(sum(ctrwd.changeTubeSupporterExpense),0) as sumChangeTubeSupporterExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId
	inner join tr_changeTireRimWheelDetails ctrwd on ctr.requestId=ctrwd.requestId
	where (r.changeTubeSupporter='Y') and r.jobCompletedDate is not null and r.branchId<>1000 
		  and month(ctr.dateOfChange)='12' and YEAR(ctr.dateOfChange)='2014'
	group by vt.typeCode,r.branchId
) as ctse on b.id=ctse.branchId and cntJob.typeCode=ctse.typeCode
left join
( --กะทะล้อ
	SELECT 
	vt.typeCode,r.branchId,isnull(count(ctrwd.changeRim),0) as cntChangeRim,isnull(sum(ctrwd.changeRimExpense),0) as sumChangeRimExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId
	inner join tr_changeTireRimWheelDetails ctrwd on ctr.requestId=ctrwd.requestId
	where r.changeRim='Y' and r.jobCompletedDate is not null and r.branchId<>1000 and month(ctr.dateOfChange)='12' and YEAR(ctr.dateOfChange)='2014'
	group by vt.typeCode,r.branchId
) as cre on b.id=cre.branchId and cntJob.typeCode=cre.typeCode
left join
( --จำนวนล้อประมาณการที่เช็คลมยาง
	SELECT vt.typeCode,r.branchId,isnull(sum(cw.countWheel),0) as sumCountWheelCheckAir
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId		
	inner join tr_countWheelThatShouldInstallTireOfVehicle cw on v.id=cw.vehicleId
	where r.jobCompletedDate is not null and r.branchId<>1000 and month(ctr.dateOfChange)='12' and YEAR(ctr.dateOfChange)='2014'	
		  and r.airCheck='Y'
	group by vt.typeCode,r.branchId
) as acew on b.id=acew.branchId and cntJob.typeCode=acew.typeCode
left join
( --ค่าใช้จ่ายเช็คลมยาง
	SELECT 
	vt.typeCode,r.branchId,isnull(sum(ctr.checkAirExpense),0) as sumAirCheckExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_changeTireRims ctr on r.id=ctr.requestId		
	where r.jobCompletedDate is not null and r.branchId<>1000 and month(ctr.dateOfChange)='12' and YEAR(ctr.dateOfChange)='2014'
	and r.airCheck='Y'
	group by vt.typeCode,r.branchId
) as ace on b.id=ace.branchId and cntJob.typeCode=ace.typeCode
left join
( --จำนวนล้อที่ถูกออดิต
	select isnull(count(tafd.serialNumber),0) countAuditedTire,taf.branchId,vt.typeCode 
	from tr_tireAuditingForm taf 
	inner join tr_tireAuditingVehicles tafv on taf.id=tafv.formId
	inner join tr_tireAuditingFormDetails tafd on taf.id=tafd.formId
	inner join tr_vehicles v on tafv.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where MONTH(taf.auditDate)='12' and YEAR(taf.auditDate)='2014' and taf.branchId<>1000 and taf.statusId<>2
	group by taf.branchId,vt.typeCode
) as auditTire on b.id=auditTire.branchId and cntJob.typeCode=auditTire.typeCode
left join
( --ค่าใช้จ่ายออดิตยาง
	--select isnull(SUM(taf.expense),0) sumAuditExpense,taf.branchId,vt.typeCode 
	select  isnull(SUM(taf.expense),0) as aa,0 sumAuditExpense,taf.branchId,vt.typeCode 
	from tr_tireAuditingForm taf 
	inner join tr_tireAuditingVehicles tafv on taf.id=tafv.formId
	inner join tr_vehicles v on tafv.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where MONTH(taf.auditDate)='12' and YEAR(taf.auditDate)='2014' and taf.branchId<>1000 and taf.statusId<>2
	group by taf.branchId,vt.typeCode
) as audit on b.id=audit.branchId and cntJob.typeCode=audit.typeCode
left join
( --ค่าใช้จ่ายอื่นๆ
	SELECT 
	vt.typeCode,r.branchId,sum(oted.expense) as sumOtherExpense
	from tr_requestForTireMaintenances r
	inner join tr_vehicles v on r.vehicleId=v.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	inner join tr_otherExpenses ote on r.id=ote.requestId
	inner join tr_otherExpenseDetails oted on ote.requestId=oted.requestId
	where MONTH(ote.dateOfAction)='12' and YEAR(ote.dateOfAction)='2014' and r.branchId<>1000 and r.otherExpense='Y'
	group by vt.typeCode,r.branchId
) as ote on b.id=ote.branchId and cntJob.typeCode=ote.typeCode
where b.id<>1000 and cntJob.typeCode is not null
order by b.name,cntJob.typeCode



select C3.* 
from tr_requestForTireMaintenances r 
	 inner join tr_vehicles v on r.vehicleId=v.id
	 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	 inner join tr_case3Scheema c3 on r.id=c3.requestId	
where r.branchId=1 and month(c3.movementDate)='12' and year(c3.movementDate)='2014'
--group by r.branchId,vt.typeCode
order by r.id desc


