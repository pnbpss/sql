/****** Script for SelectTopNRows command from SSMS  ******/
SELECT case movementCase
	when '1' then '- ล้อที่ '+tmm.insWheel+' ใส่ '+tmm.insSerialNumber
	when '2' then '- ล้อที่ '+remWheel+' ถอด '+tmm.remSerialNumber
	when '3' then '- ล้อที่ '+tmm.remWheel+' ถอด '+tmm.remSerialNumber+', ใส่ '+tmm.insSerialNumber+''
	when '6' then '- สลับยาง '+tmm.remSerialNumber+' ล้อที่ '+ tmm.remWheel+ ' กับ '+tmm.insSerialNumber+' ล้อที่ '+ tmm.insWheel
	when '4' then '- ล้อที่ '+tmm.remWheel + ' ถอด '+tmm.remSerialNumber+' ใส่ '+tmm.insSerialNumber +' (subst)'	
	when '5' then '- ย้าย '+tmm.remSerialNumber+' จากล้อที่ '+tmm.remWheel+' ไปล้อที่ '+tmm.insWheel
end As 'mmType'
,tmm.requestId,tmm.movementDate,tmm.insVehicleId,tmm.insSerialNumber,tmm.iTreadDept,tmm.insWheel,tmm.remVehicle,tmm.remSerialNumber
,tmm.rTreaDept,tmm.remWheel,tmm.movementCase,tmm.insSince
FROM tr_tireRemInsMovements tmm 
where tmm.requestId='1318'-- or tmm.requestId='1315' 
order by tmm.requestId, tmm.movementCase
/*
--เปลี่ยนในล้อเดียวกัน
select C3.requestId,C3.movementDate,C3.insVehicleId,C3.insSerialNumber,C3.iTreadDept,C3.insWheel,C3.remVehicle,C3.remSerialNumber,C3.rTreaDept,C3.remWheel,C3.movementCase,C3.insSince
	   --,COInsIns.requestId
	   ,COInsRem.requestId
	   ,CORemIns.requestId
	   --,CORemRem.requestId
from (	
    tr_tireRemInsMovements C3 	
	left join tr_tireRemInsMovements COInsRem on C3.requestId=COInsRem.requestId and C3.insSince=COInsRem.insSince and C3.insSerialNumber=COInsRem.remSerialNumber
	)left join tr_tireRemInsMovements CORemIns on C3.requestId=CORemIns.requestId and C3.insSince=CORemIns.insSince and C3.remSerialNumber=CORemIns.insSerialNumber
	/* case 3 ที่ไม่มีการย้ายไปล้ออื่นในคำร้องเดียวกัน */
where COInsRem.requestId is null and CORemIns.requestId is null and C3.movementCase='3'
and C3.requestId='1318'
and C3.movementDate between '2013/01/01' and '2014/07/31'
--and C3.movementCase<>'4' and C3.movementCase<>'1' and C3.movementCase<>'1' 
order by C3.requestId desc;

*/
--เปลี่ยนแบบมีการย้ายข้างใน
select C4.requestId,C4.movementDate,C4.insVehicleId,C4.insSerialNumber,C4.iTreadDept,C4.insWheel,C3.remVehicle,C3.remSerialNumber,C3.rTreaDept,C3.remWheel,C4.movementCase,C4.insSince
	   ,C3.requestId,C5.requestId,C3.requestId,C42.requestId
from 
	tr_tireRemInsMovements C4 
	inner join tr_tireRemInsMovements C5 on C4.requestId=C5.requestId and C4.insSince=C5.insSince and C4.remSerialNumber=C5.insSerialNumber
	inner join tr_tireRemInsMovements C3 
		on C5.requestId=C3.requestId and C5.insSince=C3.insSince
	inner join tr_tireRemInsMovements C42 on C3.requestId=C42.requestId and C3.insSince=C42.insSince and C3.insSerialNumber=C42.remSerialNumber
	
where --C42.requestId is not null and C4.movementCase='4' and C5.movementCase='5' and C3.movementCase='3'
C4.movementCase='4' and C5.movementCase='5' and C3.movementCase='3' and C42.movementCase='4' and C4.requestId='1318'
order by C4.requestId desc

--เปลี่ยนแบบมีการย้ายข้างใน เอา C3 ตั้ง
select C4.requestId,C4.movementDate,C4.insVehicleId,C4.insSerialNumber,C4.iTreadDept,C4.insWheel,C3.remVehicle,C3.remSerialNumber,C3.rTreaDept,C3.remWheel,C4.movementCase,C4.insSince
	   ,C3.requestId,C5.requestId,C3.requestId,C42.requestId
from 
	tr_tireRemInsMovements C3  

--select * from tr_tireRemInsMovements where movementCase='6' and requestId='1318'