select *
/*sum(saExp.auditExpense) auditExpense ,SUM(saExp.wheels) wheels
,sum(saExp.byVehicleExpense)/SUM(saExp.wheels) wheelsaverageExpense
,saExp.branchId,saExp.HT
*/
from 
(
	select 
	formId,auditExpense,branchId,auditExpense/cntVehicle byVehicleExpense,cntVehicle,hWheels+tWheels as wheels
	,h as HT
	from (
		select taf.id as formId, isnull(taf.expense,0) auditExpense,taf.branchId
		,isnull((select COUNT(tafv.vehicleId) from tr_tireAuditingVehicles tafv where tafv.formId=id),0) as cntVehicle
		,(select vt.typeCode from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id where tafv.formId=taf.id and tafv.HT='H') h
		,isnull((select COUNT(vtpd.vehicleId) from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTirePositioningDetails vtpd on v.id=vtpd.vehicleId where tafv.formId=taf.id and tafv.HT='H'),0) hWheels
		,(select vt.typeCode from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id where tafv.formId=taf.id and tafv.HT='T') t
		,isnull((select COUNT(vtpd.vehicleId) from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTirePositioningDetails vtpd on v.id=vtpd.vehicleId where tafv.formId=taf.id and tafv.HT='T'),0) tWheels
		from tr_tireAuditingForm taf 
		where MONTH(taf.auditDate)='12' and YEAR(taf.auditDate)='2014' and taf.statusId<>2 and taf.branchId<>1000 
	) as vInspections
	where cntVehicle<>0 and h is not null
	union
	select 
	formId,auditExpense,branchId,auditExpense/cntVehicle byVehicleExpense,cntVehicle,hWheels+tWheels as wheels
	,t as HT
	from (
		select taf.id as formId, isnull(taf.expense,0) auditExpense,taf.branchId
		,isnull((select COUNT(tafv.vehicleId) from tr_tireAuditingVehicles tafv where tafv.formId=id),0) as cntVehicle
		,(select vt.typeCode from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id where tafv.formId=taf.id and tafv.HT='H') h
		,isnull((select COUNT(vtpd.vehicleId) from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTirePositioningDetails vtpd on v.id=vtpd.vehicleId where tafv.formId=taf.id and tafv.HT='H'),0) hWheels
		,(select vt.typeCode from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id where tafv.formId=taf.id and tafv.HT='T') t
		,isnull((select COUNT(vtpd.vehicleId) from tr_tireAuditingVehicles tafv inner join tr_vehicles v on tafv.vehicleId=v.id inner join tr_vehicleTirePositioningDetails vtpd on v.id=vtpd.vehicleId where tafv.formId=taf.id and tafv.HT='T'),0) tWheels
		from tr_tireAuditingForm taf 
		where MONTH(taf.auditDate)='12' and YEAR(taf.auditDate)='2014' and taf.statusId<>2 and taf.branchId<>1000 
	) as vInspections
	where cntVehicle<>0 and t is not null 
) as saExp
--group by saExp.branchId,saExp.HT