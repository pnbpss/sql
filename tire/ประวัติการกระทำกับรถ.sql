use truck;
declare @plateNumber varchar(max);
set @plateNumber='70-2002';
select 
d.plateNumber 'ป้ายทะเบียน'
,d.[action] 'สิ่งที่ทำ',d.serialNumber
,d.requestId 'เลขที่คำร้อง'
,dbo.FN_ConvertDate(d.actionDate,'D/M/Y') as 'วันที่ทำการ'
,d.details 'รายละเอียดเพิ่มเติม'
,b.name as 'ที่ฟลีต'
,s.name as 'ผู้ให้บริการ'
,d.supplierId,d.branchId
from (
--declare @plateNumber varchar(max);set @plateNumber='70-1404';
select v.plateNumber,
case
	when iur.[case]='A' then 'ติดตั้งยาง'
	when iur.[case]='R' then 'ถอดยาง'
end as [action],iur.serialNumber,iur.requestId,
case
	when iur.[case]='A' then installDate
	when iur.[case]='R' then removeDate
end as actionDate,'ตำแหน่งล้อ '+tpd.userCall as details,r.branchId,r.supplierId
from tr_requestForTireMaintenances r 	
	left join tr_installationsUnionRemovals iur on r.id=iur.requestId
	left join tr_vehicles v on iur.vehicleId=v.id
	left join tr_tirePositioningDetails tpd on iur.tpdId=tpd.id and iur.tpId=tpd.tpId and iur.trow=tpd.trow and iur.tcolumn=tpd.tcolumn
where v.plateNumber=@plateNumber and r.substituteTire='Y'
	 --v.plateNumber='70-1404' and r.substituteTire='Y'
union
--tire patching
select v.plateNumber,'ปะยาง' as [action],tpcd.serialNumber,r.id as requestId
,tpc.patchingDate as actionDate,'ตำแหน่งล้อ '+iur.userCall+', ปะที่'+isnull(tpc.repairLocation,'') as details,r.branchId,r.supplierId
from tr_requestForTireMaintenances r 
	left join tr_tirePatchings tpc on r.id=tpc.requestId
	left join tr_tirePatchingDetails tpcd on tpc.requestId=tpcd.requestId
	left join tr_vehicles v on r.vehicleId=v.id
	left join tr_tirePositioningDetails iur on tpcd.tpdId=iur.id and tpcd.tpId=iur.tpId and tpcd.trow=iur.trow and tpcd.tcolumn=iur.tcolumn
where r.patchTire='Y' and v.plateNumber=@plateNumber
union
--change rim
select v.plateNumber,'เปลี่ยนกะทะ' as [action],null as serialNumber,r.id as requestId,tcr.dateOfChange as actionDate
,'ตำแหน่งล้อ '+iur.userCall as details,r.branchId,r.supplierId
from tr_requestForTireMaintenances r 
	left join tr_changeTireRims tcr on r.id=tcr.requestId
	left join tr_changeTireRimWheelDetails tcrd on tcr.requestId=tcrd.requestId
	left join tr_vehicles v on r.vehicleId=v.id
	left join tr_tirePositioningDetails iur on tcrd.tpdId=iur.id and tcrd.tpId=iur.tpId and tcrd.trow=iur.trow and tcrd.tcolumn=iur.tcolumn
where r.changeRim='Y' and v.plateNumber=@plateNumber and tcrd.changeRim='Y' 
union
--change tube
select v.plateNumber,'เปลี่ยนยางใน' as [action],null as serialNumber,r.id as requestId,tcr.dateOfChange as actionDate
,'ตำแหน่งล้อ '+iur.userCall as details,r.branchId,r.supplierId
from tr_requestForTireMaintenances r 
	left join tr_changeTireRims tcr on r.id=tcr.requestId
	left join tr_changeTireRimWheelDetails tcrd on tcr.requestId=tcrd.requestId
	left join tr_vehicles v on r.vehicleId=v.id
	left join tr_tirePositioningDetails iur on tcrd.tpdId=iur.id and tcrd.tpId=iur.tpId and tcrd.trow=iur.trow and tcrd.tcolumn=iur.tcolumn
where r.changeTube='Y' and v.plateNumber=@plateNumber and tcrd.changeTube='Y'
union
select v.plateNumber,'เช็คลมยาง' as [action],null as serialNumber,r.id as requestId,tcr.dateOfChange as actionDate
,null as details,r.branchId,r.supplierId
from tr_requestForTireMaintenances r 
	left join tr_changeTireRims tcr on r.id=tcr.requestId
	left join tr_changeTireRimWheelDetails tcrd on tcr.requestId=tcrd.requestId
	left join tr_vehicles v on r.vehicleId=v.id	
where r.airCheck='Y' and v.plateNumber=@plateNumber
union
--ลบการติดตั้งยาง


) as d left join tr_branches b on d.branchId=b.id 
	   left join tr_suppliers s on d.supplierId=s.id
order by d.actionDate desc
-- air check


/*
select * from tr_tirePatchings;
select * from tr_requestForTireMaintenances where vehicleId is null where id='32';
select * from tr_installationsUnionRemovals where requestId is null requestId='1061';
select count(r.id) as countRight from tr_requestForTireMaintenances r left join tr_usersPrivileges u on r.branchId=u.branchId
where u.userId='15' and r.id='396' and u.taskId='75'
*/

--alter table tr_deleteTireInstallationRemovalLogs add vehicleId int, tpdId int,tpId int,trow int,tcolumn int
