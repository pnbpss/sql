
use truck;
declare @fromDate datetime, @toDate datetime, @branchId int;
set @fromDate='2014/08/01';
set @toDate='2015/12/31';
set @branchId='1';
-- �ó��ҧ���ʹ������㹪�ǧ���ҷ���˹� ���¤��� ������ʴ���������������
select 
 allUnion.movementDateShow
,allUnion.repairOrderForOutsource
,allUnion.remVehicle
,vt.typeCode
,allUnion.remNumber
,allUnion.remPlateNumber
,allUnion.remSerialNumber
,t.size as tireSize
,allUnion.remTreadPattern
,case when st.id=2 then 1 else 0 end as mayBeRetreadable
,retreadingInfo.dateOfSendToRetread
,retreadingInfo.canRetread
,retreadingInfo.supplier
,retreadingInfo.afterTreadSN
,retreadingInfo.returnDate
,retreadingInfo.PONumber
,retreadingInfo.billDoNumber
,retreadingInfo.fetchToInstallDate
,retreadingInfo.fetchToInStallPlateNumber
,retreadingInfo.orderNumberToOutSource
,retreadingInfo.dateOfDisposal
,retreadingInfo.treatAsSpareTire
,retreadingInfo.treatAsSendToRetread
,retreadingInfo.treatAsWaitForDisposal

,allUnion.rTreadDept,allUnion.remWheel,allUnion.remCauseDesc
,allUnion.movementCase,allUnion.expense,allUnion.initInsTirePrice,allUnion.curInsTireValue,allUnion.branchId,b.name as branchName 
,allUnion.movementDate
,st.stockType as keepRemovedTireIn
,allUnion.requestId
from (				
	select 
	C.requestId	
	,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow
	,C.insVehicleId,C.insSerialNumber,null as insNewOrRetread,null as insTreadPattern
	,null as insPlateNumber,null as insNumber,C.iTreadDept,C.insWheel
	,null as insCauseDesc,C.remVehicle,C.remSerialNumber
	,remTire.newOrRetread as remNewOrRetread,remTire.treadPattern as remTreadPattern
	,remVehicle.plateNumber as remPlateNumber,remVehicle.number as remNumber
	,C.rTreadDept,C.remWheel,cr.[description] as remCauseDesc,C.movementCase
	,C.expense 
	,null as initInsTirePrice
	,null as curInsTireValue
	,rfm.branchId
	,rfm.repairOrderForOutsource
	,CONVERT(varchar,C.movementDate,111) as movementDate
	from (tr_tireRemInsMovements C inner join 
	tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	) left join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	where C.movementCase='8'
	 and rfm.branchId=@branchId and C.movementDate between @fromDate and @toDate and rfm.jobCompletedDate is not null 
	 union 
	SELECT
	C.requestId
	,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow
	,C.insVehicleId,C.insSerialNumber
	,insTire.newOrRetread as insNewOrRetread
	,insTire.treadPattern as insTreadPattern
	,insVehicle.plateNumber as insPlateNumber
	,insVehicle.number as insNumber
	,C.iTreadDept
	,C.insWheel
	,ci.[description] as insCauseDesc
	,C.remVehicle
	,C.remSerialNumber as remSerialNumber
	,(select newOrRetread from tr_tires where serialNumber=C.remSerialNumber) as remNewOrRetread
	,(select treadPattern from tr_tires where serialNumber=C.remSerialNumber) as remTreadPattern
	,(select plateNumber from tr_vehicles where id=C.remVehicle) as remPlateNumber
	,(select number from tr_vehicles where id=C.remVehicle) as remNumber
	,C.rTreadDept as rTreadDept
	,C.remWheel 
	,cr.[description] as remCauseDesc
	,C.movementCase
	,C.expense 
	,insTire.price as initInsTirePrice
	,case when ittd.treadDept=0 then 0 else ((insTire.price*C.iTreadDept)/ittd.treadDept) end as curInsTireValue
	,rfm.branchId
	,rfm.repairOrderForOutsource
	,CONVERT(varchar,C.movementDate,111) as movementDate
	from (
	tr_tireRemInsMovements C 
	
	inner join tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires insTire on C.insSerialNumber=insTire.serialNumber
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfInstallations ci on C.insCause=ci.id
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	inner join tr_initTireTreadDept ittd on C.insSerialNumber=ittd.serialNumber
	) 
	where C.movementCase='4'
	and C.remSerialNumber is not null
	 and rfm.branchId=@branchId and C.movementDate between @fromDate and @toDate and rfm.jobCompletedDate is not null union 
	select 
	C.requestId	
	,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow
	,C.insVehicleId,C.insSerialNumber,insTire.newOrRetread as insNewOrRetread,insTire.treadPattern as insTreadPattern
	,insVehicle.plateNumber as insPlateNumber,insVehicle.number as insNumber,C.iTreadDept
	,C.insWheel,ci.[description] as insCauseDesc,C.remVehicle
	,C.remSerialNumber,remTire.newOrRetread as remNewOrRetread,remTire.treadPattern as remTreadPattern
	,remVehicle.plateNumber as remPlateNumber,remVehicle.number as remNumber,C.rTreadDept 
	,C.remWheel,cr.[description] as remCauseDesc,C.movementCase,C.expense 
	,insTire.price as initInsTirePrice
	,case when ittd.treadDept=0 then 0 else ((insTire.price*C.iTreadDept)/ittd.treadDept) end as curInsTireValue
	,rfm.branchId
	,rfm.repairOrderForOutsource
	,CONVERT(varchar,C.movementDate,111) as movementDate
	from (
	tr_tireRemInsMovements C 
	left join tr_tireRemInsMovements COInsRem on C.requestId=COInsRem.requestId and C.insSince=COInsRem.insSince and C.insSerialNumber=COInsRem.remSerialNumber
	)left join tr_tireRemInsMovements CORemIns on C.requestId=CORemIns.requestId and C.insSince=CORemIns.insSince and C.remSerialNumber=CORemIns.insSerialNumber
	/* case 3 �������ա�������������㹤���ͧ���ǡѹ */
	inner join tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires insTire on C.insSerialNumber=insTire.serialNumber
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfInstallations ci on C.insCause=ci.id
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	inner join tr_initTireTreadDept ittd on C.insSerialNumber=ittd.serialNumber
	where COInsRem.requestId is null and CORemIns.requestId is null and C.movementCase='3' 
	and rfm.branchId=@branchId and C.movementDate between @fromDate and @toDate and rfm.jobCompletedDate is not null 
	union 
	select 
	C.requestId	
	,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow
	,C.insVehicleId,C.insSerialNumber,null as insNewOrRetread,null as insTreadPattern
	,null as insPlateNumber,null as insNumber,C.iTreadDept,C.insWheel
	,null as insCauseDesc,C.remVehicle,C.remSerialNumber
	,remTire.newOrRetread as remNewOrRetread,remTire.treadPattern as remTreadPattern
	,remVehicle.plateNumber as remPlateNumber,remVehicle.number as remNumber
	,C.rTreadDept,C.remWheel,cr.[description] as remCauseDesc,C.movementCase
	,C.expense 
	,null as initInsTirePrice
	,null as curInsTireValue
	,rfm.branchId
	,rfm.repairOrderForOutsource
	,CONVERT(varchar,C.movementDate,111) as movementDate
	from (tr_tireRemInsMovements C inner join 
	tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	) left join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	where C.movementCase='2'
	 and rfm.branchId=@branchId and C.movementDate between @fromDate and @toDate and rfm.jobCompletedDate is not null
	) as allUnion
left join tr_branches b on allUnion.branchId=b.id
left join tr_tireStockMovement tsm on allUnion.remSerialNumber=tsm.serialNumber and allUnion.movementDate=tsm.actionDate and tsm.operationOf='IR' and tsm.stockTypeId<>'9'
left join tr_tireStockTypes st on tsm.stockTypeId=st.id	
left join tr_vehicles v on allUnion.remVehicle=v.id
left join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
left join tr_tires t on allUnion.remSerialNumber=t.serialNumber
cross apply dbo.sendToRetreadAndFetchInfos(allUnion.remSerialNumber,allUnion.movementDate) retreadingInfo
--where allUnion.remSerialNumber='FVP26286H'
--where retreadingInfo.dateOfSendToRetread is not null
order by remSerialNumber,movementDate,remPlateNumber,movementCase,requestId
