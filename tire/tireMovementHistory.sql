use truck;
-- ����Ѻ���ʵ�͡ tr_tireStatusMovements
-- ��õԴ��� tr_tireInstallations
-- ��öʹ�͡�ҡö tr_tireRemovals
-- ��û��ҧ tr_tirePatching
-- ��������Ң� tr_transferTires
-- ��������ʹ͡ tr_tireRetreading
-- �������¹ʶҹ� tr_tireStatusMovements
declare @serialNumber varchar(max),@dateOfAction datetime, @descriptions varchar(max);
--set @serialNumber = 'TESTXXX0001';
set @serialNumber = 'UVJ17350H';
create table #tireHistory(serialNumber varchar(50), dateOfAction datetime, descriptions varchar(max));

-- ����Ѻ���ʵ�͡ tr_tireStockMovement
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select stm.serialNumber, stm.actionDate, '[stock '+stm.operationOf+'] '+stm.comments+' '+st.stockType as descriptions 
from tr_tireStockMovement stm left join tr_tireStockTypes st on stm.stockTypeId=st.id
where stm.serialNumber=@serialNumber;

-- ��õԴ��� tr_tireInstallations
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select ti.serialNumber, ti.installDate, '�Դ��駺� '+v.number+' '+coi.[description]+' ���˹����'+tpd.userCall
from tr_tireInstallations ti left join tr_vehicles v on ti.vehicleId=v.id
	 left join tr_causeOfInstallations coi on ti.causeId=coi.id
	 left join tr_tirePositioningDetails tpd 
		on ti.tpdId=tpd.id and ti.tpId=tpd.tpId and ti.trow=tpd.trow and ti.tcolumn=tpd.tcolumn
where ti.serialNumber = @serialNumber

-- ��öʹ�͡�ҡö tr_tireRemovals
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select tr.serialNumber, tr.removeDate, '�ʹ�͡�ҡ '+v.number+' '+cor.[description]+' ���˹���� '+tpd.userCall
from tr_tireRemovals tr left join tr_vehicles v on tr.vehicleId=v.id
	 left join tr_causeOfRemove cor on tr.causeId=cor.id
	 left join tr_tirePositioningDetails tpd 
		on tr.tpdId=tpd.id and tr.tpId=tpd.tpId and tr.trow=tpd.trow and tr.tcolumn=tpd.tcolumn
where tr.serialNumber = @serialNumber

-- ��û��ҧ tr_tirePatching
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select tp.serialNumber, tp.approvedDate, '͹��ѵԻ� �������� ' + convert(varchar(max),expense)+' �ҷ ��� ' +repairLocation+ ' (' + 
	case isnull(v.number,'')
		when '' then ''
		else v.number
	  end +')'
from tr_tirePatchings tp left join tr_vehicles v on tp.vehicleId=v.id
where tp.serialNumber=@serialNumber

-- �������¹ʶҹ� tr_tireStatusMovements
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select tstm.serialNumber, tstm.timeOfChange, '����¹ʶҹ��� '+tc.condition
from tr_tireStatusMovements tstm left join tr_tireConditions tc on tstm.changedTo=tc.id
where tstm.serialNumber=@serialNumber

-- ��������ʹ͡ tr_tireRetreading
insert into #tireHistory (serialNumber, dateOfAction, descriptions) 
select tr.beforeTreadSN, tr.dateOfSendToRetread, '������ʹ͡��� '+s.name
+
case isnull(tr.dtbUR,'')
	when '' then ''
	else ' �������ö���ʹ͡�� �Ѻ��Ѻ������ѹ���'
	+case tr.dtbUR when NULL THEN NULL ELSE substring(convert(char(5), tr.dtbUR,101),4,2)+'/'+left(convert(char(2), tr.dtbUR,101),2)+'/'+cast(CAST(left(convert(char(4), tr.dtbUR,111),4) as int)+543 as varchar) END 
end
+
case isnull(tr.afterTreadSN,'')
	when '' then ''
	else '���Ţ�ҧ������ '+tr.afterTreadSN
end

from tr_tireRetreading tr left join tr_suppliers s on tr.supplierId=s.id
where tr.beforeTreadSN=@serialNumber

select th.serialNumber, 
case th.dateOfAction when NULL THEN NULL ELSE substring(convert(char(5), th.dateOfAction,101),4,2)+'/'+left(convert(char(2), th.dateOfAction,101),2)+'/'+cast(CAST(left(convert(char(4), th.dateOfAction,111),4) as int)+543 as varchar) END as dateOfActionShow
--th.dateOfAction
,th.descriptions from #tireHistory th order by dateOfAction desc
drop table #tireHistory;