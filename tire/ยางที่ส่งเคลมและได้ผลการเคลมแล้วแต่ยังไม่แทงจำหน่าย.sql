select trc.condition,tcr.name claimResult
,tc.serialNumber
,dbo.FN_ConvertDate(tc.dateOfSendToClaim,'D/M/Y')dateOfSendToClaim,br.name branchId
,sp.name supplier,tc.DONumberSentTire,tc.tllRepresentativeSender
,tc.supplierRepresentativeReceiver,usent.name userIdsent
,tc.sendingComment,tc.stockSeqSent,tc.statusBefore
,tc.gtbDOReceipt
,tc.returnClaimedPrice,tc.serialNumberGetBack
,dbo.FN_ConvertDate(tc.getBackDate,'D/M/Y')getBackDate
,tc.supplierRepresentativeName,tc.getBackComments
,urec.name userIdSaveReceived

from tr_tireClaimings tc left join tr_tires tr on tc.serialNumber=tr.serialNumber
left join tr_tireClaimResults tcr on tc.claimResultId=tcr.id
left join tr_branches br on tc.branchId=br.id
left join tr_users usent on tc.userIdsent=usent.id
left join tr_users urec on tc.userIdSaveReceived=urec.id
left join tr_suppliers sp on tc.supplierId=sp.id
left join tr_tireConditions trc on tr.conditionId=trc.id
where claimResultId is not null and trc.id=100

--select * from tr_branches