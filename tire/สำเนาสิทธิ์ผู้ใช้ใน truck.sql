use truck;
declare @sourceUser int;
declare @destUser int;
declare @userId int;
declare @taskId int;
declare @branchId int;
declare @canSave char(1);
set @sourceUser = 35;
set @destUser = 39;

--delete from tr_usersPrivileges where userId=@destUser;

declare userRightsCursor cursor for 
select [userId],[taskId],[branchId],[canSave]
from tr_usersPrivileges where userId=@sourceUser;

open userRightsCursor;
fetch next from userRightsCursor into @userId, @taskId, @branchId,@canSave;
While (@@FETCH_STATUS <> -1)
begin
	--update #reReport001 set c=@countOnSituation where a=@locat;
	insert into tr_usersPrivileges (userId,taskId,branchId,canSave)
	values(@destUser, @taskId, @branchId,@canSave);
	--values(@destUser, @taskId, '2',@canSave);
	fetch next from userRightsCursor into @userId, @taskId, @branchId ,@canSave;
end
deallocate userRightsCursor;


--select * from tr_usersPrivileges where userId='22'
--delete from tr_usersPrivileges where userId='22'