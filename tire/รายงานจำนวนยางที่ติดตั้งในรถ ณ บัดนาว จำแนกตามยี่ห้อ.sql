use truck;
/*
select brand,isnull([BB],0)BB,isnull([KK],0)KK,	isnull([LB],0)LB,	isnull([LP],0)LP,	isnull([NK],0)NK,	isnull([TG],0)TG,	isnull([TJ],0)TJ,isnull([HQ],0)HQ,aggr.sumall
from (
		select COUNT(*)cnt,brnd.brand,br.abbreviation branchName
		from tr_currentTireInstallationInfos cti 
		inner join tr_tires t on cti.serialNumber=t.serialNumber
		inner join tr_branches br on t.branchId=br.id
		inner join tr_brands brnd on t.brandId=brnd.id
		group by brnd.brand,br.abbreviation
)t pivot (
	sum(cnt) for branchName in ([BB],[HQ],[KK],[LB],[LP],[NK],[TG],[TJ])
) p cross apply (select(isnull([BB],0)+	isnull([HQ],0)+	isnull([KK],0)+	isnull([LB],0)+	isnull([LP],0)+	isnull([NK],0)+	isnull([TG],0)+	isnull([TJ],0))sumall)aggr
*/

select brand
,isnull([BB_S],0)BB_S,isnull([BB_H],0)BB_H,isnull([BB_T],0)BB_T
,isnull([KK_S],0)KK_S,isnull([KK_H],0)KK_H,isnull([KK_T],0)KK_T
,isnull([LB_S],0)LB_S,isnull([LB_H],0)LB_H,isnull([LB_T],0)LB_T
,isnull([LP_S],0)LP_S,isnull([LP_H],0)LP_H,isnull([LP_T],0)LP_T
,isnull([NK_S],0)NK_S,isnull([NK_H],0)NK_H,isnull([NK_T],0)NK_T
,isnull([TG_S],0)TG_S,isnull([TG_H],0)TG_H,isnull([TG_T],0)TG_T
,isnull([TJ_S],0)TJ_S,isnull([TJ_H],0)TJ_H,isnull([TJ_T],0)TJ_T
,isnull([HQ_S],0)HQ_S,isnull([HQ_H],0)HQ_H,isnull([HQ_T],0)HQ_T
,aggr.sumall
from (
		
			select COUNT(*)cnt,brnd.brand,br.abbreviation+'_'+vt.typeCode brnTypeCode
			from tr_currentTireInstallationInfos cti 
			inner join tr_tires t on cti.serialNumber=t.serialNumber
			inner join tr_branches br on t.branchId=br.id
			inner join tr_brands brnd on t.brandId=brnd.id
			inner join tr_vehicles v on cti.vehicleId=v.id
			inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
			group by brnd.brand,br.abbreviation,vt.typeCode
		
)t pivot (
	sum(cnt) for brnTypeCode in ([BB_T],[HQ_T],[KK_T],[LB_T],[LP_T],[NK_T],[TG_T],[TJ_T],[BB_H],[HQ_H],[KK_H],[LB_H],[LP_H],[NK_H],[TG_H],[TJ_H],[BB_S],[HQ_S],[KK_S],[LB_S],[LP_S],[NK_S],[TG_S],[TJ_S])
) p cross apply (select(
								isnull([BB_H],0)+	isnull([HQ_H],0)+	isnull([KK_H],0)+	isnull([LB_H],0)+	isnull([LP_H],0)+	isnull([NK_H],0)+	isnull([TG_H],0)+	isnull([TJ_H],0)+
								isnull([BB_T],0)+	isnull([HQ_T],0)+	isnull([KK_T],0)+	isnull([LB_T],0)+	isnull([LP_T],0)+	isnull([NK_T],0)+	isnull([TG_T],0)+	isnull([TJ_T],0)+
								isnull([BB_S],0)+	isnull([HQ_S],0)+	isnull([KK_S],0)+	isnull([LB_S],0)+	isnull([LP_S],0)+	isnull([NK_S],0)+	isnull([TG_S],0)+	isnull([TJ_S],0)
								)sumall)aggr
order by sumall desc


select brchBrnd
,isnull([STCK_1],0)STCK_1
,isnull([STCK_2],0)STCK_2
,isnull([STCK_3],0)STCK_3
,isnull([STCK_4],0)STCK_4
,isnull([STCK_5],0)STCK_5
,isnull([STCK_12],0)STCK_12
,isnull([STCK_13],0)STCK_13
,isnull([STCK_15],0)STCK_15

,aggr.sumAll
from (
	select COUNT(*)cnt,br.abbreviation+'_'+brnd.brand brchBrnd,'STCK_'+convert(varchar(max),tst.id) stockType
	from tr_notInstalledTires nit
	inner join tr_tires t on nit.serialNumber=t.serialNumber
	inner join tr_branches br on t.branchId=br.id
	inner join tr_brands brnd on t.brandId=brnd.id
	inner join tr_currentTireStock cts on t.serialNumber=cts.serialNumber
	inner join tr_tireStockTypes tst on cts.stockTypeId=tst.id
	where t.conditionId!=400
	group by brnd.brand,br.abbreviation,tst.id
)t pivot(
	sum(cnt) for stockType in ([STCK_13],	[STCK_1],	[STCK_2],	[STCK_5],	[STCK_12],	[STCK_15],	[STCK_4],	[STCK_3])
)p
cross apply (select (
	isnull([STCK_13],0)+isnull([STCK_1],0)+isnull([STCK_2],0)+isnull([STCK_5],0)+isnull([STCK_12],0)+isnull([STCK_15],0)+isnull([STCK_4],0)+isnull([STCK_3],0)
)sumAll)aggr
order by brchBrnd

--select * from tr_tireStockTypes where id in (1,2,3,4,5,12,13,15)