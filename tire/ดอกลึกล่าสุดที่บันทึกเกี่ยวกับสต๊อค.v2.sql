/****** Script for SelectTopNRows command from SSMS  ******/
/*SELECT TOP 1000 [serialNumber]
      ,[installDate]
      ,[treadDept]
      ,[requestId]
      ,[vehicleId]
      ,[tpdId]
      ,[tpId]
      ,[tcolumn]
      ,[trow]
  FROM [Truck].[dbo].
  */
--create view tr_lastTireTreadDepth as
select lIst.serialNumber
	   ,lIst.treadDept as lIstTreadDept, lIst.installDate as lIstDate
	   ,lSmm.treadDept as lSmmTreadDept, lSmm.maxActionDate as lSmmDate
	   ,lAud.treadDept as lAudTreadDept, lAud.maxAuditDate as lAudDate
	   ,case
		when (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then lIst.installDate
		when (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then lSmm.maxActionDate
		when (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) then lAud.maxAuditDate		
	   end as lastActionDate
	   ,case
		when (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then 'ins'
		when (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then 'sto'
		when (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) then 'aud'
	   end as lastTypeActionDate
	   ,case
		when (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lIst.installDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then lIst.treadDept
		when (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE())) >= isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE()))) then lSmm.treadDept
		when (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lIst.installDate,DATEADD(Year,-10,GETDATE()))) and (isnull(lAud.maxAuditDate,DATEADD(Year,-10,GETDATE())) >= isnull(lSmm.maxActionDate,DATEADD(Year,-10,GETDATE()))) then lAud.treadDept
	   end as lastTreadDept
from tr_lastInstalledTreadDept lIst full outer join tr_lastStockMovementTreadDept lSmm 
   on lIst.serialNumber=lSmm.serialNumber full outer join tr_lastAuditedTreadDept lAud
   on lSmm.serialNumber=lAud.serialNumber
where lIst.serialNumber = 'BVO35839H'

--SELECT DATEADD(Year,-10,GETDATE())