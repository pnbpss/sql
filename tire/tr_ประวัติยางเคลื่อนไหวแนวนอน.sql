use truck;
IF OBJECT_ID('tempdb..#tempLTTD') is not null DROP TABLE #tempLTTD
SELECT distinct [lastActionDate],[lastAction],[lastTreadDept],[serialNumber]
into #tempLTTD
FROM [tr_lastTireTreadDepth]
create index snIdx on #tempLTTD (serialNumber)

IF OBJECT_ID('tempdb..#tempCTS') is not null DROP TABLE #tempCTS
SELECT distinct [serialNumber],[maxActionDate],[newOrRetread],[treadDept],[stockTypeId],[stockType],[treadPattern],[brandName],[size],[initTreaDept],[currentValue],[branchName],[causeOfRemove]
into #tempCTS
FROM [truck].[dbo].[tr_currentTireStock]
create index snIdx on #tempCTS (serialNumber)

IF OBJECT_ID('tempdb..#final01') is not null DROP TABLE #final01

select 
t.serialNumber
,t.newOrRetread
,t.retreaded
,t.PONumber
,tcd.condition
,crBr.name currentBranch
,pcBr.name purchaseBranch
,cti.plateNumber+'/'+cti.userCall installedOn
,cts.stockType currentStock
,t.price purchasePrice,tsm.treadDept purchaesTreadDept
,ltt.lastTreadDept
,ltt.lastAction
,dbo.FN_ConvertDAte(ltt.lastActionDate,'Y/M/D')lastActionDate
,convert(decimal(18,2),case when tsm.treadDept=0 then 0 else (t.price*ltt.lastTreadDept)/tsm.treadDept end) currentPrice 
,lrtv.plateNumber+'/'+lrtTp.userCall lRemV,dbo.FN_ConvertDate(lrt.removeDate,'Y/M/D') removeDate
,litv.plateNumber+'/'+litTp.userCall lInsV,dbo.FN_ConvertDate(lit.installDate,'Y/M/D') installDate
,dbo.tireHistory(t.serialNumber) history
--into #final01
from tr_tires t 
left join tr_branches crBr on t.branchId=crBr.id
left join tr_branches pcBr on t.purchaseAtBranchId=pcBr.id
left join tr_currentTireInstallationInfos cti on t.serialNumber=cti.serialNumber
left join #tempCTS cts on t.serialNumber=cts.serialNumber
left join tr_tireConditions tcd on t.conditionId=tcd.id
left join #tempLTTD ltt on t.serialNumber=ltt.serialNumber
left join tr_initTireTreadDept tsm on t.serialNumber=tsm.serialNumber 
left join tr_lastRemovedTreadDept2 lrt on t.serialNumber=lrt.serialNumber left join tr_vehicles lrtv on lrt.vehicleId=lrtv.id left join tr_tirePositioningDetails lrtTp on lrt.tpdId=lrtTp.id and lrt.tpId=lrtTp.tpId and lrt.tcolumn=lrtTp.tcolumn and lrt.trow=lrtTp.trow
left join tr_lastInstalledTreadDept lit on t.serialNumber=lit.serialNumber left join tr_vehicles litv on lit.vehicleId=litv.id left join tr_tirePositioningDetails litTp on lit.tpdId=litTp.id and lit.tpId=litTp.tpId and lit.tcolumn=litTp.tcolumn and lit.trow=litTp.trow 
where 1=1
--and crBr.name='������ѧ' 
--and t.serialNumber in ('1312459204','1314062840','1506G37115','1507E36060','1507F51360','1507G06102','1515120734','15270109105','15271109051','15450109525','15462108260')
--and t.serialNumber='16366104230'
--select dbo.tireHistory(t.serialNumber) hstr from tr_tires t

--select serialNumber,newOrRetread,retreaded,PONumber,condition,currentBranch,purchaseBranch,installedOn,currentStock,purchasePrice,purchaesTreadDept,lastTreadDept,lastAction,lastActionDate,currentPrice,lRemV,removeDate,lInsV,installDate ,dbo.tireHistory(serialNumber) history from #final01;


 