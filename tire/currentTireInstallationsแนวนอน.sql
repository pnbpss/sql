use truck;
/****** Script for SelectTopNRows command from SSMS  ******/
/*
SELECT TOP 1000 [serialNumber]
      ,[treadPattern]
      ,[size]
      ,[causeId]
      ,[vehicleId]
      ,[mileage]
      ,[installDate]
      ,[trow]
      ,[tcolumn]
      ,[treadDept]
      ,[requestId]
      ,[id]
      ,[plateNumber]
      ,[tpId]
      ,[tpdId]
      ,[plateProvinceId]
      ,[PROVINCE_NAME]
      ,[vehicleNumber]
      ,[branchId]
      ,[userCall]
      ,[seq]
  FROM [Truck].[dbo].[tr_currentTireInstallationInfos]
  select * from tr_vehicleTypes
  */
select fleet,headTrail,plateNumber,vehicleNumber
,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[A],[B],[SP1],[SP2],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[SP2ห]
from (
  select 
	br.name fleet,vt.typeCode headTrail,cti.plateNumber,cti.vehicleNumber,cti.userCall
	,cti.serialNumber tireInfo
	--,cti.serialNumber+', '+cti.treadPattern+', '+cti.size+', '+convert(varchar,cti.installDate,111)+', '+convert(varchar,cti.treadDept) tireInfo
  from [tr_currentTireInstallationInfos] cti 
  left join tr_vehicles v on cti.vehicleId=v.id
  left join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
  left join tr_branches br on v.branchId=br.id
) t pivot
(
	--max(serialNumber) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2ห])
	max(tireInfo) for userCall in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B],[SP1],[SP2],[SP2ห])
) p