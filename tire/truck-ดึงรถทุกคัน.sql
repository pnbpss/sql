/****** Script for SelectTopNRows command from SSMS  ******/
use truck;
SELECT t.id 'เลขคุมในระบบ'
      ,t.number 
      ,t.plateNumber 'ป้าย'
      --,t.plateProvinceId
      ,p.PROVINCE_NAME 'ป้ายจังหวัด'
      ,b.name 'ฟลีต'
      ,brnd.brand 'ยี่ห้อ'
      ,vt.typeDesc 'ชนิด'
      ,vt.typeCode 'หัวหาง S=หกล้อ, T=หาง, H=หัว'
      --,t.tirePositioningId
      --,t.productId
      --,t.statusId
      ,vts.name 'สถานะ'
      ,t.comments 'บันทึกเพิ่มเติม'
      ,isnull(t.oldPlateNumber,'') 'ทะเบียนเก่า'
      ,t.enrolledDate 'เอาเข้าระบบเมื่อ'
  FROM tr_vehicles t 
	   left join tr_provinces p on t.plateProvinceId=p.PROVINCE_ID
       left join tr_branches b on t.branchId=b.id
       left join tr_brands brnd on t.brandId=brnd.id
       left join tr_vehicleTypes vt on t.vehicleTypeId=vt.id
       left join tr_vehicleStatuses vts on t.statusId=vts.id
       ;
  
  /*select * from tr_provinces;
  select * from tr_brands;
  select * from tr_vehicleTypes;
*/