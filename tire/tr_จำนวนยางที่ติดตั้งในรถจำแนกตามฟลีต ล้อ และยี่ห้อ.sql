use truck;
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
declare @fleetId int;
set @fleetId=1;
select brand,branchName,isnull(W1,0)W1,	isnull(W2,0)W2,	isnull(W3,0)W3,	isnull(W4,0)W4,	isnull(W5,0)W5,	isnull(W6,0)W6,	isnull(W7,0)W7,	isnull(W8,0)W8,	isnull(W9,0)W9,	isnull(W10,0)W10,	isnull(WA,0)WA,	isnull(WB,0)WB,	isnull(W11,0)W11,	isnull(W12,0)W12,	isnull(W13,0)W13,	isnull(W14,0)W14,	isnull(W15,0)W15,	isnull(W16,0)W16,	isnull(W17,0)W17,	isnull(W18,0)W18,	isnull(W19,0)W19,	isnull(W20,0)W20,	isnull(W21,0)W21,	isnull(W22,0)W22,	isnull(WSP,0)WSP
into #tempTb
from (
		select case when brnd.brand='(n/a)' then '����բ�����������' else brnd.brand end brand,br.abbreviation branchName
		,case when cti.userCall in ('SP1','SP2','SP2�') then 'WSP'
			else 'W'+cti.userCall 
		end wheelPosition
		, COUNT(*)cnt
		from tr_currentTireInstallationInfos cti left join tr_tires t on cti.serialNumber=t.serialNumber
		left join tr_brands brnd on t.brandId=brnd.id
		left join tr_branches br on t.branchId=br.id
		where 1=1
		--and br.id=@fleetId
		and cti.plateNumber not like '%XX%'
		group by brnd.brand,br.abbreviation,cti.userCall
) t
pivot (
	sum(cnt) for wheelPosition in ([W1],	[W2],	[W3],	[W4],	[W5],	[W6],	[W7],	[W8],	[W9],	[W10],	[WA],	[WB],	[W11],	[W12],	[W13],	[W14],	[W15],	[W16],	[W17],	[W18],	[W19],	[W20],	[W21],	[W22],	[WSP])
) p

-----------�ӹǹ�ҧ���Դ����ö ��ṡ��������� ��յ ��� ���˹����
select brand,branchName
,[W1],[W2],[W3],[W4],[W5],[W6],[W7],[W8],[W9],[W10],[WA],[WB], ([W1]+[W2]+[W3]+[W4]+[W5]+[W6]+[W7]+[W8]+[W9]+[W10]+[WA]+[WB]) sumHead
,[W11],[W12],[W13],[W14],[W15],[W16],[W17],[W18],[W19],[W20],[W21],[W22],[WSP],([W11]+[W12]+[W13]+[W14]+[W15]+[W16]+[W17]+[W18]+[W19]+[W20]+[W21]+[W22]+[WSP] ) sumTrail
,([W1]+[W2]+[W3]+[W4]+[W5]+[W6]+[W7]+[W8]+[W9]+[W10]+[WA]+[WB]+ [W11]+[W12]+[W13]+[W14]+[W15]+[W16]+[W17]+[W18]+[W19]+[W20]+[W21]+[W22]+[WSP]) sumAll
from #tempTb

/*-----------�ӹǹ�ҧ���Դ����ö ��ṡ��������� --select brand, SUM([W1]+	[W2]+	[W3]+	[W4]+	[W5]+	[W6]+	[W7]+	[W8]+	[W9]+	[W10]+	[WA]+	[WB]+	[W11]+	[W12]+	[W13]+	[W14]+	[W15]+	[W16]+	[W17]+	[W18]+	[W19]+	[W20]+	[W21]+	[W22]+	[WSP]) sumbyBrand from #tempTb group by brand*/

-----------�ӹǹ�ҧ���Դ����ö ��ṡ��������� ��յ 
select brand, isnull(BB,0)BB,	isnull(HQ,0)HQ,	isnull(KK,0)KK,	isnull(LB,0)LB,	isnull(NK,0)NK,	isnull(TG,0)TG, isnull(LP,0)LP,	isnull(TJ,0)TJ
,(isnull(BB,0)+	isnull(HQ,0)+	isnull(KK,0)+	isnull(LB,0)+	isnull(NK,0)+	isnull(TG,0)+	isnull(TJ,0)+	isnull(LP,0)) sumAllBranch
from (
	select brand, branchName, SUM([W1]+	[W2]+	[W3]+	[W4]+	[W5]+	[W6]+	[W7]+	[W8]+	[W9]+	[W10]+	[WA]+	[WB]+	[W11]+	[W12]+	[W13]+	[W14]+	[W15]+	[W16]+	[W17]+	[W18]+	[W19]+	[W20]+	[W21]+	[W22]+	[WSP]) cnt from #tempTb group by brand, branchName
) t pivot (
	sum(cnt) for branchName in (BB,HQ,KK,LB,NK,TG,TJ,LP)
) p
union
select brand, isnull(BB,0)BB,	isnull(HQ,0)HQ,	isnull(KK,0)KK,	isnull(LB,0)LB,	isnull(NK,0)NK,	isnull(TG,0)TG, isnull(LP,0)LP,	isnull(TJ,0)TJ
,(isnull(BB,0)+	isnull(HQ,0)+	isnull(KK,0)+	isnull(LB,0)+	isnull(NK,0)+	isnull(TG,0)+	isnull(TJ,0)+	isnull(LP,0)) sumAllBranch
from (
	select '����' brand, branchName, SUM([W1]+	[W2]+	[W3]+	[W4]+	[W5]+	[W6]+	[W7]+	[W8]+	[W9]+	[W10]+	[WA]+	[WB]+	[W11]+	[W12]+	[W13]+	[W14]+	[W15]+	[W16]+	[W17]+	[W18]+	[W19]+	[W20]+	[W21]+	[W22]+	[WSP]) cnt from #tempTb group by  branchName
) t pivot (
	sum(cnt) for branchName in (BB,HQ,KK,LB,NK,TG,TJ,LP)
) p
order by brand 
--select distinct branchName from #tempTb