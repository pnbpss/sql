ALTER TABLE tr_tireInstallations
ADD CONSTRAINT tireInstallationDate CHECK (installDate <= GetDate() );

ALTER TABLE tr_tireRemovals
ADD CONSTRAINT tireRemovDate CHECK (removeDate <= GetDate() and removeDate > installDate );