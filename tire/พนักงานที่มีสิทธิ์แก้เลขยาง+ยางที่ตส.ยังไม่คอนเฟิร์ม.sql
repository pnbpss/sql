/****** Script for SelectTopNRows command from SSMS  ******/
--พนักงานที่มีสิทธิ์แก้เลขยาง
SELECT u.name userName,u.loginname
	  ,br.name branchName
      ,up.[userId]
      --,up.[taskId]
      ,up.[branchId]
      --,up.[canSave]      
      ,lastLogin.lastLoginDateTime
  FROM [Truck].[dbo].[tr_usersPrivileges] up left join tr_users u on up.userId=u.id
  left join tr_branches br on up.branchId=br.id
  left join (
	SELECT max([logindatetime])lastLoginDateTime,userId
	FROM [Truck].[dbo].[tr_usersLoginLogs]
	where logindatetime>'20140101'
	group by userId	  
  ) lastLogin on u.id=lastLogin.userId
  where taskId=64 --แก้ไขข้อมูลยาง
  and canSave='Y' and u.[disabled]='N' and u.id!=40
  and lastLogin.userId is not null
order by userId,branchId

--ยางที่ตส.ยังไม่คอนเฟิร์ม
select 
--t.*,
t.serialNumber
,dbo.FN_ConvertDate(t.purchaseDate,'Y/M/D') purchaseDate
,t.purchaseAtBranchId
,br.name purchaseAtBranch
,t.taxInvoiceNumber,t.PONumber 
,sp.name 

from tr_tires t left join tr_tireInternalAuditConfirmations tac on t.serialNumber=tac.serialNumber
left join tr_suppliers sp on t.supplierId=sp.id
left join tr_branches br on br.id=t.purchaseAtBranchId
where tac.confirmedBy is null and t.conditionId=100
order by t.purchaseDate desc