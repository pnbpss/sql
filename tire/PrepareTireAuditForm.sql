/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [id]
      ,[dateCreated]
      ,[createdBy]
      ,[auditDate]
      ,[parkToAuditAt]
      ,[comments]
      ,[statusId]
      ,[auditor]
      ,[lastUpdateInfo]
FROM [Truck].[dbo].[tr_tireAuditingForm]

SELECT TOP 1000 [formId]
      ,[vehicleId]
      ,[HT]
      ,[driverName]
      ,[mileage]
      ,[driverTelNo]
  FROM [Truck].[dbo].[tr_tireAuditingVehicles]
  -- where formId<>11;
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [formId]
      ,[serialNumber]
      ,[tpdId]
      ,[tpId]
      ,[trow]
      ,[tcolumn]
      ,[vehicleId]
      ,[serialNumberFound]
      ,[treadDept]
  FROM [Truck].[dbo].[tr_tireAuditingFormDetails]