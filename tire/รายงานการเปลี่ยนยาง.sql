/****** Script for SelectTopNRows command from SSMS  ******/
SELECT cti.addedSerialNumber
	  ,addTire.newOrRetread
	  ,addTire.treadPattern
      ,cti.causeId as addCauseId,cti.remCauseId
      ,coi.[description] as installCause,cor.[description] as removeCause
      ,v.plateNumber, v.number
      ,cti.mileage
      ,case cti.installDate when NULL THEN NULL ELSE substring(convert(char(5), cti.installDate,101),4,2)+'/'+left(convert(char(2), cti.installDate,101),2)+'/'+cast(CAST(left(convert(char(4), cti.installDate,111),4) as int)+543 as varchar) END as installDate
      --,cti.tpId--,cti.tpdId--,cti.trow--,cti.tcolumn
      ,cti.treadDept as addTreadDept,cti.requestId,cti.comments as addComment,cti.removedSerialNumber
      ,cti.removedTreadDept,cti.removedMileage,tpd.userCall as wheelPosition,st.stockType as keepRemovedTireIn
      ,cti.remComment
      
  FROM tr_changeTireInsameWheelPosition cti left join 
	   tr_causeOfInstallations coi on cti.causeId=coi.id left join
	   tr_causeOfRemove cor on cti.remCauseId=cor.id left join
	   tr_vehicles v on cti.vehicleId=v.id left join
	   tr_tires addTire on cti.addedSerialNumber=addTire.serialNumber left join
	   tr_tires remTire on cti.removedSerialNumber=remTire.serialNumber left join
	   tr_tireStockMovement tsm on cti.removedSerialNumber=tsm.serialNumber and cti.installDate=tsm.actionDate and operationOf='IR' left join
	   tr_tireStockTypes st on tsm.stockTypeId=st.id left join
	   tr_tirePositioningDetails tpd on cti.tpId=tpd.tpId and cti.tpdId=tpd.id and cti.trow=tpd.trow and cti.tcolumn=tpd.tcolumn	   
  --WHERE --installDate ='2013/08/13' 
		--and 
        --cti.addedSerialNumber<>removedSerialNumber
  order by cti.installDate