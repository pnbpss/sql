---create view tr_tireInstallationDistBySourceTire as 
select mainStockMovement.*
from tr_tireInstallations ti inner join
tr_tireStockMovement mainStockMovement 
	on ti.installDate=mainStockMovement.actionDate and ti.serialNumber=mainStockMovement.serialNumber
inner join
(
	select distinct install.serialNumber
		--,install.actionDate as installDate
		--,fromStock.actionDate as putInstockDate
		,install.stockTypeId as toStockTypeId
		--,install.stockType as toStockType
		,fromStock.stockTypeId as fromStockTypeId
		,fromStock.stockType as fromStockType
		,install.seq as seqOfInstall
		--,fromStock.seq as seqOfFromStock
	from
	(
		select stm.serialNumber,stm.actionDate,stm.stockTypeId,stm.seq,tst.stockType
		from tr_tireStockMovement stm left join tr_tireStockTypes tst on stm.stockTypeId=tst.id	
		where stm.stockTypeId='9' and tst.internal='N' and stm.operationOf='IR'
	) as install left join (
		select stm.serialNumber,stm.actionDate,stm.stockTypeId,stm.seq,tst.stockType
		from tr_tireStockMovement stm left join tr_tireStockTypes tst on stm.stockTypeId=tst.id	
		where tst.id<>'9' --and stm.operationOf='AD'
	) as fromStock on install.serialNumber=fromStock.serialNumber
) as fetchFromStock on mainStockMovement.seq=fetchFromStock.seqOfInstall

--where install.actionDate between '2013/09/30' and '2013/11/30'
--and install.serialNumber = '2322B106' 
--and install.seq>fromStock.seq
--order by seqOfInstall

--select * from tr_tireStockMovement where serialNumber='TEST1234569' order by seq;
--select * from tr_tireInstallations where serialNumber='TEST1234569';
/*
select * from tr_tires where comment is not null;
select top 50 * from tr_tireStockMovement where 
updateDate between '2013/11/28' and '2013/11/30'
and serialNumber='2322b106'
and ipAddress<>'192.168.0.170';
*/
