select 
row_number() over (order by b.name,v.plateNumber desc,tpch.patchingDate Desc) as rank1
,b.name as branchName
,v.number
,v.plateNumber
,v.oldPlateNumber
,dbo.FN_ConvertDate(r.approvedDate,'D/M/Y') as approvedDate
,vt.typeDesc
,tpch.mileage
,tpd.userCall
,tireBrand.brand as tireBrand
,t.treadPattern
,t.size
,tpchd.treadDept
,tpchd.serialNumber
,case when tpchd.punctureWounds<>0 then '1' end as gotWounds
,case when tpchd.airValveProblem='Y' then '1' end as airValveProblem
,case when tpchd.tubeProblem='Y' then '1' end as tubeProblem
,sp.name as supplierName
,tpchd.expense
,r.id as requestId
from tr_branches b inner join tr_requestForTireMaintenances r on b.id=r.branchId
	 inner join tr_tirePatchingDetails tpchd on r.id=tpchd.requestId
	 inner join tr_tirePatchings tpch on tpch.requestId=tpchd.requestId
	 inner join tr_vehicles v on tpchd.vehicleId=v.id
	 inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	 inner join tr_tirePositioningDetails tpd on tpchd.tpdId=tpd.id and tpchd.tpId=tpd.tpId and tpchd.trow=tpd.trow and tpchd.tcolumn=tpd.tcolumn
	 inner join tr_tires t on tpchd.serialNumber=t.serialNumber
	 inner join tr_brands tireBrand on t.brandId=tireBrand.id
	 inner join tr_suppliers sp on r.supplierId=sp.id
where month(tpch.patchingDate) ='9' and YEAR(tpch.patchingDate)='2014'
	 
	 
