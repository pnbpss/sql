SELECT 
--tis.fromStockType, 
--t.treadPattern, tis.serialNumber, 
--tis.branchName, tis.fromStockTypeId
tis.branchName, t.treadPattern, count(tis.serialNumber) countTire
FROM  tr_tireInstallationDistBySourceTire tis INNER JOIN tr_tires t ON tis.serialNumber = t.serialNumber
group by tis.branchName, t.treadPattern
order by tis.branchName

--select * from tr_tires where treadPattern COLLATE Latin1_General_CS_AS ='r157';
--update tr_tires set serialNumber='B3A1K9751', treadPattern='R157' where serialNumber COLLATE Latin1_General_CS_AS = 'b3a1k9751';
--update tr_tires set serialNumber='B3A2K9906', treadPattern='R157' where serialNumber COLLATE Latin1_General_CS_AS = 'b3a2k9906';