use truck;
IF OBJECT_ID('tempdb..#allRemovedWithBrandAndBranch') is not null DROP TABLE #allRemovedWithBrandAndBranch
--goto processing

IF OBJECT_ID('tempdb..#allInstalled') is not null DROP TABLE #allInstalled
IF OBJECT_ID('tempdb..#allRemoved') is not null DROP TABLE #allRemoved

select * into #allInstalled 
from (
SELECT ti.requestId, ti.installDate AS movementDate, ti.vehicleId AS insVehicleId,ti.serialNumber AS insSerialNumber, 
                      ti.treadDept AS iTreadDept, itpd.userCall AS insWheel, NULL AS remVehicle, NULL AS remSerialNumber, NULL AS rTreadDept, NULL remWheel, 
                      '1' AS movementCase, ti.insSince, NULL AS expense, ti.causeId AS insCause, NULL AS remCause, ti.mileage
/*,'#' as sp, tr.**/ FROM ((tr_tireInstallations ti INNER JOIN
                      tr_vehicleTirePositioningDetails ivtpd ON ti.vehicleId = ivtpd.vehicleId AND ti.tpdId = ivtpd.tpdId AND ti.tpId = ivtpd.tpId AND ti.trow = ivtpd.trow AND 
                      ti.tcolumn = ivtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails itpd ON ivtpd.tpdId = itpd.id AND ivtpd.tpId = itpd.tpId AND ivtpd.trow = itpd.trow AND ivtpd.tcolumn = itpd.tcolumn) LEFT JOIN
                      tr_tireRemovals tr ON /*����������蹶١�ʹ�͡仨ҡ���˹觷��Դ���㹤���ͧ���ǡѹ*/ ti.requestId = tr.requestId AND ti.insSince = tr.insSince AND 
                      ti.installDate = tr.removeDate AND ti.serialNumber <> tr.serialNumber AND ti.vehicleId = tr.vehicleId AND ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND 
                      ti.trow = tr.trow AND ti.tcolumn = tr.tcolumn) LEFT JOIN
                      tr_tireRemovals trb ON /*�����ʹ�͡�Ҩҡ�ö�ѹ�˹���������˹�㹤���ͧ���ǡѹ*/ ti.requestId = trb.requestId AND ti.serialNumber = trb.serialNumber AND
                       ti.insSince = trb.insSince AND 
                      ti.installDate = trb.removeDate
WHERE tr.serialNumber IS NULL AND trb.serialNumber IS NULL
union
SELECT     tr.requestId, tr.removeDate AS movementDate, tic.vehicleId AS insVehicleId, tic.serialNumber AS insSerialNumber, tic.treadDept AS iTreadDept, 
                      rtpd.userCall AS insWheel, tr.vehicleId AS remVehicle, tr.serialNumber AS remSerialNumber, tr.treadDept AS rTreadDept, rtpd.userCall remWheel, 
                      '3' AS movementCase, ti.insSince, tr.expense AS expense, tic.causeId AS insCause, tr.causeId AS remCause, 
                      tr.mileage
/*,tia.serialNumber,'#'as sp,tr.insSince,tic.**/ FROM (tr_tireInstallations ti INNER JOIN
                      tr_tireRemovals tr ON /*�ա�õԴ�����жʹ�ҧ��鹷���������ö㹵��˹� x*/ ti.serialNumber <> tr.serialNumber AND ti.insSince = tr.insSince AND 
                      ti.vehicleId = tr.vehicleId AND ti.installDate = tr.removeDate AND ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND ti.trow = tr.trow AND 
                      ti.tcolumn = tr.tcolumn INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr.vehicleId = rvtpd.vehicleId AND tr.tpdId = rvtpd.tpdId AND tr.tpId = rvtpd.tpId AND tr.trow = rvtpd.trow AND 
                      tr.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn) LEFT JOIN
                      tr_tireInstallations tia ON /*�ҧ��鹷�������������������㹤���ͧ����ҡ�͹��觶١�ʹ�͡���� insSince ���*/ tia.serialNumber = tr.serialNumber AND 
                      tia.requestId = tr.requestId AND tia.insSince = tr.insSince /*tia.vehicleId=tr.vehicleId and */ AND tia.installDate = tr.removeDate INNER JOIN
                      tr_tireInstallations tic ON /*�ա�õԴ����ҧ������ŧ�㹵��˹�������ǡѹ�Ѻ��鹷��ʹ*/ tic.serialNumber <> tr.serialNumber AND 
                      tic.requestId = tr.requestId AND tic.insSince = tr.insSince AND tic.vehicleId = tr.vehicleId AND tic.installDate = tr.removeDate AND tic.tpdId = tr.tpdId AND
                       tic.tpId = tr.tpId AND tic.trow = tr.trow AND tic.tcolumn = tr.tcolumn
WHERE     tia.serialNumber IS NULL --AND (tic.requestId IS NULL)
union
SELECT     ti.requestId, ti.installDate AS movementDate, ti.vehicleId AS insVehicleId, ti.serialNumber AS insSerialNumber, ti.treadDept AS iTreadDept, 
                      itpd.userCall AS insWheel, st.remVehicle remVehicle, ST.remSerialNumber AS remSerialNumber, ST.rTreadDept AS rTreadDept, 
                      ST.remWheel remWheel, '4' AS movementCase, ti.insSince, 
                      /*dbo.verifyMovementExpense(tr2.expense, ST.remExpense)*/ ST.remExpense AS expense, ti.causeId AS insCause, ST.remCauseId AS remCause, 
                      ti.mileage
FROM         (tr_tireInstallations ti LEFT JOIN
                      /*���������� �����������ҧ��� ����ҧ ti �ա�õԴ������ҧ���Ǩҡʵ�ͤ����Ѻ����ͧ���*/ tr_tireRemovals tr ON ti.serialNumber = tr.serialNumber AND 
                      ti.requestId = tr.requestId AND ti.seq = tr.seq AND ti.insSince = tr.insSince) INNER JOIN
                      tr_vehicleTirePositioningDetails ivtpd ON ti.vehicleId = ivtpd.vehicleId AND ti.tpdId = ivtpd.tpdId AND ti.tpId = ivtpd.tpId AND ti.trow = ivtpd.trow AND 
                      ti.tcolumn = ivtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails itpd ON ivtpd.tpdId = itpd.id AND ivtpd.tpId = itpd.tpId AND ivtpd.trow = itpd.trow AND ivtpd.tcolumn = itpd.tcolumn INNER JOIN
                      tr_tireRemovals tr2 ON /*�繡�õԴ��駷�����ҧ�����蹶١�ʹ�͡仨ҡ���˹���鹷�� ti �ʹ*/ ti.insSince = tr2.insSince AND ti.installDate = tr2.removeDate AND 
                      ti.requestId = tr2.requestId AND ti.serialNumber <> tr2.serialNumber AND ti.vehicleId = tr2.vehicleId AND ti.tpdId = tr2.tpdId AND ti.tpId = tr2.tpId AND 
                      ti.trow = tr2.trow AND ti.tcolumn = tr2.tcolumn INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr2.vehicleId = rvtpd.vehicleId AND tr2.tpdId = rvtpd.tpdId AND tr2.tpId = rvtpd.tpId AND 
                      tr2.trow = rvtpd.trow AND tr2.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn CROSS 
                      APPLY dbo.finalRemTire(tr2.requestId, tr2.insSince, tr2.serialNumber, rtpd.userCall, tr2.vehicleId) AS ST
WHERE     tr.serialNumber IS NULL AND ST.remSerialNumber IS NOT NULL
UNION
SELECT ti.requestId, ti.installDate AS movementDate, ti.vehicleId AS insVehicleId, ti.serialNumber AS insSerialNumber,
                       ti.treadDept AS iTreadDept, itpd.userCall AS insWheel, st.remVehicle remVehicle, ST.remSerialNumber AS remSerialNumber, 
                      ST.rTreadDept AS rTreadDept, ST.remWheel remWheel, '7' AS movementCase, ti.insSince, tr2.expense AS expense, ti.causeId AS insCause, 
                      ST.remCauseId AS remCause, ti.mileage
FROM         (tr_tireInstallations ti LEFT JOIN
                      /*���������� �����������ҧ��� ����ҧ ti �ա�õԴ������ҧ���Ǩҡʵ�ͤ����Ѻ����ͧ���*/ tr_tireRemovals tr ON ti.serialNumber = tr.serialNumber AND 
                      ti.requestId = tr.requestId AND ti.seq = tr.seq AND ti.insSince = tr.insSince) INNER JOIN
                      tr_vehicleTirePositioningDetails ivtpd ON ti.vehicleId = ivtpd.vehicleId AND ti.tpdId = ivtpd.tpdId AND ti.tpId = ivtpd.tpId AND ti.trow = ivtpd.trow AND 
                      ti.tcolumn = ivtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails itpd ON ivtpd.tpdId = itpd.id AND ivtpd.tpId = itpd.tpId AND ivtpd.trow = itpd.trow AND ivtpd.tcolumn = itpd.tcolumn INNER JOIN
                      tr_tireRemovals tr2 ON /*�繡�õԴ��駷�����ҧ�����蹶١�ʹ�͡仨ҡ���˹���鹷�� ti �ʹ*/ ti.insSince = tr2.insSince AND ti.installDate = tr2.removeDate AND 
                      ti.requestId = tr2.requestId AND ti.serialNumber <> tr2.serialNumber AND ti.vehicleId = tr2.vehicleId AND ti.tpdId = tr2.tpdId AND ti.tpId = tr2.tpId AND 
                      ti.trow = tr2.trow AND ti.tcolumn = tr2.tcolumn INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr2.vehicleId = rvtpd.vehicleId AND tr2.tpdId = rvtpd.tpdId AND tr2.tpId = rvtpd.tpId AND 
                      tr2.trow = rvtpd.trow AND tr2.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn CROSS 
                      APPLY dbo.finalRemTire(tr2.requestId, tr2.insSince, tr2.serialNumber, rtpd.userCall, tr2.vehicleId) AS ST
WHERE     tr.serialNumber IS NULL AND ST.remSerialNumber IS NULL AND remWheel IS NOT NULL
) allInstalled

select * into #allRemoved 
from (
SELECT     tr.requestId, tr.removeDate AS movementDate, tia.vehicleId AS insVehicleId, tia.serialNumber AS insSerialNumber, NULL AS iTreadDept, NULL 
                      AS insWheel, tr.vehicleId AS remVehicle, tr.serialNumber AS remSerialNumber, tr.treadDept AS rTreadDept, rtpd.userCall remWheel, 
                      '2' AS movementCase, tr.insSince, tr.expense AS expense, NULL AS insCause, tr.causeId AS remCause, tr.mileage
FROM         (tr_tireRemovals tr INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr.vehicleId = rvtpd.vehicleId AND tr.tpdId = rvtpd.tpdId AND tr.tpId = rvtpd.tpId AND tr.trow = rvtpd.trow AND 
                      tr.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn) LEFT JOIN
                      tr_tireInstallations tia ON /*�ҧ���ʹ�����仵Դ��駷��������*/ tia.serialNumber = tr.serialNumber AND tia.requestId = tr.requestId AND 
                      tia.insSince = tr.insSince AND tia.installDate = tr.removeDate /*AND tia.vehicleId = tr.vehicleId*/ AND 
                      tia.tpdId <> tr.tpdId /*AND tia.tpId <> tr.tpId 
      AND tia.trow <> tr.trow 
      AND tia.tcolumn <> tr.tcolumn */ LEFT JOIN
                      tr_tireInstallations tic ON /*������ҧ�������ҵԴ���᷹*/ tic.serialNumber <> tr.serialNumber AND tic.requestId = tr.requestId AND 
                      tic.insSince = tr.insSince AND tic.vehicleId = tr.vehicleId AND tic.installDate = tr.removeDate AND tic.tpdId = tr.tpdId AND tic.tpId = tr.tpId AND 
                      tic.trow = tr.trow AND tic.tcolumn = tr.tcolumn
WHERE     tia.serialNumber IS NULL AND tic.serialNumber IS NULL
union
SELECT     tr.requestId, tr.removeDate AS movementDate, tic.vehicleId AS insVehicleId, tic.serialNumber AS insSerialNumber, tic.treadDept AS iTreadDept, 
                      rtpd.userCall AS insWheel, tr.vehicleId AS remVehicle, tr.serialNumber AS remSerialNumber, tr.treadDept AS rTreadDept, rtpd.userCall remWheel, 
                      '3' AS movementCase, ti.insSince, tr.expense AS expense, tic.causeId AS insCause, tr.causeId AS remCause, 
                      tr.mileage
/*,tia.serialNumber,'#'as sp,tr.insSince,tic.**/ FROM (tr_tireInstallations ti INNER JOIN
                      tr_tireRemovals tr ON /*�ա�õԴ�����жʹ�ҧ��鹷���������ö㹵��˹� x*/ ti.serialNumber <> tr.serialNumber AND ti.insSince = tr.insSince AND 
                      ti.vehicleId = tr.vehicleId AND ti.installDate = tr.removeDate AND ti.tpdId = tr.tpdId AND ti.tpId = tr.tpId AND ti.trow = tr.trow AND 
                      ti.tcolumn = tr.tcolumn INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr.vehicleId = rvtpd.vehicleId AND tr.tpdId = rvtpd.tpdId AND tr.tpId = rvtpd.tpId AND tr.trow = rvtpd.trow AND 
                      tr.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn) LEFT JOIN
                      tr_tireInstallations tia ON /*�ҧ��鹷�������������������㹤���ͧ����ҡ�͹��觶١�ʹ�͡���� insSince ���*/ tia.serialNumber = tr.serialNumber AND 
                      tia.requestId = tr.requestId AND tia.insSince = tr.insSince /*tia.vehicleId=tr.vehicleId and */ AND tia.installDate = tr.removeDate INNER JOIN
                      tr_tireInstallations tic ON /*�ա�õԴ����ҧ������ŧ�㹵��˹�������ǡѹ�Ѻ��鹷��ʹ*/ tic.serialNumber <> tr.serialNumber AND 
                      tic.requestId = tr.requestId AND tic.insSince = tr.insSince AND tic.vehicleId = tr.vehicleId AND tic.installDate = tr.removeDate AND tic.tpdId = tr.tpdId AND
                       tic.tpId = tr.tpId AND tic.trow = tr.trow AND tic.tcolumn = tr.tcolumn
WHERE     tia.serialNumber IS NULL --AND (tic.requestId IS NULL)
union
SELECT     ti.requestId, ti.installDate AS movementDate, ti.vehicleId AS insVehicleId, ti.serialNumber AS insSerialNumber, ti.treadDept AS iTreadDept, 
                      itpd.userCall AS insWheel, st.remVehicle remVehicle, ST.remSerialNumber AS remSerialNumber, ST.rTreadDept AS rTreadDept, 
                      ST.remWheel remWheel, '4' AS movementCase, ti.insSince, 
                      /*dbo.verifyMovementExpense(tr2.expense, ST.remExpense)*/ ST.remExpense AS expense, ti.causeId AS insCause, ST.remCauseId AS remCause, 
                      ti.mileage
FROM         (tr_tireInstallations ti LEFT JOIN
                      /*���������� �����������ҧ��� ����ҧ ti �ա�õԴ������ҧ���Ǩҡʵ�ͤ����Ѻ����ͧ���*/ tr_tireRemovals tr ON ti.serialNumber = tr.serialNumber AND 
                      ti.requestId = tr.requestId AND ti.seq = tr.seq AND ti.insSince = tr.insSince) INNER JOIN
                      tr_vehicleTirePositioningDetails ivtpd ON ti.vehicleId = ivtpd.vehicleId AND ti.tpdId = ivtpd.tpdId AND ti.tpId = ivtpd.tpId AND ti.trow = ivtpd.trow AND 
                      ti.tcolumn = ivtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails itpd ON ivtpd.tpdId = itpd.id AND ivtpd.tpId = itpd.tpId AND ivtpd.trow = itpd.trow AND ivtpd.tcolumn = itpd.tcolumn INNER JOIN
                      tr_tireRemovals tr2 ON /*�繡�õԴ��駷�����ҧ�����蹶١�ʹ�͡仨ҡ���˹���鹷�� ti �ʹ*/ ti.insSince = tr2.insSince AND ti.installDate = tr2.removeDate AND 
                      ti.requestId = tr2.requestId AND ti.serialNumber <> tr2.serialNumber AND ti.vehicleId = tr2.vehicleId AND ti.tpdId = tr2.tpdId AND ti.tpId = tr2.tpId AND 
                      ti.trow = tr2.trow AND ti.tcolumn = tr2.tcolumn INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr2.vehicleId = rvtpd.vehicleId AND tr2.tpdId = rvtpd.tpdId AND tr2.tpId = rvtpd.tpId AND 
                      tr2.trow = rvtpd.trow AND tr2.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn CROSS 
                      APPLY dbo.finalRemTire(tr2.requestId, tr2.insSince, tr2.serialNumber, rtpd.userCall, tr2.vehicleId) AS ST
WHERE     tr.serialNumber IS NULL AND ST.remSerialNumber IS NOT NULL
UNION
SELECT     tr.requestId, tr.removeDate AS movementDate, NULL AS insVehicleId, NULL AS insSerialNumber, NULL AS iTreadDept, NULL AS insWheel, 
                      ST.remVehicle AS remVehicle, ST.remSerialNumber AS remSerialNumber, ST.rTreadDept AS rTreadDept, ST.remWheel remWheel, 
                      '8' AS movementCase, tr.insSince, ST.remExpense AS expense/*, ST.expense AS expense*/ , NULL AS insCause, ST.remCauseId AS remCause, 
                      tr.mileage
/*, ST.remCause AS remCause*/ FROM (tr_tireRemovals tr INNER JOIN
                      tr_vehicleTirePositioningDetails rvtpd ON tr.vehicleId = rvtpd.vehicleId AND tr.tpdId = rvtpd.tpdId AND tr.tpId = rvtpd.tpId AND tr.trow = rvtpd.trow AND 
                      tr.tcolumn = rvtpd.tcolumn INNER JOIN
                      tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn LEFT JOIN
                      tr_tireInstallations ti /* �ա������ҧ tr ��鹷��ʹ仵Դ��駷������ö�ѹ���ǡѹ */ ON tr.insSince = ti.insSince AND tr.requestId = ti.requestId AND 
                      tr.serialNumber = ti.serialNumber AND tr.removeDate = ti.installDate /* AND tr.vehicleId = ti.vehicleId */ AND 
                      tr.tpdId <> ti.tpdId/*AND tr.tpId = ti.tpId 
                      AND tr.trow = ti.trow 
                      AND tr.tcolumn = ti.tcolumn*/ ) LEFT JOIN
                      tr_tireInstallations tia /* 㹵��˹���ͷ�� tr �ʹ ������ҧ�������ҵԴ��駷�᷹ */ ON tia.serialNumber <> tr.serialNumber AND tia.requestId = tr.requestId AND 
                      tia.insSince = tr.insSince AND tia.installDate = tr.removeDate AND tr.vehicleId = tr.vehicleId AND tia.tpdId = tr.tpdId AND tia.tpId = tr.tpId AND 
                      tia.trow = tr.trow AND 
                      tia.tcolumn = tr.tcolumn /*CROSS APPLY dbo.finalRemTireOnlyRemove(tr.requestId, tr.insSince, tr.serialNumber, rtpd.userCall, tr.vehicleId) AS ST*/ CROSS
                       APPLY dbo.finalRemTire(tr.requestId, tr.insSince, tr.serialNumber, rtpd.userCall, tr.vehicleId) AS ST
WHERE     ti.serialNumber IS NOT NULL AND tia.serialNumber IS NULL AND ST.remSerialNumber IS NOT NULL
) allRemoved

CREATE NONCLUSTERED INDEX [allInstalledIdx] ON [dbo].[#allInstalled] ([insSerialNumber],[mileage])INCLUDE ([requestId],[movementDate],[insVehicleId],[iTreadDept],[insWheel])
CREATE NONCLUSTERED INDEX [allRemovedIdx]ON [dbo].[#allRemoved] ([remVehicle],[movementDate])INCLUDE ([requestId],[remSerialNumber],[rTreadDept],[remWheel],[mileage])

processing:

--select distinct ',['+brand+']' from #allRemoved allr left join tr_tires t on allr.remSerialNumber=t.serialNumber left join tr_brands br on t.brandId=br.id left join tr_requestForTireMaintenances r on allr.requestId=r.id left join tr_branches brn on r.branchId=brn.id left join tr_causeOfRemove cor on allr.remCause=cor.id


select allr.requestId
,datepart(year,allr.movementDate)yRemove
,datepart(month,allr.movementDate)mRemove
,allr.remVehicle,allr.remSerialNumber,allr.rTreadDept,allr.remWheel,allr.movementCase
,allr.expense
,cor.[description] causeOfRemove
,case when cor.id in (9,10,11,12,13,14,15,100) then 100 else cor.id end causeOfRemoveId
--,allr.mileage,allr.insCause,allr.insSince,allr.insVehicleId,allr.insSerialNumber,allr.iTreadDept,allr.insWheel
,br.brand,brn.abbreviation fleet
into #allRemovedWithBrandAndBranch 
from #allRemoved allr left join tr_tires t on allr.remSerialNumber=t.serialNumber left join tr_brands br on t.brandId=br.id left join tr_requestForTireMaintenances r on allr.requestId=r.id left join tr_branches brn on r.branchId=brn.id left join tr_causeOfRemove cor on allr.remCause=cor.id
where 1=1
and brn.id!=1000

--and cor.[description] in ('�з�ᵡ����','�١�Ҵ��','�պҴ��','�ҧ���','���Դ','����','���˵�����','�֡�Դ����','����͡')
--select requestId,yRemove,mRemove,remVehicle,remSerialNumber,rTreadDept,remWheel,movementCase,expense,causeOfRemove,brand,fleet from #allRemovedWithBrandAndBranch 
--select * from tr_causeOfremove


select fleetBrand,isnull([1],0)c1,isnull([2],0)c2,isnull([3],0)c3,isnull([4],0)c4,isnull([5],0)c5,isnull([6],0)c6,isnull([7],0)c7,isnull([9],0)c9,isnull([100],0)c100,aggr.sumAll
,cast(case when aggr.sumAll=0 then 0 else isnull([1],0)*100.00/aggr.sumAll end as decimal(5,2)) percentOutOfTread_2
,cast(case when aggr.sumAll=0 then 0 else isnull([2],0)*100.00/aggr.sumAll end as decimal(5,2)) percentFlat_2
,cast(case when aggr.sumAll=0 then 0 else isnull([4],0)*100.00/aggr.sumAll end as decimal(5,2)) percentExplode_4
,cast(case when aggr.sumAll=0 then 0 else isnull([5],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWear_5
,cast(case when aggr.sumAll=0 then 0 else isnull([7],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWound_7
,cast(case when aggr.sumAll=0 then 0 else isnull([9],0)*100.00/aggr.sumAll end as decimal(5,2)) percentSwell_9
from
(
	select causeOfRemoveId,fleet+'_'+brand fleetBrand,count(*)cnt from #allRemovedWithBrandAndBranch group by causeOfRemoveId,brand,fleet
) t pivot(sum(cnt) for causeOfRemoveId in ([5],[9],[100],[7],[1],[4],[3],[6],[2]))p
cross apply (select(isnull([5],0)+isnull([9],0)+isnull([100],0)+isnull([7],0)+isnull([1],0)+isnull([4],0)+isnull([3],0)+isnull([6],0)+isnull([2],0))sumAll) aggr

select fleetBrand,isnull([1],0)c1,isnull([2],0)c2,isnull([3],0)c3,isnull([4],0)c4,isnull([5],0)c5,isnull([6],0)c6,isnull([7],0)c7,isnull([9],0)c9,isnull([100],0)c100,aggr.sumAll
,cast(case when aggr.sumAll=0 then 0 else isnull([1],0)*100.00/aggr.sumAll end as decimal(5,2)) percentOutOfTread_2
,cast(case when aggr.sumAll=0 then 0 else isnull([2],0)*100.00/aggr.sumAll end as decimal(5,2)) percentFlat_2
,cast(case when aggr.sumAll=0 then 0 else isnull([4],0)*100.00/aggr.sumAll end as decimal(5,2)) percentExplode_4
,cast(case when aggr.sumAll=0 then 0 else isnull([5],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWear_5
,cast(case when aggr.sumAll=0 then 0 else isnull([7],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWound_7
,cast(case when aggr.sumAll=0 then 0 else isnull([9],0)*100.00/aggr.sumAll end as decimal(5,2)) percentSwell_9
from(select causeOfRemoveId,brand fleetBrand,count(*)cnt from #allRemovedWithBrandAndBranch group by causeOfRemoveId,brand) t pivot(sum(cnt) for causeOfRemoveId in ([5],[9],[100],[7],[1],[4],[3],[6],[2]))p 
cross apply (select(isnull([5],0)+isnull([9],0)+isnull([100],0)+isnull([7],0)+isnull([1],0)+isnull([4],0)+isnull([3],0)+isnull([6],0)+isnull([2],0))sumAll) aggr

select fleetBrand,isnull([1],0)c1,isnull([2],0)c2,isnull([3],0)c3,isnull([4],0)c4,isnull([5],0)c5,isnull([6],0)c6,isnull([7],0)c7,isnull([9],0)c9,isnull([100],0)c100,aggr.sumAll
,cast(case when aggr.sumAll=0 then 0 else isnull([1],0)*100.00/aggr.sumAll end as decimal(5,2)) percentOutOfTread_2
,cast(case when aggr.sumAll=0 then 0 else isnull([2],0)*100.00/aggr.sumAll end as decimal(5,2)) percentFlat_2
,cast(case when aggr.sumAll=0 then 0 else isnull([4],0)*100.00/aggr.sumAll end as decimal(5,2)) percentExplode_4
,cast(case when aggr.sumAll=0 then 0 else isnull([5],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWear_5
,cast(case when aggr.sumAll=0 then 0 else isnull([7],0)*100.00/aggr.sumAll end as decimal(5,2)) percentWound_7
,cast(case when aggr.sumAll=0 then 0 else isnull([9],0)*100.00/aggr.sumAll end as decimal(5,2)) percentSwell_9
from
(
	select causeOfRemoveId,fleet fleetBrand,count(*)cnt from #allRemovedWithBrandAndBranch group by causeOfRemoveId,fleet
) t pivot(sum(cnt) for causeOfRemoveId in ([5],[9],[100],[7],[1],[4],[3],[6],[2]))p
cross apply (select(isnull([5],0)+isnull([9],0)+isnull([100],0)+isnull([7],0)+isnull([1],0)+isnull([4],0)+isnull([3],0)+isnull([6],0)+isnull([2],0))sumAll) aggr


/*
select 
ai.*
--ai.requestId,ai.insSerialNumber,ai.iTreadDept,v.plateNumber,ai.insWheel,ai.movementDate,ai.mileage,vt.typeCode
from #allInstalled ai 
left join tr_vehicles v on  ai.insVehicleId=v.id
left join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
where ai.mileage is not null and mileage !=0 and vt.typeCode in ('H','S')
and ai.insSerialNumber='1312454999';


select 
ar.*
--ar.requestId,ar.remSerialNumber,ar.rTreadDept,vr.plateNumber,ar.remWheel,ar.movementDate,ar.mileage,vtr.typeCode
from #allRemoved ar 
left join tr_vehicles vr on  ar.insVehicleId=vr.id
left join tr_vehicleTypes vtr on vr.vehicleTypeId=vtr.id
where ar.mileage is not null and ar.mileage !=0 and vtr.typeCode in ('H','S');



select 
distinct
ai.requestId aiReq,ai.insSerialNumber,ai.iTreadDept,vi.plateNumber viPlateNumber,ai.insWheel,ai.movementDate aimmd,ai.mileage aiMileage,vti.typeCode vtiCode
,ar.requestId arReq,ar.remSerialNumber,ar.rTreadDept,vr.plateNumber vrPlateNumber,ar.remWheel,ar.movementDate armmd,ar.mileage arMileage,vtr.typeCode vtrCode
,ar.mileage-ai.mileage mileDiff, datediff(day,ai.movementDate,ar.movementDate) dDiff
from #allInstalled ai
left join tr_vehicles vi on  ai.insVehicleId=vi.id
left join tr_vehicleTypes vti on vi.vehicleTypeId=vti.id

left join #allRemoved ar on ai.insVehicleId=ar.remVehicle and ai.insSerialNumber=ar.remSerialNumber
left join tr_vehicles vr on  ar.remVehicle=vr.id
left join tr_vehicleTypes vtr on vr.vehicleTypeId=vtr.id 
where 1=1
and ai.mileage is not null 
and ai.mileage !=0 and vti.typeCode in ('H','S')
and ar.mileage is not null 
and ar.mileage !=0 and vtr.typeCode in ('H','S')
and ar.mileage-ai.mileage>0 
and ar.movementDate>ai.movementDate
and ai.insSerialNumber in ('1315060664','1506G37115','1508F40680','1312454999')
order by ai.insSerialNumber,ai.movementDate,ar.movementDate

*/