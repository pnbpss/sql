/*
SELECT     ti.requestId, ti.installDate AS movementDate, ti.vehicleId AS insVehicleId, ti.serialNumber AS insSerialNumber, ti.treadDept AS iTreadDept, 
                      itpd.userCall AS insWheel, tr2.vehicleId AS remVehicle, tr2.serialNumber AS remSerialNumber, tr2.treadDept AS rTreadDept, rtpd.userCall remWheel, 
                      '4' AS movementCase, ti.insSince,tr3.requestId
FROM         (((tr_tireInstallations ti LEFT JOIN
                      tr_tireRemovals tr ON ti.serialNumber = tr.serialNumber AND ti.vehicleId = tr.vehicleId AND ti.installDate = tr.installDate AND 
                      ti.seq = tr.seq 
              ) 
	  
      /*ti �繡�õԴ����ҧ�·������ա�öʹ*/      
      INNER JOIN
		  tr_vehicleTirePositioningDetails ivtpd ON ti.vehicleId = ivtpd.vehicleId AND ti.tpdId = ivtpd.tpdId AND ti.tpId = ivtpd.tpId AND ti.trow = ivtpd.trow AND 
		  ti.tcolumn = ivtpd.tcolumn INNER JOIN
		  tr_tirePositioningDetails itpd ON ivtpd.tpdId = itpd.id AND ivtpd.tpId = itpd.tpId AND ivtpd.trow = itpd.trow AND ivtpd.tcolumn = itpd.tcolumn INNER JOIN
		  tr_tireRemovals tr2 ON /*�繡�õԴ��駷�����ҧ�����蹶١�ʹ�͡仨ҡ���˹���鹷�� ti �ʹ*/ ti.insSince = tr2.insSince AND ti.installDate = tr2.removeDate AND 
		  ti.requestId = tr2.requestId AND ti.serialNumber <> tr2.serialNumber AND ti.vehicleId = tr2.vehicleId AND ti.tpdId = tr2.tpdId AND ti.tpId = tr2.tpId AND 
		  ti.trow = tr2.trow AND ti.tcolumn = tr2.tcolumn 
		  
		  
		  INNER JOIN
		  tr_tireInstallations ti2 ON /*�ա������ҧ��鹷��ʹ�͡�ҡ���˹觷��Դ��� ti 仵Դ����������*/ 
		  tr2.serialNumber = ti2.serialNumber AND 
		  tr2.insSince = ti2.insSince AND tr2.removeDate = ti2.installDate 
		  
		  INNER JOIN
		  tr_vehicleTirePositioningDetails rvtpd ON tr2.vehicleId = rvtpd.vehicleId AND tr2.tpdId = rvtpd.tpdId AND tr2.tpId = rvtpd.tpId AND 
		  tr2.trow = rvtpd.trow AND tr2.tcolumn = rvtpd.tcolumn INNER JOIN
		  tr_tirePositioningDetails rtpd ON ivtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn
		  ) 
		  LEFT JOIN
		  tr_tireRemovals tr3 ON /*�ҧ� ti2 ������ա�öʹ*/ ti2.requestId = tr3.requestId AND ti2.serialNumber = tr3.serialNumber AND 
		  ti2.insSince = tr3.insSince AND ti2.installDate = tr3.installDate AND ti2.seq = tr3.seq
		  
		  ) left join 
		  (
			select 'a' as serialNumber
		  ) swapTire on ti.serialNumber=swaptire.serialNumber

WHERE tr.serialNumber IS NULL and swapTire.serialNumber is null and ti.requestId='1301'

*/
select 
ti1.requestId, ti1.installDate AS movementDate, ti1.vehicleId AS insVehicleId, ti1.serialNumber AS insSerialNumber, ti1.treadDept AS iTreadDept, 
rtpd.userCall AS insWheel, tr1.vehicleId AS remVehicle, tr1.serialNumber AS remSerialNumber, tr1.treadDept AS rTreadDept, rtpd.userCall remWheel, 
'6' AS movementCase, ti1.insSince
--ti1.requestId,ti1.serialNumber,tr1.serialNumber
from (((tr_tireInstallations ti1 inner join tr_tireRemovals tr2 on
	 ti1.serialNumber=tr2.serialNumber
	 and ti1.requestId=tr2.requestId
	 and ti1.insSince=tr2.insSince)
	 inner join tr_tireInstallations ti2 on
	 tr2.serialNumber<>ti2.serialNumber
	 and tr2.requestId=ti2.requestId
	 and tr2.insSince=ti2.insSince
	 and tr2.seq=ti2.seq
	 and tr2.removeDate=ti2.installDate
	 and tr2.vehicleId=ti2.vehicleId
	 and tr2.tpdId=ti2.tpdId
	 and tr2.tpId=ti2.tpId
	 and tr2.trow=ti2.trow
	 and tr2.tcolumn=ti2.tcolumn)
	 inner join tr_tireRemovals tr1 on
	 ti1.serialNumber>tr1.serialNumber	/*����ҡ�������ͻ�ͧ�ѹ�������ҧ�͡������ Ẻ��Ѻ���ѹ*/
	 and ti1.requestId=tr1.requestId
	 and ti1.insSince=tr1.insSince
	 and ti1.seq=tr1.seq
	 and ti1.installDate=tr1.removeDate
	 and ti1.vehicleId=tr1.vehicleId
	 and ti1.tpdId=tr1.tpdId
	 and ti1.tpId=tr1.tpId
	 and ti1.trow=tr1.trow
	 and ti1.tcolumn=tr1.tcolumn
	 
	 and ti2.requestId=tr1.requestId
	 and ti2.insSince=tr1.insSince
	 and ti2.serialNumber=tr1.serialNumber)
	 INNER JOIN tr_vehicleTirePositioningDetails rvtpd ON 
		ti1.vehicleId = rvtpd.vehicleId 
		AND ti1.tpdId = rvtpd.tpdId 
		AND ti1.tpId = rvtpd.tpId 
		AND ti1.trow = rvtpd.trow 
		AND ti1.tcolumn = rvtpd.tcolumn 
	 INNER JOIN tr_tirePositioningDetails rtpd ON rvtpd.tpdId = rtpd.id AND rvtpd.tpId = rtpd.tpId AND rvtpd.trow = rtpd.trow AND rvtpd.tcolumn = rtpd.tcolumn
order by ti1.requestId  desc
