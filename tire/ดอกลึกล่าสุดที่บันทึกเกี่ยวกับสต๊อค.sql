goto skipper;
/*
select serialNumber, max(actionDate) maxActionDate,treadDept
from tr_tireStockMovement
group by treadDept,serialNumber
order by serialNumber,max(actionDate)
*/
--�͡�֡����ش�ʵ�ͤ
--create view tr_lastStockMovementTreadDept as
select mst.serialNumber, mst.maxActionDate,st.treadDept,st.operationOf,st.stockTypeId,stt.internal,stt.stockType
from (
select 
	--top 1 
	serialNumber, max(actionDate) maxActionDate
from tr_tireStockMovement
--where serialNumber='B2S1K0611'
group by serialNumber
--order by serialNumber,max(actionDate) desc
) as mst left join tr_tireStockMovement st on mst.serialNumber=st.serialNumber and mst.maxActionDate=st.actionDate left join
tr_tireStockTypes stt on st.stockTypeId=stt.id


--�͡�֡����ش㹡�úѹ�֡�Դ������/�����ʹԵ
select instTDept.maxInstallDate,instTDept.serialNumber,ti.treadDept as instTreadDept,ti.vehicleId
	   --,audTDept.serialNumber,audTDept.formId,audTDept.maxAuditDate, audTDept.treadDept as auditTreadDept
	   
from (
	select max(installDate) maxInstallDate,serialNumber
	from tr_tireInstallations
	--where serialNumber='BVO35637H'
	group by serialNumber
) as instTDept left join tr_tireInstallations ti on instTDept.maxInstallDate=ti.installDate and instTDept.serialNumber=ti.serialNumber
--��ͧ��ҡ���ʹԵ����Ҽ�ǡ����
left join (
--
		select maxAd.serialNumber,maxAd.formId,maxAd.maxAuditDate, trDept.treadDept
		from (
		--���ѹ����ʹԵ����ش�ͧ�ҧ�������
		select tafd.serialNumber serialNumber,taf.id formId,max(taf.auditDate) maxAuditDate
		from tr_tireAuditingFormDetails tafd left join tr_tireAuditingForm taf on tafd.formId = taf.id
		--where taf.auditDate is not null
		group by tafd.serialNumber,taf.id
		) as maxAd left join (
			-- ����Ҵ͡�ҧ�֡�������
			select tafd.formId, tafd.serialNumber, tafd.treadDept
			from tr_tireAuditingFormDetails tafd
		) trDept on maxAd.formId=trDept.formId and maxAd.serialNumber=trDept.serialNumber
--	
) as audTDept on instTDept.serialNumber=audTDept.serialNumber

skipper:
--create view tr_lastInstalledTreadDept as
select tireMaxInstDate.serialNumber, tireMaxInstDate.maxInstallDate as installDate,tireVt.treadDept
	   ,tireVt.requestId,tireVt.vehicleId,tireVt.tpdId,tireVt.tpId,tireVt.tcolumn,tireVt.trow
from (
	select serialNumber,max(installDate) maxInstallDate
	from tr_tireInstallations
	group by serialNumber
) as tireMaxInstDate left join (
	select serialNumber,installDate,vehicleId,treadDept,requestId,tpdId,tpId,trow,tcolumn
	from tr_tireInstallations
) as tireVt on tireMaxInstDate.maxInstallDate=tireVt.installDate and tireMaxInstDate.serialNumber=tireVt.serialNumber
