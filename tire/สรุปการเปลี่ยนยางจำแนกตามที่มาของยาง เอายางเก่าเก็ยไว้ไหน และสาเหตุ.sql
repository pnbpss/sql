/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [branchName],[fromStockType],count([serialNumber]) amountOfTire,month([actionDate]) [month]
FROM [Truck].[dbo].[tr_tireInstallationDistBySourceTire]
group by [branchName],[fromStockType], month([actionDate])
order by [branchName],[fromStockType], month([actionDate])


SELECT [branchName],[stockType] keepInStock, count([serialNumber]) as amountOfTire ,month([actionDate]) [month]
FROM [Truck].[dbo].[tr_tireRemovalsDistByKeepTire]
group by [branchName],[stockType] , month([actionDate])
order by [branchName],[stockType] , month([actionDate])

--select * from tr_tireRemovalsDistByKeepTire;


SELECT [branchName],[description],count([removedSerialNumber]) as amountOfTire ,month([removedDate]) as [month]
FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
group by [branchName],[description],month([removedDate])
order by [branchName],[description],month([removedDate])

--  select * from tr_tireStockMovement where serialNumber='BVP16975H'