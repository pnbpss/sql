use truck;
IF OBJECT_ID('tempdb..#ccltd') is not null DROP TABLE #ccltd
IF OBJECT_ID('tempdb..#cntAvgStdev') is not null DROP TABLE #cntAvgStdev
--goto processing
IF OBJECT_ID('tempdb..#allIR') is not null DROP TABLE #allIR


;with insRem (remReqId,remTreadDept,tinsTreadDept,serialNumber,removeDate,installDate,insReqId
,vehicleId,rInsSince,iInsSince,tpdId,tpId,trow,tcolumn) as
(
select 
tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
from tr_tireRemovals tr inner join tr_tireInstallations ti
on  tr.serialNumber=ti.serialNumber 
	and tr.seq=ti.seq
	and tr.installDate=ti.installDate
	and tr.vehicleId=ti.vehicleId
	and tr.tpdId=ti.tpdId
	and tr.tpId=ti.tpId
	and tr.trow=ti.trow
	and tr.tcolumn=ti.tcolumn
where 1=1
union all
select 
tr.requestId as remReqId,tr.treadDept remTreadDept,ti.treadDept as insTreadDept
,tr.serialNumber,tr.removeDate,ti.installDate,ti.requestId as insReqId
,tr.vehicleId,tr.insSince as rInsSince,ti.insSince as iInsSince
,tr.tpdId,tr.tpId,tr.trow,tr.tcolumn
from tr_tireRemovals tr inner join tr_tireInstallations ti
on  tr.serialNumber=ti.serialNumber 
	and tr.seq=ti.seq
	and tr.installDate=ti.installDate
	and tr.vehicleId=ti.vehicleId
	and tr.tpdId=ti.tpdId
	and tr.tpId=ti.tpId
	and tr.trow=ti.trow
	and tr.tcolumn=ti.tcolumn
	inner join insRem ir on
	tr.serialNumber=ir.serialNumber
	and tr.insSince=ir.iInsSince
	and tr.vehicleId=ir.vehicleId
	and tr.requestId=ir.insReqId
	and tr.removeDate=ir.installDate
)
select 
--top 1 
distinct
ir.remReqId,ir.remTreadDept,ir.tinsTreadDept
,ir.serialNumber,ir.removeDate,ir.installDate,ir.insReqId
,ir.vehicleId,ir.rInsSince,ir.iInsSince
,tpd.userCall,ir.tpdId,ir.tpId,ir.trow,ir.tcolumn
into #allIR
from insRem ir inner join tr_tirePositioningDetails tpd
on ir.tpdId=tpd.id and ir.tpId=tpd.tpId and ir.trow=tpd.trow and ir.tcolumn=tpd.tcolumn
order by insReqId;

--select * from #allIR

processing:
select 
allIR.insReqId
,vi.plateNumber
--,allIR.vehicleId,allIR.rInsSince
--,allIR.iInsSince
,allIR.tinsTreadDept
,allIR.remReqId,allIR.remTreadDept,allIR.serialNumber
,dbo.FN_ConvertDate(allIR.removeDate,'Y/M/D') removeDate
,dbo.FN_ConvertDate(allIR.installDate,'Y/M/D') installDate
,allIR.userCall wp
--,allIR.tpdId,allIR.tpId,allIR.trow,allIR.tcolumn
,ti.mileage insMileage
,tr.mileage remMileage
,(tr.mileage-ti.mileage) mileageDif
,(allIR.tinsTreadDept-allIR.remTreadDept) tdDif
,datediff(day,allIR.installDate,allIR.removeDate) dateDif
,itd.treadDept itd,t.price initPrice 
,cast(calc.bahtPerMill as decimal(18,1)) bahtPerMill
,calc2.usedInBaht 
,calc3.millPerKM
,calc4.bahtPerKM
,brnd.brand
,br.abbreviation branch
,cor.[description] causeOfRemove
into #ccltd
from #allIR allIR 
left join tr_tireInstallations ti on allIR.insReqID=ti.requestId and allIR.serialNumber=ti.serialNumber
left join tr_vehicles vi on  ti.vehicleId=vi.id
left join tr_vehicleTypes vti on vi.vehicleTypeId=vti.id

left join tr_tireRemovals tr on allIR.remReqID=tr.requestId and allIR.serialNumber=tr.serialNumber
left join tr_vehicles vr on  tr.vehicleId=vr.id
left join tr_vehicleTypes vtr on vr.vehicleTypeId=vtr.id 
left join tr_causeOfRemove cor on tr.causeId=cor.id

left join tr_tires t on allIR.serialNumber=t.serialNumber
left join tr_initTireTreadDept itd on t.serialNumber=itd.serialNumber

left join tr_brands brnd on t.brandId=brnd.id
left join tr_requestForTireMaintenances rq on allIR.remReqID=rq.id
left join tr_branches br on rq.branchId=br.id
cross apply (select(cast(case when itd.treadDept=0 then 0 else (t.price/itd.treadDept) end as decimal(18,1)))bahtPerMill)calc
cross apply (select(calc.bahtPerMill*(allIR.tinsTreadDept-allIR.remTreadDept))usedInBaht)calc2
cross apply (select(cast(case when (tr.mileage-ti.mileage)=0 then 0 else (allIR.tinsTreadDept-allIR.remTreadDept)/(tr.mileage-ti.mileage) end as decimal(11,10)))millPerKM)calc3
cross apply (select(cast(case when (tr.mileage-ti.mileage)=0 then 0 else (calc2.usedInBaht)/(tr.mileage-ti.mileage) end as decimal(15,10)))bahtPerKM)calc4
where 1=1
and ti.mileage is not null 
and ti.mileage !=0 
--and vti.typeCode in ('H','S') and vtr.typeCode in ('H','S')
and tr.mileage is not null 
and tr.mileage !=0 
and bahtPerMill!=0 and bahtPerMill<1000
and tr.mileage-ti.mileage>0
and allIR.tinsTreadDept!=allIR.remTreadDept
and datediff(day,allIR.installDate,allIR.removeDate) > 10
and allIR.userCall not in ('SP1','SP2','SP2�')
and allIR.serialNumber not like 'TEST%'
and t.newOrRetread='R'

--select * from #ccltd where brand='Jinyu' and branch='LB' and cast(bahtPerKM as decimal(18,2))<0.99
--select * from #ccltd order by bahtPerMill desc

select brand,branch,count(*)cnt
,cast(avg(bahtPerKM) as decimal(9,4))avg_bpKM
,cast(stdev(bahtPerKM) as decimal(6,3)) stdev_bpKM 
into #cntAvgStdev
from #ccltd 
where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)'
group by brand,branch 
--order by brand,avg(bahtPerKM)

select cnt.brand
,cnt.BB cBB,case when cnt.BB=0 then null else av.BB end avBB,case when cnt.BB=0 or cnt.BB=1 then null else avd.BB end stdevBB
,cnt.KK cKK,case when cnt.KK=0 then null else av.KK end avKK,case when cnt.KK=0 or cnt.KK=1 then null else avd.KK end stdevKK
,cnt.LB cLB,case when cnt.LB=0 then null else av.LB end avLB,case when cnt.LB=0 or cnt.LB=1 then null else avd.LB end stdevLB
,cnt.LP cLP,case when cnt.LP=0 then null else av.LP end avLP,case when cnt.LP=0 or cnt.LP=1 then null else avd.LP end stdevLP
,cnt.NK cNK,case when cnt.NK=0 then null else av.NK end avNK,case when cnt.NK=0 or cnt.NK=1 then null else avd.NK end stdevNK
,cnt.TG cTG,case when cnt.TG=0 then null else av.TG end avTG,case when cnt.TG=0 or cnt.TG=1 then null else avd.TG end stdevTG
,(cnt.BB+cnt.KK+cnt.LB+cnt.LP+cnt.NK+cnt.TG) csumOper
,cast(avgallBrand.avgPerKM as decimal(6,4)) allBrandAvgPerKM
,cast(stdevallBrand.[stdev] as decimal(6,4)) allBrandstdev
from 
(
	select brand,isnull([BB],0)BB,isnull([KK],0)KK,isnull([LB],0)LB,isnull([LP],0)LP,isnull([NK],0)NK,isnull([TG],0)TG
	from (
		select brand,branch,max(cnt)cnt
		from #cntAvgStdev t group by branch,brand
	) t
	pivot (max(cnt) for branch in ([BB],[KK],[LB],[LP],[NK],[TG])) p1
) cnt left join
(
	select brand,isnull([BB],0)BB,isnull([KK],0)KK,isnull([LB],0)LB,isnull([LP],0)LP,isnull([NK],0)NK,isnull([TG],0)TG
	from (
		select brand,branch,max(avg_bpKM)avg_bpKM
		from #cntAvgStdev t group by branch,brand
	) t
	pivot (max(avg_bpKM) for branch in ([BB],[KK],[LB],[LP],[NK],[TG])) p1
) av on cnt.brand=av.brand
left join 
(
	select brand,isnull([BB],0)BB,isnull([KK],0)KK,isnull([LB],0)LB,isnull([LP],0)LP,isnull([NK],0)NK,isnull([TG],0)TG
	from (
		select brand,branch,max(stdev_bpKM)stdev_bpKM		
		from #cntAvgStdev t group by branch,brand
	) t
	pivot (max(stdev_bpKM) for branch in ([BB],[KK],[LB],[LP],[NK],[TG])) p1
) avd on cnt.brand=avd.brand
left join 
(
	select brand,avg(bahtPerKM)avgperKM from #ccltd t where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)'  group by brand
) avgallBrand on cnt.brand=avgallBrand.brand
left join 
(
	select brand,stdev(bahtPerKM)[stdev] from #ccltd t where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)' group by brand
) stdevallBrand on cnt.brand=stdevallBrand.brand


--��º�͡���繵��˹����
--select * from #ccltd where cast(bahtPerKM as decimal(18,2))<0.99
--select count(*)cnt,avg(bahtPerKM),stdev(bahtPerKM) from #ccltd where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990

select branch,[1]w1,[2]w2,[3]w3,[4]w4,[5]w5,[6]w6,[7]w7,[8]w8,[9]w9,[10]w10,[11]w11,[12]w12,[13]w13,[14]w14,[15]w15,[16]w16,[17]w17,[18]w18,[19]w19,[20]w20,[21]w21,[22]w22,[A]wA,[B]wB
from (
	select branch,wp,cast(AVG(bahtPerKM) as decimal(18,4))avgBahtPerKM 
	from #ccltd 
	where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)'
	group by branch,wp 
	--order by branch,wp
) t pivot (
	max(avgBahtPerKM) for wp in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B])
) p

select branch,[1]w1,[2]w2,[3]w3,[4]w4,[5]w5,[6]w6,[7]w7,[8]w8,[9]w9,[10]w10,[11]w11,[12]w12,[13]w13,[14]w14,[15]w15,[16]w16,[17]w17,[18]w18,[19]w19,[20]w20,[21]w21,[22]w22,[A]wA,[B]wB
from (
	select brand branch,wp,cast(AVG(bahtPerKM) as decimal(18,4))avgBahtPerKM 
	from #ccltd 
	where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)'
	group by brand,wp 
	--order by branch,wp
) t pivot (
	max(avgBahtPerKM) for wp in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B])
) p
order by branch


select branch,[1]w1,[2]w2,[3]w3,[4]w4,[5]w5,[6]w6,[7]w7,[8]w8,[9]w9,[10]w10,[11]w11,[12]w12,[13]w13,[14]w14,[15]w15,[16]w16,[17]w17,[18]w18,[19]w19,[20]w20,[21]w21,[22]w22,[A]wA,[B]wB
from (
	select branch+'_'+brand branch,wp,cast(AVG(bahtPerKM) as decimal(18,4))avgBahtPerKM 
	from #ccltd 
	where cast(bahtPerKM as decimal(18,2)) between 0.001 and 0.990 and brand!='(n/a)'
	group by branch,brand,wp 
	--order by branch,wp
) t pivot (
	max(avgBahtPerKM) for wp in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[A],[B])
) p
order by branch


/*select distinct '['+branch+'],' from #cntAvgStdev [BB],[KK],[LB],[LP],[NK],[TG]*/

--select * from #allIR where userCall in ('SP2�','11','12','13','14','15','16','17','18','19','20','21','22')
--select top 11 count(*),brandId from tr_tires group by brandId order by count(*) desc
--select id, brand from tr_brands where id in (7,18,5,21,31,30,29,22,28,25)
--select distinct replace(b.brand,' ','')brand from tr_tires t inner join tr_brands b on t.brandId=b.id
/*
5	Bridgestone
7	Michelin
18	Jinyu
21	Otani
22	Double Coin
25	continental
28	MIRAGE
29	CST
30	CHENGSHAN
31	DRC
*/
/*
select p1.causeOfRemove
,p1.[BB]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='BB' and causeOfRemove=p1.causeOfRemove) [BB_DRC]
,p1.[KK]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='KK' and causeOfRemove=p1.causeOfRemove) [KK_DRC]
,p1.[LB]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='LB' and causeOfRemove=p1.causeOfRemove) [LB_DRC]
,p1.[LP]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='LP' and causeOfRemove=p1.causeOfRemove) [LP_DRC]
,p1.[NK]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='NK' and causeOfRemove=p1.causeOfRemove) [NK_DRC]
,p1.[TG]
	,(select count(*) from #ccltd where brand='Bridgestone' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_Bridgestone]
	,(select count(*) from #ccltd where brand='Michelin' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_Michelin]
	,(select count(*) from #ccltd where brand='Jinyu' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_Jinyu]
	,(select count(*) from #ccltd where brand='Otani' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_Otani]
	,(select count(*) from #ccltd where brand='Double Coin' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_DoubleCoin]
	,(select count(*) from #ccltd where brand='continental' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_Continental]
	,(select count(*) from #ccltd where brand='MIRAGE' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_MIRAGE]	
	,(select count(*) from #ccltd where brand='CST' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_CST]
	,(select count(*) from #ccltd where brand='CHENGSHAN' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_CHENGSHAN]
	,(select count(*) from #ccltd where brand='DRC' and branch='TG' and causeOfRemove=p1.causeOfRemove) [TG_DRC]
from
(	
	select count(*)cnt,causeOfRemove,branch
	from #ccltd 
	where 1=1
	and brand in (select brand from tr_brands where id in (7,18,5,21,31,30,29,22,28,25))	
	group by causeOfRemove,branch		
) t pivot(max(cnt) for branch in ([BB],[KK],[LB],[LP],[NK],[TG])) p1


select branch,causeOfRemove,[ADVANCE],[apollo],[BlueStreak],[Bridgestone],[CHENGSHAN],[CM335WESTLAKE],[CMIC],[continental],[CST],[Deestone],[DoubleCoin],[DRC],[Dunlop],[Goodyear],[Jinyu],[KINGSKY],[linglong],[Michelin],[MIRAGE],[Otani],[Volvo],[Yokohama],[(n/a)]
,aggr.sumAll
from (
	select count(*)cnt,causeOfRemove,replace(brand,' ','') brand,branch
	from #ccltd 
	where 1=1
	--and brand in (select brand from tr_brands where id in (7,18,5,21,31,30,29,22,28,25))	
	--and branch='TG'
	group by causeOfRemove,replace(brand,' ',''),branch
) t pivot (
	 sum(cnt) for brand in ([(n/a)],[ADVANCE],[apollo],[BlueStreak],[Bridgestone],[CHENGSHAN],[CM335WESTLAKE],[CMIC],[continental],[CST],[Deestone],[DoubleCoin],[DRC],[Dunlop],[Goodyear],[Jinyu],[KINGSKY],[linglong],[Michelin],[MIRAGE],[Otani],[Volvo],[Yokohama])
)p
cross apply (select (
				isnull([(n/a)],0)+isnull([ADVANCE],0)+isnull([apollo],0)+isnull([BlueStreak],0)+isnull([Bridgestone],0)+isnull([CHENGSHAN],0)+isnull([CM335WESTLAKE],0)+
				isnull([CMIC],0)+isnull([continental],0)+isnull([CST],0)+isnull([Deestone],0)+isnull([DoubleCoin],0)+isnull([DRC],0)+isnull([Dunlop],0)+isnull([Goodyear],0)+
				isnull([Jinyu],0)+isnull([KINGSKY],0)+isnull([linglong],0)+isnull([Michelin],0)+isnull([MIRAGE],0)+isnull([Otani],0)+isnull([Volvo],0)+isnull([Yokohama],0)
)sumAll) aggr
order by branch,causeOfRemove
*/