--เริ่มการทำ transaction
declare @errorOccured int;
set @errorOccured = 0;
SET NOCOUNT ON;
begin transaction t1;
begin try 
declare @nextSeq int;
/*เริ่ม paste query*/

--ลบการติดตั้ง
delete from tr_tireInstallations where serialNumber='ZVK04009H' and requestId='32' and tpdId='178' and tpId='1' and trow='4' and tcolumn='1' and vehicleId='1275' and seq='1'; insert into tr_tireStockMovement (serialNumber,actionDate,treadDept,stockTypeId,updateBy,ipAddress,updateDate,comments,requestId,operationOf) values('ZVK04009H','2014/07/31',5.00,'2','2','192.168.0.170',getdate(),'ถอดออกจากรถ 1275','32','IR'); --
--ติดตั้งยาง
set @nextSeq = dbo.fn_getMaxInstallationSequence('1275','1311411104',178,1,'4','1','2014/07/31'); insert into tr_tireInstallations (serialNumber,causeId,vehicleId,mileage,seq,installDate,tpId,tpdId,trow,tcolumn,treadDept,requestId,comments,insSince) values('1311411104','5','1275',NULL,@nextSeq,'2014/07/31',1,178,'4','1',15,'32','ติดตั้งยางกรณีคีย์ไม่ทัน(เสียงว่า)',getdate()); delete from tr_tireStockMovement where serialNumber='1311411104' and actionDate='2014/07/31' and stockTypeId='9' and operationOf='IR'; insert into tr_tireStockMovement (serialNumber,actionDate,treadDept,stockTypeId,updateBy,ipAddress,updateDate,comments,requestId,operationOf) values('1311411104','2014/07/31',15,'9','2','192.168.0.170',getdate(),'ติดตั้งบนรถ 1275','32','IR'); 

/*จบ paste query*/
commit transaction t1; 
print 'ok'; --หากมัน run ผ่านมาได้ทั้งหมดจะเข้าสู่การ commit
end try
begin catch 
	declare @errorMessage varchar(max); 
	declare @errorNumber int;
	set @errorMessage = (SELECT convert(varchar(max),ERROR_NUMBER())+':'+ERROR_MESSAGE());
	print 'Error('+ @errorMessage+')';
	-- หากมัน catch error(มี error เกิดขึ้น) มันจะมาสู่การ rollback ย้อนกลับไปสู่ค่าเดิมทุกๆ table
	rollback transaction t1; 
	
end catch
--จบ transaction
--commit;