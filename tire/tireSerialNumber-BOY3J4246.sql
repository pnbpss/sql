SELECT [branchName],[description],removedSerialNumber as amountOfTire ,month([removedDate]) as [month]
FROM [Truck].[dbo].[tr_tireRemovalsDistByCause]
--group by [branchName],[description],month([removedDate])
order by [branchName],[description],month([removedDate]);

select 'stock',* from tr_tireStockMovement where serialNumber='BOY3J4246';
select 'install',* from tr_tireInstallations where serialNumber='BOY3J4246';
select 'remove',* from tr_tireRemovals where serialNumber='BOY3J4246';
select 'notInstalled',* from tr_notInstalledTires where serialNumber='BOY3J4246';