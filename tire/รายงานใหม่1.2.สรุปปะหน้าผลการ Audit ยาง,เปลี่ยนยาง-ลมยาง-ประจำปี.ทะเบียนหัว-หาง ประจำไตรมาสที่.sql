declare @month int;
declare @year int;
set @month = 6; set @year=2014
select b.id as branchId,b.name as branchName
,cntVehGroupbyBranchAndHT.typeCode 
,cntVehGroupbyBranchAndHT.cntVehicle
--��͹��� 1
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(auditedVehGroupByBranchAndHT.cntAudited,0)) as remainAuditedM1
,((isnull(auditedVehGroupByBranchAndHT.cntAudited,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAuditedM1
,(isnull(auditedVehGroupByBranchAndHT.cntAudited,0)) as cntAuditedM1

,isnull(cntInsRem.cntInsRemVehicle,0) as cntInsRemVehicleM1
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntInsRem.cntInsRemVehicle,0)) as remainInsRemVehicleM1
,((isnull(cntInsRem.cntInsRemVehicle,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentInsRemM1

,isnull(cntAirChecked.cntAirChecked,0) as cntAirCheckedM1
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntAirChecked.cntAirChecked,0)) as remainAirCheckedM1
,((isnull(cntAirChecked.cntAirChecked,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAirCheckM1

--��͹��� 2
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(auditedVehGroupByBranchAndHTM2.cntAudited,0)) as remainAuditedM2
,((isnull(auditedVehGroupByBranchAndHTM2.cntAudited,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAuditedM2
,(isnull(auditedVehGroupByBranchAndHTM2.cntAudited,0)) as cntAuditedM2

,isnull(cntInsRemM2.cntInsRemVehicle,0) as cntInsRemVehicleM2
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntInsRemM2.cntInsRemVehicle,0)) as remainInsRemVehicleM2
,((isnull(cntInsRemM2.cntInsRemVehicle,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentInsRemM2

,isnull(cntAirCheckedM2.cntAirChecked,0) as cntAirCheckedM2
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntAirCheckedM2.cntAirChecked,0)) as remainAirCheckedM2
,((isnull(cntAirCheckedM2.cntAirChecked,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAirCheckM2

--��͹��� 3
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(auditedVehGroupByBranchAndHTM3.cntAudited,0)) as remainAuditedM3
,((isnull(auditedVehGroupByBranchAndHTM3.cntAudited,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAuditedM3
,(isnull(auditedVehGroupByBranchAndHTM3.cntAudited,0)) as cntAuditedM3

,isnull(cntInsRemM3.cntInsRemVehicle,0) as cntInsRemVehicleM3
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntInsRemM3.cntInsRemVehicle,0)) as remainInsRemVehicleM3
,((isnull(cntInsRemM3.cntInsRemVehicle,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentInsRemM3

,isnull(cntAirCheckedM3.cntAirChecked,0) as cntAirCheckedM3
,cntVehGroupbyBranchAndHT.cntVehicle-(isnull(cntAirCheckedM3.cntAirChecked,0)) as remainAirCheckedM3
,((isnull(cntAirCheckedM3.cntAirChecked,0))*100)/cntVehGroupbyBranchAndHT.cntVehicle as percentAirCheckM3


--ö������ ��ṡ������-�ҧ
from tr_branches b inner join ( 
	select COUNT(v.id) cntVehicle,v.branchId, vt.typeCode
	 from tr_vehicles v inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	 group by v.branchId, vt.typeCode
) as cntVehGroupbyBranchAndHT on b.id=cntVehGroupbyBranchAndHT.branchId
--ö�������ʹԵ��͹�á
left join (
	select COUNT(distinct tav.vehicleId) cntAudited,v.branchId,vt.typeCode
	from tr_tireAuditingVehicles tav 
		inner join tr_tireAuditingForm taf on tav.formId=taf.id
		inner join tr_vehicles v on tav.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where month(taf.auditDate)=@month and year(taf.auditDate)=@year
	group by v.branchId,vt.typeCode
) as auditedVehGroupByBranchAndHT on b.id=auditedVehGroupByBranchAndHT.branchId
	and auditedVehGroupByBranchAndHT.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö����������¹/��Ѻ�ҧ
left join (
	select COUNT(distinct allUnion.vehicleId) as cntInsRemVehicle,allUnion.branchId,vt.typeCode from (
		select vehicleId,v.branchId,i.requestId from tr_tireInstallations i inner join tr_vehicles v on i.vehicleId=v.id where month(i.installDate)=@month and year(i.installDate)=@year
		union
		select vehicleId,v.branchId,r.requestId from tr_tireRemovals r inner join tr_vehicles v on r.vehicleId=v.id where month(r.removeDate)=@month and year(r.removeDate)=@year
		) as allUnion inner join tr_vehicles v on allUnion.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id 
		inner join tr_requestForTireMaintenances req on allUnion.requestId=req.id
	where req.approverNote not like '%��͹��ѧ%'
	group by allUnion.branchId,vt.typeCode
) as cntInsRem on b.id=cntInsRem.branchId and cntInsRem.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö�����������ҧ
left join
(
	select count(ctr.vehicleId) cntAirChecked,v.branchId,vt.typeCode
	from tr_changeTireRims ctr inner join tr_requestForTireMaintenances r on r.id=ctr.requestId
		inner join tr_vehicles v on ctr.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where r.airCheck='Y' and month(ctr.dateOfChange)=@month and year(ctr.dateOfChange)=@year
	group by v.branchId,vt.typeCode	
) as cntAirChecked on b.id=cntAirChecked.branchId and cntAirChecked.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö����ʹԵ��͹���2
left join (
	select COUNT(distinct tav.vehicleId) cntAudited,v.branchId,vt.typeCode
	from tr_tireAuditingVehicles tav 
		inner join tr_tireAuditingForm taf on tav.formId=taf.id
		inner join tr_vehicles v on tav.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where month(taf.auditDate)=@month+1 and year(taf.auditDate)=@year
	group by v.branchId,vt.typeCode
) as auditedVehGroupByBranchAndHTM2 on b.id=auditedVehGroupByBranchAndHTM2.branchId	and auditedVehGroupByBranchAndHTM2.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö����������¹/��Ѻ�ҧ��͹���2
left join (
	select COUNT(distinct allUnion.vehicleId) as cntInsRemVehicle,allUnion.branchId,vt.typeCode from (
		select vehicleId,v.branchId,i.requestId from tr_tireInstallations i inner join tr_vehicles v on i.vehicleId=v.id where month(i.installDate)=@month+1 and year(i.installDate)=@year
		union
		select vehicleId,v.branchId,r.requestId from tr_tireRemovals r inner join tr_vehicles v on r.vehicleId=v.id where month(r.removeDate)=@month+1 and year(r.removeDate)=@year
		) as allUnion inner join tr_vehicles v on allUnion.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id 
		inner join tr_requestForTireMaintenances req on allUnion.requestId=req.id
	where req.approverNote not like '%��͹��ѧ%'
	group by allUnion.branchId,vt.typeCode
) as cntInsRemM2 on b.id=cntInsRemM2.branchId and cntInsRemM2.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö�����������ҧ��͹��� 2
left join
(
	select count(ctr.vehicleId) cntAirChecked,v.branchId,vt.typeCode
	from tr_changeTireRims ctr inner join tr_requestForTireMaintenances r on r.id=ctr.requestId
		inner join tr_vehicles v on ctr.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where r.airCheck='Y' and month(ctr.dateOfChange)=@month+1 and year(ctr.dateOfChange)=@year
	group by v.branchId,vt.typeCode	
) as cntAirCheckedM2 on b.id=cntAirCheckedM2.branchId and cntAirCheckedM2.typeCode=cntVehGroupbyBranchAndHT.typeCode

--ö����ʹԵ��͹���3
left join (
	select COUNT(distinct tav.vehicleId) cntAudited,v.branchId,vt.typeCode
	from tr_tireAuditingVehicles tav 
		inner join tr_tireAuditingForm taf on tav.formId=taf.id
		inner join tr_vehicles v on tav.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where month(taf.auditDate)=@month+2 and year(taf.auditDate)=@year
	group by v.branchId,vt.typeCode
) as auditedVehGroupByBranchAndHTM3 on b.id=auditedVehGroupByBranchAndHTM3.branchId	and auditedVehGroupByBranchAndHTM3.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö����������¹/��Ѻ�ҧ��͹���3
left join (
	select COUNT(distinct allUnion.vehicleId) as cntInsRemVehicle,allUnion.branchId,vt.typeCode from (
		select vehicleId,v.branchId,i.requestId from tr_tireInstallations i inner join tr_vehicles v on i.vehicleId=v.id where month(i.installDate)=@month+2 and year(i.installDate)=@year
		union
		select vehicleId,v.branchId,r.requestId from tr_tireRemovals r inner join tr_vehicles v on r.vehicleId=v.id where month(r.removeDate)=@month+2 and year(r.removeDate)=@year
		) as allUnion inner join tr_vehicles v on allUnion.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id 
		inner join tr_requestForTireMaintenances req on allUnion.requestId=req.id
	where req.approverNote not like '%��͹��ѧ%'
	group by allUnion.branchId,vt.typeCode
) as cntInsRemM3 on b.id=cntInsRemM3.branchId and cntInsRemM3.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö�����������ҧ��͹��� 3
left join
(
	select count(ctr.vehicleId) cntAirChecked,v.branchId,vt.typeCode
	from tr_changeTireRims ctr inner join tr_requestForTireMaintenances r on r.id=ctr.requestId
		inner join tr_vehicles v on ctr.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where r.airCheck='Y' and month(ctr.dateOfChange)=@month+2 and year(ctr.dateOfChange)=@year
	group by v.branchId,vt.typeCode	
) as cntAirCheckedM3 on b.id=cntAirCheckedM3.branchId and cntAirCheckedM3.typeCode=cntVehGroupbyBranchAndHT.typeCode

--ö����ʹԵ��������
left join (
	select COUNT(distinct tav.vehicleId) cntAudited,v.branchId,vt.typeCode
	from tr_tireAuditingVehicles tav 
		inner join tr_tireAuditingForm taf on tav.formId=taf.id
		inner join tr_vehicles v on tav.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where month(taf.auditDate)=@month+2 and year(taf.auditDate)=@year
	group by v.branchId,vt.typeCode
) as auditedVehGroupByBranchAndHTM13 on b.id=auditedVehGroupByBranchAndHTM13.branchId and auditedVehGroupByBranchAndHTM13.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö����������¹/��Ѻ�ҧ��������
left join (
	select COUNT(distinct allUnion.vehicleId) as cntInsRemVehicle,allUnion.branchId,vt.typeCode from (
		select vehicleId,v.branchId,i.requestId from tr_tireInstallations i inner join tr_vehicles v on i.vehicleId=v.id where month(i.installDate) between @month and @month+2 and year(i.installDate)=@year
		union
		select vehicleId,v.branchId,r.requestId from tr_tireRemovals r inner join tr_vehicles v on r.vehicleId=v.id where month(r.removeDate) between @month and @month+2 and year(r.removeDate)=@year
		) as allUnion inner join tr_vehicles v on allUnion.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id 
		inner join tr_requestForTireMaintenances req on allUnion.requestId=req.id
	where req.approverNote not like '%��͹��ѧ%'
	group by allUnion.branchId,vt.typeCode
) as cntInsRemM13 on b.id=cntInsRemM13.branchId and cntInsRemM13.typeCode=cntVehGroupbyBranchAndHT.typeCode
--ö�����������ҧ��������
left join
(
	select count(ctr.vehicleId) cntAirChecked,v.branchId,vt.typeCode
	from tr_changeTireRims ctr inner join tr_requestForTireMaintenances r on r.id=ctr.requestId
		inner join tr_vehicles v on ctr.vehicleId=v.id
		inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	where r.airCheck='Y' and month(ctr.dateOfChange) between @month and @month+2 and year(ctr.dateOfChange)=@year
	group by v.branchId,vt.typeCode	
) as cntAirCheckedM13 on b.id=cntAirCheckedM3.branchId and cntAirCheckedM13.typeCode=cntVehGroupbyBranchAndHT.typeCode

where b.id<>'1000'
order by b.id,cntVehGroupbyBranchAndHT.typeCode


