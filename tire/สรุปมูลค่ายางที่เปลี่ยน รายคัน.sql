use truck;

declare @startDate datetime,@endDate datetime,@fleetId int;
set @startDate='20160101'; set @endDate='20160930'
IF OBJECT_ID('tempdb.dbo.#tempTb', 'U') IS NOT NULL DROP TABLE #tempTb;
select allUnion.requestId,allUnion.movementDateShow,allUnion.insVehicleId,allUnion.insSerialNumber
,allUnion.insNewOrRetread
,allUnion.insTreadPattern,allUnion.insPlateNumber,allUnion.insNumber,allUnion.iTreadDept,allUnion.insWheel
,allUnion.insCauseDesc,allUnion.remVehicle,allUnion.remSerialNumber,allUnion.remNewOrRetread,allUnion.remTreadPattern
,allUnion.remPlateNumber,allUnion.remNumber,allUnion.rTreadDept,allUnion.remWheel,allUnion.remCauseDesc
,allUnion.movementCase,allUnion.expense,allUnion.initInsTirePrice,allUnion.curInsTireValue,allUnion.branchId,b.name as branchName 
,allUnion.movementDate,st.stockType as keepRemovedTireIn
,case 
	when allUnion.movementCase = 1 or allUnion.movementCase=3 or allUnion.movementCase=4 or allUnion.movementCase=7 then 
		(select stockType from tr_tireStockTypes where id=dbo.fn_getTireFromStock4SubStitute(allUnion.insSerialNumber,allUnion.movementDate))
	else null	
 end as fetchTireFromStock
 ,dbo.FN_ConvertDate(mainTire.purchaseDate,'D/M/Y') purchaseDate,mainTire.price
 into #tempTb
from 
 (

	SELECT C.requestId, dbo.FN_ConvertDate(C.movementDate, 'D/M/Y') AS movementDateShow, C.insVehicleId,
	 C.insSerialNumber, insTire.newOrRetread AS insNewOrRetread, 
	  insTire.treadPattern AS insTreadPattern, insVehicle.plateNumber AS insPlateNumber, insVehicle.number
	 AS insNumber, C.iTreadDept, C.insWheel, 
	  ci.description AS insCauseDesc, NULL AS remVehicle, NULL AS remSerialNumber, NULL AS remNewOrRetread
	, NULL AS remTreadPattern, NULL 
	  AS remPlateNumber, NULL AS remNumber, NULL AS rTreadDept, NULL AS remWheel, NULL AS remCauseDesc, '7'
	 AS movementCase, 0 AS expense, 
	  insTire.price AS initInsTirePrice, CASE WHEN ittd.treadDept = 0 THEN 0 ELSE ((insTire.price * C.iTreadDept
	) / ittd.treadDept) END AS curInsTireValue, NULL 
	  AS branchId, CONVERT(varchar, C.movementDate, 111) AS movementDate
	FROM dbo.tr_tireRemInsMovements AS C INNER JOIN
	  dbo.tr_vehicles AS remVehicle ON C.remVehicle = remVehicle.id INNER JOIN
	  dbo.tr_requestForTireMaintenances AS rfm ON C.requestId = rfm.id INNER JOIN
	  dbo.tr_tires AS insTire ON C.insSerialNumber = insTire.serialNumber INNER JOIN
	  dbo.tr_vehicles AS insVehicle ON insVehicle.id = C.insVehicleId INNER JOIN
	  dbo.tr_causeOfInstallations AS ci ON C.insCause = ci.id INNER JOIN
	  dbo.tr_initTireTreadDept AS ittd ON C.insSerialNumber = ittd.serialNumber
	 WHERE (C.movementCase = '7') 
	 and C.movementDate between @startDate and @endDate and rfm.jobCompletedDate is not null 
	 
	 union 

	SELECT C.requestId,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow,C.insVehicleId,C.insSerialNumber,insTire.newOrRetread as insNewOrRetread
	,insTire.treadPattern as insTreadPattern,insVehicle.plateNumber as insPlateNumber,insVehicle.number as insNumber,C.iTreadDept
	,C.insWheel,ci.[description] as insCauseDesc,C.remVehicle,C.remSerialNumber as remSerialNumber
	,(select newOrRetread from tr_tires where serialNumber=C.remSerialNumber) as remNewOrRetread
	,(select treadPattern from tr_tires where serialNumber=C.remSerialNumber) as remTreadPattern
	,(select plateNumber from tr_vehicles where id=C.remVehicle) as remPlateNumber
	,(select number from tr_vehicles where id=C.remVehicle) as remNumber
	,C.rTreadDept as rTreadDept,C.remWheel ,cr.[description] as remCauseDesc,C.movementCase,C.expense ,insTire.price as initInsTirePrice
	,case when ittd.treadDept=0 then 0 else ((insTire.price*C.iTreadDept)/ittd.treadDept) end as curInsTireValue
	,rfm.branchId,CONVERT(varchar,C.movementDate,111) as movementDate
	from (
	tr_tireRemInsMovements C 
	inner join tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires insTire on C.insSerialNumber=insTire.serialNumber
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfInstallations ci on C.insCause=ci.id
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	inner join tr_initTireTreadDept ittd on C.insSerialNumber=ittd.serialNumber
	) 
	where C.movementCase='4' and C.remSerialNumber is not null and C.movementDate between @startDate and @endDate and rfm.jobCompletedDate is not null 

	union 

	select C.requestId,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow,C.insVehicleId,C.insSerialNumber,insTire.newOrRetread as insNewOrRetread,insTire.treadPattern as insTreadPattern
	,insVehicle.plateNumber as insPlateNumber,insVehicle.number as insNumber,C.iTreadDept,C.insWheel,ci.[description] as insCauseDesc,C.remVehicle
	,C.remSerialNumber,remTire.newOrRetread as remNewOrRetread,remTire.treadPattern as remTreadPattern,remVehicle.plateNumber as remPlateNumber,remVehicle.number as remNumber,C.rTreadDept 
	,C.remWheel,cr.[description] as remCauseDesc,C.movementCase,C.expense ,insTire.price as initInsTirePrice
	,case when ittd.treadDept=0 then 0 else ((insTire.price*C.iTreadDept)/ittd.treadDept) end as curInsTireValue ,rfm.branchId
	,CONVERT(varchar,C.movementDate,111) as movementDate
	from (
	tr_tireRemInsMovements C 
	left join tr_tireRemInsMovements COInsRem on C.requestId=COInsRem.requestId and C.insSince=COInsRem.insSince
	 and C.insSerialNumber=COInsRem.remSerialNumber
	)left join tr_tireRemInsMovements CORemIns on C.requestId=CORemIns.requestId and C.insSince=CORemIns.insSince and C.remSerialNumber=CORemIns.insSerialNumber

	/* case 3 �������ա�������������㹤���ͧ���ǡѹ */
	inner join tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	inner join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	inner join tr_tires insTire on C.insSerialNumber=insTire.serialNumber
	inner join tr_tires remTire on C.remSerialNumber=remTire.serialNumber
	inner join tr_causeOfInstallations ci on C.insCause=ci.id
	inner join tr_causeOfRemove cr on C.remCause=cr.id
	inner join tr_initTireTreadDept ittd on C.insSerialNumber=ittd.serialNumber
	where COInsRem.requestId is null and CORemIns.requestId is null and C.movementCase='3'
	 and C.movementDate between @startDate and @endDate and rfm.jobCompletedDate is not null 
	 
	union 

	select C.requestId,dbo.FN_ConvertDate(C.movementDate,'D/M/Y') as movementDateShow,C.insVehicleId
	,C.insSerialNumber,insTire.newOrRetread as insNewOrRetread,insTire.treadPattern as insTreadPattern
	,insVehicle.plateNumber as insPlateNumber,insVehicle.number as insNumber,C.iTreadDept,C.insWheel
	,ci.[description] as insCauseDesc,null as remVehicle,null as remSerialNumber,null as remNewOrRetread
	,null as remTreadPattern,null as remPlateNumber,null as remNumber
	,null as rTreadDept ,null as remWheel,null as remCauseDesc,C.movementCase,C.expense 
	,insTire.price as initInsTirePrice
	,case when ittd.treadDept=0 then 0 else ((insTire.price*C.iTreadDept)/ittd.treadDept) end as curInsTireValue
	,rfm.branchId,CONVERT(varchar,C.movementDate,111) as movementDate
	from (tr_tireRemInsMovements C inner join 
	tr_requestForTireMaintenances rfm on C.requestId=rfm.id
	inner join tr_vehicles insVehicle on C.insVehicleId=insVehicle.id
	inner join tr_tires insTire on C.insSerialNumber=insTire.serialNumber
	inner join tr_causeOfInstallations ci on C.insCause=ci.id
	inner join tr_initTireTreadDept ittd on C.insSerialNumber=ittd.serialNumber
	)left join tr_vehicles remVehicle on C.remVehicle=remVehicle.id
	where C.movementCase='1' and C.requestId<>'32'
	 and C.movementDate between @startDate and @endDate and rfm.jobCompletedDate
	 is not null
) as allUnion
left join tr_branches b on allUnion.branchId=b.id
left join tr_tireStockMovement tsm on allUnion.remSerialNumber=tsm.serialNumber and allUnion.movementDate=tsm.actionDate and tsm.operationOf='IR' and tsm.stockTypeId<>'9'
left join tr_tireStockTypes st on tsm.stockTypeId=st.id
left join tr_tires mainTire on allUnion.insSerialNumber=mainTire.serialNumber
order by requestId desc,insPlateNumber,movementCase


--select * from #tempTb
select cnt.branchName,cnt.insPlateNumber 
,cnt.M01 cntM01,ini.M01 iniM01,cur.M01 curM01,averg.M01 avgM01
,cnt.M02 cntM02,ini.M02 iniM02,cur.M02 curM02,averg.M02 avgM02
,cnt.M03 cntM03,ini.M03 iniM03,cur.M03 curM03,averg.M03 avgM03
,cnt.M04 cntM04,ini.M04 iniM04,cur.M04 curM04,averg.M04 avgM04
,cnt.M05 cntM05,ini.M05 iniM05,cur.M05 curM05,averg.M05 avgM05
,cnt.M06 cntM06,ini.M06 iniM06,cur.M06 curM06,averg.M06 avgM06
,cnt.M07 cntM07,ini.M07 iniM07,cur.M07 curM07,averg.M07 avgM07
,cnt.M08 cntM08,ini.M08 iniM08,cur.M08 curM08,averg.M08 avgM08
,cnt.M09 cntM09,ini.M09 iniM09,cur.M09 curM09,averg.M09 avgM09
,cnt.M10 cntM10,ini.M10 iniM10,cur.M10 curM10,averg.M10 avgM10
,cnt.M11 cntM11,ini.M11 iniM11,cur.M11 curM11,averg.M11 avgM11
,cnt.M12 cntM12,ini.M12 iniM12,cur.M12 curM12,averg.M12 avgM12
from 
(
	select branchName,insPlateNumber 
	,isnull([M01],0) [M01],isnull([M02],0) [M02],isnull([M03],0) [M03],isnull([M04],0) [M04],isnull([M05],0) [M05],isnull([M06],0) [M06],isnull([M07],0) [M07],isnull([M08],0) [M08],isnull([M09],0) [M09],isnull([M10],0) [M10],isnull([M11],0) [M11],isnull([M12],0) [M12]
	from
	(
	select b.name branchName,t.insPlateNumber,'M'+substring(t.movementDate,6,2) insMonth
	--,sum(t.initInsTirePrice) sumValue
	--,sum(t.curInsTireValue) sumValue
	--,case when count(*)=0 then 0 else sum(t.curInsTireValue)/count(*) end sumValue --�Ҥ�����µ�����
	,count(*) sumValue
	from #tempTb t 
	left join tr_requestForTireMaintenances r on t.requestId=r.id
	left join tr_branches b on r.branchId=b.id
	group by b.name,insPlateNumber,'M'+substring(movementDate,6,2)
	) t
	pivot (sum(sumValue) for insMonth in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
)cnt left join (
	select branchName,insPlateNumber 
	,isnull([M01],0) [M01],isnull([M02],0) [M02],isnull([M03],0) [M03],isnull([M04],0) [M04],isnull([M05],0) [M05],isnull([M06],0) [M06],isnull([M07],0) [M07],isnull([M08],0) [M08],isnull([M09],0) [M09],isnull([M10],0) [M10],isnull([M11],0) [M11],isnull([M12],0) [M12]
	from
	(
	select b.name branchName,t.insPlateNumber,'M'+substring(t.movementDate,6,2) insMonth
	,sum(t.initInsTirePrice) sumValue
	--,sum(t.curInsTireValue) sumValue
	--,case when count(*)=0 then 0 else sum(t.curInsTireValue)/count(*) end sumValue --�Ҥ�����µ�����
	--,count(*) sumValue
	from #tempTb t 
	left join tr_requestForTireMaintenances r on t.requestId=r.id
	left join tr_branches b on r.branchId=b.id
	group by b.name,insPlateNumber,'M'+substring(movementDate,6,2)
	) t
	pivot (sum(sumValue) for insMonth in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
)ini on cnt.branchName=ini.branchName and cnt.insPlateNumber=ini.insPlateNumber
left join (
	select branchName,insPlateNumber 
	,isnull([M01],0) [M01],isnull([M02],0) [M02],isnull([M03],0) [M03],isnull([M04],0) [M04],isnull([M05],0) [M05],isnull([M06],0) [M06],isnull([M07],0) [M07],isnull([M08],0) [M08],isnull([M09],0) [M09],isnull([M10],0) [M10],isnull([M11],0) [M11],isnull([M12],0) [M12]
	from
	(
	select b.name branchName,t.insPlateNumber,'M'+substring(t.movementDate,6,2) insMonth
	--,sum(t.initInsTirePrice) sumValue
	,sum(t.curInsTireValue) sumValue
	--,case when count(*)=0 then 0 else sum(t.curInsTireValue)/count(*) end sumValue --�Ҥ�����µ�����
	--,count(*) sumValue
	from #tempTb t 
	left join tr_requestForTireMaintenances r on t.requestId=r.id
	left join tr_branches b on r.branchId=b.id
	group by b.name,insPlateNumber,'M'+substring(movementDate,6,2)
	) t
	pivot (sum(sumValue) for insMonth in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
)cur on cnt.branchName=cur.branchName and cnt.insPlateNumber=cur.insPlateNumber
left join (
select branchName,insPlateNumber 
	,isnull([M01],0) [M01],isnull([M02],0) [M02],isnull([M03],0) [M03],isnull([M04],0) [M04],isnull([M05],0) [M05],isnull([M06],0) [M06],isnull([M07],0) [M07],isnull([M08],0) [M08],isnull([M09],0) [M09],isnull([M10],0) [M10],isnull([M11],0) [M11],isnull([M12],0) [M12]
	from
	(
	select b.name branchName,t.insPlateNumber,'M'+substring(t.movementDate,6,2) insMonth
	--,sum(t.initInsTirePrice) sumValue
	--,sum(t.curInsTireValue) sumValue
	,case when count(*)=0 then 0 else sum(t.curInsTireValue)/count(*) end sumValue --�Ҥ�����µ�����
	--,count(*) sumValue
	from #tempTb t 
	left join tr_requestForTireMaintenances r on t.requestId=r.id
	left join tr_branches b on r.branchId=b.id
	group by b.name,insPlateNumber,'M'+substring(movementDate,6,2)
	) t
	pivot (sum(sumValue) for insMonth in ([M01],[M02],[M03],[M04],[M05],[M06],[M07],[M08],[M09],[M10],[M11],[M12])) p
)averg on cnt.branchName=averg.branchName and cnt.insPlateNumber=averg.insPlateNumber
order by branchName,insPlateNumber
--select distinct '['+substring(t.movementDate,1,7)+'],' a from #tempTb t order by a