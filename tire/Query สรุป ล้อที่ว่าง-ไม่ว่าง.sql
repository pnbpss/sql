select v.id,v.plateNumber,v.number,b.name,vt.typeDesc,vt.typeCode,allWheel.cntAllWheel,countWheel.cntWheel
,isw.installedWheel,isw.notInstalledWheel
from tr_vehicles v 
	inner join tr_branches b on v.branchId=b.id
	inner join tr_vehicleTypes vt on v.vehicleTypeId=vt.id
	left join (
	select
		cti.vehicleId,COUNT(cti.vehicleId) as cntWheel
	from 
	tr_currentTireInstallationInfos cti
	group by cti.vehicleId
) as countWheel on v.id=countWheel.vehicleId
left join (
	select COUNT(vtpd.vehicleId) cntAllWheel,vtpd.vehicleId
	from tr_vehicleTirePositioningDetails vtpd
	group by vtpd.vehicleId
) as allWheel on v.id=allWheel.vehicleId
cross apply dbo.installedWheel(v.id) isw
where v.branchId<>1000

select tpd.userCall,cti.serialNumber
	from tr_vehicleTirePositioningDetails vtpd 
		 inner join tr_tirePositioningDetails tpd on vtpd.tpdId=tpd.id
		 and vtpd.tpId=tpd.tpId and vtpd.trow=tpd.trow and vtpd.tcolumn=tpd.tcolumn
		 left join tr_currentTireInstallationInfos cti
	on vtpd.vehicleId=cti.vehicleId and vtpd.tpdId=cti.tpdId and vtpd.tpId=cti.tpId
	   and vtpd.trow=cti.trow and vtpd.tcolumn=vtpd.tcolumn
where vtpd.vehicleId='1025' 