/*delete from TEST.dbo.HARMAST where SDATE<'2015/09/30'

delete FROM [TEST].[dbo].[HARPAY] where DDATE < '2007/01/01'



delete from TEST.dbo.BILLMAS where TMBILDT<'2015/09/30';
delete from TEST.dbo.CHQMAS where TMBILDT<'2015/09/30';
*/
use TEST;
DECLARE @continue INT
DECLARE @rowcount INT
DECLARE @TotalRowCount INT
set @TotalRowCount = 0;
SET @continue = 1
WHILE @continue = 1
BEGIN
    PRINT GETDATE()
    SET ROWCOUNT 10000
    BEGIN TRANSACTION
      delete FROM [TEST].[dbo].[HARPAY] where DDATE < '2012/01/01'
      --select count(*) FROM [TEST].[dbo].[HARPAY] where DDATE < '2008/01/01'
      SET @rowcount = @@rowcount 
      set @TotalRowCount = @TotalRowCount + @rowcount;
    COMMIT
    PRINT GETDATE()
    IF @rowcount = 0
    BEGIN
        SET @continue = 0
    END
END
print @TotalRowCount;