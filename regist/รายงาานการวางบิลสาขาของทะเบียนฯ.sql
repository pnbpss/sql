/****** Script for SelectTopNRows command from SSMS  ******/
declare @billDateFrom datetime; declare @billDateTo datetime;
set @billDateFrom ='20160601';set @billDateTo ='20160630'
/*
--จดใหม่
select billDate,billNo
,isnull(c1,0)c1,isnull(c2,0)c2,isnull(c3,0)c3,isnull(c4,0)c4,isnull(c5,0)c5
,signedName,strno ref,locat,customerFullName,plateNumber
from (
	SELECT
	count(*)cnt,dbo.FN_ConvertDate(billDate,'Y/M/D') billDate,b.strno,signedName,locat,'c'+convert(varchar,billCase) billCase,billNo,info.customerFullName,info.plateNumber
	FROM [serviceweb].[dbo].wb_billedbv2 b
	left join wb_firstRegistered info on b.strno=info.strno
	where billDate between @billDateFrom and @billDateTo
	group by dbo.FN_ConvertDate(billDate,'Y/M/D'),b.strno,signedName,locat,billCase,billNo,info.customerFullName,info.plateNumber
) t pivot
(
	sum(cnt) for billCase in ([c1],[c2],[c3],[c4],[c5])
) p


--แจ้งย้าย
select billDate,billNo
,isnull(c3,0)c3,isnull(c4,0)c4,isnull(c5,0)c5
,signedName,strno ref,locat,customerFullName,plateNumber
from (
	SELECT
	count(*)cnt,dbo.FN_ConvertDate(billDate,'Y/M/D') billDate,b.strno,signedName,locat,'c'+convert(varchar,billCase) billCase,billNo,info.customerFullName,info.plateNumber
	FROM [serviceweb].[dbo].wb_billedRegTraBV b
	left join wb_firstRegistered info on b.strno=info.strno
	where billDate between @billDateFrom and @billDateTo
	group by dbo.FN_ConvertDate(billDate,'Y/M/D'),b.strno,signedName,locat,billCase,billNo,info.customerFullName,info.plateNumber
) t pivot
(
	sum(cnt) for billCase in ([c3],[c4],[c5])
) p
*/
/*
--ต่อทะเบียน
select billDate,billNo
,isnull(c3,0)c3,isnull(c4,0)c4,isnull(c5,0)c5
,signedName,tmbillNo ref,locat,strno,customerFullName,extPlateNumber
from (
	SELECT
	count(*)cnt,dbo.FN_ConvertDate(billDate,'Y/M/D') billDate,tmbillNo,signedName,locat,'c'+convert(varchar,billCase) billCase,billNo,info.strno,c.customerFullName,info.extPlateNumber
	FROM [serviceweb].[dbo].wb_billedExtBv b
	left join wb_extensions info on b.tmbillNo=info.extendReceiptNumber
	left join wb_extensionCustomers c on info.extendReceiptNumber=c.TMBILL
	where billDate between @billDateFrom and @billDateTo --and tmbillNo='ฆTB-16040128'
	group by dbo.FN_ConvertDate(billDate,'Y/M/D'),tmbillNo,signedName,locat,'c'+convert(varchar,billCase),billNo,info.strno,c.customerFullName,info.extPlateNumber
) t pivot
(
	count(cnt) for billCase in ([c3],[c4],[c5])
) p
order by billDate,locat,billNo
*/

--โอนทะเบียน
select billDate,billNo
,isnull(c3,0)c3,isnull(c4,0)c4,isnull(c5,0)c5,isnull(c6,0)c6
,signedName,tmbillNo ref,locat,strno,customerFullName,plateNumber
from (
	SELECT
	count(*)cnt,dbo.FN_ConvertDate(billDate,'Y/M/D') billDate,tmbillNo,signedName,locat,'c'+convert(varchar,billCase) billCase,billNo,info.strno,c.customerFullName,info.plateNumber
	FROM [serviceweb].[dbo].wb_billedTraBv b 
	left join wb_transfers info on b.tmbillNo=info.transferReceiptNumber
	left join wb_transferCustomers c on info.transferReceiptNumber=c.TMBILL
	where billDate between @billDateFrom and @billDateTo
	group by dbo.FN_ConvertDate(billDate,'Y/M/D'),tmbillNo,signedName,locat,billCase,billNo,info.strno,c.customerFullName,info.plateNumber
) t pivot
(
	count(cnt) for billCase in ([c3],[c4],[c5],[c6])
) p
