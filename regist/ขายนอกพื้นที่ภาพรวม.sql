declare @fromdate datetime;
		declare @todate datetime;
		set @fromdate = '2013/1/1'; set @todate = '2013/1/1';
		declare @wholesale int; declare @newvehicle int; declare @oldvehicle int; declare @wrongzone int;

		declare @wrongZoneOldNewVehicle int; declare @wrongZoneFinanceVehicle int;
		set @wholesale =(select count(CONTNO) from wb_wholesaleALL where SDATE between @fromdate and @todate)
		set @newvehicle =(select count(a.CONTNO) from wb_wholesaleALL a left join wb_invtran i on a.STRNO=i.STRNO where a.SDATE between @fromdate and @todate and i.STAT='N')
		set @oldvehicle =(select count(a.CONTNO) from wb_wholesaleALL a left join wb_invtran i on a.STRNO=i.STRNO where a.SDATE between @fromdate and @todate and i.STAT='O')

		set @wrongZone = (
		select count(a.CONTNO)
		from (((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 --wb_customerForSaleZone ciz on a.CONTNO=ciz.CONTNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate
		)

		set @wrongZoneOldNewVehicle = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and vs.APCODE<>'030'
		)


		set @wrongZoneFinanceVehicle = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and vs.APCODE='030'
		)

		declare @wzApproveByChecker int --͹��ѵ��½������������Թ����
		set @wzApproveByChecker = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and swz.approveId='3'
		)

		declare @wzApproveByBranchMgr int --͹��ѵ��¼��Ѵ����Ң�
		set @wzApproveByBranchMgr = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and swz.approveId in ('1','2')
		)

		declare @haveApprovementPaper int --��˹ѧ���͹��ѵ�
		set @haveApprovementPaper = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and swz.approveId in ('4')
		)

		declare @waitApprovementPaper int --��˹ѧ���͹��ѵ�
		set @waitApprovementPaper = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and swz.approveId in ('5')
		)

		declare @unknowStatus int --�ѧ�����ʶҹ�
		set @unknowStatus = (
		select count(a.CONTNO)
		from ((((
			 wb_armast a 	 
			 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO) 
			 left join wb_vehicleSuppliers vs on a.STRNO=vs.STRNO) 	 
			 left join wb_invtran i on a.STRNO=i.STRNO)
			 left join wb_contractSaleWrongZone swz on a.CONTNO=swz.contno)
			 left join wb_contractSaleWrongZoneApprovements swzd on swz.approveId=swzd.id
		where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
		and a.SDATE between @fromdate and @todate and swz.approveId is null
		)


		select @wholesale as wholesale
			  ,@newvehicle as newvehicle
			  ,@oldvehicle as oldvehicle
			  ,@wrongzone as wrongzone
			  ,@wrongZoneOldNewVehicle as wrongZoneOldNewVehicle
			  ,@wrongZoneFinanceVehicle as wrongZoneFinanceVehicle
			  ,@wzApproveByChecker as wzApproveByChecker 
			  ,@wzApproveByBranchMgr as wzApproveByBranchMgr
			  ,@haveApprovementPaper as haveApprovementPaper
			  ,@waitApprovementPaper as waitApprovementPaper
			  ,@unknowStatus as unknowStatus into #tempTable;
			  
		select * from #tempTable;