declare @fromdate datetime;
declare @todate datetime;
set @fromdate = '2013/01/01'; set @todate = '2013/01/31';

select wholesale.zoneId,wholesale.LOCAT as wholesaleLOCAT ,wholesale.countALL,swzByZoneBranch.LOCAT
,swzByZoneBranch.soldItemsCount as wrongZoneItems
from
(
	select count(a.CONTNO) as countALL,csn.zoneId,a.LOCAT 
	from wb_wholesaleALL a left join wb_contractSaleZoneNumber csn on a.LOCAT=csn.branchId
	where a.SDATE between @fromdate and @todate
	and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
	group by csn.zoneId,a.LOCAT
) wholesale left join
(
	select csn.zoneId,a.LOCAT,count(a.LOCAT) as soldItemsCount
	from (
		 wb_armast a 	 
		 left join wb_contractsInRightZone ciz on a.CONTNO=ciz.CONTNO)
		 left join wb_contractSaleZoneNumber csn on a.LOCAT=csn.branchId
		
	where ciz.CONTNO is null and a.LOCAT not in ('MT��','OFF��','OFF��','OFF��','OFF¹','SI���','TH��7','TH��','TH��','THȫ','THʹ�')
	and a.SDATE between @fromdate and @todate
	group by csn.zoneId,a.LOCAT
--order by csn.zoneId
) swzByZoneBranch 
on wholesale.LOCAT=swzByZoneBranch.LOCAT and wholesale.zoneId=swzByZoneBranch.zoneId
order by wholesale.zoneId, wholesale.LOCAT